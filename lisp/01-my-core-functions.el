;; -*- lexical-binding: t; -*-


(require 'cl-macs) ; `cl-loop' is a macro who will cause problems when byte-compiling when not required
(require 'gv)
(require 'seq) ; Many `seq' functions are not autoloaded.
(require 'subr-x)

(require 'dash)
(require 's)
(require 'f)

(require '00-my-core-macros)

;;;###autoload (autoload #'color-lighten-name "color")
;;;###autoload (autoload #'notifications-notify "notifications")

;; while startup, refrain from using `dbus.el' since `dbus--init' is slow

;;;###autoload (autoload #'dbus-call-method "dbus")
;;;###autoload (autoload #'dbus-register-signal "dbus")

(defun my-defalias-maybe (symbol definition &optional docstring)
  (declare (indent defun))
  (cond
   ((fboundp symbol))
   (t
    (defalias symbol definition docstring)))
  symbol)

;;; Pure

;;;###autoload
(defun my-pos? (number)
  (declare (pure t) (side-effect-free t))
  (< 0 number))
(defalias #'my-pos-p #'#'my-pos?)

;;;###autoload
(defun my-neg? (number)
  (declare (pure t) (side-effect-free t))
  (< number 0))
(defalias #'my-neg-p #'#'my-neg?)

;;;###autoload
(defun my-safe-substring (str &optional l r)
  (declare (pure t) (side-effect-free t))
  (let ((len (length str)))
    (let ((l (max (or l 0) 0))
          (r (min (or r len) len)))
      (if (< r l)
          ""
        (substring str l r)))))

;;;###autoload
(defun my-keep (func &rest sequences)
  "Return non-nil results of FUNC applied over all SEQUENCES.
FUNC's arity must match the number of SEQUENCES."
  (seq-remove #'null (apply #'seq-mapn func sequences)))

;;;###autoload
(defun my-flatten-1 (sequences)
  (declare (pure t) (side-effect-free t))
  (seq-mapcat #'identity sequences))

;;;###autoload
(defun my-keep-indexed (predicate sequence)
  "Return non-nil results of PREDICATE applied on each of SEQUENCE.
PREDICATE must take 2 arguments: the element and its index."
  (seq-remove #'null (seq-map-indexed predicate sequence)))

;;;###autoload
(cl-defun my-range (start|stop &optional stop (step 1))
  (declare (pure t) (side-effect-free t))
  (cond
   ((not stop)
    (number-sequence 0 (1- start|stop)))
   ;; When STOP - START is divisible by STEP, don't include STOP
   ((= 0 (% (- stop start|stop) step))
    (number-sequence start|stop (- stop step) step))
   (t
    (number-sequence start|stop stop step))))

;;;###autoload
(defun my-at (seq position)
  "Return SEQ's element at POSITION cyclically.
See also `my-at*' for the safe version that doesn't raise errors."
  (declare (pure t) (side-effect-free t))
  (elt
   seq
   (if (< position 0)
       (+ position (length seq))
     position)))

;;;###autoload
(defun my-at* (seq position &optional fallback-value)
  "Like (`my-at' SEQ POSITION), but defaults to FALLBACK-VALUE when out of range."
  (declare (pure t) (side-effect-free t))
  (condition-case _err
      (my-at seq position)
    ((args-out-of-range arith-error) fallback-value)))

(defun my-in--index* (seq idx)
  (if (< idx 0)
      (+ idx (length seq))
    idx))
;;;###autoload
(defun my-in (sequence &rest indexes)
  (declare (pure t) (side-effect-free t))
  (-reduce-from (lambda (seq idx)
                  (elt seq (my-in--index* seq idx)))
                sequence
                indexes))
(gv-define-setter my-in (value sequence &rest indexes)
  `(setf ,(-reduce-from (lambda (seq idx)
                          `(elt ,seq (my-in--index* ,seq ,idx)))
                        sequence
                        indexes)
         ,value))

;;;###autoload
(defun my-inserted-at (seq position &rest elements)
  "Return SEQ with ELEMENTS inserted at POSITION."
  (declare (pure t) (side-effect-free t))
  (append (seq-subseq seq 0 position)
          elements
          (seq-subseq seq position)
          nil)) ;; ensure list(s)


;;;###autoload
(defun my-removed-at (seq &rest positions)
  "Return SEQ with elements at POSITIONS removed."
  (declare (pure t) (side-effect-free t))
  (let* ((l (length seq))
         (cyclic-positions (-map (-cut mod <> l) positions)))
    (->> seq
         (-map-indexed (lambda (i e)
                         (if (member i cyclic-positions)
                             nil
                           (list e))))
         -non-nil
         (-map #'car))))

;;;###autoload
(defun my-plist-remove (plist key)
  "Return PLIST without KEY and its value in-destructively."
  (declare (pure t) (side-effect-free t))
  (named-let recur ((plist* plist)
                    (accu '()))
    (-let* ((init (car plist*)))
      (cond ((seq-empty-p plist*)
             (nreverse accu))
            ((equal init key)
             (recur (cddr plist*)
                    accu))
            (t
             (recur (cdr plist*)
                    (cons init accu)))))))

;;;###autoload
(defun my-split-mixed-plist (lst)
  "Partition LST into 2 two groups as a cons cell: one with keys
and values and one with others."
  (declare (pure t) (side-effect-free t))
  (cons (my-key-value-in-mixed-plist lst)
        (my-non-key-value-in-mixed-plist lst)))

;;;###autoload
(defun my-key-value-in-mixed-plist (lst)
  (declare (pure t) (side-effect-free t))
  (my-loop [tail lst reversed-plist '()]
    (cond ((length= tail 0)
           (nreverse reversed-plist))
          ((keywordp (car tail))
           (recur (cddr tail)
                  `(,(cadr tail) ,(car tail) . ,reversed-plist)))
          (t
           (recur (cdr tail)
                  reversed-plist)))))

;;;###autoload
(defun my-non-key-value-in-mixed-plist (lst)
  (declare (pure t) (side-effect-free t))
  (my-loop [tail lst reversed-non-plist '()]
    (cond ((length= tail 0)
           (nreverse reversed-non-plist))
          ((keywordp (car tail))
           (recur (cddr tail)
                  reversed-non-plist))
          (t
           (recur (cdr tail)
                  `(,(car tail) . ,reversed-non-plist))))))

;;;###autoload
(defun my-seq-differences (lst0 lst1)
  "Return 2 lists of member differences as a cons cell.
- Members of LST0 who are not in LST1.
- Members of LST1 who are not in LST0."
  (declare (pure t) (side-effect-free t))
  (cons (seq-difference lst0 lst1)
        (seq-difference lst1 lst0)))

;;;###autoload
(defun my-n-below (n)
  "Return \\'(0 1 .. (1- n))."
  (declare (pure t) (side-effect-free t))
  (number-sequence 0 (1- n)))

;;;###autoload
(defun my-cyclic-literal-string-substitute (origin &rest strs)
  "Replace STRS in ORIGIN in cycle.
Replace STRS[-2] with STRS[-1], [-3] with [-2], etc and [-1] with
[0] in ORIGIN."
  (declare (pure t) (side-effect-free t))
  (--> origin
       ;; Split the string using the last of STRS as the delimiter, into segments
       (split-string it (regexp-quote (car (last strs))))
       ;; Replace in each segment, each pair of of STRS
       (-reduce-from
        (lambda (segments pair)
          (-let* (((from . to) pair))
            (-map
             (-partial #'replace-regexp-in-string (regexp-quote from) to)
             segments)))
        it
        ;; Replace from right to left to prevent overriding with duplicates
        (nreverse
         ;; Collect the pairs of replacements
         (cl-loop for (from to) on strs
                  ;; Exclude the last list ( var nil )
                  when to
                  collect (cons from to))))
       ;; Finally concatenate segments, fill each void with the first of STRS, completing a cyclic replacement
       (mapconcat #'identity it (car strs))))

;;;###autoload
(defun my-repeat-list (n l)
  "Return a list as a result of N L concatenated."
  (declare (pure t) (side-effect-free t))
  (cl-loop repeat n append l))

;;;###autoload
(defun my-multiply-string (num str)
  "Return the concatenation of NUM STRs."
  (declare (pure t) (side-effect-free t))
  (string-join (-repeat num str) ""))

;;;###autoload
(defun my-non-empty-list? (l)
  "Return whether L is a non-empty proper list."
  (declare (pure t) (side-effect-free t))
  (and l (proper-list-p l)))
(defalias 'my-non-empty-list-p 'my-non-empty-list?)

;;;###autoload
(cl-defun my-color-hex-to-name (hex &optional (digits 2))
  (declare (pure t) (side-effect-free t))
  (-some
   (lambda (name)
     (if (equal hex (apply #'color-rgb-to-hex
                           (append (color-name-to-rgb name) (list digits))))
         name
       nil))
   (defined-colors)))

;;;###autoload
(defun my-color-mean (colors &optional digits/component)
  (declare (pure t) (side-effect-free t))
  (-let* ((vals (-map #'color-name-to-rgb colors))
          (r_total (my-mean (-map #'cl-first vals)))
          (g_total (my-mean (-map #'cl-second vals)))
          (b_total (my-mean (-map #'cl-third vals)))
          (n (length colors)))
    (color-rgb-to-hex
     (/ r_total n) (/ g_total n) (/ b_total n) (or digits/component 2))))

;;;###autoload
(cl-defun my-int-to-bits (num &optional (len 0))
  (declare (pure t) (side-effect-free t))
  (if (< num 2)
      (append
       (cl-loop for i below (1- len)
                collect 0)
       (list num))
    (append (my-int-to-bits (/ num 2) (1- len))
            (list (% num 2)))))

;;;###autoload
(defun my-bits-to-int (bits)
  (declare (pure t) (side-effect-free t))
  (my-loop ((bits bits)
            (result 0))
    (if bits
        (recur
         (cdr bits)
         (+ result
            (* (car bits) (expt 2 (length (cdr bits))))))
      result)))

;;;###autoload
(defun my-decimal-part-to-bits (decimal bit-length)
  (declare (pure t) (side-effect-free t))
  (my-loop ((decimal decimal)
            (bit-length bit-length)
            (result '()))
    (cond ((zerop bit-length)
           result)
          ((<= 0.5 decimal)
           (recur (* 2 (- decimal 0.5)) (1- bit-length) (append result '(1))))
          ((> 0.5 decimal)
           (recur (* 2 decimal) (1- bit-length) (append result '(0)))))))

;;;###autoload
(cl-defun my-decimal-to-bits (num &optional (decimal-bit-length 52))
  (declare (pure t) (side-effect-free t))
  (let ((int-part (truncate num)))
    (cons (my-int-to-bits int-part)
          (my-decimal-part-to-bits (abs (- num int-part)) decimal-bit-length))))

;;;###autoload
(defun my-2/two-complement-int (num)
  (declare (pure t) (side-effect-free t))
  (let ((bits (my-int-to-bits (abs num))))
    (if (<= 0 num)
        bits
      (last
       (my-int-to-bits
        (1+ (my-bits-to-int
             (-map (-partial #'- 1) bits))))
       (length bits)))))

;;;###autoload
(defun my-by-length< (s0 s1)
  (declare (pure t) (side-effect-free t))
  (< (length s0) (length s1)))

;;;###autoload
(defun my-by-length> (s0 s1)
  (declare (pure t) (side-effect-free t))
  (> (length s0) (length s1)))

;;;###autoload
(defun my-ensure-list (proper-list-candidate)
  "If PROPER-LIST-CANDIDATE is already a proper list, return it,
else return a list which contains it."
  (declare (pure t) (side-effect-free t))
  (if (proper-list-p proper-list-candidate)
      proper-list-candidate
    (list proper-list-candidate)))

(defun my-delist (arg)
  "If ARG is a list, return its car, else return itself."
  (declare (pure t) (side-effect-free t))
  (if (listp arg) (car arg) arg))

;;;###autoload
(defun my-ensure-kbd (arg)
  (declare (pure t) (side-effect-free t))
  (if (stringp arg)
      (kbd arg)
    arg))

;;;###autoload
(defun my-promote-universal-prefix-arg (arg)
  (declare (pure t) (side-effect-free t))
  (cond
   ((null arg)
    '(4))
   ((numberp arg)
    (* 4 (max 1 arg)))
   (t
    (list (* 4 (car arg))))))

;;;###autoload
(defun my-drop-trailing-nils (seq)
  (declare (pure t) (side-effect-free t))
  (nreverse
   (seq-drop-while #'null (reverse seq))))

;;;###autoload
(defun my-factorial (num)
  "Return NUM!."
  (declare (pure t) (side-effect-free t))
  (if (and (integerp num)
           (<= 0 num))
      (my-loop [res 1 num* num]
        (if (= 0 num*)
            res
          (recur (* res num*) (- num* 1))))
    (error "(my-factorial %d)" num)))

;;;###autoload
(defun my-variance (lst)
  "Calculate the variance of `LST': variance(X) = E(X^2) - (E(X))^2."
  (declare (pure t) (side-effect-free t))
  (- (my-mean (seq-map (my-fn% (expt %1 2)) lst))
     (expt (my-mean lst) 2)))

;;;###autoload
(defun my-mean (lst)
  "Calculate the arithmetic mean of `LST'."
  (declare (pure t) (side-effect-free t))
  (/ (float (apply '+ lst)) (length lst)))

;;;###autoload
(defun my-mean* (&rest lst)
  "Calculate the arithmetic mean of `LST'."
  (declare (pure t) (side-effect-free t))
  (/ (float (apply '+ lst)) (length lst)))

;;;###autoload
(defun my-hook? (sym)
  "Return SYM when it's name looks like a hook."
  (and (string-match-p "-\\(hook\|functions\\)" (symbol-name sym))
       sym))
(defalias 'my-hook-p 'my-hook?)

;;;###autoload
(defun my-list-of-functions-p (arg)
  (and (proper-list-p arg)
       (-every-p (-orfn #'functionp #'symbolp) arg)))

;;;###autoload
(defun my-ensure-function-list (arg)
  (cond ((functionp arg)
         (list arg))
        ((my-list-of-functions-p arg)
         arg)
        (t
         (list arg))))

;;;###autoload
(cl-defun my-regexp-match (regexp string &optional (group-num 0))
  (and (string-match regexp string)
       (match-string group-num string)))

;;;###autoload
(defun my-regexp-match-strings (&optional string)
  "Depends on previous `match-data'.
STRING is used if non-nil, else current buffer."
  (my-for [(beg end) (-partition 2 (match-data))]
    (and beg end
         (if string
             (substring string beg end)
           (buffer-substring beg end)))))

;;;###autoload
(defun my->str (arg)
  (declare (pure t) (side-effect-free t))
  (format "%s" arg))

;;;###autoload
(defun my-Str->str (Str)
  "Unquote/unescape double quotes in STR.
Essentially the reverse of `my->Str', so STR must have double
quotes around it to return only string the represent it whole
self."
  (--> (read-from-string Str) (car it)))

;;;###autoload
(defun my-str (&rest args)
  (declare (pure t) (side-effect-free t))
  (mapconcat #'my->str args ""))

;;;###autoload
(defun my-str* (&rest args)
  "`my-str' on ARGS with interposed spaces."
  (declare (pure t) (side-effect-free t))
  (mapconcat #'my->str args " "))

;;;###autoload
(defun my-safe-string-no-properties (str)
  (declare (pure t) (side-effect-free t))
  (if (stringp str)
      (substring-no-properties str)
    str))

;;;###autoload
(defun my-cyclic||loop-list? (lst)
  (declare (pure t) (side-effect-free t))
  (my-loop [slow-head lst fast-head (cdr lst)]
    (cond
     ((or (null fast-head)
          (null slow-head))
      nil)
     ((equal slow-head fast-head)
      t)
     (t
      (recur (cdr slow-head) (cddr fast-head))))))
(defalias 'my-cyclic||loop-list-p 'my-cyclic||loop-list?)

;;;###autoload
(defun my-returned-map (map-func transform-func seq)
  "Apply a special map function MAP-FUNC that doesn't return
normally on SEQ, return TRANSFORM-FUNC's return values applied on
SEQ as it was called inside MAP-FUNC."
  (let (my-mut-map--retval)
    (funcall map-func
             (lambda (&rest args)
               (push (apply transform-func args)
                     my-mut-map--retval))
             seq)
    (nreverse my-mut-map--retval)))

;;;###autoload
(defun my-string-special-regexp|not-literal-p (str)
  (declare (pure t) (side-effect-free t))
  (not (string= str (regexp-quote str))))

;;;###autoload
(defun my-valid-regexp? (str)
  (declare (pure t) (side-effect-free t))
  (condition-case _
      (or (string-match-p str "")
          t)
    (invalid-regexp nil)))
;;;###autoload
(defalias #'my-valid-regexp-p #'my-valid-regexp?)

;;;###autoload
(defun my-ensure-valid-regexp-or-literal (str)
  (declare (pure t) (side-effect-free t))
  (if (my-valid-regexp? str)
      str
    (regexp-quote str)))

;;;###autoload
(defun my-split-string-by-space-and-ensure-validity (str)
  (declare (pure t) (side-effect-free t))
  (-map #'my-ensure-valid-regexp-or-literal
        (split-string str " ")))

;;;###autoload
(defun my-symbol-to-sentence (sym)
  (declare (pure t) (side-effect-free t))
  (s-capitalize
   (replace-regexp-in-string
    "\\(.\\)-"
    "\\1 "
    (string-remove-prefix (format "%s" 'my-)
                          (format "%s" sym)))))

;;;###autoload
(defconst my-golden-ratio (/ (+ 1 (sqrt 5)) 2.0))
;;;###autoload
(cl-defun my-golden-ratio (&optional (power 1))
  (declare (pure t) (side-effect-free t))
  (expt (/ (+ 1 (sqrt 5)) 2.0)
        power))

;;;###autoload
(defun my**2 (power)
  (declare (pure t) (side-effect-free t))
  (expt 2 power))

;;;###autoload
(defun my-step-to-0 (num)
  (declare (pure t) (side-effect-free t))
  (cond ((zerop num) 0)
        ((< 0 num) (1- num))
        ((< num 0) (1+ num))))

;;;###autoload
(cl-defun my-replace-newlines (str &optional (replacer "⏎"))
  (declare (pure t) (side-effect-free t))
  (string-replace "\n" replacer str))

;;;###autoload
(defun my-length-alpha< (str0 str1)
  (declare (pure t) (side-effect-free t))
  (or (< (length str0) (length str1))
      (and (= (length str0) (length str1))
           (string< str0 str1))))

;;;###autoload
(defun my-count-unique (lst)
  "Return an alist of unique elements in LST and their count
there."
  (declare (pure t) (side-effect-free t))
  (--> (-uniq lst)
       (-map (lambda (elem)
               (cons elem
                     (-count (-cut equal <> elem) lst)))
             it)
       (cl-sort it '> :key 'cdr)))

;;;###autoload
(defun my-parse-lines->alist (str separator &optional trim regexp)
  "Construct an alist from lines in STR.
For each line of STR who matches REGEXP, take the substring
before SEPARATOR as key and the one after (with TRIM trimmed) as
value."
  (declare (pure t) (side-effect-free t))
  (let ((regexp (or regexp
                    ;; """(alias )?<alias-name>='<alias-definition>'"""
                    (concat "^\\([^ ]+ \\)?[^ ]+" separator))))
    (-keep (lambda (line)
             (and (string-match-p regexp line)
                  (-let (((k v) (s-split-up-to separator line 1)))
                    (cons k
                          (if trim
                              (string-trim v trim trim)
                            v)))))
           (split-string-and-unquote str "\n"))))

;;;###autoload
(defun my-ensure-string-prefix-suffix (str prefix &optional suffix)
  (--> (my-ensure-string-prefix prefix str)
       (my-ensure-string-suffix (or suffix prefix) it)))

;;;###autoload
(defun my-uppercase? (obj)
  (declare (pure t) (side-effect-free t))
  (equal obj (upcase obj)))
;;;###autoload
(defalias #'my-uppercase-p #'my-uppercase?)

;;;###autoload
(defun my-pad-string-by-columns (width padding str &optional left)
  "Assume that PADDING's length is 1."
  (let ((final-length (* width
                         (ceiling (/ (length str)
                                     (float width))))))
    (funcall (if left #'s-pad-left #'s-pad-right)
             final-length
             padding
             str)))

;;;###autoload
(defun my-min->=-multiple (obj-size bin-size)
  "The smallest multiple of BIN-SIZE that isn't smaller than OBJ-SIZE."
  (* (ceiling (/ (float obj-size) bin-size)) bin-size))

;;;###autoload
(defun my-equal* (&rest args)
  (declare (pure t) (side-effect-free t))
  (my-binary->seq-test #'equal args))

;;;###autoload
(defun my-=* (&rest args)
  (declare (pure t) (side-effect-free t))
  (my-binary->seq-test #'= args))

;;;###autoload
(defun my-make-vector (len init &optional copier)
  (declare (pure t) (side-effect-free t))
  (--> (cl-loop for _ below len
                collect (if copier
                            (funcall copier init)
                          init))
       (seq-into it 'vector)))

;;;###autoload
(cl-defun my-make-tensor (dimension-lengths
                          init-value
                          &key (init-copier #'identity) (seq-copier #'copy-sequence))
  (declare (pure t) (side-effect-free t))
  (-let* ((last-to-1st-dims (reverse dimension-lengths))
          (last-dim (seq-first last-to-1st-dims))
          (to-1st-dims (seq-into (seq-rest last-to-1st-dims) 'list)))
    (-reduce-from (lambda (sub-tensor dim-size)
                    (my-make-vector dim-size sub-tensor seq-copier))
                  (my-make-vector last-dim init-value init-copier)
                  to-1st-dims)))

;;;###autoload
(defun my-sequence? (arg)
  "(`sequencep' ARG) but not `consp', whose `cdr' is non-nil?"
  (declare (pure t) (side-effect-free t))
  (cond
   ((consp arg) (proper-list-p arg))
   (t (sequencep arg))))
;;;###autoload
(defalias #'my-sequence-p #'my-sequence?)

;;;###autoload
(defun my-non-empty-sequence? (arg)
  (declare (pure t) (side-effect-free t))
  (and (my-sequence? arg)
       (< 0 (length arg))))
;;;###autoload
(defalias #'my-non-empty-sequence-p #'my-non-empty-sequence?)

;;;###autoload
(cl-defun my-tree-contain-node? (tree node &optional (compare-fn #'equal))
  (declare (pure t) (side-effect-free t))
  (cond ((funcall compare-fn tree node)
         t)
        ((sequencep tree)
         (or (my-tree-contain-node? (seq-first tree) node)
             (-let* ((tail (seq-rest tree)))
               (or
                ;; a non-nil CDR
                (and node
                     (not (sequencep tail))
                     (funcall compare-fn tail node))
                (and (my-non-empty-sequence? tail)
                     (my-tree-contain-node? tail node))))))
        (t
         nil)))
;;;###autoload
(defalias #'my-tree-contain-node-p #'my-tree-contain-node?)

;;;###autoload
(cl-defun my-text-property-display-image? (string &optional (position 0))
  (declare (pure t) (side-effect-free t))
  (-some--> string
    (get-text-property 0 'display it)
    imagep))
(defalias #'my-text-property-display-image-p #'my-text-property-display-image?)

;;;###autoload
(cl-defun my-remove-text-property-display-image (string &optional (beg 0) (end 1))
  (if (my-text-property-display-image? string beg)
      (remove-text-properties beg end 'display string)
    string))

;;;###autoload
(defun my-rx->str (form)
  "(`rx-to-string' FORM 'no-group)."
  (declare (pure t) (side-effect-free t))
  (rx-to-string form 'no-group))

;;;###autoload
(defun my-alist-assoc-all (key alist &optional testfn)
  (-let* ((testfn (or testfn #'equal)))
    (-filter (-lambda ((k . _))
               (funcall testfn k key))
             alist)))

;;;###autoload
(defun my-elt (seq idx)
  (declare (pure t) (side-effect-free t))
  (ignore-error args-out-of-range
    (elt seq idx)))

;;;###autoload
(defun my-first-chars-alphabetically-sorted? (strings)
  (declare (pure t) (side-effect-free t))
  (--> (-map (lambda (cand)
               (or (my-elt cand 0)
                   0))
             strings)
       (apply #'<= it)))
(defalias #'my-first-chars-alphabetically-sorted-p #'my-first-chars-alphabetically-sorted?)

;;;###autoload
(defun my-plist-get* (plist prop &optional pred)
  "Get multiple elements after PROP and before another keyword (or end) in PLIST.
PRED defaults to `equal'."
  (-when-let* ((pred (or pred #'equal))
               (key-idx
                (-find-last-index (-lambda (%1) (funcall pred prop %1)) plist)))
    (--> (-slice plist (+ 1 key-idx)) (-take-while (-not #'keywordp) it))))

;;;###autoload
(defun my-mixed-plist-get (lst prop)
  "`plist-get' PROP on LST's pure keyword plist part."
  (declare (pure t) (side-effect-free t))
  (-let* ((plist (--> (-drop-while (-not 'keywordp) lst))))
    (plist-get plist prop)))

;;;###autoload
(defun my-mixed-plist-put (lst &rest kw-vals)
  "Return LST with the trailing pure trailing plist part modified by KW-VALS."
  (declare (pure t) (side-effect-free t))
  (-let* ((plist (--> (-drop-while (-not 'keywordp) lst)
                      (-reduce-from
                       (-lambda (accu (prop val))
                         (plist-put accu prop val))
                       it
                       (-partition 2 kw-vals)))))
    (append (-take-while (-not 'keywordp) lst)
            plist)))

;;;###autoload
(defun my-split-leading-plist (lst)
  (declare (pure t) (side-effect-free t))
  (named-let recur ((plist '())
                    (remain lst))
    (-let* (((e0 e1 . tail) remain))
      (cond
       ((keywordp e0)
        (recur `(,e1 ,e0 . ,plist)
               tail))
       (t
        (list (nreverse plist)
              remain))))))

;;;###autoload
(defun my-interger? (num)
  (declare (pure t) (side-effect-free t))
  (and (numberp num)
       (= num (round num))))
(defalias #'my-interger-p #'my-interger?)

;;;###autoload
(defun my-natural-num? (num)
  (declare (pure t) (side-effect-free t))
  (and (my-interger? num)
       (<= 0 num)))
(defalias #'my-natural-num-p #'my-natural-num?)

;;;###autoload
(defun my-quote|escape-spaces-in-filename|path-for-shell (path)
  "Primarily escape spaces is PATH.
Useful to quote file names. Uses `shell-quote-argument' under the
hood, but prevents it from quoting \"~\"."
  (-let* ((tilde-flag (string-prefix-p "~" path)))
    (-->
     path
     (if tilde-flag
         (expand-file-name it)
       it)
     (shell-quote-argument it)
     (if tilde-flag
         (abbreviate-file-name it)
       it))))

;;;###autoload
(cl-defun my-text-property-values (obj &optional (pos 0))
  (declare (pure t) (side-effect-free t))
  (--> (text-properties-at pos obj)
       (-slice it 1 nil 2)))

;;;###autoload
(cl-defun my-alphabet (&optional (init-char "a") (amount 26))
  "Return AMOUNT successive characters (as strings) from INIT-CHAR."
  (declare (pure t) (side-effect-free t))
  (mapcar (lambda (i) (char-to-string (+ (string-to-char init-char) i)))
          (number-sequence 0 (1- amount))))

;;;###autoload
(defun my-calculate (str-expr &optional return-string &rest calc-eval-args)
  "Use `calc-eval' to compute infix STR-EXPR.
`calc-multiplication-has-precedence' is disabled to align with
normal mathematics. Non-nil RETURN-STRING: don't convert to a
number before returning. CALC-EVAL-ARGS are passed to the
underlying function."
  (dlet ((calc-multiplication-has-precedence nil))
    (-let* ((result (apply #'calc-eval str-expr calc-eval-args)))
      (if return-string
          result
        (string-to-number result)))))

;;;###autoload
(defun my-string->string-list (str)
  (--> (string-to-list str) (-map #'char-to-string it)))

;;;###autoload
(defun my-get-in* (nested-map &rest keys)
  "(`map-nested-elt' NESTED-MAP KEYS).
Being variadic a as syntactic sugar, use the said function in
code instead."
  (map-nested-elt nested-map keys))

;;;###autoload
(defun my-string-prefix-ignore-case? (prefix str)
  (declare (side-effect-free t))
  (string-prefix-p prefix str t))
;;;###autoload
(defalias #'my-string-prefix-ignore-case-p #'my-string-prefix-ignore-case?)

;;;###autoload
(defun my-string-suffix-ignore-case? (suffix str)
  (declare (side-effect-free t))
  (string-suffix-p suffix str t))
;;;###autoload
(defalias #'my-string-suffix-ignore-case-p #'my-string-suffix-ignore-case?)

;;;###autoload
(defun my-plist? (obj)
  (declare (side-effect-free t) (pure t))
  (and (proper-list-p obj)
       (-let* ((len (length obj)))
         (and (= 0 (% len 2))
              (named-let
                  recur ((lst obj))
                (-let* (((key _ . tail) lst))
                  (cond
                   ((seq-empty-p lst)
                    t)
                   ((not (keywordp key))
                    nil)
                   (t
                    (recur tail)))))))))
;;;###autoload
(defalias #'my-plist-p #'my-plist?)

;;;###autoload
(defun my-merge-plist (plist1 plist2 &optional prefer-latter)
  "Merge PLIST1 and PLIST2.
When a key already in PLIST1 appears in PLIST2: if PREFER-LATTER,
use the latter's value, else (default) use the former's."
  (declare (pure t) (side-effect-free t))
  (named-let
      recur
      ((ret-plist (copy-sequence plist1))
       (existing-props (-slice plist1 0 nil 2))
       (plist2-pairs (-partition 2 plist2)))
    (-let* (((prop val) (car plist2-pairs)))
      (cond
       ((seq-empty-p plist2-pairs)
        ret-plist)
       (prefer-latter
        (recur (plist-put ret-plist prop val #'equal) '() (cdr plist2-pairs)))
       ((member prop existing-props)
        (recur ret-plist existing-props (cdr plist2-pairs)))
       (t
        (recur
         `(,@ret-plist ,prop ,val)
         (cons prop existing-props)
         (cdr plist2-pairs)))))))

;;;###autoload
(defun my-truncate-left-of-string
    (str end-col &optional beg-col padding ellipsis &rest args)
  (-->
   (reverse str)
   (apply #'truncate-string-to-width
          it
          end-col
          beg-col
          padding
          ellipsis
          args)
   (reverse it)))

;;;###autoload
(defun my-alist-merge-duplicate-keys (alist)
  "Return ALIST with duplicate keys have their values concatenated."
  (named-let
      recur ((retval '()) (remain alist))
    (cond
     ((seq-empty-p remain)
      (reverse retval))
     (:else
      (-let* (((k0 . v0) (car remain))
              ([other-vals filtered-remain]
               (named-let
                   recur1
                   ((other-vals-1 '())
                    (filtered-remain-1 '())
                    (remain-1 (cdr remain)))
                 (cond
                  ((seq-empty-p remain-1)
                   (vector (reverse other-vals-1) (reverse filtered-remain-1)))
                  (:else
                   (-let* ((cell (car remain-1))
                           ((k1 . v1) cell))
                     (cond
                      ((equal k0 k1)
                       (recur1
                        (append v1 other-vals-1)
                        filtered-remain-1
                        (cdr remain-1)))
                      (:else
                       (recur1
                        other-vals-1
                        (cons cell filtered-remain-1)
                        (cdr remain-1))))))))))
        (recur
         (cons (cons k0 (append v0 other-vals)) retval) filtered-remain))))))

;;;###autoload
(defun my-plist-remove-nil (plist)
  (named-let
      recur ((lst plist) (retval '()))
    (cond
     ((= 0 (length lst))
      (nreverse retval))
     (:else
      (-let* (((k v . rest) lst))
        (cond
         ((null v)
          (recur rest retval))
         (:else
          ;; reverse later as above
          (recur rest `(,v ,k ,@retval)))))))))

;;;###autoload
(cl-defun my-seq-replace-at (coll pos-new-lst &optional call pad-value)
  "For each (POS NEW) in POS-NEW-LST, replace COLL's element at POS by NEW.
CALL: when-non-nil, treat each NEW as a function that takes the old
element and return the new element. PAD-VALUE: value to right-pad when
COLL is shorter than POS-NEW-LST's need."
  (cond
   ((seq-empty-p pos-new-lst)
    coll)
   (:else
    (-let* ((replacing-len
             (--> (seq-map #'seq-first pos-new-lst) (seq-max it) (+ 1 it)))
            (coll*
             (cond
              ((< (length coll) replacing-len)
               (seq-concatenate 'list
                                coll
                                (-repeat
                                 (- replacing-len (length coll)) pad-value)))
              (:else
               coll))))
      (seq-map-indexed
       (lambda (elem idx)
         (-let* ((replace (assoc idx pos-new-lst))
                 ((_ new) replace))
           (cond
            ((not replace)
             elem)
            (call
             (funcall new elem))
            (:else
             new))))
       coll*)))))

;;;###autoload
(defun my-resolve-minor-mode-setter-argument (arg)
  (cond
   ((eq arg 'toggle)
    'toggle)
   ((and (numberp arg) (< arg 1))
    nil)
   (t
    t)))

;;;###autoload
(defun my-clamp (number floor ceil)
  (min ceil (max floor number)))

;;;###autoload
(cl-defun my-string-truncate-middle (str len-beg len-end &optional (ellipsis-str "…"))
  (-let* ((target-len (+ len-beg len-end (length ellipsis-str))))
    (cond
     ((<= (length str) target-len)
      str)
     (:else
      (concat
       (substring str 0 len-beg)
       ellipsis-str
       (substring str (- (length str) len-end)))))))

;;; Helpers

;;;###autoload
(defun my-append-separate (pred lst)
  ;; (named-let recur ((satisfied '()) (unsatisfied '())
  ;;                   (left lst))
  ;;   (cond ((seq-empty-p left)
  ;;          (append (nreverse satisfied) (nreverse unsatisfied)))
  ;;         ((-let* ((elem (car left)))
  ;;            (if (funcall pred elem)
  ;;                (recur (cons elem satisfied) unsatisfied (cdr left))
  ;;              (recur satisfied (cons elem unsatisfied) (cdr left)))))))
  (--> (-separate pred lst)
       (append (nth 0 it) (nth 1 it))))

;;;###autoload
(defun my-juxtapose-call (funcs &rest args)
  (-map (lambda (f) (apply f args))
        funcs))

;;;###autoload
(defun my-shuffle (lst)
  "Return LST with orders of elements randomly shuffled."
  (my-loop [shuffled '() remain (seq-into lst 'list)]
    (cond ((seq-empty-p remain)
           (nreverse shuffled))
          (t
           (-let* ((picked-position (random (length remain))))
             (recur (cons (nth picked-position remain) shuffled)
                    (-remove-at picked-position remain)))))))

;;;###autoload
(defun my-lexical-eval (form)
  (eval form t))

;;;###autoload
(defun my-unary->variadic-call (func &rest args)
  "Call FUNC on ARGS.
FUNC is a function that can accept a list as its first argument."
  (declare (indent defun))
  (funcall func args))

;;;###autoload
(defun my-binary->seq-test (pred args)
  "Return if binary PRED is non-nil for every consecutive pair of ARGS."
  (cl-every pred args (cdr args)))

;;;###autoload
(defun my-defalias* (symbol definition &optional docstring)
  "Like (`defalias' SYMBOL DEFINITION DOCSTRING), return SYMBOL."
  (declare (indent defun))
  (defalias symbol definition docstring)
  symbol)

;;;###autoload
(defun my-callable-p (obj)
  (or (functionp obj) (macrop obj)))

;;;###autoload
(defun my-safe-call (sym &rest args)
  (when (functionp sym)
    (apply sym args)))

;;;###autoload
(cl-defun my->valid-symbol-name (str &optional (separator "_"))
  (save-match-data
    (mapconcat (lambda (char)
                 (-let* ((ss (char-to-string char)))
                   (if (string-match lisp-mode-symbol-regexp ss)
                       ss
                     separator)))
               str
               "")))

;;;###autoload
(defun my-form-sym (form &rest args)
  "Intern a symbol whose name is in format FORM ARGS.
Beware when the name is too long, it may hide annotations in the
minibuffer."
  (intern (my->valid-symbol-name (apply #'format form args))))

;;;###autoload
(defun my-safe-add-hook/s (hook/s functions &optional depth local)
  (declare (indent defun))
  (my-add-hook/s hook/s
    (-filter #'functionp functions) ;; `fboundp' doesn't work on lambdas
    depth local))

;;;###autoload
(defun my-add-hook/s (hook/s functions &optional depth local)
  "`add-hook' multiple FUNCTIONS to a single/multiple HOOK/S.
DEPTH, LOCAL are passed to it."
  (declare (indent defun))
  (dolist (hook (ensure-list hook/s))
    (dolist (func (reverse functions))
      (add-hook hook func depth local))))

;;;###autoload
(defun my-remove-hook/s (hook/s functions &optional local)
  (declare (indent defun))
  (dolist (hook (ensure-list hook/s))
    (dolist (func functions)
      (remove-hook hook func local))))

;;;###autoload
(defun my-add-live-buffer-hook (hook/s functions &optional depth)
  (declare (indent defun))
  (dolist (func functions)
    (when (fboundp func)
      (let ((func-live-buf (my-concat-symbols '@ 'my func 'when-live-buffer--h)))
        (defalias func-live-buf (lambda (&rest _)
                                  (when (buffer-live-p (current-buffer))
                                    (funcall func))))
        (dolist (hook (ensure-list hook/s))
          (add-hook hook func-live-buf depth))))))

;;;###autoload
(defun my-add-advice/s (symbol/s where symbol|function-list &optional props)
  "Add SYMBOL|FUNCTION-LIST to SYMBOL/S.
WHERE and PROPS are passed to `advice-add'."
  (declare (indent defun))
  (-let* ((symbols (ensure-list symbol/s))
          (functions (my-ensure-function-list symbol|function-list)))
    (dolist (sym symbols)
      (dolist (func functions)
        (advice-add sym where func props)))))

;;;###autoload
(defun my-remove-advice/s (symbol/s function/s-or-where &optional symbol|function-list)
  "Remove SYMBOL|FUNCTION-LIST from SYMBOL/S.
When SYMBOL|FUNCTION-LIST is nil, take from FUNCTION/S-OR-WHERE
instead (which is actually optional, just be there for
`my-add-advice/s''s compatibility)."
  (declare (indent defun))
  (let ((symbols (ensure-list symbol/s))
        (functions (my-ensure-function-list (or symbol|function-list
                                                function/s-or-where))))
    (dolist (sym symbols)
      (dolist (func functions)
        (advice-remove sym func)))))

;;;###autoload
(defun my-remove-advice (symbol function)
  "(`advice-remove' SYMBOL FUNCTION) interactively."
  (interactive (if-let* ((sym
                          (my-completing-read-symbol
                           nil
                           nil
                           'fboundp
                           nil
                           nil
                           nil
                           (thing-at-point 'symbol)))
                         (adv (my-completing-read-advice sym)))
                   (list sym adv)
                 (user-error "No advices.")))
  (when (called-interactively-p t)
    (message "%S" `(advice-remove ,symbol ,function)))
  (advice-remove symbol function))

;;;###autoload
(defun my-ensure-keyword (symbol)
  (if (keywordp symbol)
      symbol
    (intern (format ":%s" symbol))))

;;;###autoload
(defun my-define-keymap (&rest bindings)
  (-let* ((kmap (make-sparse-keymap)))
    (cl-loop for (k d) in (-partition 2 bindings)
             do (define-key kmap (my-ensure-kbd k) d))
    kmap))

;;;###autoload
(defun my-y-or-n-forms (&rest forms)
  (declare (indent defun))
  (when (y-or-n-p (--> (-map (lambda (form) (pp form))
                             forms)
                       (string-join it " ")))
    (my-lexical-eval `(progn ,@forms))))

;;;###autoload
(cl-defun my-ensure-hash-table (hash-table-symbol &rest args &key (test #'equal)
                                                  &allow-other-keys)
  "When HASH-TABLE-SYMBOL's value is nil, assign it a new hash table created with ARGS.
ARGS, TEST are passed to `make-hash-table'. Return the hash
table."
  (unless (symbol-value hash-table-symbol)
    (set hash-table-symbol
         (apply #'make-hash-table :test test
                (my-plist-remove args :local))))
  (symbol-value hash-table-symbol))

;;;###autoload
(defun my-assoc-hash (key table)
  (-let* ((unhashed (make-symbol ""))
          (got-val (gethash key table unhashed)))
    (if (equal got-val unhashed)
        nil
      (cons key got-val))))

;;;###autoload
(defun my-plist-put-ensure (plist prop val &rest other-p-v)
  "Ensure that PROP appears in PLIST.
If PROP presents, just return PLIST unmodified, else put VAL.
Do the same for OTHER-P-V."
  (my-loop [accu plist
                 p-v `((,prop ,val) ,@(-partition 2 other-p-v))]
    (cond
     ((seq-empty-p p-v)
      accu)
     (t
      (-let* (((p v) (car p-v)))
        (recur (if (my-mixed-plist-get accu p)
                   accu
                 (my-mixed-plist-put accu p v))
               (cdr p-v)))))))

;;;###autoload
(defun my-set-ensure-defined-variable (symbl init-value &optional local)
  (unless (boundp symbl)
    (set symbl init-value))
  (when local
    (make-variable-buffer-local symbl))
  (symbol-value symbl))

;;;###autoload
(defun my-set-local (var val)
  (make-local-variable var)
  (set var val))

;;;###autoload
(defun my-symbol-function-without-advices (func)
  "Return just FUNC's `symbol-function' without its added advices.
Credit: `corfu--metadata-get'."
  (advice--cd*r (symbol-function func)))

;;; Background

;; NOTE: need speed.
;;;###autoload
(defun my-project-root (&optional dir)
  "Return DIR (or `default-directory')\'s project root path."
  (declare (side-effect-free t))
  (-let* ((prj (project-current nil dir)))
    (and prj (project-root prj))))

(defvar my-project-marker-file-list '(".project"))

;;;###autoload
(defun my-project-file-marker-root-fn (&optional dir)
  (-some-->
      (-some
       (lambda (file)
         (locate-dominating-file (or dir default-directory) file))
       my-project-marker-file-list)
    (cons 'my-project-file-marker-root-fn it)))

;;;###autoload
(defun my-project-bottom-up-root-fn (&optional dir)
  (-let* ((normalize-dir-fn
           (lambda (it)
             (--> (expand-file-name it) (file-name-as-directory it))))
          (parent-fn
           (lambda (path) (file-name-directory (directory-file-name path))))
          (init-dir
           (--> (or dir default-directory) (funcall normalize-dir-fn it)))
          (vc-proj (project-try-vc init-dir))
          (root-by-vc
           (-some--> vc-proj (project-root it) (funcall normalize-dir-fn it)))
          (found-root
           (named-let
               recur ((cur-dir init-dir))
             (-let* ((parent-dir (funcall parent-fn cur-dir)))
               (cond
                ((equal root-by-vc cur-dir)
                 vc-proj)
                ((-some
                  (lambda (marker-file)
                    (file-exists-p (file-name-concat cur-dir marker-file)))
                  my-project-marker-file-list)
                 (cons
                  'my-project-bottom-up (abbreviate-file-name cur-dir)))
                ((equal parent-dir cur-dir)
                 nil)
                (t
                 (recur parent-dir)))))))
    (-some--> found-root)))

(cl-defmethod project-root ((project (head my-project-bottom-up)))
  (cdr project))

;;;###autoload
(defun my-project-root-maybe (&optional dir)
  (ignore-error (file-missing file-error void-function)
    (my-project-root dir)))

;;;###autoload
(defun my-project-root-or-current-directory ()
  (abbreviate-file-name
   (or (my-project-root)
       default-directory)))

;;;###autoload
(defun my-project-directory-is-root-p (dir)
  (or
   ;; check the most common case quickly
   (file-directory-p (file-name-concat dir ".git"))
   (let ((pr (my-project-root-maybe dir)))
     (and pr (f-equal-p pr dir)))))

;;;###autoload
(defun my-filename-relative-dwim (path)
  "Return PATH that is relative to `my-project-root-or-current-directory'."
  (file-relative-name path (my-project-root-or-current-directory)))

;;;###autoload
(defun my-outline-level-at-point ()
  (save-match-data
    (save-excursion
      (beginning-of-line)
      (and (looking-at-p outline-regexp)
           (funcall outline-level)))))

;;;###autoload
(defvar my-time-ISO-format "%FT%T%z"
  "Doesn't affect `my-ISO-time'.")
;;;###autoload
(defun my-ISO-time (&optional time zone no-special-chars)
  (declare (side-effect-free t))
  (format-time-string (cond
                       (no-special-chars
                        "%FT%H-%M-%S%z")
                       (:else
                        "%FT%T%z"))
                      (or time (current-time)) zone))

;;;###autoload
(defun my-time-hour-in-day-float* ()
  "Not precise fraction-wise!"
  (string-to-number (format-time-string "%H.%M%S%N")))

(defun my-time-encode-hhmmss--encode (hhmmss decoded-now)
  (encode-time
   (list
    (% hhmmss 100)
    (% (floor (/ hhmmss 100)) 100)
    (floor (/ hhmmss 10000))
    (decoded-time-day decoded-now)
    (decoded-time-month decoded-now)
    (decoded-time-year decoded-now)
    nil
    nil
    nil)))

;;;###autoload
(defun my-time-encode-hhmm (hhmm-maybe-ss)
  (declare (pure t) (side-effect-free t))
  (-let* ((hhmmss
           (-->
            hhmm-maybe-ss
            ;; keep only numbers
            (replace-regexp-in-string "[^0-9]" "" it)
            ;; pad so that HHMM -> HHMM00
            (string-pad it 6 ?0 nil) (string-to-number it)))
          (now (current-time))
          (decoded-now (decode-time now)))
    (my-time-encode-hhmmss--encode hhmmss decoded-now)))

;;;###autoload
(defun my-time-convert-hhmm-to-future (hhmm-maybe-ss)
  "Shift HHMM-MAYBE-SS to tomorrow when passed today."
  (declare (side-effect-free t))
  (-let* ((time (my-time-encode-hhmm hhmm-maybe-ss)))
    (if (time-less-p time (current-time))
        (time-add time (* 24 60 60))
      time)))

;;;###autoload
(defun my-run-at-future-hhmm-time (hhmm-maybe-ss repeat function &rest args)
  (apply #'run-at-time
         (my-time-convert-hhmm-to-future hhmm-maybe-ss)
         repeat
         function
         args))

;;;###autoload
(defun my-line-non-whitespace-beg-position (&optional pos)
  (save-excursion
    (when pos
      (goto-char pos))
    (back-to-indentation)
    (point)))

;;;###autoload
(define-obsolete-function-alias
  #'my-back-to-indentation-position #'my-line-non-whitespace-beg-position "")

;;;###autoload
(defun my-line-indent-offset ()
  (declare (side-effect-free t))
  (save-excursion
    (back-to-indentation)
    (current-column)))

;;;###autoload
(defun my-column-at-position (&optional pos)
  (declare (side-effect-free t))
  (save-excursion
    (goto-char (or pos (point)))
    (current-column)))

;;;###autoload
(defun my-set-mark-from-to (other-end new-point)
  "Set the mark at OTHER-END and goto NEW-POINT.
Effectively making a region."
  (push-mark other-end nil 'activate)
  (goto-char new-point))

;;;###autoload
(defun my-mark-bounds-as-cons-maybe (bounds &optional beg-to-end)
  (-when-let* (((beg . end) bounds))
    (if beg-to-end
        (my-set-mark-from-to beg end)
      (my-set-mark-from-to end beg))))

(defun my-having-some-opened-files-p ()
  (declare (side-effect-free t))
  (-some #'buffer-file-name (buffer-list)))

(defun my-get-emacs-version-of-elc-file (file)
  (with-temp-buffer
    (insert-file-contents file)
    (goto-char (point-min))
    (re-search-forward ";; in Emacs version \\(.*\\)$")
    (match-string 1)))

;;;###autoload
(defun my-find-minimum-indentation-of-buffer ()
  (declare (side-effect-free t))
  ;; (string-match-p (rx (not blank)) (thing-at-point 'line t))
  (save-excursion
    (goto-char (point-min))
    (named-let
        recur ((min-indent most-positive-fixnum))
      (let ((min-indent
             (min (if (looking-at-p "^$")
                      min-indent
                    (string-match-p (rx (not blank)) (thing-at-point 'line t)))
                  min-indent)))
        (let ((line (line-number-at-pos)))
          (forward-line 1)
          (beginning-of-line)
          (if (or (eobp) (= line (line-number-at-pos)))
              (if (= min-indent most-positive-fixnum)
                  0
                min-indent)
            (recur min-indent)))))))

;;;###autoload
(defun my-find-minimum-indentation-of-string (str)
  (declare (pure t) (side-effect-free t))
  ;; (string-match-p (rx (not blank)) (thing-at-point 'line t))
  (-let* ((lines (--> (split-string str "\n" 'omit-nulls)
                      (-filter (lambda (line)
                                 (string-match-p "^[[:space:]]*[^[:space:]]" line))
                               it))))
    (-min (-map (lambda (line)
                  (string-match-p "[^[:space:]]" line))
                lines))))

;;;###autoload
(defun my-buffers-with-minor-mode (mode/s &optional buf-lst)
  "Return buffers where one on MODE/S is active."
  (declare (side-effect-free t))
  (-let* ((modes (ensure-list mode/s)))
    (-filter (lambda (buf)
               (-some (lambda (mode)
                        (buffer-local-value mode buf))
                      modes))
             (or buf-lst (buffer-list)))))

;;;###autoload
(defun my-buffers-with-major-mode/s (mode/s &optional buf-lst)
  (declare (side-effect-free t))
  (let* ((modes (ensure-list mode/s)))
    (-filter (lambda (buf)
               (apply #'provided-mode-derived-p
                      (buffer-local-value 'major-mode buf)
                      modes))
             (or buf-lst (buffer-list)))))

;;;###autoload
(defun my-beg-defun-position (&optional nil-if-not-found)
  (declare (side-effect-free t))
  (save-excursion
    ;; Don't take the previous defun when at the top
    (when (and (bolp) (not (eobp)))
      (forward-char))
    (let* ((bod-retval (beginning-of-defun)))
      (and (or bod-retval (not nil-if-not-found)) (point)))))

;;;###autoload
(defun my-end-defun-position (&optional trim-space)
  (declare (side-effect-free t))
  (save-excursion
    ;; If moving backward when `eolp', then `end-of-defun', we may end up moving
    ;; to the next defun
    (goto-char (my-beg-defun-position))
    (end-of-defun)
    (when trim-space
      (skip-chars-backward " \t\n\r"))
    (point)))

;;;###autoload
(cl-defun my-bounds-of-defun (&optional treesit-tactic)
  "Faster than (thing-at-point \\'defun)?.
Problem with (`bounds-of-thing-at-point' 'defun): it first go to the end
defun, then go to the beg; if current nested defun is at the end of
parent defun, the go beg operation will jump to parent's instead of the
nested one's."
  (declare (side-effect-free t))
  (dlet ((treesit-defun-tactic
          (or treesit-tactic (bound-and-true-p treesit-defun-tactic))))
    (cons (my-beg-defun-position) (my-end-defun-position))))

;;;###autoload
(defun my-code-completion-get-metadata-prop (prop)
  (cond
   ;; get nil?
   ;; ((bound-and-true-p corfu-mode)
   ;;  (corfu--metadata-get prop))
   (:else
    (-let* ((cird4 (nth 4 completion-in-region--data)))
      (cond
       ((member prop cird4)
        (plist-get cird4 prop))
       (:else
        (plist-get completion-extra-properties prop)))))))

(defvar my-code-completion-metadata--kind-function #'ignore)
(defvar my-code-completion-metadata--empty-input nil)
(defvar my-code-completion-metadata--input nil)

;;;###autoload
(defun my-score-code-completion-candidate (cand)
  "Score CAND by my taste, the less the better."
  (declare (pure t) (side-effect-free t))
  (let* ((l (length cand))
         (kind
          (funcall my-code-completion-metadata--kind-function cand))
         (snippet-flag (member kind '(snippet))))
    (cond
     ((zerop l)
      0)
     ;; when empty input: waiting for "semantic" completions so snippets are pushed farther
     ((and my-code-completion-metadata--empty-input snippet-flag)
      (* 1024 l))

     ((and snippet-flag
           ;; like a "bookmark", a prioritized snippet
           (string-suffix-p "⭐" cand))
      (/ l 4.0))

     ;; Push candidates whose names are peculiar lower
     ((not (my-common-character? (aref cand 0)))
      (* 2 l))
     ((equal my-code-completion-metadata--input cand)
      (* 2 l))
     (t
      l))))

;;;###autoload
(defun my-common-character? (char)
  (or (<= ?a char ?z)
      (<= ?A char ?Z)
      (<= ?0 char ?9)
      (= char ?-)))
;;;###autoload
(defalias #'my-common-character-p #'my-common-character?)

(defun
    my-sort-code-completion-candidates--delete-consecutive-duplicates--prefer-text-properties
    (cands)
  (named-let
      recur ((lst cands) (reversed-retval '()))
    (let* ((head (nth 0 lst)))
      (cond
       ((= 0 (length lst))
        (nreverse reversed-retval))
       ;; most common case: different candidates
       ((not (equal head (nth 1 lst)))
        (recur (cdr lst) (cons head reversed-retval)))
       (:else
        (let*
            ((consec-dup (-take-while (lambda (cand) (equal head cand)) lst))
             (filtered
              (-->
               consec-dup

               ;; ;; only keep candidates with text properties
               ;; (-filter (lambda (cand) (text-properties-at 0 cand)) it)
               ;; (cond
               ;;  ;; accidentally all candidates don't have text properties, keep 1
               ;;  ((= 0 (length it))
               ;;   (list head))
               ;;  (:else
               ;;   it))

               (-remove
                (lambda (cand)
                  ;; remove candidates from "dabbrev" that are the same as others
                  (member
                   (funcall my-code-completion-metadata--kind-function
                            cand)
                   '(text)))
                it))))
          (recur
           (nthcdr (length consec-dup) lst)
           (append (nreverse filtered) reversed-retval))))))))

;;;###autoload
(defun my-sort-code-completion-candidates (cands)
  "Sort CANDS."
  (-let* (((beg end _table _pred _extra-meta-data . _)
           completion-in-region--data))
    (dlet ((my-code-completion-metadata--kind-function
            (or (my-code-completion-get-metadata-prop :company-kind) #'ignore))
           (my-code-completion-metadata--empty-input (equal beg end))
           (my-code-completion-metadata--input
            (and beg end (buffer-substring beg end))))
      ;; `seq-sort-by' can also be used, but we don't really need immutability here
      (-let* ((sorted
               (cl-sort cands #'< :key #'my-score-code-completion-candidate)))
        (-->
         sorted
         (my-sort-code-completion-candidates--delete-consecutive-duplicates--prefer-text-properties
          it))))))

;;;###autoload
(defun my-sort-code-completion-candidates-maybe (candidates)
  (if (my-first-chars-alphabetically-sorted? candidates)
      (my-sort-code-completion-candidates candidates)
    candidates))

;;;###autoload
(defun my-1st-fn (&rest args)
  "Return the first one that is a function in ARGS."
  (declare (side-effect-free t))
  (-find #'functionp args))

;;;###autoload
(defun my-get-line-string-at-position (&optional pos length-limit no-properties)
  (declare (side-effect-free t))
  (my-get-line-string-at-line (line-number-at-pos pos) length-limit no-properties))

;;;###autoload
(defun my-get-line-string-at-line (line &optional length-limit no-properties)
  (declare (side-effect-free t))
  (save-excursion
    (goto-char (point-min)) (forward-line (1- line))
    ;; (unless no-properties
    ;;   (font-lock-ensure (line-beginning-position) (line-end-position)))
    ;; (or (thing-at-point 'line no-properties) "")
    (--> (let ((beg (line-beginning-position)))
           (funcall
            (if no-properties #'buffer-substring-no-properties #'buffer-substring)
            beg
            (if length-limit
                (min (line-end-position) (+ beg length-limit))
              (line-end-position))))
         (string-remove-suffix "\n" it))))

;;;###autoload
(defun my-fbound-p (sym)
  (declare (side-effect-free t))
  (or (boundp sym) (fboundp sym)))

;;;###autoload
(defun my-point-in-emptyish-line-p ()
  (declare (side-effect-free t))
  (save-excursion
    (beginning-of-line)
    (looking-at-p "[ \t]*$")))

;;;###autoload
(defun my-buffer-emptyish-p (&optional buf)
  "Does BUF only contain white spaces (if any)?"
  (declare (side-effect-free t))
  (with-current-buffer (or buf (current-buffer))
    (string-blank-p
     (string-trim
      (buffer-substring-no-properties (point-min) (point-max))))))

;;;###autoload
(defun my-balanced-parens-and-brackets-p ()
  (declare (side-effect-free t))
  (save-excursion
    (condition-case _err
        (not (check-parens))
      ('user-error
       nil))))

;;;###autoload
(defun my-expand-path (path &optional dir)
  (declare (side-effect-free t))
  (dlet (file-name-handler-alist)
    (expand-file-name path dir)))

;;;###autoload
(defun my-apply-interactively (func &rest args)
  "Like `apply' FUNC on ARGS but mark the call as interactive."
  (apply #'funcall-interactively
         func
         `(,@(butlast args) ,@(car (last args)))))

;;;###autoload
(defun my-add-list! (list-var elements &optional at-end)
  "Add ELEMENTS to LIST-VAR's global value.
If AT-END, add to the end, else to the beginning. LIST-VAR's
current elements that exist in ELEMENTS are reordered (and
de-duplicated) to satisfy being AT-END or at the beginning."
  (declare (indent defun))
  (unless (boundp list-var)
    (set list-var '()))
  (-let* ((val0 (default-value list-var)))
    (set-default
     list-var
     (-->
      (-difference val0 elements)
      (if at-end
          (append it elements)
        (append elements it))))))

;;;###autoload
(defun my-delete-in-list! (list-var &rest elements)
  "In LIST-VAR, in-place delete each element which is `equal' to one of ELEMENTS."
  (declare (indent defun))
  (set list-var (-difference (symbol-value list-var) elements)))

;;;###autoload
(defun my-delete-in-default-list! (list-var &rest elements)
  "In LIST-VAR (global), in-place delete each element which is `equal' to one of ELEMENTS."
  (declare (indent defun))
  (set-default list-var (-difference (default-value list-var) elements)))

;;;###autoload
(defun my-get-alist (alist key &optional testfn default)
  "(`alist-get' KEY ALIST DEFAULT nil TESTFN)."
  (declare (pure t) (side-effect-free t))
  (-let* ((found (if testfn
                     (assoc key alist testfn)
                   (assoc key alist)))
          ((k . v) found))
    (if found v default)))

;;;###autoload
(cl-defun my-set-alist (alist key value &key testfn)
  "In ALIST, set KEY's value to VALUE, compare keys by TESTFN."
  (-let* ((testfn (or testfn #'equal)))
    ;; (--> (assoc-delete-all key alist testfn) (cons (cons key value) it))
    ;; manually loop to preserve the order, if exists
    (named-let
        recur ((remain alist) (replaced nil) (accu '()))
      (cond
       ((seq-empty-p remain)
        (-->
         (reverse accu)
         (if replaced
             it
           ;; prepend
           (cons (cons key value) it))))
       (:else
        (-let* ((cell0 (car remain))
                ((k . _v) cell0)
                (remain1 (cdr remain)))
          (cond
           ((not (funcall testfn key k))
            (recur remain1 replaced (cons cell0 accu)))
           (replaced
            (recur remain1 replaced accu))
           (:else ; not replaced
            (recur remain1 t (cons (cons key value) accu))))))))))

;;;###autoload
(cl-defun my-set-alist! (alist-symbol key value &key (testfn #'equal) global)
  (-let* ((new-val
           (my-set-alist
            (funcall (if global
                         #'default-value
                       #'symbol-value)
                     alist-symbol)
            key value
            :testfn testfn)))
    (funcall (if global
                 #'set-default
               #'set)
             alist-symbol new-val)))

;;;###autoload
(cl-defun my-delete-alist! (alist-symbol key &optional (testfn #'equal))
  (set alist-symbol
       (assoc-delete-all key (symbol-value alist-symbol) testfn)))

;;;###autoload
(defun my-get-command-remap-maybe (cmd &optional keymap/s)
  "Return the command which CMD is remapped to in KEYMAP/S or itself."
  (declare (side-effect-free t))
  (or (command-remapping cmd nil keymap/s) cmd))

;;;###autoload
(defun my-get-command-with-same-bindings (cmd &optional original-map)
  (declare (side-effect-free t))
  (let ((keymap (pcase original-map
                  (`all nil)
                  (`nil global-map)
                  (_ original-map))))
    (key-binding (kbd (key-description (where-is-internal cmd (list keymap) t))))))

;;;###autoload
(defun my-get-current-equivalence-command (cmd &optional original-map)
  (declare (side-effect-free t))
  (or (command-remapping cmd nil original-map)
      (my-get-command-with-same-bindings cmd original-map)))

;;;###autoload
(cl-defun my-get-all-buffers-in-frames
    (&optional (frames (frame-list)))
  (declare (side-effect-free t))
  (--> frames (-mapcat #'window-list it) (-map #'window-buffer it) (-uniq it)))

;;;###autoload
(defun my-log-to (buf &rest objs)
  "Log OBJS to BUF, use '%s' to format."
  (cl-assert
   (and (or (null buf) (stringp buf) (bufferp buf)) (<= 1 (length objs))))
  (-let* ((buf-name (or buf (format "*%s*" 'my-log-to))))
    (unless (get-buffer buf-name)
      (get-buffer-create buf-name))
    (with-current-buffer buf-name
      (goto-char (point-max))
      (dlet ((inhibit-read-only t))
        (dolist (str
                 (-->
                  objs
                  (-map (lambda (o) (format "%s" o)) it)
                  (-interpose " " it)))
          (insert str))
        (insert "\n"))))
  objs)

;;;###autoload
(defun my-log-to* (buf &rest objs)
  "Log OBJS to BUF, use '%S' to format."
  (apply #'my-log-to buf (-map #'my->Str objs))
  objs)

;;;###autoload
(defun my-log🌈 (&rest objs)
  (apply #'my-log-to (format "*%s*" 'my-log🌈) objs))

;;;###autoload
(defun my-log*🌈 (&rest objs)
  (apply #'my-log-to* (format "*%s*" 'my-log🌈) objs))

;;;###autoload
(defun my-concat-symbols (separator &rest symbols)
  "Return a symbol whose name is a concatenation of SYMBOLS's (strings or symbols).
With SEPARATOR inserted between each pair."
  (intern (mapconcat (-partial #'format "%s") symbols (format "%s" separator))))

;;;###autoload
(defun my-mode->hook (mode)
  "Convert MODE to it's corresponding hook."
  (intern (format "%s-hook" mode)))

;;;###autoload
(defun my-remove-symbol-suffix (symbol suffix)
  (intern (string-remove-suffix (format "%s" suffix)
                                (format "%s" symbol))))

;;;###autoload
(defun my-mode->map-symbol (mode)
  "Convert MODE to it's corresponding keymap (symbol)."
  (intern (format "%s-map" mode)))

;;;###autoload
(defun my-describe-keymap (keymap)
  (declare (side-effect-free t))
  (mapconcat (-lambda
               ((key . def))
               (-let* ((def*
                        (cond
                         ((and (listp def) (equal 'menu-item (car def)))
                          (nth 1 def))
                         (t
                          def))))
                 (format "%s %s"
                         (propertize (format "%s" key) 'face 'help-key-binding)
                         (propertize (format "%s" def*)
                                     'face
                                     'font-lock-function-name-face))))
             (my-keymap-bindings keymap)
             "  "))

;;;###autoload
(cl-defun my-set-face-decreasing-heights-by-level
    (min-lv max-lv fmt &optional (max-height 2.0))
  (dolist (lv (number-sequence min-lv max-lv))
    (let* ((face (intern (format fmt lv)))
           (inheritanted-from-face (face-attribute face :inherit))
           (target-height (1+ (/ (1- max-height) lv)))
           (height-to-set
            (if (string-match-p (format fmt (1- lv))
                                (symbol-name inheritanted-from-face))
                (* target-height
                   (/ 1.0 (face-attribute inheritanted-from-face :height nil t)))
              target-height)))
      ;; `my-custom-set-faces' may be a macro
      (my-lexical-eval
       `(my-custom-set-faces
          '(,face :height ,height-to-set
                  ;; `my-custom-set-faces' may not preserve inheritance
                  :inherit ,inheritanted-from-face))))))

;;;###autoload
(defun my-get-default-value-of-symbol (symbol)
  (my-lexical-eval (car (or (get symbol 'saved-value)
                            (get symbol 'standard-value)))))

(defconst my-clipboard-file-prefix "file://")

;;;###autoload
(defun my-file-list-to-clipboard-string (file-list)
  (declare (side-effect-free t))
  (mapconcat (-partial #'concat my-clipboard-file-prefix) file-list "\n"))

;;;###autoload
(defun my-clipboard-string-to-file-list (str)
  (declare (side-effect-free t))
  (if (string-match-p (concat "^" my-clipboard-file-prefix) str)
      (-map #'string-trim
            (split-string str
                          (concat "\\(\\n\\)?" my-clipboard-file-prefix)
                          'omit-nulls))
    nil))

;;;###autoload
(defun my-get-files-in-clipboard ()
  (declare (side-effect-free t))
  (-some #'my-clipboard-string-to-file-list kill-ring))

;;;###autoload
(defun my-filter-map-symbols (&optional filter-fn map-fn)
  (-let* ((filter-fn (or filter-fn #'always))
          (map-fn (or map-fn #'identity)))
    (cl-loop for sym being the symbols
             when (funcall filter-fn sym)
             collect (funcall map-fn sym))))

;;;###autoload
(defun my-find-keymap-symbol (keymap)
  (declare (side-effect-free t))
  (my-filter-map-symbols
   (lambda (kmap-sym)
     (let ((val (my-bounded-value kmap-sym)))
       (and (keymapp val)
            (equal val keymap))))))

;;;###autoload
(defun my-keymap-list ()
  (declare (side-effect-free t))
  (my-filter-map-symbols
   (lambda (sym) (keymapp (my-bounded-value sym)))
   #'identity))

;;;###autoload
(defun my-lookup-key-in-all-maps (key)
  (declare (side-effect-free t))
  (my-keep (lambda (kmap)
             (let ((cmd (lookup-key (my-bounded-value kmap)
                                    (my-ensure-kbd key))))
               (if (not (or (null cmd)
                            (numberp cmd)))
                   (cons kmap
                         (if (keymapp cmd)
                             (my-keymap-bindings cmd)
                           cmd))
                 nil)))
           (my-keymap-list)))

;;;###autoload
(cl-defun my-point-at-opening-bracket-p (&optional (pos (point)))
  "Check whether POS is before an opening bracket."
  (declare (side-effect-free t))
  ;; Pos is start of the innermost parenthetical grouping contains it's right
  (eq (nth 1 (save-excursion (syntax-ppss (1+ pos))))
      pos))

;;;###autoload
(cl-defun my-point-at-closing-bracket-p (&optional (pos (point)))
  "Check whether POS is before a closing bracket."
  (declare (side-effect-free t))
  ;; Pos' depth in parentheses is greater than it's right
  (> (nth 0 (save-excursion (syntax-ppss pos)))
     (nth 0 (save-excursion (syntax-ppss (1+ pos))))))

;;;###autoload
(defun my-point-after-parenthetical-group-beg-p ()
  "Check whether POS is immediately after an opening bracket."
  (declare (side-effect-free t))
  (let ((beg (nth 1 (syntax-ppss))))
    (and (integerp beg)
         (= (1+ beg)
            (point)))))

;;;###autoload
(defun my-point-before-parenthetical-group-end-p ()
  "Check whether POS is immediately before a closing bracket."
  (declare (side-effect-free t))
  (save-excursion
    (my-point-at-closing-bracket-p (1+ (point)))))

;;;###autoload
(cl-defun my-get-parent-modes (&optional (mode major-mode) result)
  "Get `MODE''s parent mode recursively up the top most mode.
Doesn't include MODE itself."
  (declare (side-effect-free t))
  (if mode
      (let ((parent-mode (get mode 'derived-mode-parent)))
        (my-get-parent-modes
         parent-mode
         (append result (and parent-mode (list parent-mode)))))
    result))

;;;###autoload
(defun my-parent-mode-p (parent child)
  "Is CHILD derived from PARENT (or is itself)?"
  (or (equal parent child)
      (provided-mode-derived-p child parent)))

;;;###autoload
(cl-defun my-mode-regexp-list (&optional (mode 'prog-mode) single dont-load)
  "Return a list of regular expressions, each is a file name extension of a mode derived from MODE.
MODE defaults to `prog-mode'.
Warning: take a long time to process.
With non-nil SINGLE, return a single regexp with all elements ORed.
With non-nil DONT-LOAD, don't `autoload-do-load' to detect unloaded modes."
  (declare (side-effect-free t))
  (let ((l (cl-loop for con in auto-mode-alist
                    for r = (car con)
                    for mmode = (cdr con)
                    when (not dont-load)
                    do (ignore-errors (autoload-do-load (symbol-function mmode)))
                    when (commandp mmode)
                    when (provided-mode-derived-p mmode mode)
                    collect r)))
    (if single
        (string-join l "\\|")
      l)))

;;;###autoload
(defun my-rgb-of-background ()
  (declare (pure t) (side-effect-free t))
  (color-name-to-rgb (face-attribute 'default :background)))

(defun my-keys-of-command (command &optional include-states)
  "Return list of keybindings as strings bound to COMMAND in current context.
With non-nil INCLUDE-STATES, show \"*-state>\" keys."
  (declare (side-effect-free t))
  (my-for [vect (where-is-internal command)
                :when (if (string-match-p "[a-z]+-state$" (format "%s" (elt vect 0)))
                          include-states
                        t)]
    (key-description vect)))

;;;###autoload
(defun my-keymap-bindings (keymap &optional keep-sub-keymaps)
  (declare (side-effect-free t))
  (my-for [(key . def) (my-raw-keymap-bindings keymap keep-sub-keymaps)]
    (cons (key-description key) def)))

;;;###autoload
(defun my-raw-keymap-bindings (keymap &optional keep-sub-keymaps)
  (declare (side-effect-free t))
  (-mapcat #'identity
           (my-raw-keymap-bindings--unspliced keymap keep-sub-keymaps)))

(defun my-raw-keymap-bindings--unspliced (keymap &optional keep-sub-keymaps)
  "-> (list (list (cons \"key\" \\'def)))"
  (declare (side-effect-free t))
  (my-returned-map
   #'map-keymap
   (lambda (key def)
     (cond
      ((and (keymapp def)
            (not keep-sub-keymaps))
       (-map
        (my-fn1 (key1 . def1)
          (cons (vconcat (vector key) key1)
                def1))
        (-mapcat #'identity
                 (my-raw-keymap-bindings--unspliced def keep-sub-keymaps))))
      (t
       (list
        (cons (vector key) def)))))
   (keymap-canonicalize keymap)))

;;;###autoload
(defun my-move-key (map old-key new-key)
  "In `MAP', unbind `OLD-KEY' (string) and optionally bind its old non-nil command to `NEW-KEY' (string)."
  (let ((command (lookup-key map (kbd old-key))))
    (when command
      (define-key map (kbd old-key) nil)
      (define-key map (kbd new-key) command))))

;;;###autoload
(defun my-string-insert-new-before-1st-space-or-end (str0 str1)
  "If STR0 contains STR1, return STR0, else return a new string
with STR1 inserted before the first space or the end."
  (declare (pure t) (side-effect-free t))
  (if (string-match-p (regexp-quote str1) str0)
      str0
    (replace-regexp-in-string (rx (group string-start (* (not space))))
                              (concat "\\1" str1)
                              str0)))

;;;###autoload
(defun my-up-sexp-beg-&-end-positions ()
  "Return list of the upper sexp's beginning and end positions."
  (declare (side-effect-free t))
  (ignore-errors
    (-let* ((upper-sexp-start (nth 1 (syntax-ppss))))
      (list upper-sexp-start (scan-sexps upper-sexp-start 1)))))

;;;###autoload
(defun my-next-sexp-beg-&-end-positions (num)
  "Return the NUM next sexps' beginning and end positions.
When NUM is negative, return previous sexp(s) instead."
  (declare (side-effect-free t))
  (list (point) (scan-sexps (point) num)))

;;;###autoload
(defun my-ignore-auth-sources-a (func &rest args)
  (dlet ((auth-sources nil))
    (apply func args)))

;;;###autoload
(defun my-message (&rest objects)
  "A variadic `message' on OBJECTS."
  ;; (apply #'my-println (-interpose " " objects))
  ;; `message' doesn't emit a new line in the echo area like `princ' does
  (apply #'message (string-join (-repeat (length objects) "%s") " ") objects)
  objects)

;;;###autoload
(defun my-message* (&rest objects)
  "Like (`my-message' @OBJECTS) but with `inhibit-message' when mini buffer is active."
  (dlet ((inhibit-message (active-minibuffer-window)))
    (apply #'my-message objects)))

(defun my-message🌈 (&rest objects)
  (apply #'my-message "🌈" objects))

;;;###autoload
(defun my-keep-messaging-until-next-command (&rest objects)
  (let ((timer-obj (apply #'run-with-timer 0 0.5
                          #'my-message objects)))
    (my-add-hook-once 'pre-command-hook
      (lambda () (cancel-timer timer-obj)))))

(defun my-lookup-key (key &optional keymap state)
  "`lookup-key' KEY (string or vector) in KEYMAP (`global-map' by
default), and optionally `evil' STATE."
  (declare (side-effect-free t))
  (let* ((keymap-to-look
          (cond ((null state)
                 (or keymap global-map))
                ((member keymap (list 'global global-map 'global-map nil))
                 (evil-state-property state :keymap t))
                (t
                 (evil-get-auxiliary-keymap keymap state t t))))
         (cmd (lookup-key keymap-to-look (my-ensure-kbd key))))
    (when (or (commandp cmd)
              (keymapp cmd))
      cmd)))

;;;###autoload
(defun my-copy-or-insert (str insert-flag)
  "If INSERT-FLAG, insert STR to the kill ring, else at point."
  (if insert-flag
      (insert str)
    (progn (kill-new str)
           (my-message str))))

(defun my-insert-and-copy (str)
  "Insert STR at point and copy it to the kill ring."
  (unless buffer-read-only (insert str))
  (kill-new str)
  (my-message str)
  str)

(defvar-local my-cached-minibuffer-completion-category t
  "Nil is a possible value too, so let's choose t as uninitialized.")
;;;###autoload
(defun my-minibuffer-completion-category (&optional refresh)
  ;; (run-hook-with-args-until-success 'marginalia-classifiers)
  (when (or (equal my-cached-minibuffer-completion-category t)
            refresh)
    (setq my-cached-minibuffer-completion-category
          ;; Currently there are some non-compliant commands such as `swiper'
          (condition-case _
              (my-get-minibuffer-completion-category)
            (error
             (bound-and-true-p current-minibuffer-command)))))
  my-cached-minibuffer-completion-category)
;;;###autoload
(defun my-get-minibuffer-completion-category ()
  (completion-metadata-get (my-completion-metadata) 'category))

;;;###autoload
(defun my-completion-metadata (&optional account-for-point)
  (completion-metadata
   (if account-for-point
       (buffer-substring-no-properties
        (minibuffer-prompt-end)
        (max (minibuffer-prompt-end) (point)))
     (minibuffer-contents))
   minibuffer-completion-table
   minibuffer-completion-predicate))

;;;###autoload
(defun my-minibuffer-candidates ()
  (my-with-deferred-gc
   (when (minibufferp)
     (let ((all (completion-all-completions
                 (minibuffer-contents)
                 minibuffer-completion-table
                 minibuffer-completion-predicate
                 (max 0 (- (point) (minibuffer-prompt-end))))))
       ;; The last cdr may be a number
       (my-loop [lst all retval nil]
         (cond ((or (numberp lst) (null lst))
                (nreverse retval))
               (t (recur (cdr lst) `(,(car lst) . ,retval)))))))))

(defvar my-string-face-list
  '(font-lock-string-face
    font-lock-doc-face
    font-lock-regexp-face
    font-lock-regexp-grouping-backslash
    font-lock-regexp-grouping-construct))

(defvar my-comment-face-list '(font-lock-comment-face font-lock-comment-delimiter-face))

;;;###autoload
(defun my-point-in-string-face-p (&optional str-faces)
  (-let* ((faces (text-properties-at (point)))
          (str-faces (or str-faces my-string-face-list)))
    (-intersection faces str-faces)))

;;;###autoload
(defun my-point-in-comment-face-p (&optional str-faces)
  (-let* ((faces (text-properties-at (point)))
          (str-faces (or str-faces my-comment-face-list)))
    (-intersection faces str-faces)))

;;;###autoload
(cl-defun my-point-in-string-p (&optional position parsed-syntax &key face)
  (declare (side-effect-free t))
  (let ((parsed-syntax (or parsed-syntax (syntax-ppss (or position (point))))))
    (and (nth 3 parsed-syntax)
         (or (not face)
             (my-point-in-string-face-p (and (proper-list-p face) face))))))

;;;###autoload
(cl-defun my-point-in-comment-p (&optional position parsed-syntax &key face)
  (declare (side-effect-free t))
  (let ((parsed-syntax (or parsed-syntax (syntax-ppss (or position (point))))))
    (and (nth 4 parsed-syntax)
         (or (not face)
             (my-point-in-comment-face-p (and (proper-list-p face) face))))))

;;;###autoload
(cl-defun my-point-in-string-or-comment-p (&optional position parsed-syntax &key face)
  "Check if text at POSITION is string or comment.
PARSED-SYNTAX: pre-computed `syntax-ppss'. FACE: also must consider faces."
  (declare (side-effect-free t))
  (let ((parsed-syntax (or parsed-syntax (syntax-ppss (or position (point))))))
    (or (my-point-in-string-p position parsed-syntax :face face)
        (my-point-in-comment-p position parsed-syntax :face face))))

;;;###autoload
(defun my-put-plist! (plist-sym &rest prop-val-lst)
  (if (boundp plist-sym)
      (cl-loop for (prop val) on prop-val-lst by #'cddr
               do (plist-put (symbol-value plist-sym) prop val))
    (set plist-sym prop-val-lst)))

;;;###autoload
(defun my-overwrite-file (text file)
  "Overwrite FILE's content with TEXT."
  (with-temp-file file
    (text insert)))

;;;###autoload
(defun my-notify-and-message (body &rest kvs)
  (apply #'notifications-notify :body body kvs)
  (message "%s" body))

;;;###autoload
(defun my-user-invisible-frame-selected-p ()
  (declare (side-effect-free t))
  (and (eq t (framep (selected-frame)))
       (equal "initial_terminal" (terminal-name))))

;;;###autoload
(defun my-autoload-functions (feature symbol-lst &optional docstring interactive &rest other-autoload-args)
  (declare (indent defun))
  (let ((file (format "%s" feature)))
    (dolist (sym symbol-lst)
      (apply #'autoload
             sym file docstring interactive
             other-autoload-args))))

;;;###autoload
(defun my-autoload-commands (feature command-list)
  (declare (indent defun))
  (let ((file (format "%s" feature)))
    (dolist (cmd command-list)
      (autoload cmd file nil t))))

;;;###autoload
(defun my-bounded-value (sym)
  (declare (side-effect-free t))
  (and (boundp sym)
       (symbol-value sym)))

;;;###autoload
(defun my-buffer-string (&optional buf with-properties trim-last-newline)
  (declare (side-effect-free t))
  (with-current-buffer (or buf (current-buffer))
    (-->
     (if with-properties
         (buffer-string)
       (buffer-substring-no-properties (point-min) (point-max)))
     (if trim-last-newline
         (string-trim-right it "\n")
       it))))

;;;###autoload
(defun my-replace-buffer-string (replacement &optional beg end)
  "Replace buffer's string from BEG to END with REPLACEMENT."
  (let ((beg (or beg (minibuffer-prompt-end) (point-min)))
        (end (or end (point-max)))
        (p0 (point)))
    ;; (replace-region-contents beg end (-const replacement)) ; undoing later
    ;; doesn't give the correct `point'
    (delete-region beg end)
    (goto-char beg)
    (prog1
        (insert replacement)
      (cond ((< p0 beg)
             (goto-char p0))
            ((< end p0)
             (goto-char (+ p0 (- (length replacement) (- end beg)))))))))

;;;###autoload
(defun my-used-active-region-bounds (&optional may-min-max)
  "(interactive \"r\") always gives non-nil values."
  (declare (side-effect-free t))
  (cond ((use-region-p) (list (region-beginning) (region-end)))
        (may-min-max (list (point-min) (point-max)))
        (t '(nil nil))))

;;;###autoload
(defun my-skip-chars-position (&optional string pos lim backward)
  "From POS (default to current `point'), (`skip-chars-forward' STRING LIM).
STRING default to spaces and newlines. With non-nil BACKWARD, use
`skip-chars-backward' instead."
  (let ((string (or string " \t\n\r")))
    (save-excursion
      (when pos (goto-char pos))
      (if backward
          (skip-chars-backward string lim)
        (skip-chars-forward string lim))
      (point))))

;;;###autoload
(defun my-update-hash (key value-fn table &optional default)
  "VALUE-FN receive the current value as its argument."
  (declare (indent defun))
  (let ((old-val (gethash key table default)))
    (puthash key
             (funcall value-fn old-val)
             table)))

(defvar-local my-eval-in-mode--cache nil)
;;;###autoload
(cl-defun my-eval-in-mode (mode expr &key setups new)
  "Return EXPR's valued called in major MODE.
SETUPS: list of initializing functions to call when creating buffer.
NEW: force a new evaluate buffer ground (normally they are reused per
MODE AND SETUPS). EXPR can be a function or an expression."
  (let ((buf (format " *%S %S %S*" 'my-eval-in-mode mode setups)))
    (when (and new (get-buffer buf))
      (kill-buffer buf))
    (unless (get-buffer buf)
      (get-buffer-create buf 'inhibit-buffer-hooks)
      (with-current-buffer buf
        (delay-mode-hooks
          (funcall mode))
        (dolist (setup-fn (or setups '()))
          (when (functionp setup-fn)
            (funcall setup-fn)))
        (setq my-eval-in-mode--cache (make-hash-table :test #'equal))))
    (with-current-buffer buf
      (with-memoization (gethash expr my-eval-in-mode--cache)
        (cond
         ((functionp expr)
          (funcall expr))
         (:else
          (eval expr t)))))))

;;;###autoload
(defun my-string-in-mode (str mode &optional setups new)
  "Return how STR would look in MODE.
Keep a cached buffer for re-usablity. MODE's hooks are ignore,
but SETUPS are called after initializing the
buffer. With non-nil NEW, kill the cache buffer first."
  (my-eval-in-mode
   mode
   (lambda ()
     (dlet ((inhibit-read-only t))
       (erase-buffer)
       (insert str)
       (ignore-errors
         (font-lock-ensure))
       (buffer-string)))
   :new new
   :setups setups))

;;;###autoload
(defun my-string-in-lisp-mode (str)
  "STR in `lisp' mode, with some additional prettiers."
  (my-string-in-mode str
                     'lisp-data-mode
                     '(rainbow-delimiters-mode highlight-quoted-mode)))

;;;###autoload
(defun my-buffer-string-no-properties (&optional beg end)
  (buffer-substring-no-properties (or beg (point-min))
                                  (or end (point-max))))

;;;###autoload
(defun my-stylize-string (str &rest properties)
  (add-face-text-property 0 (length str) properties nil str)
  str)

;;;###autoload
(defun my-random-string (len &optional character-pool)
  (let ((character-pool (or character-pool
                            (append (number-sequence ?a ?z)
                                    (number-sequence ?A ?Z)
                                    (number-sequence ?0 ?9)))))
    (apply #'string
           (my-for [_ (-iota len)]
             (seq-random-elt character-pool)))))

;;;###autoload
(defun my-temp-dir (&optional sub-dir)
  (--> (or (getenv "XDG_RUNTIME_DIR")
           (file-name-concat temporary-file-directory user-login-name))
       (if sub-dir
           (file-name-concat it sub-dir)
         it)
       (progn (make-directory it 'parents)
              it)))

;;;###autoload
(defun my-make-temp-filename (&optional prefix suffix)
  (let* ((temp-dir (my-temp-dir)))
    (cl-flet ((gen () (--> (concat prefix (format "%08x" (random (my**2 32)))
                                   suffix)
                           (file-name-concat temp-dir it))))
      (my-loop [file (gen)]
        (if (file-exists-p file)
            (recur (gen))
          file)))))

;;;###autoload
(defun my-write-text-to-temp-file (text &optional temp-file-prefix)
  "Write TEXT to a new temporary file and return the file name.
TEMP-FILE-PREFIX is passed to `make-temp-file'."
  (-let* ((tmp (make-temp-file (or temp-file-prefix ""))))
    (my-overwrite-file text tmp)
    tmp))

;;;###autoload
(defun my-read-text-file-lines (file)
  (--> (my-read-text-file file)
       (split-string it "\n")))

;;;###autoload
(defun my-search-regexp-matched-position (func regexp &rest args)
  (save-excursion
    (and (apply func regexp args)
         (point))))

;;;###autoload
(defun my-current-text-scale ()
  (expt text-scale-mode-step text-scale-mode-amount))

;;;###autoload
(defun my-relative-buffer-substring (bw fw &optional with-properties)
  (funcall (if with-properties #'buffer-substring #'buffer-substring-no-properties)
           (max (- (point) bw) (point-min))
           (min (+ (point) fw) (point-max))))

;;;###autoload
(defun my-self-insert-command (n &optional char)
  (self-insert-command n char))

;;;###autoload
(defun my-emulate-self-insert-command (n char)
  "`self-insert-command' N CHAR"
  ;; (funcall-interactively #'self-insert-command n char)
  (setq last-command-event char)
  (setq this-command #'self-insert-command)
  (my-self-insert-command n char))

;;;###autoload
(cl-defun my-minibuffer-annotation-align-affixation (lst
                                                     &key
                                                     pre-left-alignment
                                                     (compl-right 4)
                                                     (compl-col 8)
                                                     (pre-right 4)
                                                     (pre-col 8))
  "TODO: Info node `(elisp) Specified Space', example usage: `marginalia--align'."
  (and (length> lst 0)
       (let* ((max-compl-len (-max (-map (-compose #'length #'car) lst)))
              (max-pre-len (-max (-map (-compose #'length #'cadr) lst))))
         (my-for
           [(compl pre suf) lst]
           (list
            (-->
             (s-pad-right (my-min->=-multiple max-compl-len compl-col) " " compl)
             (concat it (s-repeat compl-right " ")))
            (and pre
                 (-->
                  (funcall (if pre-left-alignment
                               #'s-pad-right
                             #'s-pad-left)
                           (my-min->=-multiple max-pre-len pre-col) " " pre)
                  (concat it (s-repeat pre-right " "))))
            suf)))))

;;;###autoload
(defun my-imenu->outline-regexp ()
  (--> (my-for [(_ re . _) imenu-generic-expression]
         (string-remove-prefix "^" re))
       (my-regexp-or it)))

;;;###autoload
(defun my-enabled-mode-p (mode)
  "Check if MODE is enabled, it can either be a major or minor one."
  (or (derived-mode-p mode) (and (boundp mode) (symbol-value mode))))

;;;###autoload
(defun my-thing-at-point-dwim (&optional with-properties)
  (declare (side-effect-free t))
  (cond
   ((my-region-string-maybe (not with-properties) 'trim))
   (t
    (or (thing-at-point 'symbol (not with-properties))
        (thing-at-point 'word (not with-properties))))))

;;;###autoload
(defun my-region-string-maybe (&optional no-properties trim)
  (declare (side-effect-free t))
  (and (use-region-p)
       (--> (funcall (if no-properties
                         #'buffer-substring-no-properties
                       #'buffer-substring)
                     (region-beginning)
                     (region-end))
            (if trim
                (string-trim it)
              it))))

;;;###autoload
(defun my-dispatch-cmds (&rest cond-body)
  "Return an extended menu to dispatch command base of COND-BODY.
COND-BODY is used for `cond', it may be better if the conditions
and commands are not tied together, but it need extra complexity
to solve the double quote that is needed before command names.

Usage:
(my-dispatch-cmds
    '(cond1 cmd1)
    '(cond2 cmd2)
).

See Info node `(elisp) Extended Menu Items'."
  `(menu-item
    "" nil
    :filter
    (lambda (&optional _)
      (cond
       ,@cond-body))))

;;;###autoload
(defun my-get-file-major-modes (&optional trim)
  (--> (append (-->
                auto-mode-alist
                (-map 'cdr it)
                -uniq
                (-filter (-andfn #'commandp #'symbolp) it))
               (cl-loop for symbol being the symbols
                        for name = (symbol-name symbol)
                        when (and (string-match-p "-ts-mode$" name)
                                  (not (string-search "--" name)))
                        collect symbol))
       (if trim
           (-map
            (lambda (mode)
              (--> (symbol-name mode) (string-remove-suffix "-mode" it) intern))
            it)
         it)))

;;;###autoload
(defun my-read-file-major-mode (&optional default)
  (-let* ((collection `(,@(my-get-file-major-modes) lisp-interaction-mode)))
    (-->
     (completing-read "my-read-file-major-mode : "
                      (lambda (string pred action)
                        (if (eq action 'metadata)
                            `(metadata (category . command))
                          (complete-with-action action collection string pred)))
                      nil nil nil nil (and default (format "%s" default)))
     intern)))

;;;###autoload
(defun my-get-shebang-line ()
  (save-excursion
    (goto-char (point-min))
    (buffer-substring-no-properties (point)
                                    (line-end-position))))

;;;###autoload
(defun my-shebang-p (&optional interpreter)
  (if interpreter
      (save-excursion
        (goto-char (point-min))
        (looking-at (format "^#!/.*[/ ]%s\\( \\|$\\)" interpreter)))
    (--> (point-min)
         (equal (buffer-substring it (min (+ 2 it) (point-max)))
                "#!"))))

;;;###autoload
(defun my-window-or-frames->windows (&optional wd|fr)
  (cond ((null wd|fr)
         (list (selected-window)))
        ((windowp wd|fr)
         (list wd|fr))
        ((framep wd|fr)
         (window-list wd|fr))))

;;;###autoload
(defun my-hi-lock-get-default-face-cycled (&optional idx)
  (require 'hi-lock)
  (my-at hi-lock-face-defaults
         (or idx 0)))

;;;###autoload
(defun my-require-provided
    (feat file-name &optional noerror always str func)
  "Like (`require' FEAT FILE-NAME NOERROR).
When FILE-NAME doesn't provide FEAT, add a `provide' clause to
it. ALWAYS: always `load' without checking if FEAT is in
`features' first. STR is added and FUNC is called before adding
`provide' with that file opened."
  (when (or always (not (member feat features)))
    (load file-name noerror 'quiet))
  (unless (member feat features)
    (with-current-buffer (find-file-noselect file-name)
      (goto-char (point-max))
      (when str
        (insert str))
      (goto-char (point-max))
      (when func
        (insert "\n")
        (funcall func))
      (goto-char (point-max))
      (insert (format "\n%s\n" `(provide ',feat)))
      (add-file-local-variable-prop-line 'lexical-binding t)
      (save-buffer 0))
    (require feat file-name noerror)))

;;;###autoload
(defun my-looking-back-p (regexp &rest args)
  (save-match-data (apply #'looking-back regexp args)))

;;;###autoload
(defun my-point-as-marker-at (pos)
  (save-excursion
    (goto-char pos)
    (point-marker)))

;;;###autoload
(cl-defun my-intern-unique-name-symbol (name-str pred &optional suffix-format)
  (let* ((counter 0)
         (suffix-format (or suffix-format "-%d"))
         (retval (intern name-str)))
    (while (funcall pred retval)
      (setq retval (intern (concat name-str (format suffix-format counter))))
      (cl-incf counter))
    retval))

;;;###autoload
(defun my-directory-not-exist-or-empty-p (dir)
  (or (not (file-exists-p dir)) (directory-empty-p dir)))

;;;###autoload
(cl-defun my-multisession (symbol &rest args &key set make synchronized)
  "Safe wrapper around (`multisession-value' SYMBOL).
SET: set the value to this (including explicit nil when `:set' is
provided). MAKE: `make-multisession' arguments. If `multisession.el' is
unavailable, there's little mechanic to mutate the value during this
Emacs session only, but it's not persisted. SYNCHRONIZED: see
`make-multisession'. ARGS variadic."
  (require 'multisession nil t)
  (-let* ((kwargs-keys (-slice args 0 nil 2)))
    (cond
     ((not (fboundp #'define-multisession-variable))
      (when (or (not (boundp symbol))
                (null (symbol-value symbol))
                (member :set kwargs-keys))
        (set-default symbol `((,symbol . ,set))))
      (alist-get symbol (default-value symbol) nil nil #'equal))
     (:else
      (-let* ((name (symbol-name symbol)))
        (when (or (not (boundp symbol)) (null (default-value symbol)))
          (when (null (plist-get make :package))
            (setq make
                  (plist-put
                   make
                   :package (replace-regexp-in-string "-.*" "" name))))
          (set-default
           symbol
           (apply #'make-multisession
                  :key name
                  :initial-value set
                  :synchronized
                  synchronized
                  make)))
        (-let* ((mtss-obj (default-value symbol)))
          (when (member :set kwargs-keys)
            (setf (multisession-value mtss-obj) set))
          (multisession-value mtss-obj)))))))

;;;###autoload
(defun my-regexp-replace-in-buffer
    (old-regexp
     new
     &optional
     beg
     end
     count
     replace-match-optional-args)
  "Replace all OLD-REGEXP matches with NEW.
Begin at BEG (default: buffer beginning), end at END (default: buffer
end).  COUNT: see `re-search-forward'.  REPLACE-MATCH-OPTIONAL-ARGS:
`replace-match''s arguments."
  (save-match-data
    (-let* ((beg (or beg (point-min)))
            (bound
             (cond
              (end
               (my-point-as-marker-at end))
              (:else
               nil))))
      (save-excursion
        (goto-char beg)
        (while (re-search-forward old-regexp bound t count)
          (apply #'replace-match new replace-match-optional-args))))))

(defvar my-greek-letters
  '(("alpha" "α" "Α")
    ("beta" "β" "Β")
    ("chi" "χ" "Χ")
    ("dei" "ϯ" "Ϯ")
    ("delta" "δ" "Δ")
    ("digamma" "ϝ" "Ϝ")
    ("epsilon" "ε" "Ε")
    ("eta" "η" "Η")
    ("fei" "ϥ" "Ϥ")
    ("gamma" "γ" "Γ")
    ("gangia" "ϫ" "Ϫ")
    ("heta" "ͱ" "Ͱ")
    ("hori" "ϩ" "Ϩ")
    ("iota" "ι" "Ι")
    ("kappa" "κ" "Κ")
    ("khei" "ϧ" "Ϧ")
    ("koppa" "ϟ" "Ϟ")
    ("lambda" "λ" "Λ")
    ("mu" "μ" "Μ")
    ("nu" "ν" "Ν")
    ("omega" "ω" "Ω")
    ("omicron" "ο" "Ο")
    ("phi" "φ" "Φ")
    ("pi" "π" "Π")
    ("psi" "ψ" "Ψ")
    ("rho" "ρ" "Ρ")
    ("sampi" "ϡ" "Ϡ")
    ("san" "ϻ" "Ϻ")
    ("shei" "ϣ" "Ϣ")
    ("shima" "ϭ" "Ϭ")
    ("sho" "ϸ" "Ϸ")
    ("sigma" "σ" "Σ")
    ("stigma" "ϛ" "Ϛ")
    ("tau" "τ" "Τ")
    ("theta" "θ" "Θ")
    ("upsilon" "υ" "Υ")
    ("xi" "ξ" "Ξ")
    ("zeta" "ζ" "Ζ")))

;;;###autoload
(defun my-hook-local-global-value (hook-sym)
  "Return HOOK-SYM's local and global elements as a single list.
If HOOK-SYM is not local, return its value. If it's local, return its
value with t elements with the appropriate global elements."
  (cond
   ((not (local-variable-p hook-sym))
    (symbol-value hook-sym))
   (:else
    (-mapcat
     (lambda (elem)
       (cond
        ((equal t elem)
         (default-value hook-sym))
        (:else
         (list elem))))
     (buffer-local-value hook-sym (current-buffer))))))

;;;###autoload
(defun my-keymap-local-set* (key-command-list)
  "(`keymap-local-set' KEY COMMAND) but use a locally copied keymap without affecting other buffers."
  (let* ((kmap (copy-keymap (current-local-map))))
    (use-local-map kmap)
    (mapcar
     (lambda (kc)
       (cl-destructuring-bind (k c) kc (keymap-local-set k c)))
     key-command-list)))    

;;; General advices

;;;###autoload
(defun my-demote-errors-from (func)
  "Demote FUNC's errors."
  (interactive (list
                (my-completing-read-symbol
                 "my-demote-errors-from : " nil 'functionp)))
  (advice-add func :around #'my-with-demoted-errors-a))

;;;###autoload
(defun my-with-demoted-errors-a (func &rest args)
  (my-with-demoted-errors-no-echo
   (apply func args)))

;;;###autoload
(defun my-with-deferred-gc-around-a (func &rest args)
  (my-with-deferred-gc
   (apply func args)))

;;;###autoload
(defun my-function-with-calls-to-replaced (outer-func inner-func replacer)
  "Return OUTER-FUNC with calls to INNER-FUNC replaced by REPLACER."
  (let ((f (my-concat-symbols '@ 'my-function-with-calls-to-replaced
                              outer-func inner-func replacer)))
    (defalias f
      (lambda (&rest args)
        (cl-letf (((symbol-function inner-func) replacer))
          (apply outer-func args))))
    (put f 'interactive-form (interactive-form outer-func))
    f))

;;;###autoload
(defun my-make-wrapped-function (wrapper func)
  "Return a function where FUNC is called as WRAPPER's body.
Both must be named symbols."
  (let ((rf (my-concat-symbols '@ 'my #'my-make-wrapped-function wrapper func)))
    (defalias rf
      `(lambda (&rest args)
         (interactive)
         (,wrapper
          (apply #',func args))))
    rf))

(defvar my-benchmark-accumulate-time--num-time '(0 0))
;;;###autoload
(defun my-benchmark-accumulate-time-a (func &rest args)
  (-let* ((time0 (current-time)))
    (unwind-protect
        (apply func args)
      (-let* ((delta (float-time (time-since time0)))
              ((num total-time) my-benchmark-accumulate-time--num-time))
        (setq my-benchmark-accumulate-time--num-time
              (list (+ (or num 0) 1)
                    (+ (or total-time 0) delta)))))))
(defun my-benchmark-accumulate-time-report (&optional reset)
  (pcase-let* ((`(,num ,total-time) my-benchmark-accumulate-time--num-time))
    (when reset (setq my-benchmark-accumulate-time--num-time nil))
    (list :num num
          :total-time total-time
          :average (ignore-error arith-error (/ total-time num)))))

;;;###autoload
(defun my-memoize-local-a (func &rest args)
  "(`my-memoize' :local t FUNC @ARGS)."
  (apply #'my-memoize :local t func args))

;;;###autoload
(defun my-memoize-non-nil-a (func &rest args)
  "(`my-memoize' :nnil t FUNC @ARGS)."
  (apply #'my-memoize :nnil t func args))

(defvar my-memoize--table nil)
(defvar-local my-memoize--local-table nil)
;;;###autoload
(cl-defun my-memoize (&rest all-args &key local nnil &allow-other-keys)
  "(`apply' FUNC ARGS) with FUNC's return values memoized.
FUNC and ARGS are found from the trailing non-plist part of
ALL-ARGS. FUNC must be pure for correctness (unless maybe LOCAL).

LOCAL: only store cache locally in this buffer.

NNIL: only cache non-nil return values, therefore retrieving
cached non-nil values should be a little bit faster at expense of
not storing nil return values, but that is useful in case non-nil
return values are fixed but nil ones mean yet-to-find.

Can also act as an advice:
\(`advice-add' ... :around #'`my-memoize')."
  (-let* ((table (if local
                     (my-ensure-hash-table 'my-memoize--local-table)
                   (my-ensure-hash-table 'my-memoize--table)))
          ((_plist (func . args)) (my-split-leading-plist all-args)))
    (-if-let* ((key (vector func args))
               (cached-val (if nnil
                               (gethash key table)
                             (-when-let* ((hashed (my-assoc-hash key table))
                                          ((_cached-key . cached-val) hashed))
                               cached-val))))
        cached-val
      (-let* ((val (apply func args)))
        (puthash key val table)
        val))))

;;;###autoload
(defun my-memoize-clean ()
  (interactive)
  (setq-default my-memoize--table nil)
  (setq-default my-memoize-last-args--table nil)
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (setq-local my-memoize--local-table nil))))

(defvar my-memoize-last-args--table nil)
;;;###autoload
(defun my-memoize-last-args (func &rest args)
  (my-ensure-hash-table 'my-memoize-last-args--table)
  (-let* ((key func)
          (hashed (gethash key my-memoize-last-args--table))
          ((cached-args . cached-val) hashed))
    (if (equal cached-args args)
        cached-val
      (-let* ((val (apply func args)))
        (puthash key
                 (cons args val)
                 my-memoize-last-args--table)
        val))))


;;; 01-my-core-functions.el ends here

(provide '01-my-core-functions)

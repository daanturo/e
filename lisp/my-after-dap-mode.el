;; -*- lexical-binding: t; -*-

(require 'dap-mode)

;; (my-backtrace)

;; (my-bind :map 'prog-mode-map "C-S-y" #'dap-ui-repl)

;; Fix truncated breakpoints with `git-gutter'
(add-hook 'dap-breakpoints-changed-hook #'set-fringe-style)

;; (eval-when-compile (require 'dap-hydra nil 'noerror))
(with-eval-after-load 'dap-hydra
  (eval `(defhydra+
           dap-hydra ()
           (,my-execu-confirm-key my-execu-dwim "Execute/Evaluate" :exit t))
        t))

(provide 'my-after-dap-mode)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require '00-my-core-macros)
(require 'dash)


;;;###autoload
(defun my-find-file-toggle-recursion-maybe ()
  "Select and jump to a file below current `find-file' input, if possible.
Else just `self-insert-command'."
  (interactive)
  (let* ((input (minibuffer-contents-no-properties))
         (cur-file (file-name-concat default-directory input))
         (category (my-minibuffer-completion-category)))
    (cond
     ;; normal -> recursive
     ((and (member category '(file))
           ;; `read-file-name'?
           (equal (syntax-table) minibuffer-local-filename-syntax)
           (equal (char-before) ?/)
           (not (string-empty-p input)))
      (my-quit-minibuffer-and-run
       (my-findutil-find-file-recursively input)))
     ;; recursive -> normal
     ((and (member category '(file project-file))
           (file-directory-p cur-file))
      (my-quit-minibuffer-and-run
       (dlet ((default-directory (file-name-as-directory cur-file)))
         (call-interactively #'find-file))))
     (t
      (call-interactively #'self-insert-command)))))

;;;###autoload
(defun my-find-file-toggle-recursion ()
  (interactive)
  (let* ((input (minibuffer-contents-no-properties))
         (input-dir (file-name-directory input))
         (input-file (string-remove-prefix (or input-dir "") input))
         (category (my-minibuffer-completion-category))
         (current-dir default-directory))
    ;; TODO: consider that the action isn't always `find-file'
    (cond
     ;; normal -> recursive
     ((and (member category '(file))
           ;; `read-file-name'?
           (equal (syntax-table) minibuffer-local-filename-syntax)
           (not (string-empty-p input)))
      (my-quit-minibuffer-and-run
       (my-findutil-fallback-to-async input-dir :initial-input input-file)))
     ;; recursive -> normal
     ((and (member category '(file project-file)))
      (my-quit-minibuffer-and-run
       (dlet ((default-directory current-dir))
         (minibuffer-with-setup-hook (lambda () (insert input-file))
           (call-interactively #'find-file)))))
     (t
      (call-interactively #'self-insert-command)))))

;;;###autoload
(defun my-indent-buffer (&optional buf)
  (interactive)
  (my-with-current-buffer buf
    (indent-region (point-min) (point-max))))

(provide '16-my-commands-compiled)

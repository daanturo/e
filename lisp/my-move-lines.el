;; -*- lexical-binding: t; -*-

;;; Commentary:

;; Known problems with existing packages:

;; https://github.com/rejeep/drag-stuff.el : doesn't do anything when distance
;; exceeds; like dragging a line up with 999 passed as prefix argument

;; https://github.com/emacsfodder/move-text : only move to the next-to-top
;; line when distance exceeds, also it use repeat instead of one-time
;; computation so performance may not be great

;; https://github.com/wyuenho/move-dup : doesn't keep the region

;;; Code:

(require 'dash)
(require '00-my-core-macros)

;; NOTE: - In block visual type, `use-region-p' returns nil - In line visual
;; type, when in the middle of the line, `evil-visual-mark' and
;; `evil-visual-point' may have the same position (like initially)

(defun my-move-lines--move (line-distance point-0 &optional active-region-mark)
  (-let* ((mark-0* (or active-region-mark point-0))
          (vi-visual-type (and (equal 'visual evil-state)
                               (evil-visual-type)))
          (evil-point-0 (if vi-visual-type (marker-position evil-visual-point) point-0))
          (evil-mark-0 (if vi-visual-type (marker-position evil-visual-mark) mark-0*))
          (reg-beg (min point-0 mark-0* evil-point-0 evil-mark-0))
          (reg-end (max point-0 mark-0* evil-point-0 evil-mark-0))
          ;; (beg-line-num (line-number-at-pos reg-beg))
          (move-beg-pos (save-excursion
                          (goto-char reg-beg)
                          (line-beginning-position)))
          ;; We want to grab the final newline as it must be moved along, so
          ;; extend such that region to move's end is at the beginning of a line
          (move-end-pos (save-excursion
                          (goto-char reg-end)
                          (if (and active-region-mark (bolp))
                              ;; Typical case: mark paragraph, in that case the
                              ;; mark is at a blank line and therefore newline
                              ;; is included, we don't want to extend anymore
                              reg-end
                            (min (1+ (line-end-position))
                                 (point-max)))))
          (str (--> (buffer-substring-no-properties move-beg-pos move-end-pos)
                    ;; Normalize the line at the end of buffer
                    (my-ensure-string-suffix "\n" it))))
    ;; Delete those lines, including the newline
    (delete-region move-beg-pos move-end-pos)
    (forward-line line-distance)
    ;; End of buffer but not at a new line after the above?
    (unless (bolp)
      (insert "\n"))
    (-let* ((new-line-beg-pos (point))
            (adjust (- new-line-beg-pos move-beg-pos))
            (new-mark (+ adjust mark-0*))
            (new-point (+ adjust point-0)))
      ;; Restore text
      (insert str)
      ;; Restore the point-0 position and the marked region (if any) relatively
      (goto-char new-point)
      (when active-region-mark
        (push-mark new-mark 'nomsg t))
      (when vi-visual-type
        (evil-visual-make-selection (+ adjust evil-mark-0)
                                    (+ adjust evil-point-0)
                                    vi-visual-type))))
  ;; Needed when the region isn't active after `push-mark'
  (setq deactivate-mark nil))

;;;###autoload
(defun my-move-lines-up (arg)
  "Move up the minimal number of lines that cover the region or point ARG lines.
When the distance to move exceeds the buffer or ARG is a
universal argument, move those lines to the beginning of the
buffer."
  (interactive "P")
  (-let* ((n (prefix-numeric-value arg)))
    (my-move-lines--move (- (if (car-safe arg)
                                ;; universal arg: move to the beginning of
                                ;; buffer, ideally we should provide the correct
                                ;; line distance, but here just a number that is
                                ;; surely bigger than that without further
                                ;; computation is sufficient
                                (point-max)
                              n))
                         (point)
                         (and (use-region-p) (mark)))))

;;;###autoload
(defun my-move-lines-down (arg)
  "`my-move-lines-up' - ARG."
  (interactive "P")
  (-let* ((n (prefix-numeric-value arg)))
    (my-move-lines--move (if (car-safe arg)
                             (point-max)
                           n)
                         (point)
                         (and (use-region-p) (mark)))))

(provide 'my-move-lines)

;;; my-move-lines.el ends here

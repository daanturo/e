;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-colorful-add-conf-decimal-rgb-colors ()
  "Fontify \"Color=255,255,255\"."
  (-let* ((font-lock-kw
           `(,(rx
               "="
               (group
                (group (+ num))
                (or (seq "," (* space)) (+ space))
                (group (+ num))
                (or (seq "," (* space)) (+ space))
                (group (+ num)))
               (or (not num) eol))
             (1 (-let* ((colors
                         (-->
                          (list
                           (match-string 2) (match-string 3) (match-string 4))
                          (-map #'string-to-number it)
                          (-map (lambda (num) (/ num (float #xFF))) it))))
                  (colorful--colorize-match
                   (color-rgb-to-hex
                    (nth 0 colors) (nth 1 colors) (nth 2 colors) 2)
                   (match-beginning 1) (match-end 1)))))))
    (cl-pushnew font-lock-kw colorful-color-keywords)))


;;; my-functions-color.el ends here

(provide 'my-functions-color)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)

;; Too annoying since magit is designed for interactive uses!

;; `magit-save-repository-buffers' set to nil as dir-local, still get asked

;;;###autoload
(define-minor-mode my-autosync-magit-global-disable-mode
  nil
  :global
  t
  (cond
   (my-autosync-magit-global-disable-mode
    (advice-add #'autosync-magit-pull :override #'ignore)
    (advice-add #'autosync-magit-push :override #'ignore))
   (:else
    (advice-remove #'autosync-magit-pull #'ignore)
    (advice-remove #'autosync-magit-push #'ignore))))

(advice-add #'autosync-magit-pull :around #'my-magit-with-noninteractive-a)
(advice-add #'autosync-magit-push :around #'my-magit-with-noninteractive-a)

(add-hook
 'my-magit-auto-display-process-buffer-inhibit-functions
 (lambda () autosync-magit-mode))

(advice-add #'autosync-magit-pull :around #'my-ssh-with-batch-mode-a)
(advice-add #'autosync-magit-pull :around #'my-with-inhibit-message-a)

(add-hook 'autosync-magit-mode-hook #'my-magit-never-save-repository-buffers-h)

;; Not using real-time collaboration
(setq autosync-magit-push-debounce 60)

;;; my-after-autosync-magit.el ends here

(provide 'my-after-autosync-magit)

;; Local Variables:
;; no-byte-compile: t
;; End:

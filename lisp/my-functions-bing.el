;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defun my-bing-chat-save-buffer ()
  (interactive)
  (with-current-buffer (get-file-buffer aichat-bingai-chat-file)
    (call-interactively #'save-buffer)))

;;;###autoload
(defun my-bing-search-command-buffer ()
  (interactive)
  (-let* ((buf
           (my-ui-make-command-buffer
            'my-bing-search-command-buffer
            ;; format: off
            (list
             "c" #'aichat-bingai-chat
             "a" #'aichat-bingai-assistant
             "s" #'my-bing-chat-save-buffer
             ;; format: on
             ))))

    (switch-to-buffer buf)

    (my-auto-resize-window-mode)

    ;;
    ))

;;; my-functions-bing.el ends here

(provide 'my-functions-bing)

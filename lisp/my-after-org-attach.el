;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(defun my-org-attach-id-flat-folder-format (_id &rest _)
  (declare)
  "")

(defun my-org-attach-flatten-id-dir (&optional local)
  (cond
   (local
    (setq-local org-attach-preferred-new-method 'dir)
    (setq-local org-attach-id-to-path-function-list
                (delete-dups
                 `((lambda (&rest _) "")
                   ,@org-attach-id-to-path-function-list))))
   (:else
    (setq-default org-attach-preferred-new-method 'dir)
    (setq-default org-attach-id-to-path-function-list
                  (delete-dups
                   `((lambda (&rest _) "")
                     ,@(default-value 'org-attach-id-to-path-function-list)))))))

;; dangerous?
(my-org-attach-flatten-id-dir)

;; Don't expose full local paths, remember to set this locally for files/project-wide
(setq-default org-attach-dir-relative t)

;;; my-after-org-attach.el ends here

(provide 'my-after-org-attach)

;; Local Variables:
;; no-byte-compile: t
;; End:

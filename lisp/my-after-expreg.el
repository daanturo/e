;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-expreg-before-first-expand-in-buffer-hook '()
  "Can be used to populate `expreg-functions' lazily.")
(defun my-expreg-expand--before-a (&rest _)
  (run-hooks 'my-expreg-before-first-expand-in-buffer-hook)
  (setq-local my-expreg-before-first-expand-in-buffer-hook '()))
(advice-add #'expreg-expand :before #'my-expreg-expand--before-a)

(add-hook
 'my-expreg-before-first-expand-in-buffer-hook
 (lambda ()
   (when (derived-mode-p 'text-mode)
     (add-hook 'expreg-functions #'expreg--sentence nil 'local))))

;; seems like `expreg--list' doesn't handle lists's inner bounds and lists in
;; comments

(setq-default expreg-functions
              (delete-dups
               (append
                (default-value 'expreg-functions) '(my-expreg--identifier*)
                (and (fboundp #'puni-mode)
                     '(my-expreg--lists
                       ;; my-expreg--puni
                       )))))

;;; after-expreg.el ends here

(provide 'my-after-expreg)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defun my-edit-indirect-unescape-double-quotes-in-buffer-h ()
  (message "my-edit-indirect-unescape-double-quotes-in-buffer-h")
  (save-excursion
    (-let* ((str
             ;; doesn't include double quotes when copying, so add before
             ;; converting, remember to strip after converting back
             (concat
              "\""
              (buffer-substring-no-properties (point-min) (point-max))
              "\"")))
      (delete-region (point-min) (point-max))
      (insert (my-Str->str str)))))
(defun my-edit-indirect-escape-double-quotes-in-buffer-h ()
  (when (string-match-p "\\<edit-indirect-commit\\>" (symbol-name this-command))
    (message "my-edit-indirect-escape-double-quotes-in-buffer-h")
    (save-excursion
      (-let* ((str (buffer-substring-no-properties (point-min) (point-max))))
        (delete-region (point-min) (point-max))
        (insert
         (-->
          (prin1-to-string str)
          ;; see comment above
          (substring it 1 -1)))))))

(defvar-local my-edit-indirect--final-newline-originally nil)

;; Unless the original string has trailing newline, strip the newline at the
;; end.
(defun my-edit-indirect-handle-final-newline ()
  (when (and (not my-edit-indirect--final-newline-originally)
             (member (char-to-string (char-before (point-max))) '("\n")))
    (save-excursion
      (goto-char (point-max))
      (delete-char -1))))

(defun my-edit-indirect-region-or-string-at-point--setup ()
  (my-edit-region-or-string-at-point--setup-keys)
  ;; transform escaped double quotes to edit normally and re-escape them later
  (my-edit-indirect-unescape-double-quotes-in-buffer-h)
  (add-hook 'edit-indirect-before-commit-hook
            #'my-edit-indirect-escape-double-quotes-in-buffer-h
            nil
            'local)
  (add-hook
   'edit-indirect-before-commit-hook #'my-edit-indirect-handle-final-newline
   nil 'local))

(defvar my-edit-indirect-mode-table (make-hash-table :test #'equal))
(defvar my-edit-indirect-get-major-mode-force-ask nil)
(defun my-edit-indirect-get-major-mode (parent-buf beg end)
  (-let* ((dir (or (my-project-root) default-directory))
          (cached (gethash dir my-edit-indirect-mode-table))
          (mode
           (if (or my-edit-indirect-get-major-mode-force-ask (not cached))
               (puthash
                dir (my-read-file-major-mode) my-edit-indirect-mode-table)
             cached)))
    (prog1 (funcall mode)
      (setq my-edit-indirect--final-newline-originally
            (with-current-buffer parent-buf
              (member (char-to-string (char-before end)) '("\n"))))
      (my-edit-indirect-region-or-string-at-point--setup))))

;;;###autoload
(defun my-edit-indirect-region-or-string-at-point ()
  (interactive)
  (-if-let* (((beg . end) (if (use-region-p)
                              (cons (region-beginning) (region-end))
                            (my-bounds-of-inner-string))))
      (edit-indirect-region beg end 'display-buffer)
    (user-error "`my-edit-indirect-region-or-string-at-point': no string at point!")))

;;;###autoload
(defun my-edit-region-or-string-at-point-dwim (&optional force-ask)
  "Edit region or string at point using an to be asked major mode.
With non-nil FORCE-ASK, always ask the major mode, else use the
project's previous answer."
  (interactive "P")
  (dlet ((my-edit-indirect-get-major-mode-force-ask force-ask)
         (edit-indirect-guess-mode-function #'my-edit-indirect-get-major-mode))
    (save-excursion (my-edit-indirect-region-or-string-at-point))))

;;;###autoload
(defun my-edit-indirect-commit ()
  (interactive)
  (prog1 (edit-indirect-commit)
    (my-delete-window-when-duplicated-buffer)))

;;;###autoload
(defun my-edit-region-or-string-at-point--setup-keys ()
  ;; Saving is error-prone, it may duplicate the region
  (local-set-key [remap edit-indirect-save] #'my-dummy-disabled-command)
  (local-set-key [remap edit-indirect-commit] #'my-edit-indirect-commit))

(provide 'my-functions-edit-indirect)

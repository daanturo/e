;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defvar url-http-end-of-headers nil)

;;;###autoload
(defun my-last-url-in-buffer (&optional buffer noerror)
  (with-current-buffer (or buffer (current-buffer))
    (save-excursion
      (goto-char (point-max))
      (when (re-search-backward "https?://[^ \t\n\r]+" nil noerror)
        (match-string-no-properties 0)))))

;;;###autoload
(defun my-web-search-prefix ()
  (require 'eww)
  (string-replace "/html/?q=" "/?q=" eww-search-prefix))

;;;###autoload
(defun my-search-online-by-web-browser (query)
  (interactive (list (read-string "Search on the web: " (my-thing-at-point-dwim))))
  (-->
   (concat
    (my-web-search-prefix)
    ;; (mapconcat #'url-hexify-string (split-string query) "+")
    ;; (url-encode-url query)
    (url-hexify-string query))
   (browse-url it)))

(defvar my-pastebin-table
  `((paste.rs
     :url "https://paste.rs/"
     :data ,(lambda (text) text)
     :method "POST")
    (ix.io
     :url "http://ix.io/"
     :data ,(lambda (text) (format "f:1=%s" text))
     :method "POST")
    ;; (0x0.st :url "https://0x0.st/" :data ,(lambda (text) text) :method "POST")
    ))

(defvar my-pastebin-default 'paste.rs)

;;;###autoload
(defun my-pastebin-upload-text-file (file &optional quiet)
  (interactive (list
                (-let* ((f
                         (read-file-name "`my-pastebin-upload-text-file' : "
                                         nil
                                         buffer-file-name)))
                  (when (y-or-n-p
                         (format "`my-pastebin-upload-text-file' %s?" f))
                    f))
                nil))
  (my-pastebin-upload-text (my-read-text-file file) quiet))

;;;###autoload
(defun my-pastebin-upload-text (text &optional quiet)
  "Upload TEXT (region or whole buffer) to ix.io and display the returned-url URL.
QUIET: mean that."
  (interactive (list
                (-let* ((txt
                         (if (use-region-p)
                             (buffer-substring (region-beginning) (region-end))
                           (buffer-string))))
                  (when (y-or-n-p (format "`my-pastebin-upload-text' %s?" txt))
                    txt))))
  (-let* (((&plist :url url :data data-fn :method request-method)
           (map-nested-elt my-pastebin-table (list my-pastebin-default))))
    (dlet ((url-request-method (or request-method "POST"))
           (url-request-data (funcall data-fn text)))
      (url-retrieve
       url
       (lambda (status)
         (-let* ((returned-url
                  (and url-http-end-of-headers
                       (buffer-substring url-http-end-of-headers (point-max))))
                 (msg (format "Uploaded to: %s" returned-url)))
           (unless quiet
             (notifications-notify
              :body msg
              :actions '("0" "Copy URL" "1" "Open URL")
              :on-action
              (lambda (_id key)
                (pcase key
                  ("0" (my-clipboard-kill-new-or-copy returned-url))
                  ("1" (browse-url returned-url))))))
           (message "%s" msg)))
       nil))))

;;;###autoload
(defun my-alist->url-request-data (alst)
  (mapconcat (-lambda ((k . v))
               (concat
                (url-hexify-string (format "%s" k)) "=" (url-hexify-string v)))
             alst
             "&"))

(defun my-browse-url-no-xdg-open--interactive-form ()
  (progn
    (require 'browse-url)
    ;; don't take '(nil) args
    (list (cl-first (browse-url-interactive-arg "URL: ")))))

;;;###autoload
(defun my-browse-url-no-xdg-open (url &rest args)
  (interactive (my-browse-url-no-xdg-open--interactive-form))
  (my-with-advice
    #'browse-url-can-use-xdg-open
    :override #'ignore (apply #'browse-url url args)))

;;;###autoload
(defun my-browse-url-default-browser-no-xdg-open (url &rest args)
  (my-with-advice
    #'browse-url-can-use-xdg-open
    :override #'ignore (apply #'browse-url-default-browser url args)))

;;;###autoload
(defun my-browse-url-in-new-window (url &rest args)
  "Like (`browse-url' URL &rest ARGS)."
  (interactive (my-browse-url-no-xdg-open--interactive-form))
  (dlet ((browse-url-new-window-flag t))
    ;; `browse-url-xdg-open' doesn't support new window
    (apply #'my-browse-url-no-xdg-open url t (cdr args))))

;;;###autoload
(cl-defun my-net-check-port-open-p (port &key host server)
  (-let* ((host (or host "localhost")))
    (condition-case nil
        (-let* ((proc
                 (make-network-process
                  :name "my-net-check-port-open-p"
                  ;; :server : `t' is faster when opened, `nil' when not
                  :server server
                  :host host
                  :service port)))
          (delete-process proc)
          t)
      (file-error nil))))


(provide 'my-functions-net)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(grugru-default-setup)

(grugru-define-global 'non-alphabet '("<" ">"))
(grugru-define-global 'non-alphabet '("<-" "->"))
(grugru-define-global 'non-alphabet '("<=" ">="))
(grugru-define-global 'word '("True" "False"))
(grugru-define-global 'word '("backward" "forward"))
(grugru-define-global 'word '("decrease" "increase"))
(grugru-define-global 'word '("enable" "disable"))
(grugru-define-global 'word '("enabled" "disabled"))
(grugru-define-global 'word '("even" "odd"))
(grugru-define-global 'word '("get" "set"))
(grugru-define-global 'word '("in" "out"))
(grugru-define-global 'word '("light" "dark"))
(grugru-define-global 'word '("male" "female"))
(grugru-define-global 'word '("man" "woman"))
(grugru-define-global 'word '("men" "women"))
(grugru-define-global 'word '("min" "max"))
(grugru-define-global 'word '("minimum" "maximum"))
(grugru-define-global 'word '("positive" "negative"))
(grugru-define-global 'word '("previous" "next"))
(grugru-define-global 'word '("supervised" "unsupervised"))
(grugru-define-global 'word '("true" "false"))
(grugru-define-global 'word '("up" "down"))
(grugru-define-global 'word '("yes" "no"))
(grugru-define-on-major-mode 'emacs-lisp-mode 'word '("list" "vector"))

;;; my-after-grugru.el ends here

(provide 'my-after-grugru)

;; Local Variables:
;; no-byte-compile: t
;; End:

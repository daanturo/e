;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-email-message-body-beg-position (&optional ignore-spaces)
  (save-excursion
    (message-goto-body)
    (when ignore-spaces
      (skip-syntax-forward " "))
    (point)))

;;;###autoload
(defun my-mail-copy-message-body ()
  (interactive)
  (-let* ((beg (my-email-message-body-beg-position t))
          (end (point-max)))
    (pulse-momentary-highlight-region beg end)
    (kill-new (buffer-substring beg end))))

;;; my-functions-mail.el ends here

(provide 'my-functions-mail)

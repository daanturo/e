;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(set-popup-rule! "\\`\\*Shortdoc .*\\*" :ignore t)

(provide 'my-after-shortdoc)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-scimax-init (&optional scimax-dir)
  ;; (find-library-name "scimax")
  (-let* ((scimax-dir
           (or scimax-dir
               (file-name-concat my-useelisp-repositories-dir/ "scimax"))))
    (load (file-name-concat scimax-dir "init.el"))))

;;; my-functions-scimax.el ends here

(provide 'my-functions-scimax)

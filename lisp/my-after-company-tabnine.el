;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(autoload #'my-complete-with-tabnine "after-company-tabnine" nil t)
(defalias #'my-complete-with-tabnine
  (cape-capf-interactive (cape-company-to-capf #'company-tabnine)))

;;;###autoload
(defun my-complete-with-company-tabnine ()
  (interactive)
  (my-with-mode/s '(company-mode) t
    (company-tabnine 'interactive)))


;; (add-hook 'company-backends #'company-tabnine)


;;; after-company-tabnine.el ends here

(provide 'my-after-company-tabnine)

;; Local Variables:
;; no-byte-compile: t
;; End:

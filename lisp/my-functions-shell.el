;; -*- lexical-binding: t; -*-

;; (backtrace)

(require 'dash)
(require '00-my-core-macros)

(require 'comint)

;;;###autoload
(defun my-get-shell ()
  (or explicit-shell-file-name (getenv "ESHELL") shell-file-name))

;;;###autoload
(defmacro my-with-interactive-sh (&rest body)
  "Run BODY with the interactive shell option enabled."
  `(dlet ((shell-command-switch (replace-regexp-in-string "^-\\([^-]+\\)" "-i\\1"
                                                          shell-command-switch)))
     ,@body))

;;;###autoload
(defun my-with-interactive-sh-maybe-a (func &rest args)
  "Use interactive shell around (FUNC @ARGS) when invoked interactively.
Setting the \"-i\" switch all the time will significantly slow
down `shell-command' because maybe there are too many files to
source."
  (if (called-interactively-p t)
      (my-with-interactive-sh (apply func args))
    (apply func args)))

;;;###autoload
(defun my-with-interactive-sh-a (func &rest args)
  (my-with-interactive-sh (apply func args)))

;;;###autoload
(defun my-shell--environment ()
  (append (comint-term-environment)
          process-environment))

;;;###autoload
(defun my-get-shell-aliases ()
  "Return the list of system shell's aliases as an alist."
  (my-parse-lines->alist (my-with-interactive-sh
                          (shell-command-to-string "alias"))
                         "="
                         "'"))

;;;###autoload
(defun my-confirm-shell-command (ask-interact command &optional prompt)
  "With non-nil ASK-INTERACT, `read-shell-command' on COMMAND with PROMPT.
Else just return COMMAND."
  (declare (indent defun))
  (if ask-interact
      (read-shell-command (or prompt (format "%s: " #'my-confirm-shell-command))
                          command)
    command))

;;;###autoload
(defun my-async-shell-command-maybe-region (&optional in-project-root)
  (interactive "P")
  (declare (interactive-only t))
  (my-with-interactive-sh
   (dlet ((default-directory
           (if in-project-root
               (or (my-project-root-maybe) (user-error "No project root!"))
             default-directory)))
     (-let* ((cmd
              (read-shell-command (format
                                   "`async-shell-command' in `%s/' : "
                                   (file-name-nondirectory
                                    (directory-file-name default-directory)))
                                  (and (use-region-p)
                                       (buffer-substring-no-properties
                                        (region-beginning) (region-end))))))
       (async-shell-command cmd)))))

;;;###autoload
(progn
  (defun my-new-shell||terminal-command (func &rest func-args)
    (let ((cmd (my-form-sym "%s-%s" 'my-new func)))
      (defalias cmd
        (lambda ()
          (interactive)
          (let ((buf-name (buffer-name)))
            (select-window (my-split-current-window))
            (dlet ((display-buffer-alist nil))
              (apply func (-map #'my-lexical-eval func-args)))
            (rename-buffer buf-name ':unique)))
        (format "Split current window and create a new `%s'." func))
      cmd)))

;;;###autoload
(defun my-read-shell-command-toggle-sudo ()
  (interactive)
  (-let* ((sudo-str "sudo "))
    (save-excursion
      (goto-char (minibuffer-prompt-end))
      (if (looking-at (regexp-quote sudo-str))
          (delete-char (length sudo-str))
        (insert sudo-str)))))


(provide 'my-functions-shell)

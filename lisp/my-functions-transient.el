;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'transient)

;;;###autoload
(transient-define-prefix
  my-vc-smerge-transient
  ()
  ;; always continue
  :transient-suffix t
  :transient-non-suffix t
  ;; based on `smerge-basic-map' and smerge hydras
  [["Navigate"
    ;; ("M-n" "Next" smerge-next)
    ("p" "Previous" smerge-prev) ("n" "Project's next" smerge-vc-next-conflict)]
   ["Keep"
    ("b" "Keep base" smerge-keep-base)
    ("u" "Keep upper" smerge-keep-upper)
    ("l" "Keep lower" smerge-keep-lower)
    ("a" "Keep all" smerge-keep-all)]
   ["Diff"
    ("= <" "Base upper" smerge-diff-base-upper)
    ("= >" "Base lower" smerge-diff-base-lower)
    ("= =" "Upper lower" smerge-diff-upper-lower)]
   ["Other"
    ("r" "Resolve" smerge-resolve)
    ("E" "Ediff" smerge-ediff)
    ("C" "Combine with next" smerge-combine-with-next)
    ("R" "Refine" smerge-refine)
    ("<C-m>" "Keepcurrent" smerge-keep-current)]
   ["" ("<escape>" ""
        (lambda ()
          (interactive)
          (smerge-auto-leave)
          (setq this-command #'transient-quit-one)
          (transient-quit-one))
        :transient nil)]
   ;;
   ]

  (interactive)
  (require 'smerge-mode)
  (smerge-mode)
  (transient-setup 'my-vc-smerge-transient)

  ;;
  )

;;; my-functions-transient.el ends here

(provide 'my-functions-transient)

;; Local Variables:
;; no-byte-compile: t
;; End:

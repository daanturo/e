;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'calc-comb)

;;;###autoload
(defun my-calculate-region-and-insert ()
  (declare (interactive-only t))
  (interactive)
  (unless (use-region-p)
    (user-error "`%s': No region!" #'my-calculate-region-and-insert))
  (-let* ((str-expr
           (buffer-substring-no-properties (region-beginning) (region-end)))
          (result (my-calculate str-expr 'return-string)))
    (save-mark-and-excursion
      (goto-char (region-end))
      (insert result))
    (setq deactivate-mark nil) ; keep marked region after this command
    result))

;;;###autoload
(defun my-calculate-region-and-copy ()
  (declare (interactive-only t))
  (interactive)
  (unless (use-region-p)
    (user-error "`%s': No region!" #'my-calculate-region-and-copy))
  (-let* ((str-expr
           (buffer-substring-no-properties (region-beginning) (region-end)))
          (result (my-calculate str-expr 'return-string)))
    (my-clipboard-kill-new-or-copy result)
    (message "`my-calculate-region-and-copy': %s" result)
    result))


;;; my-functions-calc.el ends here

(provide 'my-functions-calc)

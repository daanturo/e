;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-eat-maybe-project (&optional arg)
  (interactive "P")
  (if (project-current)
      (eat-project arg)
    (eat nil arg)))

;;;###autoload
(defun my-sub-terminal-emulator (&optional name)
  (interactive)
  (cond
   ((fboundp #'eat)
    (-let* ((buf (eat nil)))
      (when name
        (with-current-buffer buf
          (rename-buffer name)))
      buf))
   (t
    (ansi-term shell-file-name name))))

;;;###autoload
(defun my-toggle-terminal (terminal-buffer-name)
  (if (equal (buffer-name) terminal-buffer-name)
      (delete-window)
    (progn
      (unless (get-buffer terminal-buffer-name)
        (save-window-excursion (my-sub-terminal-emulator terminal-buffer-name)))
      (prog1 (pop-to-buffer terminal-buffer-name)
        (my-termimal-renew)
        ;; Keep the ESC key
        (when (bound-and-true-p evil-local-mode)
          (evil-local-mode 0))
        ;; Doom's `+popup' prevents terminal emulators from sending the "ESC"
        ;; key, TODO report
        (my-safe-call #'+popup-buffer-mode 0)))))

(defvar my-terminal-external-emulator-list
  '( ;
    ("konsole" "--hold" "-e") ;
    ("qterminal" "-e") ;
    ("xfce4-terminal" "--hold" "-e" cmd) ;
    ("lxterminal" "-e") ;
    ("gnome-terminal" "--") ;
    ("kgx" "-e") ;
    ;;
    ))

;;;###autoload
(defun my-terminal-external-emulator (&optional dir)
  (interactive)
  (dlet ((default-directory (or dir default-directory)))
    (with-temp-buffer
      (-let*
          ((cmdargs
            (list
             (-find
              #'executable-find
              (-map #'car my-terminal-external-emulator-list))))
           (proc
            (progn
              (unless cmdargs
                (error
                 "`my-terminal-external-emulator': no known terminal emulator in `%s' is installed!"
                 'my-terminal-external-emulator-list))
              (apply #'start-process "" (current-buffer) "setsid" cmdargs))))
        (unless (process-live-p proc)
          (error
           "`my-terminal-external-emulator' [%S]: %s"
           cmdargs
           (buffer-substring-no-properties (point-min) (point-max))))
        (set-process-buffer proc nil)
        proc))))

;;;###autoload
(defun my-terminal-external-emulator-cmdargs (cmdargs &optional options)
  (-when-let* (((program . must-opts)
                (-some-->
                    (-find
                     (-lambda ((prog . _)) (executable-find prog))
                     my-terminal-external-emulator-list)))
               (raw-program-args `(,program ,@must-opts))
               (must-cmdargs
                (cond
                 ((member 'cmd raw-program-args)
                  (-replace 'cmd (combine-and-quote-strings cmdargs) must-opts))
                 (:else
                  `(,@must-opts ,@cmdargs)))))
    (--> `(,program ,@options ,@must-cmdargs) (delete nil it))))

;;;###autoload
(defun my-terminal-execute-in-external-emulator (&rest cmdargs)
  (-when-let* ((program-args
                (-some-->
                    (my-terminal-external-emulator-cmdargs
                     cmdargs)
                  `("setsid" ,@it)))
               (cmd* (combine-and-quote-strings program-args)))
    (apply #'start-process cmd* nil program-args)))

;;;###autoload
(defun my-terminal-emulator-at-project-root (arg)
  "ARG: the reverse of `my-terminal-emulator-dwim'."
  (interactive "P")
  (my-terminal-emulator-dwim (not arg) (my-project-root-maybe)))

;;;###autoload
(defun my-terminal-emulator-dwim (&optional arg dir)
  "Launch/switch to/hide a terminal emulator at DIR.
- Universal prefix ARG and current buffer has a process:
  `my-new-session-process-external-terminal-emulator.'
- Non-nil ARG: `my-terminal-external-emulator.'
- Else: `my-toggle-terminal.'"
  (interactive "P")
  (let* ((dir (or dir default-directory))
         (terminal-buffer-name (my-project-buffer-name "term" dir)))
    (dlet ((default-directory dir))
      (cond
       ((and (my-non-empty-list? arg) (get-buffer-process (current-buffer)))
        (my-new-session-process-external-terminal-emulator nil t))
       (arg
        (my-terminal-external-emulator))
       (t
        (my-toggle-terminal terminal-buffer-name))))))

;;;###autoload
(defun my-terminal-launch-an-emacs-lisp-emulator-in-new-session
    (&optional init-dir profile debug-init)
  (interactive (list
                nil
                (if current-prefix-arg
                    "unkit"
                  nil)
                nil))
  (dlet ((default-directory (or init-dir (my-project-root) default-directory)))
    (my-process-eval-in-new-session-emacs
     `(progn
        (my-sub-terminal-emulator)
        (set-face-attribute 'default nil
                            :height ,(face-attribute 'default :height)))
     `(,@(and debug-init '("--debug-init"))
       ,@(and profile (list "--with-profile" profile))))))

(defvar my-termimal-renew-function-alist
  '((eat-mode . my-eat-renew) (term-mode . my-ansi-term-renew)))

;;;###autoload
(defun my-termimal-renew (&optional force)
  (interactive)
  (when (or force (not (get-buffer-process (current-buffer))))
    (-let* ((fn
             (-some
              (-lambda ((mode . renew-fn))
                (and (derived-mode-p mode) renew-fn))
              my-termimal-renew-function-alist)))
      (cond
       (fn
        (funcall fn))
       (:else
        (user-error "How?"))))))

;;;###autoload
(defvar my-repl|term-modes-to-exit
  '( ;
    shell-mode
    term-mode vterm-mode eat-mode

    ;; cider-repl-mode geiser-repl-mode inferior-python-mode inferior-scheme-mode

    ;;
    ))
;;;###autoload
(defun my-try-to-exit-term-buffers-a (&rest _)
  (my-try-to-exit-repl|term-buffers))
;;;###autoload
(defun my-try-to-exit-repl|term-buffers (&optional send-times)
  (interactive)
  (dolist (proc
           (-keep
            'get-buffer-process
            (my-buffers-with-major-mode/s my-repl|term-modes-to-exit)))
    (dotimes (_ (or send-times 1))
      (my-send-EOF-to-process-unless-has-children proc))))


(provide 'my-functions-terminal)

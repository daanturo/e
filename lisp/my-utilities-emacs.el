;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-reset|custom-reevaluate-setting-variable ()
  (interactive)
  (let ((var
         (intern-soft
          (completing-read "Reset variable: " #'help--symbol-completion-table
                           (lambda (%1) (get %1 'variable-documentation)) nil
                           (and (symbolp (variable-at-point))
                                (symbol-name (variable-at-point)))))))
    (eval (read--expression "Evaluate: "
                            (format "%s" `(custom-reevaluate-setting ',var)))
          t)
    (message "`%s' : %S" var (symbol-value var))))

;;;###autoload
(defun my-outdated-native-compile-eln-directories ()
  (--> native-comp-eln-load-path
       (-mapcat (-cut directory-files <> t "[0-9]") it)
       (-filter #'file-writable-p it)
       (-remove (-cut string-suffix-p comp-native-version-dir <>) it)
       (-map #'abbreviate-file-name it)))

;;;###autoload
(defun my-delete-outdated-native-compile-eln-directories (&optional yes)
  "YES. Use `native-compile-prune-cache' instead when available."
  (interactive)
  (if (fboundp #'native-compile-prune-cache)
      (native-compile-prune-cache)
    (let ((cmd (format "rm -rf %s"
                       (string-join (my-outdated-native-compile-eln-directories) " "))))
      (async-shell-command
       (if yes
           cmd
         (read-shell-command (format "Delete except %s: " comp-native-version-dir) cmd))))))

;;;###autoload
(defun my-get-library-dependencies-recursively (lib)
  (let ((file (locate-library (format "%s.el" lib) 'nosuffix)))
    (when file
      (list
       (intern (format "%s" lib))
       "."
       (--> file
            (my-read-text-file it)
            (s-match-strings-all "^(require '\\(.*?\\))" it)
            (-map (-partial #'nth 1) it)
            (-map #'my-get-library-dependencies-recursively it))))))

;;;###autoload
(defun my-pull-config ()
  "Fetch and reset Emacs configuration to origin/BRANCH."
  (interactive)
  (require 'vc-git)
  (let* ((default-directory my-emacs-conf-dir/)
         (branches (vc-git-branches))
         (commands (my-for [branch branches]
                     (format "git fetch ; git reset --hard origin/%s" branch))))
    (shell-command
     (completing-read
      (format "Command to run in %s LOCAL CHANGES WILL BE LOST!!!: " default-directory)
      commands))
    (load user-init-file)))

;;;###autoload
(cl-defun my-append-to-file-interactively
    (text
     file &optional (no-pop-new (member (buffer-name) '("*doom*" "*GNU Emacs*"))))
  "Append TEXT to FILE in a new frame."
  (unless (file-exists-p file)
    (make-empty-file file 'parents))
  (-let* ((buf
           (if (or no-pop-new
                   (and buffer-file-name (file-equal-p buffer-file-name file)))
               (find-file file)
             (find-file-other-frame file))))
    (goto-char (point-max))
    (insert text)
    buf))

;;;###autoload
(defun my-ensure-empty-elisp-file (path)
  (unless (file-exists-p path)
    (make-empty-file path)
    (with-current-buffer (find-file-noselect path)
      (add-file-local-variable-prop-line 'lexical-binding t)
      (add-file-local-variable 'no-byte-compile t)
      (save-buffer)))
  path)

;;;###autoload
(defun my-restart-emacs-daemon||server ()
  "Restart emacs daemon and may create a new frame."
  (interactive)
  (start-process-shell-command "restart emacs daemon"
                               nil
                               "systemctl --user restart my-emacs && emacsclient -nc"))

;;;###autoload
(defun my-print-arguments (&rest args)
  (interactive "P")
  (print args))

;;;###autoload
(defun my-print-arguments-a (fn &rest args)
  (interactive "P")
  (message "%s %s %S" #'my-print-arguments-a fn args)
  (apply fn args))

;;;###autoload
(defun my-print-function-arguments-when-called ()
  (interactive)
  (advice-add
   (my-completing-read-symbol
    "my-print-function-arguments-when-called : " nil 'functionp)
   :around #'my-print-arguments-a)
  (message
   "Stop printing with `my-undo-print-function-arguments-when-called'."))

;;;###autoload
(defun my-undo-print-function-arguments-when-called ()
  (interactive)
  (advice-remove
   (my-completing-read-symbol
    "my-undo-print-function-arguments-when-called : " nil 'functionp)
   #'my-print-arguments-a))

;;;###autoload
(defun my-load-init-file ()
  (interactive "P")
  (load (file-name-concat my-emacs-conf-dir/ "init.el"))
  (load (file-name-concat my-emacs-conf-dir/ "config.el") 'noerror))

;;;###autoload
(defun my-load-custom-file ()
  (interactive)
  (load custom-file)
  (my-set-sensible-default-face-height))

;;;###autoload
(defun my-get-list-of-installed-emacs-versions ()
  (-->
   (format "bash -c %S" "compgen -c emacs-")
   (shell-command-to-string it)
   (string-lines it t)
   (seq-filter (my-fn% (string-match-p "emacs-[0-9\\.]+$" %1)) it)
   (sort it #'string<)
   (delete-consecutive-dups it)))

;;;###autoload
(cl-defun my-compile-elisp-directory (dir &optional (exclude-regexps my-elisp-compile-exclude-regexps))
  (let ((r (my-regexp-or exclude-regexps)))
    (my-with-advice #'directory-files :filter-return
      (lambda (files)
        (seq-remove (lambda (file) (string-match-p r file))
                    files))
      (byte-recompile-directory dir 0)
      (when (fboundp #'native-compile-async)
        (native-compile-async dir 'recursively)))))

;;;###autoload
(defun my-clean-package-el ()
  (interactive)
  (let ((d (file-name-concat my-emacs-conf-dir/ "elpa")))
    (when (and (file-directory-p d) (not (equal package-user-dir d)))
      (delete-directory d 'recursive)))
  (when (file-exists-p package-quickstart-file)
    ;; `package-quickstart-refresh''s `emacs-lisp-mode' is problematic
    (condition-case _err
        ;; dlet ((delay-mode-hooks t))
        (my-safe-call #'package-quickstart-refresh)
      (error
       (delete-file package-quickstart-file)
       (delete-file
        (concat (file-name-sans-extension package-quickstart-file) ".elc"))
       (my-safe-call #'package-quickstart-refresh)))))

(defvar my-clean-emacs-functions '())

;;;###autoload
(defun my-clean-emacs ()
  (interactive)
  (save-selected-window

    (delete-directory my-useelisp-build-dir 'recursive)
    (my-clean-package-el)
    (my-elisp-clean-compiled-files my-emacs-conf-dir/)
    (my-lsp-remove-workspaces)
    (my-safe-call #'native-compile-prune-cache)
    (my-safe-call #'project-forget-zombie-projects)
    (ignore-errors (projectile-invalidate-cache))
    (project-forget-projects-under my-useelisp-repositories-dir/ 'recursive)

    (run-hooks 'my-clean-emacs-functions)

    ;;
    ))

;;;###autoload
(defun my-filter-faces (&rest key-vals)
  "Get list of faces whose KEY = VAL."
  (let ((kv-pairs (seq-partition key-vals 2)))
    (seq-filter
     (lambda (face)
       (seq-every-p
        (-lambda ((key val))
          (let ((fv (face-attribute face key)))
            (if (member key '(:foreground :background :distant-foreground))
                (facemenu-color-equal fv val)
              (equal fv val))))
        kv-pairs))
     (face-list))))

;;;###autoload
(defun my-emacs-invocation-path ()
  (file-name-concat invocation-directory invocation-name))

;;;###autoload
(defun my-process-eshell-commands (&rest commands)
  "Run `eshell' in an \"Emacs -q\" process with each of COMMANDS inserted as a line in the prompt."
  (my-process-eval-in-new-session-emacs
   `(progn (eshell)
           (insert ,(--> (-map (-cut format "%s" <>) commands)
                         (string-join it "\n"))))))

;;;###autoload
(defun my-force-debug-when-error-a (func &rest args)
  (condition-case err
      (apply func args)
    ((debug error) (signal (car err) (cdr err)))))

;;;###autoload
(defun my-add-force-debug-when-error-advice-to-symbol ()
  (declare (interactive-only t))
  (interactive)
  (-let* ((sym
           (my-completing-read-symbol
            "Force debug: " nil #'functionp nil (thing-at-point 'symbol))))
    (advice-add sym :around #'my-force-debug-when-error-a)))

;;;###autoload
(defun my-report-current-outline-level ()
  (interactive)
  (save-excursion
    (forward-line 0)
    (save-match-data
      (string-match (concat "^" outline-regexp)
                    (thing-at-point 'line))
      (print (outline-level)))))

;;;###autoload
(defun my-thread-list (pos initial-list &optional placeholder)
  "Return the threaded form of INITIAL-LIST.
POS is the threaded position.
POS = 1: `thread-first', POS < 1: `thread-last'.
With non-nil PLACEHOLDER, insert it at POS."
  (my-loop [lst initial-list accu '()]
    (cond
     ((null lst)
      accu)
     ((or (not (listp lst))
          (equal 'quote (car lst)))
      (cons lst accu))
     (t
      (let* (
             ;; bind POS within the possible range
             (pos+ (let ((mi (1- (length lst))))
                     (if (<= 1 pos mi) pos mi)))
             (others (cond
                      ;; single argument: just the form
                      ((length= lst 2)
                       (car lst))
                      (placeholder
                       (-replace-at pos+ placeholder lst))
                      (t
                       (-remove-at pos+ lst))))
             (sub (nth pos+ lst)))
        (recur sub (cons others accu)))))))

;;;###autoload
(defun my-unthread-list (pos initial-list &optional placeholder)
  (my-loop [lst (cdr initial-list)
                ;; the first form is likely a complete expression
                accu (car initial-list)]
    (let* ((form (car-safe lst))
           ;; max index = POS: insert at last
           (mi (length form))
           (pos+ (if (<= 1 pos mi) pos mi)))
      (cond ((seq-empty-p lst)
             accu)
            ;; standalone form
            ((not (listp form))
             (recur (cdr lst)
                    `(,form ,accu)))
            ;; replace placeholder
            ((and placeholder
                  (member placeholder form))
             (recur (cdr lst)
                    (-replace placeholder accu form)))
            (t
             (recur (cdr lst)
                    (-insert-at pos+ accu form)))))))

;;;###autoload
(defun my-thread-region (beg end position)
  "Apply a threading macro by prefix arg POSITION from BEG to END."
  (interactive (list (region-beginning) (region-end)
                     (prefix-numeric-value current-prefix-arg)))
  (replace-region-contents
   beg end
   (-const
    (--> (buffer-substring beg end)
         read
         (my-thread-list position it (and (> position 1) 'it))
         (cons (cond ((= position 1) 'thread-first)
                     ((< position 1) 'thread-last)
                     ((> position 1) '-->))
               it)
         (format "%S" it)))))

;;;###autoload
(defun my-unthread-region (beg end position &optional placeholder)
  (interactive (list (region-beginning) (region-end)
                     (prefix-numeric-value current-prefix-arg)
                     'it))
  (replace-region-contents
   beg end
   (-const
    (--> (buffer-substring beg end)
         read
         ;; ignore the callable `thread-first', etc
         cdr
         (my-unthread-list position it (and (< 1 position) placeholder))
         (format "%S" it)))))

;;;###autoload
(progn
  (defun my-feature-loaded-p (feature)
    (interactive (list
                  (if (and (member 'find-func features)
                           (fboundp #'read-library-name))
                      (intern (read-library-name))
                    (read))))
    (print (car (member feature features))))
  (defun my-filter-loaded-features (re)
    (-filter (lambda (ft) (string-match-p re (format "%s" ft))) features)))

;;;###autoload
(defun my-electric-pairs-to-string ()
  (my-for [sym '(electric-pair-pairs electric-pair-text-pairs)]
    (cons sym
          (my-for [(open . close) (buffer-local-value sym (current-buffer))]
            (cons (char-to-string open)
                  (char-to-string close))))))

;;;###autoload
(defun my-buffer-process (&optional buffer)
  "Don't use this in lisp codes."
  (declare (obsolete nil nil))
  (get-buffer-process (or buffer (current-buffer))))

(defun my-keys-not-in-maps (map &rest maps)
  "Print keys in `MAP' that are not found in any of `MAPS'. "
  (map-keymap (lambda (sym-key _command)
                ;; For every map in MAPS, construct a list of commands that `sym-key' binds to
                ;; Filter-out nil members of that list
                ;; if the list is empty afterwards, then the command bound by `sym-key' is not found in MAPS
                (unless (seq-remove #'null
                                    (seq-map
                                     (lambda (map-in-maps)
                                       (lookup-key map-in-maps (list sym-key)))
                                     maps))
                  (print (key-description (list sym-key)))))
              map))

;;;###autoload
(defun my-emacs-lisp-disable-checkdoc (&optional directory)
  (interactive "P")
  (if directory
      (add-dir-local-variable 'emacs-lisp-mode
                              'flycheck-disabled-checkers
                              ''(emacs-lisp-checkdoc))
    (add-file-local-variable-prop-line 'flycheck-disabled-checkers
                                       ''(emacs-lisp-checkdoc))))

;;;###autoload
(defun my-auto-self-removal-expression (file-name form)
  (declare (indent defun) (pure t) (side-effect-free t))
  (-let* ((hashed (secure-hash 'sha512 (format "%s" form))))
    `(progn
       (with-current-buffer (find-file-noselect ,file-name)
         (save-excursion
           (goto-char (point-min))
           (search-forward ,hashed nil)
           (delete-region (my-beg-defun-position)
                          (my-end-defun-position))
           (save-buffer 0)))
       ,form)))

;;;###autoload
(defun my-restart-daemon-make-frame-a (fn &rest args)
  (f-append-text (--> (my-auto-self-removal-expression custom-file
                        '(make-frame))
                      (pp it)
                      (concat "\n" it))
                 'utf-8
                 custom-file)
  (apply fn args))

;;;###autoload
(defun my-find-display-buffer-alist-associative-action-rule (&optional buf-name)
  (display-buffer-assq-regexp (or (bound-and-true-p buf-name)
                                  (buffer-name))
                              display-buffer-alist
                              nil))

;;;###autoload
(cl-defun my-nearest-named-colors (color &optional (num 7))
  (--> (-map 'car color-name-rgb-alist)
       (sort it
             (lambda (c0 c1)
               (< (color-distance c0 color)
                  (color-distance c1 color))))
       (-take num it)))

;;;###autoload
(defun my-trimmed-load-path (&optional keep-regexp)
  (-filter (lambda (path)
             (or (and keep-regexp
                      (string-match-p keep-regexp path))
                 (not (or (string-prefix-p (expand-file-name "~/") path)
                          (string-prefix-p "~/" path)))))
           load-path))

;;;###autoload
(defun my-built-in-restart-emacs ()
  (interactive)
  (when (and (<= 29 emacs-major-version)
             ;; non-nil plist => in `restart-emacs.el'
             (symbol-plist 'restart-emacs))
    (load "files")
    (setplist 'restart-emacs nil))
  (restart-emacs))

;;;###autoload
(defun my-add-dir-local-variable-2 ()
  (interactive)
  (dlet ((dir-locals-file ".dir-locals-2.el"))
    (call-interactively #'add-dir-local-variable)))

;;;###autoload
(defun my-add-dir-local-variable-by-read-expr ()
  (interactive)
  (-let* ((expr (minibuffer-with-setup-hook (lambda ()
                                              (backward-char 1))
                  (read--expression "Eval: " "(add-dir-local-variable nil ')"))))
    (eval expr t)
    (when (string-match-p "\\.dir-locals\\(-2\\)?\\.el$"
                          buffer-file-name)
      (save-buffer))))

;;;###autoload
(defun my-abbreviate-load-paths (&rest _)
  "Useful to generalize user name."
  (interactive)
  (setq load-path
        (-map #'abbreviate-file-name load-path)))

;;;###autoload
(defun my-file-ensure-directory (file)
  (if (file-directory-p file)
      file
    (file-name-parent-directory file)))

;;;###autoload
(defun my-dired-delete-all-directories-of-listed-files ()
  (interactive)
  (-let* ((dirs
           (-->
            (my-dired-listed-files)
            (-map #'my-file-ensure-directory it)
            (-map #'file-relative-name it)
            (-map #'my-normalize-directory-name it)
            (-uniq it))))
    (when (y-or-n-p (format "Delete: %s" dirs))
      (mapc #'my-delete-directory dirs))))

;;;###autoload
(defun my-print-current-command-keys ()
  (message "`my-print-current-command-keys': %S"
           (-->
            (my-for
              [func
               '(this-command-keys
                 this-command-keys-vector
                 this-single-command-keys
                 this-single-command-raw-keys)]
              (-let* ((called (funcall func)))
                (list func called (key-description called))))
            (seq-into it 'vector))))

;;;###autoload
(defun my-delete-all-overlays ()
  (interactive)
  (declare (obsolete nil nil))
  (mapc #'delete-overlay (overlays-in (point-min) (point-max))))

;;;###autoload
(defun my-decode-coding-string-utf-8 (str &rest args)
  (declare (obsolete nil nil))
  (apply #'decode-coding-string str 'utf-8 args))

;;;###autoload
(defun my-encode-coding-string-utf-8 (str &rest args)
  (declare (obsolete nil nil))
  (apply #'encode-coding-string str 'utf-8 args))

;;;###autoload
(defun my-emacs-symbol-list ()
  (declare (obsolete nil nil))
  (cl-loop for symbol being the symbols collect symbol))

;;;###autoload
(defun my-rename-buffer ()
  (declare (interactive-only t))
  (interactive)
  (-let* ((new-name (read-string "`my-rename-buffer': " (buffer-name))))
    (rename-buffer new-name)))

;;;###autoload
(defun my-buffer-substring-by-cons-cell-bounds (cons-cell)
  (-let* (((beg . end) cons-cell))
    (buffer-substring-no-properties beg end)))

;;;###autoload
(defun my-get-from-auto-mode-alist (&optional file-name)
  (-let* ((file-name (or file-name buffer-file-name)))
    (-find
     (-lambda ((re . rest)) (string-match-p re file-name)) auto-mode-alist)))

;;;###autoload
(cl-defun my-backup-user-emacs-config-directory (&optional callback interact dry-run)
  (interactive (list nil t))
  (-let* ((path (directory-file-name (file-truename my-emacs-conf-dir/)))
          (config-dir-name (file-name-nondirectory path))
          (parent-dir (file-name-parent-directory path))
          (file-list
           (dlet ((default-directory path))
             `(".git" ,@(process-lines-ignore-status "git" "ls-files"))))
          (excludes
           (list
            (-->
             (file-relative-name my-useelisp-repositories-dir/
                                 my-emacs-conf-dir/)
             (file-name-as-directory it) (concat it "*"))
            "custom.el"))
          (backup-path
           (format "%s--%s.tar.%s"
                   path
                   (format-time-string "%FT%T%z")
                   my-compress-suffix))
          (cmdargs
           `("tar" "-cavf" ,backup-path
             ;; ,@(-map
             ;;    (lambda (pattern) (format "--exclude=%s" pattern)) excludes)
             ,@(-map
                (lambda (file) (file-name-concat config-dir-name file))
                file-list))))
    (when (not dry-run)
      (dlet ((default-directory parent-dir))
        (my-process-shell-async
          cmdargs
          (lambda (&rest _)
            (when interact
              (my-xdg-open-file-with-default-application parent-dir))
            (when callback
              (funcall callback backup-path))))))
    backup-path))

;;;###autoload
(defun my-file-modification-time (file)
  (declare (obsolete nil nil))
  (--> (file-attributes file) (file-attribute-modification-time it)))

;;;###autoload
(defun my-json-parse-string (str &rest args)
  (apply #'json-parse-string str :object-type 'alist args))

;;;###autoload
(defun my-json-parse-buffer (&rest args)
  (apply #'json-parse-buffer :object-type 'alist args))

(defvar my-add-advice-count-execution-time-table '())

;;;###autoload
(defun my-add-advice-count-execution-time (func)
  (interactive (list
                (my-completing-read-symbol
                 "`my-add-advice-count-execution-time': " nil #'functionp)))
  (-let*
      ((adv
        (lambda (func1 &rest args)
          (:documentation
           (format
            "`my-add-advice-count-execution-time', `my-add-advice-count-execution-time-table'."))
          (cl-incf
           (alist-get func my-add-advice-count-execution-time-table
                      nil
                      nil
                      #'equal))
          (apply func1 args))))
    (when (not (assoc func my-add-advice-count-execution-time-table))
      (add-to-list 'my-add-advice-count-execution-time-table (cons func 0)))
    (advice-add func :around adv)
    adv))


;;;###autoload
(defun my-pulse-momentary-highlight-region-as-cons (bounds &optional face)
  (interactive (list (cons (region-beginning) (region-end)) nil))
  (pulse-momentary-highlight-region (car bounds) (cdr bounds) face))

;;;###autoload
(defun my-call-menu-item-filter (menu-item-lst)
  (-let* (((_ _ real-binding . (&plist :filter filter)) menu-item-lst))
    (funcall filter real-binding)))

;;; my-utilities-emacs.el ends here

(provide 'my-utilities-emacs)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)

(setq calc-multiplication-has-precedence nil)

;; TIP: use `quick-calc' to quickly perform an express that is read from the
;; minibuffer

(provide 'my-after-calc)

;; Local Variables:
;; no-byte-compile: t
;; End:

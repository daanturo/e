;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-disable-kill-emacs-hook-exclude-regexps '())

(defvar my-disable-kill-emacs-hook--disabled '())
;;;###autoload
(defun my-disable-kill-emacs-hook (&rest _)
  (setq my-disable-kill-emacs-hook--disabled
        (delete-dups
         (append my-disable-kill-emacs-hook--disabled kill-emacs-hook)))
  (setq kill-emacs-hook
        (save-match-data
          (-filter
           (lambda (fn)
             (-let* ((name (format "%s" fn)))
               (-some
                (lambda (regexp) (string-match regexp name))
                my-disable-kill-emacs-hook-exclude-regexps)))
           kill-emacs-hook))))

;;;###autoload
(defun my-daemon-default-p (&optional default-name)
  (-let* ((default-name (or default-name "server")))
    (equal default-name server-name)))

;;;###autoload
(defun my-disable-kill-emacs-hook-when-daemon-and-not-default
    (&optional default-daemon-name)
  "Try to disable persisting when in daemon-mode and doesn't match DEFAULT-DAEMON-NAME."
  (when (and (daemonp) (not (my-daemon-default-p default-daemon-name)))
    (-let* ((_kill-hook-0 (seq-copy kill-emacs-hook)))
      (my-disable-kill-emacs-hook)
      ;; catch other lazy stuffs
      (my-add-hook/s
        '(window-selection-change-functions) '(my-disable-kill-emacs-hook))
      ;; (advice-add #'delete-frame :after #'my-disable-kill-emacs-hook)
      (message
       "`my-disable-kill-emacs-hook-unless-default-daemon' `kill-emacs-hook' disabled."))))
;; (when (not (equal kill-hook-0 kill-emacs-hook)))


;;; my-functions-daemon.el ends here

(provide 'my-functions-daemon)

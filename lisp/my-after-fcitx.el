;; -*- lexical-binding: t; -*-

(require 'dash)

(setq fcitx-active-evil-states my-evil-typing-state-list)

(when (executable-find "fcitx5-remote")
  (setq fcitx-remote-command "fcitx5-remote"))
;; (setq fcitx-use-dbus (and (eq system-type 'gnu/linux) 'fcitx5))


;; preedit conflicts
(when (fboundp #'blamer-mode)
  (my-when-mode0-turn-off||disable-mode1 'my-fcitx-auto-de?activate-mode '(blamer-mode)))

(fcitx-default-setup)
(fcitx-prefix-keys-add
 "<f1>" "<f2>" "<help>" "ESC" "M-g" "M-r" "M-s" doom-leader-alt-key)

(provide 'my-after-fcitx)

;; Local Variables:
;; no-byte-compile: t
;; End:

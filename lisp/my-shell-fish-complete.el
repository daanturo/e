;; -*- lexical-binding: t; -*-

;; fish -c "complete -C \"str\""

(defvar-local my-shell-fish-complete-string->capf-data--cache nil)

(defvar my-shell-fish-complete-fish-executable "fish")

;;;###autoload
(defun my-shell-fish-complete--do-complete (str)
  (with-temp-buffer
    (condition-case _
        ;; TODO: why are some output line truncated
        (let* ((proc
                (call-process my-shell-fish-complete-fish-executable
                              nil
                              '(t nil)
                              nil
                              "--command"
                              (format "complete --do-complete %S" str))))
          (if (zerop proc)
              (buffer-substring-no-properties (point-min) (point-max))
            ""))
      (error ""))))

;;;###autoload
(defun my-shell-fish-complete-string->capf-data (str)
  (cond
   ((equal str (nth 0 my-shell-fish-complete-string->capf-data--cache))
    (nth 1 my-shell-fish-complete-string->capf-data--cache))
   (t
    (let*
        ((stdout (my-shell-fish-complete--do-complete str))
         (lines (string-lines stdout t))
         (cand-annot-lst
          (mapcar
           (lambda (line)
             (save-match-data
               (string-match "\\([^ \t]+\\)[ \t]*\\(.*\\)" line)
               (list
                (match-string 1 line)
                ;; TODO alignment
                (concat "\t\t" (match-string 2 line)))))
           lines))
         (retval
          `(,(mapcar #'car cand-annot-lst)
            :annotation-function ,(lambda (cand) (nth 1 (assoc cand cand-annot-lst))))))
      (setq-local my-shell-fish-complete-string->capf-data--cache
                  (list str retval))
      retval))))

;;;###autoload
(defun my-shell-fish-completion-at-point ()
  (when-let* ((bounds
               (or (bounds-of-thing-at-point 'symbol)
                   (bounds-of-thing-at-point 'word)
                   ;; point at empty
                   (cons (point) (point))))
              ;; take from the beginning of line/list to generate completions,
              ;; but only replace current symbol/word
              (beg-to-replace (car bounds))
              (end (cdr bounds))
              (beg
               (max (if (minibufferp)
                        (minibuffer-prompt-end)
                      (pos-bol))
                    (save-excursion
                      (condition-case _
                          (puni-beginning-pos-of-list-around-point)
                        (error 0)))))
              (str (buffer-substring-no-properties beg end))
              (data (my-shell-fish-complete-string->capf-data str))
              (_ (< 0 (length (nth 0 data)))))
    ;; (my-println🌈 (buffer-substring-no-properties (car bounds) (cdr bounds)))
    `(,beg-to-replace ,end ,@data)))

;;;###autoload
(defun my-shell-fish-complete-command ()
  (interactive)
  (dlet ((completion-at-point-functions '(my-shell-fish-completion-at-point)))
    (completion-at-point)))

;;;###autoload
(autoload #'my-lsp-and-shell-fish-completion-at-point "my-shell-fish-complete"
  nil
  t)

(my-cape-lazy-make-capf
  #'my-lsp-and-shell-fish-completion-at-point
  '(cape-capf-super 'lsp-completion-at-point 'my-shell-fish-completion-at-point))

(provide 'my-shell-fish-complete)

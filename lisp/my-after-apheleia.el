;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'apheleia)

;;;###autoload
(progn
  (defun my-setup-apheleia ()
    (add-hook 'apheleia-inhibit-functions #'my-repo-git-not-own-p)
    (my-add-hook-once 'before-save-hook #'apheleia-global-mode)))

;; (when (documentation-property 'apheleia-inhibit-functions 'variable-documentation)
;;   (my-global-auto-format-mode 0)
;;   (unless apheleia-global-mode
;;     (apheleia-global-mode)))

(defun my-apheleia-set-mode-alist (mode/s formatter/s)
  (-let* ((modes
           (-->
            (ensure-list mode/s)
            (append it (-map #'my-treesit-mode->ts-mode it)))))
    (dolist (mode modes)
      (setq apheleia-mode-alist
            (cons
             (cons mode formatter/s)
             (assoc-delete-all mode apheleia-mode-alist))))))

(advice-add
 #'apheleia-format-after-save
 :before #'my-apheleia-format-after-save-before-hook--run)

(setq apheleia-remote-algorithm 'local)

(add-to-list 'apheleia-formatters '(my-bibtex-tidy . ("bibtex-tidy" "--blank-lines")))
(add-to-list 'apheleia-formatters '(my-dockerfmt . ("dockfmt" "fmt" filepath)))
(add-to-list 'apheleia-formatters '(my-isort . ("isort" "--float-to-top" "-")))
(add-to-list 'apheleia-formatters '(my-prettier-jsonc . ("apheleia-npx" "prettier" "--stdin-filepath" filepath "--parser=jsonc")))
(add-to-list 'apheleia-formatters '(my-raco-fmt . ("raco" "fmt" "--width" "80")))
(add-to-list 'apheleia-formatters '(my-taplo . ("taplo" "format" "-")))
(add-to-list 'apheleia-formatters '(my-typstyle . ("typstyle")))
(add-to-list 'apheleia-formatters '(my-zprint . ("zprint" "{:width 80}")))
;; (add-to-list 'apheleia-formatters '(my-scalafmt "scalafmt" "--stdin" "--config-str" "\"version=$(scalafmt --version | cut -f2 -d' ')\""))

(my-apheleia-set-mode-alist '(bibtex-mode) 'my-bibtex-tidy)
(my-apheleia-set-mode-alist '(clojure-mode clojurec-mode clojurescript-mode) 'my-zprint)
(my-apheleia-set-mode-alist '(conf-toml-mode toml-ts-mode) 'my-taplo)
(my-apheleia-set-mode-alist 'emacs-lisp-mode 'lisp-indent)
(my-apheleia-set-mode-alist 'python-mode '(my-isort ruff))
(my-apheleia-set-mode-alist 'racket-mode 'my-raco-fmt)
(my-apheleia-set-mode-alist 'sh-mode 'shfmt) ; `bash-ts-mode' doesn't support in-string fontification ATM
(my-apheleia-set-mode-alist 'typst-mode 'my-typstyle) ; `bash-ts-mode' doesn't support in-string fontification ATM
;; (my-apheleia-set-mode-alist '(json-ts-mode) 'my-prettier-jsonc) ; allow trailing commas, for easy comments
;; (my-apheleia-set-mode-alist 'dockerfile-mode 'my-dockerfmt)
;; (my-apheleia-set-mode-alist 'scala-mode 'my-scalafmt)


(provide 'my-after-apheleia)

;; Local Variables:
;; no-byte-compile: t
;; End:

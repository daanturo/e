;; -*- lexical-binding: t; -*-

(require 'dash)

;;;###autoload
(defun my-queuexecution-make-element (func wait)
  (lambda ()
    (-let* ((result (funcall func)))
      (vector result wait))))

;;;###autoload
(defun my-queuexecution-run (queue)
  (-let* ((func (cl-first queue)))
    (cond
     ((seq-empty-p queue)
      nil)
     ((functionp func)
      (-let* (([_result wait] (funcall func))
              (wait* (or wait 0.0)))
        (run-at-time wait* nil #'my-queuexecution-run (cl-rest queue)))))))

;;;###autoload
(defun my-queuexecution-add (func-lst wait to-queue)
  (-let* ((new-queue
           (-map
            (lambda (func)
              (my-queuexecution-make-element func wait))
            func-lst)))
    (append to-queue new-queue)))

;;;###autoload
(defun my-queuexecution-make-and-run (func-lst wait)
  "Make a queue to execute FUNC-LST sequentially.
Each execution stands apart from each other WAIT seconds."
  (--> (my-queuexecution-add func-lst wait) (my-queuexecution-run it)))

;;; my-queuexecution.el ends here

(provide 'my-queuexecution)

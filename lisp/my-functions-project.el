;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'project)
(require 'f)

;;;###autoload
(defun my-project?-name (&optional root-of-project)
  ;; (when-let ((root-of-project (or root-of-project (my-project-root))))
  ;;   (file-name-nondirectory (directory-file-name root-of-project)))
  (-some--> (project-current) (project-name it)))

;;;###autoload
(cl-defun my-project-tracked-file-list (&optional (root-dir (my-project-root)))
  (dlet ((default-directory root-dir))
    (my-for
      [file (process-lines-ignore-status "git" "ls-files")]
      (file-name-concat root-dir file))))

;;;###autoload
(defun my-project-open-files-in-background-once (&rest _)
  "Prepare a project-wide `Imenu'."
  (interactive)
  (unless (member (my-project-root) my-project-fully-opened-list)
    (my-project-open-files-in-background)
    (add-to-list 'my-project-fully-opened-list (my-project-root))))

;;;###autoload
(defun my-project-open-files-in-background ()
  "Open/find all source files in the background which are tracked by Git."
  (my-with-deferred-gc
   (dlet ((enable-local-variables nil))
     (my-with-mode/s 'recentf-mode nil
       (save-window-excursion
         (dolist (file (my-project-tracked-file-list))
           (unless (find-buffer-visiting file)
             (ignore-errors
               (with-current-buffer (find-file-noselect file 'nowarn)
                 (if (provided-mode-derived-p major-mode 'prog-mode)
                     (bury-buffer)
                   (kill-buffer)))))))))))

;;;###autoload
(defun my-project-imenu (&optional open-all)
  (interactive "P")
  (when open-all (my-project-open-files-in-background-once))
  (consult-imenu-multi))

;;;###autoload
(cl-defun my-project-file-and-directory-list (&optional (root-dir (my-project-root)))
  "Get the list of files and directories of ROOT-DIR which are not
ignored by the VCS."
  (when-let ((default-directory root-dir))
    (my-with-deferred-gc
     ;; `project-files' is faster for small projects
     ;; `fd' is faster for large projects
     ;; combine the two approaches for a balanced compromise
     (append
      (my-eval-until-no-error
       (-map
        #'file-relative-name
        (project-files (project-current nil root-dir) (list root-dir)))
       (projectile-project-files root-dir))
      (my-findutil-dir-list root-dir)))))

(defvar my-project-fully-opened-list '(nil)
  "List of project paths, each has all tracked files opened, count \
non-vc projects (nil) as opened.")

;;;###autoload
(defun my-project-buffer-name (what &optional root path)
  "Return a buffer name from WHAT with project name to distinguish.
Use ROOT as project root, else find from PATH."
  (dlet ((default-directory
          (or (and path (or (my-project-root path) path))
              root
              default-directory)))
    (project-prefixed-buffer-name what)))

;;;###autoload
(defun my-project-remember-current-h (&rest _)
  (when-let ((p (project-current)))
    (project-remember-project p)))

;;;###autoload
(defun my-local-directories->projects (directories &optional abbr)
  ;; avoid TRAMP from asking for passwords
  (dlet ((file-name-handler-alist nil))
    (-->
     directories
     ;; (-filter #'file-directory-p it)
     (-keep
      (lambda (dir)
        ;; dir may have become a file
        (ignore-errors
          (my-project-root-maybe dir)))
      it)
     (if abbr
         (-map #'abbreviate-file-name it)
       it))))

(defvar my-recent-project-ignore-ancestor-directory-list
  '(my-useelisp-repositories-dir/))

;;;###autoload
(defun my-recentf-directories->project-roots ()
  (-let* ((ignore-dirs
           (-map
            (lambda (dir)
              (-->
               (cond
                ((symbolp dir)
                 (symbol-value dir))
                (:else
                 dir))
               (file-truename it) (file-name-as-directory it)))
            my-recent-project-ignore-ancestor-directory-list)))
    (-->
     (my-recent-directory-list)
     (my-local-directories->projects it)
     (delete-dups it)
     (-map #'file-truename it)
     (my-uniqify-directory-list it)
     (-remove
      (lambda (dir)
        (-some
         (lambda (parent-dir) (string-prefix-p parent-dir dir)) ignore-dirs))
      it)
     (-map #'abbreviate-file-name it))))

;;;###autoload
(defun my-project-remember-root (root-dir)
  "(`project-remember-project' (... ROOT-DIR) NO-WRITE!)."
  (project-remember-project
   (project-current nil root-dir)
   'no-write))

;;;###autoload
(defun my-project-remember-recent-roots ()
  (interactive)
  (my-with-deferred-gc
   (-let* ((known-roots0 (project-known-project-roots))
           (known-roots (-map #'abbreviate-file-name known-roots0))
           (recent-roots (my-recentf-directories->project-roots))
           (new-roots (-difference recent-roots known-roots)))
     (mapc #'my-project-remember-root new-roots)
     ;; write
     (unless (equal known-roots0 (project-known-project-roots))
       (-some--> (project-current) (project-remember-project it nil))))
   (my-project-normalize-known-roots)))

;;;###autoload
(defun my-project-normalize-known-roots (&optional save)
  (setq project--list
        (my-for [(dir . tail) project--list]
          (cons (abbreviate-file-name dir)
                tail)))
  (when save (project--write-project-list)))

;;;###autoload
(defun my-project-create-empty-project-marker-file ()
  (interactive)
  (-let* ((file (cl-first my-project-marker-file-list)))
    (unless (file-exists-p file)
      (delete-file file))
    (make-empty-file file)
    (my-project-clear-all-root-cache)
    (ignore-errors (projectile-invalidate-cache))
    (message "`my-project-create-empty-project-marker-file': %s" file)
    (expand-file-name file)))

;;;###autoload
(defun my-project-switch ()
  (interactive)
  (my-project-remember-recent-roots)
  (-some--> (funcall project-prompter) (find-file it)))

;;;###autoload
(defun my-project-switch-project-and-find-file ()
  (interactive)
  (dlet ((project-switch-commands #'project-find-file))
    (call-interactively #'project-switch-project)))

;;;###autoload
(cl-defun my-filename-path-relative-to-project (&optional abbrev-components prj-root filepath)
  (-when-let* ((prj-root (or prj-root (my-project-root)))
               (filepath
                (-some-->
                    (or filepath
                        buffer-file-name
                        (my-buffer-filename))
                  (directory-file-name it)))
               (rlt-ancestors
                (-->
                 filepath
                 (file-name-parent-directory it)
                 (file-relative-name it prj-root)
                 (if abbrev-components
                     (-->
                      it (file-name-split it)
                      (-map
                       (lambda (str)
                         (if (< 0 (length str))
                             (substring str 0 1)
                           str))
                       it)
                      (apply #'file-name-concat it))
                   it))))
    (file-name-concat rlt-ancestors (file-name-nondirectory filepath))))

;;;###autoload
(defun my-project-clear-all-root-cache ()
  (interactive)
  (dolist (buf (buffer-list))
    (-let* ((prj-root (-some--> (project-current) (project-root it))))
      ;; clear `project-try-vc''s cache
      (when prj-root
        (vc-file-setprop prj-root 'project-vc nil))
      (vc-file-setprop default-directory 'project-vc nil)
      (with-current-buffer buf
        (kill-local-variable 'my-project-current-cache-a--cache)))))


;;; my-functions-project.el ends here

(provide 'my-functions-project)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(progn
  (defun my-setup-org-roam ()

    (when (fboundp #'org-roam-capture)

      (setq org-roam-directory
            (expand-file-name "~/Documents/knowledge/org-roam/"))

      (defun my-org-roam--bind-key-org-mode (&rest args)
        (apply #'my-bind :map 'org-mode-map :from 'org-roam args))

      (my-org-roam--bind-key-org-mode "M-l A" #'org-roam-alias-remove)
      (my-org-roam--bind-key-org-mode "M-l F" #'org-roam-ref-remove)
      (my-org-roam--bind-key-org-mode "M-l T" #'org-roam-tag-remove)
      (my-org-roam--bind-key-org-mode "M-l a" #'org-roam-alias-add)
      (my-org-roam--bind-key-org-mode "M-l i" #'org-roam-node-insert)
      (my-org-roam--bind-key-org-mode "M-l l" #'org-roam-buffer-toggle)
      (my-org-roam--bind-key-org-mode "M-l r" #'org-roam-ref-add)
      (my-org-roam--bind-key-org-mode "M-l t" #'org-roam-tag-add)

      ;; https://www.orgroam.com/manual.html#Full_002dtext-search-with-Deft
      (setq deft-recursive t)
      (setq deft-use-filter-string-for-filename t)
      (setq deft-default-extension "org")
      (setq deft-directory org-roam-directory)

      (advice-add #'deft-parse-title :override #'my-deft-org-roam-parse-title)
      (advice-add
       #'deft-parse-summary
       :override #'my-deft-org-roam-parse-summary)
      (with-eval-after-load 'deft
        (setq deft-separator " "))

      ;; (my-autoload-all-commands-from-library 'org-roam)

      (with-eval-after-load 'consult-notes
        (consult-notes-org-roam-mode)))))




;; [Title] :tag1:...:tag \t [Content without headers]

;; (defalias #'my-org-roam-load-when-its-directory-h
;;   (my-make-hook-function-defer-until-visible
;;    (lambda ()
;;      (when (and buffer-file-name
;;                 (f-ancestor-of-p org-roam-directory buffer-file-name))
;;        (require 'org-roam)
;;        (remove-hook
;;         'org-mode-hook 'my-org-roam-load-when-its-directory-h)))))
;; (add-hook 'org-mode-hook 'my-org-roam-load-when-its-directory-h)



;; (with-eval-after-load 'org-roam
;;   (make-directory org-roam-directory 'parents))

(setq org-roam-node-display-template
      (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))

;; https://www.orgroam.com/manual.html#Completing-anywhere
(setq org-roam-completion-everywhere t)
;; https://www.orgroam.com/manual.html#Performance-Optimization
(setq org-roam-db-gc-threshold most-positive-fixnum)

(org-roam-db-autosync-mode)

;; type new name by default
(add-hook 'vertico-multiform-commands '(org-roam-capture (vertico-preselect . prompt)))
(add-hook 'vertico-multiform-commands '(org-roam-tag-add grid))

(advice-add #'org-roam-tag-completions :around #'my-org-roam-tag-completions--filter-a)

;;; my-after-org-roam.el ends here

(provide 'my-after-org-roam)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-tab-bar-switch-to-next-tab-or-new-tab (&optional arg)
  (interactive "P")
  (-let* ((tabs (funcall tab-bar-tabs-function))
          (current-tab-name (map-nested-elt tabs '(current-tab name))))
    (cond
     ((< 1 (length tabs))
      (tab-bar-switch-to-next-tab arg))
     (t
      (tab-bar-duplicate-tab arg)
      (print (format "Tab %s duplicated." current-tab-name))))))

(defun my-tab-bar--icon-for-buffer ()
  (-let* ((icon (all-the-icons-icon-for-buffer))
          (face (get-text-property 0 'face icon)))
    (propertize icon 'face (map-delete face :height))))

(defun my-tab-bar-current-tab-index ()
  (-let* ((tabs (frame-parameter nil 'tabs)))
    (-find-index (lambda (tab)
                   (equal (car tab) 'current-tab))
                 tabs)))

;;;###autoload
(defun my-tab-bar-tab-name ()
  (format " %s [%s] "
          (tab-bar-tab-name-truncated)
          (my-tab-bar-current-tab-index)))

(provide 'my-functions-tab-bar)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-edit-clipboard-major-mode nil)

;;;###autoload
(defun my-edit-clipboard-major-mode ()
  my-edit-clipboard-major-mode)

;;;###autoload
(defun my-edit-clipboard-ensure-setting-major-mode (&optional interact)
  (interactive (list t))
  (when (or interact (null my-edit-clipboard-major-mode))
    (setq my-edit-clipboard-major-mode (my-read-file-major-mode)))
  my-edit-clipboard-major-mode)

(defun my-edit-clipboard-buffer (&optional mj-mode)
  (if mj-mode
      (with-current-buffer (get-buffer-create
                            (format
                             "*%s %s*"
                             'my-edit-clipboard my-edit-clipboard-major-mode))
        (unless (equal major-mode my-edit-clipboard-major-mode)
          (funcall my-edit-clipboard-major-mode))
        (buffer-name))
    (get-buffer-create (format "*%s*" 'my-edit-clipboard))))

(defun my-edit-clipboard-setup-buffer (&optional mj-mode)
  (-let* ((buf (my-edit-clipboard-buffer mj-mode)))
    (with-current-buffer buf
      (erase-buffer)
      (my-edit-clipboard-mode))
    buf))

(defun my-edit-clipboard-copy-and-notify (str)
  (kill-new str)
  (unless (equal str (my-clipboard-get))
    (message "`%s': clipboard not synchronized by `kill-new', using `%s'."
             'my-edit-clipboard-finish
             'my-clipboard-copy)
    (my-clipboard-copy str))
  (notifications-notify :body str))

(defun my-edit-clipboard-finish (&optional no-copy)
  (interactive)
  (-let* ((str (string-trim-right (buffer-string))))
    (unless no-copy
      (my-edit-clipboard-copy-and-notify str))
    (unless (< 0 (length (my-frame-window-buffer-file-list)))
      (delete-frame))))

(define-minor-mode my-edit-clipboard-mode
  nil
  :keymap `((,(kbd "C-c C-c") . my-edit-clipboard-finish)))

;;;###autoload
(defun my-edit-clipboard (&optional at-side)
  (interactive (list t))
  (-let* ((buf (my-edit-clipboard-setup-buffer)))
    (if at-side
        (pop-to-buffer buf)
      (switch-to-buffer buf))

    ;; `yank' may not be synchronized with the DE
    ;; (yank)

    ;; (or (gui-get-selection 'CLIPBOARD) (current-kill 0))
    (-let* ((str (my-clipboard-get)))
      (insert str))

    buf))

;;;###autoload
(defun my-edit-clipboard-format ()
  (interactive)
  (when (called-interactively-p t)
    (my-edit-clipboard-ensure-setting-major-mode))
  (save-window-excursion
    (-let* ((buf (my-edit-clipboard-setup-buffer t)))
      (with-current-buffer buf
        (-let* ((str (my-clipboard-get)))
          (insert str))
        (my-code-format-buffer
         (lambda ()
           (with-current-buffer buf
             (-let* ((str (string-trim-right (buffer-string))))

               ;; too prone to errors
               ;; (my-automation-type-insert str)

               (my-edit-clipboard-copy-and-notify str))))
         (lambda (proc event)
           (-let* ((exit-code (process-exit-status proc)))
             (unless (zerop exit-code)
               (notifications-notify
                :title "Formatting failed"
                :body (list :exit-code exit-code :event event)
                :actions '("my-edit-clipboard-ensure-setting-major-mode" "Set language")
                :on-action (lambda (_id key)
                             (when (equal
                                    "my-edit-clipboard-ensure-setting-major-mode" key)
                               (make-frame)
                               (my-edit-clipboard-ensure-setting-major-mode t)
                               (delete-frame))))))))))))

;;; my-edit-clipboard.el ends here

(provide 'my-edit-clipboard)

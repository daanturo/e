;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(add-hook 'my-elisp-tree-sitter-excluded-modes 'polymode-mode)

(provide 'my-after-polymode)

;; Local Variables:
;; no-byte-compile: t
;; End:

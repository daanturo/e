;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-kaomoji-completing-read-flat ()
  (require 'insert-kaomoji)
  (-let* ((categories (-map #'car insert-kaomoji-alist))
          (kaomojis
           (-mapcat
            (-lambda ((cat . elems))
              (--map (concat it " " cat) elems))
            insert-kaomoji-alist))
          (readed
           (minibuffer-with-setup-hook (lambda ()
                                         (when (and (fboundp
                                                     #'vertico-grid-mode)
                                                    (not
                                                     (bound-and-true-p
                                                      vertico-grid-mode)))
                                           (vertico-multiform-grid)))
             (completing-read
              (format "Kaomoji (of: %s): "
                      (string-join categories " "))
              kaomojis))))
    (replace-regexp-in-string " [^ ]*?$" "" readed)))

;;;###autoload
(cl-defun my-completing-read-kaomoji (&optional prompt category-prompt &key minibuf-setup)
  (require 'insert-kaomoji)
  (-let* ((minibuf-setup-fn
           (lambda ()
             (when minibuf-setup
               (funcall minibuf-setup)))))
    (my-navistack-minibuffer-backspace-to-back-act
     (list
      (lambda (&rest _)
        (-let* ((annot-cat-fn
                 (lambda (cat)
                   (-->
                    cat
                    (my-get-in* insert-kaomoji-alist it)
                    (cl-first it)
                    (propertize it 'face 'shadow)
                    (concat " " it)))))
          (minibuffer-with-setup-hook (lambda () (funcall minibuf-setup-fn))
            (completing-read
             (or (bound-and-true-p category-prompt) "Kaomoji category: ")
             (lambda (string pred action)
               (if (eq action 'metadata)
                   `(metadata
                     (annotation-function . ,annot-cat-fn)
                     (category . my-insert-kaomoji-category))
                 (complete-with-action
                  action (-map #'car insert-kaomoji-alist) string pred)))))))
      (lambda (category &rest _)
        (-let* ((cands (my-get-in* insert-kaomoji-alist category)))
          (minibuffer-with-setup-hook (lambda () (funcall minibuf-setup-fn))
            (completing-read
             (or prompt "Kaomoji (backspace to category): ")
             (lambda (string pred action)
               (if (eq action 'metadata)
                   `(metadata (category . my-insert-kaomoji))
                 (complete-with-action action cands string pred)))))))))))

(with-eval-after-load 'vertico-multiform
  ;; `vertico-posframe' sometimes draws the frame smudged
  (setf (alist-get 'my-insert-kaomoji-category vertico-multiform-categories)
        '(grid
          buffer
          ;; posframe
          (vertico-grid-annotate . 1) (vertico-count . 16)))
  (setf (alist-get 'my-insert-kaomoji vertico-multiform-categories)
        '(grid
          buffer
          ;; posframe
          (vertico-count . 16))))

;;;###autoload
(defun my-copy-kaomoji-to-clipboard (&optional zoom-face)
  (interactive)
  ;; TODO: consider `transient'
  (-let* ((face-h (face-attribute 'default :height))
          (kaomoji
           (my-completing-read-kaomoji
            nil nil
            :minibuf-setup (lambda ()
                             ;; (text-scale-adjust 7)
                             ;; (when (fboundp #'vertico--exhibit)
                             ;;   (vertico--exhibit))
                             ;;
                             (my-safe-call #'hide-mode-line-mode)
                             (when (bound-and-true-p vertico-posframe-mode)
                               (set-face-attribute 'default (selected-frame) :height face-h))
                             (when zoom-face
                               (set-face-attribute 'default (selected-frame)
                                                   :height (if (bound-and-true-p vertico-buffer-mode)
                                                               125
                                                             150)))
                             ;;
                             ))))
    (my-clipboard-kill-new-or-copy kaomoji)
    kaomoji))


;;; my-functions-emoticon.el ends here

(provide 'my-functions-emoticon)

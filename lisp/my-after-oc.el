;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; https://github.com/minad/vertico/issues/261 , completion doesn't stop if just
;; use "<enter>"

;; TODO: report upstream to org, or propose such that entering an already added key
;; stops the completion
(advice-add
 #'org-cite-basic--complete-key
 :around #'my-org-cite-basic--complete-key--vertico-a)


;;; my-after-oc.el ends here

(provide 'my-after-oc)

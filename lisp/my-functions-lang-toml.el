;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'treesit)

;;;###autoload
(defun my-lang-toml-parse-file->json (file &rest json-parse-string-args)
  (-let* (((&plist :exit :out)
           (my-process-sync-run
            (list "taplo" "get" "-f" (expand-file-name file) "-o" "json") t)))
    (if (= 0 exit)
        (apply #'json-parse-string out json-parse-string-args)
      (error "%s" out))))

;;;###autoload
(defun my-lang-toml-treesit-matching-table-node-p (node0 table-name-regexp)
  (and
   ;; node type is "table"
   (member (treesit-node-type node0) '("table"))
   ;; its direct 1st named child matches `table-name-regexp'
   (string-match-p
    table-name-regexp (treesit-node-text (treesit-node-child node0 0 t)))))

;;;###autoload
(defun my-lang-toml-get-tables-from-buffer (table-re &rest _)
  (-let* ((root (treesit-buffer-root-node))
          (pred
           (lambda (node)
             (my-lang-toml-treesit-matching-table-node-p node table-re)))
          (1st-table-node (treesit-search-subtree root pred))
          (table-nodes
           (named-let
               recur ((cur-node 1st-table-node) (node-coll (list 1st-table-node)))
             (-let* ((next-node (treesit-search-forward cur-node pred)))
               (cond
                ((null next-node)
                 (nreverse node-coll))
                (next-node
                 (recur next-node (cons next-node node-coll))))))))
    (cl-loop
     for node in table-nodes collect (my-treesit-children-texts node t))))

;;;###autoload
(defun my-lang-toml-get-tables-from-string (str table-re &rest args)
  (with-temp-buffer
    (insert str)
    (treesit-parser-create 'toml)
    (apply #'my-lang-toml-get-tables-from-buffer table-re args)))

(provide 'my-functions-lang-toml)

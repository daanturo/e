;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-eww-other-window-noselect (&optional url)
  "Open URL in other window using `eww' without selecting it."
  (interactive)
  (save-selected-window
    (my-in-other-window nil
                        (if url
                            (eww url)
                          (call-interactively #'eww)))))

;;;###autoload
(defun my-browser-eww-other-frame (url)
  "Use Emacs's integrated browser to browse URL."
  (interactive (list (read-string "URL: ")))
  (select-frame (make-frame))
  (funcall-interactively #'eww url)
  (delete-other-windows))

;;;###autoload
(defun my-eww-new (url-or-query)
  "(`eww' URL-OR-QUERY t)."
  (interactive (list
                (read-string "Browse: "
                             (my-region-string-maybe t t)
                             'eww-prompt-history)))
  (eww url-or-query t))


;;; my-functions-eww.el ends here

(provide 'my-functions-eww)

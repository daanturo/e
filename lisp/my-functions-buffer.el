;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-kill-other-file-buffers (&optional kill-special-buffers no-confirm)
  (interactive "P")
  (my-kill-multiple-buffers
   (remove
    (current-buffer)
    (-filter
     (if kill-special-buffers
         #'always
       #'buffer-file-name)
     (buffer-list)))
   no-confirm))

;;;###autoload
(defun my-kill-buffer-no-ask (buf)
  (-let* ((buf (or buf (current-buffer))))
    (with-current-buffer buf
      (set-buffer-modified-p nil)
      (dlet ((kill-buffer-query-functions nil))
        (kill-buffer buf)))))

;;;###autoload
(defun my-kill-multiple-buffers (buffers-to-kill &optional yes)
  (interactive (list (my-completing-read-multiple-buffers "Kill: ") nil))
  (when (or yes
            (y-or-n-p (format "Kill %s buffers:\n%s"
                              (length buffers-to-kill)
                              buffers-to-kill)))
    (dolist (buf buffers-to-kill)
      (condition-case _
          (kill-buffer buf)
        (error (kill-buffer (get-buffer buf)))))))

;;;###autoload
(defun my-get-displayed-buffer-list ()
  (-let* ((frames (-filter #'frame-visible-p (frame-list))))
    (-->
     frames
     (-mapcat #'window-list it)
     (-filter #'window-live-p it)
     (-map #'window-buffer it)
     (-uniq it))))

;;;###autoload
(defun my-kill-other-undisplayed-buffers ()
  (interactive)
  (-let* ((bufs-to-keep (my-get-displayed-buffer-list))
          (bufs-to-kill (-difference (buffer-list) bufs-to-keep)))
    (when (y-or-n-p
           (format "Kill %s buffers except %s: %S"
                   (length bufs-to-kill)
                   (length bufs-to-keep)
                   (-map #'buffer-name bufs-to-keep)))
      (my-kill-multiple-buffers bufs-to-kill 'yes))))

;;;###autoload
(defun my-make-clone-buffer-by-contents (&optional orig-buffer name)
  (let* ((orig-buffer (or orig-buffer (current-buffer)))
         (clone-buf
          (get-buffer-create (or name
                                 (generate-new-buffer-name
                                  (my-buffer-name-string orig-buffer)))
                             t)))
    (with-current-buffer orig-buffer
      (copy-to-buffer clone-buf (point-min) (point-max))
      ;; minimize visual interruptions
      (let ((p (point))
            (header header-line-format))
        (with-current-buffer clone-buf
          (goto-char p)
          (when header
            (setq-local header-line-format "")))))
    clone-buf))

(defvar my-get-initial-buffer-choice-name--cache nil)
;;;###autoload
(defun my-get-initial-buffer-choice-name ()
  (with-memoization (plist-get my-get-initial-buffer-choice-name--cache
                               initial-buffer-choice
                               #'equal)
    (save-window-excursion
      (cond
       ((and (equal nil initial-buffer-choice)
             (equal nil inhibit-startup-screen))
        (buffer-name (fancy-startup-screen)))
       ((stringp initial-buffer-choice)
        (with-current-buffer (find-file-noselect initial-buffer-choice)
          (prog1 (buffer-name)
            (kill-current-buffer))))
       ((functionp initial-buffer-choice)
        (with-current-buffer (funcall initial-buffer-choice)
          (buffer-name)))
       ((or (equal t initial-buffer-choice)
            ;; empirically, any other values give the scratch buffer
            t)
        (buffer-name (get-scratch-buffer-create)))))))

;;;###autoload
(defun my-buffer-same-major-mode-fn (&optional derivable mmode0)
  (-let* ((mmode0 (or mmode0 major-mode)))
    (lambda (obj)
      (-when-let*
          ;; completions candidates being able to be cons cell!
          ((buf (-some--> (or (car-safe obj) obj) (get-buffer it)))
           (mmode1 (buffer-local-value 'major-mode buf)))
        (cond
         (derivable
          (or (provided-mode-derived-p mmode0 mmode1)
              (provided-mode-derived-p mmode1 mmode0)))
         (t
          (equal mmode0 mmode1)))))))

;;;###autoload
(defun my-switch-to-buffer-with-same-major-mode (&optional derivable)
  (interactive "P")
  (switch-to-buffer
   (read-buffer "Switch to buffer: " nil nil (my-buffer-same-major-mode-fn derivable))))


;;; my-functions-buffer.el ends here

(provide 'my-functions-buffer)

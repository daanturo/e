;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-closed-AI-get-api-key-from-auth-source ()
  (my-auth-source-get "openai.com" "apikey"))

;; ;;;###autoload
;; (progn
;;   (defun my-setup-closed-AI ()

;;     (with-eval-after-load 'openai
;;       (setq openai-key #'my-closed-AI-get-api-key-from-auth-source))

;;     (with-eval-after-load 'chatgpt-shell
;;       (setq chatgpt-shell-openai-key
;;             #'my-closed-AI-get-api-key-from-auth-source))

;;     (with-eval-after-load 'khoj
;;       (setq khoj-openai-api-key #'my-closed-AI-get-api-key-from-auth-source))

;;     ;;
;;     ))


;;; after-closed-AI.el ends here

(provide 'my-after-closed-AI)

;; Local Variables:
;; no-byte-compile: t
;; End:

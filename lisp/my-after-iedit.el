;; -*- lexical-binding: t; -*-

(require 'iedit)

;;;###autoload
(progn
  (defun my-setup-iedit ()
    ;; Reset against Doom's config
    (my-after-each '(emacs iedit)
      (setq iedit-toggle-key-default (kbd "C-;")))
    (setq evil-multiedit-dwim-motion-keys nil) ; RET, <return> are often shadowed
    ;; The above may not work
    (with-eval-after-load 'evil-multiedit
      (setcdr evil-multiedit-mode-map nil))
    (my-bind iedit-toggle-key-default #'iedit-mode)
    (my-bind :vi 'normal "M-d" #'evil-multiedit-match-symbol-and-next)
    (my-bind :vi 'normal "M-D" #'evil-multiedit-match-symbol-and-prev)
    (my-bind :map 'iedit-mode-keymap "<C-m>" #'iedit-switch-to-mc-mode)
    (my-bind :map 'mc/keymap "<C-m>" #'my-mc-switch-to-iedit)))

;; Let me see other key bindings
(my-bind :map 'iedit-help-map "b" nil)

;; `evil-multiedit' hides `iedit-occurrence-keymap-default'
(my-bind :map 'evil-multiedit-mode-map "M-M" #'iedit-switch-to-mc-mode)

(my-bind :map 'iedit-occurrence-keymap-default doom-leader-alt-key nil)

(provide 'my-after-iedit)

;; Local Variables:
;; no-byte-compile: t
;; End:

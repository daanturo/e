;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'evil-core)
(require 'evil)

;;;###autoload
(defun my-insert-&-ESC-TAB (arg)
  "Enter insert state and perform ESC TAB in current context.
As M-TAB is usually caught by the window manager, ESC TAB can be
used instead but also caught by `evil'."
  (interactive "P")
  (progn
    (cond
     ((derived-mode-p 'special-mode)
      (forward-button (prefix-numeric-value arg)))
     ;; in source code blocks, `org-indent-line' calls the command that is
     ;; invoked by "TAB" natively, so we need to be stricter as usually it will
     ;; find the command in normal state
     ((member last-command '(evil-normal-state evil-force-normal-state)) ; (lookup-key evil-insert-state-map (kbd "<escape>"))
      ;; don't move back (insert->normal->insert)
      (if evil-move-cursor-back
          (evil-append 0)
        (evil-insert 0))
      (call-interactively #'my-complete-in-minibuffer)))))

;;;###autoload
(defun my-enable-evil ()
  (interactive)
  (when (fboundp #'evil-collection-init)
    (evil-collection-init))
  (evil-mode))

;;;###autoload
(defun my-evil-prefix-keymap-in-state (prefix keymap state)
  (-let* ((evil-map (symbol-value (intern (format "evil-%s-state-map" state))))
          (key-in-state (format "<%s-state> %s" state prefix))
          (local (if (fboundp #'keymap-lookup)
                     (keymap-lookup keymap key-in-state)
                   (lookup-key keymap (kbd key-in-state))))
          (global (if (fboundp #'keymap-lookup)
                      (keymap-lookup evil-map prefix)
                    (lookup-key evil-map (kbd prefix)))))
    (unless (-some 'numberp (list local global))
      (make-composed-keymap local global))))

;;;###autoload
(defun my-native-command-when-fail--a (evil-command &rest args)
  (condition-case _
      (apply evil-command args)
    ((user-error)
     (let ((native-command (my-evil-non-vi-command (this-command-keys-vector))))
       (message "Current key: %s, %s failed, calling `%s' instead."
                (key-description (this-command-keys-vector))
                evil-command
                native-command)
       (call-interactively native-command)))))

;;;###autoload
(defun my-evil-non-vi-command (key)
  "Return the command that KEY invokes in Emacs state."
  (save-excursion
    ;; `evil-with-state' doesn't work here?
    (my-evil-save-state
     (evil-change-state 'emacs)
     (key-binding (my-ensure-kbd key)))))

;;;###autoload
(defun my-evil--visual-line-prefix-argument-after-a (&rest _)
  (when (and (called-interactively-p 'any)
             current-prefix-arg)
    (let ((col (current-column))
          (num (prefix-numeric-value current-prefix-arg)))
      (forward-line (- num (/ num (abs num))))
      (move-to-column col))))

;;;###autoload
(defun my-evil-adjust-cursor-when-visual ()
  (when (and (my-evil-motion-derived-state-p)
             (eolp))
    (backward-char)))

;;;###autoload
(progn

  (defvar evil-state nil)

  (defun my-evil-adjust-cursor-maybe-a (func &rest args)
    "Only apply when the command is `evil'-related."
    ;; Don't use `this-command' because it can be set to another value
    (when (and (symbolp this-original-command)
               (string-prefix-p "evil-" (symbol-name this-original-command)))
      (apply func args)))

  (defun my-evil-filter-native-command-when-read-only (evil-cmd)
    (if buffer-read-only
        (let ((cmd1 (my-evil-non-vi-command (this-command-keys-vector))))
          (message "Buffer is read-only, calling `%s' instead." cmd1)
          cmd1)
      evil-cmd)))

;; (defun my-evil-set-initial-state (mode state &optional lib-to-disable)
;;   (when lib-to-disable
;;     (add-hook '+evil-collection-disabled-list lib-to-disable))
;;   (with-eval-after-load 'evil
;;     (-let* ((fn (lambda (&rest _) (evil-set-initial-state mode state))))
;;       ;; need hook to be effective? TODO report
;;       (my-add-hook-once (my-mode->hook mode) fn)
;;       (funcall fn))))

;;



(defvar my-evil-force-normal-state-hook '())

;; `keyboard-escape-quit' also delete split windows
(add-hook 'my-evil-force-normal-state-hook 'keyboard-quit)
;; (add-hook 'my-evil-force-normal-state-hook 'keyboard-escape-quit)

;;;###autoload
(defun my-evil-run-force-normal-state-hook-after-a (&rest _)
  "Run `my-evil-force-normal-state-hook'."
  (run-hooks 'my-evil-force-normal-state-hook))

;;;###autoload
(defun my-evil-normal-state-no-move-cursor-back ()
  (interactive)
  (dlet ((evil-move-cursor-back nil))
    (evil-normal-state)))

;;;###autoload
(defun my-evil-state-modes ()
  (-let* ((alist (evil-state-property t :modes)))
    (-map (-lambda ((k . v)) (cons k (symbol-value v))) alist)))

;;;###autoload
(defun my-evil-insert-when-server-frame-open-file-h ()
  (when (and buffer-file-name
             (frame-parameter nil 'client)
             (evil-normal-state-p))
    (evil-insert-state)))

;;; my-functions-evil.el ends here

(provide 'my-functions-evil)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'transient)


;;;###autoload
(transient-define-prefix
  my-copy-filename () "A transient prefix for copying file name."
  ["Variants"
   [("~" "Inhibit abbreviate-file-name (~/...)" ":function expand-file-name")
    ("1"
     "Go 1 level of symlink source        "
     ":function my-file-symlink-target-1")
    ("2" "Absolute symlink source             " ":function file-truename")
    ("9"
     "No parent dir                       "
     ":function file-name-nondirectory")
    ("0" "No extension and parent dir         " ":function file-name-base")]

   [ ;; (";" "path:LINE" ":LINE")
    (";" "FILE:LINE:COLUMN" ":LINE:COLUMN")
    ;; `command-line-1'
    ("'" "Use the Emacs variant +LINE:COLUMN" "+LINE:COLUMN")]
   ;;
   ]

  [["Operations"

    ("c" "Buffer file name/path"
     (lambda ()
       (interactive)
       (my-copy-filename--copy #'my-buffer-filename)))

    ("b" "Buffer name"
     (lambda ()
       (interactive)
       (my-copy-filename--copy #'buffer-name)))

    ("d" "Current directory"
     (lambda ()
       (interactive)
       (my-copy-filename--copy default-directory)))

    ("p" "Relative to project"
     (lambda ()
       (interactive)
       (my-copy-filename--copy
        #'my-completing-read-multiple-filenames-in-project-relative-to-current)))

    ;;
    ]
   [("<escape>" "" transient-quit-one)]]

  ;;
  )

(defun my-copy-filename--transient-transform-by-args (str/s &optional trs-args)
  (-let*
      ((trs-args (or trs-args (transient-args 'my-copy-filename)))
       (transform
        (lambda (str)
          (-->
           str (s-chop-prefixes '("/sudo:root@localhost:") it)
           (cond
            ((member ":function expand-file-name" trs-args)
             it)
            (:else
             (abbreviate-file-name it)))
           (directory-file-name it) ; prevent the functions to clear whole directory name
           (funcall (apply #'-compose
                           (-keep
                            (lambda (arg)
                              (and (string-prefix-p ":function " arg)
                                   (read
                                    (string-remove-prefix ":function " arg))))
                            trs-args))
                    it)
           (cond
            ((file-directory-p it)
             (file-name-as-directory it))
            (:else
             it))
           (cond
            ((member ":LINE:COLUMN" trs-args)
             (format "%s:%d:%d" str (line-number-at-pos) (current-column)))
            ((member "+LINE:COLUMN" trs-args)
             (format "%s+%d:%d" str (line-number-at-pos) (current-column)))
            (:else
             it))))))
    (cond
     ((listp str/s)
      (-map transform str/s))
     ((stringp str/s)
      (funcall transform str/s)))))

(defun my-copy-filename--copy (src)
  (-let* ((filenames
           (cond
            ((functionp src)
             (funcall src))
            ((symbolp src)
             (symbol-value src))
            (:else
             src)))
          (str
           (-->
            filenames (my-copy-filename--transient-transform-by-args it)
            (cond
             ((listp it)
              (string-join it "\n"))
             (:else
              it)))))
    (kill-new str)
    (message "Copied: %S" str)))

;;; my-copy-filename.el ends here

(provide 'my-copy-filename)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-Buffer-menu-kill-buffer-at-point-now ()
  (interactive)
  (kill-buffer (Buffer-menu-buffer t))
  (revert-buffer))


;;; my-functions-buff-menu.el ends here

(provide 'my-functions-buff-menu)

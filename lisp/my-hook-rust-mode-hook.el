;; -*- lexical-binding: t; -*-

(require 'dash)

(add-hook 'my-execu-cmd-alist '(rust-mode . my-execu-cmd-lang-rust))

(my-evalu-set-handler 'rust-mode '("evcxr"))

(defun my-lsp-rust-analyzer-debug-require-dap-cpptools-a (fn &rest args)
  (condition-case err
      (apply fn args)
    ((user-error)
     (message "`my-lsp-rust-analyzer-debug-require-dap-cpptools-a'!")
     (require 'dap-cpptools)
     (apply fn args))))
(advice-add
 #'lsp-rust-analyzer-debug
 :around #'my-lsp-rust-analyzer-debug-require-dap-cpptools-a)

;; dependencies-aware?
(setq rust-ts-flymake-command '("cargo" "clippy"))

(provide 'my-hook-rust-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

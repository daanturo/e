;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defun my-epkg-sort-by-stars ()
  (interactive)
  (tabulated-list-sort (-find-index (-lambda ((_ _ sort . _))
                                      (equal sort 'epkg-list-sort-by-stars))
                                    epkg-list-columns)))
(my-add-mode-hook-and-now 'epkg-list-mode
  'my-epkg-sort-by-stars)

(my-bind :map 'epkg-list-mode-map
  '("M-RET" "M-<return>") #'my-epkg-visit-package-home-page|website-at-point)

;; ignore Emacs Wiki packages
(cl-pushnew 'wiki epkg-list-exclude-types)

(setq epkg-repository (expand-file-name epkg-repository))

;;;###autoload
(progn
  (add-hook
   'my-unkit-after-update-hook
   #'my-epkg-update-by-delete-and-re-shallow-clone))

(provide 'my-after-epkg)

;; Local Variables:
;; no-byte-compile: t
;; End:

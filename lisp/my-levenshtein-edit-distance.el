;; -*- lexical-binding: t; -*-

(require 'dash)

;;; Commentary:
;; 

(require 'seq)


;;; Code:

;;;###autoload
(defun my-levenshtein-distance (s0 s1 &optional case-insensitive)
  "Compute the Levenshtein distance between S0 and S1.
Non-nil CASE-INSENSITIVE: ignore cases. I didn't know about
`string-distance' when defining this."
  (cond
   (case-insensitive
    (my-levenshtein-distance (downcase s0) (downcase s1)))
   (t
    (my-levenshtein-distance--compute s0 s1))))

(defun my-levenshtein-distance--compute (s0 s1 )
  (-let* ((l0 (length s0))
          (l1 (length s1))
          ;; mat[x][y] = lev(s0[0..x-1][0..y-1])
          (mat (my-make-tensor `[,(1+ l0)
                                 ,(1+ l1)]
                               0)))
    ;;lev(s0,"") = |s0|
    (dolist (i (-iota (1+ l0)))
      (setf (my-in mat i 0) i))
    ;;lev("",s1) = |s1|
    (dolist (i (-iota (1+ l1)))
      (setf (my-in mat 0 i) i))
    (dolist (r (-iota l0 1))
      (dolist (c (-iota l1 1))
        (--> (if (= (aref s0 (- r 1))
                    (aref s1 (- c 1)))
                 ;; same char, no increase
                 (my-in mat (1- r) (1- c))
               (+ 1 (min (my-in mat (1- r) (1- c))
                         (my-in mat (1- r) c)
                         (my-in mat r (1- c)))))
             (setf (my-in mat r c) it))))
    (my-in mat l0 l1)))

(provide 'my-levenshtein-edit-distance)

;;; my-levenshtein-edit-distance.el ends here

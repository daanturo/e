;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;; (add-hook 'my-evil-or-leader-mode-alist '(gptel-mode nil))
(add-hook 'gptel-mode-hook #'my-evil-force-insert-state-maybe-h)

(my-bind :map 'gptel-mode-map '("M-RET" "M-<return>") #'gptel-send)

;;; my-after-gptel.el ends here

(provide 'my-after-gptel)

;; Local Variables:
;; no-byte-compile: t
;; End:

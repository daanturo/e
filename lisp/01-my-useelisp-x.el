;; -*- lexical-binding: t; -*-

(require 'dash)

(require '00-my-useelisp)

;;; List installed symlinks

;;;###autoload
(defun my-useelisp-install-all-registered ()
  (-uniq my-useelisp--registered-source-directories)
  (prog1 (mapcar (lambda (args)
                   (apply #'my-useelisp-install
                          (my-mixed-plist-put args :install t)))
                 my-useelisp--registered-source-directories)
    (-uniq my-useelisp--registered-symlinks)))

;;;###autoload
(defun my-useelisp-registered-symlinks ()
  "See `my-useelisp--registered-symlinks'."
  (unless (car my-useelisp--registered-symlinks)
    (my-useelisp-install-all-registered))
  my-useelisp--registered-symlinks)

;;; Update

(defun my-useelisp-refresh--keep-p (abbr-path)
  (cond ((string-suffix-p ".elc" abbr-path)
         (my-useelisp-refresh--keep-p (string-remove-suffix "c" abbr-path)))
        (t
         (and
          (let* ((target (my-file-symlink-target-1 abbr-path)))
            (file-exists-p target))
          (or
           ;; registered
           (member abbr-path (my-useelisp-registered-symlinks))
           ;; not a symlink
           (not (file-symlink-p abbr-path)))))))

;;;###autoload
(defun my-useelisp-refresh-dir (dir)
  "Ensure DIR's byte-compiled elisp files are updated."
  ;; clear ".el", then ".elc"
  (dotimes (_ 2)
    (dolist (file
             (mapcar
              #'abbreviate-file-name
              (directory-files dir 'full directory-files-no-dot-files-regexp)))
      (unless (or (my-useelisp-refresh--keep-p file)
                  (my-useelisp-refresh--keep-p
                   (replace-regexp-in-string "\\.elc\\'" ".el" file)))
        (my-println🌈 #'my-useelisp-refresh ": deleting" file)
        (delete-file file))))
  (my-elisp-recompile-directory dir nil 'verbose)
  (my-useelisp-ensure-autoload nil 'update dir))

;;;###autoload
(defun my-useelisp-refresh ()
  "Delete unused files in `my-useelisp-build-dir'."
  (interactive)
  (dolist (dir my-useelisp--build-dirs)
    (my-useelisp-refresh-dir dir)))

;;;###autoload
(defun my-useelisp-update-repo (package-name)
  (let ((dir (my-filename-join my-useelisp-repositories-dir/ package-name)))
    ;; "git fetch" will
    (when (file-directory-p (my-filename-join dir ".git"))
      (dlet ((default-directory dir))
        (let ((prev-commit (my-vc-git-get-current-revision-commit-hash)))
          (let ((buf (generate-new-buffer (format "*Update %s*" dir))))
            (let* ((cmd-func
                    (lambda ()
                      (let ((cmd "time git pull"))
                        (my-println🌈 "In" dir ":" cmd)
                        cmd)))
                   (finish-func
                    (lambda (&optional _proc _event)
                      (dlet ((default-directory dir))
                        (let ((commit
                               (my-vc-git-get-current-revision-commit-hash)))
                          (unless (string= prev-commit commit)
                            (my-println🌈
                             (f-filename dir)
                             ":"
                             "from"
                             prev-commit
                             "to"
                             commit))))))
                   (shell-cmd-out-buf
                    (cond
                     (noninteractive
                      nil)
                     (:else
                      buf))))
              (with-current-buffer buf
                (insert "\n" (shell-command-to-string (funcall cmd-func)) "\n"))
              (funcall finish-func))))))))

;;;###autoload
(defun my-useelisp-update-all-repos ()
  (interactive)
  (dolist (l my-useelisp-repos--registered-list)
    (unless (plist-get l :rev)
      (my-useelisp-update-repo (car l)))))

;;;###autoload
(defun my-useelisp-update-all-files ()
  (interactive)
  (dolist (args0 my-useelisp-files--registered-list)
    (let ((args (my-mixed-plist-put args :install t)))
      (my-println🌈 #'my-useelisp-file args)
      (apply #'my-useelisp-file args))))

;;;###autoload
(defun my-useelisp-update-all (&rest _)
  (interactive)
  (my-useelisp-update-all-files)
  (my-useelisp-update-all-repos))

(provide '01-my-useelisp-x)

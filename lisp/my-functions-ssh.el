;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; ;; looks like not needed, even systemd services inherit SSH_AUTH_SOCK?
;; (require 'ssh-agency)

;;;###autoload
(defun my-ssh-directory ()
  "Return the SSH directory.
Maybe in the future, some SSH implementations will adhere to the
XDG conventions?"
  "~/.ssh")

(my-multisession 'my-ssh-default-key)

;;;###autoload
(defun my-ssh-read-keys ()
  (-let* ((ssh-dir (my-ssh-directory))
          (retval
           (my-completing-read-multiple-filenames
            "`my-ssh-agent-add-keys': "
            ssh-dir
            (my-multisession 'my-ssh-default-key)
            nil
            nil
            (lambda (filename)
              (not (member (file-name-extension filename) '("pub")))))))
    (when retval
      (my-multisession 'my-ssh-default-key :set retval))
    retval))


;; Also: https://stackoverflow.com/a/62278407/8051562
;; git config --add --local core.sshCommand 'ssh -i /path/to/ssh-key/'

;;;###autoload
(cl-defun my-ssh-agent-add-keys (keys &key interact synchronous)
  (interactive (progn
                 (cl-assert (getenv "SSH_AUTH_SOCK"))
                 (list (my-ssh-read-keys) :interact t)))
  (when interact
    (message "Adding: %s" keys))
  (-let* ((cmdargs `("ssh-add" ,@keys)))
    (cond
     (synchronous
      (apply #'call-process (car cmdargs) nil nil interact (cdr cmdargs)))
     (:else
      (my-process-async
       cmdargs
       (lambda (stdout &rest _)
         (message "`my-ssh-agent-add-keys' output: %S" stdout)))))))

;;;###autoload
(cl-defmacro my-ssh-with-batch-mode (&rest body)
  (declare (debug t) (indent defun))
  ;; beware that gcr-ssh-agent does not support custom SSH_ASKPASS /
  ;; SSH_ASKPASS_REQUIRE / BatchMode
  ;; https://gitlab.gnome.org/GNOME/gcr/-/issues/117 ; check if SSH_AUTH_SOCK is
  ;; "/run/user/*/gcr/ssh"
  `(with-environment-variables (("GIT_SSH_COMMAND" "ssh -o BatchMode=yes"))
     ,@body))

;;;###autoload
(defun my-ssh-with-batch-mode-a (func &rest args)
  (my-ssh-with-batch-mode (apply func args)))

;;; my-functions-ssh.el ends here

(provide 'my-functions-ssh)

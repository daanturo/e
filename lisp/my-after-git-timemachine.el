;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(add-hook 'git-timemachine-mode-hook #'font-lock-mode)

;;; my-after-git-timemachine.el ends here

(provide 'my-after-git-timemachine)

;; Local Variables:
;; no-byte-compile: t
;; End:

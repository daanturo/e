;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'consult)

;;;###autoload
(defun my-consult-async-split-initial ()
  "Seel also: `consult--async-split-initial'"
  (-->
   (alist-get consult-async-split-style consult-async-split-styles-alist)
   (or (plist-get it :initial) "")))

;;;###autoload
(defun my-consult-async-split-separator ()
  (--> (alist-get consult-async-split-style consult-async-split-styles-alist)
       (or
        (-some--> (plist-get it :initial)
          (list :initial it))
        (-some--> (plist-get it :separator)
          char-to-string
          (list :separator it)))))

;;;###autoload
(defun my-consult-async-ensure-separator ()
  (-when-let* (((type sep) (my-consult-async-split-separator)))
    (unless (cond
             ;; 1
             ((equal type :separator)
              (string-search sep
                             (minibuffer-contents)))
             ;; 2
             ((equal type :initial)
              (string-match-p (concat sep ".*?" sep)
                              (minibuffer-contents))))
      (my-insert-at-buffer-position (point-max) " " sep " "))))

;;;###autoload
(defun my-consult-async-insert-at-filter-part-maybe (str)
  (when (and (member (my-minibuffer-completion-category) '(consult-grep))
             consult-async-split-style)
    (my-consult-async-ensure-separator)
    (-if-let* ((neg-pos (string-match-p (my-completion-style-negater-regexp)
                                        (buffer-string))))
        (my-insert-at-buffer-position (+ 1 neg-pos) (concat " " str))
      (my-insert-at-buffer-position (point-max) str))))

;;;###autoload
(defun my-consult-switch-to-buffer-same-major-mode (&optional derivable)
  (interactive "P")
  (-let* ((source
           `(:category
             buffer
             :state ,#'consult--buffer-state
             :items ,(-filter
                      (my-buffer-same-major-mode-fn
                       (not derivable))
                      (-map #'buffer-name (buffer-list))))))
    (consult-buffer (list source))))

;;;###autoload
(defun my-consult-affe-make-find-file-state (&optional no-enter)
  (-let* ((dir default-directory)
          (file-preview-state (consult--file-preview)))
    (lambda (action cand)
      (cond
       ;; beware of this, as the caller may only expects the callee to return
       ;; without handling the ENTER key, therefore double `find-file' may
       ;; happen
       ((and cand (eq action 'return) (not no-enter))
        (find-file cand))
       (t
        (funcall file-preview-state action cand))))))

;;;###autoload
(define-minor-mode my-consult-buffer-sort-recency-mode
  "See https://github.com/minad/consult/discussions/892."
  :global t)

(defun my-consult-buffer-sort-recency--sort (buffer-lst)

  ;; (-let* ((def (other-buffer)))
  ;;   (cons def (delete def buffer-lst)))

  ;; ;; `ibuffer-do-sort-by-recency'
  ;; (cl-sort
  ;;  buffer-lst
  ;;  (-not #'time-less-p)
  ;;  :key (lambda (buf) (or (buffer-local-value 'buffer-display-time buf) 0)))

  (-let* ((exist-table (make-hash-table :test #'equal))
          (otb0 (other-buffer))
          (recent-bufs
           (progn
             (puthash otb0 t exist-table)
             (named-let
                 recur ((retval (list otb0)))
               (-let* ((buf (other-buffer (car retval))))
                 (cond
                  ((or (null buf) (gethash buf exist-table))
                   (--> retval (delete nil it) (reverse it)))
                  (:else
                   (puthash buf t exist-table)
                   (recur (cons buf retval)))))))))
    (delete-dups (append recent-bufs buffer-lst)))

  ;;
  )

;;;###autoload
(defun consult--buffer-sort-my-pref (buffer-lst)
  "BUFFER-LST."

  (cond

   ((not my-consult-buffer-sort-recency-mode)
    ;; default, see `consult--source-buffer'
    (consult--buffer-sort-visibility buffer-lst))

   (:else
    (my-consult-buffer-sort-recency--sort buffer-lst))))

;;;###autoload
(defun my-affe-orderless-regexp-compiler (input _type _ignorecase)
  (declare)
  ;; https://github.com/minad/affe?tab=readme-ov-file#installation-and-configuration
  (setq input (cdr (orderless-compile input)))
  (cons input (apply-partially #'orderless--highlight input t)))

;;; my-functions-consult.el ends here

(provide 'my-functions-consult)

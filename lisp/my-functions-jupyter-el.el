;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'jupyter)

;;;###autoload
(defun my-jupyter-ensure-repl-buffer (&optional kernel-name display client-callback)
  (interactive)
  (cond
   ((-let* ((_ (bound-and-true-p jupyter-current-client))
            (repl-buf
             (ignore-errors
               (jupyter-with-repl-buffer
                   jupyter-current-client (current-buffer)))))
      ;; jupyter-repl-pop-to-buffer
      (when display
        (pop-to-buffer repl-buf))
      repl-buf))
   (:else
    (-let* ((avail-repls (jupyter-repl-available-repl-buffers))
            (buf0 (current-buffer)))
      (when (seq-empty-p avail-repls)
        (-let* ((client
                 (cond
                  (kernel-name
                   (jupyter-run-repl kernel-name nil t nil display))
                  (:else
                   (call-interactively #'jupyter-run-repl)))))
          (when client-callback
            (funcall client-callback client))))
      (-when-let* ((avail-repls (jupyter-repl-available-repl-buffers))
                   (_ (not (seq-empty-p avail-repls)))
                   (repl-buf (nth 0 avail-repls)))
        (with-current-buffer buf0
          (setq-local jupyter-current-client
                      (buffer-local-value 'jupyter-current-client repl-buf)))
        (when display
          (pop-to-buffer repl-buf))
        repl-buf)))))

;; ;;;###autoload
;; (defun my-jupyter-get-client (&optional kernel-name client-class)
;;   (-let* ((kernel-name (or kernel-name "python")))
;;     (let ((spec
;;            (jupyter-kernelspec-name (jupyter-guess-kernelspec kernel-name))))
;;       (jupyter-client
;;        (if jupyter-use-zmq
;;            (jupyter-kernel :spec spec)
;;          (jupyter-kernel :server (jupyter-repl-server) :spec spec))
;;        client-class))))

;; ;;;###autoload
;; (defun my-jupyter-get-repl-buffer (&optional client)
;;   (-let* ((client (or client (my-jupyter-get-client))))
;;     (oref client buffer)))

(defvar my-lang-python-jupyter-repl-client-list '())

(defvar my-lang-python-jupyter-repl-kernel-name "python")

;;;###autoload
(defun my-lang-python-jupyter-repl (&optional display)
  (interactive (list t))
  (my-jupyter-ensure-repl-buffer
   my-lang-python-jupyter-repl-kernel-name
   display
   (lambda (client) (push client my-lang-python-jupyter-repl-client-list))))

;;;###autoload
(defun my-jupyter-repl-send-string (str)
  (interactive (list
                (and (use-region-p)
                     (buffer-substring-no-properties
                      (region-beginning) (region-end)))))
  (cl-assert (stringp str))
  (with-current-buffer (my-jupyter-ensure-repl-buffer)
    (goto-char (point-max))
    (insert str)
    (jupyter-repl-ret)))


;;; my-functions-jupyter-el.el ends here

(provide 'my-functions-jupyter-el)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)

;; (cond
;;  ((-let* ((bounded (keymap-lookup backtrace-mode-map "s"))
;;           (flag (and bounded (not (member bounded '(my-backtrace-save))))))
;;     (when flag
;;       (my-println🌈
;;        (format "\"s\" is already bounded to `%s' in `backtrace-mode-map'"
;;                bounded)))
;;     flag))
;;  (:else
;;   (my-bind :map 'backtrace-mode-map "s" #'my-backtrace-save)))

;;; after-backtrace.el ends here

(provide 'my-after-backtrace)

;; Local Variables:
;; no-byte-compile: t
;; End:

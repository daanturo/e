;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(setq cdlatex-auto-help-delay 0.5)

;;; my-after-cdlatex.el ends here

(provide 'my-after-cdlatex)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'cider)

;; (my-backtrace)

(setq cider-allow-jack-in-without-project t)

;; Forced load chain: `clj-refactor' -> `cider' -> `tramp'

(with-eval-after-load 'cider
  ;; configured by `my-popup-repl-set-rule'
  (my-delete-alist! 'display-buffer-alist "^\\*cider-repl")
  (setq
   cider-allow-jack-in-without-project t ;
   cider-connection-message-fn #'cider-random-tip
   cider-eldoc-display-for-symbol-at-point t ; set by Doom
   cider-repl-pop-to-buffer-on-connect 'display-only ;
   )
  (my-add-mode-hook-and-now
    '(clojure-mode clojure-ts-mode) #'my-lang-clojure-set-cider-jack-in-tool))

;; `cider-company-enable-fuzzy-completion' will override buffer's
;; `completion-styles'
(advice-add
 #'cider-complete-at-point
 :around
 (my-make-with-dlet-variables-advice '((completion-styles '(cider)))))

(with-eval-after-load 'consult
  (add-to-list
   'consult-mode-histories `(cider-repl-mode cider-repl-input-history)))

;;; after-cider.el ends here

(provide 'my-after-cider)

;; Local Variables:
;; no-byte-compile: t
;; End:

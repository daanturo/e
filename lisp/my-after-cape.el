;; -*- lexical-binding: t; -*-

(require 'cape)

;;;###autoload
(progn
  (defun my-setup-cape ()

    (when (fboundp #'cape-line)
      ;; https://github.com/minad/corfu/issues/188#issuecomment-1148658471
      ;; https://github.com/emacs-lsp/lsp-mode/issues/3555
      ;; https://github.com/minad/corfu/issues/227
      ;; https://www.reddit.com/r/emacs/comments/15bzk4a/comment/ju37074/
      ;; Hangs with several special characters when typing (and completing), TODO: remove this when fixed
      (advice-add
       #'lsp-completion-at-point
       :around #'cape-wrap-noninterruptible)

      ;; https://github.com/doomemacs/doomemacs/blob/665d808d09ecba0be1b2de3977950e579bb5daee/modules/completion/corfu/config.el#L131
      (advice-add #'lsp-completion-at-point :around #'cape-wrap-nonexclusive)
      (advice-add #'comint-completion-at-point :around #'cape-wrap-nonexclusive)
      (advice-add #'eglot-completion-at-point :around #'cape-wrap-nonexclusive)
      (advice-add
       #'pcomplete-completions-at-point
       :around #'cape-wrap-nonexclusive)

      ;;
      )))

;; beware when using `cape-dict' in `completion-at-point-functions', errors will
;; be raised when `cape-dict-file' isn't available
(when (and (not (file-readable-p cape-dict-file))
           (stringp ispell-alternate-dictionary)
           (file-readable-p ispell-alternate-dictionary))
  (my-add-advice-once #'cape-dict :before (lambda (&rest _) (require 'ispell))) ; for `ispell-alternate-dictionary'
  (setq cape-dict-file ispell-alternate-dictionary))

(unless (file-readable-p cape-dict-file)
  (cl-callf (lambda (lst) (remove 'cape-dict lst))
      (default-value 'completion-at-point-functions)))

(advice-add #'cape-dabbrev :around #'my-cape-dabbrev-a)

;; for auto completion, too many candidates is chaotic
(setq cape-dabbrev-check-other-buffers nil)

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'my-after-cape)

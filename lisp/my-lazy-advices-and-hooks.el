;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)

;; Note that this file is only loaded by interactive commands

;;; Code

(defvar my-default-comment-beg "#")

;;;###autoload
(defun my-comment-normalize-vars--default-a (func &rest args)
  (cond
   (comment-start
    (apply func args))
   (t
    (letrec ((adv-name (make-symbol ""))
             (adv-fn
              (lambda (func1 prompt &optional init-input &rest args1)
                (prog1 (apply func1
                              prompt
                              (or init-input my-default-comment-beg)
                              args1)
                  (advice-remove #'read-string adv-name)))))
      (advice-add #'read-string :around adv-fn `((name . ,adv-name)))
      (unwind-protect
          (apply func args)
        (advice-remove #'read-string adv-name))))))

;;;###autoload
(defun my-lsp--read-rename--beg-a (func &rest args)
  (minibuffer-with-setup-hook (lambda ()
                                (goto-char (minibuffer-prompt-end)))
    (apply func args)))

;;;###autoload
(defun my-with-interruptible-while-no-input-nil-a (func &rest args)
  (-let* ((time0 (current-time))
          (val
           (while-no-input
             (vector (apply func args)))))
    (cond
     ((vectorp val)
      (elt val 0))
     ((equal val nil)
      (message
       "`my-with-interruptible-while-no-input-nil-a': quit after %ss `%S'"
       (float-time (time-since time0)) func)
      nil)
     ((equal val t)
      (message
       "`my-with-interruptible-while-no-input-nil-a': input after %ss `%S'"
       (float-time (time-since time0)) func)))))

;;;###autoload
(defun my-write-region-quiet-a
    (func start end filename &optional append _visit &rest args)
  (apply func start end filename append :silent args))

;;;###autoload
(defun my-with-quiet-write-region-to-file-a (fn &rest args)
  (my-with-advice
    #'write-region
    :around #'my-write-region-quiet-a (apply fn args)))

;;;###autoload
(defun my-pop-to-buffer-display-buffer-same-window-a (func &rest args)
  (my-with-advice
    #'pop-to-buffer
    :around
    (lambda (func1 buf _action &rest args1)
      (apply func1 buf '(display-buffer-same-window) args1))
    (apply func args)))

;;; my-lazy-advices-and-hooks.el ends here

(provide 'my-lazy-advices-and-hooks)

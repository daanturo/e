;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(my-add-hook-once
  'ellama-request-mode-hook (lambda () (my-llm-ollama-ensure-serve 'ask)))

;;;###autoload
(progn
  (defun my-setup-ellama ()
    (autoload #'ellama-transient-main-menu "ellama" nil t)))

;;; after-ellama.el ends here

(provide 'my-after-ellama)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'bookmark)

(defun my-bookmark--default-name (&optional _no-file _no-context posn)
  (save-excursion
    (when posn
      (goto-char posn))
    (format "%s:%d:%d (%s) %S"
            (bookmark-buffer-name) (line-number-at-pos) (current-column)
            (file-name-nondirectory
             (or (-some--> (project-current) (project-name it))
                 default-directory))
            (-->
             (thing-at-point 'line t)
             (string-remove-suffix "\n" it)
             (replace-regexp-in-string "^[\t ]+" " " it)
             (truncate-string-to-width
              it (default-value 'comment-column) nil nil 'ellipsis)))))

;; (concat
;;  (-some--> (bookmark-prop-get rec 'front-context-string) (string-trim it) (my-truncate-left-of-string it ctx-len/2 nil nil t))
;;  "¦"
;;  (-some--> (bookmark-prop-get rec 'rear-context-string) (string-trim it) (truncate-string-to-width it ctx-len/2 nil nil t)))

(defun my-bookmark-make-record--custom-defaults (rec)
  (save-excursion
    (save-match-data
      (-let* ((beg
               (save-excursion
                 (skip-chars-backward "[ \t\n]")
                 (my-line-non-whitespace-beg-position)))
              (process-ctx-fn
               (lambda (str)
                 (-->
                  str
                  (string-trim it)
                  (string-replace "\n" "⏎" it)
                  (replace-regexp-in-string "[ \t\n]+" " " it))))
              (ctx
               (concat
                (save-excursion
                  (-some-->
                      (buffer-substring-no-properties beg (point))
                    (my-string-truncate-middle it 16 12)
                    (funcall process-ctx-fn it)))
                "¦"
                (save-excursion
                  (skip-chars-forward "[ \t\n]")
                  (-some-->
                      (my-relative-buffer-substring 0 12)
                    (funcall process-ctx-fn it)
                    (concat it (truncate-string-ellipsis))))))
              (dft-name
               (save-excursion
                 (format "%s:%d:%d %s/ %S"
                         (bookmark-buffer-name)
                         (line-number-at-pos)
                         (current-column)
                         (file-name-nondirectory
                          (or (-some--> (project-current) (project-name it))
                              default-directory))
                         ctx))))
        (bookmark-prop-set
         rec 'defaults
         (-->
          (bookmark-prop-get rec 'defaults)
          (append (list dft-name) it)
          (delete-dups it)
          (delete nil it)))
        rec))))

(defun my-bookmark-make-record--region (rec)
  (when (use-region-p)
    ;; Save the difference instead of the mark position directly, to make use of
    ;; bookmark.el's ability to adjust the position when the file is changed (in
    ;; `bookmark-default-handler')
    (bookmark-prop-set rec :my-region-mark-point-diff (- (mark) (point))))
  rec)

;;;###autoload
(defun my-bookmark-handle-bookmark--restore-region-a
    (func bookmark-name-or-record &rest args)
  (-let* ((bookmark-record (bookmark-get-bookmark bookmark-name-or-record))
          (retval (apply func bookmark-name-or-record args))
          (region-diff
           (bookmark-prop-get bookmark-record :my-region-mark-point-diff)))
    (when region-diff
      (-when-let* ((buf
                    (-some-->
                        bookmark-record
                      (bookmark-get-filename it)
                      (get-file-buffer it)))
                   (mark-pos
                    (-->
                     (+ (point) region-diff)
                     (my-clamp it (point-min) (point-max)))))
        (with-current-buffer buf
          (push-mark mark-pos nil 'active))))
    retval))

;;;###autoload
(defun my-bookmark-make-record-fn (&optional no-file no-context posn)
  (save-excursion
    (when posn
      (goto-char posn))
    (-let* ((rec
             (-->
              (bookmark-make-record-default no-file no-context posn)
              (my-bookmark-make-record--custom-defaults it)
              (my-bookmark-make-record--region it))))
      rec)))

;; ;;;###autoload
;; (defun my-bookmark-set (&optional no-overwrite)
;;   (interactive "P")
;;   (-let* ((dft (my-bookmark--default-name))
;;           (name
;;            (read-from-minibuffer (format-prompt
;;                                   (if (bound-and-true-p no-overwrite)
;;                                       "Add bookmark named"
;;                                     "Set bookmark named")
;;                                   dft)
;;                                  nil bookmark-minibuffer-read-name-map t nil dft)))
;;     name
;;     ;; (bookmark-set name no-overwrite)
;;     ))

;;; my-functions-bookmark.el ends here

(provide 'my-functions-bookmark)

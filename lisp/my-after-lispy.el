;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)

;; DO NOT LOAD `lispy.el' HERE!
;; ;; (require 'lispy)


(defun my-advise-lispy-insert-command (cmd str-to-ins)
  "Advise CMD:

* When the point is after the start of the sexp, insert
  STR-TO-INS and jump back, useful for expressions which utilize
  higher-order functions.

* When region is active, to `self-insert-command' (let other
  packages wrap the region when needed)."
  (let ((adv (intern (format "%s-%s-%s" 'my- cmd '-around-a))))
    (defalias adv
      (lambda (func &rest args)
        (cond ((and (not (use-region-p))
                    (called-interactively-p 'any)
                    (my-point-after-parenthetical-group-beg-p)
                    (symbol-at-point))
               (save-excursion
                 (insert str-to-ins)))
              ((use-region-p)
               (funcall-interactively
                #'self-insert-command
                (prefix-numeric-value current-prefix-arg)))
              (t
               (apply func args))))
      (format "When point is at the start of sexp,
               insert %s instead and jump back to start."
              str-to-ins))
    (advice-add cmd :around adv)))


;; Try my best to defer loading `lispy', delay its monstrous dependency list

;;;###autoload
(defun my-lispy-special-p ()
  (and (or (member (char-after) '(?\( ?\[ ?\{))
           (member (char-before) '(?\) ?\] ?\}))
           (use-region-p))
       (not (my-point-in-string-or-comment-p))))

(defvar my-lispy-dispatch--table (make-hash-table :test #'equal))
(defun my-lispy-dispatch (char-str &optional skip-check)
  (with-memoization (gethash
                     (list char-str skip-check) my-lispy-dispatch--table)
    (-->
     (lookup-key lispy-mode-map (kbd char-str))
     ;; we did the check manually, now reduce the overhead of the built-in check
     (if skip-check
         (intern (string-remove-prefix "special-" (symbol-name it)))
       it)
     (and (commandp it) it))))
(my-add-advice-once #'my-lispy-dispatch :before (lambda (&rest _) (require 'lispy)))

;;;###autoload
(defun my-lispy-make-dispatch-fn (char-str)
  (lambda (&optional _)
    (and (not buffer-read-only)
         (cond
          ;; just insert ";" when before itself
          ((equal char-str ";")
           (not (equal (char-after) ?\;)))
          (t
           (not (my-point-in-string-or-comment-p))))
         ;; `my-lispy-special-p' must be satisfied for alphanumeric keys
         (if (not (string-match-p "^[[:alnum:]]$" char-str))
             (my-lispy-dispatch char-str)
           (and (my-lispy-special-p)
                (my-lispy-dispatch char-str 'skip-check))))))

(defun my-lispy-make-dispatch-tab-key-fn (char-str)
  (lambda (&optional _)
    (and
     ;; don't override the REPL's tab key
     (not (get-buffer-process (current-buffer)))
     (funcall (my-lispy-make-dispatch-fn char-str)))))

;;;###autoload
(define-minor-mode my-lispy-mode nil
  :keymap
  (append
   (my-for [k (append (my-alphabet "a") (my-alphabet "A")
                      (my-alphabet "0" 10)
                      '("`" "-" "="
                        "~" "!" "@" "#" "$" "%" "^" "&" "*" "(" ")" "_" "+"
                        "<backtab>"
                        "[" "]" "\\" "{" "}" "|"
                        ";" "'" ":" "\""
                        "," "." "/" "<" ">" "?"))]
     ;; "SPC"
     
     (cons (kbd k)
           (list 'menu-item "" nil :filter (my-lispy-make-dispatch-fn k))))
   (my-for [k '("TAB" "<tab>")]
     (cons (kbd k)
           (list 'menu-item "" nil :filter (my-lispy-make-dispatch-tab-key-fn k))))))

;;;###autoload
(define-minor-mode my-lispyville-mode
  nil
  :keymap
  (append
   (cl-loop
    for (key . cmd) in
    `(
      ;; only use some operations

      ([remap evil-change-line] . lispyville-change-line)
      ([remap evil-change-whole-line] . lispyville-change-whole-line)
      ([remap evil-change] . lispyville-change)
      ([remap evil-delete-line] . lispyville-delete-line)
      ([remap evil-delete-whole-line] . lispyville-delete-whole-line)
      ([remap evil-delete] . lispyville-delete))
    ;; ([remap evil-delete-backward-char] . lispyville-delete-char-or-splice-backwards)
    ;; ([remap evil-delete-backward-word] . lispyville-delete-backward-word)
    ;; ([remap evil-delete-char] . lispyville-delete-char-or-splice)
    ;; ([remap evil-join] . lispyville-join)
    ;; ([remap evil-substitute] . lispyville-substitute)
    ;; ([remap evil-yank-line] . lispyville-yank-line)
    ;; ([remap evil-yank] . lispyville-yank)

    ;;
    
    do
    (when cmd
      (autoload cmd "lispyville" nil 'interactive))
    collect (cons key cmd))
   `(([remap evil-indent]
      .
      (menu-item
       ""
       my-elisp-code-format-dwim
       :filter my-apheleia-menu-item-filter-not-external-formatters)))))

;;


;;;###autoload
(progn
  (defun my-setup-lispy ()
    ;; (my-setup-symex)
    (setq lispyville-key-theme '(operators c-w prettify))
    (when (fboundp #'lispy-mode)
      ;; `lispy' is buggy for REPLs
      (my-add-startup-hook
        (-let* ((modes my-lisp-major-modes)
                (hooks
                 (-map
                  #'my-mode->hook (append modes my-lisp-repl-modes))))
          (my-add-hook/s
            hooks
            `(
              ;; my-lispy-mode
              ,@(and (fboundp #'lispyville-mode)
                     '(my-lispyville-mode))))))
      ;; (my-add-hook-once
      ;;   'evil-insert-state-entry-hook
      ;;   (lambda ()
      ;;     (my-add-mode-hook-and-now modes 'my-lispy-mode)))
      ;; (add-hook
      ;;  'eval-expression-minibuffer-setup-hook #'my-lispy-mode)

      (my-autoload-commands
        'lispy
        '(lispy-multiline lispy-oneline lispy-to-cond lispy-to-ifs)))))

(defun my-lispy-self-insert-when-unbalanced--a (func &rest args)
  (if-let* ((_ (and (called-interactively-p 'any)
                    (not (my-balanced-parens-and-brackets-p))))
            (pressed (key-description (this-command-keys-vector)))
            (_ (length= pressed 1)))
      (self-insert-command (prefix-numeric-value current-prefix-arg))
    (apply func args)))
(my-add-advice/s #'lispy-barf-to-point-nostring :around #'my-lispy-self-insert-when-unbalanced--a)

;; ;; Don't load `cider' and `clojure-mode' just to overlay evaluation results;
;; ;; although loading is long, the overlay display has quoting problems
;; (defun my-lispy-eval--no-cider-a (func &rest args)
;;   (my-with-advice #'require :around (lambda (func1 feat &rest args1)
;;                                       (unless (member feat '(cider))
;;                                         (apply func1 feat args1)))
;;     (apply func args)))
;; (advice-add #'lispy-eval :around #'my-lispy-eval--no-cider-a)
;; (with-eval-after-load 'cider
;;   (advice-remove #'lispy-eval #'my-lispy-eval--no-cider-a))

;; `cider' will ask for `comment-start' when after evaluating, the focused
;; buffer doesn't have it
(setq lispy-eval-display-style 'message)

;; Prevent unwanted spaces
(dolist (cmd '(lispy-parens-auto-wrap
               lispy-brackets-auto-wrap
               lispy-braces-auto-wrap))
  (autoload cmd "lispy" nil 'interactive)
  (my-bind :map 'read-expression-map
    (vector 'remap cmd)
    (lambda (arg)
      (interactive "P")
      (my-without-spaces-inserted
       (funcall cmd arg)))))

;; Workaround `lispy-space' being no-op in `edebug-eval-expression'
(defun my-lispy--ignore-edebug-a (func &rest args)
  (interactive "p")
  (dlet ((edebug-active nil))
    (apply func args)))
(advice-add #'lispy-space :around #'my-lispy--ignore-edebug-a)

;; (my-advise-lispy-insert-command 'lispy-backtick " `")
(my-advise-lispy-insert-command 'lispy-tick " '")
(my-advise-lispy-insert-command 'lispy-hash " #'")

;; Too aggressive in minibuffer
(advice-add #'lispy-raise :override #'raise-sexp)

(when (member 'parinfer lispy-key-theme)
  (my-bind :map 'my-lispy-mode-map "(" #'my-lispy-parens-parinfer-auto-wrap))
(my-bind :map 'my-lispy-mode-map
  ":" nil)                               ; don't insert space before


(with-eval-after-load 'lispy
  (add-to-list
   'lispy-eval-alist
   (cons
    '(inferior-emacs-lisp-mode minibuffer-mode)
    (cdr
     (assoc 'emacs-lisp-mode lispy-eval-alist
            (lambda (mode/s mode)
              (my-mode-derived-p mode (ensure-list mode/s)))))))

  (my-elisp-add-doc-to-function-maybe
   #'lispy-indent-adjust-parens
   (format "\n\nSee also `indent-for-tab-command'.")))

;; (my-when-mode0-turn-off||disable-mode1 'cider--debug-mode 'lispy-mode)

(my-add-advice/s 'lispy--auto-wrap :around #'my-lispy-wrap-try-not-edit-before-point-a)


;;; after-lispy.el ends here

(provide 'my-after-lispy)

;; Local Variables:
;; no-byte-compile: nil
;; End:

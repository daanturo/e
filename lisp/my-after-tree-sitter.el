;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'tree-sitter nil t)


(defvar my-elisp-tree-sitter-excluded-modes '())

(defvar tree-sitter-mode nil)
;;;###autoload
(defun my-elisp-tree-sitter-mode-maybe ()
  (unless (-some 'my-enabled-mode-p my-elisp-tree-sitter-excluded-modes)
    (ignore-errors
      (tree-sitter-mode))
    (when tree-sitter-mode
      (my-elisp-tree-sitter-mode))))

;;;###autoload
(define-globalized-minor-mode my-elisp-tree-sitter-global-mode
  tree-sitter-mode
  my-elisp-tree-sitter-mode-maybe)

;; (defvar-local my-elisp-tree-sitter-mode--state nil)
;;;###autoload
(define-minor-mode my-elisp-tree-sitter-mode
  nil
  :global nil
  :keymap `(
            ;; ([remap mark-defun] . my-elisp-tree-sitter-mark-defun)
            )
  (if my-elisp-tree-sitter-mode
      (progn
        ;; (setq-local my-elisp-tree-sitter-mode--state
        ;;             (buffer-local-set-state
        ;;              beginning-of-defun-function
        ;;              #'my-elisp-tree-sitter-beg-of-defun
        ;;              end-of-defun-function
        ;;              #'my-elisp-tree-sitter-end-of-defun))
        )
    (progn
      ;; (buffer-local-restore-state my-elisp-tree-sitter-mode--state)
      )))

(provide 'my-after-tree-sitter)

;; Local Variables:
;; no-byte-compile: nil
;; End:

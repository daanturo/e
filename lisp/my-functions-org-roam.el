;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'transient)

;;;###autoload
(defun my-org-roam-open-note-directory ()
  (interactive)
  (find-file org-roam-directory)
  (save-excursion
    (when (and (dired-goto-file
                (directory-file-name
                 (expand-file-name org-roam-dailies-directory)))
               (fboundp #'dired-subtree-insert))
      (dired-subtree-insert))))

;;;###autoload
(transient-define-prefix
  my-org-roam-transient ()
  [["Capture"
    ;; ("l" "buffer-toggle" org-roam-buffer-toggle)
    ("f" "Find node" org-roam-node-find)
    ("c" "Capture" org-roam-capture)
    ("j" "Capture today" org-roam-dailies-capture-today)]
   ;; ["Edit"
   ;;  ("i" "Insert node" org-roam-node-insert)
   ;;  ("ta" "Add tag" org-roam-tag-add)
   ;;  ("tr" "Remove tag" org-roam-tag-remove)]
   ;; ["Graph" ("g" "Graph" org-roam-graph)]
   ["UI" ("uo" "Open" org-roam-ui-open) ("um" "UI mode" org-roam-ui-mode)]
   [("<escape>" "" transient-quit-one)]])

;; ;;;###autoload
;; (defun my-org-roam-capture-dwim ()
;;   (interactive)
;;   (-let* ((node (org-roam-node-read )))
;;     ))

;;;###autoload
(defun my-org-roam-tag-completions--filter-a (func &rest args)
  (-let* ((retval (apply func args)))
    (cond
     ((proper-list-p retval)
      (-difference retval '("ATTACH")))
     (:else
      retval))))


;;; my-functions-org-roam.el ends here

(provide 'my-functions-org-roam)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(setq doi-utils-download-pdf nil)

;;; my-after-doi-utils.el ends here

(provide 'my-after-doi-utils)
;; Local Variables:
;; no-byte-compile: t
;; End:

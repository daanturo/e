;; -*- lexical-binding: t; -*-

;;;###autoload
(define-minor-mode my-undim-commentary-mode
  "Doesn't work now."
  :global nil
  (if my-undim-commentary-mode
      (font-lock-add-keywords nil my-undim-commentary-font-lock-keywords t)
    (font-lock-remove-keywords nil my-undim-commentary-font-lock-keywords))
  (jit-lock-mode t))

(defun my-commentary-regexp ()
  "A paragraph of comments with 2 or more lines."
  (format "\n\\(\\(?:^%s.*\n\\)\\{2,\\}\\)"
          (my-trimmed-comment-beg* 2)))

(defvar my-undim-commentary-font-lock-keywords
  `((eval . (list
             ;; re
             (my-commentary-regexp)
             ;; num
             1
             ;; face
             'default
             ;; t/override/append
             t
             ;; t?
             t
             ))))

(defvar-local my-undim-comment-mode--cookie nil)
;;;###autoload
(define-minor-mode my-undim-comment-mode nil
  :global nil
  (if my-undim-comment-mode
      (setq my-undim-comment-mode--cookie
            (face-remap-add-relative
             'font-lock-comment-face
             `(:slant normal
		      :foreground ,(if (equal 'dark (frame-parameter nil 'background-mode))
                                       "light gray"
				     "dim gray"))))
    (face-remap-remove-relative my-undim-comment-mode--cookie))
  (force-window-update (current-buffer)))

(provide 'my-undim-commentary)

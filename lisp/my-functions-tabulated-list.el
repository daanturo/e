;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar-local my-tabulated-list-old-widths nil)
;;;###autoload
(defvar-local my-tabulated-list-max-widths nil)
(defun my-tabulated-list-fit-width-all-columns ()
  (interactive)
  (my-with-deferred-gc
   (unless my-tabulated-list-max-widths
     (setq-local my-tabulated-list-max-widths
                 (my-tabulated-list-get-maximum-entry-lengths)))
   (setq-local my-tabulated-list-old-widths
               (seq-map (-lambda ((name width sort . props))
                          width)
                        tabulated-list-format))
   (setq-local tabulated-list-format
               (--> (seq-map-indexed
                     (-lambda ((name width sort . props) idx)
                       `(,name ,(nth idx my-tabulated-list-max-widths)
                         ,sort . ,props))
                     tabulated-list-format)
                    (seq-into it 'vector)))
   (tabulated-list-init-header)
   (my-save-line-and-column
     (tabulated-list-print t))))
(defun my-tabulated-list-get-maximum-entry-lengths ()
  (cl-loop
   for idx below (length tabulated-list-format)
   collect (cl-loop
            for entry in tabulated-list-entries
            maximize
            (--> entry cadr (aref it idx)
                 (if (stringp it)
                     it
                   (car it))
                 length))))

;;;###autoload
(defun my-tabulated-list-restore-from-fit-width-all-columns ()
  (interactive)
  (setq-local tabulated-list-format
              (--> (seq-map-indexed
                    (-lambda ((name width sort . props) idx)
                      `(,name
                        ,(min width
                              (nth idx my-tabulated-list-old-widths))
                        ,sort
                        . ,props))
                    tabulated-list-format)
                   (seq-into it 'vector)))
  (tabulated-list-init-header)
  (my-save-line-and-column
    (tabulated-list-print t)))

;;;###autoload
(defun my-tabulated-list-auto-columns ()
  "Assume that `tabulated-list-entries' has been set."
  (let ((column-num (-->  tabulated-list-entries
                          car
                          (nth 1 it)
                          length)))
    (setq-local tabulated-list-format
                `[,@(cl-loop for i below column-num
                             collect (list (format "Column %d" i) 48 nil))])))

(provide 'my-functions-tabulated-list)

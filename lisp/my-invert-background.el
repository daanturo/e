;; -*- lexical-binding: t; -*-

(defvar-local my-invert-background-mode--face-remap-cookie nil)
;;;###autoload
(define-minor-mode my-invert-background-mode nil
  :global nil
  (if my-invert-background-mode
      (unless my-invert-background-mode--face-remap-cookie
        (let ((bg (face-attribute 'default :background)))
          (setq my-invert-background-mode--face-remap-cookie
                (face-remap-add-relative
                 'default
                 `(:background ,(my-inverse-color bg))))))
    (when my-invert-background-mode--face-remap-cookie
      (face-remap-remove-relative my-invert-background-mode--face-remap-cookie)
      (setq my-invert-background-mode--face-remap-cookie nil)))
  (force-window-update (current-buffer)))

(provide 'my-invert-background)

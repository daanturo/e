;; -*- lexical-binding: t; -*-

;;;###autoload
(defun my-emacsql-sqlite-ensure-binary-maybe (&optional interac)
  (interactive (list t))
  (when (or interac
            (and (executable-find "gcc")
                 (require 'emacsql-sqlite nil 'noerror)))
    (emacsql-sqlite-ensure-binary)))

;;;###autoload
(defun my-package-delete-heavy-unused-files-in-cloned-repositories
    (&optional return-files)
  (-let*
      ((dirty-table (make-hash-table :test #'equal))
       (files-to-delete
        '( ;

          "blamer.el/images"
          "cider/doc"
          "clj-refactor.el/examples"
          "clojure-mode/doc"
          "coercion.el/example.gif"
          "dap-mode/screenshots"
          "elisp-tree-sitter/doc"
          "elisp-tree-sitter/langs/bin/d.so"
          "importmagic.el/importmagic.gif"
          "lsp-dart/images"
          "lsp-mode/docs"
          "lsp-mode/examples"
          "lsp-mode/test"
          "lsp-ui/images"
          "org-ref/screenshots"
          "powerthesaurus/assets"
          "tree-sitter-langs/bin/d.so"
          "tree-sitter-langs/bin/perl.so"
          "tree-sitter-langs/bin/verilog.so"
          "treemacs/screenshots"

          ;;
          ))
       (del-fn
        (lambda (path)
          (-when-let* ((dir (file-name-parent-directory path))
                       (_ (file-exists-p dir))
                       (root (my-vc-git-get-root dir)))
            ;; only check the initial status at the start of function, to delete multiple paths
            (-let* ((cached-dirty-status (gethash root dirty-table))
                    (dirty
                     (if cached-dirty-status
                         (equal :true cached-dirty-status)
                       (my-vc-git-dirty-unstaged-or-uncommitted-p dir))))
              (unless cached-dirty-status
                (puthash root (if dirty :true :false) dirty-table))
              (cond
               (dirty
                t)
               ((and (not dirty)
                     (not (file-exists-p path))
                     (not (my-vc-git-check-ignore-p path dir)))
                (my-println🌈 path "doesn't exist (repo is clean)"))
               ((not (file-exists-p path))
                nil)
               ((dlet ((default-directory dir))
                  (-let* ((default-branch
                           (my-vc-git-get-default-branch 'local)))
                    (and default-branch
                         (not
                          (equal default-branch (my-vc-git-get-current-branch)))
                         (progn
                           (my-println🌈
                            "Not default branch, skip deleting:" path)
                           t)))))
               ((progn
                  (my-println🌈 "Deleting unused" path)
                  nil))
               ((file-directory-p path)
                (delete-directory path 'recursive))
               (t
                (delete-file path))))))))
    (with-eval-after-load 'straight
      (-let* ((paths
               (-->
                (-map #'straight--repos-dir files-to-delete)
                (-map #'abbreviate-file-name it)
                (-map #'directory-file-name it))))
        (if return-files
            paths
          (mapc del-fn paths))))))

;;;###autoload
(defun my-cli-tasks (tasks)
  (interactive)
  (unless (seq-empty-p tasks)
    (my-println🌈 #'my-cli-tasks)
    (let* ((verbose? (member (bound-and-true-p doom-print-minimum-level)
                             '(info debug))))
      (dolist (task tasks)
        (if verbose?
            (progn
              (my-println🌈 "	" task)
              (my-benchmark-progn* "	 %fs\n"
                (funcall task)))
          (progn
            (funcall task)))))))

;;;###autoload
(defun my-cli-tasks-before ()
  (my-cli-tasks '(
                  ;;
                  )))

;;;###autoload
(defun my-cli-tasks-after ()
  (my-cli-tasks
   '( ;;
     my-useelisp-refresh
     my-package-delete-heavy-unused-files-in-cloned-repositories
     ;;
     )))

(defvar my-doom-before-upgrade-hook '())
;;;###autoload
(defun my-doom-run-before-upgrade-hook-a (&rest _)
  (run-hooks 'my-doom-before-upgrade-hook))

(provide 'my-functions-command-line)

;; Local Variables:
;; no-byte-compile: t
;; End:

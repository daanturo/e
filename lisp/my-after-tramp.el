;; -*- lexical-binding: t; -*-

(require 'tramp)

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)

;; https://emacs.stackexchange.com/questions/64062/how-to-avoid-using-auth-sources-when-editing-with-sudo

;; Don't use an authentication file for TRAMP passwords
;; based on https://debbugs.gnu.org/46674

(my-safe

 (connection-local-set-profile-variables
  'remote-without-auth-sources
  '((auth-sources . nil)))

 (connection-local-set-profiles
  '(:application tramp :protocol "sudo"
                 :user "root" :machine "localhost")
  'remote-without-auth-sources)

 ;;
 )

;; https://emacs.stackexchange.com/questions/78644/how-to-tell-tramp-to-not-ask-me-about-autosave-on-local-directory
(setq tramp-allow-unsafe-temporary-files t)

;; NOTE when `tramp' hangs for too long, the culprit maybe the remote's usage of
;; a fancy prompt or zsh

(provide 'my-after-tramp)

;; Local Variables:
;; no-byte-compile: t
;; End:

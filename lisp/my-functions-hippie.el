;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'hippie-exp)

(defun my-hippie-try-complete-filename-in-project--candidates (str)
  (dlet ((default-directory (or (my-project-root) default-directory)))
    (and (executable-find "find")
         (-let* ((fn
                  (lambda (&optional hidden)
                    (-->
                     (apply #'process-lines
                            `("find" ,@(if hidden
                                           '()
                                         '("-not" "-path" "*/.*"))
                              "-name" ,(format "%s*" str)
                              "-printf" "%f\n" ; only print base file names
                              ))
                     (delete-dups it) (sort it #'string-lessp))))
                 (files
                  (or
                   ;; prefer no-hidden files
                   (funcall fn)
                   (funcall fn t))))
           files))))

;;;###autoload
(defun my-hippie-filename-component-beg (&optional directory-separator)
  (-let* ((directory-separator (or directory-separator "/"))
          (path-beg (he-file-name-beg)))
    (or (save-excursion
          (save-match-data
            (-some-->
                (search-backward directory-separator path-beg t)
              (+ it (length directory-separator)))))
        path-beg)))

;;;###autoload
(defun my-hippie-try-complete-filename-in-project (old)
  (when (not old)
    ;; reference: `try-complete-lisp-symbol'
    (he-init-string (my-hippie-filename-component-beg) (point))
    (when (not (he-string-member he-search-string he-tried-table))
      (setq he-tried-table (cons he-search-string he-tried-table)))
    (setq he-expand-list
          (if (equal he-search-string "")
              '()
            (my-hippie-try-complete-filename-in-project--candidates
             he-search-string))))
  (while (and he-expand-list
              (he-string-member (car he-expand-list) he-tried-table))
    (setq he-expand-list (cdr he-expand-list)))
  (cond
   ((seq-empty-p he-expand-list)
    (when old
      (he-reset-string))
    nil)
   (t
    (he-substitute-string (car he-expand-list))
    (setq he-expand-list (cdr he-expand-list))
    t)))

;;;###autoload
(defun my-hippie-expand* (arg)
  (interactive "P")
  (dlet ((hippie-expand-try-functions-list
          ;; `try-expand-dabbrev' doesn't honor cases
          (append
           ;; '(my-hippie-try-complete-filename-in-project)
           '(my-dabbrev-expand-no-error) ;
           hippie-expand-try-functions-list ;
           ;;
           )))
    (hippie-expand arg)
    ;; fallback to word only, without considering the whole symbol
    (when (equal "No expansion found" (current-message))
      (dlet ((hippie-expand-dabbrev-as-symbol
              (not hippie-expand-dabbrev-as-symbol)))
        (hippie-expand arg)))))

;;;###autoload
(defun my-hippie-expand-only-word-no-symbol (arg)
  (interactive "P")
  (dlet ((hippie-expand-dabbrev-as-symbol nil))
    (hippie-expand arg)))


;;; my-functions-hippie.el ends here

(provide 'my-functions-hippie)

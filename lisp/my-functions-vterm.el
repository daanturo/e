;; -*- lexical-binding: t; -*-

;;;###autoload
(defun my-vterm-ensure-compiled-module-h ()
  "Ensure that `vterm-module' is compiled."
  (interactive)
  (setq vterm-always-compile-module t)
  (require 'vterm nil 'noerror))

;;;###autoload
(defun my-vterm-quit ()
  (interactive)
  (when (equal major-mode 'vterm-mode)
    (-let* ((vterm-buf (current-buffer))
            (wd (selected-window)))
      (when (vterm-check-proc)
        (dlet ((vterm-kill-buffer-on-exit t))
          (vterm-send-C-d)))
      (dlet ((ignore-window-parameters t))
        (delete-window wd))
      ;; the `vterm' process is still alive
      (run-at-time vterm-timer-delay nil
                   #'kill-buffer vterm-buf))))

;;;###autoload
(defun my-vterm-other-window ()
  (interactive)
  (my-in-other-window 'ensure-window (vterm (buffer-name))))

;; (shell-command-to-string (format "pgrep -P %s" (process-id proc)))

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'my-functions-vterm)

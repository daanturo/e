;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-snippet-expandable-p ()
  (or (my-tempel-expandable-p #'my-bounds-of-prev-sexp-to-point)
      (my-yasnippet-expandable-p)))

(defvar my-setup-expand-snippet-after-tab-completion-last-commands '())

(defvar-local my-setup-expand-snippet-after-tab-completion--fallback-undo-state
    nil)

(defvar-local my-setup-expand-snippet-after-tab-completion--already-undo nil)

;;;###autoload
(defun my-setup-expand-snippet-after-tab-completion ()
  (cond

   ;; don't edit when expandable
   ((my-snippet-expandable-p)
    nil)

   ((member
     last-command
     `(,my-popup-complete-command
       ,@my-setup-expand-snippet-after-tab-completion-last-commands))


    (when (not my-setup-expand-snippet-after-tab-completion--already-undo)
      (undo-only)
      (setq my-setup-expand-snippet-after-tab-completion--already-undo t)
      (my-add-hook-once
        'post-command-hook
        (lambda ()
          (setq my-setup-expand-snippet-after-tab-completion--already-undo nil))
        nil 'local))


    ;; https://github.com/minad/corfu/issues/495 undo after `corfu-complete'
    ;; deletes too much that the original candidate is lost
    (when my-setup-expand-snippet-after-tab-completion--fallback-undo-state
      (my-fallback-undo-restore
       my-setup-expand-snippet-after-tab-completion--fallback-undo-state)
      (setq my-setup-expand-snippet-after-tab-completion--fallback-undo-state
            nil))

    t)
   (:else

    (setq my-setup-expand-snippet-after-tab-completion--fallback-undo-state
          (my-fallback-undo-state))
    (my-add-hook-once-at-count
     1 'post-command-hook
     (lambda ()
       (setq
        my-setup-expand-snippet-after-tab-completion--fallback-undo-state nil)))

    nil)))


;;; my-functions-snippet.el ends here

(provide 'my-functions-snippet)

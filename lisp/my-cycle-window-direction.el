;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-cycle-window-direction-order '(below right above left))

(defun my-cycle-window--exclude-subset-if-contain (subset superset)
  (if (cl-subsetp subset superset)
      (cl-set-difference superset subset)
    superset))

(defun my-cycle-window-direction--display-current-buffer-at (direct)
  (-let* ((buf (current-buffer)))
    (delete-window)
    (display-buffer-in-direction buf `((direction . ,direct)))
    (select-window (get-buffer-window buf))))

(defun my-cycle-window-direction--determine-current-and-next
    (&optional clock-wise)
  (-let* ((order
           (if clock-wise
               (reverse my-cycle-window-direction-order)
             my-cycle-window-direction-order))
          (edge-flags
           (-->
            (-map
             (lambda (direct)
               (and (null (window-in-direction direct)) direct))
             order)
            (my-cycle-window--exclude-subset-if-contain '(left right) it)
            (my-cycle-window--exclude-subset-if-contain
             '(above below) it)
            (remove nil it))))
    (cond
     ((= 1 (length edge-flags))
      (-let* ((curr (car edge-flags))
              (idx (-elem-index curr order))
              (next (nth (% (+ idx 1) (length order)) order)))
        (list curr next)))
     (:else
      nil))))

(defvar my-cycle-window-direction-before-hook '())

;;;###autoload
(defun my-cycle-window-direction (&optional clock-wise orig-buf)
  "Rotate current window in the anti-clock-wise (or CLOCK-WISE) direction.
ORIG-BUF is an internal argument for recursive calls."
  (interactive "P")
  (when (not orig-buf)
    (run-hooks 'my-cycle-window-direction-before-hook))
  (cond
   ((or (one-window-p 'momini)
        (and orig-buf (equal (window-main-window) (selected-window))))
    (message "`my-cycle-window-direction': is main window, can't do.")
    nil)
   ;; current window can't be deleted and re-displayed, do the other one instead
   ((equal (window-main-window) (selected-window))
    (-let* ((buf0 (current-buffer)))
      (dlet ((ignore-window-parameters t))
        (-some--> (next-window) (select-window it)))
      (my-cycle-window-direction clock-wise buf0)))
   (:else
    (-let* ((determined
             (my-cycle-window-direction--determine-current-and-next clock-wise))
            ((_curr next) determined))
      (cond
       (determined
        (my-cycle-window-direction--display-current-buffer-at next)
        (balance-windows)
        (-some--> orig-buf (get-buffer-window it) (select-window it))
        t)
       (:else
        (message "`my-cycle-window-direction': can't determine next direction.")
        nil))))))

;; (defun my-cycle-window-direction--until-edge (direct &optional wd0)
;;   (-let* ((wd0 (or wd0 (selected-window))))
;;     (named-let
;;         recur ((wd1 wd0))
;;       (-let* ((wd2 (window-in-direction direct wd1)))
;;         (cond
;;          ((null wd2)
;;           wd1)
;;          (:else
;;           (recur wd2)))))))

;; (defun my-cycle-window-direction--get-sole-edge-window (direct)
;;   ;; (--> (-map (lambda (wd) (my-cycle-window-direction--until-edge direct wd))
;;   ;;            (window-list))
;;   ;;      (-uniq it) (if (= 1 (length it)) (car it) nil))
;;   (-let* ((wd (my-cycle-window-direction--until-edge direct)))
;;     (cond
;;      ((member direct '(right left))
;;       (and (window-full-height-p wd) wd))
;;      (:else
;;       (and (window-full-width-p wd) wd)))))


;;; my-cycle-window-direction.el ends here

(provide 'my-cycle-window-direction)

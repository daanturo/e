;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(my-bind :map 'tar-mode-map '("C-RET" "C-<return>") #'tar-display-other-window)

;;; after-tar-mode.el ends here

(provide 'my-after-tar-mode)

;; Local Variables:
;; no-byte-compile: t
;; End:

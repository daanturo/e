;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-custom-file-save-variable (var val)
  (interactive (list (my-completing-read-symbol) (read--expression "Value: ")))
  (custom-set-variables (list var val))
  (custom-save-all))

;;; my-functions-custom.el ends here

(provide 'my-functions-custom)

;;; my-previewow.el --- Previewing library, press up/down to peek at another window  -*- lexical-binding: t; -*-


;;; Commentary:



;;; Code:

(require 'cl-macs)
(require 'dash)

(defgroup my-previewow nil
  nil)

(defvar my-previewow--mark-visited-maybe-hook-variables
  '(window-selection-change-functions
    window-buffer-change-functions server-visit-hook))

(defvar-local my-previewow--windows nil)
(defvar-local my-previewow--trigger-keys nil)
(defvar-local my-previewow--close-before-keys nil)
(defvar-local my-previewow--close-condition #'always)

(defvar-local my-previewow-visited nil
  "Do not kill this buffer when closing the preview window.")

(defvar-local my-previewow-open-window-fn nil
  "Function to open preview and return the (new) window.")

(defcustom my-previewow-delay 0.5
  nil
  :type '(choice integer))

(defun my-previewow-close--1 (window)
  (when (window-live-p window)
    (with-selected-window window
      (when (not my-previewow-visited)
        ;; TODO: don't kill buffer too eagerly
        (kill-buffer))
      (delete-window window))))

(defun my-previewow-close ()
  (interactive)
  (-let* ((windows my-previewow--windows))
    (dolist (wd windows)
      (my-previewow-close--1 wd)))
  (setf my-previewow--windows '()))

;;;###autoload
(defun my-previewow-open (&optional open-window-fn)
  (when (funcall my-previewow--close-condition)
    (my-previewow-close))
  (-when-let* ((window
                (save-selected-window
                  (funcall (or open-window-fn my-previewow-open-window-fn)))))
    (push window my-previewow--windows)
    (with-selected-window window
      (-let* ((buf (current-buffer)))
        (letrec ((fn
                  (lambda (&rest _)
                    (when (equal buf (current-buffer))
                      (setq my-previewow-visited t)
                      (dolist (hook
                               my-previewow--mark-visited-maybe-hook-variables)
                        (remove-hook hook fn 'local))))))
          (dolist (hook my-previewow--mark-visited-maybe-hook-variables)
            (add-hook hook fn nil 'local)))))))

(defvar-local my-previewow-delay--timer nil)


(defun my-previewow--post-command-h (&rest _)
  (when (member (this-command-keys) my-previewow--trigger-keys)
    (my-previewow-open--trigger-after-a)))

(defun my-previewow--pre-command-h (&rest _)
  (when (member (this-command-keys) my-previewow--close-before-keys)
    (my-previewow-close)))

;;;###autoload
(define-minor-mode my-previewow-mode
  nil
  :global
  nil
  (cond
   (my-previewow-mode
    (add-hook 'post-command-hook #'my-previewow--post-command-h nil 'local)
    (add-hook 'pre-command-hook #'my-previewow--pre-command-h nil 'local))
   (:else
    (remove-hook 'post-command-hook #'my-previewow--post-command-h 'local)
    (remove-hook 'pre-command-hook #'my-previewow--pre-command-h 'local)
    (my-previewow-close))))

(defun my-previewow-open--trigger-after-a (&rest _)
  (cond
   ((null my-previewow-delay)
    (my-previewow-open))
   (:else
    (-let* ((pt (point))
            (buf (current-buffer)))
      (when my-previewow-delay--timer
        (cancel-timer my-previewow-delay--timer))
      (setq my-previewow-delay--timer
            (run-with-idle-timer
             my-previewow-delay nil
             (lambda ()
               (with-current-buffer buf
                 (when (and my-previewow-mode (= pt (point)))
                   (my-previewow-open))))))))))

;;;###autoload
(defun my-previewow-toggle ()
  (interactive)
  (my-previewow-mode 'toggle)
  (when my-previewow-mode
    (my-previewow-open)))

;; TODO: when pressing "enter", close previews

;;;###autoload
(cl-defun my-previewow-setup (open-window-fn
                              &optional
                              close-cond
                              toggle-key/s
                              &key
                              (close-before-keys '("RET" "<return>"))
                              (keys '("<down>" "<up>")))
  "Set up preview in other window.
OPEN-WINDOW-FN: see `my-previewow-open-window-fn'. CLOSE-COND: function
to determine that the previous preview window(s) should be close before
attempting to open a new preview. TOGGLE-KEY/S: a single key binding or
list of them, when non-nil bind `my-previewow-toggle' to this locally.
KEYS: trigger keys, after each command, if the executed key sequence
match those, open a new preview. CLOSE-BEFORE-KEYS: close previews when
hitting those keys."
  (setq my-previewow-open-window-fn open-window-fn)
  (setq my-previewow--trigger-keys (-map #'kbd keys))
  (setq my-previewow--close-before-keys (-map #'kbd close-before-keys))
  (when close-cond
    (setq my-previewow--close-condition close-cond))
  (when toggle-key/s
    (dolist (toggle-key (ensure-list toggle-key/s))
      (keymap-local-set toggle-key #'my-previewow-toggle)))
  (my-previewow-mode 1))

;;; my-previewow.el ends here

(provide 'my-previewow)

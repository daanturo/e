;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'comint)

(defvar-local my-repl-input-history--local-flag nil)

(defvar my-repl-input-history--initialized nil)

(defvar my-repl-input-history-dir
  (file-name-concat my-user-emacs-local-dir/ "repl-input-history"))

;;;###autoload
(defun my-repl-input-history-filename ()
  (or (bound-and-true-p my-repl-program)
      (--> (symbol-name major-mode) (string-remove-suffix "-mode" it))))


;; https://www.n16f.net/blog/making-ielm-more-comfortable/

;;;###autoload
(defun my-repl-input-history-init-comint-h (&rest _)
  (declare)
  ;; (my-backtrace)
  (-let*
      ((fn
        (lambda ()
          (when (and
                 ;; not already set
                 (not (bound-and-true-p comint-input-ring-file-name))
                 ;; not a specialized mode
                 (not (member major-mode '(comint-mode))))
            (unless my-repl-input-history--initialized
              (require 'comint)
              (make-directory my-repl-input-history-dir t)
              (advice-add
               #'comint-send-input
               :after #'my-repl-input-history-after-comint-send-input-a)
              (setq-default my-repl-input-history--initialized t))
            (-let* ((save-file
                     (file-name-concat my-repl-input-history-dir
                                       (my-repl-input-history-filename))))
              (setq-local my-repl-input-history--local-flag t)

              (setq-local comint-input-ignoredups t)
              (setq-local comint-input-ring-file-name save-file)
              (setq-local comint-input-ring-separator "\n\n") ; allow multi-line
              (setq-local comint-input-ring-size most-positive-fixnum)

              (comint-read-input-ring 'silent))))))
    ;; `comint-mode-hook' runs immediately in `make-comint-in-buffer', at that
    ;; time other initializations haven't been run yet, TODO: find another more
    ;; robust way
    ;; (my-run-at-0-current-buffer (funcall fn))
    (funcall fn)))


;;;###autoload
(defun my-repl-input-history-after-comint-send-input-a (&rest _)
  (when my-repl-input-history--local-flag
    (make-thread #'comint-write-input-ring)))

;; bug#23063 https://yhetil.org/emacs/56ED6BE9.208@gmail.com/, TODO: report
;; reproducible steps
;;;###autoload
(defun my-comint-ring-fix-separator-a (func &rest args)
  (-let* ((global-sep (default-value 'comint-input-ring-separator))
          (local-sep
           (buffer-local-value 'comint-input-ring-separator (current-buffer))))
    (setq-default comint-input-ring-separator local-sep)
    (unwind-protect
        (apply func args)
      (setq-default comint-input-ring-separator global-sep))))

(provide 'my-repl-input-history)

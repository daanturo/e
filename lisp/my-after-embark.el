;; -*- lexical-binding: t; -*-

(require 'embark)

(require '00-my-core-macros)
;;;###autoload
(progn
  (defun my-setup-embark ()
    (when (fboundp #'embark-act)
      ;; don't use `embark-prefix-help-command', the default displayed interface
      ;; is arguably more readable than the former's exported view
      (setq prefix-help-command #'describe-prefix-bindings)
      ;; Shift was chosen as the modifier so that [A-Z] keys (capitalized,
      ;; there shifting) in `embark-general-map' can be pressed easier
      (my-bind '("M-S-<menu>") #'my-embark-act-no-quit)
      (my-bind :map 'minibuffer-mode-map "C-c C-o" #'my-embark-export)
      (my-bind :map 'minibuffer-mode-map "C-c C-e" #'my-embark-writable-export)
      (my-bind [remap describe-bindings] #'embark-bindings))))

(with-eval-after-load 'consult
  (require 'embark-consult nil 'noerror))

;; Don't close the minibuffer when an unpresented command is invoked
(my-add-advice/s #'embark--act
  :around #'my-embark--act-dont-quit-when-unexpected-command--a)
;; TODO: customize `embark-quit-after-action' to be an alist instead?

;; (defun my-embark-keymap-prompter-protect-other-keys--a (func keymap update &rest args)
;;   (let ((cmd (apply func keymap update args)))
;;     (cond
;;      ;; `right-char' as `[right]' even when not bound?
;;      ;; ((where-is-internal cmd keymap) cmd)
;;      ((or (member cmd (seq-filter #'commandp (flatten-tree keymap)))
;;           (not (minibufferp)))
;;       cmd)
;;      (t
;;       (apply #'embark-keymap-prompter keymap update args)))))
;; (my-add-advice/s #'embark-keymap-prompter :around
;;   #'my-embark-keymap-prompter-protect-other-keys--a)

(setq embark-verbose-indicator-display-action
      '(display-buffer-at-bottom (window-height . fit-window-to-buffer)))

;; NOTE: if command has an interactive form, `embark' will (may?) use that
;; instead of the highlighted thing

(my-bind :map 'embark-file-map "M-f" #'my-find-file-with-initial)
(my-bind :map 'embark-file-map "M-o" #'my-embark-xdg-open-file)
(my-bind :map 'embark-file-map "M-t" #'my-terminal-external-emulator)
(my-bind :map 'embark-general-map "M-s" #'my-speech-speak-text)
;; (my-bind :map 'embark-identifier-map "d" #'my-embark-definition)
;; (my-bind :map 'embark-symbol-map "d" #'my-embark-elisp-definition)

(set-popup-rule! "\\`\\*Embark " :height (/ 1.0 2) :ttl nil :quit nil)

;; ;; transform into completing-read instead of just pressing a single key
;; (with-eval-after-load 'vertico-multiform
;;   (add-to-list 'vertico-multiform-categories '(embark-keybinding grid)))
;; (setq embark-prompter #'embark-completing-read-prompter)

;; https://github.com/oantolin/embark/wiki/Additional-Configuration#use-which-key-like-a-key-menu-prompt
(with-eval-after-load 'which-key
  (when which-key-mode
    (setq embark-indicators
          '(embark-which-key-indicator
            embark-highlight-indicator embark-isearch-highlight-indicator))
    (advice-add
     #'embark-completing-read-prompter
     :around #'embark-hide-which-key-indicator)))

(provide 'my-after-embark)

;; Local Variables:
;; no-byte-compile: t
;; End:

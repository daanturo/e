;; -*- lexical-binding: t; -*-

(require 'dash)

(add-hook
 'my-execu-cmd-alist
 (cons
  'java-mode
  (lambda (file-name)
    (format "javac %S && java %S"
            file-name
            (file-name-sans-extension file-name)))))

(my-evalu-set-handler 'java-mode '("jshell"))

(provide 'my-hook-java-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

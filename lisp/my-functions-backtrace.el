;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-backtrace (&optional return-str)
  (progn
    (message "%s:\n" #'my-backtrace)
    (defvar backtrace-line-length)
    (let ((backtrace-line-length nil))
      (if (bound-and-true-p return-str)
          (backtrace-to-string (backtrace-get-frames 'backtrace))
        (backtrace)))))

;;;###autoload
(defun my-backtrace-before-a (&rest _)
  (my-backtrace))

;;;###autoload
(defun my-backtrace-after-load (lib)
  (eval-after-load lib #'my-backtrace))

;;;###autoload
(defun my-backtrace-when-error-a (func &rest args)
  (condition-case err
      (apply func args)
    (error (message "%S" err) (my-backtrace))))

;;;###autoload
(defun my-backtrace-before-a (&rest _)
  (my-backtrace))

;;;###autoload
(defun my-quit-and-view-backtrace ()
  (interactive)
  (dlet ((debug-on-quit t))
    (call-interactively (key-binding (kbd "C-g")))))

;;;###autoload
(defun my-backtrace-expand-all ()
  ;; (declare (obsolete backtrace-expand-ellipses "0"))
  (interactive)
  (save-excursion
    (goto-char (point-max))
    (while (and (search-backward "..." nil 'noerror) (button-at (point)))
      (push-button)
      (goto-char (point-max)))))

(defvar my-backtrace-save--history '())

;;;###autoload
(defun my-backtrace-save (&optional dest-file)
  (interactive)
  (my-backtrace-expand-all)
  (-let* ((line-fn
           (lambda ()
             (-->
              (my-get-line-string-at-position)
              (string-trim it)
              (url-hexify-string it))))
          (name
           (save-match-data
             (or
              ;; get to the first `command-execute'
              (save-excursion
                (goto-char (point-min))
                (when (re-search-forward "^\s*command-execute(" nil t)
                  (funcall line-fn)))
              (save-excursion
                (goto-char (point-max))
                (skip-chars-backward "\n")
                (funcall line-fn))))))
    (-let* ((dest-file
             (or dest-file
                 (file-name-concat my-user-emacs-local-dir/
                                   "log"
                                   (format "backtrace-%s-%s"
                                           (my-ISO-time)
                                           name))))

            (buf-str
             (buffer-substring-no-properties
              (point-min) (point-max))))
      (when (not (member buf-str my-backtrace-save--history))
        (push buf-str my-backtrace-save--history)
        (make-directory (file-name-parent-directory dest-file)
                        'parents)
        ;; (write-region (point-min) (point-max) dest-file)
        ;; (write-region buf-str nil dest-file)
        ;; `f-write-text' handles the interactive `select-safe-coding-system'
        (f-write-text buf-str 'utf-8 dest-file)
        (message "`%s': %s" #'my-backtrace-save dest-file)
        dest-file))))

(defun my-backtrace-auto-save-mode--auto-h ()
  (my-backtrace-save))

(defun my-backtrace-auto-save-mode--setup (&optional remove)
  (if (not remove)
      (add-hook 'pre-command-hook #'my-backtrace-auto-save-mode--auto-h
                nil
                'local)
    (remove-hook 'pre-command-hook #'my-backtrace-auto-save-mode--auto-h
                 'local)))

;;;###autoload
(define-minor-mode my-backtrace-auto-save-mode
  "Use this when the *Backtrace* buffer changes before we can do anything."
  :global
  t
  (-let* ((set-all-fn
           (lambda ()
             (dolist (buf (buffer-list))
               (with-current-buffer buf
                 (when (derived-mode-p 'backtrace-mode)
                   (my-backtrace-auto-save-mode--setup
                    (not my-backtrace-auto-save-mode))))))))
    (if my-backtrace-auto-save-mode
        (progn
          (add-hook
           'backtrace-mode-hook #'my-backtrace-auto-save-mode--setup)
          (funcall set-all-fn))
      (progn
        (remove-hook
         'backtrace-mode-hook #'my-backtrace-auto-save-mode--setup)
        (funcall set-all-fn)))))

;;; my-functions-backtrace.el ends here

(provide 'my-functions-backtrace)

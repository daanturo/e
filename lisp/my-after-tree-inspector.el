;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;; (advice-add
;;  #'tree-inspector-inspect
;;  :around #'my-with-switch-to-buffer-other-window->pop-to-buffer-a)

;; (set-popup-rule! "\\`\\*tree-inspector.*\\*" :modeline nil)

(add-hook 'my-evil-or-leader-mode-alist '("\\`\\*tree-inspector\\*" my-leader-mode))

;;; after-tree-inspector.el ends here

(provide 'my-after-tree-inspector)

;; Local Variables:
;; no-byte-compile: t
;; End:

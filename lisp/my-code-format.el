;; -*- lexical-binding: t; -*-

(require '00-my-core-macros)
(require 'dash)

;; ;;;###autoload
;; (defalias #'set-formatter! #'ignore)

;; Packages required: `apheleia'

;;;###autoload
(defun my-lsp-format-feature-p (&optional region)
  (and (bound-and-true-p lsp-mode)
       (lsp-feature?
        (if region
            "textDocument/rangeFormatting"
          "textDocument/formatting"))))

;;;###autoload
(defun my-code-format-region|selection-or-line (beg end)
  (interactive (cond
                ((use-region-p)
                 (list (region-beginning) (region-end)))
                (:else
                 (list (line-beginning-position) (line-end-position)))))
  (cond
   ((my-lsp-format-feature-p :region)
    (lsp-format-region beg end))
   (t
    (format-all-region beg end))))

;;;###autoload
(defun my-code-format-dwim ()
  "Format the active region or the entire buffer."
  (interactive)
  (if (use-region-p)
      (if (my-lsp-format-feature-p :region)
          (lsp-format-region (region-beginning) (region-end))
        (indent-region (region-beginning) (region-end)))
    (save-window-excursion
      (my-code-format-buffer))))

(defvar my-code-format-indent-modes '(prog-mode))

;;;###autoload
(defun my-code-format-buffer (&optional callback sentinel)
  "Format current buffer.
CALLBACK is passed to`apheleia-format-buffer', or call after in
other cases. See `set-process-sentinel' for SENTINEL."
  (interactive)
  (-let* ((got-apheleia-formatter (my-apheleia-get-formatters-safe)))
    (cond
     (got-apheleia-formatter
      (-let* ((proc (apheleia-format-buffer got-apheleia-formatter callback))
              (sentinel0 (process-sentinel proc)))
        (when sentinel
          (set-process-sentinel
           proc
           (lambda (_proc event)
             (funcall sentinel0 proc event)
             (funcall sentinel proc event))))
        proc))
     ((apply #'derived-mode-p my-code-format-indent-modes)
      (my-indent-buffer)
      (my-safe-call callback)))))

(provide 'my-code-format)

;; -*- lexical-binding: t; -*-

;;;###autoload
(cl-defun my-permute-sequence-by-table (table seq &optional (index 0))
  "Collect in SEQ, TABLE[]-nth elements."
  (declare (pure t) (side-effect-free t))
  (seq-map (-cut nth (- <> index) seq)
           table))

(defun my-colorize-maybe (col &optional comp dim forced)
  "Colorize `COL' (string) with `COMP' (\"r\" (default), \"g\" or \"b\").
With non-nil `DIM', make the color not too bright.
With non-nil `FORCED', apply even if saturation is not low."
  (interactive "p")
  (let* ((bonus-seq (pcase comp
                      ("g" '(0 1 0))
                      ("b" '(0 0 1))
                      (_ '(1 0 0))))
         (rgb (color-name-to-rgb col))
         (no-gray-rgb (if (and (> (sqrt (my-variance rgb)) (/ 1.0 8))
                               (not forced))
                          rgb
                        (cl-mapcar (lambda (bonus elem)
                                     (/ (+ bonus elem) 2.0))
                                   bonus-seq rgb)))
         (new-rgb (if (and dim (> (apply '+ no-gray-rgb) 2))
                      (-map (lambda (arg) (/ arg 2.0)) no-gray-rgb)
                    no-gray-rgb)))
    (color-rgb-to-hex (nth 0 new-rgb) (nth 1 new-rgb) (nth 2 new-rgb) 2)))

;;;###autoload
(defun my-divible-by-7? (num)
  "Return if NUM is divisible by 7 (10m +n ⋮ 7 ⇔ m - 2n ⋮ 7)."
  (named-let recur ((n num))
    (cond
     ((cl-member n '(-7 0 7) :test '=)
      t)
     ((< -14 n 14)
      nil)
     (t
      (-let* ((abs-val (abs n)))
        (recur (- (floor (/ abs-val 10))
                  (* 2 (mod abs-val 10)))))))))

(provide 'my-fun-lib)

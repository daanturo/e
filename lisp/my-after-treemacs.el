;; -*- lexical-binding: t; -*-


(require 'dash)
(require '00-my-core-macros)

(require 'treemacs)

;;;###autoload
(progn
  (defvar-local my-sidebar-local-enable nil
    "Set to \"false\" to explicitly disable.")
  ;; Don't load just for this hook
  (defun my-sidebar-local-enable-h (&rest _)
    (setq-local my-sidebar-local-enable t)))

;; NOTE: need speed for all
(defun my-treemacs-show (&rest _)
  (interactive)
  (save-selected-window
    (treemacs-without-messages
     ;; (unless (treemacs-get-local-window) (treemacs))
     (with-timeout ((/ 1.0 64))
       (treemacs-add-and-display-current-project-exclusively)))))
(defun my-treemacs-quit (&rest _)
  (interactive)
  (when (treemacs-get-local-window)
    (treemacs)))
(defvar my-sidebar-echo nil)
(defun my-treemacs-should-show-p ()
  "Don't show should the narrowest window (except `treemacs')'s
width is < ~ 1/2 of pre-treemacs `split-width-threshold'."
  (let ((treemacs-window (treemacs-get-local-window)))
    (and
     (or buffer-file-name treemacs-window my-sidebar-local-enable)
     (not (member my-sidebar-local-enable '(:false "false")))
     (let ((min-window-width
            (-->
             (remove treemacs-window (window-list))
             (-map #'window-width it)
             -min)))
       (< (/ my-split-width-threshold-0 2)
          (if treemacs-window
              ;; When `treemacs' is visible, does the narrowest window look
              ;; reasonable?
              min-window-width
            ;; When `treemacs' isn't visible, does the narrowest window look
            ;; reasonable after having the to-be-shown `treemacs' window's width
            ;; taken into account?
            (- min-window-width treemacs-width)))))))
(defun my-treemacs-no-select-maybe-h (&rest _)
  (interactive)
  (unless (or
           ;; may rapidly change windows and frames, which reduces performance
           completion-in-region-mode
           (minibufferp) (file-remote-p default-directory))
    (dlet ((treemacs-pulse-on-success nil) (treemacs-pulse-on-failure nil))
      (if (my-treemacs-should-show-p)
          (my-treemacs-show)
        (my-treemacs-quit)))))
;;;###autoload
(define-minor-mode my-auto-sidebar-mode
  "Always show a left sidebar where applicable."
  :global t
  (progn
    (setq treemacs-width my-sidebar-width)
    (if my-auto-sidebar-mode
        (progn
          (treemacs-project-follow-mode)
          (my-add-hook/s '(window-state-change-functions) #'(my-treemacs-no-select-maybe-h)))
      (progn
        (my-remove-hook/s '(window-state-change-functions)  #'(my-treemacs-no-select-maybe-h))))))


;;;###autoload
(defconst my-sidebar-width 28)

;;;###autoload
(progn
  (defun my-setup-treemacs ()
    
    (setq-default treemacs-missing-project-action 'remove)
    (setq-default treemacs-persist-file nil) ; no need when `treemacs-project-follow-mode'
    (setq-default treemacs-user-mode-line-format 'none)
    
    (when (fboundp #'treemacs)

      ;; Prevent the default theme from being set from the first place by
      ;; intercepting the first call of `treemacs-load-theme' in
      ;; `treemacs-icons.el'; TODO: feature request customizable treemacs theme
      ;; name that would be loaded from the call mentioned above

      (my-add-advice-once
        #'treemacs-load-theme
        :around
        (lambda (func theme-name &rest args)
          (cond
           ((and (member theme-name '("Default"))
                 (require 'treemacs-nerd-icons nil 'noerror))
            (apply func "nerd-icons" args))
           (t
            (apply func theme-name args)))))

      ;; (my-add-hook-once 'find-file-hook 'my-auto-sidebar-mode)
      ;; (my-add-startup-hook
      ;;   (my-add-hook-once
      ;;     'pre-command-hook
      ;;     (my-fn%
      ;;      (my-add-hook-once
      ;;        'window-state-change-hook 'my-auto-sidebar-mode))))

      (-let* ((func (lambda ()
                      (require 'treemacs)
                      (my-auto-sidebar-mode))))
        (make-thread func)))))


(define-key treemacs-mode-map (kbd "<mouse-2>") 'bs-show)

;; ignore the sidebar before performing those operations
(my-add-advice/s '(rotate-layout org-todo) :before #'my-treemacs-quit)
(add-hook 'my-cycle-window-direction-before-hook #'my-treemacs-quit)

(defvar my-split-width-threshold-0 split-width-threshold "Original `split-width-threshold'.")
(setq split-width-threshold (- my-split-width-threshold-0 treemacs-width)) ;splitting on the r/l happens before hiding `treemacs'

;; (my-demote-errors-from #'treemacs--apply-annotations-deferred)
(my-demote-errors-from #'treemacs--follow)

(treemacs-follow-mode)

;; (defun my-disable-treemacs-follow-mode-temporarily (&rest _)
;;   (treemacs-follow-mode 0)
;;   (dlet ((inhibit-message t))
;;     (message "Disabled `%s' until the next command." #'treemacs-follow-mode))
;;   (my-add-hook-once 'pre-command-hook #'treemacs-follow-mode))
;; ;; TODO fix `buffer-list-update-hook' is constantly updated, which leads to
;; ;; unreasonable CPU usage by `treemacs-follow-mode', even when idle, use this as
;; ;; a WORKAROUND
;; (defvar my-treemacs-follow-mode-auto-disable-timer nil)
;; (unless my-treemacs-follow-mode-auto-disable-timer
;;   (setq my-treemacs-follow-mode-auto-disable-timer
;;         (run-with-idle-timer auto-save-timeout 'repeat
;;                              #'my-disable-treemacs-follow-mode-temporarily)))

(add-hook 'my-refresh-find|open-current-file-after-hook #'my-treemacs-kill)

;; (set-popup-rule!
;;  "\\` \\*Treemacs-Scoped-Buffer-#<frame [^*]*\\*"
;;  :ignore t
;;  ;; :side 'left
;;  :modeline nil)
;; ;; :width my-sidebar-width


;; ;; Prevent loading `treemacs' theme before loading `emacs' theme
;; (my-when-graphical
;;  (setq doom-themes-treemacs-theme "all-the-icons") ; Doom forcefully `treemacs-load-theme'
;;  (when (require 'treemacs-all-the-icons nil 'noerror)
;;    (treemacs-load-theme "all-the-icons")))

(provide 'my-after-treemacs)

;; Local Variables:
;; no-byte-compile: nil
;; End:

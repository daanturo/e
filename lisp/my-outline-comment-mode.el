;; -*- lexical-binding: t; -*-

;; (my-backtrace)

(require 'dash)
(require 'outline)

(require '00-my-core-macros)


;;; Mode

;;;###autoload
(define-minor-mode my-outline-minor-highlight-mode
  nil
  :global
  nil
  (if my-outline-minor-highlight-mode
      (progn
        ;; Ought to be set before applying `outline-minor-mode'
        (setq-local outline-minor-mode-highlight 'override)
        (outline-minor-mode))
    (progn
      (kill-local-variable 'outline-minor-mode-highlight))))

;;;###autoload
(define-minor-mode my-outline-comment-mode
  "Recognize '`comment-start'''`comment-start''+\\*+ comments as `outline' headings."
  :global
  nil
  (if my-outline-comment-mode
      (progn
        (setq-local outline-regexp (my-outline-regexp))
        (setq-local outline-level #'my-outline-comment-level)
        (my-outline-minor-highlight-mode))
    (progn
      (my-outline-minor-highlight-mode 0))))


;;;###autoload
(progn

  (defun my-should-outline-comment-mode-p ()
    (and
     ;; unexpected non-comment outlines
     (null (bound-and-true-p outline-search-function))
     ;; Don't enable when line comments are not available
     (my-trimmed-comment-beg)
     ;; dir/file sets local `outline-regexp' explicitly (therefore maybe
     ;; non-comments), this must be evaluated after setting local variables!
     (not
      (or (alist-get 'outline-regexp dir-local-variables-alist)
          (alist-get 'outline-regexp file-local-variables-alist)))))

  ;; dump to the autoload file to prevent recursive load
  (defun my-enable-outline-comment-mode-maybe--after-local-vars-h ()

    ;; ;; ignore `treesit-outline-search', prefer comment-based outlines
    ;; (my-kill-local-variable-with-matching-name
    ;;  'outline-search-function
    ;;  (rx-to-string '(seq word-boundary (or "treesit") word-boundary) t))

    (cond
     ((my-should-outline-comment-mode-p)
      (my-outline-comment-mode))
     (:else
      (outline-minor-mode))))

  (defun my-enable-outline-comment-mode-maybe ()
    (add-hook 'hack-local-variables-hook
              #'my-enable-outline-comment-mode-maybe--after-local-vars-h
              nil
              'local)))

;;;###autoload
(defun my-outline-comment-level ()
  "Should be compatible with `lisp-data-mode''s `outline-regexp'."
  ;; (let ((with-space (concat (my-trimmed-comment-beg) (my-trimmed-comment-beg) " ")))
  ;;   (if (looking-at-p (regexp-quote with-space))
  ;;       (- (match-end 0) (length with-space)) ;; *
  ;;     (- (match-end 0) (length with-space) -1) ;;;
  ;;     ))
  (my-cond-let
   ([b (match-beginning 2) e (match-end 2)]
    (- e b))
   ([b (match-beginning 1) e (match-end 1)]
    (- e b))
   ([b (match-beginning 0) e (match-end 0)]
    (- e b))))

;;;###autoload
(defun my-outline-regexp ()
  (my-save-in-local-variable my-outline-regexp
    (-some--> (my-trimmed-comment-beg) my-compute-outline-regexp)))

(defconst my-comment-outline-regexp-alist '()
  "`outline-regexp's computed by trimmed `comment-start's.")
(defun my-compute-outline-regexp (cb)
  "Should be compatible with `lisp-outline-level' for lisp modes.
CB: `my-trimmed-comment-beg'."
  (or (my-get-alist my-comment-outline-regexp-alist cb)
      (let ((re (concat cb cb
                        (rx-to-string
                         `(or (group (+ ,cb))
                              (seq (?  " ") (group (+ "*")))))
                        ;; not consecutive spaces, NOTE this will make
                        ;; `outline-insert-heading' inserts caught characters
                        ;; used for negation
                        " \\($\\|[^ \t]\\)")))
        (push (cons cb re) my-comment-outline-regexp-alist)
        re)))

(defvar my-trimmed-comment-beg-alist
  `((c-mode . "//")))
(defvar-local my-trimmed-comment-beg--cache nil)
;;;###autoload
(defun my-trimmed-comment-beg ()
  "Return the local comment beginning.
Only line comments are supported."
  (and comment-start
       (with-memoization my-trimmed-comment-beg--cache
         (cond
          ;; 2 cases below: special languages where `comment-start' isn't
          ;; line-based
          ((alist-get major-mode my-trimmed-comment-beg-alist))
          ((length> comment-end 0)
           nil)
          (t
           (string-trim-right comment-start))))))

;;;###autoload
(defun my-trimmed-comment-beg* (&optional n)
  (when-let ((cb (my-trimmed-comment-beg)))
    (my-multiply-string (or n 1) cb)))

(defconst my-lisp-outline-regexp (my-compute-outline-regexp ";"))

;;; Workarounds

(setq +emacs-lisp-outline-regexp my-lisp-outline-regexp)

;; lispy-mode-hook      ; `lispy' doesn't recognize ";;;;" as level 2
(setq lispy-outline (concat "^" my-lisp-outline-regexp))
(advice-add #'lispy-outline-level :override #'lisp-outline-level)

(advice-add #'doom--setq-outline-level-for-emacs-lisp-mode-h :override #'ignore)
(advice-add #'doom--setq-outline-regexp-for-emacs-lisp-mode-h :override #'ignore)

(provide 'my-outline-comment-mode)

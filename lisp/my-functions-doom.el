;; -*- lexical-binding: t; -*-

;; I don't use Doom anymore. There are many features that I feel like
;; getting in the way and a lot code are already dedicated to
;; disabling them. Also commit
;; 8cafbe4408e7d6c68947a2066e5837379d0983c1
;; (https://github.com/doomemacs/doomemacs/commit/8cafbe4408e7d6c68947a2066e5837379d0983c1)
;; broke my config without even giving a backtrace. Therefore I gave
;; up.

(require '00-my-core-macros)

(require 'dash)

(autoload 'straight--repos-dir "straight")
(autoload 'straight--build-dir "straight")

;;;###autoload
(defun my-doom-module-locate-path-all-list (category module)
  (-let* ((default-path (doom-module-locate-path category module))
          (other-paths
           (my-with-advice
             #'doom-module-expand-path
             :override #'ignore
             (-map
              (lambda (exclude-path)
                (dlet ((doom-module-load-path
                        (remove exclude-path doom-module-load-path)))
                  (doom-module-locate-path category module)))
              doom-module-load-path))))
    (delete-dups `(,default-path ,@other-paths))))

;;;###autoload
(defun my-get-doom-module-flags (category module)
  "Return a list of available flags as symbols of MODULE in CATEGORY."
  (-uniq
   (flatten-tree
    (cl-loop
     for file in
     (-->
      (my-doom-module-locate-path-all-list category module)
      (-mapcat (lambda (path) (directory-files-recursively path "\\.el$")) it))
     collect
     (cl-loop
      for match in
      (s-match-strings-all
       "(modulep! \\(\\+.*?\\))" (f-read file))
      collect (intern (cadr match)))))))

;;;###autoload
(defun my-install-new-local-languages||doom-modules (cat-mod-list)
  (interactive
   (let ((candidates (cl-loop for (cate . modu) in (doom-module-list 'all)
                              collect (format "%s %s" cate modu))))
     (list
      (completing-read-multiple
       "Module: " candidates
       nil nil nil nil
       (-find (lambda (cand)
                (--> (string-remove-suffix "-mode" (symbol-name major-mode))
                     (string-search it cand)))
              candidates)))))
  (let ((add-list (cl-loop
                   for str in cat-mod-list collect
                   (let* ((cate (intern (car (split-string str))))
                          (modu (intern (cadr (split-string str))))
                          (flags (mapconcat #'symbol-name (my-get-doom-module-flags cate modu) " ")))
                     (format "%s" `(doom! ,cate (,modu ,flags)))))))
    (with-current-buffer
        (my-append-to-file-interactively
         (concat "\n"
                 (string-join add-list "\n"))
         (my-ensure-empty-elisp-file my-local-init-el))
      (my-ask-for-emacs-refresh))))

;;;###autoload
(defun my-list-unused-doom-modules-and-flags ()
  "List unused Doom modules and modules with unused flags."
  (interactive)
  (let ((temp-buffer-name (generate-new-buffer-name
                           "modules")))
    (with-output-to-temp-buffer
        temp-buffer-name
      (seq-map
       #'princ-list
       (seq-remove
        #'null
        (cl-loop for pair in (doom-module-list 'all)
                 collect
                 (-let* (((category . module) pair)
                         (supported-flags (my-get-doom-module-flags category module))
                         (unused-flags (seq-difference
                                        supported-flags
                                        (plist-get (gethash pair doom-modules) :flags))))
                   (when (and (or (null (gethash pair doom-modules))
                                  unused-flags)
                              ;; (when (equal category :lang)
                              ;;   (> (length supported-flags) 0))
                              )
                     (cons category (append `(,module) unused-flags))))))))
    (switch-to-buffer-other-window temp-buffer-name)
    (emacs-lisp-mode)))

;;;###autoload
(defun my-add-doom-bin/doom-to-path (profile-file)
  "Add bin/doom utility to PATH."
  (interactive (list
                (read-file-name "Profile file location: "
                                "~/" nil nil
                                "profile")))
  (let ((command-to-add
         (concat
          "export PATH=\"$HOME/"
          (file-relative-name (file-name-concat doom-emacs-dir "bin") "~")
          ":$PATH\"")))
    (my-append-to-file-interactively command-to-add profile-file)))

;;;###autoload
(defun my-doom-bin ()
  (file-name-concat doom-bin-dir "doom"))

;;;###autoload
(defun my-doom-emacs-sync-and-restart ()
  (interactive)
  (my-require-doom-noninteractive-defs)
  (my-process-shell-async
    (concat (my-doom-bin) " -! sync")
    (lambda (&rest _)
      (dlet ((confirm-kill-emacs nil))
        (call-interactively
         (my-1st-fn 'doom/restart-and-restore 'restart-emacs))))))

;;;###autoload
(defun my-doom/open-scratch-buffer--elisp-a (func &rest args)
  (dlet ((doom-scratch-initial-major-mode
          (cond
           ((and (equal t doom-scratch-initial-major-mode)
                 (member major-mode '(emacs-lisp-mode)))
            'lisp-interaction-mode)
           (t doom-scratch-initial-major-mode))))
    (apply func args)))

;;;###autoload
(defun my-load-doom-autoloads-file ()
  (interactive)
  (require 'doom-profiles)
  (load (file-name-sans-extension
         (file-name-concat doom-profile-dir
                           doom-profile-init-file-name))))

(defvar my-doom-noninteractive-loaded nil "")

;;;###autoload
(defun my-require-doom-noninteractive-defs ()
  (unless my-doom-noninteractive-loaded
    ;; `doom-bin'
    (load (file-name-concat doom-emacs-dir "lisp/lib/config"))
    ;; ;; CLIs
    ;; (load (file-name-concat doom-emacs-dir "core/cli/lib/lib"))
    ;; (load (file-name-concat doom-emacs-dir "core/core-cli"))
    ;; (dolist (f (directory-files-recursively (file-name-concat doom-emacs-dir "core/cli")
    ;;                                         "\\.el$"))
    ;;   (load (string-remove-suffix ".el" f)))
    (setq my-doom-noninteractive-loaded t)))

;;;###autoload
(defun my-ask-for-emacs-refresh ()
  (my-y-or-n-forms
    '(save-buffer)
    (pcase my-emacs-kit
      (`doom '(my-execute-doom-cli-and-reload-autoloads 'sync))
      (_ '(eval-buffer)))))

;;;###autoload
(cl-defun my-execute-doom-cli-and-reload-autoloads (&optional (command 'sync) (reload-autloads t))
  (interactive (list
                (if current-prefix-arg
                    (completing-read "Doom command: " '(sync upgrade clean))
                  'sync)))
  (my-require-doom-noninteractive-defs)
  (let* ((final-command
          (concat
           (abbreviate-file-name (my-doom-bin)) (format " -! %s" command)))
         (buf-name (format "*%s %s*" (format-time-string "%T") final-command))
         (output-buffer (get-buffer-create buf-name))
         (running-proc (get-buffer-process output-buffer)))
    (when (or (not running-proc)
              (when (y-or-n-p (format "Kill %s?" output-buffer))
                (my-kill-buffer-no-ask buf-name)
                (get-buffer-create buf-name)
                t))
      (my-with-interactive-sh
       (my-process-shell-async
         final-command
         (lambda (process &rest _)
           (when (member (process-status process) '(exit signal))
             (when reload-autloads
               (my-load-doom-autoloads-file))))
         :buffer output-buffer)))))

;;;###autoload
(defun my-doom-reinstall-straight ()
  (interactive)
  (let ((dirs (--> (list (file-name-concat straight-base-dir "straight" straight-build-dir "straight")
                         (file-name-concat straight-base-dir "straight" straight-build-dir "straight.el")
                         (file-name-concat straight-base-dir "straight" "repos" "straight")
                         (file-name-concat straight-base-dir "straight" "repos" "straight.el"))
                   (-map #'abbreviate-file-name it))))
    (shell-command (my-confirm-shell-command t
                     (format "rm -rf \\\n%s" (string-join dirs " \\\n"))))
    (my-execute-doom-cli-and-reload-autoloads)))

;;;###autoload
(defun my-update-doom-packages (packages &optional ask-for-confirm)
  (interactive (list
                (-->
                 (my-completing-read-multiple-filenames
                  "Package: " (straight--repos-dir))
                 (-map 'file-name-base it))
                t))
  (let* ((commands
          (-map
           (-lambda (package)
             (format "cd %s ; git pull --rebase; rm -rf %s ; \\\n"
                     (f-abbrev (straight--repos-dir package))
                     (f-abbrev (straight--build-dir package))))
           packages))
         (final-command
          (format "%s cd %s ; \\\n doom -! sync"
                  (apply #'concat commands)
                  (f-short default-directory))))
    (async-shell-command
     (if ask-for-confirm
         (read-shell-command "Execute: " final-command)
       final-command))))

(defun my-doom--re*-read-package (build-dir)
  (require 'straight)
  (-->
   (read-directory-name "Package: "
                        (if build-dir
                            (straight--build-dir)
                          (straight--repos-dir)))
   (f-filename it)))

;;;###autoload
(defun my-doom-rebuild-package (package &optional ask-for-confirm re-install)
  (interactive (list (my-doom--re*-read-package t) t nil))
  (require 'straight)
  (let* ((build-path (f-abbrev (straight--build-dir package)))
         (source-path (f-abbrev (straight--repos-dir package)))
         (cmd
          (concat
           (and re-install (format "rm -rf %s ; " source-path))
           (format "rm -rf %s ; " build-path)
           "doom -! sync")))
    (async-shell-command
     (if ask-for-confirm
         (read-shell-command "Execute: " cmd)
       cmd))))

;;;###autoload
(defun my-doom-reinstall-package (package &optional ask-for-confirm)
  (interactive (list (my-doom--re*-read-package nil) t))
  (my-doom-rebuild-package package ask-for-confirm t))

;;;###autoload
(defun my-doom-installed-packages (&optional user-only)
  (doom-initialize-packages)
  (-keep (-lambda ((package . plist))
           (and
            ;; by inspect `doom-packages'
            (if user-only
                (equal '((:user) (:user . modules))
                       (plist-get plist :modules))
              t)
            (cons package
                  (my-plist-remove plist :modules))))
         doom-packages))

;;;###autoload
(defun my-doom--report-installed-packages (&optional user-only one-line)
  "ONE-LINE: squeeze into one line."
  (interactive "P")
  (-let* ((buf (get-buffer-create (format "*%s*" #'my-doom--report-installed-packages)))
          (packages (my-doom-installed-packages user-only)))
    (with-current-buffer buf
      (lisp-data-mode)
      (with-silent-modifications
        (erase-buffer)
        (insert (if one-line
                    (--> (-map #'my-str packages)
                         (string-join it " "))
                  (pp packages)))))
    (pop-to-buffer buf)))

;;;###autoload
(defun my-doom-report-user-installed-packages (&optional multi-line)
  "MULTI-LINE: don't squeeze into one line."
  (interactive "P")
  (my-doom--report-installed-packages t (not multi-line)))

;;;###autoload
(defun my-doom-report-all-installed-packages ()
  (interactive)
  (my-doom--report-installed-packages))

;;;###autoload
(defun my-doom-emacs-sync-and-reload-current-elisp (&optional base-file)
  (interactive (list (file-name-base buffer-file-name)))
  (my-process-shell-async "doom sync -v" (lambda (&rest _) (load base-file))))

;;;###autoload
(defun my-doom-display-init-time ()
  (interactive)
  (doom-display-benchmark-h))

;;;###autoload
(cl-defun my-doom-use-external-module
    (url category module flags &rest plist &key rev &allow-other-keys)
  (let* ((unique-name (replace-regexp-in-string "/" "_" (concat url "/" rev)))
         (root-dir (apply #'my-useelisp-repo unique-name url plist))
         (category-str (string-remove-prefix ":" (symbol-name category)))
         (module-str (symbol-name module))
         (module-dir0
          (file-name-concat root-dir "modules" category-str module-str))
         (category-dir1 (file-name-concat my-modules-dir/ category-str))
         (link1 (file-name-concat category-dir1 module-str)))
    (make-directory category-dir1 'parents)
    ;; delete the broken symlink
    (unless (file-exists-p (file-truename link1))
      (delete-file link1))
    (unless (file-exists-p link1)
      (make-symbolic-link module-dir0 link1))
    (eval `(doom! ,category (,module ,@flags)) t)))

;;;###autoload
(defun my-doom--timestamped-message-a--omit-seconds-a
    (func format-str &rest args)
  (cond
   ;; no useful information, don't insert timestamp to clutter the message
   ;; buffer
   ((member format-str '(nil ""))
    nil)
   (t
    (my-with-advice
      #'format-time-string
      :around
      (lambda (func1 time-format-str &rest args1)
        (apply func1
               (if (string-search "%T" time-format-str)
                   "[%F %H:%M]"
                 time-format-str)
               args1))
      (apply func format-str args)))))

;; Doom's `+snippets--disable-smartparens-before-expand-h' need this defined
;;;###autoload
(defvar smartparens-mode nil)

;;;###autoload
(defun my-ignore-doom-doc-org-mode ()
  (interactive)
  (defalias #'doom-docs-org-mode #'org-mode))

;;;###autoload
(progn

  (defun my-doom-dashboard-cd-home (&rest _)
    (when (eq major-mode '+doom-dashboard-mode)
      (cd "~")))

  (defun my-disable-debug-on-quit-unless-doom-debug-mode-h (&rest _)
    (if doom-debug-mode
        nil
      (setq debug-on-quit nil)))

  (defun my-doom-compile-functions-a (func &rest fns)
    (apply func
           (-difference
            fns
            '(
              ;; prevent "Function <f> is already compiled"
              +emacs-lisp-truncate-pin
              ;;
              ))))

  (defun my-doom-load-exclude-a (func path &rest args)
    (unless (member
             path
             (list
              ;; custom-file
              ))
      (apply func path args)))

  (defvar my-doom-+word-wrap-ignore-mode-list
    '( ;
      sgml-mode ; staggering lag in `mhtml-mode', TODO: report
      ))

  (defun my-doom-+word-wrap-mode-maybe-h ()
    (unless (apply #'derived-mode-p
                   my-doom-+word-wrap-ignore-mode-list)
      (+word-wrap-mode)))

  (defvar my-doom-use-package!-exclude-list
    '(dap-mode emmet-mode projectile rjsx-mode web-mode))

  (defun my-doom-use-package!-exclude-a (fn name &rest args)
    (cond
     ((not (member name my-doom-use-package!-exclude-list))
      (apply fn name args))
     (:else
      ;; (message "`my-doom-use-package!-exclude-a': ignoring %s" name)
      nil)))

  (defun my-doom-emacs-preface ()

    (advice-add
     #'use-package!
     :around #'my-doom-use-package!-exclude-a)

    (defalias #'set-formatter! #'ignore)

    ;; ;; re-enable menu bar
    ;; (setq default-frame-alist
    ;;       (delete '(menu-bar-lines . 0) default-frame-alist))
    ;; (setq menu-bar-mode t)
    ;; ;; why not enable: the menu bar's background color doesn't change after
    ;; ;; changing the DE's theme/color scheme until restarting Emacs

    ;; Set early to let other rules inherit it's values. https://github.com/doomemacs/doomemacs/issues/6872
    (setq +popup-defaults
          `(:side
            bottom
            :height
            ,(alist-get 'window-height my-windowpopup-default-action-alist)
            :quit t
            :select ignore
            :ttl nil
            :modeline t))

    (defconst my-reserved-keys
      (let
          ((l
            (append
             '("<C-tab>" ; switching buffers
               ;; state-specific bindings override local ones
               "S-RET" "S-<return>" "C-RET" "C-<return>"
               "C-S-f" ; forward and mark a character(s)
               "<backspace>" ; `+snippets/delete-backward-char' gets in the way of deleting sometimes
               "TAB" "<tab>" ; control this key
               "M-/") ;
             ;;
             ;; exclude [CM]-[a-zA-Z] (except -[npNP] for navigations) keys
             (-mapcat
              (lambda (char)
                (list
                 (format "C-%s" char)
                 (format "C-%s" (upcase char))
                 (format "M-%s" char)
                 (format "M-%s" (upcase char))))
              (-difference (my-alphabet) '("n" "p"))))))
        (append l (mapcar #'kbd l))))
    (defvar my-modified-generals-bindings nil)
    ;; Protect "C-u" and many others in mini buffers from (evil +everywhere)
    (setq
     my-dummy-map (make-sparse-keymap)
     +default-minibuffer-maps '(my-dummy-map))
    (advice-add
     #'general-define-key
     :around #'my-general-define-key-filter-bindings-a)
    (advice-add
     #'general-define-key
     :around #'my-general-define-key-filter-keymaps-a)

    ;;
    )

  (defun my-doom-emacs-config ()

    ;; (advice-add #'doom-load :around #'my-doom-load-exclude-a)

    ;; don't kill REPLs when killing their windows
    (setq-default +eval-repl-plist '(:persist t))
    ;; Inherits the last visited buffer's `major-mode'
    (setq doom-scratch-initial-major-mode t)
    (setq
     confirm-kill-emacs nil
     winner-dont-bind-my-keys nil)
    ;; Turn off debug-on-quit
    (add-hook
     'doom-debug-mode-hook
     #'my-disable-debug-on-quit-unless-doom-debug-mode-h)
    (advice-add
     #'doom-fallback-buffer
     :after #'my-doom-dashboard-cd-home)
    ;; Stop those spammers from hiding `eval-expression''s results
    (my-add-advice-once
      '+doom-dashboard-init-h
      :after
      (lambda (&rest _)
        (remove-hook
         'doom-switch-buffer-hook #'+doom-dashboard-reload-maybe-h)))
    (add-hook 'doom-switch-buffer-hook #'my-doom-dashboard-cd-home)
    ;; Speedup a bit
    (remove-hook 'doom-first-file-hook 'global-git-commit-mode)
    (remove-hook 'doom-first-input-hook 'marginalia-mode)
    ;; Sometimes too early
    (advice-add #'lsp! :override #'ignore)
    (my-add-advice/s
      #'doom/open-scratch-buffer
      :around #'my-doom/open-scratch-buffer--elisp-a)
    (my-inplace-delete!
     doom-debug-variables 'garbage-collection-messages 'gcmh-verbose)

    ;; (my-bind [remap doom/reload] #'my-execute-doom-cli-and-reload-autoloads)
    (with-eval-after-load 'org
      ;; reset
      (plist-put org-format-latex-options :scale 1.0))
    (setq +default-want-RET-continue-comments nil)
    (setq +evil-want-o/O-to-continue-comments nil)

    ;; make `flycheck-mode' lazier
    (remove-hook 'doom-first-buffer-hook 'global-flycheck-mode)
    (defvar flycheck-disabled-checkers nil)

    ;; don't highlight exotic tabs, especially when we aren't supposed to edit
    ;; to fix them (simply removing hook isn't effective)
    (advice-add
     #'doom-highlight-non-default-indentation-h
     :override #'ignore)

    ;; electric-pair + puni
    (remove-hook 'doom-first-buffer-hook #'smartparens-global-mode)

    ;; Keep "`" in Org
    (remove-hook 'org-mode-hook 'cdlatex-mode)

    (advice-add #'+emacs-lisp-extend-imenu-h :override #'ignore)

    (set-file-template!
     #'emacs-lisp-mode
     :trigger #'my-elisp-insert-initial-template)

    ;; always lsp
    (advice-add
     #'+javascript-init-lsp-or-tide-maybe-h
     :override #'ignore)
    (advice-add
     #'+python-init-anaconda-mode-maybe-h
     :override #'ignore)

    ;; trying built-in
    (advice-add #'tree-sitter! :override #'ignore)

    ;; `+popup/other'?
    (my-bind "C-x p" (bound-and-true-p project-prefix-map))

    ;; don't spam and override any messages in the echo area
    (unless noninteractive
      (my-delete-alist! 'doom-debug-variables 'doom-inhibit-log))
    ;; reduce spams in the message buffer, TODO: issue feature request
    (advice-add
     #'doom--timestamped-message-a
     :around #'my-doom--timestamped-message-a--omit-seconds-a)

    ;; ;; `doom/restart-and-restore' doesn't restore opened files?
    ;; (unless (daemonp)
    ;;   (my-bind [remap doom/restart-and-restore] #'my-built-in-restart-emacs))

    ;; is automatically setup anyway, also avoid running for every
    ;; `revert-buffer' -> `lsp-disconnect' -> `lsp-managed-mode' ->
    ;; `lsp-unconfig-buffer' -> `dap-auto-configure-mode' -> `dap-mode' ->
    ;; `dap-mode-hook'
    (remove-hook 'dap-mode-hook 'dap-tooltip-mode)

    ;; ;; avoid hooks
    ;; (advice-add #'projectile-mode :override #'ignore)
    ;; (with-eval-after-load 'project
    ;;   (with-eval-after-load 'projectile
    ;;     (advice-remove #'projectile-mode #'ignore)))

    ;; From Doom's `use-package!' projectile config
    (dolist (projectile-func
             '(projectile-project-root
               projectile-project-name
               projectile-project-p
               projectile-locate-dominating-file
               projectile-relevant-known-projects))
      (autoload projectile-func "projectile"))
    ;; caching needs `projectile-mode'
    (my-after-each
      '(emacs projectile) (setq projectile-enable-caching nil))

    ;; Use evil bindings only
    (global-set-key [remap xref-find-definitions] nil)
    (global-set-key [remap xref-find-references] nil)

    ;; don't pulse after jumping from code greper, as it momentarily hides
    ;; syntax colors
    (with-eval-after-load 'nav-flash
      (my-remove-hook/s
        '(counsel-grep-post-action-hook
          consult-after-jump-hook)
        '(+nav-flash-blink-cursor-maybe-h)))

    ;; `consult-man' is too inaccurate
    (global-set-key [remap man] nil)

    ;; save a bit of processing for performance, also why obsfucate?
    (advice-add #'+emacs-lisp-truncate-pin :override #'ignore)

    (advice-add
     #'doom-compile-functions
     :around #'my-doom-compile-functions-a)

    ;; handled by `my-find-file-not-found-fn'
    (remove-hook
     'find-file-not-found-functions
     #'doom-create-missing-directories-h)

    ;; `my-set-auto-mode-from-fundamental-maybe-h' instead
    (remove-hook 'after-save-hook #'doom-guess-mode-h)

    ;; ;; activates too many places where it shouldn't, and prevents editing
    ;; (advice-add #'doom-docs-org-mode :override #'org-mode)
    (add-hook 'ignored-local-variable-values '(mode . doom-docs-org))

    ;; let `my-override-keymap-alist' control this
    (my-bind :vi '(insert) [tab] nil)

    ;; immediate force loading of large packages in a daemon will slow down
    ;; daemon startup needlessly
    (setq doom-incremental-first-idle-timer 60.0)
    ;; once loading triggers, the default idle timer is too small to press any
    ;; key to get out of the lag
    (setq doom-incremental-idle-timer 5.0)

    (when (daemonp)
      (doom-load-packages-incrementally
       `( ;

         ;; `lsp-mode' takes a noticeable time to load
         ,@(and (fboundp #'lsp-mode) '(lsp))

         ;; packages that need to be loaded unconditionally in an interactive
         ;; session

         recentf

         ,@(and (fboundp #'doom-modeline-mode) '(doom-modeline))

         ,@(and (fboundp #'corfu-mode) '(corfu))

         ;; really expensive to load
         ,@(and (fboundp #'yas-minor-mode) '(yasnippet))

         ,@(and (fboundp #'solaire-mode) '(solaire-mode))

         ;; nav-flash

         org-macs org-compat org

         ,@(and (fboundp #'org-roam-capture) '(org-roam))

         ;;
         )))

    (when (member '+word-wrap-mode text-mode-hook)
      (remove-hook 'text-mode-hook '+word-wrap-mode)
      (add-hook 'text-mode-hook 'my-doom-+word-wrap-mode-maybe-h))

    ;; TODO: feature request that allow installations but deny Doom's config

    ;; some packages's configs are too much, `package!''s `:disable' also
    ;; disable installations, so add to `doom-disabled-packages' late to disable
    ;; corresponding `use-package!' blocks (not work?)

    ;; (defvar my-doom-exclude-package-config-list
    ;;   '(rjsx-mode emmet-mode projectile))

    ;; (setq doom-disabled-packages
    ;;       (-union doom-disabled-packages my-doom-exclude-package-config-list))

    ;; doom calls this in many places
    (when (not (fboundp #'better-jumper-set-jump))
      (defalias #'better-jumper-set-jump #'ignore))

    ;; ;; (mapc #'require (cdr doom-incremental-packages))
    ;; (advice-add #'doom/reload :around #'my-ignore-file-missing-error-a)

    ;; Don't hide important minor modes
    (my-after-each
      '(emacs eshell vterm man)
      (my-remove-hook/s
        '(eshell-mode-hook
          vterm-mode-hook Man-mode-hook)
        '(hide-mode-line-mode)))

    (when (boundp '+popup-defaults)
      (cl-loop
       for
       (prop val)
       on
       `(:ttl nil :height 0.25 :modeline t)
       by
       #'cddr
       do
       (plist-put +popup-defaults prop val)))
    (my-setup-popup-rules)

    (my-bind
      :map '+popup-buffer-mode-map [remap make-frame]
      (cmds!
       buffer-file-name (find-file-other-frame buffer-file-name)))

    (add-hook
     '+doom-dashboard-functions #'my-doom-edit-dashboard-widget-banner
     96)

    (add-hook
     '+doom-dashboard-menu-sections
     `("Edit clipboard."
       :action my-edit-clipboard
       :icon
       (nerd-icons-faicon
        "nf-fa-clipboard"
        :face 'doom-dashboard-menu-title)))
    (add-hook
     '+doom-dashboard-menu-sections
     `("New buffer"
       :action my-new-buffer-maybe
       :icon
       (nerd-icons-codicon
        "nf-cod-new_file"
        :face 'doom-dashboard-menu-title)))

    ;; On a Wayland compositor, some applications can't be launched when
    ;; XDG_SESSION_TYPE isn't set (like Emacs daemon started by Systemd
    ;; after logging in); `browse-url-can-use-xdg-open' requires that
    ;; those are set: $DISPLAY, $WAYLAND_DISPLAY. "Lacking" (guessed
    ;; PATH): apply now, else do async.

    ;;
    ))

(provide 'my-functions-doom)

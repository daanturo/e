;; -*- lexical-binding: t; -*-

;; (setq eaf-build-dir
;;       (file-name-parent-directory
;;        (file-truename
;;         (locate-library "eaf"))))

;; (setq eaf-apps-to-install nil)

(my-autoload-commands 'eaf-airshare '(eaf-open-airshare))
(my-autoload-commands 'eaf-browser '(eaf-open-browser eaf-open-browser-with-history))
(my-autoload-commands 'eaf-camera '(eaf-open-camera))
(my-autoload-commands 'eaf-file-manager '(eaf-open-in-file-manager))
(my-autoload-commands 'eaf-file-sender '(eaf-file-sender-qrcode eaf-file-sender-qrcode-in-dired))
(my-autoload-commands 'eaf-jupyter '(eaf-open-jupyter))
(my-autoload-commands 'eaf-mindmap '(eaf-create-mindmap eaf-open-mindmap))
(my-autoload-commands 'eaf-terminal '(eaf-open-terminal))

(provide 'my-after-eaf)

;; Local Variables:
;; no-byte-compile: t
;; End:

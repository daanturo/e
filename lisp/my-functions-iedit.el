;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'iedit)

;;;###autoload
(defun my-iedit-make-occurrence-maybe (beg end)
  (unless (or (iedit-find-overlay-at-point beg 'iedit-occurrence-overlay-name)
              (iedit-find-overlay-at-point end 'iedit-occurrence-overlay-name))
    (push (iedit-make-occurrence-overlay beg end)
          iedit-occurrences-overlays)))

;;;###autoload
(defun my-iedit-make-occurences-at-points (bwd fwd points)
  "For each of POINTS, make an occurrence of `iedit'.
Each occurrence at P is created from (+ BWD P) to (+ FWD P)."
  (let* ((initial-point (seq-first points))
         (occurrence-str
          (buffer-substring-no-properties
           (+ bwd initial-point) (+ fwd initial-point))))
    (if iedit-mode
        (my-iedit-make-occurrence-maybe
         (+ bwd initial-point) (+ fwd initial-point))
      (iedit-start
       (iedit-regexp-quote occurrence-str)
       (+ bwd initial-point)
       (+ fwd initial-point)))
    ;; produce weird orders
    ;; (iedit-make-markers-overlays
    ;;  (seq-map (lambda (p) (cons (+ bwd p) (+ fwd p))) (seq-rest points)))
    (dolist (p (seq-rest points))
      (my-iedit-make-occurrence-maybe (+ bwd p) (+ fwd p)))))

;;;###autoload
(defun my-iedit-mode-toggle-on-current-treesit-function ()
  "Like `iedit-mode-toggle-on-function' but on current defun at point."
  (interactive)
  (my-with-advice
    #'mark-defun
    :override #'my-treesit-mark-current-defun (iedit-mode-toggle-on-function)))


(provide 'my-functions-iedit)

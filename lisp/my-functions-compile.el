;; -*- lexical-binding: t; -*-

(require 'dash)

(require 'compile)

;;;###autoload
(defun my-compile-buffer-list (&optional running)
  (cond
   (running
    (-map #'process-buffer compilation-in-progress))
   (t
    (-filter
     (lambda (buf)
       (with-current-buffer buf
         ;; `grep-mode' also derives from `compilation-mode'
         (member major-mode '(compilation-mode))))
     (-map #'buffer-name (buffer-list))))))

;;;###autoload
(defun my-project-compile-buffer-list (&optional running)
  (-let* ((prj (or (project-current) default-directory)))
    (-filter
     (lambda (buf)
       (with-current-buffer buf
         (equal prj (or (project-current) default-directory))))
     (my-compile-buffer-list running))))

;;;###autoload
(defun my-project-show-compilation-buffer (&optional new-frame)
  "Pop to current Compilation buffer in other window.
If there are no candidates, call `compile' instead.
With non-nil NEW-FRAME, open in a new frame."
  (interactive "P")
  (let* ((compilation-buffers (my-project-compile-buffer-list))
         (buffer-to-show
          (cond
           ((= 1 (length compilation-buffers))
            (car compilation-buffers))
           ((< 1 (length compilation-buffers))
            (completing-read "Compilation buffer: " compilation-buffers)))))
    (if (and buffer-to-show
             ;; (get-buffer-process buffer-to-show)
             )
        (if new-frame
            (switch-to-buffer-other-frame buffer-to-show)
          (save-selected-window
            (pop-to-buffer buffer-to-show)))
      (call-interactively #'project-compile))))

;;;###autoload
(defun my-project-show-all-running-compilation-buffers ()
  (interactive)
  (-let* ((bufs (my-project-compile-buffer-list t)))
    (dolist (buf bufs)
      (save-selected-window
        (pop-to-buffer buf)))
    bufs))

;;;###autoload
(defun my-compilation-comint-mode (&rest _)
  "Enable input from compilation buffer until it finishes when `major-mode' is `compilation-mode'."
  (interactive)
  (when (equal major-mode 'compilation-mode)
    (comint-mode)
    (compilation-shell-minor-mode t)
    (read-only-mode 0)
    (add-hook
     'compilation-finish-functions (lambda (&rest _) (compilation-mode))
     nil 'local)))

(provide 'my-functions-compile)

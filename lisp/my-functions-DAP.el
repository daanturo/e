;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(defvar my-dap-client-package nil)

(defvar my-dap-client-package--table
  `(("dap-mode" .
     ,(lambda (&optional disable)
        (interactive "P")
        (cond
         ((not disable)
          (dap-mode)
          (dap-ui-mode))
         (disable))))
    ("dape" .
     ,(lambda (&optional disable)
        (interactive "P")
        (cond
         ((not disable))
         (disable))))))

;;;###autoload
(defun my-dap-switch-client-package (&optional add-dir-local-var)
  (interactive "P")
  (-let* ((to-client
           (completing-read
            "Emacs DAP client: "
            (lambda (string pred action)
              (cond
               ((eq action 'metadata)
                `(metadata
                  (display-sort-function .
                                         ,(lambda (cands)
                                            (remove
                                             nil
                                             `(,@(remove my-dap-client-package cands)
                                               ,my-dap-client-package))))))
               (t
                (complete-with-action
                 action
                 (-map #'car my-dap-client-package--table)
                 string
                 pred)))))))
    (setq my-dap-client-package to-client)
    (mapc
     (-lambda ((client . func))
       (cond
        ((equal client to-client)
         (funcall func))
        (:else
         (funcall func 'disable))))
     my-dap-client-package--table)
    (when _add-dir-local-var)
    to-client))


;;; my-functions-DAP.el ends here

(provide 'my-functions-DAP)

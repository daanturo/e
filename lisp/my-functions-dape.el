;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'transient)

(require 'dape)

;; https://github.com/svaante/dape/issues/6#issuecomment-1897088967
;; TODO: remove when official support

;;;###autoload
(autoload #'my-dape-transient "my-functions-dape" nil t)

(transient-define-prefix
  my-dape-transient () "Transient for dape."
  [["Stepping"
    ("n" "Next" dape-next :transient t)
    ("i" "Step in" dape-step-in :transient t)
    ("o" "Step out" dape-step-out :transient t)
    ("c" "Continue" dape-continue :transient t)
    ("d" "Init" dape)
    ("r" "Restart" dape-restart :transient t)]
   ["Breakpoints"
    ("bb" "Toggle" dape-breakpoint-toggle :transient t)
    ("be" "Add expression breakpoint" dape-breakpoint-expression)
    ("bd" "Delete" dape-breakpoint-remove-at-point :transient t)
    ("bD" "Delete all" dape-breakpoint-remove-all :transient t)
    ("bl" "Log" dape-breakpoint-log :transient t)]
   ["Info"
    ("si" "Info" dape-info :transient t)
    ("sm" "Memory" dape-read-memory :transient t)
    ("ss" "Select Stack" dape-select-stack :transient t)
    ("R" "Repl" dape-repl :transient t)]
   ["Quit"
    ("<escape>" "" transient-quit-one)
    ("qq" "Quit" dape-quit :transient nil)
    ("Q" "Kill" dape-kill :transient nil)]]
  (interactive) (transient-setup 'my-dape-transient)
  ;; (dape-breakpoint-load)
  ;;
  )

(transient-append-suffix
  'my-dape-transient '(-1 -1)
  `["Other commands" ;
    (,my-execu-confirm-key "my-execu-dwim" my-execu-dwim :transient nil)
    ;; ("RET" "`dape'" dape :transient nil)
    ])

;;;###autoload
(defun my-dape-breakpoint-toggle-and-save ()
  (interactive)
  (cond
   ;; act like no breakpoints yet when not loaded
   ((not (featurep 'dape))
    (require 'dape)
    (dape-breakpoint-load)
    (dap-breakpoint-add))
   (:else
    (dape-breakpoint-toggle)))
  (dape-breakpoint-save))

;;;###autoload
(defun my-dape-breakpoint-save--kill-open-buffer-a
    (func &optional file &rest args)
  (-let* ((file (or file dape-default-breakpoints-file))
          (file-true (file-truename file))
          (kill-fn
           (lambda (buf-to-kill &rest msg-args)
             (dlet ((inhibit-message t))
               (apply #'message msg-args))
             (my-kill-buffer-no-ask buf-to-kill)))
          (brutal-kill-fn
           (lambda ()
             (-let* ((count 0))
               ;; WTH it keeps coming back??
               (while-let ((_ (< count 3))
                           (buf
                            (or (get-file-buffer file)
                                (get-file-buffer file-true))))
                 (cl-incf count)
                 (funcall
                  kill-fn
                  buf
                  "`my-dape-breakpoint-save--kill-open-buffer-a': kill!"))))))
    (funcall brutal-kill-fn)
    (-let* ((retval
             ;; can't find out the cause yet
             (my-with-advice
               #'ask-user-about-supersession-threat
               :override #'ignore (apply func file args))))
      (funcall brutal-kill-fn)
      retval)))

;;;###autoload
(defun my-dape-breakpoint-load-non-sync-h ()
  "`dape''s overlays maybe hidden by others, try to run later."
  (run-with-idle-timer 0.5 nil #'dape-breakpoint-load))

;;;###autoload
(defun my-dape-breakpoint-save-default (&rest _)
  (interactive)
  (dape-breakpoint-save))

;;; my-functions-dape.el ends here

(provide 'my-functions-dape)

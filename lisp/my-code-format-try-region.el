;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-format-dedented-region-keep-indent (beg end)
  "Format code from BEG to END.
Idea:
- Dedent the region by the minimimum indentation
- Format the region
- Add back the indentation."
  (interactive "r")
  (let* ((code-to-format (buffer-substring-no-properties beg end)))
    (let* ((min-indent (my-find-minimum-indentation-of-string code-to-format))
           (pre-fn (lambda ()
                     (my-decrease-indentation (point-min) (point-max) min-indent)))
           (post-fn (lambda ()
                      (my-increase-indentation (point-min) (point-max) min-indent))))
      (my-code-format-try-region beg end pre-fn post-fn))))

;;;###autoload
(defun my-code-format-try-region (beg end &optional pre-fn post-fn)
  "Format code from BEG to END.
PRE-FN is called in the temporary buffer before formatting.
POST-FN is called in the temporary buffer after formatting (before inserting)."
  (interactive "r")
  (my-save-line-and-column
    (let* ((mode major-mode)
           (buf0 (current-buffer))
           (code-to-format (buffer-substring-no-properties beg end))
           ;; Some formatters strip initial and trailing blank lines, we need to
           ;; keep them for consistency
           (initial-new-lines (car (s-match "\\`\n*" code-to-format)))
           (trailing-new-lines (car (s-match "\n*\\'" code-to-format)))
           (temp-buf (generate-new-buffer
                      (format "*%s %s*" #'my-code-format-try-region (buffer-name))))
           ;; get the formatter first, to reduce the overhead of calling the
           ;; major mode when possible
           (formatters (my-apheleia-get-formatters-safe)))
      (with-current-buffer temp-buf
        (insert code-to-format)
        ;; `apheleia-format-buffer' is asynchronous
        (let* ((call-back (lambda ()
                            (when post-fn (funcall post-fn))
                            (let* ((formatted (-->
                                               (buffer-substring-no-properties
                                                (point-min) (point-max))
                                               (string-trim it "\n" "\n")
                                               (concat initial-new-lines
                                                       it
                                                       trailing-new-lines))))
                              (with-current-buffer buf0
                                (replace-region-contents
                                 beg end
                                 (lambda () formatted))))
                            (unless debug-on-error
                              (kill-buffer temp-buf)))))
          (when pre-fn (funcall pre-fn))
          (unless formatters (funcall mode))
          (setq-local apheleia-formatter formatters)
          (my-code-format-buffer call-back))))))

;;;###autoload
(defun my-code-format-try-region-in-cloned-buffer ()
  (interactive)
  (let ((reg-beg (if (use-region-p) (region-beginning) nil))
        (reg-end (if (use-region-p) (region-end) nil)))
    (my-copy|clone-buffer)
    (my-code-format-buffer)
    (when (and reg-beg reg-end)
      (set-mark reg-end)
      (goto-char reg-beg))
    (my-recenter-region-in-window)))


;;; my-code-format-try-region.el ends here

(provide 'my-code-format-try-region)

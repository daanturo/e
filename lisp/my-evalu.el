;; -*- lexical-binding: t; -*-

(require 'dash)
(require 's)
(require '00-my-core-macros)

(require 'cl-macs)

(require 'comint)

;;;###autoload
(progn

  (defvar my-evalu-alist nil
    "Alist of mode and `my-evalu-region' handlers.
Each element is property list:
- repl handler: (() => <repl buffer>), or a CMD string to run a REPL
- eval region handler: (<beg> <end>) => <any>
- load file handler: (<file-name>) => <any>
- eval string handler: (<string>) => <any>
.")

  (cl-defun my-evalu-set-handler (mode
                                  repl-handler
                                  &rest
                                  plist
                                  &key
                                  region
                                  file
                                  string
                                  dependencies
                                  predicate
                                  import)
    "Add to `my-evalu-alist'.
MODE, REPL-HANDLER, EVAL-REGION-HANDLER, LOAD-FILE-HANDLER.  Non-nil
PREDICATE (function that takes 0 arguments) means also must satisfy
this.  IMPORT: regexp that specifies string(s) for
`my-evalu-import-statements'."
    ;; (set-repl-handler! mode repl-handler)
    (push `(,mode :repl ,repl-handler ,@plist) my-evalu-alist)))

(defvar-local my-evalu--handler-plist nil)

;;;###autoload
(defun my-evalu-get-handler (type)
  (unless my-evalu--handler-plist
    (setq my-evalu--handler-plist
          ;; (my-get-alist-major-mode my-evalu-alist)
          (-some
           (-lambda ((k . plist))
             (-let* ((pred (plist-get plist :predicate)))
               (and (my-mode-derived-p major-mode (my-ensure-list k))
                    (if pred
                        (funcall pred)
                      t)
                    plist)))
           my-evalu-alist)))
  (plist-get my-evalu--handler-plist type))

;;;###autoload
(defun my-evalu-repl-default-name (program &optional _switches)
  (-let* ((dedicated
           (or (-some--> (project-current) (project-name it)) (buffer-name))))
    (format "*%s %s*" program dedicated)))

(defvar-local my-repl-program nil)

(defvar my-evalu-repl-hook '())

;;;###autoload
(defun my-evalu-make-repl-and-run
    (program-args &optional name startfile setup-fn)
  (interactive (list
                (-->
                 (read-shell-command "REPL command: "
                                     (-let* ((cmdargs
                                              (my-evalu-get-handler :repl)))
                                       (and (listp cmdargs)
                                            (-every #'stringp cmdargs)
                                            (combine-and-quote-strings
                                             cmdargs))))
                 (split-string-shell-command it) (delete "" it))))
  (-let* (((program . switches) program-args)
          (name (or name (my-evalu-repl-default-name program)))
          (buf
           (apply #'make-comint-in-buffer
                  name
                  name
                  program
                  startfile
                  switches)))
    (pop-to-buffer buf)
    (setq-local my-repl-program program)
    (when setup-fn
      (funcall setup-fn))
    (run-hooks 'my-evalu-repl-hook)
    buf))

;;;###autoload
(defun my-evalu-ensure-repl ()
  (-let* ((repl-handler (my-evalu-get-handler :repl)))
    (when repl-handler
      (-let* ((buf
               (save-window-excursion
                 (cond
                  ((functionp repl-handler)
                   (-->
                    (funcall repl-handler)
                    (cond
                     ((or (stringp it) (bufferp it))
                      it)
                     ((processp it)
                      (process-buffer it))
                     ((windowp it)
                      (window-buffer it)))))
                  ((listp repl-handler)
                   (my-evalu-make-repl-and-run repl-handler))))))
        buf))))

(defvar-local my-evalu--repl-buffer-flag nil)

;;;###autoload
(defun my-evalu-switch-to-repl (&optional arg toggle)
  "Switch to appropriate REPL.
Numeric ARG: display but don't select it. Universal prefix ARG:
prefer displayiny on the right. TOGGLE: close the REPL's window
when at it."
  (interactive (list current-prefix-arg t))
  (-let*
      ((num-arg (prefix-numeric-value arg))
       (fn
        (lambda ()
          (-let* ((buf (my-evalu-ensure-repl)))
            (cond
             ((and toggle my-evalu--repl-buffer-flag)
              (delete-window))
             ((null buf)
              (user-error
               "`my-evalu': cannot find a REPL function for `%s', set with `my-evalu-set-handler'."
               major-mode))
             ((numberp arg)
              (save-selected-window
                (pop-to-buffer buf)
                (setq my-evalu--repl-buffer-flag t)))
             (t
              (pop-to-buffer buf)
              (setq my-evalu--repl-buffer-flag t)))))))
    (cond
     ((and arg (proper-list-p arg))
      (my-with-mode/s '(+popup-mode) nil (funcall fn)))
     (t
      (funcall fn)))))

(defun my-evalu-region--comint-default (beg end repl-buffer)
  "Fallback evaluator that sends region BEG..END to REPL-BUFFER."
  (-let* ((proc (get-buffer-process repl-buffer)))
    (comint-send-region proc beg end)
    ;; Enter!
    (comint-send-string proc "\n")))

(defvar-local my-evalu-default-send-string-enter-command nil)
(defvar my-evalu-default-send-string-delete-sent t)

(defun my-evalu-default-send-string (str &optional delete-sent-text)
  (interactive (list
                (and (use-region-p)
                     (buffer-substring-no-properties
                      (region-beginning) (region-end)))
                nil))
  (cl-assert (stringp str))
  (-let* ((str (string-trim str)))
    (with-current-buffer (my-evalu-ensure-repl)
      (goto-char (point-max))
      (ignore-error (text-read-only)
        (delete-horizontal-space))
      (goto-char (point-max)) ; some invisible read-only elements
      (-let* ((pt0 (point)))
        (insert str)
        (-let* ((pt1 (point)))
          (prog1 (call-interactively
                  (or my-evalu-default-send-string-enter-command
                      (key-binding (kbd "RET"))))
            ;; cleanup all but the first line
            (when (and (or delete-sent-text
                           my-evalu-default-send-string-delete-sent)
                       (string-search "\n" str))
              (dlet ((inhibit-read-only t))
                (save-excursion
                  (goto-char pt0)
                  (when (search-forward "\n" (+ (eol) 1) t)
                    (backward-char)
                    (delete-region (point) pt1)
                    (insert " " comment-start "…")))))))))))

;;;###autoload
(defun my-evalu-string (str &optional quiet)
  "Fallback evaluator that evaluates STR.
With non-nil QUIET, try to be that. Note that we need a trailing
\n to finish the input."
  (interactive (list
                (--> (read-string "Evaluate: ") (concat it "\n"))))
  (dlet ((inhibit-message (or inhibit-message quiet)))
    (-let* ((str* (my-ensure-string-suffix "\n" str))
            (string-handler (my-evalu-get-handler :string)))
      (cond
       (string-handler
        (funcall string-handler str*))
       (t
        (-let* ((repl-buf (my-evalu-ensure-repl))
                (proc (get-buffer-process repl-buf)))
          (if repl-buf
              (progn
                (message "`my-evalu-string':\n%s" str*)
                (comint-send-string proc str*))
            (user-error "`%s': Can't dispatch an evaluation function"
                        #'my-evalu-string))))))))

;;;###autoload
(defun my-evalu-region (beg-or-bounds &optional end quiet)
  "Evaluate region between BEG-OR-BOUNDS and END.
QUIET: `inhibit-message'. BEG-OR-BOUNDS can be cons cell that includes both beg and end."
  (interactive (and (use-region-p) (list (region-beginning) (region-end))))
  (require 'hi-lock)
  (dlet ((inhibit-message (or inhibit-message quiet)))
    (-let* ((region-handler (my-evalu-get-handler :region))
            (string-handler (my-evalu-get-handler :string))
            (beg
             (if (consp beg-or-bounds)
                 (car beg-or-bounds)
               beg-or-bounds))
            (end (or end (cdr beg-or-bounds)))
            ;; skip trailing white spaces, to prevent inserting to the repl
            (end1 (my-skip-chars-position nil end nil 'backward))
            (hl-fn
             (lambda () (pulse-momentary-highlight-region beg end 'hi-yellow))))
      (cond
       (region-handler
        (funcall hl-fn)
        (funcall region-handler beg end1))
       ;; fallback
       (string-handler
        (funcall hl-fn)
        (funcall string-handler (buffer-substring-no-properties beg end)))
       (t
        (-let* ((repl-buf (my-evalu-ensure-repl)))
          (if repl-buf
              (progn
                (funcall hl-fn)
                (message "`my-evalu-region':\n%s" (buffer-substring beg end1))
                (my-evalu-region--comint-default beg end1 repl-buf))
            (user-error "`%s': Can't dispatch an evaluation function"
                        #'my-evalu-region)))))))
  (setq deactivate-mark nil))

;;;###autoload
(defun my-evalu-whole-buffer ()
  (interactive)
  (my-evalu-region (point-min) (point-max)))

;;;###autoload
(defun my-evalu-before ()
  (interactive)
  (my-evalu-region (point-min) (point)))

;;;###autoload
(defun my-evalu-after ()
  (interactive)
  (my-evalu-region (point) (point-max)))

;;;###autoload
(defun my-evalu-load-file (file-name)
  (interactive (list (read-file-name "my-evalu-load-file : "
                                     nil (file-name-nondirectory buffer-file-name) nil)))
  (my-evalu-ensure-repl)
  (-if-let* ((load-file-handler (my-evalu-get-handler :file)))
      (funcall load-file-handler file-name)
    (with-current-buffer (find-file-noselect file-name)
      (save-restriction
        ;; ensure whole file
        (widen)
        (my-evalu-whole-buffer)))))

;;;###autoload
(defun my-evalu-file-or-buffer (&optional no-confirm)
  (interactive "P")
  (when (or no-confirm
            (y-or-n-p
             (format "Evalute whole buffer %s?"
                     (if buffer-file-name
                         (file-name-nondirectory buffer-file-name)
                       (buffer-name)))))
    (if buffer-file-name
        (my-evalu-load-file buffer-file-name)
      (my-evalu-whole-buffer))))

;;;###autoload
(defun my-evalu-function-definition ()
  (interactive)
  (-let* ((bod (bounds-of-thing-at-point 'defun))
          ((beg . end) bod))
    (my-evalu-region beg end)))

(defun my-evalu-get-treesit-node-on (&optional beg end)
  (and (treesit-parser-list)
       (-let* ((beg (or beg (my-line-non-whitespace-beg-position)))
               (end (or end (line-end-position))))
         (and (not (s-blank-str? (buffer-substring beg end)))
              (-let* ((node (treesit-node-on beg end)))
                (and (not (equal node (treesit-buffer-root-node))) node))))))

(defvar my-evalu-get-import-list-functions '())

(defvar-local my-evalu-top-level-node--import-evaluated nil)

;;;###autoload
(defun my-evalu-top-level-node ()
  (interactive)
  (when (not my-evalu-top-level-node--import-evaluated)
    (dolist (func my-evalu-get-import-list-functions)
      (-when-let* ((str (funcall func)))
        (my-evalu-string (concat "\n" str "\n") 'quiet)))
    (setq-local my-evalu-top-level-node--import-evaluated t))
  (my-evalu-region (my-treesit-node-bounds (my-treesit-top-level-node) 'cons)))

;;;###autoload
(defun my-evalu-import-statements ()
  (interactive)
  (-let* ((regexp (my-evalu-get-handler :import))
          (str
           (-->
            (s-match-strings-all
             regexp (my-buffer-string-no-properties))
            (-map #'car it) (string-join it "\n"))))
    (my-evalu-string str)))


;;;###autoload
(defun my-evalu-dwim (&optional arg)
  "Evalution based on ARG and region.
Use `my-evalu-set-handler' to configure.
- ARG = nil: active region or current trimmed line.
- ARG = 1: whole buffer.
- ARG = 2: top-level parser's node.
- ARG = 3: nested defun.
- ARG = 4: paragraph.
- ARG = 5: parser's node that covers the active region or the line.
- Other non-nil ARG: current trimmed line.
"
  (interactive "P")
  (-let* ((line-beg (my-line-non-whitespace-beg-position))
          (line-end (line-end-position))
          (r|l-beg
           (if (use-region-p)
               (region-beginning)
             line-beg))
          (r|l-end
           (if (use-region-p)
               (region-end)
             line-end)))
    (prog1 (cond
            ((and (equal nil arg) (use-region-p))
             (my-evalu-region (region-beginning) (region-end)))
            ((equal 1 arg)
             (my-evalu-file-or-buffer 'yes))
            ((equal 2 arg)
             (my-evalu-top-level-node))
            ((equal 3 arg)
             (my-evalu-region (my-bounds-of-defun 'nested)))
            ((equal 4 arg)
             (my-evalu-region (bounds-of-thing-at-point 'paragraph)))
            ((equal 5 arg)
             (-let* ((node (my-evalu-get-treesit-node-on r|l-beg r|l-end)))
               (my-evalu-region (treesit-node-start node)
                                (treesit-node-end node))))
            (:else
             (my-evalu-region line-beg line-end)))
      (setq deactivate-mark nil))))


(setq-default my-evalu-alist (delete-dups my-evalu-alist))

;;; my-evalu.el ends here

(provide 'my-evalu)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defun my-org--screen-pixel-width ()
  (--> (-map (lambda (alist)
               (-let* ((width (nth 2 (alist-get 'geometry alist)))
                       (scale-factor (or (alist-get 'scale-factor alist)
                                         1)))
                 (/ width scale-factor)))
             (display-monitor-attributes-list))
       (-min it)
       (floor it)))

;;;###autoload
(defun my-set-org-image-actual-width (&rest _)
  (interactive)
  ;; This only adjust the displayed width, not the actual one
  (setq org-image-actual-width (/ (my-org--screen-pixel-width)
                                  2)))

;;;###autoload
(defun my-org-startup-with-inline-images-h ()
  (when (and org-startup-with-inline-images (null org-inline-image-overlays))
    (org-display-inline-images)))

;;; my-functions-org-hook.el ends here

(provide 'my-functions-org-hook)

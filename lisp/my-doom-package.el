;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'doom-packages)
(require 'straight)

(defvar my-doom-update-dry-run nil)

(defun my-doom-update--log (buf &rest insert-args)
  (-let* ((fn
           (lambda ()
             (goto-char (point-max))
             (unless (equal ?\n (char-before))
               (insert "\n"))
             (apply #'insert insert-args)))
          (wd (get-buffer-window buf t)))
    (if wd
        (with-selected-window wd
          (if (eobp)
              (funcall fn)
            (save-excursion (funcall fn))))
      (with-current-buffer buf
        (funcall fn)))))

(cl-defun my-doom-update--git-pull-here (buf callback &key cmd pinned-rev)
  (-let* ((dirty (my-vc-git-dirty-unstaged-or-uncommitted-p)))
    (my-doom-update--log buf
                         "\n" "In " (abbreviate-file-name default-directory)
                         (if dirty
                             " (dirty worktree) "
                           "")
                         " :\n")
    (dlet ((shell-command-dont-erase-buffer t))
      (-let* ((remote "origin")
              (branch (my-vc-git-get-current-branch))
              (cmd
               (or cmd
                   (format "set -x ; git pull %s %s %s %s"
                           remote branch
                           (if my-doom-update-dry-run
                               "--dry-run"
                             "")
                           (cond
                            ((and pinned-rev my-doom-update-dry-run)
                             (format "; echo git reset --hard %s" pinned-rev))
                            (pinned-rev
                             ;; (format "; git checkout %s" pinned-rev)
                             (format "; git reset --hard %s" pinned-rev))
                            (t
                             "")))))
              (wd (async-shell-command cmd buf))
              (proc (--> (window-buffer wd) (get-buffer-process it)))
              (sentn0 (process-sentinel proc)))
        (set-process-sentinel
         proc
         (lambda (&rest args1)
           (apply sentn0 args1)
           (funcall callback)))))))

(defvar my-doom-package--pinned-list nil)
;;;###autoload
(defun my-doom-package-pinned-list ()
  (unless my-doom-package--pinned-list
    (setq my-doom-package--pinned-list (doom-package-pinned-list)))
  my-doom-package--pinned-list)

(defvar my-doom-package--recipe-list nil)
;;;###autoload
(defun my-doom-package-recipe-list ()
  (unless my-doom-package--recipe-list
    (setq my-doom-package--recipe-list (doom-package-recipe-list)))
  my-doom-package--recipe-list)

;;;###autoload
(defun my-doom-package-directory->name (dir)
  (-->
   (file-name-nondirectory dir)
   (or
    ;; available metadata
    (-some
     (-lambda ((&plist :package pkg :local-repo local-repo))
       (and (string-equal-ignore-case local-repo it) pkg))
     (my-doom-package-recipe-list))
    ;; heuristics
    (replace-regexp-in-string
     "^\\(?:emacs-\\)?\\(.*?\\)\\(?:\\.el\\)?$" "\\1" it))))

;;;###autoload
(defun my-doom-package-name->directory (pkg &optional build-dir)
  (-let* ((pkg* (format "%s" pkg))
          (base-dir
           (if build-dir
               (straight--build-dir)
             (straight--repos-dir)))
          ;; (sub-dirs
          ;;  (-->
          ;;   (directory-files base-dir
          ;;                    nil
          ;;                    directory-files-no-dot-files-regexp)
          ;;   (-map #'directory-file-name it)))
          ;; (pkg-local-repo
          ;;  (-some
          ;;   (-lambda ((&plist :package package :local-repo local-repo))
          ;;     (and (string-equal-ignore-case pkg* package) local-repo))
          ;;   (my-doom-package-recipe-list)))
          )
    ;; (-->
    ;;  (-some
    ;;   (lambda (dir) (string-equal-ignore-case dir pkg-local-repo)) sub-dirs)
    ;;  (file-name-concat base-dir it))
    (file-name-concat base-dir (doom-package-recipe-repo (intern pkg*)))))

;;;###autoload
(defun my-doom-package-pinned-revision (package-or-dir)
  (my-doom-package-pinned-list)
  (-let* ((package-or-dir (directory-file-name package-or-dir))
          (pred (lambda (a0 a1) (and a0 a1 (string-equal-ignore-case a0 a1)))))
    (or (alist-get package-or-dir (my-doom-package-pinned-list) nil nil pred)
        (-->
         (my-doom-package-directory->name package-or-dir)
         (alist-get it (my-doom-package-pinned-list) nil nil pred))
        ;; sometimes a package name in pinned list includes the trailing ".el"
        ;; like its directory name, while the pinning form doesn't
        (-->
         (file-name-nondirectory package-or-dir)
         (alist-get it (my-doom-package-pinned-list) nil nil pred)))))

(defun my-doom-update--git-pull-packages (dir-lst buf final-callback)
  (if (< 0 (length dir-lst))
      (-let* ((callback
               (lambda ()
                 (my-doom-update--git-pull-packages
                  (cl-rest dir-lst) buf final-callback)))
              (dir (cl-first dir-lst)))
        (dlet ((default-directory dir))
          (-let* ((pinned-rev (my-doom-package-pinned-revision dir))
                  (current-rev
                   (cl-first
                    (process-lines-ignore-status "git" "rev-parse" "HEAD")))
                  (cmd
                   (cond
                    ((and pinned-rev (equal pinned-rev current-rev))
                     (my-doom-update--log buf
                                          "\n"
                                          "Skipping "
                                          dir
                                          " (pinned to "
                                          pinned-rev
                                          ")\n")
                     "")
                    (t
                     nil))))
            (my-doom-update--git-pull-here
             buf
             callback
             :cmd cmd
             :pinned-rev
             (and (not (equal pinned-rev current-rev)) pinned-rev)))))
    (funcall final-callback)))

;;;###autoload
(defun my-doom-package-git-directories ()
  (-->
   (straight--repos-dir)
   (directory-files it 'full directory-files-no-dot-files-regexp)
   (-filter #'file-directory-p it)
   (-filter (lambda (dir) (file-exists-p (file-name-concat dir ".git"))) it)
   (-map #'abbreviate-file-name it)))


;;;###autoload
(defun my-doom-update! (&optional display)
  (interactive (list t))
  (my-doom-run-before-upgrade-hook-a)
  (-let* ((buf (get-buffer-create (format "*%s*" 'my-doom-update!))))
    (dlet ((default-directory doom-emacs-dir))
      (when display
        (pop-to-buffer buf))
      (my-doom-update--git-pull-here
       buf
       (lambda ()
         (my-doom-update--git-pull-packages
          (my-doom-package-git-directories) buf
          (lambda ()
            (dlet ((shell-command-dont-erase-buffer t))
              (async-shell-command "doom sync" buf)))))))
    buf))

;;;###autoload
(defun my-doom-update-like-cli! (&optional no-backup)
  "An alternative to the official implementation.
NO-BACKUP: do not backup the Emacs directory before updating.

Use when it's broken in some way such as:
- Indefinite hang when updating a fork
Etc."
  (interactive "P")
  ;; backup first
  (-when-let* ((_ (not no-backup))
               (fts ".%FT%T%z")
               (doom-emacs-path
                (-->
                 doom-emacs-dir (file-truename it) (directory-file-name it)))
               (backup-path (concat doom-emacs-path (format-time-string fts)))
               (pattern
                (concat
                 doom-emacs-path
                 (replace-regexp-in-string "%[[:alpha:]]" "*" fts)))
               (old-backups (file-expand-wildcards pattern)))
    (when (my-non-empty-list? old-backups)
      (notifications-notify
       :body
       (format "Doom update: old backup(s) exist in %s"
               (file-name-parent-directory doom-emacs-path))))
    (my-copy-file-or-directory doom-emacs-path backup-path "--reflink=auto"))
  (switch-to-buffer (my-doom-update!))
  (delete-other-windows))

;;;###autoload
(defun my-doom-package-list-local-forking-repos ()
  (-let* ((repos-dir (my-doom-package-git-directories)))
    (-filter
     (lambda (dir)
       (dlet ((default-directory dir))
         (or (my-vc-git-dirty-unstaged-or-uncommitted-p)
             (not
              (equal
               (or (my-vc-git-get-default-branch t)
                   (my-vc-git-get-default-branch))
               (my-vc-git-get-current-branch))))))
     repos-dir)))

;;;###autoload
(defun my-doom-rollback-clean-pinned-packages ()
  (-let* ((dirs
           (-->
            (my-doom-package-git-directories)
            (-filter #'my-doom-package-pinned-revision it)
            (-difference it (my-doom-package-list-local-forking-repos))))
          (checkout-cmd
           (-->
            dirs
            (-keep
             (lambda (dir)
               (-let* ((current-revision
                        (dlet ((default-directory dir))
                          (my-process-sync-success-stdout
                           '("git" "rev-parse" "HEAD"))))
                       (pinned-revision (my-doom-package-pinned-revision dir)))
                 (and (not (equal current-revision pinned-revision))
                      (format "cd %s ; git checkout %s"
                              (my-quote|escape-spaces-in-filename|path-for-shell
                               dir)
                              pinned-revision))))
             it)
            (string-join it ";\n")))
          (cmd (concat checkout-cmd ";\n" "doom sync")))
    (if (string-empty-p checkout-cmd)
        (message "`my-doom-package-pinned-revision': no packages to rollback!")
      (async-shell-command cmd))))

;;; my-doom-package.el ends here

(provide 'my-doom-package)

;; `straight.el' isn't available at compile-time

;; Local Variables:
;; no-byte-compile: t
;; End:

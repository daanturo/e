;; -*- lexical-binding: t; -*-

(require 'dash)

(require 'dash-docs)

(my-autoload-functions 'language-id '(language-id-buffer))

(defvar my-docs-documentation-command-alist nil)

;; not supported OOTB by devdocs.io
(add-to-list 'my-docs-documentation-command-alist '(emacs-lisp-mode . describe-symbol))
(add-to-list 'my-docs-documentation-command-alist '(scheme-mode . geiser-doc-look-up-manual))

;;;###autoload
(defun my-docs-documentation-at-point (&optional dash)
  "Use DASH when prefix arg."
  (interactive "P")
  (-let* ((func (alist-get major-mode my-docs-documentation-command-alist)))
    (cond
     (dash (my-dash-docs-at-point))
     (func (call-interactively func))
     (t (call-interactively #'my-devdocs-io)))))

(defvar-local my-devdocs-io--last-typed-docset nil)

;;;###autoload
(defun my-devdocs-io (suggested-docset thing)
  (interactive (list
                (or my-devdocs-io--last-typed-docset (my-guess-prog-lang))
                (my-thing-at-point-dwim)))
  (-let* ((base "devdocs.io/#q=")
          (init-input (concat suggested-docset " " thing))
          (typed-input (read-string base init-input))
          ;; remember the last docset instead of only presenting the buffer's
          ;; prog language
          (typed-docset
           (--> (split-string typed-input "[ \t\n]" t) (cl-first it))))
    (cond
     ;; "empty" docstring
     ((string-prefix-p " " typed-input)
      (setq my-devdocs-io--last-typed-docset " "))
     ((not (equal typed-docset suggested-docset))
      (setq my-devdocs-io--last-typed-docset typed-docset)))
    (--> typed-input (url-hexify-string it) (concat base it) (browse-url it))))

(provide 'my-functions-documentation)

;; -*- lexical-binding: t; -*-

(require 'vterm)

;; Keep `vterm' buffer to copy its contents
(let ((fn (lambda (&rest _) (setq vterm-kill-buffer-on-exit nil))))
  (my-add-advice-once #'vterm :before fn)
  (funcall fn))

(add-hook 'minions-prominent-modes 'vterm-copy-mode)

;; ;; modifying this after defining `vterm-mode-map' will remove key bindings whose prefixes match
;; (setq! vterm-keymap-exceptions (pushnew! vterm-keymap-exceptions "C-l" "<f1>"))

(my-bind :map 'vterm-mode-map "<deletechar>" #'vterm-send-delete)
(my-bind :map 'vterm-mode-map "<f1>" nil)
(my-bind :map 'vterm-mode-map "C-M-S-d" #'my-vterm-quit)
(my-bind :map 'vterm-mode-map "C-S-t" #'my-new-shell||terminal)
(my-bind :map 'vterm-mode-map "C-S-v" #'vterm-yank)
(my-bind :map 'vterm-mode-map "C-l" nil)
(my-bind :map 'vterm-mode-map "M-y" #'vterm-yank-pop)
(my-bind :map 'vterm-mode-map :vi 'insert '("<delete>") #'vterm-send-delete)
(my-bind :map 'vterm-mode-map :vi 'normal "<next>" (lookup-key global-map (kbd "<next>")))
(my-bind :map 'vterm-mode-map :vi 'normal "<prior>" (lookup-key global-map (kbd "<prior>")))

;; Use when "C-/" is overridden

;; (when-let ((cmd (lookup-key cua-global-keymap (kbd "C-/")))
;;            (commandp cmd))
;;   (define-key vterm-mode-map (vector 'remap cmd) #'vterm-undo))

;; (add-hook 'vterm-mode-hook
;;           (lambda (&rest _)
;;             (local-set-key (kbd "C-/") #'vterm-undo)))

;; (add-hook 'kill-emacs-hook 'my-try-to-exit-term-buffers-a)
(advice-add #'save-buffers-kill-emacs :before #'my-try-to-exit-term-buffers-a)

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'my-after-vterm)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-dummy-disabled-command ()
  (interactive)
  (user-error "Disabled command!"))

;;;###autoload
(defun my-try-to-balance-current-line-by-deleting-from-right ()
  (interactive)
  (let ((line-str0 (buffer-substring (line-beginning-position) (line-end-position)))
        (line0 (line-number-at-pos)))
    (save-excursion
      (end-of-line)
      (while (and (not (my-balanced-parens-and-brackets-p))
                  (not (bolp))
                  (= line0 (line-number-at-pos)))
        (delete-char -1))
      ;; "undo" deletion when still unbalanced
      (when (or (not (my-balanced-parens-and-brackets-p))
                (my-point-in-emptyish-line-p))
        (replace-region-contents (line-beginning-position)
                                 (line-end-position)
                                 (lambda ()
                                   line-str0))))))

;;;###autoload
(defun my-chmod-u+x-make-current-buffer-file-executable-when-is-script ()
  (interactive)
  (executable-make-buffer-file-executable-if-script-p))

;;;###autoload
(defun my-save-unsaved-buffers-with-files (&optional arg)
  "Like `save-some-buffers', buf only ask once.
ARG is passed to `save-buffer'."
  (interactive "P")
  (let ((bufs (seq-map #'buffer-name
                       (--filter (and (buffer-modified-p it) (buffer-file-name it))
                                 (buffer-list)))))
    (cond
     ((length= bufs 0)
      (my-message 'my-save-unsaved-buffers-with-files
                  ": all buffers are already saved."))
     ((y-or-n-p (format "Save %s files: \n%s ?"
                        (length bufs)
                        (string-join bufs "\n")))
      (dolist (buf bufs)
        (with-current-buffer buf
          (save-buffer arg)))))))

;;;###autoload
(defun my-rename|move-file&buffer (old-path new-path)
  "`rename-visited-file' with `my-read-new-filename' for input."
  (interactive (-let* ((old
                        (or buffer-file-name
                            (bound-and-true-p dired-directory))))
                 (list
                  old
                  (my-read-new-filename
                   (format "%s: " #'my-rename|move-file&buffer) old))))
  (-let* ((dir-flag (file-directory-p old-path)))
    (make-directory (file-name-parent-directory new-path) t)
    (rename-file old-path new-path 0)
    (if dir-flag
        (dired new-path)
      (set-visited-file-name new-path t t))))
;;;###autoload
(defalias #'my-rename|move-buffer&file #'my-rename|move-file&buffer)

;;;###autoload
(defun my-completing-read-multiple-buffers (prompt)
  (let ((cands (seq-map #'buffer-name (buffer-list))))
    (completing-read-multiple
     prompt
     (lambda (str pred action)
       (if (equal action 'metadata)
           `(metadata (category . buffer))
         (complete-with-action action cands str pred))))))

;;;###autoload
(defun my-copy-current-git-branch-name ()
  (interactive)
  (kill-new (print (my-vc-git-get-current-branch))))

;;;###autoload
(defun my-expand-all-customize-nodes ()
  (interactive)
  (my-save-line-and-column
    (goto-char (point-min))
    (while (re-search-forward
            (rx-to-string `(seq bol (or "Show Value"
                                        "Show [")))
            nil ':noerror)
      (beginning-of-line)
      (Custom-newline (point)))))

;;;###autoload
(defun my-make-export-buffer-writable-to-edit ()
  (interactive)
  (funcall
   (-find (lambda (cmd)
            (where-is-internal cmd (list (current-local-map))))
          '(wgrep-change-to-wgrep-mode
            occur-edit-mode
            wdired-change-to-wdired-mode))))

;;;###autoload
(defun my-dummy-filler-text-lorem-ipsum ()
  (replace-regexp-in-string
   "\n" " "
   "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
enim ad minim veniam, quis nostrud exercitation ullamco laboris
nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
reprehenderit in voluptate velit esse cillum dolore eu fugiat
nulla pariatur. Excepteur sint occaecat cupidatat non proident,
sunt in culpa qui officia deserunt mollit anim id est laborum."))

;;;###autoload
(defun my-copy-dummy-filler-text-lorem-ipsum (&optional insert)
  (interactive "P")
  (my-copy-or-insert (my-dummy-filler-text-lorem-ipsum) insert))

;;;###autoload
(defun my-copy-dummy-filler-text-lorem-ipsum-no-spaces (&optional insert)
  (interactive "P")
  (my-copy-or-insert
   (replace-regexp-in-string " " "" (my-dummy-filler-text-lorem-ipsum))
   insert))

;;;###autoload
(defun my-copy-special-character (&optional with-initial-input)
  (interactive "P")
  (minibuffer-with-setup-hook
      (lambda () (when with-initial-input (insert "^greek [[:space:]]")))
    (kill-new (char-to-string (read-char-by-name "Copy: ")))))

;;;###autoload
(defmacro my-query-replace--with-whole-buffer (&rest body)
  (declare (debug t))
  `(save-excursion
     (letrec ((adv (lambda (&rest _)
                     (goto-char (point-min))
                     (advice-remove #'query-replace-read-args adv))))
       (unless (use-region-p)
         (advice-add #'query-replace-read-args :after adv))
       (unwind-protect
           (progn ,@body)
         (advice-remove #'query-replace-read-args adv)))))

;;;###autoload
(defun my-query-replace-regexp-in-whole-buffer ()
  (interactive)
  (my-query-replace--with-whole-buffer
   ;; `anzu' isn't compatible OOTB
   (call-interactively #'query-replace-regexp)))

;;;###autoload
(defun my-query-replace-literal-in-whole-buffer ()
  (interactive)
  (my-query-replace--with-whole-buffer
   (call-interactively #'query-replace)))

;;;###autoload
(defun my-copy-filename-from-project-inclusive ()
  (interactive)
  (let ((path
         (file-name-concat (file-name-nondirectory
                            (directory-file-name (my-project-root)))
                           (file-relative-name (or (buffer-file-name)
                                                   default-directory)
                                               (my-project-root)))))
    (kill-new path)
    (message path)))

(defvar my-new-buffer-default-major-mode #'text-mode)

(defvar-local my-new-buffer--mark nil)
;;;###autoload
(defun my-new-buffer (&optional name mode)
  "Switch a new buffer with `major-mode' as MODE named NAME.
If MODE is a major mode function, call it; else when non-nil, inherit
from current buffer (default when interactively:
`my-new-buffer-default-major-mode')."
  (interactive (list
                nil
                (if current-prefix-arg
                    t
                  my-new-buffer-default-major-mode)))
  (-let* ((name (or name "new"))
          (mm major-mode))
    (prog1 (switch-to-buffer (generate-new-buffer name))
      (cond
       ((functionp mode)
        (funcall mode))
       (mode
        (funcall mm)))
      (setq-local my-new-buffer--mark t)
      (my-safe-call #'evil-insert-state))))
;;;###autoload
(defalias #'my-new #'my-new-buffer)


;;;###autoload
(defun my-new-buffer-maybe (&optional name mode)
  (interactive)
  (-if-let* ((my-new-buffer-last-empty
              (-->
               (buffer-list)
               (-filter
                (lambda (buf)
                  (with-current-buffer buf
                    (and my-new-buffer--mark
                         (string-match-p "\\`[ \t\n\r]*\\'" (buffer-string)))))
                it)
               (-map #'buffer-name it)
               (sort it (lambda (buf0 buf1) #'string-lessp))
               car)))
      (progn
        (switch-to-buffer my-new-buffer-last-empty))
    (progn
      (call-interactively #'my-new-buffer)
      (text-scale-adjust 7))))

;;;###autoload
(defun my-to-messages ()
  (interactive)
  (switch-to-buffer (messages-buffer)))
;;;###autoload
(defalias #'my-2msg #'my-to-messages)   ; Type quicker on the command prompt

;;;###autoload
(defun my-lsp-remove-workspaces (&optional invalid-only)
  "Remove LSP all workspaces.
With non-nil INVALID-ONLY, only remove workspaces which doesn't
exist anymore.
Try this to clean all workspaces first when LSP hangs."
  (interactive "P")
  (with-eval-after-load 'lsp-mode
    (dolist (session (lsp-session-folders (lsp-session)))
      (when (or (not invalid-only)
                (file-directory-p session))
        (lsp-workspace-folders-remove session)
        (message "Removed %s" session)))))

;;;###autoload
(defun my-set-sensible-default-face-height (&optional target-line-pixel-height)
  (interactive (list (or current-prefix-arg 18)))
  (custom-set-faces
   `(default
     ((t :height
         ,(floor
           (* target-line-pixel-height
              (face-attribute 'default :height) (/ 1.0 (line-pixel-height)))))))))

;;;###autoload
(defun my-set-default-face-height (height)
  (interactive (list
                (read-number "Height?: " (face-attribute 'default :height))))
  (custom-set-faces
   `(default ((t (:height ,height))))))

;;;###autoload
(defun my-swap||substitute-text||string (rbeg rend &rest strings)
  "In region from RBEG to REND, rotate STRINGS.
Enter 2 STRINGS by default. With non-nil prefix argument, prompt
for the number of STRINGS."
  (interactive
   (append
    (list (region-beginning) (region-end))
    (if current-prefix-arg
        (let ((num (read-number "Number of strings: ")))
          (seq-map
           (my-fn% (read-string (concat (number-to-string %1) ": ")))
           (number-sequence 1 num)))
      (list (read-string "First: ") (read-string "Second: ")))))
  (let ((old (buffer-substring-no-properties rbeg rend)))
    (delete-region rbeg rend)
    (insert (apply #'my-cyclic-literal-string-substitute old strings))))

;;;###autoload
(defun my-cd-home (&rest _)
  (interactive)
  (cd "~"))

;;;###autoload
(defun my-copy|clone-buffer (&optional name)
  (interactive)
  (let* ((mode major-mode)
         (new-buff
          (get-buffer-create
           (or name (format "*%s %s*" (buffer-name) 'my-copy|clone-buffer))))
         (clone-buf (my-make-clone-buffer-by-contents nil new-buff)))
    (dlet ((display-buffer-alist '()))
      (pop-to-buffer new-buff)
      (funcall mode))
    new-buff))

(defun my-recentf-add-directories ()
  "Add recent directories to `recentf'."
  (interactive)
  (require 'recentf)
  (dolist (f recentf-list)
    (recentf-add-file (file-name-directory f))))

;;;###autoload
(defun my-insert-and-copy-date ()
  "Insert current date in ISO format."
  (interactive)
  (my-insert-and-copy (format-time-string "%F" (current-time))))

;;;###autoload
(defun my-insert-and-copy-date-and-time (&optional UTC)
  "Insert current date & time.
UTC: use it."
  (interactive "P")
  (my-insert-and-copy (my-ISO-time nil (and UTC t))))

;;;###autoload
(defun my-position-in-project ()
  (-let* ((filename (my-buffer-filename)))
    (format "%s:%s:%s"
            (if filename
                (file-relative-name filename
                                    (my-project-root-or-current-directory))
              (file-name-concat (or (-some-->
                                        (project-current) (project-name it))
                                    (f-filename default-directory))
                                (buffer-name)))
            (line-number-at-pos) (current-column))))

;;;###autoload
(defun my-insert-position-in-project ()
  (interactive)
  (insert (my-position-in-project)))

;;;###autoload
(defun my-open-files-with-mode-in-dir (mode dir &rest cmds)
  "In DIR, open all files with the same major-mode as MODE quietly.
Run CMDS on them."
  (interactive)
  (save-window-excursion
    (let ((regexp (my-mode-regexp-list mode 'single 'dont-load)))
      ;; Sometimes `directory-files'' MATCH doesn't match correctly
      (dolist (file (directory-files dir))
        (when (and (string-match-p regexp file)
                   (not (get-file-buffer file)))
          (with-current-buffer (find-file-noselect file 'nowarn)
            (mapc #'funcall cmds)))))))

;;;###autoload
(defun my-list-dianostics-error-warning-problems ()
  (interactive)
  (cond
   ((bound-and-true-p lsp-mode)
    (my-open-files-with-mode-in-dir 'prog-mode default-directory #'lsp)))
  (flymake-show-project-diagnostics))

;;;###autoload
(defun my-insert-and-copy-random-C-identifier (&optional number)
  "Insert NUMBER (default: 16) alphanumeric/_ characters whose name can be a C-type variable."
  (interactive "P")
  (let* ((char-list "_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
         (alphanumeric-list (concat char-list "0123456789")))
    (my-insert-and-copy
     (concat (byte-to-string (seq-random-elt char-list))
             (mapconcat (lambda (_) (byte-to-string (seq-random-elt alphanumeric-list)))
                        (number-sequence 2 (or number 16))
                        "")))))

;;;###autoload
(defun my-recenter-left-right ()
  "Move current buffer column to the specified window column."
  (interactive)
  (set-window-hscroll (selected-window)
                      (- (current-column) (/ (window-width) 2))))

(defun my--y-or-n-to-apply (func &rest args)
  "When answer is \"y\", apply FUNC on ARGS and return the result.
Show \"*Help*\" for the first of ARGS if it is a function, else FUNC."
  (let ((1st-arg (car args)))
    (describe-function (if (and (symbolp 1st-arg)
                                (fboundp 1st-arg))
                           1st-arg
                         func)))
  (unwind-protect
      (when (y-or-n-p (format "%s %s" (symbol-name func) args))
        (apply func args))
    (quit-windows-on (help-buffer))))

;;;###autoload
(defun my-pop-back-or-undo-window ()
  (interactive)
  (if (xref-marker-stack-empty-p)
      (pop-global-mark)
    (pop-tag-mark)))

;;;###autoload
(defun my-copy-filename-with-path-xor-directory-to-clipboard (&optional dir)
  "Copy current file name (or directory), with non-nil DIR, force current directory."
  (interactive "P")
  (let ((target (if dir
                    default-directory
                  (or (buffer-file-name)
                      default-directory))))
    (kill-new target)
    (message target)))

;;;###autoload
(defun my-save-buffer-no-hook ()
  "Save buffer without running `before-save-hook' & `after-save-hook'."
  (interactive)
  (dlet ((before-save-hook nil)
         (after-save-hook nil))
    (save-buffer)))

;;;###autoload
(defun my-hydra-from-prefix-string (prefix)
  (interactive (list (read-library-name)))
  (when (or (stringp prefix)
            (symbolp prefix))
    (require (intern prefix) nil 'noerror))
  (let* ((commands (cl-loop for sym being the symbols
                            when (and (commandp sym)
                                      (string-match-p (concat "^" (regexp-quote (format "%s" prefix)))
                                                      (symbol-name sym)))
                            collect sym))
         (hydra-body (my-hydra-define (intern prefix) commands)))
    (if (called-interactively-p 'any)
        (call-interactively hydra-body)
      hydra-body)))

;;;###autoload
(defun my-fold-hydra ()
  (interactive)
  (call-interactively (my-hydra-from-prefix-key "fold"
                                                "z"
                                                'match-name
                                                evil-motion-state-map
                                                evil-normal-state-map)))

;;;###autoload
(defun my-disable-line-indicators ()
  "Locally disable highlighting current line and showing line numbers."
  (interactive)
  (setq-local hl-line-mode nil)
  (display-line-numbers-mode 0))

;;;###autoload
(defun my-clean-and-save-buffer (&rest _args)
  (interactive)
  (whitespace-cleanup)
  (indent-region (point-min) (point-max))
  (call-interactively 'save-buffer))

(defvar my-refresh-find|open-current-file-before-hook '()
  "Hook before running `my-refresh-find|open-current-file'.")
(defvar my-refresh-find|open-current-file-after-hook '()
  "Hook after running `my-refresh-find|open-current-file'.
Note that this will run regardless of refreshing errors.")
;;;###autoload
(defun my-refresh-find|open-current-file (&optional true-name)
  (declare (interactive-only t))
  (interactive (list current-prefix-arg))
  (my-save-position-in-window
   (my-evil-save-state
    (my-profiler-cpu-when-debug-progn
     (run-hooks 'my-refresh-find|open-current-file-before-hook)
     (unwind-protect
         (progn
           (when (bound-and-true-p hide-mode-line-mode)
             (hide-mode-line-mode 0))
           (-if-let* ((f (my-buffer-filename))
                      (_ my-buffer-filename-can-open-with-find-file))
               (my-with-saved-windows-displaying-current-buffer-file
                 (progn
                   (kill-buffer (current-buffer))
                   ;; do not try to open the symlink target when unwanted
                   (my-with-find-file-follow-true-name true-name (find-file f))))
             (progn
               (revert-buffer))))))))
  (run-hooks 'my-refresh-find|open-current-file-after-hook))

;;;###autoload
(defun my-re-open-current-frame ()
  (interactive)
  (let ((wc (current-window-configuration))
        (old-frame (selected-frame)))
    (let ((new-frame (make-frame)))
      (with-selected-frame new-frame
        (set-window-configuration wc))
      (delete-frame old-frame)
      (select-frame new-frame))))

;;;###autoload
(defun my-disable-relative-line-number (&optional all-buffers)
  (interactive "P")
  (dolist (buf (if all-buffers
                   (my-buffers-with-minor-mode 'display-line-numbers-mode)
                 (list (current-buffer))))
    (with-current-buffer buf
      (dlet ((display-line-numbers-type t))
        (display-line-numbers-mode)))))

;;;###autoload
(defun my-current-buffer-in-terminal-emacs-client ()
  (interactive)
  (-let* ((expr `(switch-to-buffer ,(buffer-name)))
          (cmdargs
           (my-terminal-external-emulator-cmdargs
            (list "emacsclient" "-t" "--eval" (format "%S" expr)))))
    (apply #'start-process
           "my-current-buffer-in-terminal-emacs-client"
           nil
           cmdargs)))

;;;###autoload
(defun my-save-buffer-and-close ()
  (interactive)
  (save-buffer)
  (condition-case _
      (delete-frame nil 'force)
    ('error (save-buffers-kill-terminal))))

;;;###autoload
(defun my-git-commit-with-empty-message ()
  (interactive)
  (-let* ((cmdargs
           (split-string-shell-command
            (read-shell-command "Execute: "
                                "git commit --allow-empty-message -m \"\""))))
    (my-process-async
     cmdargs (lambda (&rest _) (my-safe-call #'magit-refresh-all)))))

(my-autoload-functions 'pixel-scroll '(pixel-scroll-precision-interpolate))

(defun my-scroll-view-portion--scroll (portion &optional back)
  (cl-assert (numberp portion))
  (cond
   ((fboundp #'pixel-scroll-precision-mode)
    (pixel-scroll-precision-interpolate
     (-->
      (window-text-height nil 'pixelwise) (* it portion) (round it)
      ;; reduce the distance to move a bit to prevent moving the point when
      ;; possible
      (- it (line-pixel-height))
      (if back
          it
        (- it)))
     nil 1))
   (t
    (scroll-down-command
     (-->
      (window-text-height) (* it portion) (round it) 1-
      (if back
          it
        (- it)))))))

(defvar-local my-scroll-view-portion--last-prefix-arg nil)

;;;###autoload
(defun my-scroll-view-portion-forward (arg &optional backwward)
  "Scroll the view ARG / 8 page height forward, (half the page by default).
BACKWWARD: scroll backward. Remember the last ARG so consecutive
scrolls will do the same amount."
  (interactive "P")
  (-let* ((arg
           (or arg
               (and (member
                     last-command
                     '(my-scroll-view-portion-forward
                       my-scroll-view-portion-backward))
                    my-scroll-view-portion--last-prefix-arg)
               4.0)))
    (setq-local my-scroll-view-portion--last-prefix-arg arg)
    (my-scroll-view-portion--scroll (/ arg 8.0) backwward)))

;;;###autoload
(defun my-scroll-view-portion-backward (arg)
  "See (`my-scroll-view-portion-forward' ARG)."
  (interactive "P")
  (my-scroll-view-portion-forward arg 'backward))

;;;###autoload
(defun my-mark-jump-back-in-current-buffer ()
  "`set-mark-command' with \\[universal-argument].
~ `pop-to-mark-command'."
  (interactive)
  (set-mark-command '(4)))

;;;###autoload
(defun my-push-button-same-window (&optional pos use-mouse-action)
  (interactive
   (list (if (integerp last-command-event) (point) last-command-event)))
  (my-with-display-buffer-same-window
   (push-button pos use-mouse-action)))

;;;###autoload
(defun my-eval-last-sexp-and-insert-after-point ()
  (interactive)
  (save-excursion
    (eval-last-sexp t)))

;;;###autoload
(defun my-with-saved-selected-window-press-enter|return ()
  (interactive)
  (save-selected-window
    (call-interactively
     (or (key-binding (kbd "<return>"))
         (key-binding (kbd "RET"))))))

;;;###autoload
(defun my-with-pop-to-buffer-force-other-window-save-selected-press-enter|return ()
  (interactive)
  (save-selected-window
    (my-with-advice
      #'pop-to-buffer
      :around
      (lambda (func buf &optional action &rest args)
        (apply func buf 'display-buffer-use-some-window args))
      (call-interactively
       (or (key-binding (kbd "<return>")) (key-binding (kbd "RET")))))))

;;;###autoload
(defun my-occur-outline-imenu ()
  (interactive)
  (dlet ((list-matching-lines-face nil))
    (-let* ((regexp (--> (-map #'-second-item imenu-generic-expression)
                         (cons outline-regexp it)
                         (my-regexp-or it))))
      (occur regexp))))

;;;###autoload
(defun my-list-symlinks-to-file (file)
  (interactive (list buffer-file-name))
  (-let* ((cmd
           (read-shell-command "`my-list-symlinks-to-file': "
                               (format "find -L ~ -samefile \"%s\" 2>/dev/null"
                                       file)))
          (buf
           (generate-new-buffer
            (format "*%s %s*" 'my-list-symlinks-to-file cmd))))
    (async-shell-command cmd buf)
    (pop-to-buffer buf)))

;;;###autoload
(defun my-insert-multiple-times (arg)
  (interactive "P")
  (-let* (
          ;; with prefix arg: use that number, else ask for it
          (n
           (cond
            (arg
             (prefix-numeric-value arg))
            (t
             (read-number (format "Times to insert: ")))))
          (str (read-string (format "Insert %s times: " n))))
    (dotimes (_ n)
      (insert str))))

;;;###autoload
(defun my-echo-buffer-content (buf)
  (--> (with-current-buffer buf
         (my-buffer-string-no-properties))
       (string-trim-right it)
       (message "%s" it)))

;;;###autoload
(defun my-copy-kill-new-string-at-point ()
  (interactive)
  (kill-new (my-Str->str (thing-at-point 'string))))

;;;###autoload
(defun my-goto-random-line ()
  (interactive)
  (-let* ((bound (line-number-at-pos (point-max)))
          (des-line (--> (random bound) (+ 1 it))))
    (goto-char (point-min))
    (forward-line (1- des-line))))



;;;###autoload
(defun my-copy-buffer-name (&optional echo)
  (interactive (list t))
  (-->
   (buffer-name)
   (progn
     (kill-new it)
     (when echo
       (message "%s" it))
     it)))

;;;###autoload
(defun my-copy-dedented-region ()
  (interactive)
  (-let* ((beg (region-beginning))
          (end (region-end))
          (str0 (buffer-substring beg end))
          (min-indent (my-find-minimum-indentation-of-string str0))
          (lines (split-string str0 "\n"))
          (retval
           (-->
            lines
            (-map
             (lambda (line)
               (cond
                ((null line)
                 "")
                (:else
                 (substring line (min min-indent (length line))))))
             it)
            (string-join it "\n"))))
    (my-clipboard-kill-new-or-copy retval)
    retval))


(provide '16-my-commands)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-replace-spaces-in-filenames (&optional dir no-ask)
  (-let* ((dir (or dir default-directory))
          (files (directory-files-recursively dir " ")))
    (dolist (file files)
      (-let* ((dest (string-replace " " "_" file))
              (cmd (format "mkdir -p %s ; \
mv -nv '%s' '%s'"
                           (file-name-parent-directory dest)
                           file
                           dest)))
        (make-directory (file-name-parent-directory dest) 'parents)
        (rename-file file dest)))))

(provide 'my-scripts)

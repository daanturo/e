;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; loading package.el is
(require 'package)

(require 'my-pkg)

;;; Package.el


(defconst my-git-user/repo-regexp "^[-_[:alnum:].]+/[-_[:alnum:].]+$"
  "\\(\\.git\\)? at last is not needed.")

;;;###autoload
(defun my-pkg-str8-recipe-auto-host (plst)
  "Automatically add :host nil to PLST when needed."
  (-let* (((&plist :host host :repo repo) plst))
    (cond
     ((or host (string-match-p my-git-user/repo-regexp repo))
      plst)
     (t
      (plist-put plst :host nil)))))

;;;###autoload
(defun my-pkg-elpaca-remotes->str8-fork (lst)
  "Convert (1st of) LST in elpaca's :remotes form to straight's :fork form.
(\"fork\" :repo \"fork/zenburn-emacs\")
or
((\"fork\" :repo \"fork/zenburn-emacs\") (\"other\" :host gitlab :repo \"other/zenburn-emacs\"))

to
(:remote \"fork\" :repo \"fork/zenburn-emacs\").
"
  (-let* ((fork-args
           (if (-every #'proper-list-p lst)
               (car lst)
             lst)))
    `(:remote ,(car fork-args) ,@(cdr fork-args))))

;;;###autoload
(cl-defun my-pkg-args->str8-recipe (pkg
                                    url
                                    &rest
                                    args
                                    &key
                                    remotes
                                    fork
                                    branch
                                    _rev
                                    files
                                    depth
                                    local-repo
                                    host
                                    build
                                    &allow-other-keys)
  `(,pkg
    ,@(and url (my-pkg-str8-recipe-auto-host `(:host ,host :repo ,url)))
    ,@(and fork `(:fork ,fork))
    ,@(and remotes
           `(:fork
             ,(my-pkg-str8-recipe-auto-host
               (my-pkg-elpaca-remotes->str8-fork remotes))))
    ,@(and local-repo `(:local-repo ,local-repo))
    ,@(and (member :host args) `(:host ,host))
    ,@(and branch `(:branch ,branch))
    ,@(and files `(:files ,files))
    ,@(cond
       (depth
        `(:depth ,depth))
       ;; explicit :depth nil
       ((member :depth args)
        `(:depth full)))
    ,@(and build `(:build ,build))))

;;;###autoload
(cl-defun my-pkg-args->elpaca-recipe (pkg
                                      url
                                      &rest
                                      args
                                      &key
                                      remotes
                                      branch
                                      rev
                                      depth
                                      local-repo
                                      build
                                      &allow-other-keys)
  `(,pkg
    ,@(and url `(:repo ,url))
    ,@(and remotes `(:remotes ,remotes))
    ,@(and local-repo `(:repo ,local-repo))
    ,@(and branch `(:branch ,branch))
    ,@(and rev `(:ref ,rev :pin t))
    ,@(and depth `(:depth ,depth))
    ,@(and build `(:build ,build))))

;;


;;;###autoload
(defun my-package-el-read-package-name (prompt)
  (-let* ((coll (-map (lambda (elem)
                        (symbol-name (car elem)))
                      package-archive-contents)))
    (completing-read prompt
                     (lambda (string pred action)
                       (if (eq action 'metadata)
                           `(metadata (category . package))
                         (complete-with-action action coll string pred))))))

;;;###autoload
(defun my-package-el-auto-uninstall-undeclared (&optional ask)
  (interactive (list t))
  (require 'package-vc)
  (require 'my-packages
           (file-name-concat my-emacs-conf-dir/ "packages"))
  (-let* ((declared-lst (my-pkg!--get-list t))
          (depends-lst (package--get-deps declared-lst))
          (del-lst
           (seq-difference
            (append
             package-selected-packages
             (-map #'car package-vc-selected-packages))
            (append declared-lst depends-lst))))
    (when
        (cond
         ((= 0 (length del-lst))
          (message
           "`my-package-el-auto-uninstall-undeclared': no packages to uninstall.")
          nil)
         (ask
          (y-or-n-p
           (format
            "`my-pkg-auto-uninstall-undeclared' %d packages: %s"
            (length del-lst) del-lst)))
         (:else
          t))
      (mapc #'my-package-el-uninstall del-lst))))

;;;###autoload
(autoload #'package-delete "package" nil t)

;;;###autoload
(defun my-package-quickstart-refresh-and-load ()
  (interactive)
  (package-quickstart-refresh)
  (load (file-name-sans-extension package-quickstart-file)))

;;; Other operations

(autoload 'epkg-read-package "epkg")

;;;###autoload
(defun my-read-package-name (prompt)
  (cond
   ;; allow non-matching
   (package-archive-contents
    (my-package-el-read-package-name prompt))
   ((fboundp #'epkg-update)
    (epkg-read-package prompt))
   ((fboundp #'elpaca)
    (--> (elpaca-menu-item t) car))
   ((fboundp #'straight-get-recipe)
    (--> (straight-get-recipe) car))))

;;;###autoload
(defun my-install-local-package (package-name)
  (declare (interactive-only t))
  (interactive (list (my-read-package-name "Install: ")))
  (require 'my-packages (file-name-concat my-emacs-conf-dir/ "packages.el"))
  (with-current-buffer
      (my-append-to-file-interactively
       (format "%s" `(my-local-pkg! ',package-name))
       (my-ensure-empty-elisp-file my-local-packages-el))
    (my-ensure-final-newline)
    (my-ask-for-emacs-refresh)))

;;; my-functions-package.el ends here

(provide 'my-functions-package)

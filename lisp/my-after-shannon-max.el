;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; TODO: request this instead of advising
(advice-add
 #'shannon-max-logger-save
 :around #'my-with-quiet-write-region-to-file-a)

;;; my-after-shannon-max.el ends here

(provide 'my-after-shannon-max)

;; Local Variables:
;; no-byte-compile: t
;; End:

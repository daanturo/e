;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'image)
(require 'image-converter)

;;;###autoload
(defun my-image-at-point ()
  (-let* ((displ (get-char-property (point) 'display)))
    (or (-some--> displ
          (assoc 'image it)
          (and (imagep it) it))
        displ)))

;;;###autoload
(defun my-save-and-open-by-default-application-image-at-point (&optional path)
  (interactive)
  (-let* ((image (my-image-at-point))
          (dot-ext
           (-->
            (my-key-value-in-mixed-plist image)
            (plist-get it :type)
            (format ".%s" it)))
          (path
           (concat
            (file-name-concat (my-temp-dir "emacs-image")
                              (secure-hash 'md5 (format "%s" image)))
            dot-ext)))
    (my-save-image-object-to-file image path)
    (my-xdg-open-file-with-default-application path)
    (kill-new path)))

;;;###autoload
(defun my-save-image-object-to-file (image-obj path)
  (-let* ((plist (cdr image-obj))
          (data (plist-get plist :data)))
    (if data
        (f-write data 'no-conversion path)
      (f-copy (plist-get plist :file) path))))

;;;###autoload
(defun my-image-context-menu (menu &optional _click)
  (define-key-after menu [my-image-context-menu] menu-bar-separator)
  (define-key-after menu [my-image-display-recognized-ocr-tesseract]
    `(menu-item
      "my-image-display-recognized-ocr-tesseract"
      my-image-display-recognized-ocr-tesseract))
  (define-key-after menu [image-save]
    `(menu-item
      "Save image"
      ,(lambda (e)
         (interactive "e")
         (dlet ((use-dialog-box (mouse-event-p e)))
           (image-save)))))
  (define-key-after
    menu [my-save-and-open-by-default-application-image-at-point]
    `(menu-item
      "Open image externally"
      my-save-and-open-by-default-application-image-at-point))
  menu)

;;;###autoload
(defun my-image-display-recognized-ocr-tesseract (img)
  (interactive (list (my-image-at-point)))
  (-let* ((file-path
           (or (-some--> (my-mixed-plist-get img :file) (expand-file-name it))
               (-let* ((tmp (make-temp-file "image")))
                 (my-save-image-object-to-file img tmp)
                 tmp)))
          (name
           (format "*%s %s*"
                   'my-image-display-recognized-ocr-tesseract
                   (file-name-nondirectory file-path)))
          (buf (generate-new-buffer-name name))
          (callback
           (lambda (proc-arg _event)
             (with-current-buffer (process-buffer proc-arg)
               (goto-char (point-min)))))
          (proc (start-process name buf "tesseract" file-path "-")))
    (set-process-sentinel proc callback)
    (switch-to-buffer-other-window buf)
    (goto-char (point-min))))

;; (dlet ((coding-system-for-write 'no-conversion)) (image-save))

(defvar my-image-preferred-type 'image/webp)
(defvar my-image-preferred-type-extension-fn
  (lambda () (my-mime-type->extension my-image-preferred-type))
  "A function with 0 arguments.")

(defun my-image-convert-clipboard-to-preferred-format--convert-by-elisp
    (content input-types)
  (-let* ((converters
           (list
            'ffmpeg ; found this to be the most reliable
            image-converter))
          (converted-img
           (-some
            (lambda (converter)
              (dlet ((image-convert-to-format
                      (string-remove-prefix
                       "image/" (symbol-name my-image-preferred-type)))
                     (image-converter converter))
                (-some
                 (lambda (img-typ)
                   (-let* ((img0 content))
                     (ignore-errors
                       (image-convert img0 img-typ))))
                 input-types)))
            converters)))
    (when (stringp converted-img)
      (put-text-property
       0 (length converted-img) 'foreign-selection my-image-preferred-type
       converted-img))
    ;; (gui-set-selection clipboard-type converted-img)
    converted-img))

;;;###autoload
(defun my-image-convert-clipboard-to-preferred-format--mixed
    (&optional clipboard-type)
  "Important: There is usually a delay until the clipboard is updated.
Convert image in clipboard's CLIPBOARD-TYPE to
`my-image-preferred-type'.  NO-REPLACE-CLIPBOARD: don't replace the
clipboard, just convert and return."
  (declare (obsolete nil nil))
  (interactive)
  (-when-let* ((clipboard-type (or clipboard-type 'CLIPBOARD))
               (img-typ-pred
                (lambda (typ) (string-prefix-p "image/" (symbol-name typ))))
               (input-image-type-list
                (-->
                 (seq-into (gui-get-selection clipboard-type 'TARGETS) 'list)
                 (-filter img-typ-pred it)
                 (nreverse it)))
               (_
                (and (not (seq-empty-p input-image-type-list))
                     (not
                      (member my-image-preferred-type input-image-type-list))))
               (input-type (nth 0 input-image-type-list))
               (success-pred
                (lambda ()
                  (-let* ((types (gui-get-selection clipboard-type 'TARGETS)))
                    (seq-contains-p types my-image-preferred-type))))
               (extension (funcall my-image-preferred-type-extension-fn))
               (temp-file-prefix
                (concat
                 "my-image-convert-clipboard-to-preferred-format"
                 "--"
                 (my-ISO-time nil nil t)))
               (temp-file-0
                (make-temp-file (concat temp-file-prefix "--0")
                                nil
                                (my-mime-type->extension input-type t)))
               (temp-file-1
                (make-temp-file (concat temp-file-prefix "--1")
                                nil
                                (format ".%s" extension))))
    (my-clipboard-get :mime-type input-type :file-out temp-file-0)
    (-let* ((convert-cmdargs (list "ffmpeg" "-y" "-i" temp-file-0 temp-file-1))
            (convert-proc (my-process-sync-run convert-cmdargs)))
      (cond
       ((/= 0 (plist-get convert-proc :exit))
        (message "`my-image-convert-clipboard-to-preferred-format' %S error: %S"
                 convert-cmdargs
                 (plist-get convert-proc :err))
        nil)
       (:else
        (delete-file temp-file-0)
        (message
         "`my-image-convert-clipboard-to-preferred-format' %s (deleted) -> %s"
         temp-file-0 temp-file-1)
        (-let* ((converted-img-elisp-data (create-image temp-file-1)))
          (my-clipboard-copy-file-contents temp-file-1)
          converted-img-elisp-data))))))

;;;###autoload
(defun my-image-convert-clipboard-to-preferred-format ()
  "Convert image in clipboard to `my-image-preferred-type'."
  (interactive)
  (-when-let* ((extension (funcall my-image-preferred-type-extension-fn))
               (out-file
                (my-process-sync-success-stdout
                 (list
                  (file-name-concat my-emacs-bin-directory/
                                    "my-convert-clipboard-image-to")
                  extension)))
               (converted-img-elisp-data (create-image out-file)))
    converted-img-elisp-data))

(defvar-local my-image-show-in-new-buffer--img nil)
;;;###autoload
(defun my-image-show-in-new-buffer (img &optional buf)
  (interactive (list
                (-let* ((types
                         (-->
                          (gui-get-selection 'CLIPBOARD 'TARGETS)
                          (seq-filter
                           (lambda (typ)
                             (string-prefix-p "image/" (symbol-name typ)))
                           it)))
                        (img nil))
                  (while (null img)
                    (-let* ((displaying-type
                             (cond
                              ((< 1 (length types))
                               (intern
                                (completing-read
                                 "my-image-show-in-new-buffer valid type: "
                                 types)))
                              (:else
                               (seq-first types)))))
                      (setq img
                            (gui-get-selection 'CLIPBOARD displaying-type))))
                  img)
                nil))
  (cl-assert img)
  (-let* ((buf
           (or buf
               (generate-new-buffer
                (format "*%s*" 'my-image-show-in-new-buffer)))))
    (display-buffer buf)
    (with-current-buffer buf
      (insert-image img)
      (setq my-image-show-in-new-buffer--img (purecopy img)))))


;;; my-functions-image.el ends here

(provide 'my-functions-image)

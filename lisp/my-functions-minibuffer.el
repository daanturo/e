;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-find-file-insert-/-after-~-h ()
  (when (and (eobp)
             (equal "~"
                    (buffer-substring-no-properties
                     (if (and (bound-and-true-p rfn-eshadow-overlay)
                              (overlay-buffer rfn-eshadow-overlay))
                         (overlay-end rfn-eshadow-overlay)
                       (minibuffer-prompt-end))
                     (point-max))))
    (insert "/")))

;;;###autoload
(defun my-minibuffer-preview-call-no-quit ()
  (interactive)
  (save-selected-window
    (unless (bound-and-true-p consult--preview-function)
      (my-embark-dwim-no-quit))))

;;;###autoload
(defun my-minibuffer-next-line-and-preview (arg)
  (interactive "p")
  ;; (setq my-window-config0 (current-window-configuration))
  ;; only vertical completion UIs
  (funcall (key-binding (kbd "<down>")) arg)
  ;; ;; When `consult--preview-function' doesn't do anything like open the
  ;; ;; file (therefore change `current-window-configuration'), but how
  ;; ;; about actions other than `find-file'?
  ;; (when (cond
  ;;        ((member (my-minibuffer-completion-category) '(file project-file))
  ;;         (compare-window-configurations my-window-config0 (current-window-configuration)))))
  (my-minibuffer-preview-call-no-quit))

;;;###autoload
(defun my-minibuffer-previous-line-and-preview (arg)
  (interactive "p")
  (my-minibuffer-next-line-and-preview (- arg)))

;;;###autoload
(defun my-minibuffer-filename-go-home ()
  (interactive)
  (goto-char (point-max))
  (insert "~/"))

;;;###autoload
(defun my-with-minibuffer-no-preselect-a (func &rest args)
  (my-with-minibuffer-no-preselect (apply func args)))

;;;###autoload
(cl-defun my-propertize-annotation (str &optional left-padding &key (face 'completions-annotations))
  "`propertize' STR such that it appears as an `minibuffer' annotation.
LEFT-PADDING: string to insert to the left, defaults to a tab character,
it can be a number, it that case that many spaces are inserted. FACE:
when nil, don't face propertize."
  (-->
   (concat
    (cond
     ((numberp left-padding)
      (make-string left-padding ?\s))
     (left-padding
      left-padding)
     (:else
      "\t"))
    str)
   (if face
       (propertize it 'face face)
     it)))

(defvar my-crm-insert-command-alist
  '((vertico-mode . vertico-insert)))

;;;###autoload
(cl-defun my-crm-insert-candidate-and-separator (&key (spaces t) keep)
  "For `completing-read-multiple', insert current candidate and separator.
KEEP: re-insert current input for continued filtering. SPACES: insert
spaces around the `crm-separator' (guessed literal)."
  (interactive)
  (-let* ((whole-input (buffer-substring (minibuffer-prompt-end) (point-max)))
          (current-lst (split-string whole-input crm-separator t))
          (current-cand-input (-last-item current-lst))
          (sep (my-crm-separator-literal))
          (sep-inserting
           (if spaces
               (concat " " sep " ")
             sep))
          (cand-insert-cmd
           (-some
            (-lambda ((k . v))
              (and (ignore-errors
                     (eval k t))
                   v))
            my-crm-insert-command-alist)))
    (prog1 (cond
            (cand-insert-cmd
             (funcall cand-insert-cmd))
            (:else
             (crm-complete)))
      (insert sep-inserting)
      (when (and keep current-cand-input)
        (insert current-cand-input)))))

;;;###autoload
(cl-defun my-crm-insert-candidate-and-keep-input ()
  (interactive)
  (my-crm-insert-candidate-and-separator :keep t))

;;;###autoload
(defun my-crm-setup-alternate-tab-to-insert-key ()
  (-let* ((current-tab (keymap-local-lookup "TAB"))
          (current-S-tab (keymap-local-lookup "S-TAB")))
    (when (and (not
                (equal #'my-crm-insert-candidate-and-keep-input current-tab)))
      ;; (null current-S-tab)
      (my-keymap-local-set*
       `(("S-TAB" current-tab)
         ("TAB" my-crm-insert-candidate-and-keep-input)
         ("<tab>" my-crm-insert-candidate-and-keep-input))))))

(defvar-local my-minibuffer-setup-hint--overlay nil)
;;;###autoload
(defun my-minibuffer-setup-hint (annotation-func)
  (setq-local my-minibuffer-setup-hint--overlay
              (make-overlay (point-max) (point-max)))
  ;; make the cursor looks like it stands before the overlay, credit: `dape--minibuffer-hint'
  (overlay-put
   my-minibuffer-setup-hint--overlay 'before-string (propertize " " 'cursor 0))
  (-let* ((update-fn
           (lambda (&rest _)
             (-let* ((input
                      (buffer-substring (minibuffer-prompt-end) (point-max)))
                     (annot (funcall annotation-func input)))
               (move-overlay
                my-minibuffer-setup-hint--overlay (point-max) (point-max))
               (overlay-put
                my-minibuffer-setup-hint--overlay
                'after-string
                (concat "\n\n" annot))))))
    (add-hook 'after-change-functions update-fn nil t)
    (funcall update-fn)))

(defvar my-crm-completing-read-multiple-setup-hook '())

;;;###autoload
(defvar-local my-crm-completing-read-multiple-flag nil)

;;;###autoload
(defun my-crm-completing-read-multiple-setup-hook-a (func &rest args)
  (minibuffer-with-setup-hook (lambda ()
                                (setq my-crm-completing-read-multiple-flag t)
                                (run-hooks
                                 'my-crm-completing-read-multiple-setup-hook))
    (apply func args)))

;;; my-functions-minibuffer.el ends here

(provide 'my-functions-minibuffer)

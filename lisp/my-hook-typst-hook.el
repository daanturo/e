;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(set-popup-rule! "\\`\\*Typst-" :ignore t)

;;; my-hook-typst-hook.el ends here

(provide 'my-hook-typst-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

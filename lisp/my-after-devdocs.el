;; -*- lexical-binding: t; -*-

(my-after-each '(emacs devdocs)
  (setq devdocs-data-dir (file-name-concat "~/.local/share" "devdocs")))

(provide 'my-after-devdocs)

;; Local Variables:
;; no-byte-compile: t
;; End:

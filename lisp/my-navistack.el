;; -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code


(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-navistack-act (wrap-fn actions)
  "Perform the ACTIONS sequence in order while being able to go back from each one.

ACTIONS is a list of variadict functions, first one being the return
value of the previous action (the first one's argument is not used),
other args are not used for now.

At each step, the user can go back to the previous action by the way
provided by WRAP-FN, which takes two arguments: the function to go back
to previous step, WRAP-FN should make is invokable such binding it to a
key; and the body function to actually perform the current one of
ACTIONS, WRAP-FN must call the body function somewhere.

The return value of the last action is returned.

See `my-navistack-minibuffer-backspace-to-back-act' for example."
  (named-let
      recur
      ((action-idx 0)
       ;; "stack" of steps: each captures the previous one's return value
       (stack '())
       ;; the initial return value isn't important
       (retval nil))
    (cond
     ;; after finishing the final action
     ((= action-idx (length actions))
      retval)
     (t
      ;; (< action-idx (length stack)) : we have gone to the previous step
      (-let* ((go-back-value (make-symbol "my-navistack-act"))
              (new-stack-top
               (if (< action-idx (length stack))
                   ;; being back: execute the top of the stack using the
                   ;; appropriate captured return value
                   (cl-first stack)
                 ;; forward execution: make a closure that captures the return
                 ;; value of the previous action, and calls the next action with
                 ;; it
                 (lambda () (apply (elt actions action-idx) retval '()))))
              (stack-rest
               (if (< action-idx (length stack))
                   ;; being back: remove the top of the stack
                   (cl-rest stack)
                 ;; forward execution: keep the stack as is
                 stack))
              (new-retval
               (catch 'my-navistack--go-back
                 (funcall
                  wrap-fn
                  (lambda ()
                    (if (= 0 action-idx)
                        (message
                         "`my-navistack': cannot go back beyond the first step")
                      (throw 'my-navistack--go-back go-back-value)))
                  new-stack-top))))
        (if (equal new-retval go-back-value)
            (recur (- action-idx 1) stack-rest retval)
          (recur
           (+ action-idx 1) (cons new-stack-top stack-rest) new-retval)))))))

;;;###autoload
(defun my-navistack-minibuffer-backspace-to-back-act (actions)
  "Apply `my-navistack-act' on ACTIONS.

The user can go back on each action by pressing \"<backspace>\"
at start of the mini-buffer's input field.

See `my-navistack-act' for more details."
  (my-navistack-act
   (lambda (back-fn body-fn)
     (minibuffer-with-setup-hook (lambda ()
                                   (keymap-local-set
                                    "<backspace>"
                                    `(menu-item
                                      ""
                                      ,(lambda ()
                                         (interactive)
                                         (funcall back-fn))
                                      :filter
                                      ,(lambda (cmd)
                                         (and (= (point)
                                                 (minibuffer-prompt-end))
                                              cmd)))))
       (funcall body-fn)))
   actions))


;;; my-navistack.el ends here

(provide 'my-navistack)

;; -*- lexical-binding: t; -*-

;; (my-backtrace)

;; TODO: until https://github.com/emacs-jupyter/jupyter/issues/464 is solved
(setq jupyter-use-zmq nil)

;; TODO: until https://github.com/emacs-jupyter/jupyter/issues/500 is solved
(defun my-jupyter-api-http-request--ignore-login-error-a
    (func url endpoint method &rest data)
  (cond
   ((member endpoint '("login"))
    (ignore-error (jupyter-api-http-error)
      (apply func url endpoint method data)))
   (:else
    (apply func url endpoint method data))))
(advice-add
 #'jupyter-api-http-request
 :around #'my-jupyter-api-http-request--ignore-login-error-a)


;; The default prompt is too hard to read, set those colors from the web interface

(set-face-foreground 'jupyter-repl-input-prompt "#307EC0")

(face-spec-set
 'jupyter-repl-output-prompt
 `((((class color) (background light)) :foreground "#BE5A3D")
   (((class color) (background dark)) :foreground "#BE5A3D")))

(face-spec-set
 'jupyter-repl-traceback
 `((((class color) (background light)) :background "#FFDCDC")
   (((class color) (background dark)) :background "#4F1E1A")))

;; don't waste screen space
(setq jupyter-repl-prompt-margin-width 0)

(my-bind :map 'jupyter-repl-mode-map "C-c C-c" #'jupyter-repl-interrupt-kernel)
(my-bind :map 'jupyter-repl-interaction-mode-map "C-c C-c" nil) ; `jupyter-eval-line-or-region'

(provide 'my-after-jupyter)

;; Local Variables:
;; no-byte-compile: t
;; End:

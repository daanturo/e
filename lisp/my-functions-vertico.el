;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-vertico-inc-count||height (&optional n)
  (interactive "p")
  (dlet ((vertico-resize t))
    (setq-local vertico-count (min (+ vertico-count n) (frame-height)))
    (vertico--exhibit)))

;;;###autoload
(defun my-vertico-dec-count||height (&optional n)
  (interactive "p")
  (dlet ((vertico-resize t))
    (setq-local vertico-count (max (- vertico-count n) 0))
    (vertico--exhibit)))

;;;###autoload
(defun my-vertico-sort-files (file-list)
  (save-match-data
    (-let* ((dir
             (file-name-as-directory (abbreviate-file-name default-directory)))
            (curr-file
             (or (-some-->
                     (minibuffer-selected-window)
                   (window-buffer it)
                   (buffer-file-name it)
                   (file-name-nondirectory it))
                 (-some--> buffer-file-name (file-name-nondirectory it))))
            ;; (mod-time-table (make-hash-table :test #'equal))
            (mod-time-fn
             (lambda (file)
               ;; (with-memoization (gethash file mod-time-table))
               (-->
                (file-attributes file) (file-attribute-modification-time it))))
            (sort-pred
             (lambda (time1 time2)
               (or (time-equal-p time1 time2) (not (time-less-p time1 time2)))))
            (sort-fn
             (lambda (file-lst-1)
               (cl-stable-sort file-lst-1 sort-pred :key mod-time-fn)))
            (recent-relative-files
             (-keep
              (lambda (file)
                (and
                 file ; `nil' in `file-name-history'?
                 (equal
                  dir
                  (-some-->
                      (file-name-directory file) (abbreviate-file-name it)))
                 (-some-->
                     (file-name-nondirectory file)
                   (and (not (string-empty-p it)) it))))
              file-name-history))
            (grouped-alist
             (-group-by
              (lambda (file)
                (cond
                 ((member file recent-relative-files)
                  :promote)
                 ;; dot files
                 ((string-match "\\`\\." file)
                  :dot)
                 ((or
                   ;; TRAMP protocols from root
                   (string-match "\\`[^/]*:\\'" file)
                   (equal curr-file file))
                  :demote)))
              file-list)))
      (append
       (alist-get :promote grouped-alist)
       (--> (alist-get nil grouped-alist) (funcall sort-fn it))
       (--> (alist-get :dot grouped-alist) (funcall sort-fn it))
       (alist-get :demote grouped-alist)))))


(provide 'my-functions-vertico)

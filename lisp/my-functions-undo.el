;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-fallback-undo-state ()
  (vector
   (buffer-substring (point-min) (point))
   (buffer-substring (point) (point-max))
   (point)))

;;;###autoload
(defun my-fallback-undo-restore (state-prev &optional quiet)
  "STATE-PREV: `my-fallback-undo-restore''s previous value.
QUIET: don't message."
  (-let* (([pre0 suf0 pos0] state-prev)
          (state-curr (my-fallback-undo-state))
          ([pre1 suf1 pos1] state-curr))
    (when (not (equal state-prev state-curr))
      (dlet ((inhibit-message t))
        (when (not quiet)
          (message "`my-fallback-undo-restore' current: %S..."
                   (--> (thing-at-point 'line t) (string-trim it)))))
      (-let* ((shared-pre (s-shared-start pre0 pre1))
              (shared-suf (s-shared-start suf0 suf1))
              (shared-pre-len (length shared-pre))
              (shared-suf-len (length shared-suf))
              (inserting-pre (string-remove-prefix shared-pre pre0))
              (inserting-suf (string-remove-suffix shared-suf suf0))
              (del-beg (+ (point-min) shared-pre-len))
              (del-end (+ (point) (- (length suf1) shared-suf-len))))
        (dlet ((inhibit-read-only t))
          ;; delete the different region
          (delete-region del-beg del-end)
          (insert inserting-pre)
          (save-excursion (insert inserting-suf))
          (vector inserting-pre inserting-suf))))))

;;; my-functions-undo.el ends here

(provide 'my-functions-undo)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defun my-deft-setup-preview-h (&rest _)
  (my-previewow-setup
   #'my-deft-preview-open-file-below #'deft-filename-at-point '("TAB" "<tab>")))

(add-hook 'deft-mode-hook #'my-deft-setup-preview-h)

(my-bind :map 'deft-mode-map "<f5>" #'my-org-roam-transient)

(add-hook 'my-evil-or-leader-mode-alist '(deft-mode nil))

;;; my-after-deft.el ends here

(provide 'my-after-deft)

;; Local Variables:
;; no-byte-compile: t
;; End:

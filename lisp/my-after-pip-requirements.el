;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; WTH network request when opening a file?
(advice-add #'pip-requirements-fetch-packages :override #'ignore)

;;; my-after-pip-requirements.el ends here

(provide 'my-after-pip-requirements)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)

;; OOTB, `wdired-mode-hook' only runs when dired->wdired

(my-advice-function-to-run-hook
 #'wdired-change-to-dired-mode 'my-wdired-change-to-dired-mode-hook)


(my-turned-off-mode-between
 'wdired-mode-hook 'my-wdired-change-to-dired-mode-hook 'my-dired-key-mode)

(my-add-hook/s
  '(wdired-mode-hook my-wdired-change-to-dired-mode-hook)
  #'(my-leader-or-vi-refresh-prefer-insert))

(provide 'my-after-wdired)

;; Local Variables:
;; no-byte-compile: t
;; End:

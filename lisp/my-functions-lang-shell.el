;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(progn
  ;; maybe use `major-mode-remap-alist' instead
  (defun my-bash-ts-or-sh-mode ()
    (if (and (my-treesit-available-p) (treesit-language-available-p 'bash))
        (progn
          ;; applicable: the expected ideal case, just re-define the function
          ;; itself as an alias for performance
          (defalias #'my-bash-ts-or-sh-mode #'bash-ts-mode)
          (bash-ts-mode))
      (progn
        (sh-mode)))))


;;; my-functions-lang-shell.el ends here

(provide 'my-functions-lang-shell)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(define-minor-mode my-lsp-BRIDGE-global-mode
  nil
  :global t
  (if my-lsp-BRIDGE-global-mode
      (progn

        (my-safe-call #'my-lsp-maybe-global-mode 0)
        (my-corfu-global-mode-safe 0)

        (global-lsp-bridge-mode)
        (advice-remove #'lsp-bridge-mode #'ignore))
    (progn

      (my-safe-call #'my-lsp-maybe-global-mode 1)
      (my-corfu-global-mode-safe 1)

      (advice-add #'lsp-bridge-mode :override #'ignore))))

;;; after-lsp-bridge.el ends here

(provide 'my-after-lsp-bridge)

;; Local Variables:
;; no-byte-compile: t
;; End:

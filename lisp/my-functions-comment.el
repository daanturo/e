;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-skip-comment-start-forward ()
  (interactive)
  (when comment-start
    (save-match-data
      (-let* ((lep (line-end-position))
              (regexp (format "\\(%s\\)+[ \t]*" comment-start)))
        (re-search-forward regexp lep t)
        nil))))


;;; my-functions-comment.el ends here

(provide 'my-functions-comment)

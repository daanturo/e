;; -*- lexical-binding: t; -*-

(require 'dash)

(defvar-local my-no-mode-line--state nil)

;;;###autoload
(define-minor-mode my-no-mode-line-mode
  nil
  :global nil
  (if my-no-mode-line-mode
      (progn
        (setq-local my-no-mode-line--state
                    (buffer-local-set-state mode-line-format nil)))
    (progn
      (buffer-local-restore-state my-no-mode-line--state)))
  (when (called-interactively-p 'any)
    (redraw-display)))

;;;###autoload
(defun my-windowdisplay-no-mode-line (window)
  "Hide WINDOW's mode-line.
See Info node `(elisp) Buffer Display Action Alists'."
  (with-selected-window window
    (my-no-mode-line-mode)))

(provide 'my-no-mode-line)

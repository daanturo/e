;; -*- lexical-binding: t; -*-

;; (my-backtrace)

;; NOTE: the major mode name is `inferior-emacs-lisp-mode', while its hook
;; `inferior-emacs-lisp-mode-hook' is aliased to `ielm-mode-hook' when loading
;; ielm.el, therefore any modifications to the hook with long name will be lost
;; after that. So maybe it's better to enforce aliasing earlier like this?
;;;###autoload
(defvaralias 'inferior-emacs-lisp-mode-hook 'ielm-mode-hook)

(defun my-ielm-mode-hook ()
  (when (boundp 'xref-backend-functions)
    (add-to-list 'xref-backend-functions
                 'elisp--xref-backend)))

(my-add-mode-hook-and-now 'inferior-emacs-lisp-mode 'my-ielm-mode-hook)

(setq ielm-history-file-name
      (file-name-concat my-user-emacs-local-dir/ "ielm-history.eld"))

(provide 'my-after-ielm)

;; Local Variables:
;; no-byte-compile: t
;; End:

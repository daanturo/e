;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (when (executable-find "bat")
;;   (setq fzf/args-for-preview "--preview 'bat {}'"))

(setq fzf/args-for-preview "--preview 'test -d {} && ls --color=always -Ahl {} || lesspipe.sh {} || less {} || bat -pp -n --color=always {} || cat {}'")

;;; my-after-fzf.el ends here

(provide 'my-after-fzf)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar jinx-languages nil)
(defvar-local jinx-mode nil)

;;;###autoload
(defun my-guess-language ()
  (dlet ((inhibit-message t))
    (setq-local guess-language-current-language
                (or (my-with-demoted-errors
                     (guess-language-paragraph))
                    (my-with-demoted-errors (guess-language-buffer))))
    (-some--> guess-language-current-language (format "%s" it))))

;; -- quick keys

;;;###autoload
(defun my-jinx-force-correct-at-point ()
  "https://github.com/minad/jinx/issues/71."
  (interactive)
  (require 'jinx)
  (-when-let* (((beg . end) (bounds-of-thing-at-point 'word))
               (ov (make-overlay beg end)))
    (unwind-protect
        (jinx--correct-overlay ov nil)
      (ignore-errors
        (delete-overlay ov)))
    t))

;;;###autoload
(defun my-jinx-correct (&optional all)
  (interactive "P")
  (minibuffer-with-setup-hook (lambda ())
    (-let* ((word-bounds (bounds-of-thing-at-point 'word))
            ((wb . we) word-bounds))
      (cond
       ((and (not all) word-bounds)
        (my-with-mode/s
          '(jinx-mode) t
          ;; (my-with-advice #'jinx--bounds-of-word :around
          ;; (lambda (func1 &rest args1)
          ;; (condition-case err (apply func1 args1) (error word-bounds))))
          (jinx-correct-word)))
       (t
        (jinx-correct))))))

;;;###autoload
(defun my-spell-correct (&optional arg)
  (interactive "P")
  (my-guess-language-switch-jinx-function (my-guess-language))
  (-->
   (cond
    ((fboundp #'jinx-correct)
     (condition-case err
         (progn
           (my-jinx-correct arg)
           t)
       (error nil)))
    (t
     (call-interactively #'ispell-word)))))

;;;###autoload
(defun my-guess-language-switch-jinx-function
    (lang &optional _beg _end)
  (-when-let* ((_ lang)
               (old-langs (copy-sequence (or jinx-languages ""))))
    (setq-local jinx-languages
                (-->
                 (split-string old-langs)
                 `(,(format "%s" (or lang "")) ,@it)
                 (-uniq it)
                 (string-join it " ")))
    (when (and jinx-mode (not (equal old-langs jinx-languages)))
      (jinx-mode))
    jinx-languages))

;;; my-functions-spell.el ends here

(provide 'my-functions-spell)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'ein)
(require 'ein-log) ; `ein:log' is a macro

;; Load many useful un-autoloaded functions as starting point
(require 'ein-ipynb-mode)


;;;###autoload
(defun my-ein-run|execute-selected-and-after-cells (ws cell)
  (interactive (list (ein:worksheet--get-ws-or-error)
                     (ein:worksheet-get-current-cell)))
  (let ((prev-cell (ein:cell-prev cell)))
    (if prev-cell
        (ein:worksheet-execute-all-cells ws :below prev-cell)
      (ein:worksheet-execute-all-cells ws))))

;;;###autoload
(defun my-ein-notebook-run-or-login ()
  (interactive)
  (require 'ein-jupyter)
  (if (ein:jupyter-server-process)
      (call-interactively #'ein:notebooklist-login)
    (call-interactively #'ein:run)))

(defun my-ein-same-notebook-buffer-p (buf0 buf1)
  (equal ;; (with-current-buffer buf0 (save-excursion (goto-char (point-min)) (current-buffer)))
   ;; (with-current-buffer buf1 (save-excursion (goto-char (point-min)) (current-buffer)))
   (buffer-local-value 'ein:%notebook% buf0)
   (buffer-local-value 'ein:%notebook% buf1)))

;;;###autoload
(defun my-ein-notebook-full-path (&optional notebook)
  (file-name-concat (ein:jupyter-running-notebook-directory)
                    (ein:$notebook-notebook-path (or notebook ein:%notebook%))))

;;;###autoload
(defun my-ein-notebook-core-info (notebook)
  (and notebook
       (list
        (ein:$notebook-url-or-port notebook)
        (ein:$notebook-notebook-path notebook))))

(defun my-ein--may-read-ipynb-file ()
  (if (string-suffix-p ".ipynb" buffer-file-name)
      buffer-file-name
    (read-file-name ".ipynb file: ")))

;;;###autoload
(defun my-ein-auto-open-notebook (file &optional interactive-flag callback)
  "Open Jupyter notebook from FILE.
Without displaying the \"notebook list\" buffer.
INTERACTIVE-FLAG. Non-nil CALLBACK is a function to call which
accepts those arguments: the notebook buffer."
  (interactive (list (my-ein--may-read-ipynb-file) t))
  ;; `ein:jupyter-server-start' doesn't support multiple running servers yet
  (-if-let* ((running-dir (ein:jupyter-running-notebook-directory))
             (_ interactive-flag)
             (_ (not (f-descendant-of? file running-dir)))
             (stopping (ein:jupyter-server-stop 'ask)))
      ;; `ein:jupyter-server-stop' is currently async-only
      (let* ((proc (ein:jupyter-server-process))
             (callback* (process-sentinel proc)))
        (set-process-sentinel
         proc
         (lambda (&rest sentinel-args)
           (prog1 (apply callback* sentinel-args)
             (my-ein-open-notebook-may-start-server file nil callback)))))
    ;; running-dir may match but sometimes not recognized, don't start another
    ;; server in this case
    (my-ein-open-notebook-may-start-server file running-dir callback)))

;;;###autoload
(defun my-ein-open-notebook-may-start-server (file &optional dont-start-server callback)
  (interactive (list (my-ein--may-read-ipynb-file)))
  (-let* ((dir (ein:process-suitable-notebook-dir file))
          (path (file-relative-name file dir))
          (orig-win (selected-window))
          (orig-buf (current-buffer))
          (callback*
           (lambda (url|port)
             (my-ein--open-notebook url|port path
                                    nil
                                    nil
                                    orig-win
                                    orig-buf
                                    callback)))
          (proc (ein:process-dir-match file)))
    (if (or proc dont-start-server)
        (funcall callback* (ein:jupyter-my-url-or-port))
      (ein:jupyter-server-start
       (executable-find ein:jupyter-server-command) dir nil
       (lambda (nblist-buffer url-or-port-started)
         (funcall callback* url-or-port-started))))))

(defun my-ein-notebook--display-buffer-workaround (url|port path display-fn)
  "Advise a function to call DISPLAY-FN on the notebook buffer.
Use when the buffer is already created after opening the
notebook, but the callback takes to long to happen. Condition:
when the notebook matches URL|PORT and PATH. Return a function
that removes the advice when called."
  (letrec ((adv
            (lambda (func notebook &rest args)
              (-let* ((returned-notebook (apply func notebook args))
                      (url1 (ein:$notebook-url-or-port returned-notebook))
                      (path1 (ein:$notebook-notebook-path returned-notebook)))
                (when (and (equal url1 url|port) (equal path1 path))
                  (advice-remove #'ein:notebook-from-json adv)
                  (funcall display-fn (ein:notebook-buffer returned-notebook))
                  (my-println🌈
                   "EIN: displaying"
                   (concat url1 "/" path1)
                   "before callback!"))
                returned-notebook))))
    (advice-add #'ein:notebook-from-json :around adv)
    (lambda () (advice-remove #'ein:notebook-from-json adv))))

(defvar my-ein-auto-open-notebook-hook '()
  "Functions called after opening the notebook.
The value should be a list of functions that take those arguments: the notebook object.")

(defun my-ein--open-notebook
    (url|port
     path &optional pre-open-fn keep-position orig-win orig-buf callback)
  (-let* (
          ;; reduce flashes until the notebook is re-opened
          (orig-win (or orig-win (selected-window)))
          (orig-buf (or orig-buf (current-buffer)))
          (placeholder-buffer
           (and keep-position (my-make-clone-buffer-by-contents orig-buf)))
          (wc (and keep-position (current-window-configuration)))
          (wh (and keep-position (my-window-line-height-from-top)))
          (pos (point))
          (keep-pos-fn
           (and keep-position
                (progn
                  (switch-to-buffer placeholder-buffer)
                  (lambda ()
                    (kill-buffer placeholder-buffer)
                    (unless ein:%notebook%
                      (set-window-configuration wc))
                    (recenter-top-bottom wh)
                    (goto-char pos)))))
          (displayed nil)
          (pop-fn
           (lambda (notebook-buf)
             (if displayed
                 (my-println🌈
                  "EIN:"
                  (my-buffer-name-string notebook-buf)
                  "was already displayed.")
               (-let* ((cur-buf-in-orig-win (window-buffer orig-win)))
                 (cond
                  ((and (equal notebook-buf (current-buffer))
                        (get-buffer-window notebook-buf)))
                  ((and (window-live-p orig-win)
                        (or (equal cur-buf-in-orig-win orig-buf)
                            (equal cur-buf-in-orig-win placeholder-buffer)))
                   (with-selected-window orig-win
                     (my-switch-to-buffer-prefer-existing-window notebook-buf)))
                  (t
                   (pop-to-buffer notebook-buf)))
                 (setq displayed t)
                 (when keep-position
                   (funcall keep-pos-fn))
                 (my-safe-call callback notebook-buf)))))
          (pop-workaround-cookie
           (my-ein-notebook--display-buffer-workaround url|port path pop-fn))
          (callback*
           (lambda (&optional notebook _created)
             (funcall pop-workaround-cookie)
             (funcall pop-fn (ein:notebook-buffer notebook))
             (run-hook-with-args 'my-ein-auto-open-notebook-hook notebook))))
    (unless (and pre-open-fn keep-position)
      (my-println🌈 "EIN: opening" (concat url|port "/" path)))
    (my-safe-call pre-open-fn)
    (ein:notebook-open url|port path nil callback* nil 'no-pop)))

;;;###autoload
(defun my-ein-revert|refresh-notebook (&optional _ignore-auto _noconfirm)
  "Re-open current notebook to get the latest evaluation results.
Pass `no-pop' to `ein:notebook-open' and handle displaying in the
callback."
  (interactive)
  (let* ((notebook ein:%notebook%))
    (my-ein--open-notebook
     (ein:$notebook-url-or-port notebook) (ein:$notebook-notebook-path notebook)
     (lambda () (ein:notebook-close notebook)) 'keep-position)))

;;;###autoload
(defun my-ein-edit-cell ()
  (interactive)
  (-let* (((beg . end) (my-ein-bounds-of-cell))
          (mode major-mode))
    (dlet ((edit-indirect-guess-mode-function
            (lambda (_parent-buffer _beg _end)
              (funcall mode))))
      (edit-indirect-region beg end 'display-buffer))))

;;;###autoload
(defun my-ein-bounds-of-cell (&optional cell)
  (let* ((cell (or cell (ein:worksheet-get-current-cell))))
    (cons (ein:cell-input-pos-min cell)
          (ein:cell-input-pos-max cell))))

;;;###autoload
(defun my-ein-bounds-of-outer-cell (&optional cell)
  (-let* (((beg . end) (my-ein-bounds-of-cell cell)))
    (cons (save-excursion
            (goto-char beg)
            (line-beginning-position 0))
          (+ 1 end))))

;;;###autoload
(defun my-ein-mark-cell (&optional outer)
  (interactive "P")
  (-let* (((beg . end) (if outer
                           (my-ein-bounds-of-outer-cell)
                         (my-ein-bounds-of-cell))))
    (my-set-mark-from-to end beg)))

;;;###autoload
(defun my-ein-beg-of-cell (arg)
  (interactive "p")
  (dotimes (_ arg)
    (let* ((current-cell (ein:worksheet-get-current-cell))
           (pos (car (my-ein-bounds-of-cell current-cell))))
      (cond ((/= pos (point))
             (goto-char pos))
            (t
             (goto-char (car (my-ein-bounds-of-cell (ein:cell-prev current-cell)))))))))

;;;###autoload
(defun my-ein-end-of-cell (arg)
  (interactive "p")
  (dotimes (_ arg)
    (let* ((current-cell (ein:worksheet-get-current-cell))
           (pos (cdr (my-ein-bounds-of-cell current-cell))))
      (cond ((/= pos (point))
             (goto-char pos))
            (t
             (goto-char (cdr (my-ein-bounds-of-cell (ein:cell-next current-cell)))))))))

;;;###autoload
(defun my-ein-format-cell ()
  (interactive)
  (-let* (((beg . end) (if (use-region-p)
                           (cons (region-beginning) (region-end))
                         (my-ein-bounds-of-cell))))
    (my-code-format-try-region end beg)))

;;;###autoload
(defun my-ein-goto-current-cell-beg ()
  (goto-char (car (my-ein-bounds-of-cell))))

;;;###autoload
(defun my-ein-goto-current-cell-end ()
  (interactive)
  (goto-char (cdr (my-ein-bounds-of-cell))))

;;;###autoload
(defun my-ein-goto-current-cell-header ()
  (interactive)
  (goto-char (car (my-ein-bounds-of-outer-cell))))

;;;###autoload
(defun my-ein-move-cell-up (&optional arg)
  (interactive "p")
  (dotimes (_ arg)
    (call-interactively #'ein:worksheet-move-cell-up)
    (my-ein-goto-current-cell-header)))

;;;###autoload
(defun my-ein-move-cell-down (&optional arg)
  (interactive "p")
  (dotimes (_ arg)
    (call-interactively #'ein:worksheet-move-cell-down)
    (my-ein-goto-current-cell-header)))

(defvar my-ein-backup-notebook-suffix-sans-dot-fn #'my-ISO-time)
(defun my-ein-compute-backup-notebook-path (orig-path)
  (let ((ext (file-name-extension orig-path))
        (base (file-name-sans-extension orig-path)))
    (format "%s.%s.%s" base
            (funcall my-ein-backup-notebook-suffix-sans-dot-fn)
            ext)))

;;;###autoload
(defun my-ein-make-backup-notebook-object (orig-notebook)
  (let ((nnb (copy-ein:$notebook orig-notebook)))
    (cl-callf my-ein-compute-backup-notebook-path
        (ein:$notebook-notebook-path nnb))
    nnb))

;;;###autoload
(defun my-ein-save-backup-notebook (&optional notebook callback)
  (interactive)
  (let ((backup-notebook (my-ein-make-backup-notebook-object (or notebook ein:%notebook%))))
    (ein:notebook-save-notebook backup-notebook (or callback #'ignore))
    (ein:$notebook-notebook-path backup-notebook)))

;;;###autoload
(defun my-ein-backup-unsaved-notebooks (&optional dont-close)
  "Save modified notebooks as backups.
Use `my-ein-backup-notebook-suffix-sans-dot-fn''s returned value
as suffix. Unless DONT-CLOSE (inverted prefix arg), also close
those notebooks."
  (interactive (list (not current-prefix-arg)))
  (-let* ((notebooks (ein:notebook-opened-notebooks #'ein:notebook-modified-p)))
    (mapc #'my-ein-save-backup-notebook notebooks)
    ;; Ignore `ein:notebook-ask-save''s prompts
    (my-with-advice #'y-or-n-p :override #'ignore
      (unless dont-close
        (mapc #'ein:notebook-close notebooks)))
    (length notebooks)))

(defun my-ein:notebook-ask-save--auto-backup-a (func &rest args)
  (my-with-advice #'y-or-n-p :override #'always
    (my-with-advice #'ein:notebook-save-notebook :around
      (lambda (save-fn notebook &rest save-fn-args)
        (apply save-fn
               (my-ein-make-backup-notebook-object notebook)
               save-fn-args))
      (apply func args))))
;;;###autoload
(define-minor-mode my-ein-auto-backup-notebooks-mode nil
  :global t
  (if my-ein-auto-backup-notebooks-mode
      (advice-add #'ein:notebook-ask-save :around #'my-ein:notebook-ask-save--auto-backup-a)
    (advice-remove #'ein:notebook-ask-save #'my-ein:notebook-ask-save--auto-backup-a)))

;;;###autoload
(defun my-ein-run|execute-region-or-line (&optional beg end)
  (interactive `(,@(and (use-region-p)
                        (list (region-beginning) (region-end)))))
  (-let* ((beg (or beg (line-beginning-position)))
          (end (or end (line-end-position)))
          (cell (ein:get-cell-at-point))
          (kernel (slot-value cell 'kernel)))
    (ein:cell-execute-internal cell
                               kernel
                               (buffer-substring-no-properties beg end)
                               :silent nil)))

;;;###autoload
(defun my-ein-current-home-list-notebooks ()
  (interactive)
  ;; (ein:get-url-or-port)
  (let ((url|port (ein:$notebook-url-or-port ein:%notebook%)))
    (-if-let* ((buf (ein:notebooklist-get-buffer url|port)))
        (my-switch-to-buffer-prefer-existing-window buf)
      (ein:notebooklist-login
       (lambda (buf _url-or-port)
         (my-switch-to-buffer-prefer-existing-window buf))))))

;;;###autoload
(defun my-ein-goto-browser ()
  (interactive)
  (browse-url
   (let ((url|port (or (ein:get-url-or-port) (ein:jupyter-my-url-or-port))))
     (ein:url
      url|port
      ;; (and ein:%notebook%
      ;;      (file-name-concat ein:jupyter-server-use-subcommand "tree"
      ;;              (ein:$notebook-notebook-path ein:%notebook%)))
      (format "?token=%s" (ein:notebooklist-token-or-password url|port))))))

;;;###autoload
(defun my-ein-copy-token-or-password (&optional url|port)
  (interactive)
  (--> (or url|port (ein:get-url-or-port))
       (ein:notebooklist-token-or-password it)
       (kill-new it)))

;;;###autoload
(defun my-ein-notebook-context-menu-beg (menu click)
  (when (image-at-point-p)
    (setq menu (my-image-context-menu menu click)))
  (define-key-after menu [my-ein-notebook-context-menu-beg] menu-bar-separator)
  (define-key-after menu [ein:worksheet-insert-cell-below]
    `(menu-item "Insert cell below" ein:worksheet-insert-cell-below))
  (define-key-after menu [ein:worksheet-insert-cell-above]
    `(menu-item "Insert cell above" ein:worksheet-insert-cell-above))
  menu)

;;;###autoload
(defun my-ein-notebook-context-menu-end (menu click)
  (define-key-after menu [my-ein-notebook-context-menu-end] menu-bar-separator)
  (define-key-after menu [my-ein-current-home-list-notebooks]
    `(menu-item "List Notebooks" my-ein-current-home-list-notebooks))
  (define-key-after menu [my-ein-goto-browser]
    `(menu-item "Go to Browser" my-ein-goto-browser))
  menu)

;;;###autoload
(defun my-ein-suitable-notebook-dir (filename)
  "Project root or the bottom most directory with an \".ipynb\" file."
  (let ((parent-dir (file-name-parent-directory filename)))
    (or (my-project-root parent-dir)
        (locate-dominating-file
         parent-dir
         (lambda (dir)
           (directory-files dir nil "\\.ipynb\\'"))))))

;;;###autoload
(defun my-ein-merge-cells ()
  "Merge 2 cells around point."
  (interactive)
  (-let* ((cell (ein:worksheet-get-current-cell))
          (ws (ein:worksheet--get-ws-or-error))
          ((cur-beg . cur-end) (my-ein-bounds-of-cell cell)))
    (cond
     ((<= (point) cur-beg)
      (-let* ((prev-end (ein:cell-input-pos-max (ein:cell-prev cell))))
        (ein:worksheet-merge-cell ws cell nil 'focus)
        (goto-char prev-end)))
     ((<= cur-end (point))
      (ein:worksheet-merge-cell ws cell 'next 'focus)
      (goto-char cur-end))
     (t
      (my-message "Put cursor between cells to merge.")))))

;;;###autoload
(defun my-ein:worksheet--jigger-undo-list--a (func &rest args)
  (-let* ((val0 ein:worksheet-enable-undo))
    (unwind-protect
        ;; don't display this error
        (my-with-advice
          #'ein:display-warning
          :around
          (lambda (func-in msg &optional level &rest args-in)
            (cond
             ((and (string-prefix-p "Undo failure diagnostic " msg)
                   (member level '(:error)))
              (message "`%s': %S" 'ein:worksheet--jigger-undo-list msg))
             (:else
              (apply func-in msg level args-in))))
          (apply func args))
      (setq ein:worksheet-enable-undo val0))))

(defvar my-ein-try-keep-newline-at-end-of-cell t)

;;;###autoload
(defun my-ein-worksheet-insert-cell-below (&optional text-cell above)
  "Insert a cell below, pre-insert it with a new line.
To make edit operations easier. TEXT-CELL: insert a non-code
cell, ABOVE: insert above instead."
  (interactive "P")
  (-let* ((cell
           (funcall (if above
                        #'ein:worksheet-insert-cell-above
                      #'ein:worksheet-insert-cell-below)
                    (ein:worksheet--get-ws-or-error)
                    (if text-cell
                        'markdown
                      'code)
                    (ein:worksheet-get-current-cell :noerror t) t))
          (beg (ein:cell-input-pos-min cell))
          (end (ein:cell-input-pos-max cell)))
    (when my-ein-try-keep-newline-at-end-of-cell
      (save-excursion
        (when (= beg end)
          (goto-char beg)
          (insert "\n"))))
    ;; not focused?
    (when (/= beg (point))
      (goto-char beg))
    cell))

;;;###autoload
(defun my-ein-worksheet-insert-cell-above (&optional text-cell)
  "(`my-ein-worksheet-insert-cell-below' TEXT-CELL 'above)."
  (interactive "P")
  (my-ein-worksheet-insert-cell-below text-cell 'above))

;;;###autoload
(defun my-ein-new-line-and-indent-and-maybe-open (arg)
  (interactive "*p")
  (newline-and-indent arg)
  (when (and my-ein-try-keep-newline-at-end-of-cell
             (= (ein:cell-input-pos-max (ein:worksheet-get-current-cell))
                (point)))
    (save-excursion (insert "\n"))))

;;;###autoload
(defun my-ein-switch-kernel-to-python3 ()
  (interactive)
  (-let* ((notebook (ein:get-notebook))
          (kernel-name "python3"))
    ;; (ein:notebook-switch-kernel notebook kernel-name)
    (let* ((kernelspec
            (ein:get-kernelspec
             (ein:$notebook-url-or-port notebook) kernel-name)))
      (setf (ein:$notebook-kernelspec notebook) kernelspec))))


;;; my-functions-ein.el ends here

(provide 'my-functions-ein)

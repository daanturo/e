;; -*- lexical-binding: t; -*-

(require '00-my-core-macros)


;;;###autoload
(defconst my-repo-git-hosting-service-protocol-prefixes
  '(
    "https://" "git@"
    "git://" "http://" "ssh://"
    ))

(defconst my-repo-git-remote-hosting-service-url-regexp
  (rx-to-string `(seq bos
                  ;; protocol
                  (group (or ,@my-repo-git-hosting-service-protocol-prefixes))
                  ;; domain
                  (group (+ (not (or ":" "/"))))
                  ;; separator
                  (group (or ":" "/"))
                  ;; user
                  (?  (group (+ (not "/")))
                      ;; separator
                      (group "/"))
                  ;; project-name
                  (group (+? nonl))
                  ;; suffix
                  (group (?  ".git") (?  "/"))
                  eos
                  ;;
                  )
                'no-group)
  "(whole-match protocol host sepa user sepa project suffix)")

(defconst my-repo-git-remote-hosting-service-url-regexp-user-num 4)

(defvar my-repo-git-default-remote "origin")

;;;###autoload
(defun my-repo-get-git-remote-url (&optional remote)
  (my-with-ensured-current-directory
   (-let*
       ((remote (or remote my-repo-git-default-remote))
        ((&plist :out :exit)
         (my-process-sync-run (list "git" "remote" "get-url" remote)))
        ;; (remote-list (split-string (shell-command-to-string "git remote") "\n" t))
        )
     (cond
      ;; common errors: not a git repository, not such remote,... treat those as
      ;; owned
      ((/= exit 0)
       nil)
      (t
       (string-remove-suffix "\n" out))))))

;;;###autoload
(defun my-repo-get-git-owner-of-url (url)
  (when (string-match my-repo-git-remote-hosting-service-url-regexp
                      url)
    (match-string my-repo-git-remote-hosting-service-url-regexp-user-num url)))

;;;###autoload
(defun my-repo-parse-git-remote-hosting-service-url (url)
  (-let* ((regexp my-repo-git-remote-hosting-service-url-regexp))
    (s-match regexp url)))

;;;###autoload
(defun my-repo-get-git-owner-of-remote (&optional remote)
  (-let* ((origin-url (my-repo-get-git-remote-url remote)))
    (cond ((null origin-url) nil)
          ((my-repo-get-git-owner-of-url origin-url))
          ;; unable to parse
          (t t))))

;;;###autoload
(defun my-repo-git-get-user (&optional options)
  (string-trim-right
   (shell-command-to-string
    (format "git config %s user.name" (or options "")))))

;;;###autoload
(defun my-repo-git-own-p ()
  (-let* ((owner (my-repo-get-git-owner-of-remote my-repo-git-default-remote)))
    (or
     ;; treat non-git repos as owned
     (null owner)
     (and (stringp owner)
          (string-equal-ignore-case (my-repo-git-get-user) owner)))))

(defvar my-repo-git-not-own--table (make-hash-table :test #'equal))

(defun my-repo-git-not-own-reset-cache ()
  (interactive)
  (setq my-repo-git-not-own--table
        (make-hash-table :test #'equal)))

;;;###autoload
(defun my-repo-git-not-own-p ()
  (-let* ((dir (directory-file-name default-directory))
          (got (gethash dir my-repo-git-not-own--table 'not-found)))
    (if (equal got 'not-found)
        (puthash dir (not (my-repo-git-own-p))
                 my-repo-git-not-own--table)
      got)))

;; (my-backtrace)

(provide 'my-functions-repo)

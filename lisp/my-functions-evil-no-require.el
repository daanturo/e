;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-evil-no-state ()
  (when (bound-and-true-p evil-local-mode)
    (evil-local-mode 0))
  (setq-local evil-buffer-regexps '(("" . nil)))
  (when (fboundp #'evil-state-property)
    (dolist (state-mode (-map #'cdr (evil-state-property t :modes)))
      (my-set-local state-mode nil)))
  (setq-local evil-default-state nil))

;;;###autoload
(defun my-evil-motion-derived-state-p (&optional state)
  (when (and (fboundp #'evil-mode) (bound-and-true-p evil-state))
    (-let* ((state (or state evil-state)))
      (or (equal state 'motion)
          (-->
           evil-state-properties
           (alist-get state it)
           (plist-get it :enable)
           (member 'motion it))))))

;;;###autoload
(defun my-evil-save-state-a (func &rest args)
  (my-evil-save-state (apply func args)))

;;;###autoload
(defun my-evil-save-state-notify-a (func &rest args)
  (dlet ((my-evil-save-state-notify-flag t) (inhibit-message t))
    (my-evil-save-state (apply func args))))

;;;###autoload
(defun my-evil-force-insert-state-maybe-h (&rest _)
  (when (and (bound-and-true-p evil-local-mode)
             (not (member evil-state '(insert))))
    (evil-insert-state)))

;;;###autoload
(defvar my-evil-typing-state-list '(insert emacs hybrid)
  "May need to include `nil' (non-evil) in some cases.")

;;;###autoload
(defun my-pre-config-evil ()
  "Doom loads evil before config.el."

  ;; Try to achieve "hybrid" evil state.

  (setq evil-collection-key-blacklist '("n" "p"))
  (setq evil-disable-insert-state-bindings t)

  (setq evil-snipe-override-evil-repeat-keys nil) ; leave ","
  (setq evil-toggle-key "C-M-S-<escape>")
  (setq evil-want-C-g-bindings nil)
  (setq evil-want-C-u-delete nil)
  (setq evil-want-C-u-scroll nil)
  (setq evil-want-Y-yank-to-eol nil)
  (setq evil-want-fine-undo t)
  (setq evil-want-keybinding nil) ; handled by `evil-collection'

  (setq evil-visual-state-cursor 'hollow))

;;;###autoload
(defun my-evil-transient-hybrid--to-evil-mode ()
  (interactive)
  (my-evil-transient-hybrid-mode 0)
  (evil-mode))

;;;###autoload
(define-minor-mode my-evil-transient-hybrid-mode
  "Defer loading `evil', press ESC to do enable `evil-mode'."
  :global t
  :keymap
  `(
    (,(kbd "ESC ESC") . my-evil-transient-hybrid--to-evil-mode)
    (,(kbd "<escape>") .
     my-evil-transient-hybrid--to-evil-mode)))


;;;###autoload
(defun my-setup-evil ()

  (when (fboundp #'evil-mode)

    ;; (and (not (bound-and-true-p evil-mode))
    ;;      (not noninteractive))

    ;; (my-evil-or-leader-global-mode)

    (my-evil-transient-hybrid-mode)

    ;; ;; error
    ;; (my-add-advice-once
    ;;   #'my-leader-spc-prefix-cmd
    ;;   :before
    ;;   (lambda (&rest _) (my-evil-transient-hybrid--to-evil-mode)))

    ;; (add-hook
    ;;  'my-idle-run-once-hook
    ;;  (lambda (&rest _) (my-evil-transient-hybrid--to-evil-mode)))
    
    (when (daemonp)
      (make-thread #'my-evil-transient-hybrid--to-evil-mode))

    ;; (add-hook 'my-idle-load-list 'evil)

    ;; (my-bind '("S-ESC" "S-<escape>") #'my-toggle-evil-maybe-leader)
    (my-bind '("C-ESC" "C-<escape>") #'evil-execute-in-normal-state)))



;;; my-functions-evil-no-require.el ends here

(provide 'my-functions-evil-no-require)

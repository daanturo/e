;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-mime-type->extension (mime-type &optional dot)
  (-->
   (replace-regexp-in-string "^.*/" "" (format "%s" mime-type))
   (if dot
       (concat "." it)
     it)))

;;; my-functions-mime.el ends here

(provide 'my-functions-mime)

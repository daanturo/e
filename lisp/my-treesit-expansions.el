
;; -*- lexical-binding: t; -*-

(require 'expand-region-core)
(require 'subr-x)
(require 'treesit)

(defun my-er/mark--from-to (from to)
  (set-mark from)
  (goto-char to)
  (activate-mark))

;;;###autoload
(defun my-er/treesit-mark-bigger-node ()
  (interactive)
  (cond
   ((not (use-region-p))
    (let* ((node@pt (treesit-node-at (point))))
      (my-er/mark--from-to
       (treesit-node-end node@pt) (treesit-node-start node@pt))))
   (t
    (let* ((beg0 (region-beginning))
           (end0 (region-end))
           (cover-node (treesit-node-on beg0 end0))
           (super-node
            (treesit-parent-until
             cover-node
             (lambda (node)
               (or (< (treesit-node-start node) beg0)
                   (< end0 (treesit-node-end node))))
             'include-node))
           (super-beg (treesit-node-start super-node))
           (super-end (treesit-node-end super-node)))
      (my-er/mark--from-to super-end super-beg)))))

(defun my-er/treesit--balanced-region-p (beg end)
  "Check if region between BEG and END is balanced by `scan-sexps'.
Note that `forward-sexp' isn't used because some grammars ignore
brakets, thus the functions doesn't move a over a balanced
\"sexp\".

See:
- https://github.com/wkirschbaum/elixir-ts-mode/issues/21."
  (save-excursion
    (condition-case err
        (cl-block
            nil (goto-char beg)
            (while t
              ;; skip spaces, since the region may contain trailing spaces while
              ;; another sexp lies beyond those
              (skip-chars-forward " \t\n\r" end)
              (if (or (eobp) (equal (point) end))
                  (cl-return t)
                (let* ((next-pos (scan-sexps (point) 1)))
                  (cond
                   ;; can't find any more sexps: balanced
                   ((null next-pos)
                    (cl-return t))
                   ;; there is a sexp begins in the region and ends outside the
                   ;; region: region not balanced
                   ((< end next-pos)
                    (cl-return nil))
                   (t
                    (goto-char next-pos)))))))
      (scan-error
       ;; error because being at the end of list: balanced
       (string-search "ends prematurely" (error-message-string err))))))

;;;###autoload
(defun my-er/treesit-mark-bigger-list-or-node ()
  "Expand the marked region by navigating Tree Sitter's produced syntax tree.

When the region is not active: mark the smallest node at point.

When the region is active:

- Find the smallest node that covers on the region, when its
  range is bigger (and includes) the region, mark that range.

- When that range is exactly the region: try to mark the
  \"inner\" region between the bigger ancestor node (that is
  usually just the immediate parent node)'s first and last named
  node:

        + This region is usually equivalent to
  `er/mark-inside-pairs' but also works for keyword-delimited
  blocks such as begin ... end (if the grammar places them as
  first and last children nodes).

        + The first and last child nodes are unnamed, who are
  delimiters, there must be 2 or mode named nodes between those,
  the intended region includes all named nodes, excludes the
  mandatory preceding and trailing unnamed nodes.

        + The region must be balanced before marking, as some
  grammars let an opening bracket, eg. \"{\" (unnamed node after
  a named node) be a middle child and the closing one, eg. \"}\"
  be the last child, therefore those \"inner\" regions are not
  balanced as it has \"{\" but not \"}\", making it little sense
  to expand.

- When the above expansions can't be found, just mark the bigger
  ancestor node."
  (interactive)
  (cond
   ;; base case: mark the smallest node at point
   ((not (use-region-p))
    (let* ((node@pt (treesit-node-at (point))))
      (my-er/mark--from-to
       (treesit-node-end node@pt) (treesit-node-start node@pt))))
   (t
    ;; already marked something
    (let* ((beg0 (region-beginning))
           (end0 (region-end))
           ;; find the smallest node that covers the region
           (cover-node (treesit-node-on beg0 end0))
           (cover-beg (treesit-node-start cover-node))
           (cover-end (treesit-node-end cover-node)))
      ;; if found node is bigger than the region, mark it
      (if (or (< cover-beg beg0) (< end0 cover-end))
          (my-er/mark--from-to cover-end cover-beg)
        (let* ((super-node
                (treesit-parent-until
                 cover-node
                 (lambda (node)
                   (or (/= (treesit-node-start node) cover-beg)
                       (/= (treesit-node-end node) cover-end))))))
          (if-let* ((_
                     (and (< 1 (treesit-node-child-count super-node 'named))
                          (not
                           (treesit-node-check
                            (treesit-node-child super-node 0) 'named))
                          (not
                           (treesit-node-check
                            (treesit-node-child super-node -1) 'named))))
                    (list-beg
                     (treesit-node-start
                      (treesit-node-child super-node 0 'named)))
                    (list-end
                     (treesit-node-end
                      (treesit-node-child super-node -1 'named)))
                    (_
                     (and (< list-beg cover-beg cover-end list-end)
                          (my-er/treesit--balanced-region-p
                           list-beg list-end))))
              (my-er/mark--from-to list-end list-beg)
            (my-er/mark--from-to
             (treesit-node-end super-node)
             (treesit-node-start super-node)))))))))

;; NOTE: known inappropriate cases:
;; - Bash: "if" <cond> ";" "then" <body> "fi" is a linear list => hard to mark body
;; - Elixir: "%" <Struct> "{" <args> "}" : is a linear list => currently incorrectly mark "%" |region| "}"

;;;###autoload
(define-minor-mode my-expand-region-use-treesit-only-mode
  nil
  :global
  nil
  (if my-expand-region-use-treesit-only-mode
      (progn
        (if (treesit-parser-list)
            (setq-local er/try-expand-list
                        '(er/treesit-mark-bigger-list-or-node))
          (my-expand-region-use-treesit-only-mode 0)))
    (progn)))
;;;###autoload
(define-globalized-minor-mode my-expand-region-use-treesit-only-global-mode
  my-expand-region-use-treesit-only-mode
  my-expand-region-use-treesit-only-mode)

(provide 'my-treesit-expansions)

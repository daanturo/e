;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)

;; allow entering insert state by pressing "a" (but binding that key directly
;; doesn't work)
(my-bind :map 'edebug-mode-map
  :vi '(normal) [remap abort-recursive-edit] #'evil-append)

(my-bind :map 'edebug-mode-map "H" #'edebug-goto-here)

(provide 'my-after-edebug)

;; Local Variables:
;; no-byte-compile: t
;; End:

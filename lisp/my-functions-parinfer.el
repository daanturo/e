;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-parinfer-with-y-or-n-p-auto-no t)
;;;###autoload
(defun my-parinfer-with-yes-or-no-p-auto-no-a (func &rest args)
  (cond
   (my-parinfer-with-y-or-n-p-auto-no
    (my-with-advices
      '(yes-or-no-p y-or-n-p)
      :override
      (lambda (&rest _)
        (message "`parinfer-rust-mode' disabled in %S" (buffer-name))
        nil)
      (apply func args)))
   (:else
    (apply func args))))

;;;###autoload
(defun my-parinfer-rust-mode-interactive ()
  (interactive)
  (dlet ((parinfer-rust-check-before-enable 'immediate)
         (my-parinfer-with-y-or-n-p-auto-no nil))
    (parinfer-rust-mode)))

;;;###autoload
(defun my-parinfer-rust-mode-force-no-check ()
  (interactive)
  (dlet ((parinfer-rust-check-before-enable nil))
    (parinfer-rust-mode)))

;;;###autoload
(defun my-parinfer-rust-electric-pair-post-self-insert-inhibit-h ()
  (and parinfer-rust-mode
       (not current-prefix-arg)
       (member last-command-event '(?\( ?\[ ?\{))
       (not (my-point-in-string-face-p))
       (not (use-region-p))))

;;;###autoload
(defun my-parinfer-rust-force-version ()
  (interactive)
  (add-to-list 'parinfer-rust-supported-versions (parinfer-rust-version)))

(defvar my-parinfer-rust-execute-inhibit-variable-list '())

;;;###autoload
(defun my-parinfer-rust--execute--inhibit-maybe-a (func &rest args)
  (dlet ((parinfer-rust--disable
          (or
           parinfer-rust--disable
           (-some
            #'symbol-value my-parinfer-rust-execute-inhibit-variable-list))))
    (apply func args)))

;;;###autoload
(defun my-parinfer-disable-temporary (&rest _)
  (setq-local parinfer-rust--disable t))

;;; my-functions-parinfer.el ends here

(provide 'my-functions-parinfer)

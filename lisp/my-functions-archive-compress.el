;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-auto-extract|uncompress-and-unpack-archive
    (archives &optional confirm)
  "Automatically extract ARCHIVES while auto-detecting sub-folders.
CONFIRM: interactively confirm the shell command before
executing."
  ;; `dired-compress-file-suffixes'?
  (interactive (list (dired-get-marked-files) t))
  (-let* ((orig-buf (current-buffer))
          ;; (combine-and-quote-strings (-map #'f-relative archives))
          (file-list (-map #'f-relative archives))
          (combined (combine-and-quote-strings file-list)))
    (-->
     (start-process-shell-command
      "" nil
      (my-confirm-shell-command
        confirm
        (cond
         ((executable-find "atool")
          (concat "atool -e -x " combined))
         ((executable-find "ark")
          (concat "ark --autosubfolder --batch " combined " -o ."))
         (t
          (error
           "`%s': cannot a find an extractor utility!"
           #'my-auto-extract|uncompress-and-unpack-archive)))))
     (set-process-sentinel
      it
      (lambda (&rest _)
        (with-current-buffer orig-buf
          (when (equal major-mode 'dired-mode)
            (revert-buffer))))))))


;;; my-functions-archive-compress.el ends here

(provide 'my-functions-archive-compress)

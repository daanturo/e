;; -*- lexical-binding: t; -*-

(require 'dash)
(require 'compat)

(require '00-my-core-macros)

;;;###autoload
(defun my-delete-current-file (&optional kill-buffer?)
  (interactive "P")
  (-let* ((f (buffer-file-name))
          (d default-directory))
    (when f
      (ignore-error (file-missing file-error)
        (delete-file f t))
      (when kill-buffer?
        (kill-buffer))
      (dired d)
      f)))

;;;###autoload
(defun my-comment-buffer ()
  (interactive)
  (comment-region (point-min) (point-max)))

;;;###autoload
(defun my-open-new-indented-line (&optional arg)
  (interactive "P")
  (save-excursion
    (newline-and-indent arg)))

;;;###autoload
(defun my-open-then-new-indented-line ()
  (interactive)
  (save-excursion (open-line 1))
  (newline-and-indent))

;;;###autoload
(defun my-enter|return-dwim (&optional arg)
  (interactive "P")
  (cond
   ((and (my-point-in-emptyish-line-p)
         (or (bound-and-true-p outline-minor-mode)
             (derived-mode-p 'outline-mode)))
    (dlet ((outline-heading-alist (or outline-heading-alist
                                      (let ((cb (my-trimmed-comment-beg)))
                                        `((,(concat cb cb " * ") . 1))))))
      (outline-insert-heading)
      ;; Prevent capturing the previous heading's title
      (while (and my-outline-comment-mode
                  (save-match-data
                    (looking-back "[[:alnum:]][[:space:]]?"
                                  (line-beginning-position))))
        (delete-char -1))))
   ((and (my-point-in-comment-p)
         (eolp))
    (comment-indent-new-line))
   (t
    (my-open-new-indented-line arg))))

;;;###autoload
(defun my-string-inflection-change-symbol-case-name-style ()
  "Toggle symbol at point between camelCase and snake_case.
When none of the two above, select from a list of possible cases."
  (interactive)
  (require 'string-inflection)
  (string-inflection-insert
   (let ((str (string-inflection-get-current-word)))
     (cond
      ((string-inflection-underscore-p str)
       (string-inflection-camelcase-function str))
      ((string-inflection-camelcase-p str)
       (string-inflection-underscore-function str))
      (t
       (completing-read
        "To: "
        (list
         (string-inflection-kebab-case-function str)         ; => "emacs-lisp"
         (string-inflection-underscore-function str)         ; => "emacs_lisp"
         (string-inflection-pascal-case-function str)        ; => "EmacsLisp"
         (string-inflection-camelcase-function str)          ; => "emacsLisp"
         (string-inflection-upcase-function str)             ; => "EMACS_LISP"
         (string-inflection-capital-underscore-function str)))))))) ; => "Emacs_Lisp"


;;;###autoload
(defun my-insert-space-regexp (arg)
  (interactive "p")
  (insert (my-multiply-string
           arg
           (if (equal (char-after) ?\])
               "[:space:]"
             "[[:space:]]"))))

;;;###autoload
(defun my-toggle-comment-sexp (&optional arg)
  (interactive "P")
  (save-excursion
    ;; `sp-mark-sexp' may not be correct if the point is not at the comment start
    (unless arg
      (skip-chars-forward " "))
    ;;currently `sp-mark-sexp' does the best at detecting multi-line commented
    ;;sexps
    (sp-mark-sexp arg)
    (comment-or-uncomment-region (region-beginning) (region-end))))

;;;###autoload
(defun my-duplicate-line-or-region-down (arg &optional no-move)
  "Duplicate current region or line ARG times.
By default, try to move the cursor (or region) forward a distance equal
to the length of the inserted string, NO-MOVE prevents that."
  (interactive "p")
  (-let* ((region-flag (use-region-p))
          (pt0 (point))
          (mark0 (mark))
          (beg
           (if (use-region-p)
               (region-beginning)
             (line-beginning-position)))
          (end
           (if (use-region-p)
               (region-end)
             (progn
               (my-ensure-final-newline)
               (+ (line-end-position) 1))))
          (txt (buffer-substring beg end))
          (inserting (s-repeat arg txt))
          (insert-len (length inserting)))
    (save-excursion
      (goto-char end)
      (insert inserting))
    (cond
     (no-move
      nil)
     (region-flag
      (my-set-mark-from-to (+ mark0 insert-len) (+ pt0 insert-len)))
     (:else
      (forward-char insert-len)))
    (setq deactivate-mark nil)))

;;;###autoload
(defun my-duplicate-line-or-region-up (arg)
  "(`my-duplicate-line-or-region-down' ARG) without moving."
  (interactive "p")
  (my-duplicate-line-or-region-down arg t))

;;;###autoload
(defun my-untabify-buffer-to-spaces ()
  (interactive)
  (untabify (point-min) (point-max)))

;;;###autoload
(defun my-indent-and-untabify-to-spaces-buffer ()
  (interactive)
  (my-indent-buffer)
  (my-untabify-buffer-to-spaces))

;;;###autoload
(defun my-jump-bracket ()
  "Jump to a position, depending on brackets:
If the point is before a closing bracket: jump to before its opening one;
else jump to the next nearest closing bracket."
  (interactive)
  (pcase (- (nth 0 (save-excursion (syntax-ppss (1+ (point)))))
            (nth 0 (syntax-ppss)))
    (-1
     (backward-up-list))
    (1
     (forward-list)
     (backward-char))
    (_
     (my-go-to-end-of-list))))

;;;###autoload
(defun my-transpose-line-and-swap-comment-status ()
  (interactive)
  (save-excursion (comment-line 1))
  (my-save-line-and-column (transpose-lines 1))
  (save-excursion (comment-line 1)))

;;;###autoload
(defun my-comment-line (&optional arg)
  "Like `comment-line', but doesn't move to another line when ARG is nil and just called once.
When repeated, emulate `comment-line''s behavior."
  (declare (interactive-only "`comment-line'"))
  (interactive "P")
  (cond
   ;; non-nil `arg': just `comment-line'
   (arg
    ;; let next presses fall to the 3rd+ invocation case
    (setq this-command #'comment-line)
    (comment-line (prefix-numeric-value arg)))
   ;; 2nd+ invocation
   ((member last-command #'(my-comment-line
                            comment-line comment-line-backward))
    ;; for future invocation check(s)
    (setq this-command #'comment-line)
    ;; exact 2nd invocation: move to the next line
    (when (equal last-command #'my-comment-line)
      (forward-line 1)
      (back-to-indentation))
    ;; toggle comment and move
    (comment-line 1))
   ;; 1st invocation: toggle comment on current line, don't move
   (t
    (save-excursion
      (comment-line 1)))))

;;;###autoload
(defun my-join-next-line (&optional num)
  "Join the next NUM lines into current line without spaces.
If NUM is negative, join NUM lines before (and including) current
line."
  (interactive "p")
  (save-excursion
    (dotimes (_ (abs num))
      (save-excursion
        (if (< 0 num)
            (delete-indentation +1)
          (delete-indentation nil))))))

;;;###autoload
(defun my-mark-or-copy-line (&optional n)
  "Mark N following lines including current one.
From the first non-blank position to the character before the new
line.

When pressing a second time, copy the marked text and jump to the
position before marking this region."
  (interactive "p")
  ;; put point at the end allowing moving forward
  (-let* ((beg (my-line-non-whitespace-beg-position))
          (end (line-end-position n)))
    (if (and (equal last-command this-command)
             (use-region-p)
             (equal beg (region-beginning))
             (equal end (region-end)))
        (prog1 (copy-region-as-kill beg end)
          (pop-to-mark-command)
          (pop-to-mark-command))
      (prog1 (progn
               (push-mark)
               (my-set-mark-from-to beg end))
        (when evil-state
          (my-safe-call #'my-evil-adjust-cursor-when-visual))))))

;;;###autoload
(defun my-delete-region-or-buffer-contents (&optional force)
  (interactive (list (and buffer-read-only (y-or-n-p "inhibit-read-only ?"))))
  (dlet ((inhibit-read-only force))
    (if (use-region-p)
        (delete-region (region-beginning) (region-end))
      (delete-region (point-min) (point-max)))))

;;;###autoload
(defun my-force-delete-region-or-buffer-contents ()
  (interactive)
  (my-delete-region-or-buffer-contents t))

;;;###autoload
(defun my-split-region-string (beg end)
  (interactive (cond
                ((use-region-p)
                 (list (region-beginning) (region-end)))
                (:else
                 (-cons-to-list (bounds-of-thing-at-point 'string)))))
  (cl-assert (and beg end))
  (-let* ((str (read (buffer-substring-no-properties beg end)))
          ;; `save-excursion' or go to marker doesn't work
          (pt0 (point))
          (split
           (-->
            (split-string-and-unquote str)
            (-map #'my->Str it)
            (-interpose " " it))))
    (delete-region beg end)
    (save-excursion
      (goto-char beg)
      (apply #'insert split))
    (goto-char pt0)
    split))

;;;###autoload
(defun my-randomize-case-region (beg end &optional actions)
  (interactive (when (use-region-p)
                 (list (region-beginning) (region-end))))
  (-let* ((actions (or actions '(upcase-region downcase-region))))
    (dolist (pos (-iota (- end beg) beg))
      (-let* ((action (seq-random-elt actions)))
        (funcall action pos (+ 1 pos))))))

;;;###autoload
(defun my-insert|paste-escaped-quotes-string (str)
  "STR defaults to `current-kill'."
  (interactive (list (current-kill 0 t)))
  (insert (--> str (substring-no-properties it) (format "%S" it))))

;;;###autoload
(defun my-insert|paste-unescaped-quotes-string (str)
  "STR defaults to `current-kill'."
  (interactive (list (current-kill 0 t)))
  (insert (--> str (substring-no-properties it) (read it))))

;;;###autoload
(defun my-insert-xml|html-escaped-string (str)
  (declare (interactive-only t))
  (interactive (list
                (minibuffer-with-setup-hook (lambda ()
                                              (my-minibuffer-setup-hint
                                               (lambda
                                                 (str)
                                                 (xml-escape-string str))))
                  (read-string "`my-insert-xml|html-escaped-string': "
                               (current-kill 0 t)))))
  (insert (xml-escape-string str)))

;;;###autoload
(defun my-open-line-below (&optional arg)
  "Negative ARG: that many empty lines above."
  (interactive "p")
  (-let* ((times (or arg 1)))
    (cond
     ((< 0 times)
      (end-of-line)
      (dotimes (_ times)
        (insert "\n")))
     ((< times 0)
      (beginning-of-line)
      (save-excursion
        (dotimes (_ (abs times))
          (insert "\n")))))
    (indent-according-to-mode)
    (when (bound-and-true-p evil-state)
      (evil-insert-state))))

;; (defvar-keymap my-transpose-things-transient-keymap
;;   ;; format: off
;;   "M-t" #'transpose-words
;;   "t" #'transpose-words
;;   "c" #'transpose-chars
;;   "e" #'transpose-sexps
;;   "l" #'transpose-lines
;;   "p" #'transpose-paragraphs
;;   "r" #'transpose-regions
;;   "s" #'transpose-sentences
;;   ;; format: on
;;   )
;; (defvar my-transpose-things-with-timeout-delay 1.0)
;; ;;;###autoload
;; (defun my-transpose-things-with-timeout-or-words (arg)
;;   (interactive "*p")
;;   (-let* ((transient-exit-fn
;;            (set-transient-map my-transpose-things-transient-keymap
;;                               nil
;;                               (lambda () (transpose-words arg))
;;                               nil
;;                               my-transpose-things-with-timeout-delay)))))


(provide '16-my-commands-edit)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-chezmoi-edit (file)
  "Edit FILE's source path, with prefix arg prompt for file name, else current file."
  (interactive (list
                (if current-prefix-arg
                    (read-file-name "`my-chezmoi-edit': ")
                  buffer-file-name)))
  (-let* (((&plist :exit exit-code :out stdout :err stderr)
           (my-process-sync-run
            (list "chezmoi" "source-path" (expand-file-name file)))))
    (cond
     ((= 0 exit-code)
      (find-file (string-trim-right stdout "\n")))
     (:else
      (message "`my-chezmoi-edit': %S" (string-trim-right stderr))))))


;;; my-functions-dotfile.el ends here

(provide 'my-functions-dotfile)

;; -*- lexical-binding: t; -*-


(defvar-local my-sticky-header-mode--header-string nil)

(defun my-sticky-header-update-maybe ()
  ;; (current-idle-time) ; by using this to check, the header isn't updated
  ;; after previewing a file through the minibuffer?
  (if (buffer-modified-p)           ; reduce flickering when typing at top-level
      my-sticky-header-mode--header-string
    (my-sticky-header-update)))

(defun my-sticky-header-update ()
  (setq-local my-sticky-header-mode--header-string
              (my-sticky-header-beg-defun-line))
  my-sticky-header-mode--header-string)

;; The idle timer approach is too complicated and isn't worthy
;;;###autoload
(define-minor-mode my-sticky-header-mode
  nil
  :global
  nil
  (if my-sticky-header-mode
      (progn
        (when (proper-list-p header-line-format)
          (add-to-list
           'header-line-format '(t (:eval (my-sticky-header-update-maybe))))))
    (progn
      (when (proper-list-p header-line-format)
        (my-delete-in-list!
          'header-line-format '(t (:eval (my-sticky-header-update-maybe))))))))

;;;###autoload
(defvar my-sticky-header-mode-exclude-mode-list
  '(lsp-mode
    my-eval-output-mode
    ;; lsp-headerline-breadcrumb-mode
    ))

;;;###autoload
(defun my-sticky-header-mode-maybe ()
  (cond
   ((and (provided-mode-derived-p major-mode 'prog-mode)
         (not
          (-some
           #'my-enabled-mode-p
           my-sticky-header-mode-exclude-mode-list)))
    (my-sticky-header-mode))
   (my-sticky-header-mode
    (my-sticky-header-mode 0))))

(defun my-sticky-header-beg-defun-line ()
  (my-stylize-string
   (my-get-line-string-at-position (my-beg-defun-position) (window-width))
   ;; Italic to distinguish when at the top
   :slant 'italic
   ;; prevent too large texts
   :height (face-attribute 'default :height)
   ;;
   ))

(provide 'my-sticky-header)

;; -*- lexical-binding: t; -*-


;;;###autoload
(defvar my-idle-eval-interval 1.0)
(defvar my-idle-eval-forms-queue nil)
(defvar my-idle-eval-current-timer-list nil)

;;;###autoload
(defmacro my-idle-eval (&rest body)
  "Evaluate BODY when idle.
Recorded forms are evaluated one by one with some idle time between each."
  (declare (indent defun))
  `(progn
     (setq my-idle-eval-forms-queue
           (my-inserted-at
            my-idle-eval-forms-queue
            ;; Make the order of execution random
            (random (1+ (length my-idle-eval-forms-queue)))
            '(progn ,@body)))
     ;; Invoke the timer when we have just added a form
     (when (= 1 (length my-idle-eval-forms-queue))
       (letrec
           ((func (lambda ()
                    (if my-idle-eval-forms-queue
                        ;; Evaluate the next form
                        (let ((gc-cons-threshold most-positive-fixnum)
                              (form (pop my-idle-eval-forms-queue))
                              (prev-time (time-to-seconds (current-time))))
                          (my-lexical-eval form)
                          ;; Case: continuous idle period time
                          (let ((prev-idle-time (time-to-seconds (current-idle-time))))
                            (push (run-with-idle-timer
                                   (+ my-idle-eval-interval prev-idle-time)
                                   nil func)
                                  my-idle-eval-current-timer-list)
                            (message "%s: after %ss idling, took %ss to evaluate: %s."
                                     #'my-idle-eval
                                     prev-idle-time
                                     (- (time-to-seconds (current-time)) prev-time)
                                     form)))
                      ;; The queue is empty, stop the timer
                      (progn
                        ;; (cancel-timer my-idle-eval-current-timer-list)
                        (mapc #'cancel-timer my-idle-eval-current-timer-list)
                        (message "Finished %s." #'my-idle-eval))))))
         ;; Case: idle time is interrupted
         (push (run-with-idle-timer my-idle-eval-interval t func)
               my-idle-eval-current-timer-list)))))

(provide 'my-idle-eval)

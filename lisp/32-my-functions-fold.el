;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-fold-hide-lines-with-indent-greater-than (&optional copy)
  "`set-selective-display'. COPY: display in a cloned buffer."
  (interactive "P")
  (with-current-buffer (if copy
                           (my-copy|clone-buffer)
                         (current-buffer))
    (-let*
        ((indent-lv
          (read-number "`my-fold-hide-lines-with-indent-greater-than': "
                       (+ 1 (my-line-indent-offset))))
         ;; TODO: fix that `indent-bars-mode' may make Emacs hang when calling
         ;; `set-selective-display'
         (idm (bound-and-true-p indent-bars-mode)))
      (when idm
        (indent-bars-mode 0))
      (set-selective-display indent-lv)
      (when idm
        (my-add-local-hook-once
          'my-set-selective-display-after-hook 'indent-bars-mode))
      (message "To restore: M-x `set-selective-display' \"%s\"."
               (key-description
                (car (where-is-internal #'set-selective-display)))))))

;;; 32-my-functions-fold.el ends here

(provide '32-my-functions-fold)

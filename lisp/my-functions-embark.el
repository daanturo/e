;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'embark)

;; grab the input (excluding the consult split character) and go to there to
;; easily wgrep+iedit
(defun my-embark-writable-export--go-to-match-h-fn (&optional search-term)
  (-let* ((search-term
           (or search-term
               (or (thing-at-point 'symbol 'no-properties)
                   (-when-let* ((sym
                                 (with-selected-window
                                     (minibuffer-selected-window)
                                   (thing-at-point 'symbol 'no-properties)))
                                (curr (minibuffer-contents-no-properties)))
                     (and (string-search sym curr) sym))))))
    (lambda ()
      (when search-term
        (-let* ((pt (point)))
          (goto-char (point-min))
          (-let* ((lim (max (pos-eol 3) (window-end))))
            (unless (and (or (save-excursion
                               (re-search-forward search-term lim 'noerror))
                             (save-excursion
                               (search-forward search-term lim 'noerror)))
                         (goto-char (match-beginning 0)))
              (goto-char pt))))))))

;;;###autoload
(defun my-embark-writable-export ()
  (interactive)
  (dlet ((embark-after-export-hook
          `(,@embark-after-export-hook
            my-make-export-buffer-writable-to-edit
            ,(my-embark-writable-export--go-to-match-h-fn))))
    (embark-export)))

;;;###autoload
(defun my-embark--run-quit-minibuffer-focus (form buffer-regexp)
  (let ((old-windows (window-list)))
    (my-lexical-eval form)
    ;; quit the minibuffer and focus it
    (let ((collect-buffer (-some
                           (lambda (window)
                             (--> (window-buffer window)
                                  buffer-name
                                  (and (string-match-p buffer-regexp it)
                                       it)))
                           (-difference (cons (selected-window) (window-list))
                                        old-windows))))
      (my-quit-minibuffer-and-run
       (pop-to-buffer collect-buffer)))))

;;;###autoload
(defun my-embark-export ()
  "Like `embark-export', but prefer plain `embark-collect'-like in some cases."
  (interactive)
  (require 'embark)
  (dlet ((embark-after-export-hook
          `(,@
            embark-after-export-hook
            ,(my-embark-writable-export--go-to-match-h-fn))))
    (cond
     ;; Those exporters are either slow to perform or not well readable; manually
     ;; handle window layout, as by default, `embark-collect''s buffer isn't
     ;; focused
     ((member
       (alist-get
        (my-minibuffer-completion-category) embark-exporters-alist)
       '(embark-export-apropos embark-export-customize-variable))
      (my-export-minibuffer-symbols-and-quit))
     ((member (my-minibuffer-completion-category) '(embark-keybinding))
      (my-embark--run-quit-minibuffer-focus '(embark-collect) "^\\*Embark"))
     (t
      (my-embark--run-quit-minibuffer-focus '(embark-export) "^\\*Embark")))))

;;;###autoload
(defun my-embark-act-no-quit ()
  (interactive)
  (dlet ((embark-quit-after-action nil))
    (embark-act)))

;;;###autoload
(defun my-embark-dwim-no-quit ()
  (interactive)
  (dlet ((embark-quit-after-action nil))
    (embark-dwim)))

;;;###autoload
(defun my-embark-definition (symbol &optional display)
  (interactive "sSymbol: \np")
  (cond ((<= 2 display 4)
         (xref-find-definitions-other-window symbol))
        ((<= 5 display)
         (xref-find-definitions-other-frame symbol))
        (t
         (xref-find-definitions symbol))))

;;;###autoload
(defun my-embark-elisp-definition (symbol &optional display)
  (interactive "sSymbol: \np")
  (dlet ((xref-backend-functions (lambda () 'elisp)))
    (my-embark-definition symbol display)))

;;;###autoload
(defun my-embark-current-prompt-keymap ()
  (let* ((targets (embark--targets))
         (target (car targets)))
    (embark--action-keymap (plist-get target :type)
                           (cdr targets))))

;;;###autoload
(defun my-embark--act-dont-quit-when-unexpected-command--a
    (func action target &optional quit &rest args)
  (let ((keymap (my-embark-current-prompt-keymap)))
    (apply func action target
           (and quit (where-is-internal action (list keymap)))
           args)))

;;;###autoload
(defun embark-which-key-indicator ()
  "An embark indicator that displays keymaps using which-key.
The which-key help message will show the type and value of the
current target followed by an ellipsis if there are further
targets."
  (lambda (&optional keymap targets prefix)
    (if (null keymap)
        (which-key--hide-popup-ignore-command)
      (which-key--show-keymap
       (if (eq (plist-get (car targets) :type) 'embark-become)
           "Become"
         (format "Act on %s '%s'%s"
                 (plist-get (car targets) :type)
                 (embark--truncate-target (plist-get (car targets) :target))
                 (if (cdr targets) "…" "")))
       (if prefix
           (pcase (lookup-key keymap prefix 'accept-default)
             ((and (pred keymapp) km) km)
             (_ (key-binding prefix 'accept-default)))
         keymap)
       nil nil t (lambda (binding)
                   (not (string-suffix-p "-argument" (cdr binding))))))))

;;;###autoload
(defun embark-hide-which-key-indicator (fn &rest args)
  "Hide the which-key indicator immediately when using the completing-read prompter."
  (which-key--hide-popup-ignore-command)
  (let ((embark-indicators
         (remq #'embark-which-key-indicator embark-indicators)))
    (apply fn args)))

;;;###autoload
(defun my-embark-xdg-open-file (filename)
  (my-xdg-open-file-with-default-application filename))


(provide 'my-functions-embark)

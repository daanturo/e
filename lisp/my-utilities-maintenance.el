;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-unkit-sync (&optional inhibit-install)
  (interactive (list (not current-prefix-arg)))
  (when (not inhibit-install)
    (switch-to-buffer (messages-buffer)))
  (with-environment-variables (("MY_EMACS_SYNC" "1"))

    ;; ;; done automatically when idle
    ;; (when (not (daemonp))
    ;;   (my-env-generate-env-file))

    (load (file-name-concat my-emacs-conf-dir/ "early-init.el"))
    (load (file-name-concat my-emacs-conf-dir/ "init.el"))
    (when (not inhibit-install)
      (my-pkg-install-added))
    (my-package-vc-checkout-all-pinned-revision)))

(defvar my-unkit-after-update-hook '())
(defvar my-unkit-before-update-hook '())

;;;###autoload
(defun my-unkit-update ()
  (interactive)
  (run-hooks 'my-unkit-before-update-hook)
  (my-useelisp-update-all)
  (my-package-vc-checkout-all-default-branch-if-unpinned)
  (package-upgrade-all)
  (run-hooks 'my-unkit-after-update-hook)
  (my-unkit-sync))

;;;###autoload
(defun my-check-obsolete-or-deprecated ()
  (-let* ((removed-things '())
          (internal-alist
           '( ;

             (apheleia . (apheleia--get-formatters))

             (breadcrumb . (breadcrumb--header-line))

             (consult .
                      (consult--async-split-initial
                       consult--buffer-query
                       consult--buffer-sort-visibility
                       consult--buffer-state
                       consult--customize-alist
                       consult--file-preview
                       consult--preview-function
                       consult--source-buffer))

             (consult-xdg-recent-files
              . (consult-xdg-recent-files--source-system-file))

             (corfu .
                    (corfu--auto-complete-deferred
                     corfu--auto-tick
                     corfu--filter-completions
                     corfu--in-region
                     corfu--index
                     corfu--metadata-get
                     corfu--move-prefix-candidates-to-front
                     corfu--scroll))

             (ivy . (ivy--insert-symbol-boundaries))

             (jinx . (jinx--on))

             (marginalia . (marginalia--truncate))

             (minibuffer . (completion-pcm--string->pattern))

             (org . (org--image-yank-media-handler))

             (package . (package--alist package--builtins package--get-deps))

             (package-vc . (package-vc--read-package-desc))

             (parinfer-rust-mode
              . (parinfer-rust--check-for-indentation parinfer-rust--disable))

             (puni . (puni--in-comment-p puni--in-string-p))

             (sideline-blame . (sideline-blame--get-message))

             (simple . (read--expression-try-read))

             (telega .
                     (telega--MessageSender
                      telega--chat
                      telega--getChatHistory
                      telega--getGroupsInCommon
                      telega--getMessageLink
                      telega--info
                      telega--on-updateOption
                      telega--options
                      telega--ordered-chats
                      telega--searchChatMembers
                      telega--searchChatMessages
                      telega--sendMessage
                      telega--tl-get
                      telega-chat--info
                      telega-chat--message-filters
                      telega-chatbuf--chat
                      telega-chatbuf--gen-input-file
                      telega-file--download
                      telega-file--renew
                      telega-photo--highres
                      telega-server--call
                      telega-server--send
                      telega-user--by-username))

             (tempel .
                     (tempel--active
                      tempel--exit tempel--interactive tempel--templates))

             (treemacs . (treemacs--follow))

             (vc-git . (vc-git--out-ok))

             (vertico . (vertico--exhibit))

             (xref . (xref--read-identifier)))))

    ;;


    (dolist (lib-things internal-alist)
      (require (car lib-things) nil t)
      (with-eval-after-load (car lib-things)
        (dolist (thing (cdr lib-things))
          (when (not (or (boundp thing) (fboundp thing)))
            (push thing removed-things)))))

    (when (< 0 (length removed-things))
      (my-notify-and-message
       (format
        "my-check-obsolete-or-deprecated:
- Unbound: %S"
        removed-things)))

    removed-things))

;;;###autoload
(defun my-maintenance-print-emacs-build-time ()
  (interactive)
  (princ (my-ISO-time emacs-build-time)))

;;; my-utilities-maintenance.el ends here

(provide 'my-utilities-maintenance)

;; Local Variables:
;; no-byte-compile: t
;; End:

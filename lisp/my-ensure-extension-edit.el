;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-ensure-extension-edit-mode-alist '())

(defvar my-ensure-extension-edit-directory (make-temp-file
                                            "my-ensure-extension-edit" 'dir-flag))

;;;###autoload
(defun my-ensure-extension-edit-open (file-name dot-extension)
  "Open FILE-NAME with DOT-EXTENSION appended.
By symlink it to a temporary directory and append DOT-EXTENSION.

Use case: Workaround some linters that are unable to function
when the source file doesn't have an extension."
  (-let* ((link (file-name-concat my-ensure-extension-edit-directory
                                  (concat file-name dot-extension)))
          (modified (buffer-modified-p (get-file-buffer file-name))))
    (make-directory (file-name-parent-directory link) 'parents)
    (make-symbolic-link file-name link 'ok)
    (my-with-find-file-follow-true-name nil
      (my-set-visited-filename link))))

;;;###autoload
(defun my-ensure-extension-edit-may-open-current-file ()
  "See `my-ensure-extension-edit-open'.
Check this function's definition for the conditions."
  (interactive)
  (-let* ((ext (alist-get major-mode my-ensure-extension-edit-mode-alist)))
    (when (and
           ext
           (null (file-name-extension buffer-file-name))
           (my-shebang-p)
           ;; an error at the beginning
           (equal (point-min)
                  (-some--> (flymake-diagnostics)
                    (-map 'flymake-diagnostic-beg it)
                    (-min it))))
      (my-ensure-extension-edit-open buffer-file-name ext))))

(provide 'my-ensure-extension-edit)

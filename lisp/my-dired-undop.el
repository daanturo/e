;; -*- lexical-binding: t; -*-

;;; Commentary:

;; This package enables undoing for some Dired operations such as
;; `dired-do-rename', `dired-do-copy', `dired-do-symlink', etc.

;; Note that this doesn't enable undoing trashing (of course neither deleting),
;; as its mechanism is different from the mentioned commands

;; Initialization:

;; (with-eval-after-load 'dired-aux
;;   ;; this must be enabled for the key below to work
;;   (my-dired-undop-record-mode)
;;   (define-key dired-mode-map (kbd "C-z") #'my-dired-undop-operation))

;;; Code:

(require 'cl-macs)
(require 'dash)

(eval-when-compile
  (require 'dired-aux))

(declare-function #'dired-create-files "dired-aux")

(defcustom my-dired-undop-delete-command '("rm" "-rf")
  nil)

(defcustom my-dired-undop-move-command '("mv")
  nil)

(defcustom my-dired-undop-functions-to-move
  '(dired-rename-file)
  "Operations that should be undone by renaming/moving files.")

(defcustom my-dired-undop-functions-to-delete
  '(dired-copy-file
    make-symbolic-link
    dired-make-relative-symlink dired-hardlink)
  "Operations that should be undone by deleting files.")

(cl-defstruct (my-dired-undop-history
               (:constructor
                my-dired-undop-history-construct
                (creator-fn srcs dess
                            &optional (time (format-time-string "%H:%M:%S")))))
  creator-fn srcs dess
  time)

(defun my-dired-undop-history-op (obj)
  (list (my-dired-undop-history-creator-fn obj)
        (my-dired-undop-history-srcs obj)
        (my-dired-undop-history-dess obj)))

(defvar my-dired-undop--history '())

;;;###autoload
(defun my-dired-undop-record (file-creator src/s des/s)
  (push (my-dired-undop-history-construct
         file-creator
         (ensure-list (copy-sequence src/s))
         (ensure-list des/s))
        my-dired-undop--history))

(defun my-dired-undop--record-dired-create-files--a (func &rest args)
  (-let* (((file-creator _ srcs name-constructor . _) args))
    (my-dired-undop-record file-creator srcs (-map name-constructor srcs))
    (apply func args)))

;;;###autoload
(define-minor-mode my-dired-undop-record-mode nil
  :global t
  (if my-dired-undop-record-mode
      (progn
        (advice-add #'dired-create-files :around #'my-dired-undop--record-dired-create-files--a))
    (progn
      (advice-remove #'dired-create-files #'my-dired-undop--record-dired-create-files--a))))

(defun my-dired-undop--qquote (arg)
  (format "\"%s\"" arg))

(defvar my-dired-undop-buffer "*my-dired-undop*")

(defun my-dired-undop--make-delete-cmdargs (_oldsrcs olddess)
  (-let* ((olddess (-map #'file-relative-name olddess)))
    (append my-dired-undop-delete-command olddess)))

(defun my-dired-undop--make-move-cmdargs (oldsrcs olddess)
  (-let* ((oldsrcs (-map #'file-relative-name oldsrcs))
          (olddess (-map #'file-relative-name olddess)))
    (cond
     ;; moved only 1 file
     ((= 1 (length oldsrcs) (length olddess))
      (append my-dired-undop-move-command
              (list (cl-first olddess)
                    (cl-first oldsrcs))))
     ;; moved multiple files from a directory to another
     ((--> (-map #'file-name-parent-directory oldsrcs)
           (-uniq it)
           (length it)
           (= 1 it))
      (-let* ((old-parent-dir (file-name-parent-directory (cl-first oldsrcs))))
        (append my-dired-undop-move-command
                olddess
                (list old-parent-dir))))
     ;; moved from multiple directories: unsupported because of the complexity
     ;; to generate the undo command
     (t '("echo" "unsupported")))))

(defun my-dired-undop--make-cmdargs (creator-fn srcs dess)
  (interactive)
  (cond
   ((member creator-fn my-dired-undop-functions-to-move)
    (my-dired-undop--make-move-cmdargs srcs dess))
   ((member creator-fn my-dired-undop-functions-to-delete)
    (my-dired-undop--make-delete-cmdargs srcs dess))))

(defun my-dired-undop--cmdargs->full-cmd (cmdargs)
  (--> (mapconcat (lambda (arg)
                    (if (string-match-p "[ \t]" arg)
                        (format "\"%s\"" arg)
                      arg))
                  cmdargs
                  " ")
       (my-string-in-mode it 'sh-mode)))

(defun my-dired-undop--read-history ()
  (-let* ((alist (cl-loop for entry in my-dired-undop--history
                          collect
                          (-let* ((lisp-op (my-dired-undop-history-op entry))
                                  (cmdargs (apply #'my-dired-undop--make-cmdargs lisp-op)))
                            (list
                             ;; select
                             (my-dired-undop--cmdargs->full-cmd cmdargs)
                             ;; actual execution
                             cmdargs
                             ;; annotation
                             (--> (format " (%s) %S" (my-dired-undop-history-time entry)
                                          lisp-op)
                                  ;; (my-string-in-mode it 'lisp-data-mode)
                                  (propertize it 'face 'completions-annotations))))))
          (cands (-map #'car alist))
          (annot-fn (lambda (cand)
                      (-let* (((_cand _cmdargs annot) (assoc cand alist)))
                        annot)))
          (selected (completing-read
                     "Undo a previous Dired operation by executing: "
                     (lambda (string pred action)
                       (if (eq action 'metadata)
                           `(metadata
                             (annotation-function . ,annot-fn)
                             (display-sort-function . identity))
                         (complete-with-action action cands string pred))))))
    (-let* (((_ cmdargs _) (assoc selected alist)))
      cmdargs)))

(defun my-dired-undop-operation ()
  (interactive)
  (-let* ((buf (current-buffer))
          ((prog . args) (my-dired-undop--read-history))
          (callback (lambda (&rest _)
                      (with-current-buffer buf
                        (when (and (derived-mode-p 'dired-mode)
                                   (not (buffer-modified-p buf)))
                          (dired-revert)))))
          (proc (apply #'start-process "my-dired-undop" my-dired-undop-buffer prog args)))
    (if (process-live-p proc)
        (set-process-sentinel proc callback)
      (funcall callback))))


;;; my-dired-undop.el ends here

(provide 'my-dired-undop)

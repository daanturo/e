;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)

(my-add-mode-hook-and-now 'Info-mode
  'my-scaled-center-mode)

(my-bind :map 'Info-mode-map :vi '(motion) "L" #'Info-history)

;;; my-hook-Info-mode-hook.el ends here

(provide 'my-hook-Info-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

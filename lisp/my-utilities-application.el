;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-podman (&optional use-docker)
  "Use `docker.el' to run podman.
With non-nil USE-DOCKER, use-docker commands Docker variants and use them instead."
  (interactive "P")
  (let ((docker-command-symbols (cl-loop
                                 for sym being the symbols
                                 when (and (string-match-p "^docker.*command$"
                                                           (symbol-name sym))
                                           (custom-variable-p sym))
                                 collect sym)))
    (if use-docker
        (mapc #'custom-reevaluate-setting docker-command-symbols)
      (dolist (cmd docker-command-symbols)
        (let ((podman-command (replace-regexp-in-string "docker"
                                                        "podman"
                                                        (symbol-value cmd))))
          (when (executable-find podman-command)
            (set cmd podman-command)))))
    (docker)))

;;;###autoload
(defun my-manual ()
  "If the `man' executable is found, run `man', else `woman'."
  (interactive)
  (dlet ((Man-notify-method 'bully))
    (call-interactively
     (if (executable-find "man")
         #'man
       #'woman))))

;;;###autoload
(defun my-display-getenv ()
  (interactive)
  ;; print string
  (my-pretty-eval-expression
   (call-interactively #'getenv)))

;;;###autoload
(defun my-getenv-path-all (&optional display)
  (interactive (list t))
  (-let* ((paths (--> (getenv "PATH") (split-string it ":"))))
    (when display
      (inspector-inspect paths))
    paths))


(provide 'my-utilities-application)

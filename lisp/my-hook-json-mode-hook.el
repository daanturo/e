;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defun my-lang-json-mode-hook-h ()
  (when (and (null (bound-and-true-p apheleia-formatter))
             ;; JSONC/JSON5 not supported
             (equal "package.json" (file-name-nondirectory (buffer-file-name))))
    (setq-local apheleia-formatter 'prettier-json)))

(my-add-mode-hook-and-now
  '(json-mode js-json-mode json-ts-mode) #'my-lang-json-mode-hook-h)

;;; my-hook-json-mode-hook.el ends here

(provide 'my-hook-json-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(defun my-minibuffer-input-history-category-variable ()
  "The higher index, the older the entry."
  (my-concat-symbols '@ 'my-minibuffer-input-history
                     (my-minibuffer-completion-category)))

(defun my-minibuffer-input-history-category-variable-list ()
  (let ((prefix (symbol-name (my-concat-symbols "" 'my-minibuffer-input-history '@))))
    (cl-loop for sym being the symbols
             when (let ((sym-name (symbol-name sym)))
                    (and (boundp sym)
                         (string-prefix-p prefix sym-name)
                         (< (length prefix) (length sym-name))))
             collect sym)))

(defun my-minibuffer-input-history-alist ()
  (-map (lambda (var) (cons var (my-bounded-value var)))
        (my-minibuffer-input-history-category-variable-list)))

(defun my-minibuffer-input-current-category-history ()
  (my-bounded-value (my-minibuffer-input-history-category-variable)))

(defun my-exclude-minibuf-input-p (inp)
  (let ((sym (intern inp)))
    (or (boundp sym)
        (fboundp sym)
        (my-with-demoted-errors
         (file-exists-p (file-name-concat "/" inp)))
        (< (expt 2 8) (length inp))
        (string-empty-p inp)
        (string-match-p "\n" inp))))

;;;###autoload
(defun my-record-minibuffer-input-history ()
  (let ((inp (string-trim (minibuffer-contents-no-properties))))
    (unless (my-exclude-minibuf-input-p inp)
      (let ((var (my-minibuffer-input-history-category-variable)))
        (add-hook 'savehist-minibuffer-history-variables var)
        (set
         var
         (-->
          (cl-delete
           inp
           (my-bounded-value var)
           :test #'string=)
          (cons inp it) (-take history-length it)))))))

;;;###autoload
(defun my-minibuffer-input-history-read-&-insert ()
  (interactive)
  (-let* ((cur-inp (minibuffer-contents-no-properties)))
    (insert
     (-let* ((prompt "Previous input: ")
             (candidates (my-minibuffer-input-current-category-history)))
       (completing-read
        prompt
        (lambda (string pred action)
          (if (eq action 'metadata)
              '(metadata (display-sort-function . identity))
            (complete-with-action action candidates string pred)))
        nil nil
        cur-inp)))))

;;;###autoload
(defun my-minibuffer-input-history-insert-last ()
  (interactive)
  (-some--> (my-minibuffer-input-current-category-history) -first-item
            (insert it)))

(defun my-minibuffer-input-history-all ()
  ;; `mapcan' creates loops in lists after executing?
  (-mapcat #'my-bounded-value
           (my-minibuffer-input-history-category-variable-list)))

;;;###autoload
(defun my-delete-minibuf-input-hist ()
  (interactive)
  (let* ((all-hist (my-minibuffer-input-history-all))
         (to-deletes (completing-read-multiple
                      "Delete previous minibuffer input from all: "
                      (lambda (string pred action)
                        (if (eq action 'metadata)
                            `(metadata
                              ,(cons 'display-sort-function
                                     (lambda (cands)
                                       (nreverse (sort cands #'my-by-length<)))))
                          (complete-with-action action all-hist string pred))))))
    (dolist (var (my-minibuffer-input-history-category-variable-list))
      (set var (-difference (symbol-value var) to-deletes)))))

;;;###autoload
(defun my-record-minibuffer-input--a (func &rest args)
  (my-record-minibuffer-input-history)
  (apply func args))

;;;###autoload
(progn
  (define-minor-mode my-minibuffer-input-history-key-mode nil
    :global nil
    :keymap
    `(;;
      (,(kbd "M-h") . my-minibuffer-input-history-read-&-insert)
      (,(kbd "M-i") . my-minibuffer-input-history-insert-last)
      (,(kbd "M-n") . (menu-item "" nil :filter my-minibuffer-next-history-menu-item-filter-element))
      ;;
      ))
  (define-globalized-minor-mode my-minibuffer-input-history-mode my-minibuffer-input-history-key-mode
    ignore
    (if my-minibuffer-input-history-mode
        (progn
          (add-hook 'minibuffer-setup-hook #'my-minibuffer-input-history-key-mode)
          ;; the input isn't recorded there, only result
          (add-hook 'minibuffer-exit-hook #'my-record-minibuffer-input-history)
          (my-add-advice/s '(delete-minibuffer-contents)
            :around #'my-record-minibuffer-input--a))
      (progn
        (remove-hook 'minibuffer-setup-hook #'my-minibuffer-input-history-key-mode)
        (remove-hook 'minibuffer-exit-hook #'my-record-minibuffer-input-history)
        (my-remove-advice/s '(delete-minibuffer-contents)
          #'my-record-minibuffer-input--a)))))

(provide 'my-minibuffer-input-history-mode)

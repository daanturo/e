;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-dabbrev-expand-no-error (arg)
  (interactive "*P")
  (ignore-errors
    (dabbrev-expand arg)))

;;;###autoload
(defun my-expand-or-select-expansions (&optional arg)
  (interactive "P")
  (dlet
      (
       ;; complete file name components displayed in another buffers, such as (ma)git diff
       (dabbrev-abbrev-char-regexp "[-_[:alnum:]]"))
    (cond
     ;; incompatibilities: skip the `hippie-expand' part
     ((or
       ;; `iedit-after-change-list' vs the restorer function
       (bound-and-true-p iedit-mode))
      (my-dabbrev-completion-all-buffers))
     ((member last-command '(my-expand-or-select-expansions hippie-expand))
      ;; (ignore-errors (undo))
      (funcall-interactively my-undo-command 1)
      ;; (hippie-expand -1)
      ;; (he-reset-string)
      (my-dabbrev-completion-all-buffers))
     (t
      (my-hippie-expand* arg)))))

;;;###autoload
(defun my-dabbrev-completion-all-buffers ()
  (interactive)
  ;; If in string or comment (like writing docstrings), use the input's case,
  ;; else preserve the expansion's
  (dlet ((dabbrev-case-replace (my-point-in-string-or-comment-p))
         (completion-in-region-function
          (default-value 'completion-in-region-function))
         ;; If character before point is capitalized: case insensitive, else
         ;; smart case
         (case
          (if (my-uppercase? (char-before))
              my-completion-case-insensitive-variables
            my-completion-smart-case-variables)))
    ;; Case-insensitive
    ;; (minibuffer-with-setup-hook #'my-completion-to-case-insensitive)
    (my-lexical-eval
     `(dlet ,case
        ;; Search in all buffers
        (dabbrev-completion '(16))))))

;;;###autoload
(defun my-dabbrev-capf-current-buffer-only ()
  (dlet ((dabbrev-check-other-buffers nil)
         (dabbrev-check-all-buffers nil)
         (cape-dabbrev-check-other-buffers nil))

    ;; no hope with making `dabbrev-capf' silent, we need `cape-capf-super' to
    ;; merge anyway

    ;; (my-with-advice
    ;;   #'make-progress-reporter
    ;;   :override #'ignore
    ;;   (ignore-errors
    ;;     (my-with-advice #'user-error :override #'ignore (dabbrev-capf))))

    (cape-dabbrev)))

;; NOTE: `dabbrev-capf' always return non-nil even when input isn't sensible,
;; don't put it in `completion-at-point-functions'

;;; my-functions-dabbrev.el ends here

(provide 'my-functions-dabbrev)

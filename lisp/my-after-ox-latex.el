;; -*- lexical-binding: t; -*-

(require 'ox-latex)

(require '00-my-core-macros)

;; * Export settings

(when (require 'ox-extra nil 'noerror)
  (ox-extras-activate '(ignore-headlines)))

;; Better set those variables local for each file, for reproducibility

;; (setq org-latex-tables-centered nil)

;; (setq org-latex-compiler "lualatex")
;; ;; Enable lualatex, as currently just setting `org-latex-compiler' globally isn't enough
;; (setq org-latex-pdf-process
;;       `(,(concat my-latexmk-base-command
;;                  " -pdf -%latex \
;; -output-directory=%o %f")))

;; ;; (add-to-list 'org-latex-logfiles-extensions "tex~")
;; ;; (setq org-latex-image-default-width "1\\textwidth")
;; ;; (setq org-latex-image-default-height "1\\textheight")
;; ;; (setq org-latex-image-default-option "keepaspectratio")

;; (add-to-list 'org-latex-packages-alist '("" "color")) ; Suggested by `org-latex-src-block-backend'
;; (add-to-list 'org-latex-packages-alist '("" "indentfirst"))
;; (add-to-list 'org-latex-packages-alist '("" "mathtools" t))

;; (add-to-list 'org-latex-packages-alist '("" "listings"))
;; (my-add-list!
;;   'org-latex-listings-options
;;   '(("breaklines" "true") ("breakwhitespace" "true")))

;; (setq org-latex-listings 'minted)       ; obsolete
;; ;; The `engraved' option is cool, but is produces un-editable .tex file
;; (setq org-latex-src-block-backend 'minted)

;; (add-to-list 'org-latex-packages-alist '("newfloat" "minted"))
;; (my-add-list! 'org-latex-minted-options '(("breaklines" "true")))

(add-hook
 'org-latex-classes
 '("my-book-from-chapter"
   "\\documentclass{book}"
   ;; ("\\part{%s}" . "\\part*{%s}")
   ("\\chapter{%s}" . "\\chapter*{%s}")
   ("\\section{%s}" . "\\section*{%s}")
   ("\\subsection{%s}" . "\\subsection*{%s}")
   ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
   ("\\paragraph{%s}" . "\\paragraph*{%s}")
   ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
;;;###autoload
(defun my-org-ox-latex-set-export-heading-section-level-chapter ()
  (interactive)
  (save-excursion
    (add-file-local-variable
     'org-latex-classes `(,(assoc "my-book-from-chapter" org-latex-classes))))
  (insert "#+LATEX_CLASS: my-book-from-chapter" "\n"))

;; * Early config

;;;###autoload
(progn
  (my-accumulate-config-collect
   (defvar my-latexmk-base-command
     "latexmk -f -interaction=nonstopmode -shell-escape -bibtex")
   (defvar my-org-latex-process-command
     "lualatex -shell-escape -interaction=nonstopmode -output-directory=%o %f")))

;;; my-after-ox-latex.el ends here

(provide 'my-after-ox-latex)

;; Local Variables:
;; no-byte-compile: t
;; End:

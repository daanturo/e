;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)

(setq-default bookmark-search-size 32)

(setq bookmark-make-record-function #'my-bookmark-make-record-fn)

(advice-add #'bookmark-jump :around #'my-bookmark-handle-bookmark--restore-region-a)

(provide 'my-after-bookmark)

;; Local Variables:
;; no-byte-compile: t
;; End:

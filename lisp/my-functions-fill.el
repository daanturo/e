;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-join-region-or-paragraph ()
  (interactive)
  (dlet ((fill-column most-positive-fixnum))
    (call-interactively #'fill-paragraph)))

;;;###autoload
(defun my-with-sentence-end-double-space-a (func &rest args)
  (dlet ((sentence-end-double-space t))
    (apply func args)))

;;; my-functions-fill.el ends here

(provide 'my-functions-fill)

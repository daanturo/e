;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-vc-git-get-remote-list ()
  (split-string (my-process-sync->output-string '("git" "remote" "show"))))

;;;###autoload
(defun my-vc-git-transform-remote-url (url)
  "Return URL converted between https ssh formats.
Auto-detect the conversion direction by URL's prefix."
  (let* ((h-pre "https://")
         (s-pre "git@")
         (host "\\([^/]+\\)"))
    (if (string-prefix-p "http" url)
        (replace-regexp-in-string (concat h-pre host "/") (concat s-pre "\\1" ":") url)
      (replace-regexp-in-string (concat s-pre host ":") (concat h-pre "\\1" "/") url))))

;;;###autoload
(defun my-vc-git-convert-remote-url (remote &optional yes)
  "Convert REMOTE between HTTPS and SSH format and set it's url.
With non-nil YES, no confirm."
  (interactive (list (completing-read "Remote: " (my-vc-git-get-remote-list))))
  (let* ((old-url (my-repo-get-git-remote-url remote))
         (new-url (my-vc-git-transform-remote-url old-url))
         (command (concat "git remote set-url " remote " " new-url)))
    (save-window-excursion
      (async-shell-command (read-shell-command "Shell command: " command)))))

(defun my-vc-git-add-remote--build-url (new-user)
  (-let* ((lst0 (my-repo-parse-git-remote-hosting-service-url
                 (my-repo-get-git-remote-url)))
          (lst1 (-replace-at my-repo-git-remote-hosting-service-url-regexp-user-num
                             new-user
                             lst0)))
    (string-join (cdr lst1))))

;;;###autoload
(defun my-vc-git-add-remote (remote-name)
  (interactive (list (read-string "my-vc-git-add-remote : "
                                  (downcase (my-repo-git-get-user)))))
  (-let* ((built-url (my-vc-git-add-remote--build-url remote-name))
          (urls (list built-url
                      (my-vc-git-transform-remote-url built-url)))
          (commands (my-for [url urls]
                      (format "git remote add %s %s ; git fetch %s"
                              remote-name url
                              remote-name)))
          (command-to-run (completing-read "Shell command: "
                                           commands)))
    (async-shell-command (concat "set -x ; " command-to-run))))

;;;###autoload
(defun my-vc-git-copy-or-insert-remote-url (remote &optional insert-flag)
  "Copy REMOTE's url into the clipboard."
  (interactive
   (list (completing-read "Remote: " (my-vc-git-get-remote-list)) current-prefix-arg))
  (my-copy-or-insert (my-repo-get-git-remote-url remote) insert-flag))

;;;###autoload
(progn
  ;; (autoload #'vc-backend "vc-hooks")
  (defvar my-vc-git-tracked--alist '())
  (cl-defun my-vc-git-cached-tracked-p (&optional (file (buffer-file-name)))
    "May not be up-to-date, use other functions such as `vc-backend'
whenever possible."
    ;; (shell-command-to-string (format "git ls-files %s" file))
    ;; (member (vc-backend file) '(git Git))
    (-let* ((file buffer-file-name))
      (with-memoization (alist-get file my-vc-git-tracked--alist)
        (condition-case _err
            (equal
             0
             (call-process "git" nil nil nil "ls-files" "--error-unmatch" file))
          ((file-missing file-error) nil))))))

;;;###autoload
(defun my-revision-and-buffer-filename-no-vc (&optional file-name)
  "Get the version control revision and the part without it of FILE-NAME.
Especially used to parse `vc-revision-other-window''s visited
file name."
  (-let* ((file-name (or file-name
                         buffer-file-name
                         (bound-and-true-p magit-buffer-file-name)))
          (regexp "\\.~\\(.*?\\)~\\'"))
    (list
     (my-regexp-match regexp file-name 1)
     (replace-regexp-in-string regexp "" file-name))))

;;;###autoload
(defun my-vc-git-blame-commit-hash (&optional revision)
  (-let* (((parsed-rev file-name) (my-revision-and-buffer-filename-no-vc))
          (linum (line-number-at-pos)))
    (-->
     (shell-command-to-string
      (format "git blame -l -L %s,%s %s -- %s"
              linum
              linum
              (or revision parsed-rev "")
              file-name))
     (split-string it " ") (car it))))

;;;###autoload
(defun my-vc-git-get-current-branch ()
  (my-process-sync-success-stdout '("git" "symbolic-ref" "--short" "HEAD")))

;;;###autoload
(defun my-vc-git-get-root (&optional dir)
  (dlet ((default-directory (or dir default-directory)))
    (my-process-sync->output-string '("git" "rev-parse" "--show-toplevel"))))

;;;###autoload
(defun my-vc-git-detached-head-p ()
  (-let* ((out
           (process-lines-ignore-status "git"
                                        "rev-parse"
                                        "--abbrev-ref"
                                        "--symbolic-full-name"
                                        "HEAD")))
    (equal out "HEAD")))

;;;###autoload
(defun my-vc-git-get-default-branch (&optional local-only)
  (or
   ;; https://stackoverflow.com/questions/62352646/how-do-you-find-the-default-branch-of-a-remote-git-repository
   (-some-->
       (my-process-sync-success-stdout
        '("git" "rev-parse" "--abbrev-ref" "origin/HEAD"))
     (and (string-match-p "^origin/[^ \t]+" it)
          (string-remove-prefix "origin/" it)))
   ;; https://stackoverflow.com/questions/28666357/how-to-get-default-git-branch
   (-some-->
       (my-process-sync-success-stdout
        '("git" "symbolic-ref" "refs/remotes/origin/HEAD"))
     (and (string-match-p "^refs/remotes/origin/[^ \t]+" it)
          (string-remove-prefix "refs/remotes/origin/" it)))
   ;; single branch, maybe incorrect
   (and local-only
        (-let* ((branches
                 (ignore-errors
                   (process-lines "git" "branch"))))
          (and (= 1 (length branches))
               (--> (cl-first branches) (string-remove-prefix "* " it)))))
   ;; last option as this need network connection, therefore slow
   (and (not local-only)
        (-some-->
            (my-process-sync-success-stdout '("git" "remote" "show" "origin"))
          (s-match "^[ \t]*HEAD branch: \\([^ \t\n\r]+\\)" it)
          (nth 1 it)))))

;;;###autoload
(defun my-vc-git-momentarily-checkout-default-branch
    (repo-dir time &optional dry-run quiet)
  (dlet ((default-directory repo-dir))
    (-let* ((current-branch (my-vc-git-get-current-branch))
            (default-branch (my-vc-git-get-default-branch)))
      (when (not (equal current-branch default-branch))
        (unless quiet
          (my-println🌈
           'my-vc-git-momentarily-checkout-default-branch
           default-directory
           (my-symbols->plist default-branch current-branch)))
        (when dry-run
          (progn
            (call-process "git" nil nil nil "checkout" default-branch)
            (when (< 0 time)
              (sleep-for time))
            (call-process "git" nil nil nil "checkout" current-branch)))))))

;;;###autoload
(defun my-vc-git-dirty-unstaged-or-uncommitted-p (&optional repo-dir)
  (dlet ((default-directory (or repo-dir default-directory)))
    (-let*
        ((exit-code
          ;; https://stackoverflow.com/questions/2657935/checking-for-a-dirty-index-or-untracked-files-with-git
          ;; https://stackoverflow.com/a/2659808/8051562
          ;; git diff-index --quiet HEAD --
          (call-process "git" nil nil nil "diff-index" "--quiet" "HEAD" "--")))
      ;; actually = 1, but make it ≠ 0 to take errors into account
      (not (zerop exit-code)))))

;;;###autoload
(defun my-vc-git-check-ignore-p (file &optional repo-dir)
  (dlet ((default-directory (or repo-dir default-directory)))
    (-let* ((exit-code (call-process "git" nil nil nil "check-ignore" file)))
      (zerop exit-code))))

;;;###autoload
(defun my-vc-git-filter-remove-ignored (file-list &optional repo-dir)
  (dlet ((default-directory (or repo-dir default-directory)))
    ;; (file-error "Doing vfork" "Argument list too long")
    (-let* ((partitions (-partition 8192 file-list))
            (ignored-list
             (-mapcat
              (lambda (sub-file-list)
                (apply #'process-lines-ignore-status
                       "git"
                       "check-ignore"
                       sub-file-list))
              partitions)))
      (-difference file-list ignored-list))))

(defvar my-vc-git-list-non-ignored-files-command-args-list
  `(("git" "ls-files")
    ("git" "ls-files"
     ;; non-tracked, unignored
     "--others"
     ;; exclude ignores
     "--exclude-standard")))

;;;###autoload
(defun my-vc-list-non-ignored-files (&optional repo-dir)
  (dlet ((default-directory (or repo-dir default-directory)))
    (-let* ((out-fn
             (lambda (cmdargs)
               (condition-case _
                   (apply #'process-lines cmdargs)
                 (error nil)))))
      (or (-mapcat out-fn my-vc-git-list-non-ignored-files-command-args-list)))))

(provide 'my-functions-vc)

;; -*- lexical-binding: t; -*-

(defun my-org-elp--open-buffer ()
  (save-selected-window
    (my-with-advice #'doom-modeline-update-buffer-file-name :override #'ignore
      (pop-to-buffer org-elp-buffer-name)
      (my-no-mode-line-mode))))

(advice-add #'org-elp--open-buffer :override #'my-org-elp--open-buffer)

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'my-after-org-elp)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'sgml-mode)

;;;###autoload
(autoload #'sgml-electric-tag-pair-mode "sgml-mode")


(defvar my-before-end-of-smgl-tag-opening-regexp "/?>")

;;;###autoload
(defun my-sgml-close-tag-before-> (n)
  (interactive "P")
  (if n
      (my-emulate-self-insert-command
       (prefix-numeric-value n) last-command-event)
    (-let* ((tag-node (treesit-node-parent (treesit-node-at (point))))
            (tag-node-type (treesit-node-type tag-node))
            (tag-beg (treesit-node-text tag-node t))
            (tag-end
             (cond
              ((member tag-node-type '("start_tag"))
               (-some-->
                   (treesit-search-subtree
                    tag-node
                    (lambda (node) (equal "tag_name" (treesit-node-type node))))
                 (treesit-node-text it) (concat "</" it ">")))
              ((member
                tag-node-type
                '("jsx_opening_element" "jsx_self_closing_element"))
               (-some-->
                   (treesit-search-subtree
                    tag-node
                    (lambda (node) (equal "identifier" (treesit-node-type node))))
                 (treesit-node-text it) (concat "</" it ">")))
              (:else
               (replace-regexp-in-string
                "\\`<\\([^ /]*\\).*>\\'" "</\\1>" tag-beg)))))
      (when (looking-at-p "/>")
        (delete-char 1))
      (forward-char 1)
      (save-excursion (insert tag-end)))))
(defun my-sgml-close-tag--filter (cmd)
  (and (null current-prefix-arg)
       (treesit-parser-list)
       (looking-at-p my-before-end-of-smgl-tag-opening-regexp)
       (member
        ;; (tsc-node-type (tsc-get-parent (tree-sitter-node-at-pos)))
        (-->
         (treesit-node-at (point))
         (treesit-node-parent it)
         (treesit-node-type it))
        '("start_tag" "jsx_opening_element" "jsx_self_closing_element"))
       cmd))

;;;###autoload
(define-minor-mode my-sgml-minor-mode
  "For \"C-c C-.\" commands."
  :keymap
  (append
   (my-for
     [(key . def)
      (my-keymap-bindings sgml-mode-map)
      :when (string-match-p "^C-c C-.$" key)]
     (cons (kbd key) def))
   `(
     ;;
     (,(kbd ">") .
      (menu-item
       ""
       my-sgml-close-tag-before->
       :filter my-sgml-close-tag--filter))
     ;;
     )))

;; NOTE `sgml-get-context' doesn't work in `rjsx-mode' currently, so commands
;; that depend on it will fail

;; "t m" #'mc/mark-sgml-tag-pair ; uses `sgml-get-context'

(my-bind :map 'my-sgml-minor-mode-map "M-l t t" #'sgml-tag)

;;;###autoload
(progn

  (my-accumulate-config-collect (my-setup-web))

  (defun my-setup-web ()

    (with-eval-after-load 'tree-sitter
      (setf (alist-get
             'typescript-mode tree-sitter-major-mode-language-alist)
            'tsx))

    (my-lang-javascript-setup)

    ;; ;; `web-mode' has better auto tag closing support than m?`html-mode'
    ;; (when (fboundp #'web-mode)
    ;;   (add-to-list 'auto-mode-alist `("\\.html\\'" . web-mode)))

    (defconst my-html-tag-major-mode-list
      '(html-mode
        html-ts-mode mhtml-mode web-mode js-mode tsx-ts-mode
        ;; js-jsx-mode rjsx-mode typescript-mode typescript-tsx-mode
        ))
    (defconst my-html-tag-minor-mode-list '(rjsx-minor-mode))

    (defconst my-html-tag-mode-hooks
      (-map
       #'my-mode->hook
       (append
        my-html-tag-major-mode-list my-html-tag-minor-mode-list)))

    (my-safe-add-hook/s
      `(sgml-mode-hook
        css-mode-hook ,@my-html-tag-mode-hooks)
      #'(emmet-mode))

    (my-bind
      :map (-map #'my-mode->map-symbol my-html-tag-major-mode-list)
      :vi '(normal visual)
      "M-l [" #'sgml-skip-tag-backward ;
      "M-l ]" #'sgml-skip-tag-forward ;
      )

    ;; "<" will be too disruptive (it asks a lot) when enabling this
    ;; (setq sgml-quick-keys 'close)

    (my-add-hook/s
      my-html-tag-mode-hooks
      '(
        ;; Auto-edit the closing/opening tag when the other corresponding one
        ;; is edited, but nah, it makes multi-editing problematic
        ;; sgml-electric-tag-pair-mode

        my-sgml-minor-mode ;
        ))

    (add-hook 'my-findutil-find-exclude-patterns "*/node_modules/*")

    ;;
    ))

(provide 'my-functions-sgml)

;; -*- lexical-binding: t; -*-

(require 'dash)

(require 'epkg)

;;;###autoload
(defun my-epkg-get-package-home-page|website (package-name)
  (--> (format "%s" package-name)
       (epkg it)
       ;; homepage's language maybe not as common as repopage
       (or (oref it repopage)
           (oref it homepage))))

;;;###autoload
(defun my-epkg-visit-package-home-page|website-at-point (package-name)
  (interactive (list (tabulated-list-get-id)))
  (my-epkg-visit-package-home-page|website package-name))

;;;###autoload
(defun my-epkg-visit-package-home-page|website (package-name)
  (interactive (list (epkg-read-package "Visit: ")))
  (browse-url
   (my-epkg-get-package-home-page|website package-name)))

;;;###autoload
(defun my-epkg-update-by-delete-and-re-shallow-clone (&optional display)
  "Delete `epkg-repository' and re-clone it shallowly.
DISPLAY: display the process.

Because `epkg-update' takes more disk space overtime, use this
over it to just keep the latest commit."
  (interactive (list t))
  (cond
   ((and (file-exists-p epkg-repository)
         (vc-git-responsible-p epkg-repository)
         (my-vc-git-dirty-unstaged-or-uncommitted-p epkg-repository))
    (user-error
     "`my-epkg-update-by-delete-and-re-shallow-clone': %s isn't clean!"
     epkg-repository))
   (t
    (delete-directory epkg-repository 'recursive)
    (dlet ((default-directory (file-name-parent-directory epkg-repository)))
      (-let* ((buf
               (generate-new-buffer
                (format "*%s*"
                        'my-epkg-update-by-delete-and-re-shallow-clone))))
        (when display
          (switch-to-buffer buf))
        (call-process "git"
                      nil
                      buf
                      t
                      "clone"
                      "--depth=1"
                      epkg-origin-url
                      (expand-file-name epkg-repository)))))))

(provide 'my-functions-epkg)

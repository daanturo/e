;; -*- lexical-binding: t; -*-

(require 'dash)

;; obey popup

;; the default doesn't force key elements to be `keywordp'
(setq inspector-plist-test-function #'my-plist?)

;; `cl-print-to-string-with-limit' doesn't respect this being non-nil and above
;; 50
(setq inspector-truncation-limit nil)

(advice-add
 #'inspector-inspect
 :around #'my-with-switch-to-buffer-other-window->pop-to-buffer-a)

(advice-add
 #'inspector-pprint-inspected-object
 :around #'my-with-switch-to-buffer-other-window->pop-to-buffer-a)

;; (set-popup-rule! "\\`\\*inspector.*?\\*" :height (/ 1.0 3))
(set-popup-rule! "\\`\\*inspector.*\\*" :modeline nil)

(add-hook 'my-evil-or-leader-mode-alist '(inspector-mode my-leader-mode))

(my-bind :map 'inspector-mode-map [remap push-button] #'my-push-button-with-saved-column)
(my-bind :map 'inspector-mode-map "M-t" #'my-inspector->tree-inspector)

(my-windowpopup-set '(derived-mode inspector-mode) nil '((window-height . 0.5)) :no-modeline t)

(provide 'my-after-inspector)

;; Local Variables:
;; no-byte-compile: t
;; End:

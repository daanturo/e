;; -*- lexical-binding: t; -*-

(require 'dash)

(require '00-my-core-macros)

;; TODO: remove when
;; https://github.com/doomemacs/doomemacs/issues/7472#issuecomment-1874647363 is
;; fixed (void-function geiser-activate-implementation)
;;;###autoload
(progn
  (autoload #'geiser-activate-implementation "geiser-autoloads")
  ;; (require 'geiser-autoloads nil t)
  )

(defun my-geiser-repl ()
  (interactive)
  (geiser-repl-switch)
  (buffer-name))

(setq geiser-repl-per-project-p t)

(my-evalu-set-handler
 'scheme-mode
 #'my-geiser-repl
 :region #'geiser-eval-region)

;; force it to obey popup rules
(advice-add #'geiser-repl-switch :around #'my-with-switch-to-buffer-other-window->pop-to-buffer-a)

;; ;;;###autoload
;; (my-accumulate-config-collect
;;   (setq geiser-mode-start-repl-p t))

(with-eval-after-load 'scheme
  (when (and (not (executable-find scheme-program-name))
             (executable-find "guile"))
    (setq scheme-program-name "guile")))

(with-eval-after-load 'geiser-capf
  (when (fboundp #'cape-wrap-silent)
    (my-add-advice/s geiser-capf--capfs :around #'cape-wrap-silent)))

(provide 'my-hook-scheme-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

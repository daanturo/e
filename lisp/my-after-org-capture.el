;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (defun my-org-capture-buffer-match-p (buf-or-name &rest _args)
;;   (-let* ((buf-name (my-buffer-name-string buf-or-name)))
;;     (with-current-buffer buf-or-name
;;       (and org-capture-mode
;;            (string-match-p "^CAPTURE-" buf-name)
;;            (my-derived-mode-p '(org-mode))))))
;; (add-to-list
;;  'display-buffer-alist
;;  `(my-org-capture-buffer-match-p (display-buffer-same-window)))

(advice-add
 #'org-capture-place-template
 :around #'my-pop-to-buffer-display-buffer-same-window-a)

(advice-add #'org-capture-fill-template :around #'my-with-delay-mode-hooks-a)

;;; my-after-org-capture.el ends here

(provide 'my-after-org-capture)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'python)

(defvar my-lang-python-repl-use-jupyter nil)

(my-evalu-set-handler
 'python-mode
 #'my-lang-python-repl
 :region #'python-shell-send-region
 :string #'python-shell-send-string
 :import "\\(^import .*\\|^from [A-Za-z0-9]+ import .*\\)"
 :file #'python-shell-send-file)

;; jupyter.el, while slower, allows displaying inline images
(my-evalu-set-handler
 'python-mode
 #'my-lang-python-jupyter-repl
 :string #'my-evalu-default-send-string
 :predicate (lambda () my-lang-python-repl-use-jupyter))

(add-hook 'my-evalu-get-import-list-functions #'my-lang-python-query-import-statements)

;; (my-safe-add-hook/s '(python-mode-hook) '(importmagic-mode))
(with-eval-after-load 'importmagic
  (setcdr importmagic-mode-map nil))

;;;###autoload
(my-accumulate-config-collect

 ;; Make sexp navigation consistent
 (setq-default python-forward-sexp-function nil)

 (unless (fboundp #'xonsh-mode)
   (add-to-list 'interpreter-mode-alist '("xonsh" . python-mode))
   (add-to-list 'auto-mode-alist '("\\.xsh\\'" . python-mode))
   (add-to-list 'auto-mode-alist '("\\.xonshrc\\'" . python-mode)))
 (my-defer-hook 'python-mode-local-vars-hook)
 (with-eval-after-load 'python
   ;; In the Python REPL, """>>> ["0__dummy_completion__", ...,
   ;; "1__dummy_completion__"]""" is repeated inserted while completing
   ;; https://github.com/minad/corfu/issues/330#issuecomment-1878675663
   (when (fboundp #'cape-wrap-noninterruptible)
     (advice-add
      #'python-shell-completion-at-point
      :around #'my-cape-wrap-noninterruptible-a))
   ;; Debugger entered--Lisp error: (json-parse-error 1 1 1)
   ;;   json-parse-string("\7[\"0__dummy_completion__\", \"1__dummy_completion__\"]" :array-type list)
   ;;   python--parse-json-array("\7[\"0__dummy_completion__\", \"1__dummy_completion__\"]")
   ;;   python-shell-completion-get-completions(#<process Python[some-project]> "")
   ;;   #<subr python-shell-completion-at-point>()
   ;;   apply(#<subr python-shell-completion-at-point> nil)
   (when (fboundp #'cape-wrap-silent)
     (advice-add
      #'python-shell-completion-at-point
      :around #'my-cape-wrap-silent-a))
   ;; TODO: A better way?
   ;; `python-shell-completion-at-point'/`python-shell-completion-native-get-completions'
   ;; hangs after "." is pressed.
   ;; https://github.com/doomemacs/doomemacs/issues/7978
   (setq-default python-shell-completion-native-output-timeout 0.125)))


(setq python-shell-dedicated 'project)
(setq python-indent-guess-indent-offset-verbose nil) ; be less noisy

;; (defvar my-lang-python-virtual-environment-names '(".venv" "venv"))
;; (setq-default python-shell-virtualenv-root ".venv")

;; Disable invasive features
(setq elpy-modules nil)
;; elpy-module-eldoc hard-requires `company'
(with-eval-after-load 'elpy
  (setcdr elpy-mode-map nil))
;; (my-add-advice-once #'elpy-mode :before (lambda (&rest _) (elpy-enable)))
(setq elpy-rpc-virtualenv-path 'system) ; don't install
;; `company-in-string-or-comment' isn't autoloaded
(my-add-advice-once #'elpy-company-backend :before (lambda (&rest _) (require 'company)))

(setq dap-python-debugger 'debugpy)

;; this warning is obtrusive and usually false positive, but we may need to read
;; it once
(my-add-advice-once #'python-shell-completion-native-turn-on-maybe :after
  (lambda (&rest _)
    (add-to-list 'warning-suppress-types
                 '(python python-shell-completion-native-turn-on-maybe))))

(setq doom-modeline-env-python-executable "python") ; python version only

;; Doom's default inserts `python-shell-interpreter'
;; https://github.com/doomemacs/doomemacs/blob/master/modules/editor/file-templates/templates/python-mode/__#L3
(set-file-template! 'python-mode :trigger (lambda () (insert "#!/usr/bin/env python\n")))

(my-context-menu-add-local 'python-mode #'my-lang-python-mark-ipython-cell-context-menu
                           1 'now)

(when (equal
       'python-nav-backward-up-list
       (lookup-key python-mode-map [remap backward-up-list]))
  (my-bind :map '(python-mode-map python-ts-mode-map) [remap backward-up-list] #'my-lang-python-nav-backward-up-list))

;; (add-hook 'python-mode-hook #'my-backtrace)

(with-eval-after-load 'lsp-mode
  (setq lsp-pyright-langserver-command "basedpyright"))

(provide 'my-hook-python-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

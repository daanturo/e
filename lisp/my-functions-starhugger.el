;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-starhugger-toggle-debug ()
  "More verbose logging (and maybe indicators)."
  (interactive)
  (setq starhugger-debug (not starhugger-debug))
  (message "`starhugger-debug' := %s" starhugger-debug))

;;;###autoload
(defun my-starhugger-dwim (arg)
  (interactive "P")
  (cond
   ((member arg '(-))
    (starhugger-undo-accept-suggestion-partially))
   (t
    (call-interactively #'starhugger-trigger-suggestion))))

;;;###autoload
(defun my-starhugger-trigger-suggestion-no-FIM ()
  (interactive)
  (dlet ((starhugger-fill-in-the-middle nil))
    (call-interactively #'starhugger-trigger-suggestion)))

;;;###autoload
(defun my-starhugger-dismiss-suggestion* ()
  (interactive)
  (starhugger-dismiss-suggestion t))


(defun my-starhugger--language-comment ()
  (and comment-start
       (< 0 (length comment-start))
       (-let* ((lang (my-safe-call #'language-id-buffer)))
         (and lang
              (concat comment-start " Language: " lang " " comment-end "\n")))))

(defvar my-starhugger-language-comment--table '())

;;;###autoload
(defun my-starhugger-language-comment ()
  (with-memoization (alist-get major-mode my-starhugger-language-comment--table)
    (my-starhugger--language-comment)))

(defvar-local my-starhugger-file-path-comment--cache nil)

;;;###autoload
(defun my-starhugger-file-path-comment ()
  (with-memoization my-starhugger-file-path-comment--cache
    ;; current file name, relative to project root
    (save-match-data
      (-let* ((relative-path
               (or (and (not buffer-file-name) (buffer-name))
                   (-some-->
                       (project-current)
                     (project-root it)
                     (file-name-parent-directory it)
                     (file-relative-name buffer-file-name it))
                   (file-relative-name
                    buffer-file-name
                    (file-name-parent-directory
                     (file-name-parent-directory buffer-file-name))))))
        (cond
         ((string-match "/starcoder\\b" starhugger-model-id)
          (concat "<filename>" relative-path "\n"))
         ((string-match "/CodeLlama\\b" starhugger-model-id)
          "")
         (t
          'any!))))))

;;;###autoload
(defun my-starhugger--prompt-specify-language-maybe-a (func &rest args)
  (-let* ((prompt (apply func args)))
    (if (< 128 (buffer-size))
        prompt
      (concat (my-starhugger-language-comment) prompt))))

;; https://huggingface.co/bigcode/starcoder/discussions/40#646f8808bc42f4b0023024c7
;; https://github.com/bigcode-project/bigcode-evaluation-harness/blob/04b34934e101bdcfc4a28b31ac0643176abc9aaf/lm_eval/utils.py#L99

;; <fim_prefix><filename>solutions/solution_1.ext
;; ...<fim_suffix>...

;;;###autoload
(defun my-starhugger--prompt-build-components-filename-a (func &rest args)
  (-let* ((cmt (my-starhugger-file-path-comment)))
    (cond
     ;; explicit empty string: don't interfere
     ((equal cmt "")
      (apply func args))
     (t
      (-let* (([pre suf] (apply func args))
              (cmt-1
               (if (stringp cmt)
                   ;; a non-empty string: append it
                   cmt
                 ;; non-string: try to come up with something!
                 nil)))
        (vector
         (concat
          (or cmt-1
              (and (< 128 (buffer-size)) (my-starhugger-language-comment)))
          pre)
         suf))))))

;;;###autoload
(progn

  (defvar-local my-starhugger-auto-mode-disable nil)

  (defun my-starhugger-turn-on-auto-mode ()
    (when (and (not my-starhugger-auto-mode-disable)
               (apply #'derived-mode-p my-edit-modes)
               (not
                (-some
                 #'my-bounded-value
                 '(
                   ;; only annoying in .git/COMMIT_EDITMSG
                   with-editor-mode
                   ;;
                   ))))
      (starhugger-auto-mode)))

  (define-globalized-minor-mode my-starhugger-global-auto-mode
    starhugger-auto-mode
    my-starhugger-turn-on-auto-mode
    (if my-starhugger-global-auto-mode
        (progn
          (when (and (called-interactively-p t)
                     (not (bound-and-true-p starhugger-api-token)))
            (my-starhugger-lazy-setup)))
      (progn))))

(defvar-local my-starhugger-inlining-mode--blamer-mode-state nil)
(defvar-local blamer-mode nil)

;;;###autoload
(defun my-starhugger-inlining-mode-h ()
  (if starhugger-inlining-mode
      (progn
        (add-hook
         'my-evil-force-normal-state-hook #'my-starhugger-dismiss-suggestion*
         nil t)
        (setq my-starhugger-inlining-mode--blamer-mode-state blamer-mode)
        (when my-starhugger-inlining-mode--blamer-mode-state
          (blamer-mode 0)))
    (progn
      (when (and my-starhugger-inlining-mode--blamer-mode-state
                 (not blamer-mode))
        (blamer-mode 1)))))

;;;###autoload
(defun my-starhugger-test-alternative-model-availbility-then-set
    (url &optional callback quiet)
  "Use URL for inference if it's available.
CALLBACK is called with no arguments. non-nil QUIET: don't echo."
  (dlet ((url-request-method "POST")
         (url-request-data
          (json-serialize
           '((inputs . "Hello")
             (parameters . ((max_new_tokens . 1) (max_time . 0))))))
         (url-request-extra-headers
          `(("Content-Type" . "application/json")
            ,@(-some-->
                  (starhugger--get-api-token-as-string)
                `(("Authorization" . ,(format "Bearer %s" it)))))))
    (url-retrieve
     url
     (lambda (_status)
       (dlet ((inhibit-message (or quiet inhibit-message)))
         (-let* ((content
                  (and url-http-end-of-headers
                       (-->
                        (buffer-substring
                         url-http-end-of-headers (point-max))
                        (string-trim it))))
                 (parsed-obj (json-parse-string content :object-type 'alist)))
           (message "`my-starhugger': %S returned: %s" url content)
           (unless (and (listp parsed-obj) (assoc 'error parsed-obj))
             (setq starhugger-model-api-endpoint-url url)
             (message "Set `starhugger-model-api-endpoint-url' to %S" url))
           (when callback
             (funcall callback)))))
     nil t)))

;;; my-functions-starhugger.el ends here

(provide 'my-functions-starhugger)

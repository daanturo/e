;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; just default to "attachment", while keeping other configs
(setq mml-content-disposition-alist
      (delete
       '(text (rtf . "attachment") (t . "inline"))
       mml-content-disposition-alist))

;;; my-after-mml.el ends here

(provide 'my-after-mml)

;; Local Variables:
;; no-byte-compile: t
;; End:

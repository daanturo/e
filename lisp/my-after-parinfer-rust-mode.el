;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; let Nix package manager handle updates
(with-eval-after-load 'parinfer-rust-mode
  (defvar my-parinfer-rust-lib-name
    (file-name-nondirectory parinfer-rust-library))
  (defvar my-parinfer-rust-nix-library
    (file-name-concat "/nix/var/nix/profiles/default/lib/"
                      ;; my-parinfer-rust-lib-name
                      "libparinfer_rust.so"))
  (cond
   ((file-exists-p my-parinfer-rust-nix-library)
    (setq parinfer-rust-library-directory "/nix/var/nix/profiles/default/lib/")
    (setq parinfer-rust-library my-parinfer-rust-nix-library))
   (:else
    (setq parinfer-rust-library-directory
          (file-name-concat my-local-share-emacs-dir/ "parinfer-rust/"))
    (setq parinfer-rust-library
          (file-name-concat parinfer-rust-library-directory
                            my-parinfer-rust-lib-name))
    (setq parinfer-rust-auto-download t))))

;; automatically disable `parinfer-rust-troublesome-modes'
(setq parinfer-rust-disable-troublesome-modes t)

;; ;; don't change the whole buffer or ask when just visiting
;; (advice-add #'parinfer-rust--check-for-indentation :override #'ignore)

;; (setq parinfer-rust-check-before-enable nil)

;; auto deny buffer-modifying prompts, if forceful modifying is needed call
;; `my-parinfer-rust-mode-force-no-check'
(advice-add
 #'parinfer-rust--check-for-indentation
 :around #'my-parinfer-with-yes-or-no-p-auto-no-a)

(advice-add
 #'parinfer-rust--check-version
 :around #'my-with-yes-or-no-p-auto-no-a)

;; TODO report? :

;; lacking features without this: auto-close "", `'; wrap a region with typed bracket
(my-delete-in-list! 'parinfer-rust-troublesome-modes 'electric-pair-mode)
;; but with `electric-pair-mode' enabled, auto-wrap isn't available

;; attempt to fix the above, this also allow to insert a non-auto-wrapping
;; bracket by supplying 1 as the prefix argument
(add-hook
 'my-electric-pair-post-self-insert-inhibit-functions
 #'my-parinfer-rust-electric-pair-post-self-insert-inhibit-h)

(add-to-list 'parinfer-rust-treat-command-as '(open-line . "paren"))
;; (add-to-list 'parinfer-rust-treat-command-as '(raise-sexp . "paren"))

(dolist (cmd
         `(,(keymap-global-lookup "C-v")
           my-pair-auto-wrap-curly-bracket
           my-pair-auto-wrap-round-bracket-paren
           my-pair-auto-wrap-square-bracket
           my-tempel-expand-prev-sexp-to-point
           my-tempel-expand-word
           parinfer-rust-treat-command-as
           tempel-expand
           tempel-insert
           yank-media))
  (add-to-list 'parinfer-rust-treat-command-as `(,cmd . "paren")))

;; TODO: report that if the file contains "^M" ("C-q C-m", carriage return),
;; `parinfer-rust-mode' inserts it at every line end!

;; TODO: report that if a form has a emoji, pressing space at list end doesn't
;; insert space

;; imcompatibility with `iedit': iedit abruptly quits when whole
;; symbol is deleted
(my-when-mode0-turn-off||disable-mode1 '(iedit-mode) '(parinfer-rust-mode))

;;;###autoload
(progn
  (defun my-setup-parinfer-rust ()
    (declare)
    ;; parinfer-rust-mode's source code uses this, too tired to be asked
    (add-to-list
     'safe-local-variable-values
     '(lisp-indent-function . common-lisp-indent-function))
    (when (fboundp #'parinfer-rust-mode)
      (my-add-hook/s
        `(,@(-map #'my-mode->hook my-lisp-major-modes))
        (list
         (my-make-hook-function-defer-until-visible
          #'parinfer-rust-mode))))))

;; (advice-add
;;  #'parinfer-rust--execute
;;  :around #'my-parinfer-rust--execute--inhibit-maybe-a)
;; (add-hook
;;  'my-parinfer-rust-execute-inhibit-variable-list 'completion-in-region-mode)

;; Some completion engines such as the snippet ones may give multi-line
;; completion, parinfer may mess them up
(add-hook 'completion-in-region-mode-hook #'my-parinfer-disable-temporary)

;;; my-after-parinfer-rust-mode.el ends here

(provide 'my-after-parinfer-rust-mode)

;; Local Variables:
;; no-byte-compile: t
;; End:

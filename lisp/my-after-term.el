;; -*- lexical-binding: t; -*-

(require 'term)

(my-bind :map 'term-raw-map
  "C-S-v" #'term-paste
  [menu] #'execute-extended-command)

(provide 'my-after-term)

;; Local Variables:
;; no-byte-compile: t
;; End:

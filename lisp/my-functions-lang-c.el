;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-lang-c-comment-out-microsoft-windows-only-stuff ()
  (interactive)
  (save-excursion
    ;; (goto-char (point-min))
    (my-regexp-replace-in-buffer "^\\(#include <conio\\.h>\\)" "/* \\1 */")
    (my-regexp-replace-in-buffer "\s\\(getch();\\)" "/* \\1 */")))

(defcustom my-execu-compiler-options-cc-c-c++
  '("-g" "-Wall" "-Wextra" ; debugging
    ;; "-Werror"
    ;; "-fsanitize=address" ; detect memory leaks

    ("<omp.h>" "-fopenmp") ; OpenMP
    ("<GL/glut.h>" "-lGL -lGLU -lglut") ; OpenGL
    ("<math.h>" "-lm") ; C math, needed when arguments are not literals
    ("<pthread.h>" "-pthread") ; POSIX Threads

    ;;
    )
  "Switches to be passed to the C/C++ compiler.
Each element can be string, then it will be added
unconditionally; or a list of strings whose first element is a
regexp and the rest are options, the options will be added when
the regexp in found in includes.")

(defun my-execu-cmd-lang-cc-c-c++--get-options ()
  (save-match-data
    (-let* ((grepped-includes (my-rgrep-grep->string "^#include\\b")))
      (-mapcat
       (lambda (elem)
         (cond
          ((stringp elem)
           (list elem))
          ((listp elem)
           (-let* (((regexp . options) elem))
             (if (string-match regexp grepped-includes)
                 options
               '())))
          (t
           '())))
       my-execu-compiler-options-cc-c-c++))))

;;;###autoload
(defun my-execu-cmd-lang-cc-c-c++ (file-name)
  (interactive "f")
  (-let* ((compiler (my-get-alist-major-mode my-execu-compiler-alist))
          (options (my-execu-cmd-lang-cc-c-c++--get-options))
          (executable (file-name-sans-extension file-name)))
    (format "%s %s %S -o %S && ./%S"
            compiler
            (string-join options " ")
            file-name
            executable
            executable)))


;;; my-functions-lang-c.el ends here

(provide 'my-functions-lang-c)

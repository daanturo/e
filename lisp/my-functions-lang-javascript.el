;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;; Commands

(defconst my-lang-javascript-typescript-repl-name "TypeScript REPL")

(defvar my-lang-javascript-typescript-repl-command "ts-node")

(define-derived-mode
  my-lang-javascript-typescript-repl-mode
  comint-mode
  my-lang-javascript-typescript-repl-name
  "Major mode for TypeScript REPL."
  :syntax-table ;; typescript-mode-syntax-table
  js-mode-syntax-table
  (setq comint-input-ignoredups t)
  (setq comint-process-echoes nil)
  (ansi-color-for-comint-mode-on))

;; (when (executable-find "pnpm")
;;   (setq lsp-clients-angular-node-get-prefix-command "pnpm root -g"))

;; (setq lsp-clients-typescript-server-args '("--stdio" "--jsx"))

;;;###autoload
(defun my-lang-javascript-jsx-dwim-/-menu-item-filter-treesit (cmd)

  ;; an ERROR node: go up until a node has (... "<" named-node ... ) children

  ;; jsx_self_closing_element but its last child's text isn't "/>"

  (-let* ((has-<-pred
           (lambda (node)
             (and node
                  (-some
                   (lambda (idx)
                     (and (equal
                           "<"
                           (treesit-node-text (treesit-node-child node idx)))
                          (treesit-node-check
                           (treesit-node-child node (+ 1 idx)) 'named)))
                   (-iota (max (- (treesit-node-child-count node) 1) 0)))))))
    (and (named-let
             recur
             ((node
               (treesit-node-at
                (save-excursion
                  (skip-chars-backward " \t\n\r")
                  (point))))
              (has-< nil))
           (-let* ((typ (treesit-node-type node))
                   (has-<* (funcall has-<-pred node)))
             (cond
              ((null node)
               nil)
              ((and (member typ '("jsx_self_closing_element"))
                    (-let* ((last-child (treesit-node-child node -1)))
                      (and (member (treesit-node-type last-child) '("/>"))
                           (or (string-empty-p (treesit-node-text last-child))
                               (my-treesit-some-child
                                (lambda (child)
                                  (and (member
                                        (treesit-node-type child) '("ERROR"))
                                       (equal "<" (treesit-node-text child))))
                                node)))))
               t)
              ((and (member typ '("jsx_opening_element"))
                    (my-treesit-some-child
                     (lambda (child)
                       (-let* ((typ (treesit-node-type child))
                               (txt (treesit-node-text child)))
                         (or (and (member typ '("type_arguments"))
                                  (string-match-p "^<.*[^>]$" txt))
                             (and (member typ '("ERROR")) (equal "<" txt)))))
                     node))
               t)
              ((and (member typ '("ERROR")) (or has-< has-<*))
               t)
              (:else
               (recur (treesit-node-parent node) (or has-< has-<*))))))
         cmd)))

;;;###autoload
(defun my-lang-javascript-jsx-dwim-/-insert ()
  (interactive)
  (insert "/>"))


;;; Setup functions

;;;###autoload
(defun my-lang-javascript-dispatch-major-mode ()
  (my-lang-javascript-set-auto-mode-alist
   #'my-lang-javascript-dispatch-major-mode
   t)
  (cond
   ((my-treesit-language-available-p 'tsx)
    (my-lang-javascript-set-auto-mode-alist 'tsx-ts-mode)
    (with-eval-after-load 'typescript-ts-mode
      (remove-hook 'auto-mode-alist '("\\.ts\\'" . typescript-ts-mode)))
    (tsx-ts-mode))
   ;; ((my-treesit-language-available-p 'typescript)
   ;;  (my-lang-javascript-set-auto-mode-alist 'typescript-ts-mode)
   ;;  (typescript-ts-mode))
   ;; ((my-treesit-language-available-p 'javascript)
   ;;  (my-lang-javascript-set-auto-mode-alist 'js-ts-mode)
   ;;  (js-ts-mode))
   ;; ((fboundp #'js-jsx-mode)
   ;;  (my-lang-javascript-set-auto-mode-alist 'js-jsx-mode)
   ;;  (js-jsx-mode))
   (t
    (my-lang-javascript-set-auto-mode-alist 'js-mode)
    (js-mode))))

;;;###autoload
(progn

  (defun my-lang-javascript-set-auto-mode-alist (major-m &optional remove)
    (-let* ((fn
             (if remove
                 #'remove-hook
               #'add-to-list)))
      (funcall fn 'auto-mode-alist `("\\.js\\'" . ,major-m))
      (funcall fn 'auto-mode-alist `("\\.jsx\\'" . ,major-m))
      (funcall fn 'auto-mode-alist `("\\.mjs\\'" . ,major-m))
      (funcall fn 'auto-mode-alist `("\\.ts\\'" . ,major-m))
      (funcall fn 'auto-mode-alist `("\\.tsx\\'" . ,major-m)))
    ;;
    )

  (defun my-lang-javascript-setup ()
    "Setup Javascript."

    ;; why `my-lang-javascript-dispatch-major-mode'? Checking for grammar
    ;; availability early is expensive, when already using `auto-mode-alist' to
    ;; activate a major mode (from `js-mode' up to `tsx-ts-mode') without a
    ;; dispatcher one, switching to another is hard

    (my-lang-javascript-set-auto-mode-alist
     #'my-lang-javascript-dispatch-major-mode)

    ;; (unless (fboundp #'rjsx-mode)
    ;;   (defalias #'rjsx-mode #'js-mode))

    ;; Doom's `typescript-tsx-mode' can be used, but
    ;; `lsp-language-id-configuration' doesn't recognize it (non-file buffer?).
    ;; On the other hand Emacs>=29 + tree-sitter handles this natively.
    (defalias #'typescript-tsx-mode #'tsx-ts-mode)

    ;;
    )

  ;;
  )

;;; my-functions-lang-javascript.el ends here

(provide 'my-functions-lang-javascript)

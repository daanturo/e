;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-speech-backend-piper-dir nil)
(defvar my-speech-backend-piper-model nil)
(defvar my-speech-backend-piper-model-dot-extension ".onnx")

(defvar my-speech-speak-text--process-list '())

;;;###autoload
(defun my-speech-speak-text (text &optional callback)
  (interactive (list
                (read-string (format "%s: " 'my-speech-speak-text)
                             (cond
                              ((use-region-p)
                               (my-region-string-maybe t))
                              (:else
                               (thing-at-point 'word t))))))
  (cl-assert (executable-find "piper"))
  (cl-assert (executable-find "aplay"))
  (cl-assert my-speech-backend-piper-dir)
  (cl-assert my-speech-backend-piper-model)
  (-let*
      ((model-path
        (file-name-concat my-speech-backend-piper-dir
                          (concat
                           my-speech-backend-piper-model
                           my-speech-backend-piper-model-dot-extension)))
       (cmd
        (concat
         ;; If model doesn't exist, download it synchronously first
         (cond
          ((file-exists-p model-path)
           "")
          (t
           (format "piper --download-dir %S --model %S --output-raw ;"
                   my-speech-backend-piper-dir
                   my-speech-backend-piper-model)))
         (format "printf %%s %S" text)
         " | "
         ;; https://github.com/rhasspy/piper?tab=readme-ov-file#streaming-audio
         (format "piper --model %S --output-raw" model-path)
         " | "
         "aplay -r 22050 -f S16_LE -t raw -"))
       (callback-1
        (lambda (stdout plist &rest cb-args)
          (when (/= 0 (plist-get plist :exit))
            (message "`my-speech-speak-text'\nstdout:\n\n%s\n\nstderr:\n\n%s"
                     (plist-get plist :out)
                     (plist-get plist :err)))
          (when callback
            (apply callback stdout plist cb-args))))
       (proc (my-process-async cmd callback-1)))
    (dlet ((inhibit-message t))
      (message "`my-speech-speak-text' command:\n%s" cmd))
    (push proc my-speech-speak-text--process-list)
    cmd))

;;;###autoload
(defun my-speech-stop-all-speaking-processes ()
  (interactive)
  (mapc #'my-process-terminate my-speech-speak-text--process-list)
  (setq my-speech-speak-text--process-list '()))

;;; my-functions-speech.el ends here

(provide 'my-functions-speech)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-browser-firefox-extra-home-directory-list '()
  "Like ~/.var/app/org.mozilla.firefox/ .")

;;;###autoload
(defun my-browser-firefox-profile-directory-list ()
  (-->
   (--mapcat (-let* ((parent-dir (file-name-concat it ".mozilla/firefox/")))
               (-->
                (directory-files parent-dir t "^[a-z0-9]+\\.")
                (-filter #'file-directory-p it)))
             (append
              '("~" "~/.var/app/org.mozilla.firefox")
              my-browser-firefox-extra-home-directory-list))
   (-map #'abbreviate-file-name it)))

;;;###autoload
(defun my-capf-extra-yt-dl ()
  (-let* ((beg
           (save-excursion
             (backward-sexp 1)
             (point)))
          (end (point))
          (thing (buffer-substring-no-properties beg end)))
    (cond
     ;; complete "--cookies-from-browser"'s profile
     ((member thing '("firefox:"))
      (-let* ((profile-dirs (my-browser-firefox-profile-directory-list))
              (table
               (--map (-let* ((name (file-name-nondirectory it)))
                        (cons (concat thing name) (my-propertize-annotation it)))
                      profile-dirs)))
        (list
         beg
         end
         (-map #'car table)
         :annotation-function (lambda (cand) (cdr (assoc cand table)))))))))


;;; my-functions-media.el ends here

(provide 'my-functions-media)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)
(require 'async)


;; Cleaning strategy of a .tex template file:
;; - Asynchronous process is terminated: clean all of that file
;; - Unable to produce preview: clean auxiliary files and image file (when produced)
;; - Successful production: clean auxiliary files of that file (cleaning all aggressively may cause race condition?)

(defcustom my-preview-latex-idle-delay 0.5 "")

(defvar my-preview-latex--idle-timer nil)
(defun my-preview-latex--idle-wait ()
  (unless my-preview-latex--idle-timer
    (setq my-preview-latex--idle-timer
          (run-with-idle-timer my-preview-latex-idle-delay
                               nil
                               #'my-preview-latex--idle-now))))
(defun my-preview-latex--idle-now ()
  (cancel-timer my-preview-latex--idle-timer)
  (setq my-preview-latex--idle-timer nil)
  (when (and
         ;; Still in a valid buffer?
         my-auto-preview-latex-mode
         (not (buffer-modified-p))
         ;; Don't push the preview that was invoked interactively out
         (not (my-preview-latex--showing-interactive-preview-p)))
    (my-preview-latex-math)))

;;;###autoload
(define-minor-mode my-auto-preview-latex-mode
  "Automate `my-preview-latex-math' when idle."
  :global nil
  (if my-auto-preview-latex-mode
      (progn
        (add-hook 'post-command-hook #'my-preview-latex--idle-wait nil 'local))
    (progn
      (remove-hook 'post-command-hook #'my-preview-latex--idle-wait 'local))))

(defvar my-preview-latex-dpi 384
  "DPI of the preview image when idle.")
(defvar my-preview-latex-interactive-dpi 1024
  "DPI of the preview image when call `my-preview-latex-math'
interactively.")
(defun my-preview-latex--image-file (base-file)
  (format "%s-%s.png" base-file my-preview-latex-dpi))
(defun my-preview-latex--showing-interactive-preview-p ()
  (--> (window-list)
       (-map 'window-buffer it)
       (-keep 'buffer-file-name it)
       (-some
        (-cut string-suffix-p (format "-%s.png" my-preview-latex-interactive-dpi) <>)
        it)))

(defun my-preview-latex--error (&optional expr latex-file)
  (user-error "Unable to produce preview for %s, latex file: %s."
              expr latex-file))

;;;###autoload
(defun my-preview-latex-math (&optional focus synchronous)
  "Popup a preview of the grabbed LaTeX expression in region or at point.
With non-nil FOCUS, focus the popup. Non-default SYNCHRONOUS
means that this call is blocking."
  (interactive (list current-prefix-arg nil))
  (if-let ((expr (my-latex-math-expression-in-region-or-cached-at-point)))
      (dlet ((my-preview-latex-dpi (if (called-interactively-p t)
                                       my-preview-latex-interactive-dpi
                                     my-preview-latex-dpi)))
        (let* ((base-file (my-preview-latex--expr->file expr))
               (latex-file (concat base-file ".tex"))
               (image-file (my-preview-latex--image-file base-file)))
          ;; Successfully produced the image
          (cond
           ((file-readable-p image-file)
            (my-preview-latex-popup image-file focus)
            image-file)
           ;; .tex file but no images => error
           ((and (file-readable-p latex-file)
                 (seq-empty-p (file-expand-wildcards (concat base-file "*.png"))))
            (when (called-interactively-p t)
              (my-preview-latex--error expr latex-file))
            'error)
           ;; make the image
           (t
            (my-preview-latex-make-image
             expr
             (not synchronous)
             (lambda ()
               (my-preview-latex--async-delete-all-of base-file '("tex" "png"))
               (if (file-readable-p image-file)
                   (my-preview-latex-popup image-file focus)
                 (when (called-interactively-p t)
                   (my-preview-latex--error expr latex-file)))))))))
    (when (called-interactively-p t) (user-error "Can't find expression."))))

(defun my-preview-latex-make-image (expression &optional async post-img-fn)
  "Produce an preview image file from EXPRESSION.
POST-IMG-FN is called after creating it."
  ;; Don't proceed when a process is still handling EXPRESSION
  (unless (and (process-live-p (plist-get my-preview-latex--running :process))
               (equal (plist-get my-preview-latex--running :expression)
                      expression))
    (let* ((base-file (my-preview-latex--prepare-base-file expression))
           (dir (file-name-parent-directory base-file))
           (image-file  (my-preview-latex--image-file base-file))
           (orig-frame (selected-frame))
           (sentinel (lambda (&rest _)
                       ;; We may have wandered to another frame
                       (with-selected-frame orig-frame
                         (when post-img-fn
                           (funcall post-img-fn))))))
      (dlet ((default-directory dir))
        (let ((process (my-preview-latex--process base-file async)))
          ;; The async process may have finished already
          (if (process-live-p process)
              (set-process-sentinel process sentinel)
            (funcall sentinel))
          (setq my-preview-latex--running
                (my-symbols->plist process expression base-file))))
      image-file)))

(defvar my-preview-latex--running '(:process nil :expression nil :base-file nil)
  "Information of the running process that is making the preview,
it may have finished.")
(defun my-preview-latex--async-process (base-file cmd)
  (my-preview-latex--terminate-process-maybe)
  (start-process-shell-command "my-preview-latex" nil cmd))
(defun my-preview-latex--process (base-file async)
  (let* ((cmd (my-preview-latex-command base-file)))
    (if async
        (my-preview-latex--async-process base-file cmd)
      (call-process-shell-command cmd))))

(defun my-preview-latex--terminate-process-maybe ()
  (cl-destructuring-bind (&key process base-file &allow-other-keys)
      my-preview-latex--running
    (when (process-live-p process)
      (set-process-sentinel process nil)
      (signal-process process 'SIGTERM)
      (my-preview-latex--async-delete-all-of base-file))))

;;;###autoload
(defun my-preview-latex-command (base-file)
  "From BASE-FILE, generate a command that will be executed to produce the preview.
NOTE: Under some erroneous circumstances, \"-shell-escape\" may
prevent non-interactive modes, making the whole process hang."
  (let ((image-path (my-preview-latex--image-file base-file)))
    (my-s$ "\
latexmk -f --interaction=nonstopmode -lualatex -draftmode -dvi \
${base-file}.tex ;
dvipng -T tight \
-D ${my-preview-latex-dpi} ${base-file}.dvi \
-o ${image-path}")))

(defun my-preview-latex--prepare-base-file (expression)
  "Ensure EXPRESSION is written in a .tex file."
  (let* ((file (my-preview-latex--expr->file expression))
         (latex-file (concat file ".tex")))
    (unless (file-readable-p latex-file)
      (let ((template (my-preview-latex-template expression)))
        (f-write template 'utf-8 latex-file)))
    file))

(defun my-preview-latex-popup (image-file &optional focus)
  "Popup IMAGE-FILE, with non-nil FOCUS, focus the window."
  (my-preview-latex--clean-buffers-maybe)
  (with-current-buffer
      (my-find-file-as-hidden-popup image-file "my-preview-latex" focus)
    (add-to-list 'my-preview-latex--preview-buffers
                 (current-buffer))
    (when (equal 'dark (frame-parameter nil 'background-mode))
      (my-invert-background-mode))
    (image-transform-fit-both)
    (my-no-mode-line-mode)))

(defvar my-preview-latex--preview-buffers '())
(defun my-preview-latex--clean-buffers-maybe ()
  "Keep the number of preview buffers reasonable, but not too few
so `find-file' must work repeatedly."
  (when (length> my-preview-latex--preview-buffers 8)
    (mapc (-andfn #'bufferp #'kill-buffer) my-preview-latex--preview-buffers)
    (setq my-preview-latex--preview-buffers '())))

;; Doom-only
(set-popup-rule! "^ \\*my-preview-latex"
  :height (/ 1.0 16) :ttl 8)

;;;###autoload
(defun my-preview-latex-template (expression)
  "Return a complete LaTeX document string from EXPRESSION."
  ;; \\documentclass[convert={density=%s}]{standalone}
  ;; \\usepackage{xcolor} \\pagecolor{white}
  (format "\
\\documentclass[]{standalone}
\\usepackage{mathtools}
\\begin{document}
%s
\\end{document}
"
          expression))

(defvar my-preview-latex-temp-dir (my-temp-dir "emacs-preview-latex"))

(defun my-preview-latex--expr->file (expression &optional ext)
  "Deterministic by EXPRESSION."
  ;; I hope there won't be hash collisions, Namu Amida Butsu
  (--> (secure-hash 'sha512 expression)
       (file-name-concat my-preview-latex-temp-dir it)
       (if ext
           (concat it ext)
         it)))

(defun my-preview-latex-clear-cache ()
  "Delete all produced previews."
  (interactive)
  (my-preview-latex--terminate-process-maybe)
  (dolist (f (f-entries (file-name-concat (my-temp-dir) my-preview-latex-temp-dir)))
    (if (file-directory-p f)
        (delete-directory f t)
      (delete-file f))))

(defun my-preview-latex--async-delete-all-of (file &optional excluded-extensions)
  "Delete all files who share their base name with FILE.
With non-nil EXCLUDED-EXTENSIONS, skip files with those."
  (async-start
   (lambda ()
     (let* ((base (file-name-base file))
            (files (file-expand-wildcards (concat base "*") 'full)))
       (mapc 'delete-file
             (seq-remove (lambda (file)
                           (seq-some (lambda (ext)
                                       (string-suffix-p (concat "." ext)
                                                        file))
                                     excluded-extensions))
                         files))))))


;;; my-preview-latex.el ends here

(provide 'my-preview-latex)

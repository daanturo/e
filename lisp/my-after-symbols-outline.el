;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(setq symbols-outline-fetch-fn #'symbols-outline-lsp-fetch)

(setq symbols-outline-window-position 'left)


;;; after-symbols-outline.el ends here

(provide 'my-after-symbols-outline)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'request)

;;; Ollama

(defvar my-llm-ollama-serve-buffer (format "*%s*" 'my-llm-ollama-serve))

;;;###autoload
(defun my-llm-ollama-serve ()
  (interactive)
  (start-process "my-llm-ollama-serve" my-llm-ollama-serve-buffer "ollama" "serve"))

;;;###autoload
(defun my-llm-ollama-ensure-serve (&optional ask)
  (when (and (not (my-net-check-port-open-p 11434))
             (or (not ask) (y-or-n-p "ollama serve ?")))
    (my-llm-ollama-serve)))

(defvar my-llm-ollama-default-quantize-level "iq4_xs"
  "Or q5_k_s, q4_k_s.
https://www.reddit.com/r/LocalLLaMA/comments/1etzews/comment/likahvu/

IQ4_XS seems good:
https://www.reddit.com/r/LocalLLaMA/comments/1flqwzw/qwen25_14b_gguf_quantization_evaluation_results/
")

;;;###autoload
(defun my-llm-ollama-quick-create-from ()
  (interactive)
  (-let* ((from
           (save-match-data
             (my-findutil-completing-read-file-recursively
              "Ollama create model from file or directory: "
              default-directory
              :pred
              (lambda (str)
                (not
                 (or (string-match "\\bModelfile\\b" str)
                     (string-match "\\.\\(md\\|json\\)$" str)
                     (string-match "/\\." str)))))))
          ;;

          (model-name
           (file-name-nondirectory (directory-file-name from)))
          (modelfile-content
           (format "FROM ./%s" (file-relative-name from)))
          (modelfile-name
           (format "Modelfile-%s-%s"
                   model-name
                   (my-ISO-time nil nil t)))
          (confirm-key "C-c C-c"))
    (find-file modelfile-name)
    (when (my-buffer-emptyish-p)
      (insert modelfile-content))
    (message "Press %s to continue" confirm-key)
    (keymap-local-set
     confirm-key
     (lambda ()
       (:documentation (format "Ollama create."))
       (interactive)
       (save-buffer 0)
       ;; https://github.com/ollama/ollama/blob/main/docs/import.md#supported-quantizations
       (-let* ((cmd
                (-->
                 (format
                  "%s ollama create %s -f %s --quantize %s"
                  ;; "no space left on device", avoid using tmpfs 
                  "TMPDIR=/var/tmp/"
                  model-name
                  modelfile-name
                  my-llm-ollama-default-quantize-level)
                 (read-shell-command "Run: " it)))
               (buf-name
                (format "%s %s" 'my-llm-ollama-quick-create-from cmd)))
         (my-ansi-term-other-window cmd buf-name))))))

;;; Other backends

(defvar my-llm-google-default-model nil)
(defvar my-llm-google-api-key (getenv "GEMINI_API_KEY"))

(defvar my-llm-google-url-fn
  (lambda (model api-key)
    (format
     "https://generativelanguage.googleapis.com/v1beta/models/%s:generateContent?key=%s"
     model api-key)))

;;;###autoload
(cl-defun my-llm-google-request (data-or-prompt callback &key model api-key url success error)
  "
Doc: https://ai.google.dev/gemini-api/docs/vision?lang=rest#base64-encoded."
  (-let* ((model (or model my-llm-google-default-model))
          (api-key (or api-key my-llm-google-api-key))
          (url (or url (funcall my-llm-google-url-fn model api-key)))
          (data
           (cond
            ((stringp data-or-prompt)
             (json-serialize
              `((contents . [((parts . [((text . ,data-or-prompt))]))]))))
            (:else
             data-or-prompt))))
    (request
      url
      :type "POST"
      :data data
      :headers '(("Content-Type" . "application/json"))
      :parser #'my-json-parse-buffer
      :success success
      :error error
      :complete callback)))

;;; LLM Interaction

;;;###autoload
(progn
  (defun my-setup-llm-interaction ()

    (my-setup-ellama)

    (when-let* ((model-s (getenv "MY_LLM_S"))
                (model-l (getenv "MY_LLM_L"))
                (model-emb (getenv "MY_LLM_EMB")))

      (with-eval-after-load 'gptel
        (-let* ((be
                 (gptel-make-ollama
                     (format "Ollama")
                   :stream t
                   :models (list model-l model-s))))
          (setq gptel-model model-l)
          (setq gptel-backend be)))

      (with-eval-after-load 'ellama
        (setq ellama-provider (make-llm-ollama :chat-model model-l)))

      (with-eval-after-load 'elisa
        (require 'llm-ollama)
        (setq elisa-chat-provider
              (make-llm-ollama :chat-model model-l :embedding-model model-emb))
        (setq elisa-embeddings-provider
              (make-llm-ollama :embedding-model model-emb))
        (setq elisa-reranker-enabled t)))

    (my-setup-minuet)))



;;; my-functions-llm.el ends here

(provide 'my-functions-llm)

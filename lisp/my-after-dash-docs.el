;; -*- lexical-binding: t; -*-

(require 'dash-docs)

;; NOTE install https://github.com/sunaku/dasht to manage docsets!

(let ((fn (lambda (&rest _)
            (mapc #'custom-reevaluate-setting
                  '(dash-docs-docsets-path dash-docs-browser-func)))))
  (funcall fn)
  (my-add-advice-once #'dash-docs-docsets-path :before fn))
(setenv "DASHT_DOCSETS_DIR" dash-docs-docsets-path)
(unless (file-exists-p dash-docs-docsets-path)
  (make-directory dash-docs-docsets-path 'parents))

(defun my-dash-docs-normalize-docset-names ()
  "`dash-docs-docset-installed-p' automatically replaces \"_\" by \" \" its
argument, therefore, even when a docset is installed with \"_\"
in its name and reported by `dash-docs-installed-docsets', it
won't ever be consider \"installed\", we workaround this by
rename all docset paths from \"_\" to \" \"."
  (dlet ((default-directory dash-docs-docsets-path))
    (dolist (ds (directory-files dash-docs-docsets-path nil "_"))
      (-let* ((target (string-replace "_" " " ds)))
        (unless (file-exists-p target)
          (rename-file ds target))))))
(my-dash-docs-normalize-docset-names)

(provide 'my-after-dash-docs)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require 'cl-macs)

(require '00-my-core-macros)

(require 'dired)

(advice-add #'dired-sort-toggle-or-edit :around #'my-dired-sort-sort-size-when-read-args--around-a)

;;;###autoload
(progn
  (defun my-setup-dired ()
    ;; Automatically remember the project when opening `dired' ` my-project-remember-current-h'
    (my-add-hook/s
      '(dired-mode-hook) '(my-dired-count-show-selection-mode my-dired-key-mode))
    ;; Don't show flags on the mode line
    (setq-default dired-switches-in-mode-line (-const ""))
    ;; Don't hide anything by default
    (remove-hook 'dired-mode-hook 'dired-omit-mode)
    (add-hook 'dired-mode-hook #'my-recentf-add-directory-h)))
;;


(when (fboundp #'dired-async-mode)
  (dired-async-mode))

(my-add-hook/s '(minions-prominent-modes)
  '(;;

    dired-narrow-mode
    dired-omit-mode
    my-dired-count-show-selection-mode
    peep-dired))

;;



(setq-default dired-auto-revert-buffer #'dired-buffer-stale-p)
(setq-default dired-create-destination-dirs 'always)
(setq-default dired-du-size-format t)
(setq-default dired-dwim-target t)
(setq-default dired-mouse-drag-files t)
(setq-default dired-recursive-copies 'always)
;; (setq-default dired-recursive-deletes 'always)  ; until it's able to ask y/n instead of yes/no


;; (order may matter)
;;;###autoload
(my-accumulate-config-collect
 (setq-default dired-listing-switches
               (string-join '(
                              "--all"
                              ;; ;; I want --almost-all, but having "." has its appeal, too
                              ;; "--almost-all"
                              "--group-directories-first"
                              "--human-readable"
                              "--time-style=long-iso"
                              ;; sort alphabetically by entry extension
                              "-X"
                              ;; use a long listing format, MUST
                              "-l"
                              ;; natural sort of (version) numbers within text
                              "-v")
                            " ")))

(setq-default all-the-icons-dired-monochrome nil)
(with-eval-after-load 'dired-x
  (setq-default dired-omit-files
                (my-rx->str `(or (regexp ,(my-get-default-value-of-symbol 'dired-omit-files))
                                 ;; Omit dotfiles
                                 (regexp "\\`\\.")))))

;; don't make them harder to read
(advice-add #'diredfl-match-ignored-extensions :override #'ignore)

;;;###autoload
(define-minor-mode my-dired-key-mode
  nil
  :keymap
  `(
    ;; ;; vi-like
    ;; (,(kbd "j") . next-line)
    ;; (,(kbd "k") . previous-line)
    ;; (,(kbd "I") . dired-maybe-insert-subdir)
    ;; (,(kbd "J") . dired-goto-file)

    ;; (,(kbd "C-<return>") . peep-dired)
    ;; (,(kbd "C-RET") . peep-dired)

    (,(kbd "/") . dired-isearch-filenames-regexp)
    (,(kbd "<delete>") . dired-do-delete)
    (,(kbd "C-v C-c") . my-dired-multi-paste-copy)
    (,(kbd "C-v C-x") . my-dired-multi-paste-cut)
    (,(kbd "K") . dired-do-kill-lines)
    (,(kbd "M-+") . dired-create-empty-file)
    (,(kbd "M-<home>") . my-dired-home)
    (,(kbd "M-<left>") . my-dired-switch-to-prev-buffer)
    (,(kbd "M-<return>") . my-dired-du-count-sizes)
    (,(kbd "M-<right>") . my-dired-switch-to-next-buffer)
    (,(kbd "M-<up>") . dired-up-directory)
    (,(kbd "M-RET") . my-dired-du-count-sizes)
    (,(kbd "M-c") . my-dired-copy-filenames)
    (,(kbd "M-m") . dired-mark-files-regexp)
    (,(kbd "S-<delete>") . my-dired-delete-no-trash)
    (,(kbd "TAB") . my-dired-toggle-preview-directory-as-subtree-or-file)
    (,(kbd "W") . my-dired-open-by-default-application) ; fallback on CWD
    (,(kbd "Z") . my-dired-compress-dwim)
    (,(kbd "e") . my-find-file-alternative-truename) ; originally `dired-find-file', let this key do a little different
    (,(kbd "f") . dired-narrow-regexp)
    (,(kbd "h") . dired-omit-mode)
    (,(kbd "q") . quit-window)
    (,(kbd "r") . my-dired-do-rename)
    (,(kbd my-leader-key) . my-leader-spc-prefix-cmd))

  ;;
  
  (if my-dired-key-mode
      (progn)
    (progn)))

(with-eval-after-load 'evil
  (-let* ((z-prefix (my-evil-prefix-keymap-in-state "z" dired-mode-map 'normal))
          (g-prefix (my-evil-prefix-keymap-in-state "g" dired-mode-map 'normal)))
    (my-bind :map 'my-dired-key-mode-map
      "z" z-prefix
      "g" g-prefix
      "g g" #'revert-buffer)))


(my-dired-undop-record-mode)
(my-bind :map 'my-dired-key-mode-map "C-z" #'my-dired-undop-operation)

;; make `dired-up-directory' less surprising: only move up a "path component"
;; instead of changing suddenly in case of symlinks
(advice-add
 #'dired-up-directory
 :around
 (my-make-with-dlet-variables-advice '((find-file-visit-truename nil))))


(with-eval-after-load 'mwim
  (add-hook 'mwim-beginning-position-functions #'my-dired-filename-beg-position)
  (add-hook 'mwim-end-position-functions #'my-dired-filename-end-position))
;; this will disallow going to `beginning-of-line'
;; (add-hook 'mwim-beginning-of-line-function '(dired-mode . dired-move-to-filename) 100)

;; ;; `dired' already has "Open with"
;; (my-context-menu-add-local 'dired-mode #'my-xdg-open-with-context-menu-completing-read 25 t)

(when (fboundp #'nerd-icons-dired-mode)
  (my-add-mode-hook-and-now '(dired-mode) #'nerd-icons-dired-mode))

(when (fboundp #'diredfl-mode)
  (my-add-mode-hook-and-now '(dired-mode) #'diredfl-mode))

(provide 'my-hook-dired-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

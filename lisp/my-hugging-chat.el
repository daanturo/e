;; -*- lexical-binding: t; -*-

;;; Commentary:


;;; Code:

(require 'compat)

(require 'dash)
(require 's)

(defvar my-hugging-chat-executable-path
  (expand-file-name "my-hugging-chat.py" my-emacs-bin-directory/))

(defun my-hugging-chat-default-shell-name-project-dedicated ()
  (format "%s %s"
          'my-hugging-chat-shell
          (or (-some--> (project-current) (project-name it))
              (abbreviate-file-name default-directory))))

(defcustom my-hugging-chat-shell-name
  (format "*%s*" "my-hugging-chat-shell")
  "String (or function to be called with 0 arguments) to name the comint buffer.
The final name will wrapped with \"*\"s. To have per-project
buffers, set this to
`my-hugging-chat-default-shell-name-project-dedicated' (but
currently doesn't seem to work as the session is still shared)."
  :group 'my-hugging-chat)

(defun my-hugging-chat-shell-setup-buffer ()
  (visual-line-mode))

(defun my-hugging-chat-shell--buffer ()
  (-->
   (cond
    ((functionp my-hugging-chat-shell-name)
     (funcall my-hugging-chat-shell-name))
    ((stringp my-hugging-chat-shell-name)
     my-hugging-chat-shell-name))))

(defcustom my-hugging-chat-cookies nil
  "JSON string or non-relative cookies file path."
  :group 'my-hugging-chat
  :type 'string)

(defcustom my-hugging-chat-prompt-indicator nil
  nil
  :group 'my-hugging-chat
  :type 'string)

;;;###autoload
(defun my-hugging-chat-make-shell-buffer ()
  (cl-assert (stringp my-hugging-chat-cookies))
  (-let* ((comint-name (my-hugging-chat-shell--buffer))
          (buf
           (apply #'make-comint-in-buffer
                  comint-name comint-name my-hugging-chat-executable-path nil
                  `(,my-hugging-chat-cookies
                    ,@(and my-hugging-chat-prompt-indicator
                           (list "--prompt" my-hugging-chat-prompt-indicator))))))
    (with-current-buffer buf
      (my-hugging-chat-shell-setup-buffer))
    buf))

;;;###autoload
(defun my-hugging-chat-switch-to-shell-buffer ()
  "https://github.com/Soulter/hugging-chat-api."
  (interactive)
  (-let* ((buf-name (my-hugging-chat-shell--buffer)))
    (unless (get-buffer buf-name)
      (my-hugging-chat-make-shell-buffer))
    (pop-to-buffer buf-name)))

;;; my-hugging-chat.el ends here

(provide 'my-hugging-chat)

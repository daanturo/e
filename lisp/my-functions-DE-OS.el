;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-display-wayland-p ()
  (member (getenv "XDG_SESSION_TYPE") '("wayland" "" nil)))

;;;###autoload
(defun my-select-file-and-copy-contents ()
  (interactive)
  (my-clipboard-copy-file-contents (read-file-name "Copy contents: ") t))

;;;###autoload
(defun my-flatpak-executable-find (app)
  (let* ((installed-apps (--> (shell-command-to-string "flatpak list  --columns=application")
                              (string-lines it 'omit-nulls)
                              cdr -uniq))
         (found-app (car (member app installed-apps))))
    found-app))

(defvar my-automation-type-insert--service nil)

(defun my-automation-assert-service ()
  (cl-assert
   (equal 0
          (with-memoization my-automation-type-insert--service
            (-->
             (call-process "systemctl" nil nil nil
                           "--user" "is-active"
                           "ydotool.service")
             (and (zerop it) it))))))

;;;###autoload
(defun my-automation-type-insert (str &optional delay-secs)
  (-let* ((delay (or delay-secs 0))
          (tmpfile (make-temp-file "my-automation-type-insert"))
          ;; the tool here!
          (automation-cmd (format "ydotool type -d 10 -H 10 -f %s" tmpfile))
          ;; (automation-cmd (format "ydotool type -f %s" tmpfile))
          (cmd (my-s$ "sleep ${delay} ; ${automation-cmd} ; rm ${tmpfile}")))
    (my-automation-assert-service)
    (f-write-text str 'utf-8 tmpfile)
    (start-process-shell-command "my-automation-type-insert" nil cmd)))


(provide 'my-functions-DE-OS)

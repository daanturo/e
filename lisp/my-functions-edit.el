;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-ensure-final-newline ()
  (interactive)
  (unless (or (buffer-narrowed-p)
              (= ?\n (char-before (point-max))))
    (save-excursion
      (goto-char (point-max))
      (insert "\n"))))

;;;###autoload
(defun my-insert-at-buffer-position (pos &rest args)
  (save-excursion
    (goto-char pos)
    (apply #'insert args)))

;;;###autoload
(defun my-wrap-or-insert (pre suf)
  (cond
   ((use-region-p)
    (-let* ((beg (region-beginning))
            (end (region-end)))
      (save-excursion
        (goto-char end)
        (insert suf)
        (goto-char beg)
        (insert pre))))
   (t
    (insert pre)
    (save-excursion (insert suf)))))

;;;###autoload
(defun my-indent-project-buffers ()
  (interactive)
  (-let* ((bufs (--> (project-buffers (project-current))
                     (-filter #'buffer-file-name it))))
    (when (y-or-n-p (format "Indent: %s" bufs))
      (mapc #'my-indent-buffer bufs))))

;;;###autoload
(defun my-copy-to-clipboard-whole-buffer ()
  (declare (interactive-only t))
  (interactive)
  (-let* ((pt-max
           (save-excursion
             (goto-char (point-max))
             (skip-chars-backward "\n")
             (point))))
    (kill-ring-save (point-min) pt-max)
    (dlet ((pulse-flag t))
      (pulse-momentary-highlight-region (point-min) pt-max 'highlight))))


(provide 'my-functions-edit)

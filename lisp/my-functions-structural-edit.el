;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-tranpose-sexp-and-go-left (&optional n)
  (interactive "p")
  (dotimes (_ n)
    (transpose-sexps 1 (point))
    (backward-sexp 2)))

;;;###autoload
(defun my-tranpose-sexp-and-go-right (&optional n)
  (interactive "p")
  (dotimes (_ n)
    (transpose-sexps 1 (point))))

;;;###autoload
(defun my-mark-sexp-default (&optional arg allow-extend)
  (interactive "P\np")
  ;; the local value of `forward-sexp-function' may cause unexpected results,
  ;; such as `treesit-forward-sexp'
  (dlet ((forward-sexp-function nil))
    (mark-sexp arg allow-extend)))

;;;###autoload
(defun my-kill-sexp-default (&optional arg interact)
  (interactive "p\nd")
  (dlet ((forward-sexp-function nil))
    (kill-sexp arg interact)))

;;;###autoload
(defun my-puni-splice-sexp-default ()
  (interactive)
  (dlet ((forward-sexp-function nil))
    (puni-splice)))

;;;###autoload
(defun my-puni-splice-killing-backward-sexp-default ()
  (interactive)
  (dlet ((forward-sexp-function nil))
    (puni-splice-killing-backward)))

;;;###autoload
(defun my-puni-splice-killing-forward-sexp-default ()
  (interactive)
  (dlet ((forward-sexp-function nil))
    (puni-splice-killing-forward)))

;;;###autoload
(defun my-backward-or-backward-up-sexp (arg)
  "See (`my-forward-or-up-sexp' ARG)."
  (interactive "p")
  (my-with-default-forward-sexp-function-if-non-code
    (my-with-default-forward-sexp-function-if-braces
      (cond
       ((and (integerp arg) (<= arg 0))
        (backward-up-list (max 1 (- arg)) (point) (point)))
       (t
        (ignore-error (scan-error)
          (backward-sexp arg)))))))

;;;###autoload
(defun my-forward-or-up-sexp (arg)
  "`forward-sexp' ARG or (when non-positive ARG) `up-list'."
  (interactive "p")
  (my-with-default-forward-sexp-function-if-non-code
    (my-with-default-forward-sexp-function-if-braces
      (cond
       ((and (integerp arg) (<= arg 0))
        (up-list (max 1 (- arg)) (point) (point)))
       (t
        (ignore-error (scan-error)
          (forward-sexp arg)))))))

;;;###autoload
(defun my-forward-sexp-or-paragraph (&optional n)
  "Forward N sexps or paragraphs, whichever moves the most."
  (interactive "p")
  (-let* ((n (or n 1))
          (pos-para
           (save-excursion
             (forward-paragraph n)
             (point)))
          (pos-sexp
           (save-excursion
             (forward-sexp n)
             (point)))
          (dest
           (cond
            ((< n 0)
             (min pos-para pos-sexp))
            (:else
             (max pos-para pos-sexp)))))
    (goto-char dest)))

;;;###autoload
(defun my-expand-region||selection-to-forward-sexp (arg)
  (interactive "p")
  (if (use-region-p)
      (progn
        (when (< (region-beginning) (point))
          (exchange-point-and-mark))
        (mark-sexp arg 'allow-extend))
    (mark-sexp arg))
  (exchange-point-and-mark))

;;;###autoload
(defun my-expand-region||selection-to-backward-sexp (arg)
  (interactive "p")
  (if (use-region-p)
      (progn
        (when (< (point) (region-end))
          (exchange-point-and-mark))
        (mark-sexp (- arg) 'allow-extend))
    (mark-sexp (- arg)))
  (exchange-point-and-mark))

;;;###autoload
(defun my-join-closing-delimiters ()
  "Join lines which start with closing delimiters in this buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    ;; Suppress error after having done searching
    (while (ignore-errors (search-forward-regexp "^[[:space:]]*[])}>]"))
      (save-excursion
        ;; Jump to the last character that is not space or new line and delete blank lines following it
        (back-to-indentation)
        (search-backward-regexp "[^[:space:]\n]")
        (delete-blank-lines)
        ;; Perform joining if that line doesn't end with comment
        (unless (nth 4 (syntax-ppss))
          (join-line 1))))))

;;;###autoload
(defun my-split-sexps->multiple-lines-by-spaces (&optional rbeg rend)
  (interactive (my-used-active-region-bounds t))
  (progn
    (goto-char rbeg)
    (while (re-search-forward "\\([^ \t\n\r]\\)[[:space:]]+\\([^ \t\n\r]\\)" rend t)
      (unless (my-point-in-string-or-comment-p)
        (replace-match "\\1\n\\2")))))

;;;###autoload
(defun my-split-sexps->multiple-lines-by-opening-brackets (&optional rbeg rend)
  (interactive (my-used-active-region-bounds t))
  (progn
    (goto-char rbeg)
    ;; a non (space or new line), then space(s), then a closing bracket
    (while (re-search-forward "\\([^ \t\n\r]\\)[[:space:]]+\\([(\[{]\\)" rend t)
      (unless (my-point-in-string-or-comment-p)
        (replace-match "\\1\n\\2")))))

;;;###autoload
(defun my-split-sexps->multiple-lines-by-closing-brackets (&optional rbeg rend)
  (interactive (my-used-active-region-bounds t))
  (progn
    (goto-char rbeg)
    ;; a closing bracket, then a space
    (while (re-search-forward "\\([)\]}]\\)\\( \\)" rend t)
      (unless (my-point-in-string-or-comment-p)
        (replace-match "\\1)\n\\2")))))

;;;###autoload
(defun my-split-sexps->multiple-lines-by-brackets (&optional rbeg rend)
  (interactive (my-used-active-region-bounds t))
  (my-split-sexps->multiple-lines-by-opening-brackets rbeg rend)
  (my-split-sexps->multiple-lines-by-closing-brackets rbeg rend)
  (indent-region rbeg rend))

;;;###autoload
(defun my-backward-up-string ()
  (interactive)
  (when (my-point-in-string-p)
    (goto-char (car (bounds-of-thing-at-point 'string)))))

;;;###autoload
(defun my-kill-all-sexps-greedy (&optional backward)
  (interactive)
  (kill-region
   (point)
   (if backward
       (puni-beginning-pos-of-list-around-point)
     (puni-end-pos-of-list-around-point))))

;;;###autoload
(defun my-puni-kill-line (arg)
  (interactive "P")
  (cond
   ((equal arg '(4))
    (my-kill-all-sexps-greedy))
   ((equal arg '(-4))
    (my-kill-all-sexps-greedy t))
   (t
    (puni-kill-line arg))))

;; TODO: report that `puni-backward-kill-line' doesn't work because of
;; REPL/minibuffer prompts

;;;###autoload
(defun my-end-position-of-last-sexp-of-current-line ()
  (-let* ((line0 (line-number-at-pos)))
    (save-excursion
      (while (and (= line0 (line-number-at-pos))
                  (not (looking-at-p "[ \t\n\r]*$")))
        (condition-case _
            (forward-sexp 1)
          ((scan-error) nil)))
      (point))))

;;;###autoload
(defun my-kill-line-backward-balanced ()
  (interactive)
  (-let* ((bolp-flag (bolp))
          (list-beg (puni-beginning-pos-of-list-around-point)))
    ;; nothing to delete, move to the previous line
    (when bolp-flag
      (delete-char -1))
    ;; note that `kill-sexp' will kill through comments when not in comments
    (-let* ((back-pos
             (max list-beg
                  ;; `pos-bol' ignores REPL/SHELL promtps
                  (line-beginning-position))))
      (if (puni-region-balance-p back-pos (point))
          (kill-region back-pos (point))
        (kill-sexp -1)))))

;;;###autoload
(defun my-duplicate-current-sexp ()
  (interactive)
  (-let* ((bs0 (buffer-size))
          (pt0 (point))
          (str
           (save-excursion
             (backward-up-list)
             (thing-at-point 'sexp t)))
          (sep
           (cond
            ((string-search "\n" str)
             "\n")
            (:else
             " "))))
    (save-excursion
      (up-list)
      (insert sep str))
    (-let* ((bs1 (buffer-size)))
      (goto-char (+ pt0 (- bs1 bs0))))))

(provide 'my-functions-structural-edit)

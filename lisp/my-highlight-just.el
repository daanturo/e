;; -*- lexical-binding: t; -*-

(require 'dash)

(require 'hi-lock)

;;;###autoload
(defun my-highlight-just-overlays-at (&optional pos whole-buffer)
  (--> (cond (whole-buffer
              (overlays-in (point-min) (point-max)))
             (pos
              (overlays-at pos)))
       (-filter (lambda (ov)
                  (overlay-get ov 'my-highlight-just))
                it)))

(defun my-highlight-just-default-face ()
  (my-hi-lock-get-default-face-cycled
   (length (my-highlight-just-overlays-at nil t))))

(defun my-highlight-just--face (prompt?)
  (--> (and prompt?
            (read-face-name "`my-highlight-just': "
                            (my-highlight-just-default-face)))
       (or it
           (my-highlight-just-default-face))))

;;;###autoload
(defun my-highlight-just-region (beg end &optional face)
  (interactive (list
                (and (use-region-p) (region-beginning))
                (and (use-region-p) (region-end))
                (my-highlight-just--face current-prefix-arg)))
  (-let* ((face (or face (my-highlight-just-default-face)))
          (ov (make-overlay beg end)))
    (overlay-put ov 'face `((t (:inherit ,face))))
    (overlay-put ov 'my-highlight-just t)
    ov))

;;;###autoload
(defun my-highlight-just-remove-at-point (pos)
  (interactive "d")
  (-let* ((ovs (my-highlight-just-overlays-at pos)))
    (mapc #'delete-overlay ovs)
    ovs))

;;;###autoload
(defun my-highlight-just-remove-all-in-buffer ()
  (interactive)
  (-let* ((ovs (my-highlight-just-overlays-at nil t)))
    (mapc #'delete-overlay ovs)
    ovs))

;;;###autoload
(defun my-highlight-just-dwim (&optional read-face)
  (interactive "P")
  (-let* (((beg . end) (cond ((use-region-p)
                              (cons (region-beginning)
                                    (region-end)))
                             ((bounds-of-thing-at-point 'symbol))
                             ((bounds-of-thing-at-point 'line)))))
    (or (my-highlight-just-remove-at-point (point))
        (my-highlight-just-region
         beg end
         (my-highlight-just--face read-face)))))

(provide 'my-highlight-just)

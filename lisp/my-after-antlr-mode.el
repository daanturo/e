;; -*- lexical-binding: t; -*-

;;;###autoload
(my-accumulate-config-collect
 (add-to-list 'auto-mode-alist '("\\.g4\\'" . antlr-mode)))

(provide 'my-after-antlr-mode)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)

(require '00-my-core-macros)

(require 'transient)

;;;###autoload
(defun my-quit|delete-window-buffer (buf)
  (-some--> (get-buffer-window buf)
    delete-window))

;;;###autoload
(defun my-window-visible-p (window)
  (declare (side-effect-free t))
  (or (window-parameter window 'visible)
      (not (window-dedicated-p window))))

;;;###autoload
(defun my-quit-other-window ()
  (interactive)
  (save-selected-window
    (quit-window nil (other-window 1))))

;;;###autoload
(defun my-display-current-buffer-on-left-window ()
  (interactive)
  (-let* ((buf (current-buffer)))
    (delete-window)
    (display-buffer-in-direction buf `((direction . left)))
    (select-window (get-buffer-window buf))))

;;;###autoload
(defun my-recenter-region-in-window ()
  (interactive)
  (recenter-top-bottom
   (max 0
        (floor (/ (- (window-height)
                     (if (use-region-p)
                         (count-lines (region-beginning)
                                      (region-end))
                       0))
                  2)))))

;;;###autoload
(defun my-split-current-window ()
  "Split current window on the right or below, depends on current window height and width."
  (interactive)
  (dlet ((ignore-window-parameters t))
    (if (> (window-pixel-height)
           (window-pixel-width))
        (split-window-below)
      (split-window-right))))

;;;###autoload
(defun my-try-to-enlarge|widen|fit-window-width-maybe ()
  "Resize current window's width sensibly.
To 2/3 of `frame-width' or longest line of it, which ever is
lower."
  (interactive)
  (let ((width (min (floor (* (frame-width)
                              (/ 2.0 3)))
                    (+
                     ;; margin size, cause `window-body-width' includes margins??
                     (- (window-width) (window-max-chars-per-line))
                     (nth 1 (buffer-line-statistics))))))
    (enlarge-window (- width (window-width)) 'horizontal)))

;;;###autoload
(defun my-resize-window-width-to-frame-fraction (fraction)
  (interactive (list (--> (read-string "Fraction: ") (my-calculate it))))
  (let ((width (ceiling (* (frame-width) fraction))))
    (enlarge-window (- width (window-width)) 'horizontal)))

;;;###autoload
(defun my-total-margin-widths-of-window (&optional window)
  "Return WINDOW's total width of its margins.
Using (- (window-width) (window-max-chars-per-line)) maybe more
reliable."
  (declare (side-effect-free t))
  (cl-destructuring-bind (rw . lw) (window-margins window)
    (+ (or rw 0) (or lw 0))))

;;;###autoload
(defun my-minimum-width-of-windows-and-notable-margins (window-lst)
  (declare (side-effect-free t))
  (cl-loop
   for window in window-lst
   minimize
   (let ((margin-width (my-total-margin-widths-of-window window)))
     (if (<= my-sidebar-width margin-width)
         (min (window-width window) margin-width)
       (window-width window)))))

;;;###autoload
(cl-defun my-get-other-windows-in-frames
    (&optional (frame/s (frame-list)) (window (selected-window)))
  "Get the list of windows in FRAME/S (single or list) beside WINDOW."
  (declare (side-effect-free t))
  (let ((frames (ensure-list frame/s)))
    (-remove (-partial #'equal window)
             (mapcan #'window-list frames))))

;;;###autoload
(defun my-window-line-height-from-top ()
  (- (line-number-at-pos)
     (line-number-at-pos (window-start))))

;;;###autoload
(defun my-window-line-height-fraction ()
  (/ (float (my-window-line-height-from-top))
     (window-height)))

;;;###autoload
(defun my-switch-to-buffer-prefer-existing-window (buffer)
  (if (get-buffer-window buffer)
      (pop-to-buffer buffer)
    (switch-to-buffer buffer)))

;;;###autoload
(defun my-list-windows-around (&optional wd)
  (list
   (window-in-direction 'left wd 'ignore)
   (window-in-direction 'below wd 'ignore nil nil 'never)
   (window-in-direction 'above wd 'ignore)
   (window-in-direction 'right wd 'ignore)))

;;;###autoload
(defun my-prefer-split-window-right ()
  "Make current window prefer splitting horizontally."
  (interactive)
  (let ((w split-width-threshold)
        (h split-height-threshold))
    (setq-local split-height-threshold (max h w))
    (setq-local split-width-threshold (min w h))))

;;;###autoload
(defun my-align-minimum-indentation-to-window-left-margin (offset)
  (interactive "p")
  ;; (window-hscroll)
  (-let* ((target (- (my-find-minimum-indentation-of-buffer)
                     offset)))
    (scroll-left (- target (window-hscroll)))))

;;;###autoload
(defun my-cycle-popup-buffer-window (buf)
  (-let* ((bufname (my-buffer-name-string buf))
          (wd (get-buffer-window buf)))
    (cond
     ;; not shown: display it without selecting
     ((null wd)
      (save-selected-window
        (pop-to-buffer bufname)))
     ;; shown, not selected: select it
     ((not (equal bufname (buffer-name)))
      (select-window wd))
     ;; selected: hide it
     ((equal bufname (buffer-name))
      (quit-window)))))

;;;###autoload
(defun my-minibuffer-selected-buffer ()
  (window-buffer (minibuffer-selected-window)))

;;;###autoload
(defun my-popup-window-target-fit-height (&optional max-ratio)
  (-let* ((max-ratio
           (or max-ratio
               (alist-get
                'window-height my-windowpopup-default-action-alist))))
    (min (window-buffer-height (selected-window))
         (floor (* max-ratio (frame-height))))))

;;;###autoload
(defun my-fit-current-popup-window-height (&optional max-ratio more-lines)
  (interactive)
  (-let* ((target
           (-->
            (my-popup-window-target-fit-height max-ratio)
            (if more-lines
                (+ it more-lines)
              it))))
    (window-resize (selected-window) (- target (window-height)) nil t)))

;;;###autoload
(defun my-popup-shrink-to-fit (window)
  (with-selected-window window
    (my-fit-current-popup-window-height)))

;;;###autoload
(defun my-window-duplicated-buffer-p (&optional wd frm)
  "Return if WD's buffer is displayed by another window in FRM.
WD and FRM defaults to selected ones."
  (-let* ((wd (or wd (selected-window)))
          (buf (window-buffer wd))
          (buf-lst (-map #'window-buffer (window-list frm))))
    (< 1 (-count (lambda (item) (equal item buf)) buf-lst))))

;;;###autoload
(defun my-delete-window-when-duplicated-buffer (&optional wd)
  "Delete window WD (or selected one) when displayed unnecessarily.
See `my-window-duplicated-buffer-p'."
  (when (my-window-duplicated-buffer-p wd)
    (delete-window wd)))

;;;###autoload
(defun my-force-delete-other-windows ()
  (interactive)
  (set-window-parameter nil 'window-side nil)
  (dlet ((ignore-window-parameters t))
    (delete-other-windows nil t)))

;;;###autoload
(defun my-force-split-window-below ()
  (interactive)
  (set-window-parameter nil 'window-side nil)
  (dlet ((ignore-window-parameters t))
    (split-window-below)))

;;;###autoload
(defun my-force-split-window-right ()
  (interactive)
  (set-window-parameter nil 'window-side nil)
  (dlet ((ignore-window-parameters t))
    (split-window-right)))

;;;###autoload
(defun my-window-ask-to-try-again-with-ignore-window-parameters-interactive-a
    (func &rest args)
  (if (called-interactively-p t)
      (condition-case err
          (apply func args)
        (error
         (if (y-or-n-p
              (format "%S, try again with `ignore-window-parameters'?"
                      err))
             (dlet ((ignore-window-parameters t))
               (apply func args))
           (signal (car err) (cdr err)))))
    (apply func args)))

;;;###autoload
(defun my-frame-window-buffer-file-list ()
  (--> (window-list) (-map #'window-buffer it) (-keep #'buffer-file-name it)))

(defvar my-alpha-background-transparency-step 4)

;;;###autoload
(defun my-increase-alpha-background-transparency ()
  (interactive)
  (-let* ((current-alpha (or (frame-parameter nil 'alpha-background) 100)))
    (set-frame-parameter
     nil 'alpha-background
     (my-clamp (+ current-alpha my-alpha-background-transparency-step) 0 100))))

;;;###autoload
(defun my-decrease-alpha-background-transparency ()
  (interactive)
  (-let* ((current-alpha (or (frame-parameter nil 'alpha-background) 100)))
    (set-frame-parameter
     nil 'alpha-background
     (my-clamp (- current-alpha my-alpha-background-transparency-step) 0 100))))

;;;###autoload
(defun my-reset-alpha-background-transparency ()
  (interactive)
  (set-frame-parameter nil 'alpha-background nil))

;;;###autoload
(transient-define-prefix
  my-transient-alpha-background-transparency ()
  [[("-"
     "Decrease (more transparent)"
     my-decrease-alpha-background-transparency
     :transient t)]
   [("="
     "Increase (more opaque)"
     my-increase-alpha-background-transparency
     :transient t)]
   [("0" "Reset" my-reset-alpha-background-transparency :transient t)]])

(provide 'my-functions-frame-window)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(autoload #'tree-inspector-inspect "tree-inspector")

(defvar my-inspector-buffer-name "*inspector*")

;;;###autoload
(defun my-inspector-inspect-top-level ()
  "Useful in eval output buffer."
  (interactive)
  (inspector-inspect (read (thing-at-point 'defun))))

;;;###autoload
(defun my-inspector-expression (expr)
  "(`inspector-inspect-expression' EXPR)."
  (interactive (list (read--expression (format "Inspect evaluation: "))))
  (my-with-save-window-and-fit-popup
    my-inspector-buffer-name (inspector-inspect-expression expr)))

;;;###autoload
(defun my-inspector-defun (&optional edebug-it)
  (interactive "P")
  (cond
   (edebug-it
    (eval-defun t))
   (t
    (-let* ((str (thing-at-point 'defun t))
            (result (eval str t))))
    (my-with-save-window-and-fit-popup
      my-inspector-buffer-name (inspector-inspect result)))))

;;;###autoload
(defun my-inspector-region (beg end)
  (interactive (and (use-region-p) (list (region-beginning) (region-end))))
  (my-with-save-window-and-fit-popup
    my-inspector-buffer-name (inspector-inspect-region beg end)))

(defvar-local my-inspector--data nil)
;;;###autoload
(defun my-inspector->tree-inspector ()
  (interactive)
  (-let* ((key
           (-->
            (where-is-internal #'my-inspector->tree-inspector) (cl-first it)))
          (data (buffer-local-value '* (current-buffer)))
          (buf (tree-inspector-inspect data)))
    (with-current-buffer buf
      (setq-local my-inspector--data data)
      (local-set-key key #'my-tree-inspector->inspector))
    buf))

;;;###autoload
(defun my-tree-inspector->inspector ()
  (interactive)
  (-let* ((data my-inspector--data))
    (inspector-inspect data)))


(provide 'my-functions-inspector)

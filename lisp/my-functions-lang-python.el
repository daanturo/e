;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'python)

(my-autoload-functions 'elpy '(elpy-company-backend))

;;;###autoload
(autoload #'my-lang-python-cape-elpy-company-capf "my-functions-lang-python")
(my-cape-lazy-make-capf #'my-lang-python-cape-elpy-company-capf
  '(cape-company-to-capf 'elpy-company-backend)
  '(:sort t
          ;; workaround https://github.com/minad/cape/issues/44
          :category dummy-elpy-company-backend))

(my-add-advice-once #'elpy-company-backend :before (lambda (&rest _) (require 'company)))

;; fake `elpy-mode' for completions
(advice-add
 #'elpy-company-backend
 :around (my-make-with-dlet-variables-advice '((elpy-mode t))))

;;;###autoload
(define-minor-mode my-lang-python-elpy-completion-mode nil
  :global nil
  (if my-lang-python-elpy-completion-mode
      (progn
        (setq-local
         completion-at-point-functions
         ;; after LSP, before built-in python CAPF
         (-uniq
          `(,@(and (member 'lsp-completion-at-point completion-at-point-functions)
                   '(lsp-completion-at-point))
            my-lang-python-cape-elpy-company-capf
            ,@completion-at-point-functions))))
    (progn
      (remove-hook 'completion-at-point-functions
                   'my-lang-python-cape-elpy-company-capf
                   'local))))

;;;###autoload
(defun my-lang-python-importmagic-fix-missing-imports-in-file ()
  (interactive)
  (importmagic-mode)
  (importmagic-fix-imports))

;;;###autoload
(defun my-lang-python-get-virtual-environment-dirs (&optional dir)
  (let* ((dir (or dir default-directory))
         (files (directory-files dir t)))
    (seq-filter
     (lambda (dir)
       (and (file-directory-p dir)
            (file-exists-p (file-name-concat dir "bin/activate"))))
     files)))

;;;###autoload
(cl-defun my-lang-python-get-local-virtualenv-root-dir (&optional proj-root-dir &key pred &allow-other-keys)
  (interactive)
  (let* ((proj-root-dir (or proj-root-dir default-directory))
         (pred (or pred #'always))
         (venv-dir
          (cond
           ;; Common values: just ".venv" and "venv", otherwise ignore others
           ((seq-some
             (lambda (name)
               (let* ((venv-dir1 (file-name-concat proj-root-dir name)))
                 (and (file-exists-p venv-dir1)
                      (funcall pred venv-dir1)
                      venv-dir1)))
             '(".venv" "venv"))))))
    ;; (:else
    ;;  (seq-find pred (my-lang-python-get-virtual-environment-dirs proj-root-dir)))
    venv-dir))

;;;###autoload
(cl-defun my-lang-python-maybe-set-local-virtualenv-vars (&optional dir &rest args &key pred force)
  (interactive)
  (when (or (not python-shell-virtualenv-root) force)
    (-let* ((venv-dir
             (apply #'my-lang-python-get-virtual-environment-dirs dir args)))
      (when (my-non-empty-list? venv-dir)
        (setq-local python-shell-virtualenv-root (nth 0 venv-dir))))))

;;;###autoload
(defun my-lang-python-shell-dedicated ()
  (cond
   ((equal python-shell-dedicated 'project)
    (if (project-current)
        python-shell-dedicated
      'buffer))
   (t
    python-shell-dedicated)))

(defvar my-lang-python-auto-activate-virtual-environment t)

;;;###autoload
(defun my-lang-python-repl (&optional display)
  (interactive (list (not current-prefix-arg)))
  (when my-lang-python-auto-activate-virtual-environment
    (my-lang-python-maybe-set-local-virtualenv-vars (my-project-root)))
  (cond
   ;; (my-lang-python-repl-use-jupyter
   ;;  (my-lang-python-jupyter-repl display))
   (:else
    (prog1 (or (python-shell-get-buffer)
               (run-python nil (my-lang-python-shell-dedicated) nil))
      (when display
        (python-shell-switch-to-shell))))))

;;;###autoload
(defun my-lang-python-bounds-of-inner-ipython-cell ()
  (my-bounds-by-regexps
   "^# In\\[\\( \\|[0-9]+\\)\\]:\n+"
   (concat "\n+"
           (my-regexp-or '("# In\\[\\( \\|[0-9]+\\)\\]:"
                           "# # ")))))

;;;###autoload
(defun my-lang-python-mark-inner-ipython-cell ()
  (interactive)
  (my-mark-bounds-as-cons-maybe
   (my-lang-python-bounds-of-inner-ipython-cell)))

;;;###autoload
(defun my-lang-python-mark-ipython-cell-context-menu (menu &optional _click)
  (-when-let* ((bounds (my-lang-python-bounds-of-inner-ipython-cell)))
    (define-key-after menu [my-lang-python-mark-ipython-cell-context-menu] menu-bar-separator)
    (define-key-after menu [my-lang-python-mark-inner-ipython-cell]
      `(menu-item "Mark IPython cell" ,(lambda () (interactive)
                                         (my-mark-bounds-as-cons-maybe bounds))))
    (define-key-after menu [copy]
      `(menu-item "Copy IPython cell" ,(lambda () (interactive)
                                         (-let* (((beg . end) bounds))
                                           (pulse-momentary-highlight-region beg end)
                                           (kill-new (buffer-substring beg end)))))))
  menu)

;;;###autoload
(defun my-lang-python-nav-backward-up-list (&optional arg)
  (declare (interactive-only t))
  (interactive "^p")
  (-let* ((arg (or arg 1))
          (pt0 (point)))
    (python-nav-backward-up-list arg)
    ;; `python-nav-backward-up-list' didn't move
    (when (= pt0 (point))
      (backward-up-list arg pt0 pt0))))

;;;###autoload
(defun my-lang-python-query-import-statements ()
  (and (my-mode-derived-p major-mode '(python-ts-mode))
       (-let* ((root-note (treesit-buffer-root-node))
               (import-nodes
                (treesit-filter-child
                 root-note
                 (lambda (node)
                   (member
                    (treesit-node-type node)
                    '("import_statement" "import_from_statement")))))
               (strings (-map #'my-treesit-node-text* import-nodes)))
         (string-join strings "\n"))))


(provide 'my-functions-lang-python)

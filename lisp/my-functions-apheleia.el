;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'apheleia)

;;;###autoload
(progn
  (defun my-apheleia-get-formatters-safe ()
    (if (require 'apheleia nil 'noerror)
        (progn
          (defalias
            #'my-apheleia-get-formatters-safe #'apheleia--get-formatters)
          (apheleia--get-formatters))
      (progn
        (defalias #'my-apheleia-get-formatters-safe #'ignore)
        nil))))

;;;###autoload
(defun my-apheleia-external-formatter-p (formatter)
  (--> (alist-get formatter apheleia-formatters)
       ensure-list
       (-some #'stringp it)))

;;;###autoload
(defun my-apheleia-external-formatters-p ()
  (-some--> (my-apheleia-get-formatters-safe)
    (-every #'my-apheleia-external-formatter-p it)))

;;;###autoload
(defun my-apheleia-menu-item-filter-not-external-formatters (cmd)
  (and (not (my-apheleia-external-formatters-p))
       cmd))

(defvar my-apheleia-format-after-save-before-hook '())

;;;###autoload
(defun my-apheleia-format-after-save-before-hook--run ()
  (run-hooks 'my-apheleia-format-after-save-before-hook))

;;;###autoload
(defun my-apheleia-format-file (filename &optional callback)
  (-let* ((opened0 (get-file-buffer filename))
          (buf (find-file-noselect filename t)))
    (with-current-buffer buf
      (apheleia-format-buffer
       (my-apheleia-get-formatters-safe) nil
       :callback
       (lambda (&rest cbargs)
         (with-current-buffer buf
           (when callback
             (funcall callback cbargs))
           (when (not (plist-get cbargs :error))
             (save-buffer 0))
           (when (not opened0)
             (my-kill-buffer-no-ask buf))))))))

(provide 'my-functions-apheleia)

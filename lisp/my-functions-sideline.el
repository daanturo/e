;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defface my-sideline-git-blame-face
  '((t
     :foreground "gray"
     :italic t
     :inherit default ; inherit from default to prevent other overlay from meddling
     ))
  nil)

(defun my-sideline-git-blame--async-backend--blame-cmdargs (lnum)
  (list
   "git"
   "-P"
   "blame"
   "--porcelain"
   "-l"
   "-L"
   (format "%s,%s" lnum lnum)
   "--"
   (buffer-file-name)))

;; debug purpose
(defvar-local my-sideline-git-blame--async-backend--last-stdout nil)

(defun my-sideline-git-blame--async-backend (callback &rest _)
  (when buffer-file-name
    (-let* ((lnum (line-number-at-pos)))
      (my-process-async
       (my-sideline-git-blame--async-backend--blame-cmdargs lnum)
       (lambda (stdout obj &rest _)
         (setq my-sideline-git-blame--async-backend--last-stdout stdout)
         (save-match-data
           (-let* ((exit-code (plist-get obj :exit))
                   (revision
                    (-some--> (s-match "\\`\\(.*?\\) " stdout) (nth 1 it))))
             (cond
              ((or (and revision (string-match "^0+$" revision))
                   (and (/= 0 exit-code)
                        (string-match
                         "\\`fatal: no such path .* in HEAD$"
                         (plist-get obj :err))))
               (funcall callback (list "· Uncommited")))
              ((= 0 exit-code)
               (-let* ((author-name
                        (-some-->
                            (s-match "^author \\(.*\\)" stdout) (nth 1 it)))
                       (author-time
                        (-some-->
                            (s-match
                             "^author-time \\(.*\\)" stdout)
                          (nth 1 it) (string-to-number it)))
                       (author-tz
                        (-some-->
                            (s-match "^author-tz \\(.*\\)" stdout) (nth 1 it)
                            (replace-regexp-in-string "0+$" "" it)))
                       (current-tz (nth 1 (current-time-zone))))
                 (my-process-async
                  (list "git" "-P" "show" "-s" "--format=%s" revision)
                  (lambda (stdout &rest _)
                    (-let* ((commit-msg
                             (substring stdout 0 (string-search "\n" stdout)))
                            (str
                             (format "%s, %s%s · %s"
                                     author-name
                                     (format-time-string "%Y-%m-%d %H:%M"
                                                         author-time)
                                     (if (equal author-tz current-tz)
                                         ""
                                       (format " %s" author-tz))
                                     commit-msg)))
                      (put-text-property 0 1 'my-revision revision str)
                      (funcall callback (list str)))))))))))))))

;; See https://github.com/emacs-sideline/sideline-blame/issues/3

;;;###autoload
(defun my-sideline-git-blame (command)
  (cl-case
      command
    (`candidates (cons :async #'my-sideline-git-blame--async-backend))
    (`face 'my-sideline-git-blame-face)
    (`action
     (lambda (str &rest _)
       (-some-->
           (get-text-property 0 'my-revision str) (magit-show-commit it))))))

;;;###autoload
(defun my-sideline-inhibit-p ()
  (or (sideline-stop-p)
      (cond
       ;; Prevent diagnostics constantly flickering when typing 
       ((and buffer-file-name (buffer-modified-p))
        t)
       (:else
        nil))))

;;; my-functions-sideline.el ends here

(provide 'my-functions-sideline)

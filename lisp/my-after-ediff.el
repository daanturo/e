;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; Don't spawn another frame
(setq ediff-window-setup-function #'ediff-setup-windows-plain)

;; Split windows left/right
(setq ediff-split-window-function #'split-window-horizontally)

(my-windowpopup-set '(derived-mode ediff-mode) nil nil :no-modeline t)

;;; my-after-ediff.el ends here

(provide 'my-after-ediff)

;; Local Variables:
;; no-byte-compile: t
;; End:

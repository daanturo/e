;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-color-brightness (color-name)
  (my-mean (color-name-to-rgb color-name)))

;;;###autoload
(defun my-inverse-color (color-name)
  (--> (color-name-to-rgb color-name)
       (-map (-cut - 1 <>) it)
       (apply #'color-rgb-to-hex it)))

;;;###autoload
(defun my-list-themes-and-brightness ()
  (-let* ((current-theme (my-at custom-enabled-themes -1))
          (custom-safe-themes t))
    (prog1 (cl-sort
            (seq-remove
             #'null
             (cl-loop
              for theme in (custom-available-themes) collect
              (progn
                (load-theme theme)
                (let ((background
                       (face-attribute 'default :background nil nil)))
                  (list
                   theme
                   (/ (apply '+ (color-name-to-rgb background)) 3)
                   background)))))
            #'<
            :key #'cadr)
      (load-theme current-theme))))

;;;###autoload
(defun my-theme-installed-p (theme)
  "Check if THEME is installed manually.
Not much faster than `custom-available-themes', don't use this!"
  (-let* ((filenames
           (list (format "%s-theme.el" theme) (format "%s-theme.elc" theme)))
          (dirs (custom-theme--load-path)))
    (-some
     (lambda (dir)
       (-some
        (lambda (filename)
          (-let* ((path (file-name-concat dir filename)))
            (and (file-exists-p path) path)))
        filenames))
     dirs)))

;;;###autoload
(progn

  (defun my-get-current-theme ()
    (pcase my-emacs-kit
      (`doom doom-theme)
      (_ (car custom-enabled-themes))))

  (defun my-set-theme (theme &optional _now)
    (let* ((cet (list theme)))
      ;; doom's `doom--load-theme-a': having many themes, especially
      ;; with different light/dark schemes in `custom-enabled-themes'
      ;; may lead to conflicts
      (mapc #'disable-theme custom-enabled-themes)
      (pcase my-emacs-kit
        (`doom (setq doom-theme theme))
        (_
         (when theme
           (setq custom-enabled-themes cet))))
      (when (and theme)
        (dlet ((custom-safe-themes t))
          (load-theme theme 'no-confirm)))))
  ;; (customize-set-variable 'custom-enabled-themes cet)


  (defun my-set-theme-by-scheme (&optional scheme now verbose)
    (let* ((prev-enabled-themes (seq-copy custom-enabled-themes))
           (next-theme (my-get-theme-to-set scheme)))
      (my-set-theme next-theme now)
      (when verbose
        (message "`my-set-theme-by-scheme': changed to %s at %s%s"
                 next-theme (my-ISO-time)
                 (if (stringp verbose)
                     (format " (%S)" verbose)
                   "")))
      (when (and (equal
                  '("initial_terminal")
                  (delete-dups (mapcar 'terminal-name (frame-list)))))
        (my-add-hook-once
          'server-after-make-frame-hook
          (lambda ()
            (let* ((curr-scheme
                    (frame-parameter nil 'background-mode)))
              (cond
               ((not (equal scheme curr-scheme))
                (message
                 "`my-set-theme-by-scheme': next-theme needs to be set again after making frame, current scheme: %S (%s)"
                 curr-scheme (my-ISO-time))
                (my-set-theme next-theme now))
               (:else
                (run-hook-with-args 'enable-theme-functions
                                    next-theme)))))
          96))
      ;; when color scheme is changed, the menu bar doesn't adapt so
      ;; it looks out of place
      (when (and menu-bar-mode
                 prev-enabled-themes
                 (not
                  (equal custom-enabled-themes prev-enabled-themes)))
        (message
         "`%s': disabling `menu-bar-mode' since theme changed from %s to %s"
         'my-set-theme-by-scheme
         prev-enabled-themes
         custom-enabled-themes)
        (menu-bar-mode 0))
      next-theme))

  (defvar my-theme-installed-list--set nil)
  (defvar my-theme-installed-list--saved '())
  (defun my-theme-installed-list (&optional refresh)
    "Non-nil REFRESH: re-evaluate the slow `custom-available-themes'."
    (unless (or my-theme-installed-list--set refresh)
      (setq
       my-theme-installed-list--saved (custom-available-themes)
       my-theme-installed-list--set t))
    (cons nil my-theme-installed-list--saved))

  (defun my-get-theme-to-set (&optional scheme)
    (-->
     my-picked-theme-alist
     (alist-get scheme it)
     (-intersection it (my-theme-installed-list))
     car))

  (defun my-theme-auto-set (&optional now callback)
    "CALLBACK is called with the selected theme.
NOW: no-op."
    (declare (indent defun))
    (interactive (list (not current-prefix-arg)))
    (-let* ((hook-fn
             (lambda ()
               (my-theme-get-system-color-scheme
                (lambda (scheme)
                  (-let* ((theme
                           (my-set-theme-by-scheme
                            (or scheme) ;; 'dark

                            now)))
                    (when callback
                      (funcall callback theme))))))))
      (cond
       ;; the system's color scheme may not be correct immediately after startup
       ((daemonp)
        (my-add-hook-once 'server-after-make-frame-hook hook-fn))
       (:else
        (funcall hook-fn))))
    (add-hook 'my-idle-run-once-hook #'my-theme-auto-register-dbus)))

;;


(defun my-theme-auto-dbus--changed (path var value)
  (-when-let* ((_
                (and (equal path "org.freedesktop.appearance")
                     (equal var "color-scheme")))
               (val (car value))
               (scheme
                (if (equal '1 val)
                    'dark
                  'light)))
    (my-set-theme-by-scheme scheme 'now (vector path var value))))

;;;###autoload
(defun my-theme-auto-register-dbus ()
  (interactive)
  (dbus-register-signal
   :session
   "org.freedesktop.portal.Desktop"
   "/org/freedesktop/portal/desktop"
   "org.freedesktop.portal.Settings"
   "SettingChanged"
   #'my-theme-auto-dbus--changed))


;;; my-functions-theme.el ends here

(provide 'my-functions-theme)

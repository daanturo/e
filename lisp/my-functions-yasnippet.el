;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'yasnippet nil t)

(defvar-local my-yas-insert-snippet--last-modified nil)
;;;###autoload
(defun my-yas-insert-snippet ()
  (interactive)
  (yas-minor-mode)
  (let* ((mode major-mode)
         (templates (yas--all-templates (yas--get-snippet-tables)))
         (alist
          (-map
           (lambda (templ)
             (list
              (yas--template-name templ)
              (-some-->
                  (yas--template-key templ) (propertize it 'face 'marginalia-key))
              (-some-->
                  (yas--template-content templ)
                (format "%s" it)
                (my-string-in-mode it mode)
                (s-truncate 128 it)
                (my-stylize-string it :height (face-attribute 'default :height))
                (my-replace-newlines it "	"))))
           templates))
         (affix-fn
          (lambda (completions)
            (-->
             (-map
              (lambda (compl) (assoc compl alist)) completions)
             (my-minibuffer-annotation-align-affixation it)))))
    (let ((choice
           (completing-read
            "Snippet: "
            (lambda (string pred action)
              (if (eq action 'metadata)
                  `(metadata (affixation-function . ,affix-fn))
                (complete-with-action action alist string pred))))))
      ;; Allow previewing (undo previous preview first)
      (when (equal my-yas-insert-snippet--last-modified (buffer-modified-tick))
        (undo-only 1))
      (yas-expand-snippet
       (-find
        (lambda (templ) (equal (yas--template-name templ) choice)) templates))
      (setq my-yas-insert-snippet--last-modified (buffer-modified-tick)))))

;;;###autoload
(defun my-yasnippet-expandable-p ()
  (ignore-errors
    (yas-minor-mode)
    (yas-maybe-expand-abbrev-key-filter 'yas-expand)))

;;;###autoload
(defun my-yas--modes-to-activate--treesit-modes-a
    (func &optional mode &rest args)
  (cond
   (mode
    (apply func mode args))
   (t
    (-let* ((non-ts-modes (my-treesit-language->legacy-modes major-mode)))
      (-->
       `(,@(apply func mode args) ,@(--mapcat (apply func it args) non-ts-modes))
       (delete-dups it))))))

;;;###autoload
(cl-defmacro my-yasnippet-with-minor-yas-mode (&rest body)
 (declare (debug t) (indent defun))
 `(-let* ((mode-val-orig (bound-and-true-p yas-minor-mode)))
    (when (not mode-val-orig)
      (yas-minor-mode))
    (unwind-protect
        (progn
          ,@body)
      (when (not mode-val-orig)
        (yas-minor-mode 0)))))

(defvar my-yas--templates-for-key-at-point--force-distinct-a--notified nil)
;;;###autoload
(defun my-yas--templates-for-key-at-point--force-distinct-a (func &rest args)
  (-let* ((retval0 (apply func args))
          ((templates0 . rest-retval) retval0)
          (templates1 (-uniq templates0)))
    (cond
     ((/= (length templates0) (length templates1))
      (when (not my-yas--templates-for-key-at-point--force-distinct-a--notified)
        (my-notify-and-message
         "`yasnippet' returned duplicate templates! (`my-yas--templates-for-key-at-point--force-distinct-a')")
        (setq my-yas--templates-for-key-at-point--force-distinct-a--notified t))
      (cons templates1 rest-retval))
     (:else
      retval0))))

;;;###autoload
(defun my-yas-exit-all-snippets ()
  "Like `yas-exit-all-snippets' but try not to move point."
  (interactive)
  (save-excursion (yas-exit-all-snippets)))

;;; my-functions-yasnippet.el ends here

(provide 'my-functions-yasnippet)

;; Local Variables:
;; no-byte-compile: t
;; End:

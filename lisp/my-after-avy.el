;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)
(require 'avy)

;; `required' by lispy

(setq avy-single-candidate-jump t)

(advice-add #'avy-show-dispatch-help :override #'my-avy-show-dispatch-help)

;; leave alphabetic keys to actions
(setq avy-keys '(?1 ?2 ?3 ?4 ?5 ?6 ?7 ?8 ?9))

(my-add-list!
  'avy-dispatch-alist
  `(
    ;;
    (?, . my-avy-action-embark-act)
    (?e . my-avy-action-exchange-sexp) ; (aref (kbd "M-t") 0)
    ;;
    ))

(provide 'my-after-avy)

;; Local Variables:
;; no-byte-compile: t
;; End:

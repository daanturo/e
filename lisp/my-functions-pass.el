;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(autoload #'auth-source-search "auth-source")

;;;###autoload
(defun my-pass-get-pass-name-fresh (pass-name)
  "Like (`auth-source-pass-get' 'secret PASS-NAME).
But somehow calling the shell program is still faster?"
  (-->
   (my-process-sync->output-string (list "pass" "show" pass-name))
   (encode-coding-string it 'utf-8)))


(defvar my-pass--table (make-hash-table :test #'equal))
;;;###autoload
(defun my-pass-get-pass-name (pass-name &optional refresh)
  (when refresh
    (remhash pass-name my-pass--table))
  (with-memoization (gethash pass-name my-pass--table)
    (my-pass-get-pass-name-fresh pass-name)))

;;;###autoload
(defun my-auth-source-get (host user)
  "Like (`gptel-api-key-from-auth-source' HOST USER)."
  (-some-->
      (auth-source-search :host host :user user)
    (-first-item it)
    (plist-get it :secret)
    (funcall it)
    (encode-coding-string it 'utf-8)))

;;;###autoload
(defun my-auth-source-get-no-ask (host user)
  (save-window-excursion
    (condition-case _err
        (dlet ((epg-pinentry-mode 'cancel))
          (my-auth-source-get host user))
      ((file-error) nil))))

;;;###autoload
(defun my-get-cached-auth-or-pass (host user &optional pass-name)
  "Try `auth-source-search', when not locked, fallback to `my-pass-get-pass-name'.
Because `auth-source-search', if unlocked, is significantly
faster than pass. For pass, get PASS-NAME or HOST/USER."
  (or (my-auth-source-get-no-ask host user)
      (my-pass-get-pass-name (or pass-name (concat host "/" user)))))

;;; my-functions-pass.el ends here

(provide 'my-functions-pass)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'magit)

;;;###autoload
(defun my-magit/switch-project-and-status ()
  "Select a project and run `magit-status' there."
  (interactive)
  (magit-status-setup-buffer (project-prompt-project-dir)))

;;;###autoload
(defun my-magit-diff-commit-or-branch-at-point-to-HEAD ()
  (interactive)
  (magit-diff-range
   (format "%s.." (magit-branch-or-commit-at-point))))

;;;###autoload
(defun my-magit-discard-no-trash ()
  (interactive)
  (dlet ((magit-delete-by-moving-to-trash nil))
    (magit-discard)))

;;;###autoload
(defun my-magit-log-surrounding-commits (commit branch)
  "(`magit-log-merged' COMMIT BRANCH)."
  (declare (interactive-only t))
  (interactive
   (list (or (magit-commit-at-point)
             (magit-read-branch-or-commit "`my-magit-log-surrounding-commits': "))
         (or
          ;; (and (= 1 (length magit-buffer-revisions))
          ;;      (car magit-buffer-revisions))
          (magit-branch-at-point)
          (magit-get-current-branch)
          (magit-read-branch "Branch: "))))
  (apply #'magit-log-merged commit branch
         (magit-log-arguments)))

;;;###autoload
(defun my-magit-read-branch-or-commit-initial-input-as-commit-at-point--around-a (func &rest args)
  (let ((commit (magit-commit-at-point)))
    (minibuffer-with-setup-hook
        (lambda ()
          (when (and commit
                     (not (string-empty-p commit))
                     (string-empty-p (minibuffer-contents-no-properties)))
            (insert commit)))
      (apply func args))))

(defvar my-magit-auto-display-process-buffer-inhibit-functions '())

;;;###autoload
(defun my-magit-auto-display-process-buffer (&rest _)
  "Automatically display the process buffer when it is updated.
https://github.com/magit/magit/wiki/Tips-and-Tricks#automatically-displaying-the-process-buffer"
  (when (not
         (-some
          'funcall my-magit-auto-display-process-buffer-inhibit-functions))
    (dlet ((magit-display-buffer-noselect t))
      (magit-process-buffer))))

;;;###autoload
(cl-defmacro my-magit-with-noninteractive (&rest body)
  (declare (debug t) (indent defun))
  `(dlet ((my-magit-auto-display-process-buffer-inhibit-functions nil)
          (magit-save-repository-buffers nil)
          (inhibit-message t))
     ,@body))

;;;###autoload
(defun my-magit-with-noninteractive-a (func &rest args)
  (my-magit-with-noninteractive (apply func args)))

;;;###autoload
(defun my-magit-never-save-repository-buffers-h (&rest _)
  (setq-local magit-save-repository-buffers nil))

;;;###autoload
(defun my-magit-cycle-process-buffer-window ()
  (interactive)
  (my-cycle-popup-buffer-window
   (magit-process-buffer 'nodisplay)))

;;;###autoload
(defun my-magit-blame-show-commit-at-point-current-line (&optional revision)
  "With non-nil prefix arg, prompt for REVISION."
  (interactive
   (list
    (and current-prefix-arg
         (vc-read-revision "my-magit-blame-show-commit-at-point-current-line : "))))
  (magit-show-commit (my-vc-git-blame-commit-hash revision)))

;;;###autoload
(defun my-magit-status-in-parent-directory ()
  (interactive)
  (-let* ((dir
           (my-project-root (file-name-parent-directory (my-project-root)))))
    (magit-mode-bury-buffer)
    (magit-status dir)))

;;;###autoload
(defun my-git-commit-display-window-on-right-h ()
  (-when-let* ((_
                ;; (called-interactively-p 'any)
                (member last-command '(magit-commit-create)))
               (cur-win (selected-window))
               (rwd (window-right cur-win))
               (rbuf (window-buffer rwd))
               (_
                (progn
                  (and rwd
                       ;; (null (window-left cur-win))
                       (null (window-right rwd))
                       (member
                        (buffer-local-value 'major-mode rbuf)
                        '(magit-status-mode))
                       (require 'ace-window nil 'noerror)))))
    (aw-swap-window rwd)))


(provide 'my-functions-magit)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-notetaking-dashboard ()
  (interactive)
  (deft)
  (call-interactively #'my-org-roam-transient))

;;; my-functions-note-taking.el ends here

(provide 'my-functions-note-taking)

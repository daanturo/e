;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; NOTE to delete cell, consider using `ein:worksheet-kill-cell' instead

;; NOTE: [error] Login to http://127.0.0.1:8888 failed, error-thrown (error http 401), raw-header HTTP/1.1 401 Unauthorized
;; https://github.com/millejoh/emacs-ipython-notebook/issues/867:
;; Removing all obsolete files from ~/.local/share/jupyter/runtime solved the problem.

(set-popup-rule!
 (rx-to-string `(seq string-start (?  space)
                     "*ein:"
                     (or (seq (*? anything) ".ipynb")
                         (seq "notebooklist" (*? anything)))
                     "*"
                     (?  "[" (*? anything) "]")
                     string-end))
 :ignore t)

;; (with-eval-after-load 'ein-notebook
;;   (defvar my-ein-notebook-km-map
;;     (my-for [(key . def) (my-keymap-bindings ein:notebook-mode-map)
;;              :when (symbolp def)
;;              :let [km-cmd (symbol-name def)]
;;              :when (string-suffix-p "-km" km-cmd)
;;              :let [orig-cmd (intern (string-remove-suffix "-km" km-cmd))]
;;              :when (commandp orig-cmd)]
;;       (list key orig-cmd))))

(my-bind :map 'ein:notebook-mode-map

  "C-<up>" #'my-ein-beg-of-cell
  "C-<down>" #'my-ein-end-of-cell

  "<f10>" #'my-ein-run|execute-region-or-line
  "C-<f10>" #'my-ein-run|execute-selected-and-after-cells
  "C-<f8>" #'ein:worksheet-execute-all-cells-above
  "C-<f9>" #'ein:worksheet-execute-all-cells
  "C-c '" #'my-ein-edit-cell
  "C-c <C-m>" #'my-ein-merge-cells
  "C-c C-v" #'ein:worksheet-yank-cell
  "M-H" #'my-ein-mark-cell
  '("C-RET" "C-<return>") #'ein:worksheet-execute-cell
  '("C-S-i" "<C-S-i>") #'my-ein-format-cell
  '("M-RET" "M-<return>") #'ein:worksheet-execute-cell-and-insert-below
  '("S-RET" "S-<return>") #'ein:worksheet-execute-cell-and-goto-next
  '("RET" "<return>") #'my-ein-new-line-and-indent-and-maybe-open

  "C-c C-a" #'my-ein-worksheet-insert-cell-above
  "C-c C-b" #'my-ein-worksheet-insert-cell-below

  ;;
  )

;; move cells when outside
(let ((filter (lambda (cmd) (and (equal major-mode 'fundamental-mode) cmd))))
  (my-bind :map 'ein:notebook-mode-map
    "M-<up>" `(menu-item "" my-ein-move-cell-up :filter ,filter)
    "M-<down>" `(menu-item "" my-ein-move-cell-down :filter ,filter)))

;; visualizations
(let* ((filter (lambda (cmd) (and (image-at-point-p) cmd))))
  (my-bind :map 'ein:notebook-mode-map :vi '(global normal)
    '("w" "W")
    `(menu-item "" my-save-and-open-by-default-application-image-at-point :filter ,filter)))

(my-bind :map 'ein:notebook-mode-map "M-l a" #'ein:worksheet-insert-cell-above-km)
(my-bind :map 'ein:notebook-mode-map "M-l b" #'ein:worksheet-insert-cell-below-km)

;;;###autoload
(progn
  (my-maydefun my-ein-get-docstring (func)
    ;; (concat (documentation func) "\n\n"
    ;;         "`" (symbol-name func) "'")
    (my-ensure-string-suffix (format "\n\n`%s'." func)
                             (documentation func)))
  (my-maydefun my-code-cells-ein-key-h ()
    (add-hook 'my-execu-direct-alist '(code-cells-mode . my-ein-auto-open-notebook))))

;;;###autoload
(my-accumulate-config-collect
 ;; generate useful command docstrings, but no effects?
 (advice-add #'ein:get-docstring :override #'my-ein-get-docstring)
 (when (fboundp #'ein:run)
   (my-add-hook/s '(code-cells-mode-hook)
     '(my-code-cells-ein-key-h my-ein-notify-auto-notebook-key))))

;; .ipynb files may contain too long lines
(my-add-mode-hooks-and-now 'ein:ipynb-mode '(my-disable-so-long my-ein-notify-auto-notebook-key))

(defvar my-ein-notebook-imenu-generic-expression
  `(("raw" "^raw:\n\\(.*\\)" 1)
    ("markdown" "^markdown:\n\\(.*\\)" 1)
    ("code" "^\\(In \\[[0-9]*]:\n.*\\)" 1)))

(defvar my-ein-auto-default-language-kernel-name "python3")

(defun my-ein--auto-default-language-h (&optional notebook &rest _)
  (when my-ein-auto-default-language-kernel-name
    (-when-let* ((notebook (or notebook (ein:get-notebook))))
      (condition-case err
          (ein:$kernelspec-language (ein:$notebook-kernelspec nb))
        (error
         (let* ((kernelspec
                 (ein:get-kernelspec
                  (ein:$notebook-url-or-port notebook)
                  my-ein-auto-default-language-kernel-name)))
           (setf (ein:$notebook-kernelspec notebook) kernelspec))
         (message "`my-ein-auto-default-language-kernel-name': %s"
                  my-ein-auto-default-language-kernel-name))))))

;; avoid message every command: "defaulting language to python"
(add-hook 'my-ein-auto-open-notebook-hook #'my-ein--auto-default-language-h)

(defun my-ein:notebook-mode-hook-h ()
  (when (fboundp #'elpy-enable)
    (my-lang-python-elpy-completion-mode))
  (display-line-numbers-mode)
  (setq-local my-buffer-filename-function #'my-ein-notebook-full-path)
  (setq-local my-buffer-filename-can-open-with-find-file nil)
  (setq-local revert-buffer-function #'my-ein-revert|refresh-notebook)
  ;; (setq-local imenu-generic-expression
  ;;             my-ein-notebook-imenu-generic-expression)
  (my-add-hook/s
    'imenu-generic-expression my-ein-notebook-imenu-generic-expression)
  (setq-local
   outline-regexp (my-imenu->outline-regexp)
   outline-minor-mode-highlight nil))

(my-add-mode-hook-and-now 'ein:notebook-mode 'my-ein:notebook-mode-hook-h)

(my-context-menu-add-local 'ein:notebook-mode 'my-ein-notebook-context-menu-beg -100)
(my-context-menu-add-local 'ein:notebook-mode 'my-ein-notebook-context-menu-end 100)

(my-add-mode-hooks-and-now '(ein:notebook-mode ein:notebooklist-mode)
  '(my-sidebar-local-enable-h))

;; (setq ein:jupyter-server-use-subcommand "lab")
(setq ein:output-area-inlined-images t)
(setq-default ein:markdown-enable-math t)

;; don't use "~" when there is an .ipynb file there
(advice-add #'ein:process-suitable-notebook-dir :override #'my-ein-suitable-notebook-dir)

;; before EIN's built-in added hook have a chance to ask
(add-hook 'kill-emacs-hook 'my-ein-backup-unsaved-notebooks -100)

;; distinguish code and markdown cells
(my-custom-theme-set-faces (alist-get 'dark my-picked-theme-alist)
  `(ein:codecell-input-area-face
    :background "#282828" :extend t)
  `((ein:basecell-input-area-face ein:cell-output-area)
    :background "#404040" :extend t)
  `(ein:textcell-input-area-face
    :height 1.125 :inherit 'ein:basecell-input-area-face :extend t))

;; (add-hook 'my-elisp-tree-sitter-excluded-modes 'ein:markdown-mode)
;; (add-hook 'my-elisp-tree-sitter-excluded-modes 'poly-ein-mode)
;; (add-hook 'my-elisp-tree-sitter-excluded-modes 'ein:notebook-mode)
(my-safe-add-hook/s '(ein:markdown-mode-hook) '(+word-wrap-mode))

(my-context-menu-add-local 'ein:ipynb-mode 'my-jupyter-json-context-menu 1)

;; (add-hook '+nav-flash-exclude-modes 'ein:ipynb-mode)
(add-hook 'doom-detect-indentation-excluded-modes 'ein:ipynb-mode)
(add-hook 'my-lsp-exlude-mode-list 'ein:ipynb-mode)

(advice-add #'ein:notebook-save-notebook :around #'my-with-deferred-gc-around-a)

;; (advice-add #'ein:notebook-save-notebook-command-km :around #'my-evil-save-state-notify-a)
(with-eval-after-load 'ein-notebook
  (save-match-data
    (cl-loop
     for cmd in (-uniq (flatten-tree ein:notebook-mode-map)) when
     (and (symbolp cmd)
          (commandp cmd)
          (string-match "^ein:.*-km$" (symbol-name cmd)))
     do (advice-add cmd :around #'my-evil-save-state-notify-a))))

;; why?...
(advice-add #'ein:worksheet--jigger-undo-list :around #'my-ein:worksheet--jigger-undo-list--a)

;; prevent surprising jumps
(advice-add #'ein:worksheet-kill-cell-km :around #'my-with-save-excursion-a)

(provide 'my-after-ein)

;; Local Variables:
;; no-byte-compile: t
;; End:

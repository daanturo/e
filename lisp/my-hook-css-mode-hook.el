;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; use `colorful-mode': no conflict with `hl-line-mode'
(setq css-fontify-colors nil)

;;; my-hook-css-mode-hook.el ends here

(provide 'my-hook-css-mode-hook)

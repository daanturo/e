;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'eat)

;;;###autoload
(defun my-eat-renew ()
  (interactive)
  (-let* ((name (buffer-name)))
    (dlet ((eat-buffer-name name))
      (eat))))

;;;###autoload
(defun my-eat-emacs-mode-p ()
  (not (or eat--semi-char-mode eat--char-mode)))

;;;###autoload
(defun my-eat-switch-mode-by-evil-h ()
  (when (provided-mode-derived-p major-mode 'eat-mode)
    (cond
     ((member evil-state (cons nil my-evil-typing-state-list))
      (when (my-eat-emacs-mode-p)
        (eat-semi-char-mode)))
     (t
      (when eat--semi-char-mode
        (eat-emacs-mode))))))

(provide 'my-functions-eat)

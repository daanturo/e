;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(setq-default gac-automatically-push-p t)
(setq-default gac-debounce-interval 90)

(defvar my-git-auto-commit-push-notify-error--table
  '())
(defun my-git-auto-commit-push-notify-error-before-a (buf &rest _)
  (-when-let* ((root (vc-git-root (buffer-file-name buf))))
    (cond
     ((member root my-git-auto-commit-push-notify-error--table)
      nil)
     (:else
      (dlet ((default-directory root))
        (my-ssh-with-batch-mode
          (-let* ((cmdargs `("git" "push")))
            (my-process-async
             cmdargs
             (lambda (stdout obj &rest _)
               (when (/= 0 (plist-get obj :exit))
                 (my-notify-and-message
                  (format "`gac-push' at %S, error!" root)))))))
        (push root my-git-auto-commit-push-notify-error--table))))))

(advice-add #'gac-push :before #'my-git-auto-commit-push-notify-error-before-a)

;; TODO: report that when filename and CWD's "true name" statuses are different
;; (such as symlinks), the result `gac-relative-file-name' is affected
(defun my-git-auto-commit-relative-file-name (filename &rest _)
  (-let* ((filename* (file-truename filename))
          (root (my-project-root filename*)))
    (file-relative-name filename* root)))
(advice-add
 #'gac-relative-file-name
 :override #'my-git-auto-commit-relative-file-name)


;;; my-after-git-auto-commit-mode.el ends here

(provide 'my-after-git-auto-commit-mode)
;; Local Variables:
;; no-byte-compile: t
;; End:

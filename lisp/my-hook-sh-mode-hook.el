;; -*- lexical-binding: t; -*-

(require 'dash)

(require '00-my-core-macros)

;; ;;;###autoload
;; (my-accumulate-config-collect
;;  (add-to-list 'interpreter-mode-alist '("bash" . my-bash-ts-or-sh-mode)))

;;;###autoload
(defun my-evaluate-async-shell-command-region (beg end)
  (interactive (and (use-region-p)
                    (list (region-beginning) (region-end))))
  (async-shell-command (buffer-substring-no-properties beg end)))

(my-evalu-set-handler '(sh-mode) #'project-shell)
;; :region #'my-evaluate-async-shell-command-region)

(my-bind :map '(sh-mode-map) [remap my-evalu-dwim] #'my-evalu-region)

(provide 'my-hook-sh-mode-hook)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-org-agenda-all ()
  "Display Org agenda and all todos."
  (interactive)
  (org-agenda nil "n"))

;;;###autoload
(defun my-org-sort-entries-by-time-reversed (&optional ascending)
  (interactive "P")
  (save-excursion
    (outline-up-heading 1)
    (org-sort-entries nil (string-to-char (if ascending "t" "T")))))

;;;###autoload
(defun my-org-odt-libreoffice-export-to-docx ()
  "Export current buffer to a DOCX file. Require LibreOffice."
  (interactive)
  (let ((file-name-no-ext (file-name-sans-extension (org-odt-export-to-odt))))
    (when-let (libreoffice (or (executable-find "libreoffice")
                               (my-flatpak-executable-find "org.libreoffice.LibreOffice")))
      (shell-command (my-s$ "${libreoffice} --convert-to docx ${file-name-no-ext}.odt"))
      (delete-file (concat file-name-no-ext ".odt"))
      (browse-url-xdg-open (concat file-name-no-ext ".docx")))))

;;;###autoload
(cl-defun my-org-export-async (&optional callback export-function &key quiet)
  (interactive)
  ;; ignore text-properties, they may cause problems such as by
  ;; `highlight-indent-guides-mode'
  (-let* ((filename buffer-file-name)
          (sentinel
           (my-with-advice
             #'buffer-string
             :override #'my-buffer-string-no-properties
             (if export-function
                 (funcall export-function)
               (org-latex-export-to-pdf 'async)))))
    (add-function :around (var sentinel)
                  (lambda (fn &rest args)
                    (unwind-protect
                        (apply fn args)
                      (unless quiet
                        (message "`my-org-export-async' %S finshed!"
                                 filename))
                      (when callback
                        (funcall callback)))))))

(defvar-local my-org-export-to-pdf-async-symbol-function nil
  "Like `org-latex-export-to-pdf', the first arg is ASYNC.")
;;;###autoload
(defun my-org-export-to-pdf-async (&optional beamer)
  "Asynchronously export current `org' file to pdf using Latex,
respect local `my-org-export-to-pdf-async-symbol-function' or non-nil
optional prefix argument BEAMER."
  (interactive "P")
  (-let* ((exporter
           (cond
            (my-org-export-to-pdf-async-symbol-function
             my-org-export-to-pdf-async-symbol-function)
            (beamer
             #'org-beamer-export-to-pdf)
            (:else
             #'org-latex-export-to-pdf)))
          (sentinel
           (lambda (&optional _proc event)
             (message "`%s' %s : %s."
                      #'my-org-export-to-pdf-async
                      (abbreviate-file-name buffer-file-name)
                      (string-trim event)))))
    (message "`%s'-ing: `%s'" #'my-org-export-to-pdf-async exporter)
    (my-org-export-async
     sentinel
     (if exporter
         (lambda () (funcall exporter 'async))))))

;;;###autoload
(define-minor-mode my-org-auto-latex-export-to-pdf-mode
  "Automatically export after saving asynchronously."
  :global nil
  (if my-org-auto-latex-export-to-pdf-mode
      (add-hook 'after-save-hook #'my-org-export-to-pdf-async nil 'local)
    (remove-hook 'after-save-hook #'my-org-export-to-pdf-async 'local)))

;;;###autoload
(defun my-org-confirm-babel-evaluate (lang _body)
  "Do not confirm evaluation of _BODY if LANG is in a defined list."
  (not (member lang '(
                      "plantuml"))))


;;;###autoload
(defun my-org-bounds-of-inner-code-block ()
  (my-bounds-by-regexps "^#\\+begin_src.*?\n"
                        "\n#\\+end_src$"
                        :ignore-case t))
;;;###autoload
(put 'my-org-inner-code-block 'bounds-of-thing-at-point
     #'my-org-bounds-of-inner-code-block)
;;;###autoload
(defun my-org-mark-inner-code-block ()
  (interactive)
  (my-mark-bounds-as-cons-maybe
   (my-org-bounds-of-inner-code-block)))

;;;###autoload
(defun my-org-insert-stars-to-make-heading (arg)
  (interactive "p")
  (if (< 0 arg)
      (dotimes (_ arg)
        (cond
         ((string-match-p
           "^\\*+ " (buffer-substring-no-properties (pos-bol) (pos-eol)))
          (my-insert-at-buffer-position (pos-bol) "*"))
         (t
          (my-insert-at-buffer-position (pos-bol) "* "))))
    (dotimes (_ (- arg))
      (save-excursion
        (beginning-of-line)
        (when (looking-at-p (regexp-quote "*"))
          (delete-char 1))
        (when (looking-at-p (regexp-quote " "))
          (delete-char 1))))))

;; https://orgmode.org/manual/Emphasis-and-Monospace.html

;;;###autoload
(defun my-org-emphasize-bold ()
  (interactive)
  (org-emphasize (string-to-char "*")))
;;;###autoload
(defun my-org-emphasize-italic ()
  (interactive)
  (org-emphasize (string-to-char "/")))
;;;###autoload
(defun my-org-emphasize-underline ()
  (interactive)
  (org-emphasize (string-to-char "_")))
;;;###autoload
(defun my-org-emphasize-verbatim ()
  (interactive)
  (org-emphasize (string-to-char "=")))
;;;###autoload
(defun my-org-emphasize-code ()
  (interactive)
  (org-emphasize (string-to-char "~")))
;;;###autoload
(defun my-org-emphasize-strike-through ()
  (interactive)
  (org-emphasize (string-to-char "+")))

;;;###autoload
(defun my-org-emphasize-remove-emphasis ()
  (interactive)
  (org-emphasize (string-to-char " ")))

(defmacro my-org--tranpose-with-fallback (main-body fall-back-body)
  (declare (debug t) (indent defun))
  `(-let* ((pt0 (point-marker)))
     (condition-case _err
         (progn
           ,main-body)
       (error (goto-char pt0) ,fall-back-body))))

;;;###autoload
(defun my-org-tranpose-words-with-fallback ()
  (interactive)
  (my-org--tranpose-with-fallback
    (org-transpose-words)
    (transpose-words 1)))

;;;###autoload
(defun my-org-latex-preview-fragment-quiet (&rest args)
  (my-with-advice
    #'message
    :override #'ignore (apply #'org-latex-preview args)))

;;;###autoload
(defun my-org-latex-preview-fragment-whole-buffer ()
  (interactive)
  (my-org-latex-preview-fragment-quiet '(16)))

(defvar my-org-auto-latex-preview-fragment-delay 5.0)

(defvar my-org-auto-latex-preview-fragment-mode--timer nil)
(defvar-local my-org-auto-latex-preview-fragment-mode--tick nil)
(defvar my-org-auto-latex-preview-fragment-mode)

(defun my-org-auto-latex-preview-fragment-mode--idle-run ()
  (dolist (wd (window-list))
    (with-selected-window wd
      (when (and my-org-auto-latex-preview-fragment-mode (derived-mode-p '(org-mode)))
        (-let* ((tick (buffer-modified-tick)))
          (when (not (equal tick my-org-auto-latex-preview-fragment-mode--tick))
            (my-org-latex-preview-fragment-whole-buffer)
            (setq my-org-auto-latex-preview-fragment-mode--tick tick)))))))

;;;###autoload
(define-minor-mode my-org-auto-latex-preview-fragment-mode
  nil
  :global
  t
  (cond
   (my-org-auto-latex-preview-fragment-mode
    (letrec ((timer
              (run-with-idle-timer
               my-org-auto-latex-preview-fragment-delay
               'repeat
               #'my-org-auto-latex-preview-fragment-mode--idle-run)))
      (setq my-org-auto-latex-preview-fragment-mode--timer timer)))
   (:else
    (cancel-timer my-org-auto-latex-preview-fragment-mode--timer))))

;;;###autoload
(defun my-org-display-inline-images-maybe ()
  (when (and (derived-mode-p '(org-mode))
             (or org-startup-with-inline-images org-inline-image-overlays))
    (org-display-inline-images)))

(defvar my-org-wikipedia-to-org-latex-regexp-table
  (append
   (list
    (vector
     (rx-to-string
      `(seq
        (or "<math display=\"block\">"
            "<math display=block>"
            (seq bol (?  ":") (* " ") "<math>"))
        (* " ") (group (*? anychar)) (* " ") "</math>"))
     "\n\\\\[ \\1 \\\\]\n")
    (vector
     (rx-to-string `(seq "<math>" (group (*? anychar)) "</math>"))
     "\\\\( \\1 \\\\)")
    (vector " &ge; " " ≥ ")
    (vector " &le; " " ≤ ")
    (vector "&nbsp;\\([^z-a]*?\\)&nbsp;" " \\1 ")
    ;; '''...''':  bold
    (vector "'''\\([^z-a]*?\\)'''" "*\\1*")
    ;; ''..'': italic, remove them
    (vector "''\\([^z-a]*?\\)''" "\\1")
    (vector "<ref .*? />" "")
    (vector "<ref\\( .*\\)?>[^z-a]*?</ref>" "")
    (vector "<sub>\\([^z-a]*?\\)</sub>" "\\\\( _{\\1} \\\\)")
    (vector "<sup>\\([^z-a]*?\\)</sup>" "\\\\( ^{\\1} \\\\)")
    (vector "^=====\\([^=].*[^=]\\)=====$" "**** \\1\n")
    (vector "^====\\([^=].*[^=]\\)====$" "*** \\1\n")
    (vector "^===\\([^=].*[^=]\\)===$" "** \\1\n")
    (vector "^==\\([^=].*[^=]\\)==$" "* \\1\n")
    (vector "{{Equationref|[0-9]+}}" "")
    (vector "{{NumBlk||\\([^z-a]*?\\)\n|}}" "\\1\n")
    (vector "{{\\(?:Nowrap\\|nowrap\\)|\\([^z-a]*?\\)}}" "\\1")
    (vector "{{math|''\\(.*?\\)''}}" "\\\\( \\1 \\\\)")
    (vector "{{math|\\(.*?\\)}}" "\\\\( \\1 \\\\)")
    (vector "{{mvar|\\(.*?\\)}}" "\\1")
    (vector "{{r|[^ ]*?}}" ""))
   (-map
    (-lambda ((name small . _))
      (-let* ((replacing (format "&%s;" name)))
        (vector replacing small)))
    my-greek-letters)))

;;;###autoload
(defun my-org-replace-wikipedia-math-formatting-with-latex-and-org ()
  (declare (interactive-only t))
  (interactive)
  (-let* ((lst my-org-wikipedia-to-org-latex-regexp-table)
          (all-regexp
           (my-regexp-or (-map (-lambda ([old _]) old) lst)))
          (_
           (progn
             (highlight-regexp all-regexp)))
          (yes (y-or-n-p "Replace highlighted?")))
    (unwind-protect
        (when yes
          (save-excursion
            (combine-change-calls (point-min) (point-max)
              (dolist (replacing lst)
                (-let* (([old new] replacing))
                  (my-regexp-replace-in-buffer old new))))))
      (unhighlight-regexp all-regexp))))

;;;###autoload
(defun my-org-with-lualatex-a (func &rest args)
  (dlet ((org-latex-compiler "lualatex"))
    (apply func args)))

;;;###autoload
(defun my-org-latex-export-to-file-body-only ()
  (interactive)
  (-let* ((export-file (org-latex-export-to-latex)))
    (with-current-buffer (dlet ((revert-without-query '("")))
                           (find-file-noselect export-file))
      (my-lang-tex-keep-body-only)
      (save-buffer 0))))

;;;###autoload
(defun my-org-link-preview-toggle-all ()
  (interactive)
  (require 'ol)
  (cond
   ((seq-empty-p org-link-preview-overlays)
    (org-link-preview-region t t (point-min) (point-max)))
   (:else
    (org-link-preview-clear (point-min) (point-max)))))

;;; my-functions-org.el ends here

(provide 'my-functions-org)

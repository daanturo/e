;; -*- lexical-binding: t; -*-

(require 'dash)

;; (my-backtrace)

(my-bind :map 'comint-mode-map "C-S-k" #'my-force-delete-region-or-buffer-contents)

(provide 'my-hook-comint-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

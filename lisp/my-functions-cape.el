;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; https://github.com/minad/corfu/issues/337

;;;###autoload
(defun my-cape-dabbrev-a (func &rest args)
  (declare)
  ;; (dlet ((dabbrev-abbrev-char-regexp "\\(\\sw\\|\\s_\\)[^0-9]")
  ;;        (dabbrev--abbrev-char-regexp dabbrev-abbrev-char-regexp))
  ;;   (apply func args))

  ;; sometimes func is nil?
  (when func
    (cond
     ((called-interactively-p t)
      (apply func args))
     ((my-looking-back-symbol-with-only-digits-p)
      nil)
     (t
      (apply func args)))))


;;;###autoload
(defun my-balanced?-cape-line ()
  (interactive)
  ;; Other completion UIs may activate completion AFTER this function
  (dlet ((completion-in-region-function (default-value 'completion-in-region-function)))
    (cape-line 'interactive))
  (my-try-to-balance-current-line-by-deleting-from-right))

;; ;;;###autoload
;; (defun my-cape-interactive-call ()
;;   (declare (interactive-only t))
;;   (interactive)
;;   (-let* ((capfs
;;            (-->
;;             (completing-read-multiple
;;              "`my-cape-interactive-call': "
;;              #'help--symbol-completion-table
;;              (lambda (f) (or (fboundp f) (get f 'function-documentation)))
;;              nil
;;              nil
;;              nil
;;              ;; (nth 0 completion-at-point-functions)
;;              )
;;             (-map #'intern it))))
;;     (apply #'cape-interactive capfs)))

;;;###autoload
(defun my-cape-wrap-noninterruptible-a (func &rest args)
  (cape-wrap-noninterruptible (lambda () (apply func args))))

;;;###autoload
(defun my-cape-wrap-silent-a (func &rest args)
  (cape-wrap-silent (lambda () (apply func args))))

;;; my-functions-cape.el ends here

(provide 'my-functions-cape)

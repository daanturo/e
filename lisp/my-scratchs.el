;; -*- lexical-binding: t; -*-

(require 'dash)

(defgroup my-scratchs nil
  nil)

(defcustom my-scratchs-save-directory
  (locate-user-emacs-file "local/my-scratchs")
  nil)

(defun my-scratchs--get-dir (&optional directory)
  (-let* ((proj-root
           (-some-->
               (project-current nil directory) (project-root it))))
    (-->
     (or proj-root directory default-directory)
     (file-truename it)
     (directory-file-name it))))

(defun my-scratchs--get-save-path (suffix &optional directory)
  (-let* ((dir (my-scratchs--get-dir directory))
          (file
           (-->
            dir (url-hexify-string it) (format "%s#%s" it suffix))))
    (file-name-concat my-scratchs-save-directory file)))

(defvar-local my-scratchs--save-path nil)

(defvar buffer-auto-save-file-name)

(defun my-scratchs--mkdir ()
  (when my-scratchs--save-path
    (-let* ((parent
             (file-name-parent-directory my-scratchs--save-path)))
      (make-directory parent t))))


(defun my-scratchs-save (&rest _)
  (interactive)
  (when my-scratchs--save-path
    (my-scratchs--mkdir)
    (write-region (point-min) (point-max) my-scratchs--save-path
                  nil)))

(define-minor-mode my-scratchs-mode
  nil
  :global nil
  :keymap
  '(([remap save-buffer] . my-scratchs-save))
  (setq-local delete-auto-save-files nil)
  (add-hook 'kill-buffer-hook #'my-scratchs-save nil 'local)
  (auto-save-mode))


(defcustom my-scratchs-major-mode-remap
  '((emacs-lisp-mode . lisp-interaction-mode))
  nil)

(defun my-scratchs--get-buffer-name (dir mamode)
  (-let* ((save-path (my-scratchs--get-save-path mamode dir))
          (base-buf-name
           (format "*%s %s %s*"
                   'my-scratchs
                   (file-name-nondirectory (directory-file-name dir))
                   mamode)))
    ;; in case projects share the same name but different parent paths
    (cond
     ((not (get-buffer base-buf-name))
      base-buf-name)
     ((-let* ((existing
               (-some
                (lambda (buf)
                  (with-current-buffer buf
                    (and my-scratchs-mode
                         (equal save-path my-scratchs--save-path)
                         buf)))
                (buffer-list))))
        (and existing (buffer-name existing))))
     (:else
      (generate-new-buffer-name base-buf-name)))))


;;;###autoload
(defun my-scratchs (&optional directory)
  (interactive)
  (-let* ((dir (my-scratchs--get-dir directory))
          (mamode
           (or (alist-get major-mode my-scratchs-major-mode-remap
                          nil nil
                          (lambda (key _)
                            (provided-mode-derived-p major-mode key)))
               major-mode))
          (save-path (my-scratchs--get-save-path mamode directory))
          (buf-name (my-scratchs--get-buffer-name dir mamode))
          (buf (get-buffer-create buf-name)))
    (with-current-buffer buf
      (cd dir) ; prefer at project root
      (when (and (equal major-mode 'fundamental-mode)
                 (functionp mamode))
        (funcall mamode))
      (when (and (equal "" (buffer-string)) (file-exists-p save-path))
        (insert-file-contents save-path))
      (my-scratchs-mode)
      (setq-local my-scratchs--save-path save-path)
      (setq-local buffer-auto-save-file-name save-path)
      (my-scratchs--mkdir))
    (pop-to-buffer buf)
    buf))


(with-eval-after-load 'evil
  (add-to-list
   'evil-buffer-regexps `(,(format "^\\*%s " 'my-scratchs) . insert)))


;;; my-scratchs.el ends here

(provide 'my-scratchs)

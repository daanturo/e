;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(cl-defun my-flymake-diagnostic-messages-at-point (&optional pos &key copy)
  "Return messages at POS, called interactively: COPY them."
  (interactive (list nil :copy t))
  (-let* ((pos (or pos (point)))
          (txts
           (--> (flymake-diagnostics pos) (-map #'flymake-diagnostic-text it)))
          (str (string-join txts "\n")))
    (when copy
      (kill-new str)
      (message "%s" str))
    txts))

;;;###autoload
(defun my-flymake-diagnostics-buffer-peek-prev (n)
  (interactive "p")
  (save-selected-window
    (previous-line n)
    (flymake-goto-diagnostic (point))
    (recenter)))

;;;###autoload
(defun my-flymake-diagnostics-buffer-peek-next (n)
  (interactive "p")
  (my-flymake-diagnostics-buffer-peek-prev (- n)))

(provide 'my-functions-flymake)

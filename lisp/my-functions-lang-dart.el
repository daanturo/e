;; -*- lexical-binding: t; -*-

(require 'dash)

(my-autoload-functions 'flutter #'(flutter--running-p))
(my-autoload-functions 'hover #'(hover--hot-reload))

;;;###autoload
(defun my-lang-flutter-run-web ()
  (interactive)
  (flutter-run "-d web-server"))

;;;###autoload
(defun my-lang-flutter-show-cli ()
  (interactive)
  (pop-to-buffer flutter-buffer-name))

;;;###autoload
(defun my-lang-flutter-run-or-hot-reload-web ()
  (interactive)
  (if (not (flutter--running-p))
      (my-lang-flutter-run-web)
    (progn
      (flutter-hot-reload)
      (save-selected-window
        (my-lang-flutter-show-cli)))))

;;;###autoload
(defun my-lang-flutter-open-web ()
  (interactive)
  (-when-let* ((url (my-last-url-in-buffer flutter-buffer-name)))
    (browse-url url)))

;;;###autoload
(defun my-lang-flutter-hot-reload-when-running ()
  (when (flutter--running-p)
    (flutter-hot-reload))
  (with-eval-after-load 'hover
    (hover--hot-reload)))

;;;###autoload
(define-minor-mode my-lang-flutter-hot-reload-on-save-mode nil
  :global nil
  (if my-lang-flutter-hot-reload-on-save-mode
      (add-hook 'after-save-hook #'my-lang-flutter-hot-reload-when-running nil 'local)
    (remove-hook 'after-save-hook #'my-lang-flutter-hot-reload-when-running 'local)))

(provide 'my-functions-lang-dart)

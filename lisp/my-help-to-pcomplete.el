;; -*- lexical-binding: t; -*-

(require 'seq)
(require 'pcomplete)

;;;###autoload
(defun my-help-to-pcomplete-capf-data (parse-args)
  (when-let* ((parsed-list (apply #'pcomplete-from-help parse-args))
              (annot-fn
               (lambda (cand)
                 (let* ((parsed
                         (seq-find
                          (lambda (arg) (equal cand arg)) parsed-list)))
                   (concat
                    ;; TODO marginalia-like alignment
                    "\t\t"
                    (get-text-property 0 'pcomplete-annotation parsed)
                    "\t"
                    (get-text-property 0 'pcomplete-help parsed))))))
    `(,parsed-list :annotation-function ,annot-fn)))

;;;###autoload
(defun my-help-to-pcomplete-make-capf (parse-args)
  (let* ((sym
          (intern
           (format "%s--%s"
                   'my-help-to-pcomplete
                   (concat "__" (string-join (ensure-list parse-args) "__"))))))
    (unless (fboundp sym)
      ;; nil
      (defalias sym
        (lambda ()
          (when-let* ((bounds (bounds-of-thing-at-point 'symbol))
                      (data (my-help-to-pcomplete-capf-data parse-args)))
            `(,(car bounds) ,(cdr bounds) ,@data)))))
    sym))

;;;###autoload
(defun my-help-to-pcomplete-setup-completion
    (parse-args &optional complete-styles)
  (let* ((capf (my-help-to-pcomplete-make-capf parse-args)))
    (add-hook 'completion-at-point-functions capf nil 'local)
    (setq-local my-complete-at-point-alt-styles
                (or complete-styles `(partial-completion ,@completion-styles)))
    (when (fboundp #'my-tab-completion-mode)
      (my-tab-completion-mode))
    capf))

;;;###autoload
(defmacro my-help-to-pcomplete-with-setup-completion (command &rest body)
  (declare (indent defun))
  `(minibuffer-with-setup-hook (lambda () (my-help-to-pcomplete-setup-completion ,command))
     ,@body))

;;;###autoload
(defun my-help-to-pcomplete-grep-a (func &rest args)
  (my-help-to-pcomplete-with-setup-completion '("grep --help")
    (apply func args)))

;;;###autoload
(defun my-help-to-pcomplete-git-grep-a (func &rest args)
  (my-help-to-pcomplete-with-setup-completion '("git grep --help")
    (apply func args)))

;;;###autoload
(defun my-help-to-pcomplete-ripgrep-a (func &rest args)
  (my-help-to-pcomplete-with-setup-completion '("rg --help")
    (apply func args)))

(provide 'my-help-to-pcomplete)

;; -*- lexical-binding: t; -*-

(require 'consult)

;;; Defs

(defmacro my-consult-customize (&rest args)
  (if (my-doom-emacs-use-doom-configured-module-p :completion 'vertico)
      `(progn
         (advice-remove #'consult-customize #'ignore)
         (unwind-protect
             (consult-customize ,@args)
           (advice-add #'consult-customize :override #'ignore)))
    `(consult-customize ,@args)))

;;; Config

;; ;; Prevent overriding `consult-preview-key', no need when advice below
;; (set 'consult--customize-alist nil)

;;;###autoload
(progn
  (defun my-setup-consult ()

    (when (my-doom-emacs-use-doom-configured-module-p :completion 'vertico)
      (advice-add #'consult-customize :override #'ignore))

    (my-bind :map 'lsp-mode-map [remap xref-find-apropos] #'consult-lsp-symbols)
    (when (fboundp 'consult-buffer)

      (setq xref-show-xrefs-function #'consult-xref)
      (setq xref-show-definitions-function #'consult-xref)

      (my-bind
        :map
        '(minibuffer-mode-map
          comint-mode-map eshell-mode-map
          ;;
          cider-repl-mode-map)
        "C-r" #'consult-history)

      ;; Allow viewing the kill ring while `consult-yank-pop' doesn't when read-only
      (my-bind
        :map '(special-mode-map view-mode-map) [remap yank-pop] #'yank-pop)


      (my-bind [remap recentf-open] #'consult-recent-file)
      (my-bind [remap repeat-complex-command] #'consult-complex-command)
      (my-bind
        [remap switch-to-buffer-other-frame] #'consult-buffer-other-frame)
      (my-bind
        [remap switch-to-buffer-other-window] #'consult-buffer-other-window)

      (my-bind
        [remap my-switch-to-buffer-with-same-major-mode]
        #'my-consult-switch-to-buffer-same-major-mode)

      (my-bind "C-c k" #'consult-kmacro)
      ;; "C-c b" #'consult-bookmark

      ;; "C-M-#" #'consult-register
      ;; "M-#" #'consult-register-load
      ;; "M-'" #'consult-register-store

      (my-bind [remap yank-pop] #'consult-yank-pop)

      (my-bind "M-g e" #'consult-compile-error)
      (my-bind "M-g f" #'consult-flymake)
      (my-bind "M-g g" #'consult-goto-line) ; leave `goto-line' at "M-g M-g"
      (my-bind "M-g i" #'consult-imenu)
      (my-bind "M-g k" #'consult-global-mark)
      (my-bind "M-g m" #'consult-mark)

      (my-bind "M-s L" #'consult-line-multi)
      (my-bind "M-s l" #'consult-line)
      (my-bind "M-s u" #'consult-focus-lines)

      ;;


      (my-add-advice/s '(consult-grep) :around #'my-help-to-pcomplete-grep-a)
      (my-add-advice/s
        '(consult-git-grep)
        :around #'my-help-to-pcomplete-git-grep-a)
      (my-add-advice/s
        '(consult-ripgrep +vertico-file-search)
        :around #'my-help-to-pcomplete-ripgrep-a)

      ;;
      )))

(setq consult-project-function #'my-project-root)

;; NOTE that `consult-customize' will fail when the command or source isn't
;; recognized (defined)

;; https://github.com/minad/affe/issues/37#issuecomment-1416834270: Preview not
;; working with affe-find. But simply calling (consult--file-preview) will make
;; the default ENTER dysfunctional
(my-consult-customize
 consult-find
 my-findutil-async-from-home
 my-affe-find
 :state (my-consult-affe-make-find-file-state t))

(with-eval-after-load 'affe
  (my-consult-customize affe-find :state (my-consult-affe-make-find-file-state))
  (setq affe-count (expt 2 12))
  (when (require 'orderless nil t)
    (setq affe-regexp-compiler #'my-affe-orderless-regexp-compiler)))

;; ;; take precedence over its sources
;; (my-consult-customize consult-buffer :preview-key 'any)

(my-consult-customize

 consult-line consult-line-multi
 consult-mark consult-global-mark

 my-consult-switch-to-buffer-same-major-mode

 :preview-key (list "<down>" "<up>")

 ;;
 )

(my-consult-customize
 consult-man
 ;; it also searchs in descriptions, which are too much, at least this limits a
 ;; bit more but it still matches the beginning of descriptions??
 :initial (consult--async-split-initial "^")
 :sort nil)

;; (defun my-consult-customize-a (fn &rest args)
;;   (unless
;;       ;; ignore Doom's preview key
;;       (member "C-SPC" (flatten-tree args))
;;     (apply fn args)))
;; (advice-add #'consult-customize :around #'my-consult-customize-a)

(my-consult-customize
 consult-theme consult-goto-line
 :preview-key 'any
 ;; '(any :debounce 0.5)
 )


(my-add-advice-once
  'consult-buffer
  :before (lambda (&rest _)
            (when (and (require 'xdg nil 'noerror)
                       (require 'consult-xdg-recent-files nil 'noerror))
              (add-to-list
               'consult-buffer-sources 'consult-xdg-recent-files--source-system-file
               'append))
            ;; `consult-buffer' fine-grained previewing
            (-let* (((buffer-sources other-sources)
                     (-separate
                      (lambda (arg)
                        (and (symbolp arg) (string-search "buffer" (symbol-name arg))))
                      consult-buffer-sources)))
              (eval `(progn
                       (my-consult-customize
                        ,@other-sources
                        :preview-key consult-preview-key)
                       (my-consult-customize
                        ,@buffer-sources
                        :preview-key (list "<down>" "<up>")))
                    t))
            (recentf-mode)))

;; just open instead of picking anymore
(with-eval-after-load 'consult-dir
  (setq consult-dir-default-command #'consult-dir-dired))

;; being a hook variable that has a non-emtpy value by default, the hook may not
;; contain that when having been added another one before loading its
;; definition, TODO: should it be reported to Doom?
(add-hook 'consult-after-jump-hook #'recenter)

(my-bind :map 'consult-async-map "C-S-e" #'my-mark-to-end-of-line)


(plist-put
 consult--source-buffer
 :items
 (lambda ()
   (consult--buffer-query
    ;; :sort 'visibility                   ;
    ;; :sort nil                           ;
    :sort 'my-pref ;; `consult--buffer-sort-my-pref'
    :as #'buffer-name                   ;
    )))

(provide 'my-after-consult)

;; Local Variables:
;; no-byte-compile: t
;; End:

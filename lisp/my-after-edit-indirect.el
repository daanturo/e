;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; also loaded by lsp-mode -> markdown-mode -> edit-indirect

;; (my-backtrace)

(set-popup-rule! "\\`\\*edit-indirect.*\\*" :ignore t)

(provide 'my-after-edit-indirect)
;; Local Variables:
;; no-byte-compile: t
;; End:

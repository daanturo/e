;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-org-cite-basic--complete-key--vertico-a
    (func &rest args)
  (my-with-advice
    #'completing-read
    :around #'my-completing-read-no-require-match-a (apply func args)))

;;; my-functions-org-cite.el ends here

(provide 'my-functions-org-cite)

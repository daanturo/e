;; -*- lexical-binding: t; -*-

(require '00-my-core-macros)

(defun my-string-shared-pre*suf-fixes (s0 &rest other-strings)
  "Return \"shared-prefix*shared-suffix\" of S0 with OTHER-STRINGS.
When the shared fixes are the same, return one of them only."
  (declare (pure t) (side-effect-free t))
  (let ((start (-reduce-from #'s-shared-start s0 other-strings))
        (end (-reduce-from #'s-shared-end s0 other-strings)))
    (if (string= start end)
        start
      (concat start "*" end))))

;;;###autoload
(defun my-hydra-define (suf-name commands &optional hydra-args docstring prefixes-to-remove hydra-width chars bindings-alist)
  (let* ((commands (sort (cl-loop for command in commands
                                  unless (or (string-match-p
                                              (rx (or "--"
                                                      (regexp "hydra.*/")
                                                      (regexp "global.*mode")))
                                              (symbol-name command))
                                             (member
                                              (alist-get
                                               command
                                               bindings-alist)
                                              (split-string
                                               "qhjkl"
                                               ""
                                               'omit-null)))
                                  collect command)
                         (lambda (x y) (string< (symbol-name x) (symbol-name y)))))
         (hydra-args (or hydra-args '(:foreign-keys run)))
         (docstring (or docstring (format "%s" suf-name)))
         (prefixes-to-remove (or prefixes-to-remove
                                 (list (format "%s" suf-name))))
         (hydra-width (or hydra-width (frame-width)))
         (chars (or chars (split-string (concat "zxcvbnm" "asdfg" "wertyuiop") "" 'omit-null)))
         (command-number (length commands))
         (command-hint-alist
          (cl-loop for command in commands
                   collect (cons command
                                 (s-chop-prefixes (append prefixes-to-remove '("-"))
                                                  (symbol-name command)))))
         (number-of-columns (min (length chars)
                                 (/ hydra-width
                                    (cl-loop for name in (seq-map #'cdr command-hint-alist)
                                             maximize (length name)))))
         (max-binding-length (cond ((<= command-number (length chars)) 1)
                                   ((<= command-number (expt (length chars) 2)) 2)
                                   (t (user-error "To many commands!"))))
         (number-of-commands/group (ceiling (/ command-number
                                               (float number-of-columns))))
         (command-groups (seq-partition commands number-of-commands/group))
         (shared-prefixes-in-groups (cl-loop for group in command-groups
                                             collect (apply #'my-string-shared-pre*suf-fixes
                                                            (seq-map #'symbol-name group))))
         (hydra-name (intern (format "my-hydra-%s" suf-name)))
         (heads (cl-loop for group in command-groups
                         nconc (cl-loop for command in group
                                        collect (let ((column (cl-position group
                                                                           command-groups
                                                                           :test #'equal)))
                                                  (list
                                                   ;; binding
                                                   (if bindings-alist
                                                       (alist-get command bindings-alist)
                                                     (concat
                                                      (cond ((eq 1 max-binding-length)
                                                             (nth (cl-position command commands :test #'equal)
                                                                  chars))
                                                            ((eq 2 max-binding-length)
                                                             (concat (nth column chars)
                                                                     (nth (cl-position command group :test #'equal)
                                                                          chars))))))
                                                   ;; command
                                                   command
                                                   ;; hint
                                                   (alist-get command command-hint-alist)
                                                   ;; column
                                                   :column (format
                                                            "%s %s"
                                                            column
                                                            (nth column shared-prefixes-in-groups))))))))
    (my-lexical-eval `(defhydra ,hydra-name ,hydra-args ,docstring
                        ,@(append heads
                                  `(("q" nil "quit")))))
    (intern (format "%s/body" hydra-name suf-name))))

;;;###autoload
(defun my-hydra-from-prefix-key (name key-str &optional match-name &rest keymaps)
  (let* ((looked-up-maps (cl-loop for keymap in keymaps
                                  append (cdr (lookup-key keymap (kbd key-str)))))
         (commands (cl-loop for last in (seq-map #'cdr-safe looked-up-maps)
                            when (and (symbolp last)
                                      (commandp last)
                                      (if match-name
                                          (string-match-p (regexp-quote name) (symbol-name last))
                                        t))
                            collect last))
         (pairs (cl-loop for command in commands
                         collect (cons command
                                       (key-description
                                        (vector
                                         (car (rassoc command looked-up-maps)))))))
         (hydra-body
          (my-hydra-define name commands nil nil (list name) nil nil pairs)))
    (if (called-interactively-p 'any)
        (call-interactively hydra-body)
      hydra-body)))

;;;###autoload
(defun my-grammar-langtool-check-hydra ()
  (interactive)
  (call-interactively (my-hydra-from-prefix-string "langtool")))

(provide 'my-hydras)

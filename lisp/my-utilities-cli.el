;; -*- lexical-binding: t; -*-

;;;###autoload
(defun my-ediff-from-cli (&rest _)
  (ediff (nth 0 command-line-args-left)
         (nth 1 command-line-args-left)))

(provide 'my-utilities-cli)

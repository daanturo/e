;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;; Initialization helpers

;;;###autoload
(defun my-add-advice-once (symbol/s where func &optional props)
  "Run FUNC once when one of SYMBOL/S is called.
Like `advice-add': execute `FUNC' at `WHERE' when `SYMBOL/S' is
called, but remove the advice(s) `FUNC' immediately."
  (declare (indent defun))
  (-let* ((advice-remover (gensym "my-add-advice-once")))
    (defalias advice-remover
      (lambda (&rest _)
        (my-remove-advice/s symbol/s (list func))
        (my-remove-advice/s symbol/s (list advice-remover))))
    (my-add-advice/s symbol/s where (list func) props)
    (my-add-advice/s symbol/s :before (list advice-remover))))

;;;###autoload
(defun my-add-hook-once (hook/s func &optional depth)
  "(`add-hook' HOOK/S FUNC DEPTH) once.
But remove FUNC from HOOK/S after it is called once. See also
`my-add-local-hook-once'."
  (declare (indent defun))
  (-let* ((hooks (ensure-list hook/s)))
    (letrec ((hook-removal
              (lambda (&rest _)
                (my-remove-hook/s hooks (list func))
                (my-remove-hook/s hooks (list hook-removal)))))
      (my-add-hook/s hooks (list hook-removal) nil)
      (my-add-hook/s hooks (list func) depth))))

;;;###autoload
(defun my-add-local-hook-once (hook/s func &optional depth)
  (declare (indent defun))
  (-let* ((hooks (ensure-list hook/s))
          (buf (current-buffer)))
    (letrec ((hook-removal
              (lambda (&rest _)
                (with-current-buffer buf
                  (my-remove-hook/s hooks (list func) t)
                  (my-remove-hook/s hooks (list hook-removal) t)))))
      (my-add-hook/s hooks (list hook-removal) nil t)
      (my-add-hook/s hooks (list func) depth t))))

;;;###autoload
(defun my-add-mode-hook-and-now (mode/s func)
  "`add-hook' FUNC to MODE/S's hooks, activate FUNC now for buffers
who derive from MODE/S or have one of MODE/S enabled."
  (declare (indent defun))
  (when (functionp func)
    (let ((modes (ensure-list mode/s)))
      (dolist (buf (buffer-list))
        (with-current-buffer buf
          (when (or
                 ;; minor modes
                 (-some #'my-bounded-value modes)
                 ;; major modes
                 (apply #'derived-mode-p modes))
            (funcall func))))
      (my-add-hook/s (-map #'my-mode->hook modes)
        (list func)))))

;;;###autoload
(defun my-add-mode-hooks-and-now (mode/s functions)
  "`my-add-mode-hooks-and-now' for multiple FUNCTIONS."
  (declare (indent defun))
  (dolist (func functions)
    (my-add-mode-hook-and-now mode/s func)))

;;;###autoload
(defun my-add-hook-once-at-count
    (max-count hook func &optional depth local add-hook-other-args)
  "(`add-hook' HOOK FUNC ...), but run FUNC only after HOOK has run MAX-COUNT times.
For example MAX-COUNT = 0 is just the normal `add-hook'.
DEPTH LOCAL ADD-HOOK-OTHER-ARGS."
  (letrec ((cur-count 0)
           (helper-hook
            (lambda (&rest args)
              (cond
               ((<= max-count cur-count)
                (apply #'remove-hook hook helper-hook local add-hook-other-args)
                (apply func args))
               (:else
                (cl-incf cur-count))))))
    (apply #'add-hook hook helper-hook depth local add-hook-other-args)
    helper-hook))

;;;###autoload
(define-minor-mode my-dummy-mode nil)

;;;###autoload
(defun my-1st-font (&rest family-list)
  (declare (side-effect-free t))
  (seq-find (lambda (family) (find-font (font-spec :family family)))
            family-list))

;;;###autoload
(defun my-set-local-prefer-global (symbol value)
  "Ensure that SYMBOL is set to VALUE.
Avoid making it local when possible. If VALUE equals to SYMBOL's
global value, `kill-local-variable', else set it locally.
SYMBOL's global value must not change frequently."
  (declare (indent defun))
  (if (equal value (default-value symbol))
      (kill-local-variable symbol)
    (progn (make-local-variable symbol)
           (set symbol value))))

;;;###autoload
(defun my-mode-derived-p (child-mode parent-modes)
  "Compatibility wrap (`provided-mode-derived-p' CHILD-MODE PARENT-MODES).
For Emacs 30+, just use the said function. But otherwise make the
function arity compatible, and let `treesit' \"-ts\" major modes derive
from its \"non-ts\" modes, as Emacs 30 declares that by default."
  (cond
   ((<= emacs-major-version 30)
    (provided-mode-derived-p child-mode parent-modes))
   (:else
    (or (apply #'provided-mode-derived-p child-mode parent-modes)
        (apply #'provided-mode-derived-p
               (my-treesit-ts-mode->mode child-mode)
               parent-modes)))))

;;;###autoload
(defun my-derived-mode-p (parent-modes &optional child-mode)
  (let* ((child-mode (or child-mode major-mode)))
    (my-mode-derived-p child-mode parent-modes)))

;;;###autoload
(defun my-get-alist-major-mode (alist)
  (or (alist-get major-mode alist nil nil #'my-parent-mode-p)
      (alist-get (my-treesit-ts-mode->mode major-mode) alist
                 nil
                 nil
                 #'my-parent-mode-p)))

;;;###autoload
(defun my-get-alist-all-by-mode (alist)
  "Collect all associated values in ALIST, treating its keys as modes.
Each key mode are either a parent major mode (which current
`major-mode' derives from) or a minor mode (currently enabled).
Each value must be a sequence."
  (cl-loop for (k . v) in alist
           when (or (derived-mode-p k) (and (boundp k) (symbol-value k)))
           append v))

;;;###autoload
(defun my-get-alist-by-value-or-mode (alist)
  (-some
   (-lambda
     ((sym/s . tail))
     (and (-some
           (lambda (sym)
             (or (and (boundp sym) (symbol-value sym)) (derived-mode-p sym)))
           (ensure-list sym/s))
          tail))
   alist))

(defvar my-locale-list--cache nil)
;;;###autoload
(defun my-locale-list (&optional fresh)
  (when fresh
    (setq my-locale-list--cache nil))
  (with-memoization my-locale-list--cache
    (save-match-data
      (-->
       (ignore-error (file-missing file-error)
         (process-lines-ignore-status "locale" "-a"))
       (-map (lambda (line) (replace-regexp-in-string "\\..*" "" line)) it)
       (-filter (lambda (locale) (string-match "_" locale)) it)
       (-uniq it)))))

(defvar my-locale-language-code-list--cache nil)
;;;###autoload
(defun my-locale-language-code-list (&optional fresh)
  (when fresh
    (setq my-locale-language-code-list--cache nil))
  (with-memoization (alist-get
                     (my-locale-list) my-locale-language-code-list--cache)
    (-->
     (-map
      (lambda (lang-code)
        (--> (replace-regexp-in-string "_.*" "" lang-code)))
      (my-locale-list))
     (-uniq it))))

;; (defvar my-doom-emacs-use-doom-configured-module-p--cache '())
;;;###autoload
(defun my-doom-emacs-use-doom-configured-module-p (category module)
  (
   ;; with-memoization (cdr
   ;;                   (assoc
   ;;                    (vector category module)
   ;;                    my-doom-emacs-use-doom-configured-module-p--cache))
   progn
   (and (member my-emacs-kit '(doom))
        (doom-module-get category module)
        (not
         (file-exists-p
          (file-name-concat doom-user-dir
                            "modules"
                            (string-remove-prefix ":" (format "%s" category))
                            (format "%s" module)))))))


;;; Initialize / Setup

(defun my-corfu-mode--maybe ()
  (when
      (and
       (not (string-prefix-p " " (buffer-name)))
       (not
        (memq
         major-mode
         '( ;
           ;; disable in `comint-mode' (`compile''s COMINT arg), but enable in other derived REPLs
           comint-mode)))
       ;;

       (not buffer-read-only))
    (corfu-mode 1)))

;;;###autoload
(define-globalized-minor-mode my-corfu-global-mode
  corfu-mode
  my-corfu-mode--maybe)
;; (if my-corfu-global-mode
;;     (progn)
;;   (progn))


;;;###autoload
(defun my-corfu-global-mode-safe ()
  (when (fboundp #'corfu-mode)
    (my-corfu-global-mode 1)))

(defun my-corfu-enable-in-minibuffer-h ()
  (when (and
         ;; corfu official suggestion
         (local-variable-p 'completion-at-point-functions)
         ;; doom, also prevent in interactive grep commands
         (where-is-internal #'completion-at-point (list (current-local-map))))
    ;; (setq-local corfu-echo-delay nil)
    ;; (setq-local corfu-popupinfo-delay nil)
    (corfu-mode 1)))

;;;###autoload
(define-minor-mode my-corfu-enable-in-minibuffer-mode
  nil
  :global
  t
  (if my-corfu-enable-in-minibuffer-mode
      (progn
        (add-hook 'minibuffer-setup-hook #'my-corfu-enable-in-minibuffer-h))
    (progn
      (remove-hook 'minibuffer-setup-hook #'my-corfu-enable-in-minibuffer-h))))

;;;###autoload
(defvar my-popup-complete-command #'my-corfu-complete-with-selected-or-first)

;;;###autoload
(defun my-setup-corfu (&rest _)
  (interactive)
  (when (fboundp 'corfu-mode)

    ;; global-corfu-modes `(,@my-edit-modes (not comint-mode)) ; use `my-corfu-mode--maybe'
    (setq corfu-auto t)
    (setq corfu-auto-delay (/ 1.0 16))
    (setq corfu-auto-prefix 1) ; 0 may cause annoyances: {|}, delay, enter (select completion instead of newline)
    (setq corfu-cycle t)
    (setq corfu-indexed-start 1) ;; treat the input as 0, and "M-1" to commit the first candidate
    (setq corfu-on-exact-match 'quit) ; the sole candidate maybe a snippet, prevent its expansion when we just want literal
    (setq corfu-popupinfo-delay (/ 1.0 4)) ; (fetching docs is synchronous, thus being instant causes lags, try to pick a sensible value by experimenting)
    (setq corfu-preview-current nil) ; don't auto-insert while scrolling
    (setq corfu-sort-function #'my-sort-code-completion-candidates)

    ;; Don't use `company' for `lsp'
    (setq lsp-completion-provider :none)


    (my-add-advice/s
      #'corfu--move-prefix-candidates-to-front
      :override #'my-second-argument)

    ;; "https://github.com/minad/corfu/wiki#configuring-corfu-for-lsp-mode."
    ;; (add-hook 'lsp-completion-mode-hook #'my-lsp-mode-setup-completion)

    (my-after-each
      '(minibuffer orderless)
      (setf (alist-get
             'styles
             (alist-get 'lsp-capf completion-category-overrides))
            '(flex basic)))

    ;; Known completion scenarios:
    ;; - Auto-complete, `indent-for-tab-command' (TAB), C-SPC: prefer simple popups
    ;; - ESC TAB (`completion-at-point'), `complete-symbol': prefer the minibuffer for superior multi-component filtering

    (my-add-first-editable-hook (my-corfu-global-mode))
    (my-corfu-enable-in-minibuffer-mode)

    ;; leave "ESC TAB" at the default
    (my-bind
      :map 'corfu-map

      "M-m"
      #'my-corfu-move-to-minibuffer
      "S-SPC"
      #'corfu-insert-separator
      [remap completion-at-point]
      nil)

    ;;


    ;; (add-hook
    ;;  'my-override-keymap-alist
    ;;  `(completion-in-region-mode . ,(define-keymap "<escape>" #'corfu-quit)))

    (my-override-keymap-add-dynamic
     "<escape>"
     (lambda () completion-in-region-mode) #'corfu-quit)

    ;; TAB: always complete, RET: complete when selected
    (setq corfu-preselect 'prompt)
    (my-bind
      :map 'corfu-map '("RET" "<return>")
      `(menu-item
        ""
        corfu-insert
        :filter my-corfu-menu-item-filter-if-selected))

    (with-eval-after-load 'corfu
      (add-to-list
       'corfu-continue-commands #'my-corfu-move-to-minibuffer))


    ;; defer until the completion UI appears
    (my-add-advice-once '(corfu--in-region
                          corfu--auto-complete-deferred)
      :before
      (lambda (&rest _)
        "`corfu-indexed-mode' `corfu-doc-mode' `kind-icon'"
        (my-safe-call #'corfu-popupinfo-mode)
        (my-safe-call #'corfu-indexed-mode)
        (dolist (num (number-sequence corfu-indexed-start 9))
          (my-bind
            :map
            'corfu-map
            (format "M-%s" num)
            (my-corfu-select-candidate-by-index-command num)))
        ;; Fix error when the one of the above commands is executed while
        ;; `corfu''s currently selected candidate isn't at the top
        (my-add-list!
          'corfu-continue-commands
          `(,(format "^%s-" 'my-corfu-select)))
        (when (fboundp #'kind-icon-margin-formatter)
          (my-icon-in-popup-completions-global-mode)))))

  ;; sometimes when the backend fails, error is raised with almost every
  ;; keystroke
  (advice-add
   #'corfu--filter-completions
   :around #'my-with-ignore-second-errors-non-args-local-a))



(defun my-corfu-select-candidate-by-index-command (idx)
  (my-defalias*
    (intern (format "%s-%s-th-candidate" 'my-corfu-select idx))
    (lambda ()
      (interactive)
      (dlet ((corfu--index (+ corfu--scroll idx (- corfu-indexed-start))))
        (ignore-errors
          (my-with-ensured-undo-boundary (corfu-insert)))))))

(define-minor-mode my-icon-in-popup-completions-mode nil :global nil
  (if my-icon-in-popup-completions-mode
      (progn (my-set-local-prefer-global 'corfu-margin-formatters
               (-union '(kind-icon-margin-formatter) corfu-margin-formatters)))
    (progn (my-set-local-prefer-global 'corfu-margin-formatters
             (remove #'kind-icon-margin-formatter corfu-margin-formatters)))))

(define-minor-mode my-icon-in-popup-completions-global-mode nil :global t
  (if my-icon-in-popup-completions-global-mode
      (progn (add-hook 'corfu-margin-formatters #'kind-icon-margin-formatter))
    (progn (remove-hook 'corfu-margin-formatters #'kind-icon-margin-formatter))))

;;;###autoload
(defun my-company-settings (&rest _)
  (interactive)
  (with-eval-after-load 'company
    (setq company-minimum-prefix-length 1)
    (setq company-show-numbers t)
    ;; Allow more freeform completion
    (setq +vertico-company-completion-styles completion-styles)))

;;;###autoload
(defun my-setup-company-from-corfu (&rest _)
  (interactive)
  (with-eval-after-load 'lsp
    (custom-reevaluate-setting 'lsp-completion-provider))
  (my-add-first-editable-hook (my-corfu-global-mode-safe) (global-company-mode))
  (add-to-list 'company-transformers 'my-sort-code-completion-candidates)
  (with-eval-after-load 'company
    (-let* ((disabler (my-make-mode-disabler-command #'global-company-mode)))
      (add-hook 'global-corfu-mode-hook disabler)
      (add-hook 'my-corfu-global-mode-hook disabler))))

;;;###autoload
(defun my-setup-windmove ()
  "Initialize `windmove' bindings in `global-map' to keep org bindings."
  (my-bind "S-<left>" #'windmove-left)
  (my-bind "S-<up>" #'windmove-up)
  (my-bind "S-<down>" #'windmove-down)
  (my-bind "S-<right>" #'windmove-right)
  ;; Don't focus `treemacs' when moving up from a bottom popup
  (define-advice windmove-up (:after (&rest _) no-treemacs)
    (when (and (called-interactively-p 'any)
               (equal major-mode 'treemacs-mode))
      (windmove-right))))

;;;###autoload
(defun my-setup-blamer ()
  "Problematic.
Blamer problems:
- Overlays sometimes don't disappear
- Annoying echo messages when debugging is enabled:
Transmitting sexp {{{'(lambda nil (require 'vc-git)
   (apply #'vc-git--run-command-string
          \"filename\"
          '(\"blame\" \"-L\" \"row,col\")))
}}}
."

  (setq blamer-type 'visual)
  (setq blamer-view 'overlay-right)

  (when (fboundp 'blamer-mode)
    (my-add-lazy-hook-when
      'find-file-hook (my-vc-git-cached-tracked-p buffer-file-name)
      ;; Defer after the second command
      (my-add-hook-once
        'post-command-hook
        (my-fn% (my-add-hook-once 'post-command-hook #'global-blamer-mode)))))
  (setq blamer-bindings
        (list
         (cons
          "<mouse-1>"
          (lambda (commit-info)
            (interactive)
            (-some-->
                (plist-get commit-info :commit-hash) magit-show-commit)))))

  ;; (my-when-mode0-turn-off||disable-mode1 'visual-line-mode 'blamer-mode 'blamer)

  (with-eval-after-load 'blamer
    ;; Don't self-inflate (enlarge) along with org/outline headings
    (set-face-attribute 'blamer-face nil
                        :height (face-attribute 'default :height)
                        :weight 'normal)))

;; ;; TODO issue upstream
;; (advice-add
;;  #'blamer--try-render
;;  :around #'my-inhibit-when-repeating-command-a)



;;;###autoload
(define-minor-mode my-sideline-blame-global-mode
  nil
  :global
  t
  (cond
   (my-sideline-blame-global-mode
    (add-hook 'sideline-backends-right 'my-sideline-git-blame -96))
   (:else
    (remove-hook 'sideline-backends-right 'my-sideline-git-blame))))

;;;###autoload
(defun my-setup-sideline-blame ()
  (my-sideline-blame-global-mode))

;;;###autoload
(defun my-setup-why-this ()
  (when (fboundp 'why-this-mode)
    (setq why-this-annotate-enable-heat-map
          (member (frame-parameter nil 'background-mode) '(light)))
    (my-add-hook-once 'find-file-hook #'global-why-this-mode)
    (my-when-mode0-turn-off||disable-mode1 'visual-line-mode 'why-this-mode 'why-this)
    (with-eval-after-load 'why-this
      (set-face-attribute 'why-this-face nil :inherit 'default))))

(defvar my-flymake-mode-maybe-exclude-list '())

;;;###autoload
(defun my-flymake-mode-maybe ()
  (when (and (not buffer-read-only)
             (not (my-derived-mode-p my-flymake-mode-maybe-exclude-list)))
    (flymake-mode)))

;;;###autoload
(defun my-setup-flymake ()
  (my-add-startup-hook
    (my-add-hook/s my-edit-mode-hooks '(my-flymake-mode-maybe)))
  (setq flymake-fringe-indicator-position 'right-fringe) ; let the left fringe show break points
  (with-eval-after-load 'flymake
    (face-spec-set
     'flymake-note
     `((((class color) (background dark) (supports :underline (:style wave)))
        :underline
        (:style wave :color ,(color-lighten-name "yellow green" -50)))
       (((class color) (background light) (supports :underline (:style wave)))
        :underline
        (:style wave :color ,(color-lighten-name "yellow green" 50)))
       (t :inherit warning)))
    (face-spec-set
     'flymake-warning
     `((((class color) (background dark) (supports :underline (:style wave)))
        :underline (:style wave :color ,(color-lighten-name "yellow" -37.5)))
       (((class color) (background light) (supports :underline (:style wave)))
        :underline (:style wave :color ,(color-lighten-name "yellow" 12.5)))
       (t :inherit warning))))

  (my-windowpopup-set
   '(derived-mode
     flymake-diagnostics-buffer-mode flymake-project-diagnostics-mode)
   nil `((post-command-select-window . t) (window-height . 0.5)))

  (when (fboundp #'sideline-flymake)
    (add-hook 'sideline-backends-right 'sideline-flymake 0)))

(defvar my-lsp-init-exclude-mode-list '(text-mode org-mode markdown-mode)
  "Modes whose hooks will be exluded from `my-lsp-mode-hook-list'.")

;;;###autoload
(defun my-setup-lsp ()
  "."

  (add-hook 'my-idle-load-list 'lsp-mode)
  (add-hook 'my-idle-load-list 'eglot)

  ;; lsp-semantic-tokens-enable t ; Prone to errors, misfontifications, sometimes even makes the whole buffer comments (clang)

  (setq lsp-auto-execute-action nil) ; more control
  (setq lsp-auto-guess-root t) ; Manually specifying the root directory is tiresome
  (setq lsp-diagnostics-provider :flymake) ; use `flymake'
  (setq lsp-enable-dap-auto-configure nil) ; don't enable `dap-mode' so eagerly
  (setq lsp-enable-suggest-server-download nil) ; less disruptions for quick viewing/editing
  (setq lsp-headerline-breadcrumb-enable t) ; `doom-modeline' may show relative path from the project root, but LSP additionally shows current tag
  (setq lsp-headerline-breadcrumb-segments '(symbols)) ; the path may be too long
  (setq lsp-response-timeout 1) ; TODO: remove when https://github.com/emacs-lsp/lsp-mode/issues/3555#issuecomment-1939739676 is fixed
  (setq lsp-restart 'ignore) ; Don't ask when servers exit, just ignore
  (setq lsp-symbol-highlighting-skip-current t) ; Less distraction when focusing on the current symbol
  (setq lsp-treemacs-symbols-exclusions '(localvariable))
  (setq lsp-ui-imenu-buffer-position 'left) ; more readable
  (setq lsp-warn-no-matched-clients nil) ; Less chatty when a generic configuration is used

  (with-eval-after-load 'lsp-mode
    ;; ignore all dot directories
    (add-hook 'lsp-file-watch-ignored-directories "[/\\\\]\\."))

  ;; Share server installations among profiles, also better config nukes survival
  (setq lsp-server-install-dir
        (file-name-concat my-local-share-emacs-dir/ "lsp"))
  ;; after above
  (setq dap-utils-extension-path
        (file-name-concat lsp-server-install-dir "dap")) ; Same as `lsp-server-install-dir'


  (with-eval-after-load 'lsp-mode

    (cond

     ((fboundp 'json-rpc)

      ;; `lsp-completion--exit-fn'/`lsp-completion--resolve'/`lsp-request' hangs
      ;; Emacs, TODO: remove after https://github.com/emacs-lsp/emacs/issues/12,
      ;; https://github.com/emacs-lsp/lsp-mode/pull/4262 is/are solved
      ;; (advice-add #'lsp-completion--exit-fn :around #'my-with-interruptible-while-no-input-nil-a)
      (advice-add
       #'lsp-completion--resolve
       :around #'my-lsp-completion--resolve--workaround-a)

      ;; https://discord.com/channels/789885435026604033/789895930987675698/1034183769403441303
      ;; https://www.reddit.com/r/emacs/comments/ymrkyn/comment/ivkoaox/?utm_source=share&utm_medium=web2x&context=3
      (setq lsp-idle-delay (expt 2 -6)))

     ((executable-find "emacs-lsp-booster")
      ;; https://github.com/blahgeek/emacs-lsp-booster#configure-lsp-mode
      (defun lsp-booster--advice-json-parse (old-fn &rest args)
        "Try to parse bytecode instead of json."
        (or (when (equal (following-char) ?#)
              (let ((bytecode (read (current-buffer))))
                (when (byte-code-function-p bytecode)
                  (funcall bytecode))))
            (apply old-fn args)))
      (advice-add
       (if (progn
             (require 'json)
             (fboundp 'json-parse-buffer))
           'json-parse-buffer
         'json-read)
       :around #'lsp-booster--advice-json-parse)

      (defun lsp-booster--advice-final-command (old-fn cmd &optional test?)
        "Prepend emacs-lsp-booster command to lsp CMD."
        (let ((orig-result (funcall old-fn cmd test?)))
          (if (and
               (not test?) ;; for check lsp-server-present?
               (not (file-remote-p default-directory)) ;; see lsp-resolve-final-command, it would add extra shell wrapper
               lsp-use-plists
               (not (functionp 'json-rpc-connection)) ;; native json-rpc
               (executable-find "emacs-lsp-booster"))
              (progn
                (when-let
                    ((command-from-exec-path
                      (executable-find (car orig-result)))) ;; resolve command from exec-path (in case not found in $PATH)
                  (setcar orig-result command-from-exec-path))
                (message "Using emacs-lsp-booster for %s!" orig-result)
                (cons "emacs-lsp-booster" orig-result))
            orig-result)))
      (advice-add
       'lsp-resolve-final-command
       :around #'lsp-booster--advice-final-command)))

    ;; `lsp-auto-guess-root' is enabled, no need for persistent session, a
    ;; session may cause atrocious lag because of file watching
    (advice-add #'lsp--persist-session :override #'ignore)

    ;; (my-demote-errors-from 'lsp--send-no-wait)

    ;; https://github.com/emacs-lsp/lsp-mode/issues/3836
    (advice-add #'lsp-workspace-root :around #'my-memoize-local-a)
    ;; (advice-add #'lsp-server-present? :around #'my-memoize-non-nil-a)

    ;; just no, don't ask
    (advice-add #'lsp--ask-about-watching-big-repo :override #'ignore)

    ;; TODO: propose upstream?
    (advice-add #'lsp--read-rename :around #'my-lsp--read-rename--beg-a))

  ;; don't set it forcefully
  (add-hook 'eglot-stay-out-of 'eldoc-documentation-strategy)
  ;; only say once, because I may use eglot automatically
  (my-add-advice/s
    '(eglot--connect eglot--guess-contact)
    :around #'my-with-ignore-second-errors-a)

  (my-persistuse
    'my-lsp-mode-hook-list
    (lambda ()
      (and (require 'lsp-mode nil t)
           (-keep
            (-lambda ((k . _))
              (and (symbolp k)
                   (not (member k my-lsp-init-exclude-mode-list))
                   (my-mode->hook k)))
            lsp-language-id-configuration)))
    (lambda (hooks) (my-add-hook-once hooks #'my-setup-lsp-global-mode-now)))

  (my-add-startup-hook
    (my-add-hook-once 'find-file-hook #'my-lsp-maybe-global-mode)))

(defvar my-prog-after-try-lsp-hook '())
(defvar my-lsp-turn-on-function nil)
(put 'my-lsp-turn-on-function 'safe-local-variable #'functionp)

(defun my-lsp--after-trying-a (&rest _)
  (run-hooks 'my-prog-after-try-lsp-hook))

(define-minor-mode my-lsp-maybe-mode
  nil :global nil
  (if my-lsp-maybe-mode
      (progn
        (if my-lsp-turn-on-function
            (funcall my-lsp-turn-on-function)
          (my-lsp--after-trying-a)))
    (progn)))

(defvar my-lsp-exlude-mode-list
  '(
    ;; some buffers are activated when even the major mode isn't initialized yet
    fundamental-mode
    ;; usually no need for LSP when edited manually, also they are usually large
    json-mode json-ts-mode js-json-mode))

(defun my-lsp-maybe-turn-on-mode ()
  (when (and (apply #'derived-mode-p my-edit-modes)
             (not (apply #'derived-mode-p my-lsp-exlude-mode-list)))
    (my-lsp-maybe-mode)))

;;;###autoload
(define-globalized-minor-mode my-lsp-maybe-global-mode
  my-lsp-maybe-mode
  my-lsp-maybe-turn-on-mode
  (cond
   (my-lsp-maybe-global-mode)
   (:else
    (my-safe-call #'eglot-shutdown-all 'preserve-buffers))))

;;;###autoload
(defun my-setup-lsp-global-mode-now ()
  (cond
   ;; because `lsp-mode' takes a significant time to just load so opening a file
   ;; will introduce visible lag, only use it the daemon mode (where if Doom,
   ;; incrementally load it) and `eglot' in non-daemon mode, but when `dap-mode'
   ;; is loaded, switch to `lsp-mode'
   ((and (fboundp #'lsp-deferred) (daemonp))
    (setq my-lsp-client-package "lsp-mode")
    (setq-default my-lsp-turn-on-function #'lsp-deferred))
   ((fboundp #'eglot-ensure)
    (setq my-lsp-client-package "eglot")
    (setq-default my-lsp-turn-on-function #'my-eglot-ensure)))
  (my-add-advice/s '(lsp eglot-ensure eglot) :after #'my-lsp--after-trying-a)
  ;; re-run to initialize in opened buffers
  (my-lsp-maybe-global-mode))

(defvar-local my-eglot-look-mode--cache 'undefined)
;;;###autoload
(defun my-eglot-lookup-mode ()
  (if (equal my-eglot-look-mode--cache 'undefined)
      (progn
        (require 'eglot)
        (setq my-eglot-look-mode--cache (eglot--lookup-mode major-mode))
        my-eglot-look-mode--cache)
    my-eglot-look-mode--cache))

;;;###autoload
(defun my-eglot-ensure ()
  (-let* ((func
           (lambda ()
             (if (my-eglot-lookup-mode)
                 (eglot-ensure)
               (my-lsp--after-trying-a)))))
    (make-thread func)))


;;;###autoload
(defun my-lsp-complete ()
  (interactive)
  ;; (cape-interactive (cape-capf-silent (cape-capf-purify #'lsp-completion-at-point)))
  (dlet ((completion-at-point-functions '(lsp-completion-at-point)))
    (completion-at-point)))

;;;###autoload
(defun my-setup-outline ()
  
  (declare)

  ;; See also `outline-minor-mode-use-buttons'

  ;; TAB/S-TAB to fold
  (setq outline-minor-mode-cycle t)

  ;; ;; must have both `comment-start', `comment-end'
  ;; (add-hook 'prog-mode-hook 'hs-minor-mode)

  ;; TODO: replace with https://github.com/jdtsmith/outli
  
  ;; theoretically, `my-outline-comment-mode' is more invasive than just
  ;; `my-outline-minor-highlight-mode', therefore is more preferred to keep the
  ;; designed `outline-regexp' unchanged; but I haven't yet to found a adequate
  ;; heuristic that only enables `outline-minor-mode-highlight' for comments
  ;; only
  (add-hook 'prog-mode-hook #'my-enable-outline-comment-mode-maybe)

  (with-eval-after-load 'outline
    ;; Ensure readability after jumping
    (my-add-advice/s #'(outline-forward-same-level outline-backward-same-level)
      :after #'my-recenter-top-to-a-quarter|1/4-when-interactive_-a)
    (-let* ((filter (lambda (cmd) (and (outline-on-heading-p) cmd))))
      (my-bind :map '(outline-minor-mode-cycle-map org-mode-map)
        "C-M-h" `(menu-item "" outline-mark-subtree :filter ,filter))
      (my-bind :map 'outline-minor-mode-cycle-map :vi '(normal)
        "M-<left>" `(menu-item "" outline-promote :filter ,filter)
        "M-<down>" `(menu-item "" outline-move-subtree-down :filter ,filter)
        "M-<up>" `(menu-item "" outline-move-subtree-up :filter ,filter)
        "M-<right>" `(menu-item "" outline-demote :filter ,filter)))
    (my-hook-while 'outline-minor-mode-hook
      (eq 'unspecified (face-attribute 'outline-1 :height))
      (my-set-face-decreasing-heights-by-level 1 8 "outline-%d" (/ 3 2.0)))))

;;;###autoload
(defun my-rainbow-delimiters-mode-maybe ()
  (unless (derived-mode-p
           'autoconf-mode ;
           'makefile-mode) ;

    (rainbow-delimiters-mode)))


(defvar-local smerge-mode nil)
(defvar-local rainbow-delimiters-mode nil)

;;;###autoload
(defun my-rainbow-delimiters-turn-off-maybe ()
  (when (and (or smerge-mode) rainbow-delimiters-mode)
    (rainbow-delimiters-mode 0)))

;;;###autoload
(defun my-flyspell-mode-maybe ()
  (when (not buffer-read-only)
    (flyspell-mode)))

;;;###autoload
(defun my-flyspell-prog-mode-maybe ()
  (when (not buffer-read-only)
    (flyspell-prog-mode)))

;;;###autoload
(define-minor-mode my-flyspell-global-mode nil
  :global t
  (if my-flyspell-global-mode
      (progn
        (add-hook 'prog-mode-hook 'my-flyspell-prog-mode-maybe)
        (add-hook 'conf-mode-hook 'my-flyspell-prog-mode-maybe)
        (add-hook 'text-mode-hook 'my-flyspell-mode-maybe))
    (progn
      (remove-hook 'prog-mode-hook 'my-flyspell-prog-mode-maybe)
      (remove-hook 'conf-mode-hook 'my-flyspell-prog-mode-maybe)
      (remove-hook 'text-mode-hook 'my-flyspell-mode-maybe)
      (dolist (buf (my-buffers-with-minor-mode 'flyspell-mode))
        (with-current-buffer buf
          (flyspell-mode 0))))))

;;;###autoload
(defun my-config-ispell ()
  "Setup a combined dictionary of installed ones."
  (unless (and (executable-find ispell-program-name)
               ;; enchant (without a backend?) hangs Emacs
               (string-match-p "[a-z]spell$" ispell-program-name))
    (my-flyspell-global-mode 0))
  ;; hunspell can use a combined dictionary, unlike aspell
  (when (executable-find "hunspell")
    (setq ispell-program-name "hunspell")
    (unless ispell-hunspell-dictionary-alist
      (ispell-find-hunspell-dictionaries))
    (setq ispell-dictionary
          (string-join (-filter
                        (lambda (it)
                          (assoc it ispell-hunspell-dictionary-alist))
                        (my-locale-list))
                       ","))
    (ispell-set-spellchecker-params)
    (ispell-hunspell-add-multi-dic ispell-dictionary)))

(defvar my-breadcrumb-exclude-mode-list
  '(lsp-mode
    my-eval-output-mode))
;; lsp-headerline-breadcrumb-mode


(defvar-local breadcrumb-local-mode nil)

;;;###autoload
(defun my-breadcrumb-local-mode-maybe ()
  (cond
   ((and (provided-mode-derived-p major-mode 'prog-mode)
         (not (-some #'my-enabled-mode-p my-breadcrumb-exclude-mode-list)))
    (condition-case err
        (breadcrumb-local-mode)
      (error
       (let* ((err-msg (error-message-string err)))
         (message "`my-breadcrumb-local-mode-maybe': %s" err-msg)))))
   (breadcrumb-local-mode
    (breadcrumb-local-mode 0))))

;;;###autoload
(define-globalized-minor-mode my-jinx-delayed-global-mode
  jinx-mode
  (lambda ()
    (let* ((buf (current-buffer)))
      (run-with-idle-timer
       1.0 nil
       (lambda ()
         (when (buffer-live-p buf)
           (with-current-buffer buf
             (make-thread #'jinx--on)))))))
  ;; (make-thread #'jinx--on)
  (cond
   (my-jinx-delayed-global-mode
    (global-jinx-mode 0))
   (:else)))

;;;###autoload
(defun my-setup-jinx ()
  (my-bind "C-M-$" #'jinx-languages)
  (with-eval-after-load 'jinx

    (add-hook
     'vertico-multiform-categories '(jinx grid (vertico-grid-annotate . 1)))
    (setq jinx-languages (string-join (my-locale-list) " "))
    (add-hook 'jinx-exclude-faces '(conf-unix-mode font-lock-string-face))

    ;; make it less intrusive to commented identifies
    (face-spec-set
     'jinx-misspelled
     `((((class color) (background dark) (supports :underline (:style wave)))
        :underline (:style wave :color ,(color-lighten-name "orange" -50)))
       (((class color) (background light) (supports :underline (:style wave)))
        :underline (:style wave :color ,(color-lighten-name "orange" 50)))
       (t :underline t :inherit error)))))



;;;###autoload
(defun my-setup-colorful-mode ()

  (when (fboundp #'global-colorful-mode)
    ;; like `doom-first-buffer-hook'
    (my-add-hook-once
      'window-buffer-change-functions
      (lambda (&rest _) (global-colorful-mode))))

  ;; highlighting symbols is weird, but valid in CSS mode
  (setq colorful-only-strings 'only-prog)

  (with-eval-after-load 'colorful-mode

    ;; view face colors
    (add-to-list 'global-colorful-modes 'help-mode)

    (add-to-list 'global-colorful-modes 'conf-mode)
    (add-to-list
     'colorful-extra-color-keyword-functions
     '((conf-mode) . my-colorful-add-conf-decimal-rgb-colors))))





;;;###autoload
(defun my-setup-dap-debug (&optional start-key)

  (-let* ((make-mif-fn
           (lambda (dape-thing dap-thing)
             `(menu-item
               "" nil
               :filter
               ,(lambda (_)
                  (cond
                   ((bound-and-true-p dap-mode)
                    dap-thing)
                   (:else
                    dape-thing)))))))

    ;; format: off
    (when start-key
      (my-bind :map 'prog-mode-map start-key (funcall make-mif-fn #'my-dape-transient #'dap-hydra)))
    (my-bind :map 'prog-mode-map "<f9>" (funcall make-mif-fn
                                                 #'dape-breakpoint-toggle
                                                 ;; #'my-dape-breakpoint-toggle-and-save
                                                 #'dap-breakpoint-toggle))
    (my-bind :map 'prog-mode-map "<f10>" (funcall make-mif-fn #'dape-next #'dap-next))
    (my-bind :map 'prog-mode-map "<f11>" (funcall make-mif-fn #'dape-step-in #'dap-step-in))
    (my-bind :map 'prog-mode-map "S-<f11>" (funcall make-mif-fn #'dape-step-out #'dap-step-out))
    ;; format: on

    (cond
     ((fboundp #'dape)
      (setq my-dap-client-package "dape")
      (my-persistuse
        'my-dape-key-prefix
        (lambda ()
          (and (require 'dape nil 'noerror) dape-key-prefix))
        nil
        :set-pred 'identity)
      (my-persistuse
        'my-dape-global-map
        (lambda ()
          (and (require 'dape nil 'noerror) dape-global-map))
        nil
        :set-pred 'identity)

      (when (bound-and-true-p my-dape-key-prefix)
        (setq dape-key-prefix my-dape-key-prefix)
        (my-bind my-dape-key-prefix my-dape-global-map))

      (my-bind
        :map
        '(my-dape-global-map dape-global-map)
        "C-a"
        #'my-dape-transient)
      (autoload #'dape-breakpoint-toggle "dape" nil t))
     ((fboundp #'dap-debug)
      (setq my-dap-client-package "dap-mode")
      (autoload #'dap-breakpoint-toggle "dap" nil t)))))

(defvar my-idle-load-list '())
;;;###autoload
(defun my-idle-do-load-h ()
  (dolist (lib my-idle-load-list)
    (require lib nil 'noerror)))

;;;###autoload
(defun my-autoload-all-commands-from-library (lib &optional command-name-regexp)
  (-let* ((sym
           (intern
            (format "%s--from--%s" 'my-autoload-all-commands-from-library lib)))
          (command-name-regexp
           (or command-name-regexp (format "^%s\\($\\|[^[:alnum:]]\\)" lib))))
    (my-persistuse
      sym
      (lambda ()
        (save-match-data
          (require lib)
          (cl-loop
           for
           symbol
           being
           the
           symbols
           for
           name
           =
           (symbol-name symbol)
           when
           (and (commandp symbol) (string-match command-name-regexp name))
           collect
           symbol)))
      (lambda (commands)
        (dolist (cmd commands)
          (autoload cmd (format "%s" lib) nil 'interact))))))

;;; Configurers

;;;###autoload
(cl-defun my-bind (&rest args &key map from vi (if t) &allow-other-keys)
  "`define-key' ARGS. Keys can be in string form.

Keywords:

MAP : 'map | '(maps...)

FROM : 'symbol | \"string\"

VI : 'state | '(states...), 'global means also set without `evil'

IF : 'form , after knowing MAP is bound, evaluate this to decide
to bind or not (quote this when passing to prevent eager
evaluation)

."

  (declare (indent defun))
  (let* ((bindings (-partition 2 (my-non-key-value-in-mixed-plist args)))
         (map-symbols (ensure-list (or map 'global-map)))
         (vi-states (ensure-list vi)))
    ;; Checking if library is available first via `load-path' is too slow
    (when from
      (my-autoload-functions from (mapcar #'cadr bindings) nil 'interactive))
    (dolist (map-symbol map-symbols)
      (if (my-bounded-value map-symbol)
          (my-bind-keys-defs map-symbol vi-states bindings if)
        (progn
          (add-to-list 'my-bind--pending-plist
                       (my-symbols->plist map-symbol vi-states bindings if)
                       'append)
          (add-hook 'after-load-functions #'my-bind--proced-pending-list))))))

(defvar my-bind--pending-plist '()
  "`my-bind''s pending bindings.")

(defun my-bind--proced-pending-list (&rest _)
  (setq my-bind--pending-plist
        (-remove
         (lambda (arg)
           (cl-destructuring-bind (&key map-symbol vi-states bindings if) arg
             (when (my-bounded-value map-symbol)
               (my-bind-keys-defs map-symbol vi-states bindings if)
               t)))
         my-bind--pending-plist)))
;; (when (length= my-bind--pending-plist 0)
;;   (remove-hook 'after-load-functions #'my-bind--proced-pending-list))


;;;###autoload
(defun my-bind-keys-defs (map-symbol vi-states bindings if)
  (when (eval if 'lexical)
    (cl-loop
     for (key0 def) in bindings do
     (let ((keys (mapcar #'my-ensure-kbd (ensure-list key0))))
       (my-bind-keys-def map-symbol vi-states keys def)))))

;;;###autoload
(defun my-bind-unbind-non-prefix-key (keymap key)
  "Unbind a prefix of KEY that exists in KEYMAP.
Credit: `general-unbind-non-prefix-key'."
  (let ((keybd
         (if (stringp key)
             (string-to-vector key)
           key)))
    (while (numberp (lookup-key keymap keybd))
      (setq keybd (cl-subseq keybd 0 -1)))
    (define-key keymap keybd nil)))

(cl-defmacro my-bind--auto-unbind-non-prefix (keymap key &rest body)
  (declare (debug t) (indent defun))
  `(condition-case _
       (progn
         ,@body)
     ((error) (my-bind-unbind-non-prefix-key ,keymap ,key) ,@body)))

;;;###autoload
(defun my-bind-keys-def (map-symbol vi-states keys def)
  "In MAP-SYMBOL and VI-STATES, bind KEYS to DEF."
  (let* ((keymap (symbol-value map-symbol))
         (global (member 'global vi-states))
         (states (remove 'global vi-states)))
    ;; Loop through multiple keys
    (dolist (key keys)
      (my-bind--auto-unbind-non-prefix
        keymap key
        (cond
         (vi-states
          ;; `global' in `vi-states': also bind in vanilla
          (when global
            (define-key keymap key def))
          (with-eval-after-load 'evil
            (my-bind--auto-unbind-non-prefix
              keymap key
              (evil-define-key*
                states
                (if (equal 'global-map map-symbol)
                    'global
                  keymap)
                key def))))
         (:else
          (define-key keymap key def)))))))

(defvar my-override-keymap--dynamic-table '())
;;;###autoload
(defun my-override-keymap-add-dynamic (key/s pred cmd)
  (dolist (key (ensure-list key/s))
    (add-to-list 'my-override-keymap--dynamic-table (list key pred cmd))
    ;; unless (keymap-lookup my-override-keymap key)
    (keymap-set
     my-override-keymap key
     `(menu-item
       "" nil
       :filter
       ,(lambda (_)
          (-let* ((lst
                   (my-alist-assoc-all key my-override-keymap--dynamic-table)))
            (-some
             (-lambda ((_ pred1 cmd1)) (and (funcall pred1) cmd1)) lst)))))))

;;;###autoload
(defun my-when-writable-buffer (arg)
  (and (not buffer-read-only)
       arg))

;;;###autoload
(defun my-defer-hook (hook-sym)
  "Defer HOOK-SYM until after `emacs-startup-hook'."
  (let ((hooks (my-bounded-value hook-sym)))
    (my-remove-hook/s (list hook-sym) hooks)
    (add-hook 'emacs-startup-hook
              (my-defalias* (my-concat-symbols '@ 'my-defer-hook hook-sym)
                (lambda ()
                  (my-add-hook/s (list hook-sym)
                    hooks))))))

(defun my-defer-until-visible--buffer-visible-p ()
  (or (buffer-modified-p) (get-buffer-window nil t)))

;;;###autoload
(defun my-defer-until-visible (real-func)
  "Defers running REAL-FUNC until buffer is visible.
Credit: `lsp-deferred'."
  (-let* ((buffer (current-buffer)))
    (run-with-idle-timer
     0 nil
     (lambda ()
       (letrec ((init-if-visible-fn
                 (lambda ()
                   (:documentation
                    (format "`my-defer-until-visible' `%s'." real-func))
                   (when (my-defer-until-visible--buffer-visible-p)
                     (remove-hook
                      'window-configuration-change-hook init-if-visible-fn
                      t)
                     (funcall real-func)
                     t))))
         (when (buffer-live-p buffer)
           (with-current-buffer buffer
             (when (not (funcall init-if-visible-fn))
               (add-hook 'window-configuration-change-hook init-if-visible-fn
                         nil
                         t)))))))))

;;;###autoload
(defun my-make-hook-function-defer-until-visible
    (real-func &optional loaded-feature)
  "Return a function that (`my-defer-until-visible' REAL-FUNC).
LOADED-FEATURE: non-nil means if this library is loaded, don't do any
deferring."
  (lambda ()
    (:documentation
     (format "`my-make-hook-function-defer-until-visible' around `%s'."
             real-func))
    (cond
     ((and loaded-feature (featurep loaded-feature))
      (funcall real-func))
     (:else
      (my-defer-until-visible real-func)))))

;;;###autoload
(defun my-elisp-add-doc-to-variable-maybe (symbol added-doc)
  (-let* ((old-doc (documentation-property symbol 'variable-documentation)))
    (unless (string-search added-doc old-doc)
      (put symbol 'variable-documentation (concat old-doc added-doc)))))

;;;###autoload
(defun my-elisp-add-doc-to-function-maybe (symbol added-doc)
  (-let* ((old-doc (my-get-function-documentation symbol)))
    (unless (string-search added-doc old-doc)
      (put symbol 'function-documentation (concat old-doc added-doc)))))

;;;###autoload
(defun my-get-symbol-all-documentations|docstrings (symbol)
  (list
   (my-get-function-documentation symbol)
   (and (boundp symbol) (documentation-property symbol 'variable-documentation))
   (and (facep symbol) (documentation-property symbol 'face-documentation))))

;;;###autoload
(defun my-get-function-documentation (func)
  "Get FUNC's `function-documentation'."
  ;; Even when `fboundp', `void-function' may still be raised.
  (condition-case err
      (or (documentation-property func 'function-documentation)
          ;; `documentation' calls `kill-buffer'?
          (dlet ((kill-buffer-hook nil))
            (documentation func)))
    ((void-function invalid-function) nil)))

;;;###autoload
(defun my-disable-so-long (&rest _)
  (setq-local so-long-enabled nil))

;; (defvar-local indent-bars-mode nil)

;;;###autoload
(defun my-indent-bars-turn-on-mode ()
  (when (my-mode-derived-p major-mode my-edit-modes)
    (indent-bars-mode)))

;;;###autoload
(define-globalized-minor-mode my-indent-bars-global-mode
  indent-bars-mode
  my-indent-bars-turn-on-mode)

;;;###autoload
(defun my-setup-indent-bars ()
  (when (and (not noninteractive) (fboundp #'indent-bars-mode))
    (my-add-hook-once
      my-edit-mode-hooks
      (lambda () (make-thread #'my-indent-bars-global-mode))))
  ;; https://github.com/jdtsmith/indent-bars?tab=readme-ov-file#compatibility
  (when (and (< emacs-major-version 30) (equal 'pgtk window-system))
    (setq indent-bars-prefer-character t)))


;;;###autoload
(defun my-setup-minibuffer ()

  (add-hook 'savehist-save-hook #'my-make-emacs-save-directories)

  ;; Choice for `completion-in-region-function'?
  ;; `consult-completion-in-region' is ugly to configure;
  ;; `ivy-completion-in-region' doesn't use the default completion style and it
  ;; sometimes doesn't display the cursor;
  ;; `selectrum-completion-in-region' isn't dynamic;

  (-if-let* ((vertical
              (-find #'commandp '(vertico-mode fido-vertical-mode ivy-mode))))
      (my-add-hook-once 'pre-command-hook vertical)
    (my-eval-until-no-error
     (fido-mode)
     (progn
       (icomplete-mode)
       (ido-mode))
     nil))

  (setq extended-command-suggest-shorter nil) ;; only useful for partial-completion style
  (setq read-extended-command-predicate #'command-completion-default-include-p)

  ;; like `icomplete-fido-exit'
  (my-bind :map 'ido-common-completion-map "M-j" #'ido-select-text)
  (my-bind :map 'ivy-minibuffer-map "M-j" #'ivy-immediate-done)
  (my-bind :map 'vertico-map "M-j" #'vertico-exit-input)

  (when (fboundp #'vertico-mode)
    (add-hook 'minibuffer-setup-hook #'vertico-repeat-save)
    (add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy))

  (with-eval-after-load 'vertico

    (my-bind :map 'vertico-map "C->" #'my-vertico-inc-count||height)
    (my-bind :map 'vertico-map "C-<" #'my-vertico-dec-count||height)

    (my-bind
      :map 'vertico-map "C-<backspace>"
      `(menu-item
        ""
        vertico-directory-up
        :filter my-kmi-filter-minibuffer-file-category))

    (vertico-mouse-mode)

    ;; still can't catch entries that are added later
    (add-hook
     'vertico-multiform-mode-hook #'my-vertico-multiform-merge-settings-h)
    (with-eval-after-load 'vertico-multiform
      (my-add-list!
        'vertico-multiform-commands
        `(
          ;; ("^find-file" (vertico-sort-function . my-vertico-sort-files))
          (,(concat "^Info-" (regexp-opt '("menu" "index")))
           (vertico-sort-function . nil))))
      (my-add-list!
        'vertico-multiform-categories
        `((file (vertico-sort-function . my-vertico-sort-files)))))
    (vertico-multiform-mode)


    (setq vertico-resize nil)
    (setq vertico-count 12)
    (setq-default vertico-sort-function #'my-vertico-sort-prefer-literal)
    ;; nearest keys to "q"
    (setq vertico-quick1 "qazwsxedcrfv")
    (my-bind :map 'vertico-map "M-q" #'vertico-quick-exit)
    (add-hook 'vertico-mode-hook #'my-minibuffer-setup-category-maps-h))
  ;;


  (my-bind
    :map
    'minibuffer-local-filename-completion-map
    "M-<home>"
    #'my-minibuffer-filename-go-home)

  (my-bind
    :map 'minibuffer-mode-map "`"
    `(menu-item
      ""
      my-find-file-toggle-recursion
      :filter my-kmi-filter-minibuffer-file-category))

  (add-hook 'rfn-eshadow-update-overlay-hook #'my-find-file-insert-/-after-~-h)

  (when (fboundp 'marginalia-mode)
    ;; `minibuffer-setup-hook' may be too late
    (my-add-hook-once 'minibuffer-mode-hook 'marginalia-mode)
    (when (fboundp #'nerd-icons-completion-marginalia-setup)
      (add-hook
       'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup)))

  (my-setup-consult)
  (when (fboundp #'consult-completion-in-region)
    (add-hook
     'vertico-mode-hook
     (my-defn
       my-consult-completion-in-region-if-vertico ()
       (setq-default completion-in-region-function
                     (if vertico-mode
                         #'consult-completion-in-region
                       #'completion--in-region)))))

  (setq consult-preview-key
        (cl-loop
         for (keys consult-cmd ivy-cmd) in
         `((("M-<up>")
            my-minibuffer-previous-line-and-preview
            ivy-previous-line-and-call)
           (("M-<down>")
            my-minibuffer-next-line-and-preview
            ivy-next-line-and-call)
           (("M-RET" "M-<return>") my-minibuffer-preview-call-no-quit ivy-call))
         append
         (progn
           (my-bind :map 'minibuffer-mode-map keys consult-cmd)
           (my-bind :map 'ivy-minibuffer-map keys ivy-cmd)
           keys)))
  (my-after-each
    '(emacs ivy consult)
    (setq
     consult-async-min-input 1
     ivy-more-chars-alist '((t . 1))))

  (advice-add
   #'completing-read-multiple
   :filter-args #'my-crm-indicator--filter-args-a)

  (advice-add
   #'completing-read-multiple
   :around #'my-crm-completing-read-multiple-setup-hook-a)

  (my-bind
    :map
    'crm-local-completion-map
    '("TAB" "<tab>")
    #'my-crm-insert-candidate-and-separator)
  (my-bind
    :map
    'vertico-map
    '("TAB" "<tab>")
    `(menu-item "" nil :filter my-vertico-kmi-filter-tab-key))
  (defun my-vertico-kmi-filter-tab-key (_)
    (cond
     (my-crm-completing-read-multiple-flag
      #'my-crm-insert-candidate-and-separator)
     (:else
      #'vertico-insert)))

  ;; https://github.com/minad/vertico?tab=readme-ov-file#problematic-completion-commands

  (my-minibuffer-input-history-mode))

;;;###autoload
(defun my-setup-envrc ()
  (when (fboundp #'envrc-global-mode)
    (envrc-global-mode))
  (my-windowpopup-set
   ` "\\`\\*envrc\\*\\'" nil
   '((body-function . my-windowdisplay-no-mode-line)
     (window-height . 5)))
  (with-eval-after-load 'envrc
    (set-popup-rule!
     (my-make-buffer-match-regexp-and-goto-beg-pred "\\`\\*envrc\\*")
     :height 5
     :modeline nil)))

;;;###autoload
(defun my-vertico-multiform-merge-settings-h ()
  (dolist (var '(vertico-multiform-commands vertico-multiform-categories))
    (set var (my-alist-merge-duplicate-keys (symbol-value var)))))

(defvar my-context-menu--things '())
(defun my-context-menu--add-to-report (detector thing)
  (push (format "%s %s"
                (propertize (format "%s" detector)
                            'face 'font-lock-type-face)
                (my-remove-text-property-display-image thing))
        my-context-menu--things))

;;;###autoload
(defun my-context-menu (menu click)
  (setq my-context-menu--things '())
  (define-key-after menu [my-context-menu] menu-bar-separator)
  (define-key-after
    menu
    [my-search-online-by-web-browser]
    `(menu-item "Search on web" my-search-online-by-web-browser))
  (my-context-menu/url menu)
  (my-context-menu/dictionary menu click)
  (my-context-menu/filename-at-point menu)
  (my-context-menu/image menu)
  (message "`%s': %s"
           (propertize "context" 'face 'font-lock-doc-markup-face)
           (string-join my-context-menu--things "\t"))
  (setq my-context-menu--things '())
  menu)

;;;###autoload
(defun my-context-menu/filename-at-point (menu)
  (-when-let* ((file-name (thing-at-point 'filename)))
    (when (or (file-exists-p file-name) (string-search "/" file-name))
      (my-context-menu--add-to-report :file file-name)
      ;; `file-file' and `find-file-other-window' are already provided by
      ;; `embark-file-map'
      (define-key-after menu [find-file-other-frame]
        `(menu-item
          ,(format "Open file in new frame" file-name)
          ,(lambda ()
             (interactive)
             (find-file-other-frame file-name))))
      (define-key-after menu [my-rename-file-by-modification-time]
        `(menu-item
          "Rename by modification time" my-rename-file-by-modification-time))))
  menu)

;;;###autoload
(defun my-context-menu/dictionary (menu click)
  (-when-let* ((text
                (or (my-region-string-maybe nil 'trim)
                    (thing-at-point 'word 'no-properties))))
    (my-context-menu--add-to-report :text text)
    (define-key-after
      menu
      [powerthesaurus-transient]
      `(menu-item "Thesaurus" powerthesaurus-transient))))
;; (when (fboundp #'dictionary-context-menu)
;;   (dictionary-context-menu menu click))


;;;###autoload
(defun my-context-menu/url (menu)
  (-when-let* ((url (thing-at-point 'url)))
    (my-context-menu--add-to-report :url url)
    (let ((sub-menu (make-sparse-keymap)))
      (define-key-after sub-menu [kill-new]
        `(menu-item ,(format "Copy %s" url)
                    ,(lambda () (interactive) (kill-new url))))
      (define-key-after sub-menu [browse-url]
        `(menu-item ,(format "Open %s" url)
                    ,(lambda () (interactive) (browse-url url))))
      (define-key-after menu [my-context-menu/url]
        `(menu-item "URL" ,sub-menu)))))

;;;###autoload
(defun my-context-menu/image (menu)
  (when (my-safe-call #'image-at-point-p)
    (my-image-context-menu menu)))

;;;###autoload
(defun my-context-menu-add-local (mode func &optional depth now)
  (-let* ((hook-fn (intern (format "%s--%s--%s" 'my-context-menu-add-local
                                   mode func))))
    (defalias hook-fn (lambda ()
                        (add-hook 'context-menu-functions func depth 'local)))
    (if now
        (my-add-mode-hook-and-now mode hook-fn)
      (add-hook (my-mode->hook mode) hook-fn))))

(defvar-local my-persistent-mode-line-note--note nil)
(define-minor-mode my-persistent-mode-line-note-mode nil
  :lighter (:eval my-persistent-mode-line-note--note))
(add-hook 'minions-prominent-modes 'my-persistent-mode-line-note-mode)
;;;###autoload
(defun my-make-persistent-mode-line-note-function (note &optional remove)
  (lambda (&rest _)
    (my-persistent-mode-line-note note remove)))
;;;###autoload
(defun my-persistent-mode-line-note (note &optional remove)
  (if remove
      (my-persistent-mode-line-note-mode 0)
    (progn
      (setq-local my-persistent-mode-line-note--note
                  (my-ensure-string-prefix " " note))
      (my-persistent-mode-line-note-mode t))))

;;;###autoload
(defun my-when-mode0-turn-off||disable-mode1 (mode0/s mode1/s &optional after)
  "When MODE0/S is turned on/enabled, temporarily turn off/disable MODE1/S."
  (with-eval-after-load (or after 'emacs)
    (dolist (mode0 (ensure-list mode0/s))
      (dolist (mode1 (ensure-list mode1/s))
        (let ((mode1-old-val (my-concat-symbols '@ 'my- mode1 'before mode0 "?")))
          (my-set-ensure-defined-variable mode1-old-val nil t)
          (let ((func (my-concat-symbols
                       '@ 'my-when mode0 'temp-turn-off||disable mode1)))
            (defalias func
              (lambda ()
                (when (symbol-value mode0)
                  (set mode1-old-val mode1)
                  (funcall mode1 0))
                (unless (symbol-value mode0)
                  (when mode1-old-val
                    (funcall mode1 1)))))
            (add-hook (my-mode->hook mode0) func)))))))

;;;###autoload
(defun my-demote-errors-of-while-executing (child-fn parent-fn)
  "While executing PARENT-FN, demote CHILD-FN's errors."
  (let ((adv (my-concat-symbols '@ #'my-demote-errors-of-while-executing child-fn parent-fn)))
    (defalias adv
      (lambda (func &rest args)
        ;; advise child-fn
        (my-with-advice child-fn :around #'my-with-demoted-errors-a
          (apply func args))))
    (advice-add parent-fn :around adv)
    adv))

;;;###autoload
(defun my-key-chord-define--orderded (keymap keys command)
  (when (/= 2 (length keys))
    (error "Key-chord keys must have two elements"))
  (let ((key1 (logand 255 (aref keys 0)))
        (key2 (logand 255 (aref keys 1))))
    (if (eq key1 key2)
        (define-key keymap (vector 'key-chord key1 key2) command)
      (define-key keymap (vector 'key-chord key1 key2) command))))

;;;###autoload
(defun my-define-ordered-key-chord (keys command &optional key-map)
  (cond (noninteractive)                ; Don't proceed when not interactive
        ((not (fboundp 'key-chord-mode))
         (message "`key-chord' is not installed."))
        (t
         (unless (bound-and-true-p key-chord-mode)
           (key-chord-mode))
         (my-key-chord-define--orderded
          (or key-map (current-global-map))
          keys
          command))))

;;;###autoload
(defun my-scratch-initial ()
  (setq initial-major-mode #'lisp-interaction-mode)
  (prog1
      (fundamental-mode)
    (my-add-local-hook-once 'post-self-insert-hook 'lisp-interaction-mode nil)))

;;;###autoload
(defun my-advice-function-to-run-hook (func hook-sym &optional how advice-name)
  "Advice FUNC such that it run HOOK-SYM.
HOW defaults to :after, the advice's name is ADVICE-NAME or
generated from other arguments."
  (-let* ((how (or how :after))
          (advice-name
           (or advice-name
               (intern
                (format "%s--%s--%s--%s"
                        'my-advice-function-to-run-hook
                        func
                        how
                        hook-sym)))))
    (defalias advice-name (lambda (&rest _) (run-hooks hook-sym)))
    (advice-add func how advice-name)
    (unless (boundp hook-sym)
      (set hook-sym '()))
    advice-name))

;;;###autoload
(defun my-turned-off-mode-between (beg-sym end-sym mode &optional advice)
  "Ensure that MODE is turned off between BEG-SYM and END-SYM.
Non-nil ADVICE: advice after BEG-SYM and END-SYM instead of
adding elements to them as hooks."
  (-let* ((beg-fn
           (intern
            (format "%s--%s--%s--beg-fn"
                    'my-turned-off-mode-between
                    beg-sym
                    mode)))
          (end-fn
           (intern
            (format "%s--%s--%s--end-fn"
                    'my-turned-off-mode-between
                    end-sym
                    mode)))
          (state-sym
           (intern
            (format "%s--%s-%s--%s--state"
                    'my-turned-off-mode-between
                    beg-sym
                    end-sym
                    mode))))
    (make-variable-buffer-local state-sym)
    (defalias beg-fn
      (lambda (&rest _)
        (-let* ((status (my-bounded-value mode)))
          (set state-sym status)
          (when status
            (funcall mode 0)))))
    (defalias end-fn
      (lambda (&rest _)
        (when (symbol-value state-sym)
          (funcall mode 1))))
    (if advice
        (advice-add beg-sym :after beg-fn)
      (add-hook beg-sym beg-fn))
    (if advice
        (advice-add end-sym :after end-fn)
      (add-hook end-sym end-fn))
    (vector beg-fn end-fn state-sym advice)))

;;;###autoload
(defun my-emacsclient-guess-invoked-as-editor-p ()
  (and
   ;;; (getenv "_" (selected-frame))
   ;; (getenv "COLORTERM" (selected-frame))
   ;; (getenv "PROFILEHOME" (selected-frame))
   ;; (getenv "SHELL_SESSION_ID" (selected-frame))
   (getenv "TERM" (selected-frame))
   ;; (getenv "WINDOWID" (selected-frame))
   (processp (frame-parameter nil 'client))))

;;;###autoload
(defun my-invoked-as-editor-original-buffer ()
  (when
      ;; (frame-parameter nil 'my-invoked-as-editor)
      (my-emacsclient-guess-invoked-as-editor-p)
    (unless (frame-parameter nil 'my-invoked-as-editor-original-buffer)
      (set-frame-parameter nil 'my-invoked-as-editor-original-buffer (buffer-name)))
    (frame-parameter nil 'my-invoked-as-editor-original-buffer)))

(defvar my-persistuse--table '())

;;;###autoload
(cl-defun my-persistuse (symbl
                         get-val-fn
                         &optional
                         use-fn
                         &rest
                         args
                         &key
                         elem-pred
                         set-pred
                         refresh)
  "Persist SYMBL and call (USE-FN 〈its value〉).

When its value is nil or non-nil REFRESH (true when called
interactively), set its value to (GET-VAL-FN). Only operate on global
values.

Note that this may not work immediately but only after loading
the underlying variable saver."
  (declare (indent defun))
  (add-to-list
   'my-persistuse--table `(,symbl ,get-val-fn ,use-fn ,@args))
  (-let* ((fn
           (lambda (&optional get-persisted-fn set-persisted-fn)
             (-let* ((get-persisted-fn (or get-persisted-fn #'ignore))
                     (set-persisted-fn (or set-persisted-fn #'ignore))
                     (val-curr (default-value symbl)))
               (when (or refresh (null val-curr))
                 (-let* ((val-saved (funcall get-persisted-fn symbl))
                         (val-fresh
                          (cond
                           ((or refresh (null val-saved))
                            (funcall get-val-fn))
                           (:else
                            val-saved))))
                   (set-default symbl val-fresh)
                   (when (or refresh (null val-saved))
                     (message "`my-persistuse': Setting `%s' to `%S'."
                              symbl
                              val-fresh)
                     (funcall set-persisted-fn symbl val-fresh))))
               (when use-fn
                 (funcall use-fn (default-value symbl)))))))
    (my-set-ensure-defined-variable symbl '())
    (cond

     ;; `multisession.el' loads slower
     ((fboundp #'list-multisession-values)
      (require 'multisession)
      ;; (mtss-sym (intern (format "%s--variable---%s" 'my-multisession symbl)))
      (-let* ((mtss-obj
               (make-multisession
                :package "my"
                :key (symbol-name symbl)
                :initial-value nil)))
        ;; (defvar mtss-sym mtss-obj)
        (funcall fn
                 (lambda (_) (multisession-value mtss-obj))
                 (lambda (_ val2)
                   ;; (setf (multisession-value mtss-obj) val2)
                   (multisession--set-value mtss-obj val2)))
        mtss-obj))

     (:else
      (when (not (bound-and-true-p savehist-mode))
        (savehist-mode))
      (progn
        ;; with-eval-after-load 'savehist
        (add-hook 'savehist-additional-variables symbl)
        (-let* ((savehist-fn (lambda () (funcall fn))))
          (if savehist-mode
              (funcall savehist-fn)
            (my-add-hook-once 'savehist-mode-hook savehist-fn))))))))

;;;###autoload
(defun my-persistuse-refresh-all ()
  (interactive)
  (savehist-mode)
  (seq-do
   (-lambda ((symbol . args))
     (set-default symbol nil)
     (apply #'my-persistuse symbol `(,@args :refresh t))
     (message "`my-persistuse-refresh-all': %S := %S"
              symbol
              (default-value symbol)))
   my-persistuse--table))

;;;###autoload
(defun my-crm-separator-literal ()
  (replace-regexp-in-string "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" "" crm-separator))

;; https://github.com/minad/vertico?tab=readme-ov-file#configuration
;;;###autoload
(defun my-crm-indicator--filter-args-a (args)
  (cons (format "[CRM%s] %s" (my-crm-separator-literal) (car args)) (cdr args)))

;;;###autoload
(defun my-delay-function-to-async-callback (func &optional rerun callback+)
  "Delay FUNC by (`ignore'-like) `advice-add' it.
Return a closure (1) that removes the said advice. RERUN: record the
arguments and re-run them when (1) is called. CALLBACK+: also run it
with (1)'s received arguments."
  (declare (indent defun))
  (-let* ((recorded-args-list '())
          (internal-adv
           (lambda (_ &rest args)
             (when rerun
               (push (vector (vector (current-buffer) default-directory) args)
                     recorded-args-list))
             nil))
          (internal-adv-sym (gensym)))
    (defalias internal-adv-sym internal-adv)
    (advice-add func :around internal-adv-sym)
    (lambda (&rest ret-adv-args)
      (advice-remove func internal-adv-sym)
      (when (and rerun (fboundp func))
        (setq recorded-args-list (nreverse recorded-args-list))
        (dolist (env-args recorded-args-list)
          (-let* (([[buf dir] args] env-args))
            (cond
             ((buffer-live-p buf)
              (with-current-buffer buf
                (apply func args)))
             (:else
              (dlet ((default-directory dir))
                (apply func args)))))))
      (when callback+
        (apply callback+ ret-adv-args)))))

;;;###autoload
(defun my-doom-edit-dashboard-widget-banner ()
  (save-excursion
    (goto-char (point-min))
    ;; insert version information after "E M A C S"
    (when (re-search-forward "\s*E\s?M\s?A\s?C\s?S\s*" nil t)
      (-let* ((replacement (format "EMACS %s" emacs-version))
              (mbeg (match-beginning 0))
              (mend (match-end 0))
              (len0 (- mend mbeg))
              (pad-left (floor (/ (- len0 (length replacement)) 2)))
              (pad-right (- len0 (length replacement) pad-left))
              (replacement-final
               (-->
                (concat
                 (make-string pad-left ?\s)
                 replacement
                 (make-string pad-right ?\s))
                (propertize it 'face 'doom-dashboard-banner))))
        (replace-match replacement-final)))))

;;;###autoload
(define-globalized-minor-mode my-hl-line-global-mode
  hl-line-mode
  (lambda ()
    (when (my-derived-mode-p
           `(,@my-edit-modes special-mode dired-mode))
      (hl-line-mode))))

(defun my-custom-set-face--convert-specs (specs)
  "Convert SPECS' form from Doom's `my-custom-set-faces' to `my-custom-set-faces.
'((face0 :weight normal)
  ((face1 face2) :inherit unspecified))
->
'((face0 ((t (:weight normal))) t)
  (face1 ((t (:inherit unspecified))) t)
  (face2 ((t (:inherit unspecified))) t))"
  (cl-loop for (face/s . plist) in specs
           append (-map (lambda (face)
                          `(,face ((t ,plist)) t))
                        (ensure-list face/s))))

(defun my-custom-set-faces (&rest specs)
  (declare (indent defun))
  (apply #'custom-set-faces
         (my-custom-set-face--convert-specs specs)))

(defun my-custom-theme-set-faces (theme/s &rest specs)
  (declare (indent defun))
  (let ((specs* (my-custom-set-face--convert-specs specs)))
    (dolist (theme (ensure-list theme/s))
      (when (or (custom-theme-enabled-p theme) (equal theme 'user))
        (apply #'custom-theme-set-faces theme specs*)))))

;; Doom functions/macros
(when (not (fboundp #'set-file-template!)) (defalias #'set-file-template! #'ignore))
(when (not (fboundp #'set-popup-rule!)) (defalias #'set-popup-rule! #'ignore))
(when (not (fboundp #'set-popup-rules!)) (defalias #'set-popup-rules! #'ignore))
(when (not (fboundp #'set-repl-handler!)) (defalias #'set-repl-handler! #'ignore))
(when (not (fboundp #'disable-packages!)) (defmacro disable-packages! (&rest _) nil))
(when (not (fboundp #'map!)) (defmacro map! (&rest _) nil))
(when (not (fboundp #'unpin!)) (defmacro unpin! (&rest _) nil))
(when (not (fboundp #'modulep!)) (defmacro modulep! (&rest _) nil))
(when (not (fboundp #'package!)) (defmacro package! (&rest _) nil))

;;;###autoload
(defun my-setup-devil ()
  (when (and (fboundp #'devil-mode))
    ;; Quickly type literal number arrays like "[1,2,3]", use M-%d for digit
    ;; arguments.
    (with-eval-after-load 'devil
      (dolist (i (number-sequence 0 9))
        (add-to-list
         'devil-special-keys
         (cons (format "%%k %d" i) (devil-key-executor (format "%%k %d" i))))))
    (with-eval-after-load 'evil-maps
      (my-bind :vi '(motion) "," #'devil))
    (global-devil-mode)))

;;;###autoload
(defun my-setup-recentf ()
  (declare)
  
  ;; Auto-cleaning up may invoke `tramp-autoload-file-name-handler', which may
  ;; load various packages (see `tramp-foreign-file-name-handler-alist')
  (setq recentf-auto-cleanup 300)

  (add-hook 'recentf-exclude #'my-recentf-exclude)

  (advice-add #'recentf-mode :around #'my-with-inhibit-message-a)
  
  (my-add-hook-once 'window-configuration-change-hook 'recentf-mode))

;;;###autoload
(defun my-setup-lang-emacs-elisp ()
  (declare)
  ;; disable implicit defun evaluation for dangerous modes
  (my-bind :map '(emacs-lisp-mode-map) [remap my-evalu-dwim] #'my-evalu-region)
  (add-hook
   'eval-expression-minibuffer-setup-hook #'my-lisp-set-comment-start-h)
  (add-hook
   'eval-expression-minibuffer-setup-hook
   #'my-eval-expression-minibuffer-setup-flag-h)
  (setq eval-expression-print-length nil)
  (advice-add
   #'elisp-flymake-byte-compile
   :around #'my-elisp-flymake-add-load-path-a) ; Bug#48452 https://yhetil.org/emacs-bugs/8735unafob.fsf@gmail.com/
  (my-evalu-set-handler 'emacs-lisp-mode #'my-lang-elisp-repl-ielm :region #'eval-region)
  (with-eval-after-load 'elisp-autofmt
    (my-add-list! 'elisp-autofmt-load-packages-local '("dash"))) ; to macro indentations, but not effective? 
  (when (fboundp #'cape-capf-super)
    (add-hook 'emacs-lisp-mode-hook #'my-elisp-completion-buffer-symbols-h))
  (my-bind
    :map
    'read-expression-map
    [remap read--expression-try-read]
    #'my-read-expression/s-try-read)

  (my-bind :map '(emacs-lisp-mode-map lisp-interaction-mode-map) "M-l e n" #'my-elisp-eval-next-sexp)
  (my-bind :map '(emacs-lisp-mode-map lisp-interaction-mode-map) "M-l e u" #'my-elisp-eval-upper-sexp)
  (my-bind :map '(emacs-lisp-mode-map lisp-interaction-mode-map) "M-l M" #'lispy-multiline)

  (setq edebug-print-length nil)
  (setq edebug-save-windows nil)

  (my-windowpopup-set '(derived-mode inferior-emacs-lisp-mode))

  (add-hook 'trusted-content my-emacs-conf-dir/))

(defvar-local my-git-commit-timezone-UTC t
  "Try to use UTC timezone when committing.
To be enabled along with `my-git-commit-timezone-UTC-maybe-mode'.")

;;;###autoload
(defun my-git-commit-timezone-UTC-maybe-a (func &rest args)
  (cond
   (my-git-commit-timezone-UTC
    (with-environment-variables (("TZ" "UTC"))
      (dlet (;; (magit-git-environment
             ;;  `("TZ=UTC" ,@(bound-and-true-p magit-git-environment)))
             )
        (apply func args))))
   (:else
    (apply func args))))

;;;###autoload
(define-minor-mode my-git-commit-timezone-UTC-maybe-mode
  "See `my-git-commit-timezone-UTC-maybe-mode'.
Not perfect, only try to cover some known commands."
  :global
  t
  (cond
   (my-git-commit-timezone-UTC-maybe-mode
    (advice-add #'gac-commit :around #'my-git-commit-timezone-UTC-maybe-a)
    (advice-add
     #'magit-process-environment
     :around #'my-git-commit-timezone-UTC-maybe-a))
   (:else
    (advice-remove #'gac-commit #'my-git-commit-timezone-UTC-maybe-a)
    (advice-remove
     #'magit-process-environment #'my-git-commit-timezone-UTC-maybe-a))))

;;;###autoload
(defun my-setup-vc-git ()
  (my-git-commit-timezone-UTC-maybe-mode))

;;; Popups


(defvar my-lisp-major-modes
  '(clojure-mode
    clojure-ts-mode
    common-lisp-mode
    emacs-lisp-mode
    lisp-data-mode
    racket-mode
    scheme-mode)
  "Sorted by `string<'.")

(defvar my-lisp-repl-modes
  '(cider-repl-mode
    geiser-repl-mode inferior-emacs-lisp-mode racket-repl-mode)
  "Sorted by `string<'.")

;;;###autoload
(defun my-buffer-name-string (&optional buf)
  (let* ((buf (or buf (buffer-name))))
    (if (stringp buf)
        buf
      (buffer-name buf))))

;;;###autoload
(defun my-make-buffer-match-regexp-and-goto-beg-pred (regexp)
  "Return a predicate that matches REGEXP, if so in there go to start.
Usage: some log buffers are short, but landing at the end of them so not readable.
MODIFY `match-data'!"
  (lambda (buf &rest _)
    (and (string-match regexp (my-buffer-name-string buf))
         (with-current-buffer buf
           (goto-char (point-min)))
         t)))

(defvar my-windowpopup-default-action-functions '(display-buffer-in-side-window))
(defvar my-windowpopup-default-action-alist
  `((side . bottom) (window-height . ,(/ 1.0 3)) (slot . -1)))

;;;###autoload
(cl-defun my-windowpopup-set (condition
                              &optional
                              action-funtion/s
                              action-alist
                              &key
                              no-modeline
                              &allow-other-keys)
  "Set `display-buffer-alist' for CONDITION.
See Info node `(elisp) Buffer Display Action Alists' for
ACTION-FUNTION/S and ACTION-ALIST. NO-MODELINE: hide the mode-line.
TODO: restore mode-line when window is at horizontally flat at the
bottom anymore."
  (-let* ((action-funtions
           (or (my-ensure-list action-funtion/s)
               my-windowpopup-default-action-functions))
          (body-fn-0 (alist-get 'body-function action-alist))
          (body-fn
           (cond
            ((and no-modeline body-fn-0)
             (lambda (window)
               (funcall body-fn-0 window)
               (my-windowdisplay-no-mode-line window)))
            ((and no-modeline (not body-fn-0))
             #'my-windowdisplay-no-mode-line)
            ((and (not no-modeline) body-fn-0)
             body-fn-0)
            (:else
             nil)))
          (value
           `(,action-funtions
             (body-function . ,body-fn)
             ,@action-alist
             ,@my-windowpopup-default-action-alist)))
    (my-set-alist! 'display-buffer-alist condition value :global t)))

;;;###autoload
(defun my-popup-guess-repl-buffer-p (&optional buf-name)
  "Return if BUF-NAME is a REPL using some heuristics."
  (-let* ((buf-name (or buf-name (buffer-name))))
    (and
     (not (buffer-local-value 'buffer-read-only (get-buffer buf-name)))
     (string-match-p "\\`\\*[^*]*\\*" buf-name)
     (get-buffer-process buf-name))))

;;;###autoload
(defun my-popup-repl-predicate (buf _action)
  (my-popup-guess-repl-buffer-p (my-buffer-name-string buf)))

;;;###autoload
(defun my-popup-previewing-help-predicate (buf _action)
  (dlet ((case-fold-search t))
    (and (string-match-p "\\`\\*help[^*]*\\*\\'"
                         (my-buffer-name-string buf))
         (active-minibuffer-window))))

;;;###autoload
(defun my-popup-repl-set-rule (&optional remove)
  (if remove
      (my-delete-alist! 'display-buffer-alist #'my-popup-repl-predicate)
    (set-popup-rule! #'my-popup-repl-predicate
                     ;; :select nil
                     :side 'bottom :height (/ 1.0 3) :ttl nil :quit t)))

;;;###autoload
(defun my-setup-popup-rules ()
  "."
  ;; (my-popup-repl-set-rule)
  (set-popup-rules!
   ;; Prefer placing *help* buffers on the right/left when the `minibuffer' is active
   `((my-popup-previewing-help-predicate :ignore t :ttl nil))
   ;; Bigger window
   `(("\\`\\*edit-indirect " :height 0.5 :quit nil))
   `(
     ;;

     ("\\`\\*Diff\\*\\(<[0-9]+>\\)?\\'" :ignore t)
     ("\\`\\*Packages\\*" :ignore t)
     ("\\`\\*\\(:?doom:\\)?scratch[^*]*\\*" :ignore t)
     ("\\`\\*\\(un\\)?sent [^*]+\\*" :ignore t)          ; not a popup
     ("\\`\\*ert\\*" :ignore t)
     ("\\`\\*info\\*\\(<[0-9]+>\\)?\\'" :ignore t)    ; not a popup
     ("\\`\\*tree-sitter explorer for .*?\\*" :ignore t)))) ; otherwise unworkable

;;;###autoload
(defun my-setup-sideline ()
  (declare)
  (setq sideline-backends-right-skip-current-line nil)
  (when (and (fboundp #'sideline-mode))
    (my-add-startup-hook
      (my-add-hook-once 'find-file-hook #'global-sideline-mode)))
  (setq-default sideline-inhibit-display-function #'my-sideline-inhibit-p))

;;;###autoload
(defun my-setup-treesit-fold ()
  (when (fboundp #'treesit-fold-mode)
    (my-add-hook-once
      'my-treesit-major-mode-setup-hook
      (lambda () (make-thread #'global-treesit-fold-indicators-mode)))))

;;;###autoload
(defun my-setup-elisp-tree-sitter--1 ()
  (setq
   tree-sitter-debug-jump-buttons t
   tree-sitter-debug-highlight-jump-region t)

  (my-add-hook/s
    '(tree-sitter-after-on-hook)
    '(tree-sitter-hl-mode my-elisp-tree-sitter-mode))

  (when (fboundp #'tree-sitter-mode)

    (my-persistuse
      'my-elisp-tree-sitter-mode-hook-list
      (lambda ()
        (and (require 'tree-sitter-langs nil t)
             (-map
              (-lambda ((mode . _)) (my-mode->hook mode))
              tree-sitter-major-mode-language-alist)))
      (lambda (hooks)
        (my-add-hook-once hooks #'my-elisp-tree-sitter-global-mode)))

    (defun my-er/expand-region-add-elisp-tree-sitter-expansions ()
      (when (bound-and-true-p tree-sitter-mode)
        (add-hook 'er/try-expand-list 'my-er/elisp-tree-sitter-expand-region
                  nil
                  'local)))
    (add-hook
     'my-er/expand-region-before-first-expand-in-buffer-hook
     #'my-er/expand-region-add-elisp-tree-sitter-expansions)

    ;;
    ))

;;;###autoload
(defun my-setup-elisp-tree-sitter ()
  (if (or (bound-and-true-p my-initial-file-in-CLI-args))
      (my-setup-elisp-tree-sitter--1)
    (my-add-hook-once 'find-file-hook #'my-setup-elisp-tree-sitter--1)))

;;; Eager advices and hooks

;;;; Generic

;;;###autoload
(defun my-second-argument (_arg arg &rest _)
  arg)

;;;###autoload
(cl-defun my-recenter-top-to-fraction (&optional (fraction 0.25))
  (interactive)
  (recenter-top-bottom (floor (* fraction (window-height)))))

;;;###autoload
(defun my-recenter-top-to-a-quarter|1/4-when-interactive_-a (&rest _)
  (when (called-interactively-p 'any)
    (my-recenter-top-to-fraction 0.25)))

;;;###autoload
(defun my-kill-to-end-of-list-when-C-u-interact--a (func &rest args)
  (if (and (called-interactively-p 'any)
           (equal current-prefix-arg '(4)))
      (my-greedy-kill-infi-all-sexps)
    (apply func args)))

;;;###autoload
(defun my-save-excursion-when-interactively-called-with-prefix-argument--around-a (func &rest args)
  (if (and (called-interactively-p 'any)
           current-prefix-arg)
      (save-excursion
        (apply func args))
    (apply func args)))

;;;###autoload
(defun my-high-scroll-margin-a (func &rest args)
  (dlet ((scroll-margin 16))
    (apply func args)))

(defun my-with-short-minibuffer-a (func &rest args)
  (dlet ((max-mini-window-height 1))
    (apply func args)))

;;;###autoload
(defun my-message-prevent-newlines-a (func &rest args)
  (dlet ((message-truncate-lines t))
    (apply func
           (-map
            (lambda (arg)
              (if (stringp arg)
                  (string-replace "\n" "⏎" arg)
                arg))
            args))))

;;;###autoload
(defun my-call-after-when-called-interactively-a (func0/s func1 &optional remove-advice props)
  "Advise FUNC0/S such that, after calling it interactively, call FUNC1.
FUNC1 must be a named symbols."
  (let ((adv (my-concat-symbols '@ 'my-call-after-when-called-interactively-a func1)))
    (my-maydefun-set adv
      ((func &rest args)
       (prog1
           (apply func args)
         (when (called-interactively-p 'any)
           (funcall func1)))))
    (dolist (func0 (ensure-list func0/s))
      (if remove-advice
          (advice-remove func0 adv)
        (advice-add func0 :around adv props)))))

;;;###autoload
(defun my-inhibit-when-repeating-command-a (fn &rest args)
  (unless (equal last-command this-command)
    (apply fn args)))

;;;###autoload
(defun my-with-switch-to-buffer-other-window->pop-to-buffer-a (func &rest args)
  (my-with-advice #'switch-to-buffer-other-window :override #'pop-to-buffer
    (apply func args)))

;;;###autoload
(defun my-with-save-excursion-a (func &rest args)
  (save-excursion (apply func args)))

;;;###autoload
(defun my-with-inhibit-message-a (func &rest args)
  (dlet ((inhibit-message t))
    (apply func args)))

;;;###autoload
(defun my-with-ignore-message-a (func &rest args)
  (my-with-ignore-message (apply func args)))

(defvar my-with-ignore-second-errors-a--table nil)
;;;###autoload
(defun my-with-ignore-second-errors-a (func &rest args)
  (condition-case err
      (apply func args)
    (error
     (unless my-with-ignore-second-errors-a--table
       (setq my-with-ignore-second-errors-a--table
             (make-hash-table :test #'equal)))
     (with-memoization (gethash
                        (vector func args err)
                        my-with-ignore-second-errors-a--table)
       (message "Error: %s" err)
       err)
     nil)))

(defvar my-with-ignore-second-errors-local-a--table nil)
;;;###autoload
(defun my-with-ignore-second-errors-local-a (func &rest args)
  (condition-case err
      (apply func args)
    (error
     (unless my-with-ignore-second-errors-local-a--table
       (setq my-with-ignore-second-errors-local-a--table
             (make-hash-table :test #'equal)))
     (with-memoization (gethash
                        (vector func args err)
                        my-with-ignore-second-errors-local-a--table)
       (message "Error: %s" err)
       err)
     nil)))

(defvar-local my-with-ignore-second-errors-non-args-local-a--table nil)
;;;###autoload
(defun my-with-ignore-second-errors-non-args-local-a (func &rest args)
  (condition-case err
      (apply func args)
    (error
     (unless my-with-ignore-second-errors-non-args-local-a--table
       (setq my-with-ignore-second-errors-non-args-local-a--table
             (make-hash-table :test #'equal)))
     (with-memoization (gethash
                        (vector func (car err))
                        my-with-ignore-second-errors-non-args-local-a--table)
       (message "Error: %s" err)
       err)
     nil)))

;;;###autoload
(defun my-ignore-file-missing-error-a (func &rest args)
  (ignore-error (file-missing)
    (apply func args)))

;;;###autoload
(defun my-ignore-when-no-frame-a (func &rest args)
  (cond
   ((equal (terminal-name) "initial_terminal")
    (message "`my-ignore-when-no-frame-a' %s" func)
    nil)
   (:else
    (apply func args))))

;;;###autoload
(defun my-run-local-var-hooks-h ()
  "Like `doom-run-local-var-hooks-h'."
  ;; currently no inheritance, treesit/-ts mode handling
  (run-hooks (intern-soft (format "%s-local-vars-hook" major-mode))))

;;;###autoload
(defun my-completing-read-no-require-match-a
    (func prompt collection &optional predicate _require-match &rest args)
  (apply func prompt collection predicate nil args))

;;;###autoload
(defun my-with-yes-or-no-p-auto-yes-a (func &rest args)
  (my-with-advices
    '(yes-or-no-p y-or-n-p)
    :override #'always (apply func args)))

;;;###autoload
(defun my-with-yes-or-no-p-auto-no-a (func &rest args)
  (my-with-advices
    '(yes-or-no-p y-or-n-p)
    :override #'ignore (apply func args)))

;;;###autoload
(defun my-with-delay-mode-hooks-a (func &rest args)
  (delay-mode-hooks
    (apply func args)))

;;;; Specialized

;;;###autoload
(defun my-re-enable-dtrt-mode-before-a (&rest _)
  "Use when other developers don't adhere to `editorconfig' and the
likes. Alternative approach: advise after
`editorconfig-set-indentation'."
  (when (and (called-interactively-p 'any)
             (bound-and-true-p dtrt-indent-mode))
    (dlet ((dtrt-indent-verbosity 0))
      (dtrt-indent-mode t))))

;;;###autoload
(defun my-dired-to-current-dir--around-a (func &rest args)
  (let ((dir default-directory))
    (prog1
        (apply func args)
      (dired dir))))

;;;###autoload
(defun my-smart-dash-maybe (&rest _)
  "Conditionally enable `smart-dash-mode'."
  (unless
      (or
       ;; Whether "-" belongs to the "Symbol" syntax class
       (char-equal ?_ (char-syntax ?-))
       (member major-mode
               '(
                 sh-mode)))                ;commands with "-" are common

    (my-safe (smart-dash-mode))))

;;;###autoload
(defun my-general-define-key-filter-bindings-a (func &rest args)
  (cl-destructuring-bind
      (common-args . bindings) (my-split-mixed-plist args)
    (let ((violated-keys (-intersection my-reserved-keys bindings))
          (keymap/s (plist-get common-args :keymaps))
          (state/s (plist-get common-args :states)))
      (cond
       ;; Hard-reserved keys
       ((and
         ;; But allow after prefixes and evil-* maps
         violated-keys
         (not (or (member :prefix args)
                  (-filter (lambda (m) (string-match-p "^evil-.+-map$" (symbol-name m)))
                           (ensure-list keymap/s)))))
        (dolist (key violated-keys)
          (let ((pos (cl-position key args :test #'equal)))
            (add-to-list 'my-modified-generals-bindings (cons key common-args))
            (setf (nthcdr pos args) (nthcdr (+ 2 pos) args)))))
       ;; Local insert-state bindings
       ((and (eq 'insert state/s)
             (not (member keymap/s '(global-map nil))))
        (add-to-list 'my-modified-generals-bindings args)
        (setq args nil))
       ;; Omni-completion
       ((and (eq 'insert state/s)
             (string= "C-x" (plist-get common-args :prefix)))
        (add-to-list 'my-modified-generals-bindings args)
        (setq args nil)))
      (apply func args))))

;;;###autoload
(defun my-general-define-key-filter-keymaps-a (func &rest args)
  "Don't touch some keymaps."
  (let ((to-avoid-maps '(magit-section-mode-map
                         minibuffer-local-map
                         minibuffer-mode-map
                         minibuffer-mode-map
                         vertico-map))
        (maps-in-args (ensure-list (plist-get args :keymaps))))
    (if (-intersection maps-in-args to-avoid-maps)
        (let ((maps (-difference maps-in-args to-avoid-maps)))
          (when maps
            (plist-put args :keymaps maps)
            (apply func args)))
      (apply func args))))

;;;###autoload
(defun my-delete-instead-of-kill-when-interactive-a (func &rest args)
  (if (called-interactively-p 'any)
      ;; let-bound `kill-ring' may make the next pasting, the last element in
      ;; `kill-ring' may be yanked instead of the first one; so we advice
      ;; instead
      (my-with-advice #'kill-region :override
        (lambda (beg end &optional _region)
          (delete-region beg end))
        (apply func args))
    (apply func args)))

;;;###autoload
(defun my-save-position-for-original-window-a (func &rest args)
  (let ((wind (selected-window))
        (p (point)))
    (unwind-protect
        (apply func args)
      (with-selected-window wind
        (goto-char p)))))

(defvar my-highlight-indent-guide-exclude-mode-list
  '(polymode-mode)) ;; ein:ipynb-mode


;;;###autoload
(defun my-highlight-indent-guide-filter-a (func &optional arg)
  (when (not
         (or (-some
              #'my-enabled-mode-p my-highlight-indent-guide-exclude-mode-list)))
             ;;; currently prevents treesit modes to even doing any highlighting
    ;; (my-safe-call #'treesit-parser-list)

    (funcall func arg)))

;;;###autoload
(defun my-eldoc-warm--eldoc-documentation-strategy-h (&rest _)
  (-let* ((my-val (default-value 'eldoc-documentation-strategy)))
    (unless (equal my-val eldoc-documentation-strategy)
      (dlet ((inhibit-message t))
        (my-message🌈
         (buffer-name)
         'eldoc-documentation-strategy "=" eldoc-documentation-strategy
         "≠" my-val)))))

;;;###autoload
(defun my-lisp-set-comment-start-h (&rest _)
  (setq-local comment-start
              (or comment-start ";")))

;;;###autoload
(defun my-elisp-flymake-add-load-path-a (func &rest args)
  (dlet ((elisp-flymake-byte-compile-load-path
          `(,@elisp-flymake-byte-compile-load-path ,@load-path)))
    (apply func args)))

(defvar my-read-shell-command-minibuffer-hook '())
(defvar-local my-read-shell-command-minibuffer-mode nil)

;;;###autoload
(defun my-read-shell-command-run-hook-after-a (&rest _)
  (when (minibufferp)
    (setq-local my-read-shell-command-minibuffer-mode t)
    (run-hooks 'my-read-shell-command-minibuffer-hook)))

;;;###autoload
(defun my-minibuffer-use-local-completion-maps-h ()
  "Use local completion maps.
Such as `minibuffer-local-filename-completion-map'. See
https://github.com/minad/vertico/issues/214."
  (when minibuffer-completing-file-name
    (use-local-map
     (make-composed-keymap
      (list minibuffer-local-filename-completion-map (current-local-map))))))

;;;###autoload
(defun my-minibuffer-setup-category-maps-h ()
  (if (or (bound-and-true-p vertico-mode))
      (add-hook
       'minibuffer-setup-hook #'my-minibuffer-use-local-completion-maps-h)
    (remove-hook
     'minibuffer-setup-hook #'my-minibuffer-use-local-completion-maps-h)))

;;;###autoload
(defun my-set-auto-mode-from-fundamental-maybe-h ()
  "Similar to `doom-guess-mode-h'.
See https://github.com/doomemacs/doomemacs/issues/7141. The
advice on `set-auto-mode' (`+evil--persist-state-a') is too
invasive as it is applied to all calls of such an important
function. Also that advice isn't setup when configuring evil
manually."
  (-when-let* ((_ (member major-mode '(fundamental-mode)))
               (buf (or (buffer-base-buffer) (current-buffer)))
               (_
                (and (buffer-file-name buf)
                     (eq buf (window-buffer (selected-window))))))
    (my-evil-save-state
     ;; preverse `text-scale-mode-amount'
     (my-with-save-text-scale (set-auto-mode)))))

(defvar-local my-breadcrumb--header-line--a--last-error nil)

;;;###autoload
(defun my-breadcrumb--header-line--a (func &rest args)
  (condition-case err
      (apply func args)
    ((args-out-of-range)
     (-let* ((state (list :line (line-number-at-pos) :error err)))
       (when (not my-breadcrumb--header-line--a--last-error)
         (dlet ((inhibit-message t))
           (setq my-breadcrumb--header-line--a--last-error state)
           (message "%S"
                    (vector
                     #'breadcrumb--header-line (my-ISO-time) (buffer-name) ;
                     state)))))
     nil)))

(defvar my-treesit-major-mode-setup-hook '())
;;;###autoload
(defun my-treesit-major-mode-setup-run-hook--after-a (&rest _)
  "Run `my-treesit-major-mode-setup-hook'."
  (run-hooks 'my-treesit-major-mode-setup-hook))

(defvar my-treesit-opt-in-sexp-modes '()
  "Modes where Emacs's built-in sexp navigation is buggy, so use treesit's.")

;;;###autoload
(defun my-treesit-opt-out-h ()
  (cl-loop
   for (sym expected) in
   `((outline-search-function treesit-outline-search)
     (outline-level treesit-outline-level)
     ,@(and (not (apply #'derived-mode-p my-treesit-opt-in-sexp-modes))
            '((forward-sexp-function treesit-forward-sexp)
              (transpose-sexps-function treesit-transpose-sexps))))
   do
   (when (and (local-variable-p sym) (equal (symbol-value sym) expected))
     (kill-local-variable sym))))

;;;###autoload
(defun my-complete-capfs->super-capf (capfs)
  (-let* ((capf-sym (intern (format "my-super-capf-%s" capfs))))
    (or (and (fboundp capf-sym) capf-sym)
        (and (fboundp #'cape-capf-super)
             (-let* ((capfs
                      (-filter
                       (lambda (obj)
                         (or (keywordp obj) (not (symbolp obj)) (fboundp obj)))
                       capfs))
                     (capf-sym (intern (format "my-super-capf-%s" capfs)))
                     (super-capf
                      (-let* ((make-capf
                               (lambda () (apply #'cape-capf-super capfs))))
                        (my-cape-lazy-make-capf
                          capf-sym `(funcall ,make-capf)
                          '()
                          ;; :sort t

                          ;; (format "%s %s."
                          ;;         (my-ISO-time)
                          ;;         (-->
                          ;;          capfs
                          ;;          (-map
                          ;;           (lambda (func) (format "`%s'" func)) it)
                          ;;          (string-join it " ")))
                          ))))
               super-capf)))))

(defvar-local my-completion-wrap-first-capf->-super-capf-h--ran nil)
;;;###autoload
(defun my-completion-wrap-first-capf->-super-capf-h (&rest _)
  (when (and (not my-completion-wrap-first-capf->-super-capf-h--ran)
             (local-variable-p 'completion-at-point-functions))
    (setq my-completion-wrap-first-capf->-super-capf-h--ran t)
    (-let* ((1st-is-t-flag (equal t (car completion-at-point-functions)))
            (1st-capf
             (cond
              (1st-is-t-flag
               (car (default-value 'completion-at-point-functions)))
              (:else
               (car completion-at-point-functions))))
            (super-capf
             (my-complete-capfs->super-capf
              `(,1st-capf ,@my-complete-super-capf-merging))))
      (when (and 1st-capf super-capf)
        (cond
         (1st-is-t-flag
          (push super-capf completion-at-point-functions))
         (:else
          (setf (nth 0 completion-at-point-functions) super-capf)))))))

(defvar my-before-complete-at-point-hook '())
;;;###autoload
(defun my-run-before-complete-at-point-hook-before-a (&rest _)
  (run-hooks 'my-before-complete-at-point-hook))

;;;###autoload
(defun my-setup-hook-run-before-complete-at-point ()
  (advice-add
   #'completion-at-point
   :before #'my-run-before-complete-at-point-hook-before-a)
  (advice-add
   #'corfu--auto-complete-deferred
   :before #'my-run-before-complete-at-point-hook-before-a))

;;;###autoload
(defun my-minibuffer-completion-style-disable-flex-for-file-h ()
  (when minibuffer-completing-file-name
    (my-completion-style-disable-flex-local)))

;;;###autoload
(defun my-elisp-completion-buffer-symbols-h (&rest _)
  (-let* ((_capf
           (my-cape-lazy-make-capf
             'my-elisp-capf
             '(cape-capf-super
               #'elisp-completion-at-point
               #'my-dabbrev-capf-current-buffer-only))))
    ;; '(:display-sort-function my-elisp-capf--sort)

    (setq completion-at-point-functions
          (-replace
           'elisp-completion-at-point
           'my-elisp-capf
           completion-at-point-functions))))

(defvar my-electric-pair-post-self-insert-inhibit-functions '())

;;;###autoload
(defun my-electric-pair-post-self-insert-function--inhibit-a (func &rest args)
  (when (not
         (-some #'funcall my-electric-pair-post-self-insert-inhibit-functions))
    (apply func args)))

;;;###autoload
(defun my-find-file-visit-no-truename-h (&rest _)
  (setq-local find-file-visit-truename nil))

;;; Function (commands, advices) makers

;;;###autoload
(defun my-make-with-dlet-variables-advice (binders)
  (-let* ((sym
           (my-concat-symbols '@ 'my-with-dlet-variables-advice binders '-a)))
    (unless (fboundp sym)
      (-let* ((defnt
               `(lambda (func &rest args)
                  (dlet ,binders
                    (apply func args)))))
        (defalias sym defnt (format "An :around advice: %s." defnt))))
    sym))

;;;###autoload
(defun my-make-function-recenter-to (fraction)
  (let ((f (my-concat-symbols '@ 'my-make-function-recenter-to
                              (my->valid-symbol-name (format "%s" fraction)))))
    (defalias f
      (lambda (&rest _)
        (recenter-top-bottom (round (* fraction (window-height))))))
    f))

;;;###autoload
(defun my-make-mode-disabler-command (mode)
  (let ((func (intern (format "%s-%s" 'my-turn-off||disable mode))))
    (unless (fboundp func)
      (defalias func
        (lambda (&rest _)
          (interactive)
          (when (and (boundp mode) mode
                     (fboundp mode))
            (funcall mode 0)))))
    func))

;;;###autoload
(defun my-make-command-with-mode-off (mode command0)
  (let ((new-cmd (my-concat-symbols '@ #'my-make-command-with-mode-off mode command0)))
    (defalias new-cmd
      `(lambda (&rest args)
         ;; ,(ad-arglist command0)
         ,(interactive-form command0)
         (my-with-mode/s ',mode nil
           (apply #',command0 args)))
      (format "Call %s with %s temporarily turned off." command0 mode))
    new-cmd))

;;;###autoload
(defun my-make-mode-enabler-when-global (global-mode local-mode)
  "Make a variadic function that enable LOCAL-MODE when GLOBAL-MODE."
  (let ((hook (intern
               (format "%s--%s--%s"
                       'my-make-mode-enabler-when-global global-mode local-mode))))
    (defalias hook
      (lambda (&rest _)
        (when (my-bounded-value global-mode)
          (funcall local-mode))))
    hook))

;;;###autoload
(defun my-alt-cmd-when-error (cmd0 cmd1)
  "Return a command which interactively calls CMD0.
But falls to CMD1 when a user-error is thrown."
  (let ((cmd (intern (format "my-%s-or-%s" cmd0 cmd1))))
    (defalias cmd
      (lambda ()
        (interactive)
        (condition-case _
            (call-interactively cmd0)
          ('user-error
           (call-interactively cmd1))))
      (format "`%s' or `%s'" cmd0 cmd1))
    cmd))

;;;###autoload
(defun my-make-press-key-command-with-save-selected-window (key)
  (let ((cmd (my-concat-symbols '@
                                'my- (my->valid-symbol-name (format "%s" key))
                                'save-selected-window)))
    (defalias cmd
      (lambda ()
        (interactive)
        (save-selected-window
          (call-interactively
           (key-binding (my-ensure-kbd key))))))
    cmd))

;;;###autoload
(defun my-make-command-without-minibuffer (cmd)
  (-let* ((produced-cmd (intern (format "%s@%s" #'my-command-without-minibuffer cmd))))
    (my-defalias-maybe
      produced-cmd
      (lambda ()
        (interactive)
        (with-selected-window (minibuffer-selected-window)
          (call-interactively cmd))
        (ignore-errors (exit-recursive-edit))))))

;;; Interaction enhancers

;;;###autoload
(defun my-completing-read-symbol
    (&optional
     prompt
     collection
     pred-on-sym
     require-match
     initial-input
     hist
     default
     inherit-input-method)
  (let* ((prompt (or prompt "Symbol: "))
         (coll
          (or collection
              (my-with-deferred-gc
               (cl-loop
                for sym being the symbols for sn = (symbol-name sym) when
                (and (length> sn 0)
                     (or (not pred-on-sym) (funcall pred-on-sym sym)))
                collect sn))))
         (pred-on-str
          (lambda (cand)
            (if pred-on-sym
                (funcall pred-on-sym (intern-soft cand))
              t)))
         (default (or default (thing-at-point 'symbol))))
    (-->
     (completing-read prompt
                      (lambda (string pred action)
                        (if (eq action 'metadata)
                            `(metadata (category . symbol))
                          (complete-with-action action coll string pred)))
                      pred-on-str
                      require-match
                      initial-input
                      hist
                      default
                      inherit-input-method)
     intern)))

;;;###autoload
(defun my-get-symbol-advices (symbol)
  (let (alist)
    (advice-mapc (lambda (adv prop)
                   (push (list (format "%s" adv)
                               adv prop)
                         alist))
                 symbol)
    alist))

;;;###autoload
(defun my-completing-read-advice (symbol)
  (let* ((alist (my-get-symbol-advices symbol))
         (affix-fn
          (lambda (cands)
            (-->
             (my-for
               [cand cands]
               (list cand nil (format "%s" (nth 2 (assoc cand alist)))))
             (my-minibuffer-annotation-align-affixation it)))))
    (-->
     (completing-read
      (format "Remove advice from `%s':" symbol)
      (lambda (string pred action)
        (if (eq action 'metadata)
            `(metadata (affixation-function . ,affix-fn))
          (complete-with-action action alist string pred))))
     (nth 1 (assoc it alist)))))

;;;###autoload
(defun my-enable-line-wrapping (&rest _)
  (interactive)
  (when truncate-lines
    (dlet ((inhibit-message (or inhibit-message
                                (not (called-interactively-p t)))))
      (toggle-truncate-lines))))

;;;###autoload
(defun my-disable-line-wrapping (&rest _)
  (interactive)
  (when (not truncate-lines)
    (dlet ((inhibit-message (or inhibit-message
                                (not (called-interactively-p t)))))
      (toggle-truncate-lines))))

(defun my-auto-sentence-end-double-space-after-period--false-positive ()
  (save-excursion
    (save-match-data
      (when-let* ((regexp
                   (rx-to-string
                    `(seq
                      bos upper-case
                      ;; lower case consonants
                      (regexp "[b-df-hj-np-tv-z]*") ". " eos)))
                  (end (point))
                  (_
                   (progn
                     (backward-char 2) ; ". "
                     (skip-chars-backward "[:lower:]") ; some lower case chars
                     (ignore-error (beginning-of-buffer)
                       (backward-char 1)) ; a single upper case char
                     t))
                  ;; before is alphanumeric: true positive
                  (_
                   (let* ((cb (char-before)))
                     (or (not cb)
                         (not
                          (string-match "[[:alnum:]]" (char-to-string cb))))))
                  (beg (point))
                  (substr (buffer-substring-no-properties beg end)))
        (string-match regexp substr)))))

(defun my-auto-sentence-end-double-space-after-period--post-self-insert-h ()
  (when (and (equal last-command-event ?\s)
             (equal (char-before (point)) ?\s)
             (not (equal (char-after (point)) ?\s))
             (member (char-before (- (point) 1)) '(?. ?? ?!))
             ;; But treat "Mr. ", "Ms. ", "First_name M. Last_name", etc. as not
             ;; ends of sentences.
             (not
              (my-auto-sentence-end-double-space-after-period--false-positive)))
    (insert " ")))

;;;###autoload
(define-minor-mode my-auto-sentence-end-double-space-after-period-mode
  "Automatically insert another space after \". \".
Related: `sentence-end-double-space'."
  :global
  t
  (cond
   (my-auto-sentence-end-double-space-after-period-mode
    (add-hook
     'post-self-insert-hook
     #'my-auto-sentence-end-double-space-after-period--post-self-insert-h))
   (:else
    (remove-hook
     'post-self-insert-hook
     #'my-auto-sentence-end-double-space-after-period--post-self-insert-h))))


(provide '32-my-config-functions)

;; -*- lexical-binding: t; -*-

(require 'subr-x)

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-no-deactivate-mark-keep-region-after-a (&rest _)
  (setq-local deactivate-mark nil))

;;;###autoload
(defun my-mark-visual-text-paragraph-by-simple-search ()
  (interactive)
  (save-excursion
    (search-forward-regexp "\n\n\\|\n?\\'")
    (push-mark (+ 1 (match-beginning 0))))
  (search-backward-regexp "\n\n\\|\\`\n?")
  (goto-char (match-end 0))
  (activate-mark))

;;;###autoload
(defun my-mark-paragraph-default (&optional arg allow-extend)
  "Mark visual text paragraph using the default separators.
Namely `paragraph-start', `paragraph-separate'. ARG, ALLOW-EXTEND
are passed to `mark-paragraph'."
  (interactive "p\np")
  (dlet ((paragraph-start (default-value 'paragraph-start))
         (paragraph-separate (default-value 'paragraph-separate)))
    (mark-paragraph arg allow-extend)))

;;;###autoload
(defun my-mark-to-end-of-line ()
  (interactive)
  (my-set-mark-from-to
   (point)
   ;; `pos-eol' works in consult async prompts but `line-end-position' may not
   (pos-eol)))

;;;###autoload
(defun my-set-mark-and-forward-char (n)
  (interactive "p")
  (my-with-set-mark-select-region-unless-already (forward-char n)))
;;;###autoload
(defun my-set-mark-and-backward-char (n)
  (interactive "p")
  (my-with-set-mark-select-region-unless-already (backward-char n)))

;;;###autoload
(defun my-mark-inner-paragraph ()
  "Mark current paragraph without the first empty line."
  (interactive)
  (if (use-region-p)
      (user-error "Active region!")
    (progn
      (mark-paragraph)
      (skip-chars-forward "\n"))))

;;;###autoload
(defun my-mark-bigger-defuns-or-paragraphs ()
  "Mark consecutively defun(s) and/or paragraph(s).
Until the region contains only proper defuns and paragraphs."
  (interactive)
  (-let* (([final-beg final-end]
           (save-excursion
             (named-let
                 recur ((beg (point)) (end (point)))
               (-let* ((new-beg
                        (progn
                          (goto-char beg)
                          (-->
                           (list
                            (car
                             (bounds-of-thing-at-point
                              'my-visual-text-paragraph))
                            (car (bounds-of-thing-at-point 'defun)) (point))
                           (delete nil it) (-min it))))
                       (new-end
                        (progn
                          (goto-char end)
                          (-->
                           (list
                            (cdr
                             (bounds-of-thing-at-point
                              'my-visual-text-paragraph))
                            (cdr (bounds-of-thing-at-point 'defun)) (point))
                           (delete nil it) (-max it)))))
                 (if (or (< new-beg beg) (< end new-end))
                     (recur new-beg new-end)
                   (vector new-beg new-end)))))))
    (when (and final-beg final-end)
      (my-set-mark-from-to final-end final-beg))))

;;;###autoload
(defun my-adjust-selected-region (beg-delta &optional end-delta)
  (interactive "p")
  (-let* ((end-delta (or end-delta beg-delta)))
    (when (or (/= 0 beg-delta) (/= 0 end-delta))
      (-let* ((pt0 (point))
              (mk0 (mark))
              (pt-before-mk-flag (< pt0 mk0))
              (new-beg (- (region-beginning) beg-delta))
              (new-end (+ (region-end) end-delta))
              (new-pt
               (if pt-before-mk-flag
                   new-beg
                 new-end))
              (new-mk
               (if pt-before-mk-flag
                   new-end
                 new-beg)))
        (push-mark pt0)
        (push-mark new-mk)
        (goto-char new-pt)
        (activate-mark)))))

;;;###autoload
(defun my-expreg--puni ()
  (cl-loop
   for func in
   '( 
     puni-bounds-of-list-around-point 
     puni-bounds-of-sexp-around-point 
     puni-bounds-of-sexp-at-point) 
   for bounds = (funcall func) when bounds collect (cons func bounds)))

;;;###autoload
(defun my-expreg--lists ()
  (declare)
  ;; `syntax-ppss' (used by `puni-bounds-of-list-at-point') isn't
  ;; applicable in those modes
  (when (not (my-derived-mode-p '(special-mode comint-mode)))
    (my-with-advices
      (puni--in-comment-p puni--in-string-p)
      :override #'ignore
      ;; `python-nav-forward-sexp' can be very slow
      (dlet ((forward-sexp-function nil))
        (-let*
            ((vect
              (while-no-input
                (vector
                 (named-let
                     recur ((retval '()))
                   (-let*
                       ((boundss
                         ;; repeated calls of `syntax-ppss' may take a long time
                         (list
                          (puni-bounds-of-list-around-point)
                          (puni-bounds-of-sexp-around-point)))
                        (inner (puni-bounds-of-list-around-point))
                        (outer (puni-bounds-of-sexp-around-point)))
                     (cond
                      ((or inner outer)
                       (-let* ((retval-new
                                `(,@(and inner
                                         (list (cons 'my-expreg--lists inner)))
                                  ,@(and outer
                                         (list (cons 'my-expreg--lists outer)))
                                  ,@retval)))
                         (condition-case _
                             (progn
                               (backward-up-list)
                               (recur retval-new))
                           (scan-error retval))))
                      (:else
                       retval))))))))
          (cond
           ((vectorp vect)
            (elt vect 0))
           (nil)))))))

;;;###autoload
(defun my-expreg--identifier* ()
  (-some-->
      (my-bounds-of-identifier*) (list (cons 'my-expreg--identifier* it))))

;;;###autoload
(defun my-expreg--compute-regions (&optional old-regions)
  "Taken from `expreg-expand''s source."
  (let* ((orig (point))
         (regions
          (append
           (or old-regions '())
           (mapcan
            (lambda (fn) (save-excursion (funcall fn))) expreg-functions)))
         (regions (expreg--filter-regions regions orig))
         (regions (expreg--sort-regions regions))
         (regions (cl-remove-duplicates regions :test #'equal)))
    (setq-local expreg--next-regions regions)))

;;;###autoload
(defun my-expand-region (arg)
  "Wrapper around `expreg-expand' or `er/expand-region'.
See `er/try-expand-list.' When ARG is in {-1, 1} explicitly,
adjust the region by those instead."
  (interactive "P")
  ;; disable message log in the minibuffer to allow expanding selection without
  ;; jumping the cursor to the end of the buffer
  (dlet ((message-log-max
          (if (equal (messages-buffer) (current-buffer))
              nil
            message-log-max)))
    (-let* ((num-arg (prefix-numeric-value arg)))
      (cond
       ((member arg '(1 -1))
        (my-adjust-selected-region arg))
       ((not (fboundp #'expreg-expand))
        (er/expand-region num-arg))
       ((< num-arg 1)
        (cond
         ((fboundp #'expreg-contract)
          (expreg-contract))
         ((fboundp #'er/contract-region)
          (er/contract-region 1))))
       (t
        (-let* ((region0-flag (use-region-p))
                (beg0 (and region0-flag (region-beginning)))
                (end0 (and region0-flag (region-end))))
          ;; `expreg-expand' doesnt' re-compute
          ;; (my-expreg--compute-regions (bound-and-true-p expreg--next-regions))
          (expreg-expand)
          ;; fallback when `expreg' doesn't expand
          (when (and region0-flag
                     (fboundp #'er/expand-region)
                     (or (and (= beg0 (region-beginning)) (= end0 (region-end)))
                         (<= (region-end) beg0)
                         (<= end0 (region-beginning))))
            (dlet ((expand-region-fast-keys-enabled nil))
              (er/expand-region 1)))))))))


;;; my-functions-mark-region.el ends here

(provide 'my-functions-mark-region)

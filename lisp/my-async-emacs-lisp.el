;; -*- lexical-binding: t; -*-

(require 'dash)

(defvar my-async-elisp--socket-name-buffer-alist
  '()
  "'((socket-name . buffer))")

(defun my-async-elisp--client-command (socket-name form)
  (format "%s --socket-name='%s' --eval %S"
          (file-name-concat invocation-directory
                            "emacsclient")
          socket-name
          (format "%S" form)))

(defun my-async-elisp--daemon-command (socket-name)
  (format "%s --bg-daemon='%s' --eval %S"
          (file-name-concat invocation-directory
                            invocation-name)
          socket-name
          (format "%S" `(progn ,@my-async-elisp-daemon-initial-expressions))))

(defvar my-async-elisp-daemon-initial-expressions
  '(
    ;; disable theming to distinguise when we need to open a frame to inspect
    (advice-add 'load-theme :override 'ignore)
    ;; disable Doom's incremental loading that is forced to run in daemon
    (advice-add 'doom-load-packages-incrementally-h :override 'ignore)
    (advice-add 'doom-load-packages-incrementally :override 'ignore))
  "Used to disable certain unwanted features for `my-async-elisp'.")

(defun my-async-elisp--start (socket-name form callback buffer)
  (-let* ((cmd (my-async-elisp--client-command socket-name form))
          (process (start-process-shell-command (format "%S" form) buffer cmd)))
    (when callback
      (my-set-process-callback process callback))
    process))

;;;###autoload
(cl-defun my-async-elisp (socket-name form &key
                                      callback buffer
                                      notify
                                      (auto-socket t))
  "Asynchronously evaluate FORM in an Emacs daemon named SOCKET-NAME.

With non-nil AUTO-SOCKET, auto-start the daemon and assign its
name as SOCKET-NAME. CALLBACK (see `my-set-process-callback') is
called when that Emacs instance finishes evaluating FORM. CLI
output is recorded in BUFFER. Non-nil NOTIFY notifies whether the
daemon exists.

When the daemon is unresponsive, you may resort to
`my-async-elisp-kill-emacs-daemon'."
  (if (and auto-socket
           (--> (cdr (assoc socket-name my-async-elisp--socket-name-buffer-alist))
                (not it)))
      (-let* ((daemon-buffer (format "*%s %s*" #'my-async-elisp socket-name))
              (daemon-command (my-async-elisp--daemon-command socket-name))
              (daemon-process (start-process-shell-command socket-name daemon-buffer
                                                           daemon-command)))
        (when notify (message "Starting %s" daemon-command))
        (my-set-process-callback
         daemon-process
         (lambda (&rest _)
           (my-async-elisp--start socket-name form callback buffer)))
        (push (cons socket-name daemon-buffer)
              my-async-elisp--socket-name-buffer-alist)
        daemon-process)
    (progn
      (my-async-elisp--start socket-name form callback buffer))))


;;;###autoload
(cl-defun my-async-elisp-auto-output-buffer (socket-name form &rest args &key callback)
  "Mostly like (`my-async-elisp' SOCKET-NAME FORM @ARGS).
But CALLBACK accepts an additionally auto-created output buffer."
  (-let* ((buf (generate-new-buffer-name (format "*%s %s %s*"
                                                 #'my-async-elisp-auto-output-buffer
                                                 socket-name
                                                 form))))
    (apply #'my-async-elisp
           socket-name form
           :callback (lambda (process event)
                       (funcall callback process event buf))
           :buffer buf
           args)))

;;;###autoload
(cl-defun my-async-elisp-eval (socket-name form &optional finish-func &rest args)
  "(`my-async-elisp' SOCKET-NAME FORM @ARGS).
FINISH-FUNC, when non-nil, accepts 1 argument: the evaluated
output."
  (apply #'my-async-elisp-auto-output-buffer
         socket-name form
         :callback (lambda (_process _event buf)
                     (when finish-func
                       (-let* ((result (--> (with-current-buffer buf
                                              (buffer-substring (point-min)
                                                                (point-max)))
                                            (string-trim-right it "\n"))))
                         (funcall finish-func result)))
                     (kill-buffer buf))
         args))

;;;###autoload
(defun my-async-elisp-kill-emacs-daemon (socket-name &optional notify)
  "Kill an Emacs daemon whose name is SOCKET-NAME.
NOTIFY when after killing."
  (interactive (list (completing-read "Socket name: "
                                      (-map #'car
                                            my-async-elisp--socket-name-buffer-alist))
                     t))
  (set-process-sentinel
   (start-process-shell-command
    "" nil
    (my-async-elisp--client-command socket-name '(kill-emacs)))
   (and notify
        (lambda (process event)
          (message "Socket `%s,' process `%s' is killed with event `%s'."
                   socket-name process
                   (string-trim event)))))
  (setq my-async-elisp--socket-name-buffer-alist
        (map-delete my-async-elisp--socket-name-buffer-alist
                    socket-name)))

(provide 'my-async-emacs-lisp)


;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar-local my-ui-make-command-buffer--local-keymap nil)

(define-derived-mode
  my-ui-command-buffer-mode nil "Commands"
  ;;
  (setq buffer-read-only t))

(define-key my-ui-command-buffer-mode-map (kbd "0") #'digit-argument)
(define-key my-ui-command-buffer-mode-map (kbd "1") #'digit-argument)
(define-key my-ui-command-buffer-mode-map (kbd "2") #'digit-argument)
(define-key my-ui-command-buffer-mode-map (kbd "3") #'digit-argument)
(define-key my-ui-command-buffer-mode-map (kbd "4") #'digit-argument)
(define-key my-ui-command-buffer-mode-map (kbd "5") #'digit-argument)
(define-key my-ui-command-buffer-mode-map (kbd "6") #'digit-argument)
(define-key my-ui-command-buffer-mode-map (kbd "7") #'digit-argument)
(define-key my-ui-command-buffer-mode-map (kbd "8") #'digit-argument)
(define-key my-ui-command-buffer-mode-map (kbd "9") #'digit-argument)

;;;###autoload
(cl-defun my-ui-make-command-buffer (name bindings &key no* pre-str suf-str setup-fn)
  (-let* ((buf
           (get-buffer-create
            (if no*
                (format "%s" name)
              (format "*%s*" name))))
          (bindings*
           (-->
            bindings
            ;; press "q" to quit unless specified otherwise
            (if (member "q" it)
                it
              `(,@bindings "q" quit-window))))
          (kmap (apply #'define-keymap bindings*)))
    (with-current-buffer buf
      (setq-local my-ui-make-command-buffer--local-keymap kmap)
      (dlet ((inhibit-read-only t))
        (erase-buffer)

        (insert (or pre-str "") "\n")
        (describe-map-tree kmap)
        (goto-char (point-max))
        (insert "\n" (or suf-str ""))

        (my-ui-command-buffer-mode)
        (use-local-map (make-composed-keymap (list kmap) (current-local-map)))

        (my-evil-no-state)

        (when setup-fn
          (funcall setup-fn))))
    buf))

;;; my-functions-ui.el ends here

(provide 'my-functions-ui)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'evil)

;; * The initial states

;; (my-add-list!
;;   'evil-buffer-regexps
;;   `()) 
;; ;; ("^\\*scratch\\*$" . insert)
;; ;; (,(format "\\`%s\\'" my-bash-cli-editor-file-regexp) . insert)
;; ;; (,(rx-to-string `(seq bos "tmp." (= 10 alnum) ".fish" eos) t) . insert)
;; ;; (,(rx-to-string `(seq bos "zsh" (= 6 alnum) ".zsh" eos) t) . insert)



;; (my-evil-set-initial-state 'Buffer-menu-mode 'emacs 'buff-menu) ; simpler key for `Buffer-menu-other-window' (why "g O" instead of "o"?)
;; (my-evil-set-initial-state 'Info-mode 'emacs 'info)
;; (my-evil-set-initial-state 'dired-mode 'emacs 'dired)
;; (my-evil-set-initial-state 'eww-mode 'emacs 'eww)
;; (my-evil-set-initial-state 'grep-mode 'emacs)
;; (my-evil-set-initial-state 'treesit--explorer-tree-mode 'emacs) ; clicking buttons

;; (add-to-list
;;  'evil-buffer-regexps
;;  `(,(rx-to-string
;;      `(seq
;;        bos "*"
;;        (or "terminal" "ansi-term" "vterm" "eat" "term ") (*? nonl) "*")
;;      t)
;;    . emacs))

;;


(defvar my-evil-collection-mode-exclude-list
  '(
    ;; "M-h"
    outline
    ;; "SPC"
    view))

(defun my-evil-collection-mode-list ()
  (seq-difference evil-collection-mode-list
                  my-evil-collection-mode-exclude-list
                  (lambda (exclude-elem elem)
                    (cond
                     ((listp elem)
                      (equal (car elem) exclude-elem))
                     (:else
                      (equal elem exclude-elem))))))

;; (when (and (null my-emacs-kit) (fboundp #'evil-collection-init))
;;   (require 'evil-collection)
;;   (evil-collection-init (my-evil-collection-mode-list)))


(setq evil-default-state 'insert)

;; override evil's defaults, since `evil-default-state' is set above
(dolist (sym (-map #'cdr (evil-state-property t :modes)))
  (set sym nil))

(evil-set-initial-state 'fundamental-mode 'normal)

(dolist (mode my-edit-modes)
  (evil-set-initial-state mode 'normal))

;; Sometimes Emacs editor is invoked from the CLI to edit current
;; command line arguments, in that case starting from insert state is
;; better
(add-hook
 'server-visit-hook #'my-evil-insert-when-server-frame-open-file-h)

;; * Other configs

;; native operation
(my-bind :vi '(motion normal)  "C-S-e" #'my-mark-to-end-of-line)

;; far more convenient in special buffers
(my-bind :vi '(motion normal)  "n" `(menu-item "" evil-search-next :filter my-evil-filter-native-command-when-read-only))
(my-bind :vi '(motion normal)  "p" `(menu-item "" evil-paste-after :filter my-evil-filter-native-command-when-read-only))
;; use "q" for quitting special buffers
(my-bind :vi '(motion normal)  "q" `(menu-item "" evil-record-macro :filter my-evil-filter-native-command-when-read-only))

;; for visual line navigation
(my-bind :vi '(motion normal) '("<left>" "<down>" "<up>" "<right>") nil)

;; allowing clicking on widgets
(my-bind :vi '(motion normal insert visual) '([down-mouse-1] [mouse-2]) nil)


;; "ESC TAB" as completion, evil doesn't bind "TAB" explicitly anyway
(my-bind :vi '(normal) :map my-edit-mode-keymaps
  '("TAB" "<tab>") #'my-insert-&-ESC-TAB)
(with-eval-after-load 'evil-snipe
  (setq evil-snipe-scope 'whole-buffer))

(my-bind :vi 'motion "C-6" nil) ; `evil-switch-to-windows-last-buffer'???

;; (my-add-advice/s '(evil-paste-pop evil-paste-pop-next evil-repeat-pop evil-repeat-pop-next) :around 'my-native-command-when-fail--a)

(advice-add #'evil-adjust-cursor :around #'my-evil-adjust-cursor-maybe-a)
(advice-add #'evil-visual-line :after #'my-evil--visual-line-prefix-argument-after-a)

;; ;; https://github.com/emacs-evil/evil/issues/1623
;; (advice-remove #'set-window-buffer #'ad-Advice-set-window-buffer)

(defun my-evil-local-mode-h ()
  (declare)
  ;; After CUA and my one
  (add-to-ordered-list 'emulation-mode-map-alists 'evil-mode-map-alist
                       512))
(add-hook 'evil-local-mode-hook #'my-evil-local-mode-h)


(advice-add
 #'evil-force-normal-state
 :after #'my-evil-run-force-normal-state-hook-after-a)

(my-bind :map 'jinx-mode-map
  [remap evil-prev-flyspell-error] #'jinx-previous
  [remap evil-next-flyspell-error] #'jinx-next)

;; ;; "C-o" is occupied
;; (my-bind :vi '(motion) '("g DEL") #'evil-jump-backward)

;; allow going back with "M-,"
(my-bind [remap evil-goto-definition] #'xref-find-definitions)

;; I don't use any of these vi bindings anyway, Evil's "C-o": "M-," is
;; easier to press.
(my-bind
  :vi
  '(motion normal visual)
  '("C-." "C-b" "C-f" "C-o" "M-," "M-.")
  nil)

(evil-define-text-object
  my-evil-text-object-whole-buffer
  (count &optional _beg _end type)
  (evil-range (point-min) (point-max) type))

(evil-define-text-object
  my-evil-text-object-defun (count &optional _beg _end type)
  (-let* (((beg . end) (bounds-of-thing-at-point 'defun)))
    (evil-range beg end type)))

(my-bind :map '(evil-inner-text-objects-map evil-outer-text-objects-map) "g" #'my-evil-text-object-whole-buffer)
(my-bind :map '(evil-inner-text-objects-map evil-outer-text-objects-map) "f" #'my-evil-text-object-defun)

(setq-default evil-emacs-state-cursor (default-value 'cursor-type))

(when (fboundp #'global-evil-surround-mode)
  (global-evil-surround-mode))

(provide 'my-after-evil)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(set-face-attribute 'org-ref-cite-face nil :weight 'unspecified)

;;; my-after-org-ref.el ends here

(provide 'my-after-org-ref)

;; Local Variables:
;; no-byte-compile: t
;; End:

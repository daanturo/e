;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(defvar my-manifest-file-rust "Cargo.toml")

;;;###autoload
(defun my-rust-cargo-toml-treesit-parse-a-dependencies-table (table-texts)
  (-let* (((table-name . texts) table-texts)
          ;; [*.dependencies.<package>]
          (single-flag (s-match "\\<dependencies\\>\\.\\([^.]+\\)" table-name)))
    (cond
     (single-flag
      (list
       (format "%s = { %s }" (nth 1 single-flag) (string-join texts ", "))))
     (t
      texts))))

;;;###autoload
(defun my-rust-cargo-dependency-list (&optional manifest)
  (-let* ((manifest
           (or manifest
               (-some-->
                   (locate-dominating-file default-directory my-manifest-file-rust)
                 (file-name-concat it my-manifest-file-rust)))))
    (when manifest
      (-let* ((table-texts-lst
               (my-lang-toml-get-tables-from-string
                (f-read-text manifest) "\\<dependencies\\>")))
        (apply #'append
               (-map
                #'my-rust-cargo-toml-treesit-parse-a-dependencies-table
                table-texts-lst))))))

;;;###autoload
(defun my-execu-cmd-lang-rust (file-name)
  (interactive "f")
  (if (locate-dominating-file default-directory my-manifest-file-rust)
      (progn
        "cargo run")
    (progn
      (format "rustc %S && ./%S" file-name (file-name-base file-name)))))

;;;###autoload
(defun my-evalu-repl-add-dependencies-rust (&optional interact)
  (interactive (list t))
  (-let* ((all-deps (my-rust-cargo-dependency-list))
          (str
           (-->
            (completing-read-multiple
             "Import: "
             (lambda (string pred action)
               (if (eq action 'metadata)
                   `(metadata (display-sort-function . identity))
                 (complete-with-action action all-deps string pred))))
            ;; https://github.com/evcxr/evcxr/blob/main/COMMON.md
            (cl-loop for pkg in it collect (format ":dep %s" pkg))
            (string-join it "\n"))))
    (when interact
      (kill-new str))
    (my-evalu-string str)
    str))


;;; my-functions-lang-rust.el ends here

(provide 'my-functions-lang-rust)

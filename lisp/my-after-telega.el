;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-telega-tdlib-version nil)
(defvar my-telega-tdlib-min-version-original nil)

;; Force `my-telega-force-tdlib-min-version' to be the installed version
(advice-add #'telega--on-updateOption :around #'my-telega--on-updateOption--force-tdlib-min-version-a)

(-let* ((suffix "include/td/telegram/"))
  (cond
   ((file-exists-p (file-name-concat "/nix/var/nix/profiles/default/" suffix))
    (setq telega-server-libs-prefix "/nix/var/nix/profiles/default"))
   ((file-exists-p (file-name-concat "/usr" suffix))
    (setq telega-server-libs-prefix "/usr"))))

;; (setq telega-use-docker "podman")
;; (setq telega-use-docker nil)

(setq telega-open-file-function #'my-xdg-open-file-and-parent-directory)

(set-popup-rule! "\\`\\*Telega.*\\*" :ignore t )

;;;###autoload
(my-accumulate-config-collect
 (setq telega-directory
       (or (getenv "EMACS_TELEGA_DIRECTORY")
           (expand-file-name "~/.local/share/telega"))))

(with-eval-after-load 'telega-ffplay
  (when (and (executable-find "mpv") (executable-find "xdg-open"))
    (setq telega-video-player-command
          '(if telega-ffplay-media-timestamp
               (concat
                "mpv" (format " --start=%f" telega-ffplay-media-timestamp))
             "xdg-open"))))

(with-eval-after-load 'telega-msg
  (add-hook
   'vertico-multiform-commands
   '(telega-msg-add-reaction grid (vertico-grid-annotate . 1))))
(advice-add
 #'telega-msg-add-reaction
 :around #'my-telega-msg-add-reaction-annotate-a)

;; its interactive form make GC happens a lot
(advice-add #'telega-chat-with :around #'my-with-deferred-gc-around-a)

(add-hook 'my-evil-or-leader-mode-alist '(telega-chat-mode my-leader-mode))

(with-eval-after-load 'telega-chat
  (add-to-list
   'telega-chat--message-filters
   '("by-sender-id" (return t) my-telega-chatbuf-filter-by-sender-id)))

(add-hook
 'my-clean-emacs-functions
 (lambda ()
   (delete-directory (file-name-concat telega-directory "cache") 'recursive)
   (delete-directory (file-name-concat telega-directory "profile_photos")
                     'recursive)))

(add-hook 'my-disable-kill-emacs-hook-exclude-regexps "^telega-")
(my-disable-kill-emacs-hook-when-daemon-and-not-default)
(my-add-hook/s
  '(telega-ready-hook) '(my-disable-kill-emacs-hook-when-daemon-and-not-default))

(provide 'my-after-telega)

;; Local Variables:
;; no-byte-compile: t
;; End:

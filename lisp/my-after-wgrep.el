;; -*- lexical-binding: t; -*-

(my-advice-function-to-run-hook
 #'wgrep-to-original-mode 'my-wgrep-to-original-mode-hook)

(my-advice-function-to-run-hook
 #'wgrep-change-to-wgrep-mode 'my-wgrep-change-to-wgrep-mode-hook)

(setq wgrep-auto-save-buffer t)

;; Don't close the those buffers after saving/discarding, like Doom does
(advice-remove #'wgrep-abort-changes #'+popup-close-a)
(advice-remove #'wgrep-finish-edit #'+popup-close-a)

;; like dired's `wdired-abort-changes'
(my-bind :map 'wgrep-mode-map "C-c ESC" #'wgrep-abort-changes)

(my-add-hook/s
  '(my-wgrep-change-to-wgrep-mode-hook
    my-wgrep-to-original-mode-hook)
  #'(my-leader-or-vi-refresh-prefer-insert))

(provide 'my-after-wgrep)

;; Local Variables:
;; no-byte-compile: t
;; End:

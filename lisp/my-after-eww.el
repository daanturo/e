;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(set-popup-rule! "^\\*eww\\*" :ignore t)
(set-popup-rule! " # eww\\*" :ignore t)

(setq eww-auto-rename-buffer 'url)

;;; after-eww.el ends here

(provide 'my-after-eww)

;; Local Variables:
;; no-byte-compile: t
;; End:

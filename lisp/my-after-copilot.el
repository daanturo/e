;; -*- lexical-binding: t; -*-

;; (my-maydefun my-copilot-clear-overlay-h ()
;;   (when (copilot-current-completion)
;;     (copilot-clear-overlay)
;;     t))
;; (add-hook 'doom-escape-hook 'my-copilot-clear-overlay-h)

(defun my-copilot-filter (cmd)
  (and (copilot-current-completion)
       cmd))

;; (my-safe-call #'copilot-current-completion) #'copilot-accept-completion

(my-bind :map 'copilot-completion-map

  "C-e" `(menu-item "" copilot-accept-completion-by-line :filter my-copilot-filter)
  "C-g" `(menu-item "" copilot-clear-overlay :filter my-copilot-filter)
  "M-f" `(menu-item "" copilot-accept-completion-by-word :filter my-copilot-filter)
  '("TAB" "<tab>") `(menu-item "" copilot-accept-completion :filter my-copilot-filter)

  ;;
  )

(my-when-graphical
 (my-bind :map 'copilot-completion-map
   "M-[" `(menu-item "" copilot-previous-completion :filter my-copilot-filter)
   "M-]" `(menu-item "" copilot-next-completion :filter my-copilot-filter)
   ;;
   ))

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'my-after-copilot)

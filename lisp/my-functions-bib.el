;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'oc)

;;;###autoload
(defun my-bib-add-to-global-bibliography-file (bibtex-@str)
  (interactive (list
                (dlet ((vertico-multiline `("\n" . "…")))
                  (completing-read
                   "`my-bib-add-to-global-bibliography-file': "
                   (save-match-data
                     (-filter
                      (lambda (str) (string-match "^@" str)) kill-ring))))))
  (-let* ((file (car org-cite-global-bibliography)))
    (my-append-text-file file (my-ensure-string-prefix "\n" bibtex-@str))
    (my-apheleia-format-file file)
    (message "`my-bib-add-to-global-bibliography-file' added to %s: %S"
             file
             bibtex-@str)))

;;; my-functions-bib.el ends here

(provide 'my-functions-bib)

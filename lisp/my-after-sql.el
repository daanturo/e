;; -*- lexical-binding: t; -*-

(with-eval-after-load 'lsp-sqls
  (add-to-list
   'lsp-sqls-connections
   `((driver . "postgresql")
     (dataSourceName . "host=127.0.0.1 port=5432 user=postgres password=' ' dbname=postgres sslmode=disable"))))

(provide 'my-after-sql)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

;; ;;;###autoload
;; (progn
;;   (defun my-setup-symex (&rest _)
;;     (with-eval-after-load 'lispy
;;       (my-define-ordered-key-chord "df" #'symex-mode-interface lispy-mode-map))))

(symex-initialize)
;; (my-profiler-cpu-progn (symex-initialize))

;; Don't use `paredit'
(setcdr symex-mode-map nil)

(provide 'my-after-symex)

;; Local Variables:
;; no-byte-compile: t
;; End:

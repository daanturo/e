;; -*- lexical-binding: t; -*-

(require 'dash)

(require 'compile)

;;;###autoload
(defun my-execu-dwim ()
  "See also `my-evalu-dwim'."
  (interactive)
  (cond
   ((my-get-alist-major-mode my-execu-direct-alist)
    (funcall (my-get-alist-major-mode my-execu-direct-alist)))
   ((my-get-alist-major-mode my-execu-cmd-alist)
    (my-execu-do-build-run-maybe))
   (t
    (my-evalu-file-or-buffer))))

(defvar my-execu-direct-alist '()
  "Alist of major modes and functions.
The matching function is called directly with `my-execu-dwim'.")

(defvar my-execu-compiler-alist '()
  "May not be needed when there is only one/hard-coded compiler.")

(defvar my-execu-cmd-alist '()
  "Alist of major modes and execute command producers.
Each producer:
 (<relative file name>: string) => <command to run>: string.")

(defvar my-execu-confirm-key nil)

(defun my-execu-confirm-compile ()
  (interactive)
  (with-current-buffer (window-buffer (minibuffer-selected-window))
    (setq-local compilation-read-command nil))
  (exit-minibuffer))

;;;###autoload
(defun my-execu-do-build-run ()
  "Only support a single file.
For more complex compilations, please use a proper build system."
  (interactive)
  (-when-let* ((cmds-maker (my-get-alist-major-mode my-execu-cmd-alist))
               (build-run-command
                (-->
                 (funcall cmds-maker
                          (file-relative-name buffer-file-name))
                 (my-ensure-string-suffix " " it))))
    (unless (local-variable-p 'compile-command)
      (setq-local compile-command build-run-command)
      (setq-local compilation-read-command t))
    (-let* ((compilation-read-command0 compilation-read-command))
      (minibuffer-with-setup-hook (lambda ()
                                    (when (and my-execu-confirm-key
                                               compilation-read-command0)
                                      (keymap-local-set
                                       my-execu-confirm-key
                                       #'my-execu-confirm-compile)
                                      (message
                                       "'%s': skip this prompt for future runs."
                                       my-execu-confirm-key)))
        (call-interactively #'compile)))))

;;;###autoload
(defun my-execu-do-build-run-maybe ()
  "Like `my-execu-do-build-run', but first display all running compilations."
  (interactive)
  (-let* ((bufs (my-project-compile-buffer-list t)))
    (if (-every #'get-buffer-window bufs)
        (progn
          (my-execu-do-build-run))
      (progn
        (dolist (buf bufs)
          (save-selected-window
            (pop-to-buffer buf)))))))

(provide 'my-execu)

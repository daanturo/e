;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'helpful)

;;;###autoload
(defun my-helpful-go-back ()
  (interactive)
  (switch-to-buffer helpful--start-buffer))

;;;###autoload
(progn
  (defun my-setup-helpful ()

    ;; let us use built-in helps when pressing the "C-h" prefix, else
    ;; `helpful'
    (cl-loop
     for (key built-in-cmd helpful-cmd) in
     '(("f" describe-function helpful-callable)
       ("k" describe-key helpful-key)
       ("v" describe-variable helpful-variable)
       ("x" describe-command helpful-command)
       ;;
       )
     do
     (progn
       (my-bind `[remap ,built-in-cmd] nil)
       (my-bind
         (format "<help> %s" key)
         (my-kmi-menu-item-by-pressed-keys
          `(("^C-h " . ,built-in-cmd) ("" . ,helpful-cmd))))))

    ;;`helpful-symbol' isn't suitable for previewing: it asks when a symbol is both a function & a variable
    (my-bind [remap describe-symbol] nil)

    ;;
    ))

(my-bind :map 'helpful-mode-map :vi 'normal "<" #'my-helpful-go-back)

(my-bind :map 'helpful-mode-map "l" #'my-helpful-go-back)

;; ;;;###autoload
;; (defun my-helpful-push-button-try-same-window (&optional pos use-mouse-action)
;;   (interactive
;;    (list (if (integerp last-command-event) (point) last-command-event)))
;;   (if (my-safe-call #'+popup-window-p)
;;       (push-button pos use-mouse-action)
;;     (my-with-display-buffer-same-window
;;      (push-button pos use-mouse-action))))

(my-bind :map 'helpful-mode-map
  [remap push-button]
  (cmds! (not (my-safe-call #'+popup-window-p)) #'my-push-button-same-window))

(add-hook 'helpful-mode-hook 'my-focus-center-mode)

(provide 'my-after-helpful)

;; Local Variables:
;; no-byte-compile: t
;; End:

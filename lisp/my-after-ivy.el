;; -*- lexical-binding: t; -*-

;; NOTE: Because `swiper' is required by `lispy', this file may be loaded unexpectedly

(require 'ivy)

(require '00-my-core-macros)
(require 'dash)

;; * Defs

;; (my-backtrace)

;; ;; Make `counsel-unicode-char' more useable by removing padded spaces and preferring shorter matches
;; ;;;###autoload
;; (define-advice counsel--unicode-names (:filter-return (cands) change-format)
;;   (seq-map (-partial #'replace-regexp-in-string " [[:space:]]+" " ") cands))
;; (ivy-configure #'counsel-unicode-char
;;   :sort-matches-fn 'ivy--shorter-matches-first
;;   :initial-input "greek \\s-")

;;;###autoload
(defun my-swiper-C-w ()
  (interactive)
  (cond
   ((use-region-p)
    (call-interactively #'kill-region))
   (t
    (let ((inp (ivy--input))
          (cmd (ivy-state-caller ivy-last)))
      (ivy-quit-and-run
        (funcall
         (cond ((equal cmd 'swiper) #'swiper-isearch)
               ((equal cmd 'swiper-isearch) #'swiper))
         inp))))))

;;;###autoload
(defun my-swiper-isearch-thing-at-point ()
  (interactive)
  (my-with-advice
    #'ivy--insert-symbol-boundaries
    :override #'ignore (swiper-isearch-thing-at-point)))

;;;###autoload
(defun my-swiper-C-f (&optional arg)
  (interactive "p")
  (if (string= ivy-text "")
      ;; "Wrong type argument: integer-or-marker-p, nil"?
      (ignore-errors
        (my-swiper-isearch-thing-at-point))
    (ivy-next-line arg)))

;;;###autoload
(defun my-swiper-next-history-element (arg)
  "Like `swiper-isearch-thing-at-point', but without symbol boundaries."
  (interactive "P")
  (cond (arg
         (swiper-isearch-thing-at-point))
        ((string-empty-p (ivy--input))
         (insert (with-ivy-window
                   (format "%s" (symbol-at-point)))))
        (t
         (ivy-next-history-element 1))))

;;;###autoload
(defun my-ivy-toggle-regexp-quote-and-notify ()
  (interactive)
  (ivy-toggle-regexp-quote)
  (message "`ivy--regex-function': %s" ivy--regex-function))

;;;###autoload
(defun my-ivy-toggle-case-fold-and-notify ()
  (interactive)
  (ivy-toggle-case-fold)
  ;; tell `swiper-isearch' to update its candidates
  (setq ivy--old-text "")
  (message "`ivy-case-fold-search': %s" ivy-case-fold-search))

;; * Config

;;;###autoload
(progn
  (defun my-setup-counsel ()

    (my-bind "C-c j" #'counsel-git-grep)

    (my-bind "C-c o" #'counsel-outline)

    (setq ivy-count-format "%d/%d ")

    ;; it often appears first when I type "cus apr" for `customize-apropos'
    (put 'counsel-apropos 'completion-predicate #'ignore)))

(add-to-list 'ivy-highlight-functions-alist
             '(orderless-ivy-re-builder . orderless-ivy-highlight))
(setq
 ivy-re-builders-alist
 (list
  ;; Let the `isearch' variant's results ordered, otherwise they will be
  ;; chaotic; some how `ivy--split' does this: \"\\(\\).\" becomes (\"\\(\\)\"
  ;; \".\") (via `ivy--split') then \"\\(\\(\\)\\).*?\\(.\\)\" (via
  ;; `ivy--regex'). We can use a custom regexp builder on top of
  ;; `ivy--regex-plus' by advices instead, but `ivy' still caches the computed
  ;; regexps in `ivy--regex-hash'.
  '(swiper-isearch . my-ivy-regex-plus)
  ;; until `orderless' introduces "without regexp" like `ivy' already has
  '(t . ivy--regex-ignore-order)))
;;


(ivy-configure #'counsel-rg :initial-input #'my-rgrep-ripgrep|rg-initial-input)

(dolist (command
         '(

           counsel-M-x
           counsel-apropos
           counsel-describe-face
           counsel-describe-function
           counsel-describe-symbol
           counsel-describe-variable
           execute-extended-command
           ivy-yasnippet))

  ;;
  
  (my-with-demoted-errors
   (ivy-configure command
     ;; :sort-fn
     :sort-matches-fn
     'ivy--shorter-matches-first)))

(add-hook 'counsel-grep-post-action-hook #'recenter)

(my-bind :map 'ivy-minibuffer-map "M-O" #'ivy-dispatching-done) ; when "M-o" is bound
(my-bind :map 'ivy-minibuffer-map :if '(bound-and-true-p cua-mode) "C-v" nil)
(my-bind :map 'swiper-isearch-map "M-n" #'my-swiper-next-history-element)
(my-bind :map 'swiper-map "<C-m>" #'swiper-mc)
(my-bind :map 'swiper-map "C-f" #'my-swiper-C-f)
(my-bind :map 'swiper-map "C-w" #'my-swiper-C-w)

;; Don't move the cursor when using those keys (currently doesn't work)
;; (my-bind :map 'swiper-map
;;   "M-<up>" #'ivy-previous-line
;;   "M-<down>" #'ivy-next-line)

;; `ivy-read' isn't compatible
(when (my-bounded-value 'icomplete-mode)
  (advice-add #'ivy-read :around
              (lambda (func &rest args)
                (my-with-mode/s 'icomplete-mode nil
                  (apply func args)))))

;;; my-after-ivy.el ends here

(provide 'my-after-ivy)

;; Local Variables:
;; no-byte-compile: t
;; End:

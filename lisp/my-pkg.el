;; -*- lexical-binding: t; -*-

(require 'dash)
(require 'compat)

(require '00-my-core-macros)
(require '01-my-core-functions)

(require 'package)

;;; package.el

(defvar my-package-el-ensure-no-refresh nil
  "Whether to refresh in `my-package-el-ensure'.
Because of network errors, etc.")

(defun my-package-el-disable-refresh (&optional err)
  (interactive)
  (let* ((early-init (concat my-emacs-conf-dir/ "early-init.el")))
    (my-prn🌈
     err "Disabling `my-package-el-ensure' for subsequent starts in"
     early-init)
    (my-append-text-file
     early-init
     (format "\n%s\n" `(setq my-package-el-ensure-no-refresh t)))
    err))

;;;###autoload
(defun my-package-el--ignore-signal-a (func &rest args)
  (my-prn🌈 func args)
  nil)
;;;###autoload
(defun my-package-el-refresh-maybe ()
  (unless my-package-el-ensure-no-refresh
    (condition-case err
        ;; `file-error' signals can't be caught by `condition-case'?
        (my-with-demoted-package-error-signals
         (package-refresh-contents))
      ('error (my-package-el-disable-refresh err)))
    (unless package-archive-contents
      (my-package-el-disable-refresh))
    (setq my-package-el-ensure-no-refresh t)))

;;;###autoload
(cl-defun my-package-el-ensure (&optional pkg url &rest args &key unstable &allow-other-keys)
  (unless (bound-and-true-p package--initialized)
    (package-initialize))
  (when (and pkg
             (or (not (package-installed-p pkg))
                 (not (member pkg package-activated-list))))
    (my-prn🌈 'my-package-el-ensure pkg url args)
    (unless (or (assoc pkg package-archive-contents) url)
      (my-package-el-refresh-maybe))
    (condition-case err
        (let* ((pkg-contents
                (-->
                 (cdr (assoc pkg package-archive-contents))
                 (cl-sort it #'version-list-< :key #'package-desc-version)))
               ;; with a simple `(package-install pkg)', `package.el' will not
               ;; install a package which is already built-in, but we want the
               ;; latest anyway; still, some old versions of `package-install'
               ;; only accept symbols
               (pkg-desc
                (and pkg-contents
                     (cond
                      ((not (listp pkg-contents))
                       pkg-contents)
                      (unstable
                       (-last-item pkg-contents))
                      (:else
                       (-first-item pkg-contents))))))
          (if pkg-desc
              (package-install pkg-desc)
            (my-prn🌈 "Package not found:" pkg pkg-desc)))
      ((error)
       (my-prn🌈 #'my-package-el-ensure pkg (error-message-string err))))))

;;;###autoload
(defun my-package-get-all-descriptions (pkg-name)
  (alist-get pkg-name (package--alist)))

;;;###autoload
(defun my-package-get-description (pkg-name kind)
  (-some-->
      pkg-name
    (my-package-get-all-descriptions pkg-name)
    (-find (lambda (desc) (equal kind (package-desc-kind desc))) it)))

;;;###autoload
(defun my-package-ensure-description (pkg-name-or-desc kind)
  (cond
   ((package-desc-p pkg-name-or-desc)
    pkg-name-or-desc)
   (:else
    (my-package-get-description pkg-name-or-desc kind))))

;;;###autoload
(defun my-package-get-source-directory (pkg-name &optional kind)
  "Return PKG-NAME's directory, KIND: 'vc is preferred when empty."
  (cond
   (kind
    (-when-let* ((desc (my-package-get-description pkg-name kind)))
      (package-desc-dir desc)))
   ((-when-let* ((desc (my-package-get-description pkg-name 'vc)))
      (package-desc-dir desc)))
   (:else
    (-when-let* ((descs (my-package-get-all-descriptions pkg-name)))
      (package-desc-dir (nth 0 descs))))))

;;;###autoload
(defun my-package-el-uninstall (pkg &optional kinds force nosave)
  (-let* ((descs (my-package-get-all-descriptions pkg))
          (deleting-descs
           (cond
            ((null kinds)
             descs)
            (:else
             (-filter
              (lambda (desc)
                (member (package-desc-kind desc)))
              descs)))))
    (mapc
     (lambda (desc) (package-delete desc force nosave))
     deleting-descs)
    deleting-descs))

;;; package-vc.el

(defun my-demote-package-error-signals-a (func &rest args)
  (-let* (((err-sym (signal-a0 signal-a1)) args))
    (if (or
         ;;
         (equal args '(wrong-type-argument (package-desc nil))) ; `package-vc-install' on generic repos

         ;; `package-refresh-contents' on some distros
         (equal (list err-sym signal-a1) '(file-error "Bad Request"))
         (equal args '(bad-signature ("archive-contents.sig")))
         (equal args '(error ("Selecting deleted buffer"))))

        ;;

        (progn (message "%S" (list 'my-demote-package-error-signals-a
                                   func args))
               nil)
      (apply func args))))

(defmacro my-with-demoted-package-error-signals (&rest body)
  (declare (debug t))
  `(my-with-advice #'signal :around #'my-demote-package-error-signals-a
     ,@body))

;;;###autoload
(defun my-package-vc-installed-p (pkg)
  (declare)
  (require 'package)
  (-some-->
      (my-package-get-all-descriptions pkg)
    (-filter (lambda (desc) (package-vc-p desc)) it)
    (nth 0 it)
    (package-desc-dir it)))


;;;###autoload
(defun my-package-vc-get-url-from-archives (pkg)
  (require 'package-vc)
  (package-vc--archives-initialize)
  (-some-->
      (alist-get pkg package-archive-contents)
    car
    (package-desc-extras it)
    (alist-get :url it)))

(defun my-package-vc-without-prompt-a (func &rest args)
  (my-with-advice #'yes-or-no-p :around
    (lambda (func1 prompt &rest args1)
      (cond
       ((dlet ((case-fold-search t))
          (string-match-p "overwrite previous checkout" prompt))
        (my-prn🌈 #'my-package-vc-without-prompt-a "ignored: " `(,func ,@args))
        nil)
       (t
        (apply func1 prompt args1))))
    (my-with-advice #'error :override #'ignore
      (apply func args))))

;;;###autoload
(defun my-package-force-symlink-top-level
    (subdirs &optional no-override)
  (dolist (subdir subdirs)
    (let* ((cwd (expand-file-name default-directory))
           (lisp-files
            (directory-files (file-name-concat cwd subdir)
                             nil
                             "\\.\\(el\\|elc\\)$")))
      (dolist (file lisp-files)
        (let* ((target (file-name-concat cwd subdir file))
               (link-name (file-name-concat cwd file)))
          (when (and (not no-override)
                     (file-exists-p link-name)
                     (not (file-symlink-p link-name)))
            (delete-file link-name))
          (make-symbolic-link target link-name t))))))

;;;###autoload
(cl-defun my-package-vc-ensure (pkg
                                &rest
                                args
                                &key
                                url
                                branch
                                lisp-dir
                                main-file
                                doc
                                vc-backend
                                rev
                                post
                                depth
                                force
                                &allow-other-keys)
  "Install PKG using `package-vc-install'.
POST: optional function (0 arguments) to run at the package directory."
  (when (or force (not (my-package-vc-installed-p pkg)))
    (-let*
        ((spec
          `(,@(and url (list :url url))
            ,@(and branch (list :branch branch))
            ,@(and doc (list :doc doc))
            ,@(and lisp-dir (list :lisp-dir lisp-dir))
            ,@(and main-file (list :main-file main-file))))
         (package*
          (cond
           ((< 0 (length spec))
            (cons pkg spec))
           ;; `package-vc-install' won't automatically retrieve URL when
           ;; passing PKG as a cons cell
           (:else
            pkg)))
         (dir (file-name-concat package-user-dir (format "%s" pkg)))
         (depth
          (cond
           ;; an explicit number
           ((numberp depth)
            depth)
           ;; not a number, or other things than doesn't work with
           ;; shallow clones
           ((or depth branch rev)
            nil)
           (:else
            1))))
      (condition-case err
          (progn
            ;; my-with-advice #'package-vc-install :around #'my-package-vc-without-prompt-a
            (prog1 (my-vc-with-clone-depth
                     depth nil
                     (package-vc-install package* rev vc-backend))
              (when post
                (dlet ((default-directory dir))
                  (funcall post)))))
        ;; ((error)
        ;;  (my-prn🌈
        ;;   #'my-package-vc-ensure
        ;;   pkg
        ;;   args
        ;;   (error-message-string err)))
        ))))


;; ;; Install Org-mode from source example:
;; (my-package-vc-ensure
;;  'org
;;  :rev
;;  :last-release
;;  :lisp-dir "lisp"
;;  :post
;;  (lambda () (my-package-force-symlink-top-level '("lisp"))))

;;;###autoload
(defun my-package-vc-checkout-revision-and-rebuild (pkg revision)
  (interactive
   (-let* ((pkg-desc
            (package-vc--read-package-desc
             "`my-package-vc-checkout-revision-and-rebuild': " t))
           (dir (package-desc-dir pkg-desc)))
     (list
      pkg-desc
      (dlet ((default-directory dir))
        (magit-read-other-branch-or-commit "Checkout")))))
  ;; TODO: automatically do this when updating (upgrading) a just
  ;; unpinned package, or syncing after a just pinned package
  (-let* ((pkg-desc (my-package-ensure-description pkg 'vc))
          (dir (package-desc-dir pkg-desc)))
    (dlet ((default-directory dir))
      (magit-checkout revision)
      (package-vc-rebuild pkg-desc))))

;;;###autoload
(defun my-package-vc-checkout-default-branch-unpinned (pkg)
  "At PKG's source, check out the default branch when it's not pinned."
  (-let* ((my-plist (cdr (assoc pkg my-pkg!--list))))
    (-when-let* ((desc (my-package-get-description pkg 'vc))
                 (dir (package-desc-dir desc))
                 (_
                  (and (not (plist-get my-plist :rev))
                       (package-installed-p desc)
                       (not
                        (my-vc-git-dirty-unstaged-or-uncommitted-p
                         dir)))))
      (dlet ((default-directory dir))
        (-let* ((default-branch (my-vc-git-get-default-branch))
                (curr-branch (my-vc-git-get-current-branch)))
          (when (not (equal curr-branch default-branch))
            (message
             "`my-package-vc-checkout-default-branch-unpinned' %s : %s -> %s"
             dir
             (my-vc-git-get-current-revision-commit-hash)
             default-branch)
            (vector pkg (vc-switch-branch dir default-branch))))))))

;;;###autoload
(defun my-package-vc-checkout-all-default-branch-if-unpinned ()
  (interactive)
  (prog1 (-keep
          #'my-package-vc-checkout-default-branch-unpinned
          (-map #'car my-pkg!--list))
    (my-package-vc-auto-rebuild-all)))

;; TODO: update packages installed with `:last-release'?

;;;###autoload
(defun my-package-vc-checkout-pinned-revision (pkg)
  (-let* ((my-plist (cdr (assoc pkg my-pkg!--list))))
    (-when-let* ((desc (my-package-get-description pkg 'vc))
                 (dir (package-desc-dir desc))
                 (pin-rev (plist-get my-plist :rev))
                 (_
                  (and (stringp pin-rev)
                       (package-installed-p desc)
                       (not
                        (my-vc-git-dirty-unstaged-or-uncommitted-p
                         dir)))))
      (dlet ((default-directory dir))
        (-let* ((_curr-branch (my-vc-git-get-current-branch))
                (curr-rev
                 (my-vc-git-get-current-revision-commit-hash)))
          (when (not (equal curr-rev pin-rev))
            (message
             "`my-package-vc-checkout-pinned-revision' %s : %s -> %s"
             dir curr-rev pin-rev)
            (vector pkg (vc-switch-branch dir pin-rev))))))))

;;;###autoload
(defun my-package-vc-checkout-all-pinned-revision ()
  (interactive)
  (prog1 (-keep
          #'my-package-vc-checkout-pinned-revision
          package-activated-list)
    (my-package-vc-auto-rebuild-all)))


;;;###autoload
(defun my-package-get-built-commit (pkg kind)
  (-when-let* ((desc (my-package-ensure-description pkg kind)))
    (--> desc (package-desc-extras it) (alist-get :commit it))))

;;;###autoload
(defun my-package-vc-auto-rebuild (pkg &optional dry-run)
  (-when-let* ((desc (my-package-ensure-description pkg 'vc))
               (dir (package-desc-dir desc))
               (_ (equal 'Git (vc-responsible-backend dir)))
               (built-rev (my-package-get-built-commit desc 'vc))
               (curr-rev
                (my-vc-git-get-current-revision-commit-hash dir)))
    (dlet ((default-directory dir))
      (when (not (equal built-rev curr-rev))
        (-let* ((rebuild
                 (and (not dry-run) (package-vc-rebuild desc))))
          (when rebuild
            (package-load-descriptor dir))
          (vector rebuild built-rev curr-rev))))))

;;;###autoload
(defun my-package-vc-auto-rebuild-all (&optional dry-run)
  (interactive)
  (-keep
   (lambda (pkg)
     (-some-->
         (my-package-vc-auto-rebuild pkg dry-run) (cons pkg it)))
   (-uniq package-activated-list)))

;;; my-pkg

(defun my-pkg-built-in-p (pkg)
  (
   ;; benchmark-progn
   progn
   (require 'finder-inf)
   (assoc pkg package--builtins)))

(cl-defun my-pkg--use-vc-p (&rest args &key url vc branch depth rev lisp-dir &allow-other-keys)
  (or url vc branch depth rev lisp-dir))

;;;###autoload
(cl-defun my-pkg-ensure (pkg
                         &rest
                         args
                         &key
                         vc
                         url
                         local-repo
                         branch
                         rev
                         depth
                         now
                         prefer-built-in
                         globs
                         disable
                         ignore
                         lisp-dir
                         unstable
                         &allow-other-keys)
  "Ensure that PKG is installed.
VC: use `package-vc-install', some packages's tagged versions are too
oudated that fetching from source is better."
  (declare) (my-set-package-user-dir)
  (-let* ((args (my-plist-remove-nil args)))
    (dlet ((package-archive-priorities
            (cond
             (unstable
              '())
             (:else
              package-archive-priorities))))
      (cond
       ((or disable ignore (and prefer-built-in (my-pkg-built-in-p pkg)))
        nil)
       ((and (apply #'my-pkg--use-vc-p args) (fboundp #'package-vc-install))
        (apply #'my-package-vc-ensure pkg args))
       ;; (url
       ;;  (apply #'my-useelisp-repo pkg (or url local-repo) args))
       (t
        (apply #'my-package-el-ensure pkg url args))))))

(defvar my-pkg-queue '())

(cl-defun my-pkg!--1 (pkg
                      &rest
                      args
                      &key
                      url
                      local-repo
                      branch
                      rev
                      depth
                      files
                      build
                      now
                      disable
                      ignore
                      prefer-built-in
                      recipe-doom
                      unstable
                      &allow-other-keys)
  (-let* ((keys (-slice args 0 nil 2)))
    (cond
     ((equal my-emacs-kit 'doom)
      (-let* ((str8-recipe (apply #'my-pkg-args->str8-recipe pkg url args))
              (final-doom-recipe (append recipe-doom (cdr str8-recipe)))
              (form
               `(package!
                  ,pkg
                  ,@(and disable `(:disable ,disable))
                  ,@(and (member :prefer-built-in keys)
                         `(:built-in ,(and prefer-built-in ''prefer)))
                  ,@(and final-doom-recipe `(:recipe ,final-doom-recipe))
                  ,@(and ignore `(:ignore ,ignore))
                  ,@(and rev `(:pin ,rev)))))
        (when (member (bound-and-true-p doom-print-minimum-level) '(info debug))
          (my-prn🌈 'my-pkg ":" (format "%S" form)))
        (when (doom-context-p 'packages)
          (eval form t))))
     (t
      (unless (or disable ignore)
        (-let* ((fn (lambda () (apply #'my-pkg-ensure pkg args)))
                (prepend (or (apply #'my-pkg--use-vc-p args) unstable)))
          (if now
              (funcall fn)
            ;; Add packages with recipes to the end for others' potential
            ;; refreshes first
            (add-to-list 'my-pkg-queue fn (not prepend)))))))))

(defvar my-pkg!--list '())
(defun my-pkg!--get-list (&optional only-names)
  (-->
   (-filter
    (lambda (plist)
      (and (not
            (or (plist-get plist :disable) (plist-get plist :ignore)))
           plist))
    my-pkg!--list)
   (if only-names
       (-map #'car it)
     it)))

;;;###autoload
(cl-defun my-pkg! (pkg
                   &rest
                   plist
                   &key
                   url
                   vc
                   local-repo
                   _branch
                   _rev
                   _lisp-dir
                   (_depth 1)
                   _files
                   _build
                   _now
                   _disable
                   _ignore
                   _prefer-built-in
                   _unstable
                   &allow-other-keys)
  "Call `my-pkg-ensure' later, or NOW." (declare)
  ;; (my-prn🌈 'my-pkg! pkg plist)
  (-let* ((url
           (or url
               ;; the first of plist can be the URL
               (--> (car plist) (and (stringp it) it))))
          (args*
           `(,@(and url (list :url url))
             ,@(-drop-while (-not #'keywordp) plist))))
    (cond
     ;; `:local-repo': reuse a previously declared recipe
     (local-repo
      (apply #'my-pkg!--1
             pkg
             `(,@plist ,@(--> (assoc pkg my-pkg!--list) (cdr it)))))
     (t
      (push (cons pkg args*) my-pkg!--list)
      (apply #'my-pkg!--1 pkg args*)))))

;;;###autoload
(cl-defun my-local-pkg! (pkg &rest args)
  "(`my-pkg!' PKG :now t :depth nil ~ARGS)."
  (apply #'my-pkg! pkg (my-plist-put-ensure args :now t :depth 'full)))

;;;###autoload
(defun my-pkgs! (pkgs &rest my-pkg!-args)
  (dolist (pkg pkgs)
    (let* ((args (ensure-list pkg)))
      (apply #'my-pkg! (append args my-pkg!-args)))))

;;;###autoload
(defun my-pkg!-by-ver (pkg &optional min-emacs-ver-incl max-emacs-ver-excl &rest my-add-pkg-args)
  (when (and (or (null min-emacs-ver-incl) (version<= min-emacs-ver-incl emacs-version))
             (or (null max-emacs-ver-excl) (version< emacs-version max-emacs-ver-excl)))
    (apply #'my-pkg! pkg my-add-pkg-args)))

;;;###autoload
(defun my-pkg-install-added (&optional interact)
  (interactive (list t))
  (let* ((fn
          (lambda ()
            (-let* ((selected
                     (bound-and-true-p
                      package-selected-packages)))
              (mapc #'funcall my-pkg-queue)
              (my-safe-call #'elpaca-process-queues)
              ;; (my-safe-call #'package-vc-install-selected-packages)
              (when (not
                     (equal
                      selected (bound-and-true-p package-selected-packages)))
                (my-safe-call #'package-quickstart-refresh))))))
    (cond
     (interact
      (funcall fn))
     (:else
      (my-with-demoted-errors)
      (funcall fn)))))

;;; Doom


;;;###autoload
(defun my-doom-module-dir (category module)
  (file-name-concat my-doom-emacs-dir
                    "modules"
                    (-->
                     (format "%s" category)
                     (string-remove-prefix ":" it))
                    (format "%s" module)))

(cl-defmacro my-doom-with-context (contexts &rest body)
  (declare (debug t) (indent defun))
  `(condition-case _
       (with-doom-context ,contexts
         ,@body)
     ((error)
      (dlet ((doom-context ,contexts))
        ,@body))))

(defvar doom-packages '())
;;;###autoload
(defun my-pkg-doom-module-package! (module &optional category)
  "Use packages from Doom's CATEGORY/MODULE packages.el file.
Try to fake an suitable environment."
  (dlet ((doom-user-dir my-emacs-conf-dir/))
    (-let* ((category (or category :lang))
            (packages-file
             (file-name-concat (my-doom-module-dir category module)
                               "packages.el")))
      ;; (my-doom-with-context '(package) (load packages-file nil t))
      ;; (-let* ((pkgs (-map #'car doom-packages)))
      ;;   (dolist (pkg pkgs)
      ;;     (when (not (assoc pkg my-pkg!--list))
      ;;       (my-pkg! pkg))))
      (load packages-file nil t))))


;;; my-pkg.el ends here

(provide 'my-pkg)

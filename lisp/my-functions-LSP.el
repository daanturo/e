;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar lsp-mode nil)
(autoload #'eglot-managed-p "eglot")

;; (with-eval-after-load 'eglot
;;   (add-to-list
;;    'eglot-server-programs '((prog-mode text-mode conf-mode) . ("llm-ls"))
;;    'append))

;;;###autoload
(defun my-lsp-mode-use-server-llm-ls ()
  (interactive)
  (with-eval-after-load 'lsp-mode
    (lsp-register-client
     (make-lsp-client
      :server-id 'my-llm-ls
      :new-connection (lsp-stdio-connection '("llm-ls"))
      :activation-fn #'always
      :priority -8
      ;;
      ))))

;;;###autoload
(defun my-lsp-completion--resolve--workaround-a (fn &optional item &rest args)
  (-let* ((time0 (current-time))
          (retval*
           (while-no-input
             (vector (apply fn item args)))))
    (cond
     ((vectorp retval*)
      (elt retval* 0))
     (:else
      (message
       "`my-lsp-completion--resolve--workaround-a': interrupted after %ss."
       (float-time (time-since time0)))
      item))))

;;;###autoload
(defun my-lsp-mode-disconnect-all ()
  (interactive)
  (dolist (buf (buffer-list))
    (with-current-buffer buf
      (when lsp-mode
        (lsp-disconnect)))))

(defvar my-lsp-client-package nil)

(defvar my-lsp-client-package--table
  `(("lsp-mode" .
     ,(lambda (&optional disable)
        (interactive "P")
        (cond
         ((not disable)
          (setq-default my-lsp-turn-on-function #'lsp-deferred))
         (disable
          (my-lsp-mode-disconnect-all)))))
    ("eglot" .
     ,(lambda (&optional disable)
        (interactive "P")
        (cond
         ((not disable)
          (setq-default my-lsp-turn-on-function #'my-eglot-ensure))
         (disable
          (eglot-shutdown-all)))))))

;;;###autoload
(defun my-lsp-switch-client-package (&optional add-dir-local-var)
  (interactive "P")
  (-let* ((to-client
           (completing-read
            "Emacs LSP client: "
            (lambda (string pred action)
              (cond
               ((eq action 'metadata)
                `(metadata
                  (display-sort-function .
                                         ,(lambda (cands)
                                            (remove
                                             nil
                                             `(,@(remove my-lsp-client-package cands)
                                               ,my-lsp-client-package))))))
               (t
                (complete-with-action
                 action
                 (-map #'car my-lsp-client-package--table)
                 string
                 pred)))))))
    (setq my-lsp-client-package to-client)
    (mapc
     (-lambda ((client . func))
       (cond
        ((equal client to-client)
         (funcall func))
        (:else
         (funcall func 'disable))))
     my-lsp-client-package--table)
    (when add-dir-local-var
      (my-add-dir-local-variable-and-save
       nil 'my-lsp-turn-on-function my-lsp-turn-on-function))
    (my-lsp-maybe-global-mode)
    to-client))

(defvar my-lsp-code-action-timeout 1.0)

;;;###autoload
(defun my-eglot-ensure-then-execute-code-action (&optional shutdown-after)
  (interactive "P")
  (-let*
      ((buf (current-buffer))
       (already (eglot-managed-p))
       (action
        (lambda (&rest _)
          (with-current-buffer buf
            (unwind-protect
                (progn

                  ;; Issue: `eglot-code-actions' isn't available immediately
                  ;; after connnecting, it relies on flymake to get the
                  ;; suggestion, which is (I think) asynchronous

                  ;; (my-eglot--keep-trying-code-action 0.125)

                  (if already
                      (progn
                        (unwind-protect
                            (call-interactively #'eglot-code-actions)
                          (when shutdown-after
                            (eglot-shutdown (eglot-current-server)))))
                    (progn
                      ;; `message' will be overridden by eglot's "Connected!", so...
                      (notifications-notify
                       :body "`my-eglot-ensure-then-execute-code-action': press again!")))

                  ;; (run-at-time
                  ;;  my-lsp-code-action-timeout nil
                  ;;  (lambda ()
                  ;;    (with-current-buffer buf
                  ;;      (with-timeout
                  ;;          (my-lsp-code-action-timeout
                  ;;           (message
                  ;;            "`my-eglot-ensure-then-execute-code-action': timeout!"))
                  ;;        (flymake-diagnostics)))))

                  ;; (call-interactively #'eglot-code-actions)
                  )
              (progn
                ;; (when (and shutdown-after (not already))
                ;;   (eglot-shutdown (eglot-current-server)))
                ))))))
    (cond
     (already
      (funcall action))
     (t
      (my-add-hook-once 'eglot-connect-hook action)
      (dlet ((inhibit-message t))
        (my-eglot-ensure))))))

;;;###autoload
(defun my-lsp-execute-code-action ()
  (interactive)
  (cond
   (lsp-mode
    (dlet ((lsp-response-timeout my-lsp-code-action-timeout))
      (condition-case _
          (call-interactively #'lsp-execute-code-action)
        (error
         ;; timeout
         (my-eglot-ensure-then-execute-code-action t)))))
   (t
    (require 'eglot)
    (call-interactively #'eglot-code-actions))))


;;; my-functions-LSP.el ends here

(provide 'my-functions-LSP)

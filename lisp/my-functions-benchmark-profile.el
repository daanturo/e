;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-benchmark-state (&optional since)
  (-let* ((curr (vector (current-time) gc-elapsed gcs-done)))
    (cond
     (since
      (-let* (([time0 gc-e-0 gcs-0] since)
              ([time1 gc-e-1 gcs-1] curr))
        (vector
         (float-time (time-subtract time1 time0))
         (- gc-e-1 gc-e-0)
         (- gcs-1 gcs-0))))
     (:else
      curr))))

;;;###autoload
(defun my-benchmark-report-since (initial &optional msg-prefix quiet)
  "Report like `benchmark-progn' since INITIAL.
Return the report message as string. INITIAL should be from
`my-benchmark-state'. QUIET: don't `message'. MSG-PREFIX: append it
before the message."
  (-let* (([time0 gc-e-0 gcs-0] initial)
          ([time1 gc-e-1 gcs-1] (my-benchmark-state))
          (timeΔ (float-time (time-subtract time1 time0)))
          (gc-e-Δ (- gc-e-1 gc-e-0))
          (gcs-Δ (- gcs-1 gcs-0))
          (msg
           (format "%sElapsed time: %fs (%fs in %d GCs)"
                   (or msg-prefix "")
                   timeΔ
                   gc-e-Δ
                   gcs-Δ)))
    (when (not quiet)
      (message "%s" msg))
    msg))

;;;###autoload
(cl-defun my-profiler-cpu-for (for-seconds &optional interactive-flag &key callback mode)
  (interactive (list (read-number "Run profiler for ? seconds: ") t))
  (profiler-start (or mode 'cpu))
  (run-with-timer
   for-seconds nil
   (lambda ()
     (profiler-stop)
     (when callback
       (funcall callback))
     (when interactive-flag
       (profiler-report)))))

;;;###autoload
(defun my-profiler-cpu+mem-for ()
  (declare (interactive-only t))
  (interactive)
  (my-profiler-cpu-for
   (read-number "Run profiler for ? seconds: ")
   t
   :mode 'cpu+mem))

;;;###autoload
(cl-defun my-profiler-cpu-until-key (&optional (key "<pause>"))
  (interactive)
  (profiler-start 'cpu)
  (-let* ((cmd-orig (lookup-key global-map (my-ensure-kbd key))))
    (my-bind key
      (lambda ()
        (interactive)
        (profiler-stop)
        (profiler-report)
        (my-bind key cmd-orig)))))

;;;###autoload
(cl-defun my-profiler-cpu-until-next-next-command (&optional (max-count 2))
  (interactive)
  (letrec ((count 0)
           (time0 (my-benchmark-state))
           (adv
            (lambda ()
              (cl-incf count 1)
              (when (<= max-count count)
                (profiler-stop)
                (my-benchmark-report-since
                 time0
                 (format "`my-profiler-cpu-until-next-next-command': "))
                (profiler-report)
                (remove-hook 'post-command-hook adv)))))
    (profiler-start 'cpu)
    (add-hook 'post-command-hook adv)))

(defvar my-profiler-in-interval--timer nil)
(defcustom my-profiler-in-interval-repeat-interval 10 nil)
(defun my-profiler-in-interval--new-cycle (&optional terminate)
  (profiler-stop)
  (save-window-excursion
    (profiler-report))
  (unless terminate
    (profiler-start 'cpu)))
;;;###autoload
(define-minor-mode my-profiler-in-interval-mode nil
  :global t
  (if my-profiler-in-interval-mode
      (progn
        (profiler-start 'cpu)
        (setq my-profiler-in-interval--timer
              (run-with-timer nil my-profiler-in-interval-repeat-interval
                              #'my-profiler-in-interval--new-cycle)))
    (progn
      (cancel-timer my-profiler-in-interval--timer)
      (setq my-profiler-in-interval-repeat-interval nil)
      (my-profiler-in-interval--new-cycle t))))

;;;###autoload
(defun my-profiler-expand-report ()
  (interactive)
  ;; my-save-line-and-column
  (my-save-column
    (my-save-line
      (goto-char (point-min))
      ;; expand all non-zero entries
      (while (re-search-forward "\\([1-9]\\|[0-9]0\\)%[[:space:]]+\\+ "
                                nil
                                ':noerror)
        (profiler-report-expand-entry 'full)
        (goto-char (point-min)))
      ;; collapse all zero entries
      (goto-char (point-min))
      (while (re-search-forward "[[:space:]]0%[[:space:]]+\\- " nil ':noerror)
        (profiler-report-collapse-entry)
        (goto-char (point-min)))))
  (ignore-errors
    (my-profiler-normalize-buffer-indentation)))

(defvar-local my-profiler-normalize-buffer-indentation--abnormal-indent nil)

;;;###autoload
(defun my-profiler-normalize-buffer-indentation ()
  (interactive)
  (unless my-profiler-normalize-buffer-indentation--abnormal-indent
    (setq my-profiler-normalize-buffer-indentation--abnormal-indent
          (my-find-minimum-indentation-of-buffer)))
  (let* ((min-indent my-profiler-normalize-buffer-indentation--abnormal-indent)
         (target-normal-indent 16) ; vanilla: 9
         (to-erase (- min-indent 8)))
    (when (< target-normal-indent min-indent)
      (dlet ((inhibit-read-only t))
        (save-excursion
          (goto-char (point-min))
          (while (not (eobp))
            (let* ((cur-line-indent (my-line-indent-offset)))
              (when (< target-normal-indent cur-line-indent)
                (delete-char to-erase)))
            (forward-line 1)))))))

(provide 'my-functions-benchmark-profile)

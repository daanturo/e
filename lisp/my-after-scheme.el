;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'my-hook-scheme-mode-hook nil 'noerror)

(provide 'my-after-scheme)

;; Local Variables:
;; no-byte-compile: t
;; End:

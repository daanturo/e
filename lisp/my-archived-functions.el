;; -*- lexical-binding: t; -*-

;;; Commentary:
;; Functions that are not often used anymore but maybe rarely needed

;;; Code:

(require 'dash)

(my-println🌈 load-file-name "loaded")

;;;###autoload
(defun my-dash-docs-async-install-docset (&rest docset-names)
  "Asynchronously download docset with specified DOCSET-NAMES and activate it.
When called interactively, install a single docset only."
  (interactive (progn
                 (defvar my-dash-docs-official-docsets (dash-docs-official-docsets)
                   "Official Dash docsets.")
                 (completing-read-multiple
                  "Docset to install (leave empty to install a user docset): "
                  my-dash-docs-official-docsets
                  nil nil (my-prog-lang-of-mode major-mode))))
  (let ((init-file user-init-file))
    (make-directory (dash-docs-docsets-path) 'parents)
    (if (member (car docset-names) my-dash-docs-official-docsets)
        (async-start
         (lambda () (load init-file)
           (package-initialize)
           (dolist (docset-name docset-names)
             (dash-docs-install-docset docset-name)))
         (lambda (result)
           (message "%s \n Docset installed: %s" result docset-names)))
      (progn
        (defvar my-dash-docs-unofficial-docsets (dash-docs-unofficial-docsets)
          "Unofficial Dash docsets.")
        (apply
         #'my-dash-docs-async-install-user-docset
         (completing-read-multiple "Docset to install: "
                                   my-dash-docs-unofficial-docsets))))))

;;;###autoload
(defun my-dash-docs-async-install-user-docset (&rest docset-names)
  (let ((init-file user-init-file))
    (make-directory (dash-docs-docsets-path) 'parents)
    (async-start
     (lambda ()
       (load init-file)
       (package-initialize)
       (dolist (docset-name docset-names)
         (dash-docs-install-user-docset docset-name)))
     (lambda (result)
       (message "%s \n Docset installed: %s" result docset-names)))))

;;;###autoload
(defun my-dash-docs-install-multiple-user-docsets (docset-list)
  (interactive
   (progn
     (defvar my-dash-docs-user-docsets (dash-docs-unofficial-docsets)
       "Dash Docs' unofficial docsets.")
     (list (completing-read-multiple "Docset: " my-dash-docs-user-docsets))))
  (make-directory (dash-docs-docsets-path) 'parents)
  (mapc #'dash-docs-install-user-docset docset-list))

;;;###autoload
(cl-defun my-var-set-by-command
    (&optional (prefix 'my-var-set-by-command) (command this-command) init-value)
  "Return a local variable whose name is based on PREFIX, COMMAND.
It's name has PREFIX as prefix, COMMAND as suffix. INIT-VALUE as
initial value."
  (my-set-ensure-defined-variable
   ,(intern (format "%s@%s" prefix command)) init-value
   t))

;;;###autoload
(defun my-recenter&may-mark-line (&optional _)
  "Call `recenter-top-bottom'.
When it's the first call and there is no active region, mark current line.
Successive calls un-mark region marked by this command."
  (interactive "P")
  (let ((marker (my-var-set-by-command)))
    (call-interactively #'recenter-top-bottom)
    (when (called-interactively-p 'any)
      (cond
       ((eq last-command this-command)
        ;; If this command is repeated, un-mark region when it was marked by this command
        (when (symbol-value marker)
          (evil-exit-visual-state)))
       ((use-region-p)
        ;; Region wasn't marked by this command
        (set marker nil))
       ;; Mark the line, set it as marked by this command
       (t
        (evil-visual-line)
        (set marker t))))))

(defvar my-project-child-indicators '(".git"))
;;;###autoload
(defun my-project-list (init-dir)
  (-let* ((project-indicator-regexp (--> (-map 'regexp-quote my-project-child-indicators)
                                         (my-regexp-or it)
                                         (format "\\`%s\\'" it))))
    (named-let recur ((dir-list (list init-dir))
                      (projects '()))
      (-let* (((dir . dirs-tail) dir-list))
        (cond
         ((null dir)
          projects)
         ((not (file-readable-p dir))
          (recur dirs-tail projects))
         ((my-directory-has-matching-files-p dir project-indicator-regexp)
          (recur dirs-tail
                 (cons dir projects)))
         (t
          (-let* ((children-dir (-->
                                 (directory-files
                                  dir 'full
                                  directory-files-no-dot-files-regexp
                                  'nosort)
                                 (-filter (lambda (f)
                                            (file-directory-p (file-truename f)))
                                          it))))
            (recur (append children-dir dirs-tail)
                   projects))))))))

;;;###autoload
(defun my-buffer-read-only|not-writable-p (&rest _)
  buffer-read-only)

;;; Unserious aliases

;;;###autoload
(defun my-string-char-after (&optional pos)
  (char-to-string (char-after pos)))
;;;###autoload
(defun my-string-char-before (&optional pos)
  (char-to-string (char-before pos)))

;;;###autoload
(defun my-f-file-write-utf-8-text (text path)
  (f-write-text text 'utf-8 path))

;;;###autoload
(defun my-f-file-append-utf-8-text (text path)
  (f-append-text text 'utf-8 path))

;;;###autoload
(defun my-find-file-parent (file-name)
  (find-file (file-name-parent-directory file-name)))

;;;###autoload
(defmacro my-toggle! (var)
  `(setq! ,var (not ,var)))

;;;###autoload
(defun my-orderless-literal-or-regexp (component)
  (-let* ((literal (regexp-quote component))
          (regexp (orderless-regexp component)))
    (if regexp
        (concat "\\(:?" literal "\\|" regexp "\\)")
      literal)))


(defun my-filename--concat (components)
  (cond
   ((= 1 (length components))
    (car components))
   (t
    (concat
     (mapconcat (lambda (comp)
                  (file-name-as-directory comp))
                (butlast components)
                "")
     (car (last components))))))
;;;###autoload
(defun my-filename-concat (directory &rest components)
  (my-filename--concat
   (cl-remove-if-not (lambda (comp)
                       (< 0 (length comp)))
                     `(,directory ,@components))))


;;;###autoload
(defun my-vertico-prefixed-sort (candidates &optional prefix)
  "Prioritize CANDIDATES who are prefixed with PREFIX.
PREFIX defaults to the first minibuffer input component.
CANDIDATES are sorted first. Useful for problematic candidates
such as \"`c++-mode'\". Candidates who share the first character
with PREFIX maybe also prioritized, albeit lower.

If this is still undesirable, maybe we can explore https://github.com/jojojames/fussy?"
  (-let* ((sorted (vertico-sort-history-length-alpha candidates))
          (prefix
           (or prefix
               (-->
                (minibuffer-contents-no-properties)
                (replace-regexp-in-string "[ \t\n\r].*" "" it))))
          ;; (1st-char
          ;;  (ignore-error args-out-of-range
          ;;    (elt prefix 0)))
          )
    (-->
     sorted
     (-group-by
      (lambda (cand)
        (cond
         ((string-prefix-p prefix cand completion-ignore-case)
          :string)
         ;; ((= 1st-char (ignore-error args-out-of-range
         ;;                (elt cand 0)))
         ;;  :char)
         ))
      it)
     (append (alist-get :string it) (alist-get :char it) (alist-get nil it)))))



(provide 'my-archived-functions)


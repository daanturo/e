;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-treemacs-kill ()
  (when (fboundp #'treemacs)
    (-some--> (treemacs-get-local-window)
      (with-selected-window it
        (treemacs-kill-buffer)))))

;;;###autoload
(defun my-treemacs-toggle ()
  (pcase (treemacs-current-visibility)
    (`visible (my-treemacs-kill))
    (_ (my-treemacs-show))))

;;;###autoload
(defun my-sidebar-toggle (&optional auto)
  "`my-treemacs-toggle' sidebar visibility.
If nil AUTO (default), turn off `my-auto-sidebar-mode', else
enable it."
  (interactive "P")
  (require 'treemacs)
  (if auto
      (my-auto-sidebar-mode 1)
    (my-auto-sidebar-mode 0))
  (my-treemacs-toggle))


;;; my-functions-sidebar.el ends here

(provide 'my-functions-sidebar)

;; -*- lexical-binding: t; -*-

(require 'cl-macs)
(require 'dash)

(eval-when-compile
  (require 'json))

(defun my-jupyter--read-json (file)
  (cl-letf (((symbol-function 'json-read-array) 'forward-sexp))
    (json-read-file file)))

;;;###autoload
(cl-defun my-jupyter-get-nodebook-language (&optional (file buffer-file-name))
  (let ((data (my-jupyter--read-json file)))
    (or (map-nested-elt data '(metadata kernelspec language))
        (map-nested-elt data '(metadata jupytext main_language))
        (map-nested-elt data '(metadata language_info name)))))

;; (--> (shell-command-to-string "jupyter nbconvert --help | grep -m1 \\'script\\' ") ;
;;      string-trim
;;      (format "Convert notebook to one of %s:\n" it))

;;;###autoload
(cl-defun my-jupyter-get-nodebook-file-extension (&optional (file buffer-file-name))
  (let ((data (my-jupyter--read-json file)))
    (map-nested-elt data '(metadata language_info file_extension))))

;;;###autoload
(defun my-jupyter-nbconvert-to-script-and-open (nbfile)
  (interactive (list (file-relative-name buffer-file-name)))
  (-let* ((base (file-name-base nbfile))
          (new (concat base (my-jupyter-get-nodebook-file-extension nbfile)))
          (cmd
           (read-shell-command "Run: "
                               (format "jupyter-nbconvert \"%S\" --to=script"
                                       nbfile))))
    (my-process-shell-async
      cmd
      (lambda (&rest _)
        (when (file-exists-p new)
          (find-file new))))))

;;;###autoload
(defun my-jupyter-json-context-menu (menu &optional _click)
  (define-key-after menu [my-jupyter-json-context-menu] menu-bar-separator)
  (define-key-after menu [my-jupyter-nbconvert-to-script-and-open]
    `(menu-item "Convert to script" my-jupyter-nbconvert-to-script-and-open))
  (define-key-after menu [my-ein-auto-open-notebook]
    `(menu-item "Auto open notebook" my-ein-auto-open-notebook))
  menu)

(provide 'my-functions-jupyter)

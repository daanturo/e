;; -*- lexical-binding: t; -*-

(require 'dash)

;;;###autoload
(defun my-get-symbol-in-minibuffer-selected-window ()
  (with-selected-window (minibuffer-selected-window)
    (or (thing-at-point 'symbol 'no-properties)
        "")))

(defvar my-minibuffer-next-history-transform-alist
  '(
    (1 . identity)
    ;; word
    (2 . (lambda (str) (concat "\\<" str "\\>")))
    ;; symbol
    (3 . (lambda (str) (concat "\\_<" str "\\_>")))))

;;;###autoload
(defun my-minibuffer-next-history-insert-future (&optional arg)
  (interactive "P")
  ;; (insert (nth 0 (funcall minibuffer-default-add-function)))
  (-let* ((sym (my-get-symbol-in-minibuffer-selected-window))
          (str (if arg
                   (funcall (alist-get arg my-minibuffer-next-history-transform-alist)
                            sym)
                 sym)))
    (insert str)
    ;; this regexp style isn't understood by ripgrep? so we have to delegate it
    ;; to consult's filter
    (when (not (equal str sym))
      (my-consult-async-insert-at-filter-part-maybe str))
    (not (string-empty-p str))))

;;;###autoload
(defun my-minibuffer-next-history-menu-item-filter-element (_)
  (cond ((member prefix-arg (-map #'car my-minibuffer-next-history-transform-alist))
         #'my-minibuffer-next-history-insert-future)))


(provide 'my-minibuffer-next-history)

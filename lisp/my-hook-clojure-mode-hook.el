;; -*- lexical-binding: t; -*-

(require 'clojure-mode)

(require '00-my-core-macros)

(my-autoload-functions 'cider '(cider-current-repl))

;; (my-custom-set-faces `(clojure-keyword-face :slant italic :inherit ,(face-attribute 'clojure-keyword-face :inherit)))

;;;###autoload
(defun my-lang-clojure-get-cider-jack-in-tool ()
  (cond ((my-shebang-p "bb")
         'babashka)
        (t (default-value 'cider-jack-in-default))))

;;;###autoload
(defun my-lang-clojure-set-cider-jack-in-tool (&optional global)
  (-let* ((tool (my-lang-clojure-get-cider-jack-in-tool)))
    (if global
        (setq-default cider-jack-in-default tool)
      (setq-local cider-jack-in-default tool))))

;;;###autoload
(defun my-lang-clojure-cider-mode-hook-h ()
  ""
  ;; Prioritize LSP completion before Cider
  (-let* ((lsp-pos
           (seq-position
            completion-at-point-functions 'lsp-completion-at-point))
          (cider-pos
           (seq-position
            completion-at-point-functions 'cider-complete-at-point)))
    (when (and lsp-pos cider-pos (< cider-pos lsp-pos))
      (setf (nth cider-pos completion-at-point-functions)
            #'lsp-completion-at-point)
      (setf (nth lsp-pos completion-at-point-functions)
            #'cider-complete-at-point))))

;;;###autoload
(my-accumulate-config-collect
 (my-defer-hook 'clojure-mode-hook)
 (my-defer-hook 'clojure-mode-local-vars-hook))

(my-evalu-set-handler
 'clojure-mode
 #'my-lang-clojure-repl
 :region #'cider-eval-region
 :file #'cider-load-file)

(when (fboundp #'cider-mode)
  (my-add-mode-hook-and-now '(clojure-mode clojure-ts-mode) #'cider-mode))

;; maybe it can serve as a fallback in some cases?
(advice-add #'+clojure--cider-disable-completion :override #'ignore)

(my-bind :map '(clojure-mode-map clojure-ts-mode-map) "M-l m" #'cider-macroexpand-1)

(provide 'my-hook-clojure-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

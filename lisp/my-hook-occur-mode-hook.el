;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(my-add-hook/s
  '(occur-edit-mode-hook) #'(my-leader-or-vi-refresh-prefer-insert))

(my-bind :map 'occur-mode-map "M-\\" #'my-occur-execute-in-original-buffer)

(provide 'my-hook-occur-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)

;;;###autoload
(cl-defun my-dired-multi-paste
    (func new &optional
          (files (my-dired-get-marked-files-in-frames))
          (yes nil))
  "For each FILES, do FN to NEW.
Ask for confirmation unless YES."
  (when-let ((_ (or yes (y-or-n-p
                         (format "%s __ %d __ : \n%s \nto %s ?"
                                 func (length files) files new))))
             (wrapper
              (lambda ()
                (require 'dired-aux) (require 'seq)
                (setq dired-recursive-copies 'always)
                (dolist (file files)
                  (funcall func file new t))))
             (finisher
              (lambda (result)
                (message "%s" result)
                (dolist (buf (buffer-list))
                  (with-current-buffer buf
                    (when (eq major-mode 'dired-mode)
                      (revert-buffer)))))))
    (let ((existings
           (--> files
                (-map #'file-name-nondirectory it)
                (-map (-cut file-name-concat new <>) it)
                (-map #'file-exists-p it)
                (-remove #'null it))))
      (if existings
          (message "%s exist in current directory." existings)
        (async-start wrapper finisher)))))

;;;###autoload
(progn
;;;###autoload
  (defun my-dired-multi-paste-make-command (func)
    (lambda (arg)
      (:documentation
       (format
        "Run `%s' on multiple marked files from other windows.
If ARG is non-nil, take other windows from current frame only, else all frames."
        func))
      (interactive "P")
      (my-dired-multi-paste
       func
       (dired-current-directory)
       (or (my-dired-get-marked-files-in-frames
            (if arg (selected-frame) (frame-list)))
           (my-get-files-in-clipboard))))))

;;;###autoload
(defalias 'my-dired-multi-paste-cut
  (my-dired-multi-paste-make-command 'dired-rename-file))
;;;###autoload
(defalias 'my-dired-multi-paste-copy
  (my-dired-multi-paste-make-command 'dired-copy-file))
;;;###autoload
(defalias 'my-dired-multi-paste-symlink
  (my-dired-multi-paste-make-command 'make-symbolic-link))
;;;###autoload
(defalias 'my-dired-multi-paste-hardlink
  (my-dired-multi-paste-make-command 'dired-hardlink))

(provide 'my-dired-multi-paste)

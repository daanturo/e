;; -*- lexical-binding: t; -*-

(require '00-my-core-macros)
(require 'dash)

;;;###autoload
(progn

  (autoload #'puni-beginning-pos-of-list-around-point "puni")
  (autoload #'puni-beginning-pos-of-list-around-point "puni")
  (autoload #'puni-bounds-of-list-around-point "puni")
  (autoload #'puni-bounds-of-sexp-around-point "puni")
  (autoload #'puni-end-pos-of-list-around-point "puni")
  (autoload #'puni-end-pos-of-list-around-point "puni")
  (autoload #'puni-mark-sexp-at-point "puni")

  (autoload #'sp-mark-sexp "smartparens"))



;;; Helper and Utilities

;;;###autoload
(defun my-pair-matching-open-char-of (closing)
  (-let* ((char (string-to-char closing)))
    (-some--> char
      ;; (electric-pair-syntax-info it) (nth 1 it)
      (matching-paren it)
      (and (<= it char)
           it)
      (char-to-string it))))

;;;###autoload
(defun my-pair-get-pair-alist (&optional mode)
  (-let* ((mode (or mode major-mode)))
    (cl-loop for (pair-mode . pairs) in my-pair-mode-pair-alist
             when (or (equal 'global pair-mode)
                      (provided-mode-derived-p mode pair-mode))
             append pairs)))

;;; Pair insertion

(add-hook 'my-mc-always-keep-prefix-list (format "%s-" 'my-pair-insert))

(defun my-pair--make-self-insert-command (char-str)
  (let ((char (string-to-char char-str)))
    (my-maydefun-set (format "%s-%s" 'my-pair-insert char-str)
      ((n &optional _)
       (interactive "p")
       (my-emulate-self-insert-command n char))
      (format "`my-emulate-self-insert-command' with CHAR bound to %c" char))))

(defun my-pair--make-multichar-insert-command (open close)
  (let ((trigger (s-right 1 open)))
    (my-maydefun-set
      (format "%s-%s-%s" 'my-pair-insert-multichar open close)
      ((n)
       (interactive "p")
       (when (< 0 n)
         (insert trigger))
       (insert (s-repeat (- n 1) open))
       (save-excursion (insert (s-repeat n close))))
      ;; ;; emulate `self-insert-command' for the last character
      ;; (dlet ((last-command-event (my-at close -1)))
      ;;   (run-hooks 'post-self-insert-hook))

      (format "Insert <`\"%s\"'×1><`\"%s\"'×(N-1)>|<`\"%s\"'×N> when positive N."
              trigger
              open
              close))))

(defun my-pair--handle-p (open close)
  (let* ((back-length (1- (length open))))
    (equal (substring open 0 back-length)
           ;; still faster than `looking-back'
           (my-relative-buffer-substring back-length 0))))

;;;###autoload
(defun my-pair-get-handler (mode trigger)
  "In MODE and pressed TRIGGER, find an appropriate pair from
`my-pair-mode-pair-alist'."
  (let ((pairs (my-pair-get-pair-alist mode)))
    (-find (-lambda ((open . close))
             (and (string-suffix-p trigger open)
                  (my-pair--handle-p open close)))
           pairs)))

;;;###autoload
(progn

  (defun my-pair-set-pair (mode open close)
    ;; Add and sort
    (let ((lst (alist-get mode my-pair-mode-pair-alist)))
      ;; lists ought to be short, so we don't need an optimized but complex
      ;; algorithm for sorted insertion
      (--> (cons (cons open close) lst)
           -uniq
           (cl-sort it #'my-by-length> :key #'car)
           (setf (alist-get mode my-pair-mode-pair-alist) it))))

  (defun my-pair-dispatch-by-trigger (trigger &optional fallback-close)
    "With TRIGGER as the last character of an opening, return an
insert command that is appropriate in current context. When a
multi-character pair is not appropriate and non-nil
FALLBACK-CLOSE, insert it after the point; use this in case even
the auto-closer below doesn't insert."
    (my-maydefun-set (format "%s-%s" 'my-pair-dispatch-by-trigger trigger fallback-close)
      ((_)
       (require 'my-pair)
       (when (not buffer-read-only)
         (if (and (use-region-p)
                  ;; likely forced?
                  fallback-close)
             (my-pair--make-self-insert-command trigger)
           (-let* ((pair (and
                          ;; can't detect this in key filters?
                          (not current-prefix-arg)
                          (my-pair-get-handler major-mode trigger)))
                   ((open . close) pair))
             (cond (pair
                    (my-pair--make-multichar-insert-command open close))
                   (fallback-close
                    (my-pair--make-multichar-insert-command trigger fallback-close)))))))))

  (cl-defun my-pair-add-multichar-pair (mode/s open close &optional trigger-key)
    "In MODE/S, register a pair of OPEN - CLOSE for insertion and
bind TRIGGER-KEY or the last char of OPEN as the trigger."
    (let ((trigger (or trigger-key (s-right 1 open))))
      (dolist (mode (ensure-list mode/s))
        (my-pair-set-pair mode open close))
      (define-key my-pair-edit-mode-map
                  (kbd trigger)
                  `(menu-item "" nil :filter
                              ,(my-pair-dispatch-by-trigger trigger (and trigger-key close))))))

  (defun my-add-local-electric-pair (mode/s open-char close-char)
    (with-eval-after-load 'elec-pair
      (my-add-mode-hook-and-now mode/s
        (lambda ()
          (setq-local electric-pair-pairs
                      (-uniq `((,open-char . ,close-char)
                               ,@electric-pair-pairs))))))))

;;;###autoload
(progn

  (defvar my-pair-mode-pair-alist
    '((global .
              (("\\\\(" . "\\\\)")
               ("\\(" . "\\)")
               ("\\\\[" . "\\\\]")
               ("\\[" . "\\]")
               ("\\\\{" . "\\\\}")
               ("\\{" . "\\}"))))
    "Each key is a major mode, each value is open and close.
For each sub-list, when a pair whose opening is a super string of
another one, it must be sorted before that one (see
`my-pair-get-handler'' definition for the reason). Also this
should only contains multi-character pairs, single-character ones
better go to `electric-pair-pairs'.")

  (define-minor-mode my-pair-edit-mode
    "`my-pair-mode-pair-alist'"
    :global nil
    :keymap
    `((,(kbd "[") .
       (menu-item "" nil :filter ,(my-pair-dispatch-by-trigger "[")))
      (,(kbd "{") .
       (menu-item "" nil :filter ,(my-pair-dispatch-by-trigger "{")))))

  (define-globalized-minor-mode my-global-pair-edit-mode
    my-pair-edit-mode
    my-pair-edit-mode-maybe
    (if my-global-pair-edit-mode
        (progn
          (add-hook
           'post-self-insert-hook #'my-pair-insert-space-between-pairs-h)
          (add-hook 'post-self-insert-hook #'my-pair-open-newline-before-h))
      (progn
        (remove-hook
         'post-self-insert-hook #'my-pair-insert-space-between-pairs-h)
        (remove-hook 'post-self-insert-hook #'my-pair-open-newline-before-h))))

  (defvar my-pair-edit-exclude-modes '(term-mode))

  (defun my-pair-edit-mode-maybe ()
    (when (not
           (or buffer-read-only
               (apply #'derived-mode-p my-pair-edit-exclude-modes)
               ;; (get-buffer-process (current-buffer))
               ;; `find-file''s "M-backspace", sexp-editing commands are not
               ;; important while finding files anyway
               minibuffer-completing-file-name))
      (my-pair-edit-mode))))

;;;###autoload
(defun my-pair-auto-wrap (open close)
  "Automatically wrap from cursor to the end of list with OPEN and CLOSE.
If cursor at line beginning, end at
`my-end-position-of-last-sexp-of-current-line' (otherwise wrapping may
be done to the buffer end)."
  (-let* ((beg (point))
          (end
           (cond
            ((= beg (line-beginning-position))
             (my-end-position-of-last-sexp-of-current-line))
            (:else
             (puni-end-pos-of-list-around-point)))))
    (save-excursion
      (goto-char end)
      (insert close))
    (insert open)
    (save-excursion (insert " "))))

;;;###autoload
(defun my-pair-auto-wrap-round-bracket|parenthesis ()
  (interactive)
  (my-pair-auto-wrap "(" ")"))

;;;###autoload
(defun my-pair-auto-wrap-square-bracket ()
  (interactive)
  (my-pair-auto-wrap "[" "]"))

;;;###autoload
(defun my-pair-auto-wrap-curly-bracket ()
  (interactive)
  (my-pair-auto-wrap "{" "}"))


;;; Navigations

;;;###autoload
(defun my-go-to-end-of-list ()
  (interactive)
  (goto-char (puni-end-pos-of-list-around-point)))

;;;###autoload
(defun my-go-to-beg-of-list ()
  (interactive)
  (goto-char (puni-beginning-pos-of-list-around-point)))

;;;###autoload
(progn
  (define-minor-mode my-pair-navigate-mode nil
    :global t
    :keymap '()))

;;; Hooks

;;;###autoload
(progn

  ;;;###autoload
  (defun my-pair-insert-space-between-pairs-h ()
    "Insert space that \"( |)\" -> \"( | )\".
For shell scripts's \"[[ |]]\" -> [[ | ]].
TODO: upstream this, similar to
`electric-pair-open-newline-between-pairs-psif'."
    (let* ((char-before-spc (char-before (- (point) 1)))
           (char-after-spc (char-after)))
      (when (and
             ;; just typed a space
             (eq last-command-event ?\ )
             (< (1+ (point-min)) (point) (point-max))
             (cond
              ;; \( |\)
              ((equal ?\\ char-after-spc)
               (and (equal ?\\ (char-before (- (point) 2)))
                    (eq
                     char-before-spc
                     (matching-paren (char-after (+ (point) 1))))))
              ;; ( |)
              (:else
               (eq char-before-spc (matching-paren char-after-spc))))
             (not (my-point-in-string-p)))
        (save-excursion (insert " ")))))

  ;;;###autoload
  (defun my-pair-open-newline-before-h ()
    "`my-pair-looking-at-local-multichar-closing'?"
    (when (and (eq last-command-event ?\n)
               (< (1+ (point-min)) (point) (point-max))
               (my-pair-looking-at-local-multichar-closing))
      (save-excursion (newline 1)))))

;;;###autoload
(defun my-pair-looking-at-local-multichar-closing ()
  (-->
   ;; mode-local-only pairs
   (-some (-lambda ((mode . pairs))
            (and (derived-mode-p mode)
                 pairs))
          my-pair-mode-pair-alist)
   ;; looking at the a multi-character closing bracket?
   (-find (-lambda ((open . clos))
            (looking-at-p (regexp-quote clos)))
          it)))

;;; Configurations

;; `smartparens' forcefully add those hooks when loaded, as we don't use
;; `smartparens-mode' but some of its commands, remove those to prevent
;; potential performance issues
(with-eval-after-load 'smartparens
  (remove-hook 'post-command-hook 'sp--post-command-hook-handler)
  (remove-hook 'post-self-insert-hook 'sp--post-self-insert-hook-handler)
  (remove-hook 'pre-command-hook 'sp--save-pre-command-state))

;;;###autoload
(defun my-pair-menu-item-filter-read-only (cmd)
  (and
   ;; some commands such as `puni-backward-kill-line' doesn't work with
   ;; minibuffer's read-only prompt
   (not (derived-mode-p 'minibuffer-mode 'minibuffer-inactive-mode))
   cmd))

;;;###autoload
(progn

  (defun my-setup-sexp-keys ()

    ;; Tempel expansions doesn't work properly with electric-pair-mode
    ;; https://github.com/minad/tempel/issues/116
    ;; https://yhetil.org/emacs/878r8ye884.fsf@disroot.org/T/#u
    ;; TODO: remove this (just use the default) when bug#66155 is solved
    (setq electric-pair-inhibit-predicate #'ignore)
    ;; ;; this is simpler but can't insert a nested pair: "[[]"
    ;; (setq electric-pair-preserve-balance nil)


    (my-when-graphical
     (my-bind :map 'my-pair-navigate-mode-map
       "M-[" #'my-backward-or-backward-up-sexp "M-]" #'my-forward-or-up-sexp))

    (-let* ((prefix (concat "M-SPC" " "))
            ;; add check for commands that would override default ones
            (puni? (fboundp #'puni-mode))
            (smartparens? (fboundp #'smartparens-mode)))

      ;; Non-destructive

      (my-bind :map 'my-pair-navigate-mode-map "C-M-(" #'puni-syntactic-backward-punct)
      (my-bind :map 'my-pair-navigate-mode-map "C-M-)" #'puni-syntactic-forward-punct)
      (my-bind :map 'my-pair-navigate-mode-map "C-M-SPC" `(menu-item "" my-mark-sexp-default :filter my-kmi-filter-point-in-string-or-comment-strict))
      (my-bind :map 'my-pair-navigate-mode-map "C-M-b" #'puni-backward-sexp :if puni?)
      (my-bind :map 'my-pair-navigate-mode-map "C-M-f" #'puni-forward-sexp :if puni?)
      (my-bind :map 'my-pair-navigate-mode-map "M-A" #'my-go-to-beg-of-list)
      (my-bind :map 'my-pair-navigate-mode-map "M-E" #'my-go-to-end-of-list)
      (my-bind :map 'my-pair-navigate-mode-map :from 'smartparens (concat prefix "M-w") #'sp-copy-sexp)

      ;; Destructive

      (my-bind :map 'my-pair-edit-mode-map "C-(" #'puni-slurp-backward)
      (my-bind :map 'my-pair-edit-mode-map "C-)" #'puni-slurp-forward)
      (my-bind :map 'my-pair-edit-mode-map "C-M-;" #'my-toggle-comment-sexp)
      (my-bind :map 'my-pair-edit-mode-map "C-M-S-<left>" #'my-tranpose-sexp-and-go-left)
      (my-bind :map 'my-pair-edit-mode-map "C-M-S-<right>" #'my-tranpose-sexp-and-go-right)
      (my-bind :map 'my-pair-edit-mode-map "C-M-S-k" #'kill-line)
      (my-bind :map 'my-pair-edit-mode-map "C-M-k" `(menu-item "" my-kill-sexp-default :filter my-kmi-filter-point-in-string-or-comment-strict))
      (my-bind :map 'my-pair-edit-mode-map "C-k" #'my-puni-kill-line :if puni?)
      (my-bind :map 'my-pair-edit-mode-map "C-{" #'puni-barf-backward)
      (my-bind :map 'my-pair-edit-mode-map "C-}" #'puni-barf-forward)
      (my-bind :map 'my-pair-edit-mode-map "DEL" #'puni-backward-delete-char :if puni?)
      (my-bind :map 'my-pair-edit-mode-map "M-DEL" #'puni-backward-kill-word :if puni?)
      (my-bind :map 'my-pair-edit-mode-map "M-d" #'puni-forward-kill-word :if puni?)
      (my-bind :map 'my-pair-edit-mode-map (concat prefix "M-<down>") #'my-puni-splice-killing-forward-sexp-default)
      (my-bind :map 'my-pair-edit-mode-map (concat prefix "M-<up>") #'my-puni-splice-killing-backward-sexp-default)
      (my-bind :map 'my-pair-edit-mode-map (concat prefix "M-S") #'puni-split)
      (my-bind :map 'my-pair-edit-mode-map (concat prefix "M-v") #'my-duplicate-current-sexp)
      (my-bind :map 'my-pair-edit-mode-map (concat prefix "M-c") #'puni-convolute)
      (my-bind :map 'my-pair-edit-mode-map (concat prefix "M-k") #'puni-squeeze)
      (my-bind :map 'my-pair-edit-mode-map (concat prefix "M-r") #'raise-sexp)
      (my-bind :map 'my-pair-edit-mode-map (concat prefix "M-s") #'my-puni-splice-sexp-default)
      (my-bind :map 'my-pair-edit-mode-map :from 'smartparens "M-K" #'sp-kill-sexp) ; kill the sexp that contains the point
      (my-bind :map 'my-pair-edit-mode-map :from 'smartparens (concat prefix "M-j") #'sp-join-sexp)))

  ;;


  (defun my-setup-closing-square-bracket-to-opening-round-parenthesis ()
    "Map \"]\" (closing square bracket) to \"(\" (opening round parenthesis).
For ease of access to the most commonly used bracket type."
    (my-bind :map 'my-pair-edit-mode-map
      "]" `(menu-item "" nil :filter ,(my-pair-dispatch-by-trigger "(" ")")))))
;; (my-bind :map 'my-lispy-mode-map "]" 'lispy-parens-auto-wrap)

;;; my-pair.el ends here

(provide 'my-pair)

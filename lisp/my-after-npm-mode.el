;; -*- lexical-binding: t; -*-

;; disable "C-c n"
(setcdr npm-mode-keymap nil)

(provide 'my-after-npm-mode)

;; Local Variables:
;; no-byte-compile: t
;; End:

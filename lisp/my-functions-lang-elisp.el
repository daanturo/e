;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;; Defs

;;;###autoload
(defun my-ielm-in-current-directory ()
  (interactive)
  (ielm (format "*ielm %s*" (abbreviate-file-name default-directory))))

;;;###autoload
(defun my-lang-elisp-repl-ielm ()
  (interactive)
  (-let* ((buf-name (my-evalu-repl-default-name "ielm")))
    (ielm buf-name)
    (get-buffer buf-name)))

;;;###autoload
(defun my-elisp-enable-lexical-binding ()
  (interactive)
  (setq-local lexical-binding t)
  (-let* ((modified (buffer-modified-p))
          (empty (my-buffer-emptyish-p)))
    (add-file-local-variable-prop-line 'lexical-binding t)
    (when empty
      (my-ensure-final-newline)
      (goto-char (point-max)))
    (unless modified
      (save-buffer 0))))

;;;###autoload
(defun my-elisp-add-local-variable-no-byte-compile ()
  (interactive)
  (save-excursion
    (add-file-local-variable 'no-byte-compile t)
    (save-buffer 0)))

;;;###autoload
(defun my-elisp-list-no-byte-compile-files (directory)
  (--> (expand-file-name directory)
       (format "grep -l -r ';; no-byte-compile: t' \"%s\"" it)
       (shell-command-to-string it)
       (split-string it "\n" 'omit-nulls)))

;;;###autoload
(defun my-elisp-list-compiled-no-byte-compile-files (directory)
  (-keep (lambda (path)
           (-let* ((compiled (replace-regexp-in-string "\\.el$" ".elc" path)))
             (and (file-exists-p compiled)
                  compiled)))
         (my-elisp-list-no-byte-compile-files directory)))

;;;###autoload
(defun my-elisp-delete-compiled-no-byte-compile-files (directory)
  (interactive "D my-elisp-delete-compiled-no-byte-compile-files : ")
  (dolist (file (my-elisp-list-compiled-no-byte-compile-files directory))
    (message "`my-elisp-delete-compiled-no-byte-compile-files' %s" file)
    (delete-file file)))

;;;###autoload
(defun my-elisp-report-native-comp-async ()
  (interactive)
  (-let* ((buf (get-buffer-create
                (concat "*" "my-elisp-report-native-comp-async" "*"))))
    (cl-labels ((nl-at-last () (goto-char (point-max)) (insert "\n"))
                (pp-report (expr)
                  (pp expr) (nl-at-last)
                  (pp (eval expr t)) (nl-at-last)))
      (with-current-buffer buf
        (view-mode 0)
        (delete-region (point-min) (point-max))
        (with-output-to-temp-buffer buf
          (lisp-data-mode)
          (pp-report 'comp-async-compilations)
          (pp-report 'comp-files-queue)
          (view-mode))))))

(defun my-elisp--list-compiled-outputs (dir dest-fn &optional interactive?)
  (-let* ((outputs (-map dest-fn (directory-files dir 'full "\\.el\\'"))))
    (when interactive?
      (message "%s" (string-join outputs "\n")))
    outputs))

;;;###autoload
(cl-defun my-elisp-list-byte-compile-outputs (&optional
                                              (dir default-directory))
  (interactive "D my-elisp-list-byte-compile-outputs : ")
  (my-elisp--list-compiled-outputs dir #'byte-compile-dest-file
                                   (called-interactively-p t)))

;;;###autoload
(cl-defun my-elisp-list-native-compile-outputs (&optional
                                                (dir default-directory))
  (interactive "D my-elisp-list-native-compile-outputs : ")
  (my-elisp--list-compiled-outputs dir #'comp-el-to-eln-filename
                                   (called-interactively-p t)))

;;;###autoload
(cl-defun my-elisp-list-compile-outputs (&optional
                                         (dir default-directory))
  (interactive "D my-elisp-list-native-compile-outputs : ")
  (append (my-elisp-list-byte-compile-outputs dir)
          (my-elisp-list-native-compile-outputs dir)))

;;;###autoload
(defun my-elisp-byte-recompile-file (file &optional then-load)
  "`byte-recompile-file' FILE, THEN-LOAD with prefix arg."
  (interactive "f my-elisp-byte-recompile-file : \nP")
  (byte-recompile-file file nil 0)
  (when then-load
    (load (file-name-sans-extension file))))

;;;###autoload
(defun my-elisp-byte-recompile-directory (&optional dir)
  "(`byte-recompile-directory' DIR)."
  (interactive "D my-elisp-byte-recompile-directory : ")
  (byte-recompile-directory (or dir default-directory)
                            0 nil 'follow-symlinks))

;;;###autoload
(defun my-elisp-byte-recompile-directory* (&optional dir)
  "`my-elisp-byte-recompile-file' on DIR's children."
  (interactive "D my-elisp-byte-recompile-directory* : ")
  (-map #'my-elisp-byte-recompile-file
        (directory-files (or dir default-directory)
                         'full "\\.el\\'")))

(defvar-local my-eval-output-mode-result nil)

;;;###autoload
(define-derived-mode
  my-eval-output-mode
  emacs-lisp-mode
  "eval"
  nil
  (my-safe-call #'hide-mode-line-mode)
  (my-fit-current-popup-window-height nil 1))

;;;###autoload
(defun my-inspector-inspect-eval-output-result ()
  (interactive)
  (-let* ((res my-eval-output-mode-result))
    ;; without this, the appeared window may be susceptible to the old window's
    ;; minimal height, therefore hurts visibility
    (delete-window)
    (inspector-inspect res)))

;;;###autoload
(defun my-pretty-eval-expression (expr &optional detail)
  "(`pp-eval-expression' EXPR) and always display output window.
DETAIL: use `inspector-inspect-expression'."
  (interactive (-let* ((detail current-prefix-arg))
                 (list
                  (read--expression (format "Evaluate %s: "
                                            (if detail
                                                "and inspect"
                                              ""))
                                    (my-region-string-maybe nil t))
                  detail)))
  (-let* ((result (eval expr t))
          ;; originally "*Pp Eval Output*"
          (buf (format "*%s*" 'my-pretty-eval-expression)))
    (if detail
        (inspector-inspect result)
      (prog1 (pp-display-expression result buf)
        (save-selected-window
          (pop-to-buffer buf)
          (my-eval-output-mode)
          (setq-local my-eval-output-mode-result result)
          ;; enable undoing
          (when (equal t buffer-undo-list)
            (setq buffer-undo-list '())))))))

;;;###autoload
(defun my-elisp-autofmt--fixes (&optional beg end)
  (save-excursion
    (-when-let* (((fnbeg . fnend)
                  (or (and beg end (cons beg end))
                      (bounds-of-thing-at-point 'defun)))
                 (beg (my-point-as-marker-at (or beg fnbeg)))
                 (end (my-point-as-marker-at (or end fnend))))

      ;; Fix newlines after some def forms

      ;; https://codeberg.org/ideasman42/emacs-elisp-autofmt/issues/10.
      (goto-char end)
      (while (re-search-backward (rx-to-string
                                  `(seq
                                    (or (regexp "cl-def[-a-z*]+")
                                        "-lambda")
                                    "\n"))
                                 beg 'noerror)
        (-let* ((mb (match-beginning 0))
                (mstr (match-string 0)))
          (unless (my-point-in-string-or-comment-p)
            (join-line -1)
            (unless (my-point-in-string-or-comment-p)
              ;; cl-defun name
              ;;          (arg0 arg1 ...
              (when (looking-at-p "[ \t]*[^ '\t\n\r]+\n[ \t\n\r]*(")
                (join-line -1)
                ;; so that parinfer doesn't split the sexp
                (indent-sexp))
              (condition-case _err
                  (progn
                    ;; cl-defun name (arglist) body-on-the-same-line
                    (goto-char mb)
                    ;; goto the end of (arglist)
                    (cond
                     ((string-match-p "\\<lambda\\>" mstr)
                      (forward-sexp 2))
                     (t
                      (forward-sexp 3)))
                    (unless (looking-at-p "[[:space:]]*$")
                      (newline-and-indent)))
                (scan-error nil))))))

      ;; (cl-defun name (arglist) (interactive) ; all
      (goto-char end)
      (while (re-search-backward (rx-to-string
                                  `(seq
                                    bol
                                    "("
                                    (regexp "cl-def.*\\((interactive\\)")))
                                 beg 'noerror)
        (unless (my-point-in-string-or-comment-p)
          (goto-char (match-beginning 1))
          (newline)))

      ;; Fix ,@\n〈list〉
      (goto-char end)
      (while (re-search-backward ",@[ \t\n\r]" beg 'noerror)
        (-let* ((m-beg (match-beginning 0))
                (m-end (match-end 0))
                (m-beg* (my-point-as-marker-at m-beg)))
          (goto-char m-end)
          (-let* ((nl-flag (bolp)))
            (unless (my-point-in-string-or-comment-p)
              (delete-all-space)
              (indent-sexp)
              ;; prevent infinite loop because of backward search
              (goto-char m-beg*)
              (when nl-flag
                ;; Fix "^[ ]+,@something ,@somethingmore", without splitting to new
                ;; line
                (when (or (looking-back "^[ \t]+\\(,@[^ \t]* \\)" (pos-bol))
                          (and (not (looking-back "^[ \t]+" (pos-bol)))
                               (save-excursion
                                 (ignore-errors
                                   (forward-sexp 1))
                                 (< fill-column (current-column)))))
                  (newline-and-indent)))))
          (goto-char m-beg*)))

      ;; Fix '(car\n . \n cdr) (lone . in line)
      (goto-char end)
      (while (re-search-backward "^[ \t]*\\.[ \t]*$" beg 'noerror)
        (unless (my-point-in-string-or-comment-p)
          (join-line)))

      ;; Fix lone :keyword in line
      (goto-char end)
      (while (re-search-backward "^[ \t]*:[^ \t]+ [ \t]*$" beg 'noerror)
        (unless (my-point-in-string-or-comment-p)
          (join-line -1)))

      ;; Fix dash.el's alphabetic anaphoric forms doesn't have the expression
      ;; next to it:
      ;; --<macro>\n
      ;; <not comment>
      (goto-char end)
      (while (re-search-backward "(--[a-z]+\\(\n\\)[ \t]*[^; \t]" beg 'noerror)
        (goto-char (match-end 1))
        (join-line nil)))))



;;;###autoload
(defun my-elisp-autofmt-format-sexp (&optional interact)
  (interactive (list t))
  (-let* (((beg end) (my-up-sexp-beg-&-end-positions)))
    (elisp-autofmt-region beg end interact)
    (my-elisp-autofmt--fixes beg end)))

;;;###autoload
(defun my-elisp-code-format-dwim (&optional arg)
  "Normalize current list.
ARG > 1: use `elisp-autofmt-region' for the current function def.
Active region: use `elisp-autofmt-region' on it.
Default: use `elisp-autofmt-region' for the current list."

  (interactive "P")
  (-let* ((num-arg (prefix-numeric-value arg)))
    (while-no-input
      (cond
       ((and arg (< 1 num-arg))
        (elisp-autofmt-region
         (my-beg-defun-position) (my-end-defun-position) 'interact)
        (my-elisp-autofmt--fixes))
       ((use-region-p)
        (-let* (((beg . end) (car (region-bounds))))
          (elisp-autofmt-region nil nil 'interact)
          (my-elisp-autofmt--fixes beg end)))
       (:else
        ;; `elisp-autofmt-region-dwim' doesn't correctly handle current sexp only,
        ;; but whole defun (ATM)?
        (my-elisp-autofmt-format-sexp 'interact))))))

;;;###autoload
(defun my-elisp-read-sexps-from-file (path)
  (with-temp-buffer
    (insert-file-contents path)
    (named-let
        recur ((lst '()))
      (-let* ((flag (make-symbol ""))
              (sexp
               (condition-case _
                   (read (current-buffer))
                 ((end-of-file) flag))))
        (cond
         ((equal flag sexp)
          (nreverse lst))
         (t
          (recur (cons sexp lst))))))))

(defvar-local my-eval-expression-minibuffer-flag nil)
;;;###autoload
(defun my-eval-expression-minibuffer-setup-flag-h (&rest _)
  (setq my-eval-expression-minibuffer-flag t))

;;;###autoload
(defun my-elisp-eval-next-sexp (&optional arg)
  (interactive "P")
  (eval-region (point) (scan-sexps (point) 1) t))

;;;###autoload
(defun my-elisp-eval-upper-sexp ()
  (interactive)
  (save-excursion
    (up-list)
    (funcall (my-get-command-remap-maybe #'eval-last-sexp) nil)))

;;; Config

;;;###autoload
(my-accumulate-config-collect

 (when (fboundp #'elisp-demos-advice-describe-function-1)
   (advice-add
    'describe-function-1
    :after #'elisp-demos-advice-describe-function-1)
   (advice-add
    'helpful-update
    :after #'elisp-demos-advice-helpful-update))

 (when (fboundp #'highlight-quoted-mode)
   (add-hook 'emacs-lisp-mode-hook #'highlight-quoted-mode))

 ;; (when (fboundp #'highlight-defined-mode)
 ;;   (add-hook 'emacs-lisp-mode-hook #'highlight-defined-mode))

 (when (fboundp #'highlight-function-calls-mode)
   (add-hook 'emacs-lisp-mode-hook #'highlight-function-calls-mode)
   (with-eval-after-load 'highlight-function-calls
     (set-face-attribute 'highlight-function-calls-face nil
                         :inherit 'font-lock-function-call-face
                         :underline 'unspecified))))

;;;###autoload
(defun my-elisp-add-missing-provide-in-file (file &optional remove-old-provide)
  (interactive (list (read-file-name
                      (format "%s: " #'my-elisp-add-missing-provide-in-file)
                      nil nil nil
                      buffer-file-name)))
  (when remove-old-provide
    (with-current-buffer (find-file-noselect file)
      (my-elisp-remove-provide-in-buffer)
      (save-buffer 0)))
  (-let* ((text (format "\n%s" `(provide ',(file-name-base file)))))
    (my-append-missing-text-to-file text file)))

;;;###autoload
(defun my-elisp-insert-initial-template ()
  (interactive)
  (setq lexical-binding t)
  (insert
   ";; -*- lexical-binding: t; -*-\n\n"
   "(require 'dash)\n" ; many macros (threading, destructuring, etc.)
   (if (and (bound-and-true-p my-emacs-conf-dir/)
            (f-ancestor-of-p
             (file-truename my-emacs-conf-dir/)
             (file-truename buffer-file-name))
            (<= 2
                (- (f-depth buffer-file-name)
                   (f-depth my-emacs-conf-dir/))))
       "(require '00-my-core-macros)\n"
     "")
   "\n")

  (save-excursion
    (insert
     "\n\n"
     (format ";;; %s ends here"
             (file-name-nondirectory buffer-file-name))
     ;; `provide' after the outline heading comment to easily tranposing other
     ;; headings
     "\n\n"
     (format "(provide '%s)" (file-name-base buffer-file-name)))
    (when (and (file-equal-p
                my-lisp-dir/
                (file-name-parent-directory buffer-file-name))
               (string-match-p
                "^\\(^my-after-.*\\.el\\|my-hook-.*.el\\)$"
                (file-name-nondirectory buffer-file-name)))
      (insert "

;; Local Variables:
;; no-byte-compile: t
;; End:
"))))

;;;###autoload
(defun my-elisp-add-missing-footer-in-file-like-checkdoc (file)
  (interactive (list
                (read-file-name
                 (format
                  "%s: "
                  #'my-elisp-add-missing-footer-in-file-like-checkdoc)
                 nil nil nil buffer-file-name)))
  (-let* ((text
           (format "\n;;; %s ends here

%s"
                   (file-name-nondirectory file)
                   `(provide ',(file-name-base file)))))
    (my-append-missing-text-to-file text file)))

;;;###autoload
(defun my-read-expression/s-try-read ()
  (interactive)
  (dlet ((inhibit-message t))
    ;; `minibuffer-message'/`sit-for' takes some time
    (my-with-advice
      #'minibuffer-message
      :override #'ignore
      (let* ((mnb-depth (minibuffer-depth))
             (retval1 (read--expression-try-read)))
        (cond
         ;; more than 1 expressions
         ((equal mnb-depth (minibuffer-depth))
          (let* ((contents (minibuffer-contents))
                 (wrapped-contents (format "(progn\n%s\n)" contents)))
            (setq my-read-expression/s-try-read--last wrapped-contents)
            (my-replace-buffer-string wrapped-contents)
            (read--expression-try-read)))
         (:else
          retval1))))))

;;;###autoload
(defun my-read-expression/s (prompt &optional initial &rest _)
  (minibuffer-with-setup-hook (lambda ()
                                (my-keymap-local-set*
                                 '(("<remap> <read--expression-try-read>"
                                    my-read-expression/s-try-read))))
    (read--expression prompt initial)))

;;;###autoload
(defun my-trusted-content-add-diretory ()
  (interactive)
  (add-hook 'trusted-content (my-ensure-string-suffix "/" default-directory))
  ;; refresh diagnostics, but is it needed?
  (when flymake-mode
    (flymake-mode)))

;;; my-functions-lang-elisp.el ends here

(provide 'my-functions-lang-elisp)

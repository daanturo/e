;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; `(menu-item ""
;;   ,(my-make-command-without-minibuffer #'my-save-buffer-and-close)
;;   :filter my-M-z-keys--filter)

;;;###autoload
(define-minor-mode my-M-z-keys-mode nil
  :keymap (my-define-keymap
           ;;

           ;; ZZ
           "M-z" (my-make-command-without-minibuffer #'my-save-buffer-and-close)

           ;; zx, but... M-x
           "M-k" (my-make-command-without-minibuffer #'kill-current-buffer)

           ;; SPC w c
           "M-c" (my-make-command-without-minibuffer #'delete-window)
           ;; doesn't work because of `read-minibuffer-restore-windows'?

           ;;
           ))

;;;###autoload
(progn
  (defun my-M-z-keys--filter (cmd &optional _)
    (let* ((global-M-z-cmd (lookup-key global-map (kbd "M-z"))))
      (and (equal
            (or (bound-and-true-p current-minibuffer-command)
                this-command)
            global-M-z-cmd)
           cmd)))

  (defun my-M-z-keys-set-up-h ()
    (when (my-M-z-keys--filter t t)
      (my-M-z-keys-mode))))

(provide 'my-M-z-keys)

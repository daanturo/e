;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'dired)

;;;###autoload
(defun my-dired-sort-sort-size-when-read-args--around-a (func &rest args)
  (-let* ((read-string-adv (lambda (read-string-fn prompt init-input &rest read-string-args)
                             (apply read-string-fn prompt
                                    (my-ensure-string-suffix " -S" init-input)
                                    read-string-args))))
    (when (and (called-interactively-p 'any)
               args)
      (my-add-advice-once #'read-string :around read-string-adv))
    (unwind-protect
        (apply func args)
      (advice-remove #'read-string read-string-adv))))

;;;###autoload
(defun my-dired-find-true-parent-of-file-at-point ()
  (interactive)
  (--> (dired-file-name-at-point)
       file-truename file-name-parent-directory
       dired))

;;;###autoload
(defun my-dired-copy-filenames (&optional no-escape)
  "With NO-ESCAPE: don't escape spaces."
  (interactive "P")
  (-let* ((files
           (-->
            (or (dired-get-marked-files) (list dired-directory))
            (-map
             (lambda (filename)
               (string-remove-prefix "/sudo:root@localhost:" filename))
             it)
            (-map #'abbreviate-file-name it)
            (-map
             (if no-escape
                 #'identity
               #'my-quote|escape-spaces-in-filename|path-for-shell)
             it)
            (string-join it " "))))
    (message files)
    (kill-new files)))

;;;###autoload
(defun my-dired-home ()
  (interactive)
  (dired "~"))

;;;###autoload
(cl-defun my-dired-get-marked-files-in-frames
    (&optional (frames (frame-list)) (except-window (selected-window)))
  "Get the list of explicitly marked files by `dired' in FRAMES except EXCEPT-WINDOW.
When there isn't any explicit marks, `dired' counts the file at
point as marked, takes that into account."
  (cl-loop
   for window in (my-get-other-windows-in-frames frames except-window)
   append
   (with-selected-window window
     (my-dired-get-explitly-marked-files))))

;;;###autoload
(defun my-dired-get-explitly-marked-files (&optional marker short-names)
  "Return explicitly marked files in current buffer."
  (save-excursion
    (goto-char (point-min))
    (dlet ((dired-marker-char (or marker dired-marker-char)))
      (--> (dired-get-marked-files)
           (if short-names
               (-map #'abbreviate-file-name it)
             it)))))

;;;###autoload
(defun my-dired-do-rename (&optional arg)
  "Rename/move the file at point with itself as the initial input.
Doesn't include other regular file names while `completing-read'.
With non-nil ARG or marked files, call `dired-do-rename' instead."
  (interactive "P")
  (dlet ((vertico-preselect 'prompt))
    (if (or arg (my-dired-get-explitly-marked-files))
        (call-interactively #'dired-do-rename)
      (-if-let* ((old-file
                  (-some-->
                      (dired-file-name-at-point) (directory-file-name it)))
                 (new-file
                  (read-directory-name "Rename or move to: "
                                       nil
                                       nil
                                       nil
                                       (file-name-nondirectory old-file))))
          (progn
            (my-dired-undop-record #'dired-rename-file old-file new-file)
            (dired-rename-file old-file new-file nil))
        (progn
          (message "`my-dired-do-rename': no file name at point!"))))))

;;;###autoload
(defun my-dired-delete-no-trash ()
  "Like `dired-do-delete' but don't move them to trash."
  (declare (interactive-only t))
  (interactive)
  (cond
   ((and (member system-type '(gnu/linux))
         ;; (not (file-remote-p default-directory))
         )
    (let* ((files
            (-map
             ;; (lambda (f) (format "./%s" (file-relative-name f)))
             #'file-relative-name
             (or (my-dired-get-explitly-marked-files dired-del-marker)
                 (dired-get-marked-files))))

           (buf0 (current-buffer))
           (mod-tick (buffer-modified-tick)))
      (my-delete-files-async
       files t
       (lambda (stdout &rest _)
         (with-current-buffer buf0
           (when (equal mod-tick (buffer-modified-tick))
             (revert-buffer)))
         (message "`my-dired-delete-no-trash' %s: %s"
                  (string-join (-map #'my->Str files))
                  stdout)))))
   (t
    (dlet ((delete-by-moving-to-trash nil) (dired-recursive-deletes 'always))
      (dired-do-delete)))))

;;;###autoload
(defun my-dired-du-count-sizes ()
  (interactive)
  (-let* ((files (-map #'file-relative-name (dired-get-marked-files))))
    (message "`my-dired-du-count-sizes': %s ..." files)
    (my-process-async
     `("du" "-sh" ,@(and (length> files 1) '("-c")) ,@files)
     (lambda (out &rest _) (message "%s" out))
     :trim t)))

;;;###autoload
(defun my-dired-marking-archives-p (&optional files)
  "Every FILES matches some of `dired-compress-file-suffixes'?"
  (let ((files (or files (dired-get-marked-files))))
    (-every (lambda (file)
              (-some (-lambda ((suf . _))
                       (string-match-p suf file))
                     dired-compress-file-suffixes))
            files)))

;;;###autoload
(defun my-dired-compress-dwim (&optional arg)
  "Like `dired-do-compress' ARG.
See bug#52681: 29.0.50; dired-do-compress assume tar files
contain
directories (https://yhetil.org/emacs/83ee6713nv.fsf@gnu.org/t/#m3c545bfcbe0f46d389f8fc4d05cc0373d5ff4c1d)."
  (interactive "P")
  (require 'dired-aux)
  (if (my-dired-marking-archives-p)
      (my-auto-extract|uncompress-and-unpack-archive
       (dired-get-marked-files)
       'confirm)
    (dired-do-compress arg)))

;;;###autoload
(defun my-dired-open-by-default-application (&optional arg)
  (interactive "P")
  (if (or arg
          (not (dired-file-name-at-point)))
      (my-xdg-open-file-with-default-application dired-directory)
    (browse-url-of-dired-file)))

;;;###autoload
(defun my-dired-switch-to-prev-buffer (&optional pred msg)
  (interactive)
  (-let* ((buf
           (-some-->
               (my-buffers-with-major-mode/s 'dired-mode)
             (-find
              (or pred
                  (lambda (buf)
                    (my-parent-of-child-link-p
                     (buffer-local-value 'dired-directory buf) dired-directory)))
              it))))
    (if buf
        (switch-to-buffer buf)
      (message "%s"
               (or msg "`my-dired-switch-to-prev-buffer': no next buffer!")))))

;;;###autoload
(defun my-dired-switch-to-next-buffer ()
  (interactive)
  (my-dired-switch-to-next-buffer
   (lambda (buf)
     (my-parent-of-child-link-p
      dired-directory (buffer-local-value 'dired-directory buf)))
   "`my-dired-switch-to-next-buffer': no previous buffer!"))

;;;###autoload
(defun my-dired-listed-files ()
  (-let* ((res '()))
    (save-excursion
      (while (not (eobp))
        (save-excursion
          (when (not (eolp))
            (push (dired-file-name-at-point) res)))
        (forward-line 1))
      (--> (delete nil res) (nreverse res)))))

;;;###autoload
(defun my-dired-toggle-preview-directory-as-subtree-or-file (&optional arg)
  (interactive "P")
  (-let* ((file (dired-file-name-at-point)))
    (cond
     ((file-directory-p file)
      (require 'dired-subtree)
      (when (dired-subtree--is-expanded-p)
        (dolist (mode '(dired-preview-mode peep-dired))
          (when (and (boundp mode) (symbol-value mode))
            (funcall mode 0))))
      (dired-subtree-toggle))
     ((fboundp #'dired-preview-mode)
      (dired-preview-mode (or arg 'toggle)))
     ((fboundp #'peep-dired)
      (peep-dired (or arg 'toggle))))))

;;;###autoload
(defun my-dired-filename-beg-position ()
  (when (derived-mode-p 'dired-mode 'wdired-mode)
    (save-excursion
      (dired-move-to-filename)
      (point))))

;;;###autoload
(defun my-dired-filename-end-position ()
  (when (derived-mode-p 'dired-mode 'wdired-mode)
    (save-excursion
      ;; `dired-move-to-end-of-filename' errors when after its desired position
      (dired-move-to-filename)
      (dired-move-to-end-of-filename)
      (point))))

;;;###autoload
(defun my-dired-move-to-filename-beg ()
  (interactive)
  (when (derived-mode-p 'dired-mode 'wdired-mode)
    (dired-move-to-filename)))


(provide 'my-functions-dired)

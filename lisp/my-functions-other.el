;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-find-file-with-initial (init-file-name &optional link-source)
  "INIT-FILE-NAME LINK-SOURCE."

  ;; the interactive form cannot be used for `embark'

  ;; (interactive (list
  ;;               (abbreviate-file-name
  ;;                (or buffer-file-name
  ;;                    (dired-file-name-at-point)
  ;;                    default-directory))
  ;;               current-prefix-arg))

  ;; (find-file (read-file-name "Find file: " nil nil nil init-file-name))
  (-let*
      ((link-source
        (or
         link-source
         ;; for Dired mode, we are already at the file's parent directory, navigate the link source instead
         (derived-mode-p 'dired-mode))))
    (minibuffer-with-setup-hook
        (lambda ()
          ;; (replace-regexp-in-region ".*" init-file-name (minibuffer-prompt-end))
          (my-replace-buffer-string
           (abbreviate-file-name
            (if link-source
                (my-file-symlink-target-1 init-file-name)
              init-file-name)))
          (goto-char (point-max)))
      (call-interactively #'find-file))))

;;;###autoload
(defun my-append-missing-text-to-file (text file)
  "Unless FILE contains TEXT, insert TEXT at the end of FILE.
TEXT can be a multi-line string. This functions saves buffer when FILE is already visited."
  (with-current-buffer (find-file-noselect file)
    (save-excursion
      (goto-char (point-min))
      (unless (search-forward text nil 'noerror)
        (goto-char (point-max))
        (insert text)
        (save-buffer)))))

;;;###autoload
(defun my-tranpose-regions-between (buffer0 beg0 end0 buffer1 beg1 end1)
  (if (equal (get-buffer buffer0) (get-buffer buffer1))
      (with-current-buffer buffer0
        (save-excursion
          (transpose-regions beg0 end0 beg1 end1)
          (pulse-momentary-highlight-region (min beg0 beg1) (max end0 end1))))
    (let ((str0 (with-current-buffer buffer0 (buffer-substring beg0 end0)))
          (str1 (with-current-buffer buffer1 (buffer-substring beg1 end1))))
      (with-current-buffer buffer0
        (save-excursion
          (goto-char beg0)
          (delete-region beg0 end0)
          (insert str1)))
      (with-current-buffer buffer1
        (save-excursion
          (goto-char beg1)
          (delete-region beg1 end1)
          (insert str0))))))

;;;###autoload
(define-minor-mode my-sort-company-candidates-mode
  "Sort `company''s candidates by length and some other factors."
  :global t
  (with-eval-after-load 'company
    (if my-sort-company-candidates-mode
        (add-to-list 'company-transformers 'my-sort-company-candidates)
      (remove-hook 'company-transformers 'my-sort-company-candidates))))
(defun my-sort-company-candidates (cands)
  ;; ;; LSP servers usually provide smarter sorting
  ;; (if (bound-and-true-p lsp-mode) cands (sort cands #'my-by-length<))
  ;; Most are not that smart
  (cl-sort cands #'< :key #'my-score-company-candidate))
(defun my-score-company-candidate (cand)
  (let ((score (my-score-code-completion-candidate cand)))
    ;; whether CAND belongs to a low-priority completion backend.
    (if (member (get-text-property 0 'company-backend cand)
                my-completion-low-priority-backend-list)
        (* 2 score)
      score)))
(defvar my-completion-low-priority-backend-list
  '(company-yasnippet)
  "List of completion backends which should appear at the last.")

(provide 'my-functions-other)

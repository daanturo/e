;; -*- lexical-binding: t; -*-

;; (require 'dbus)
(require 'cl-macs)

;;; Package management

(defun my-package-el--install-interactively (install-fn)
  (package-initialize)
  (when (fboundp #'my-package-el-ensure)
    (my-package-el-ensure))
  (let* ((package-lst (mapcar #'car package-archive-contents))
         (pkgs
          (mapcar
           #'intern
           (completing-read-multiple
            "Install package(s): " (mapcar #'symbol-name package-lst)))))
    (condition-case err
        (mapc install-fn pkgs)
      (error
       (let* ((err-msg (error-message-string err))
              (retry
               ;; (y-or-n-p
               ;;  (format "`%s': %S, `package-refresh-contents' then retry?"
               ;;          install-fn err-msg))
               t))
         (when retry
           (package-refresh-contents)
           (mapc install-fn pkgs)))))))

;;;###autoload
(defun my-package-el-install-interactively ()
  "Prompt `package-install', ask to refresh and retry when unsuccessful."
  (interactive)
  (my-package-el--install-interactively #'package-install))

;;;###autoload
(defun my-package-vc-el-install-interactively ()
  "Prompt `package-vc-install', ask to refresh and retry when unsuccessful."
  (interactive)
  (my-package-el--install-interactively #'package-vc-install))

;;; Theme

;; https://andreyor.st/posts/2024-03-05-accepting-your-own-solutions-to-your-own-problems/

;; ;;;###autoload
;; (defun my-dbus-color-theme-dark-p ()
;;   (equal
;;    1
;;    (caar
;;     (condition-case nil
;;         (dbus-call-method
;;          :session
;;          "org.freedesktop.portal.Desktop"
;;          "/org/freedesktop/portal/desktop"
;;          "org.freedesktop.portal.Settings"
;;          "Read"
;;          "org.freedesktop.appearance"
;;          "color-scheme")
;;       (error nil)))))


;;;###autoload
(progn
  (cl-defun my-theme-get-system-color-scheme (&optional async-cb cmdargs out-to-scheme-fn)
    (let* ((cmdargs
            (or cmdargs
                '("dbus-send"
                  "--session"
                  "--print-reply"
                  "--dest=org.freedesktop.portal.Desktop"
                  "/org/freedesktop/portal/desktop"
                  "org.freedesktop.portal.Settings.Read"
                  "string:org.freedesktop.appearance"
                  "string:color-scheme")))
           (callback
            (lambda (stdout exit-code &rest _)
              (let* ((out (string-trim-right stdout))
                     (scheme
                      (cond
                       (out-to-scheme-fn
                        (funcall out-to-scheme-fn out))
                       ((/= 0 exit-code)
                        nil)
                       ((string-suffix-p " 1" out)
                        'dark)
                       ((string-suffix-p " 2" out)
                        'light))))
                (when async-cb
                  (funcall async-cb scheme))
                scheme))))
      (condition-case err
          (cond
           (async-cb
            (let* ((buf
                    (generate-new-buffer "my-theme-get-system-color-scheme" t))
                   (sentinel
                    (lambda (proc &rest _)
                      (with-current-buffer buf
                        (funcall callback
                                 (buffer-string)
                                 (process-exit-status proc)))
                      (kill-buffer buf))))
              (make-process
               :name "my-theme-get-system-color-scheme"
               :command cmdargs
               :buffer buf
               :sentinel sentinel)))
           (:else
            (with-temp-buffer
              (let* ((exit-code
                      (apply #'call-process
                             (car cmdargs)
                             nil
                             t
                             nil
                             (cdr cmdargs))))
                (funcall callback (buffer-string) exit-code)))))
        ((file-missing file-error) (and async-cb (funcall async-cb nil)))))))


;;;###autoload
(cl-defun my-theme-simple-auto-set (&key (dark 'modus-vivendi) (light 'modus-operandi))
  (interactive)
  (let ((scheme (my-theme-get-system-color-scheme)))
    (cond
     ;; ((equal 'light scheme)
     ;;  (customize-set-variable 'custom-enabled-themes (list light)))
     ((equal 'dark scheme)
      (customize-set-variable 'custom-enabled-themes (list dark))))))

;;; Other

;;;###autoload
(defun my-toggle-variable ()
  (declare (interactive-only t))
  (interactive)
  (let* ((syms (cl-loop for x being the symbols collect x))
         (var (intern (completing-read "`my-toggle-variable': " syms))))
    (set-default var (not (symbol-value var)))
    (message "`my-toggle-variable': %s -> %s" var (symbol-value var))))

;;; my-functions-for--quick.el ends here

(provide 'my-functions-for--quick)

;; Local Variables:
;; no-byte-compile: t
;; End:

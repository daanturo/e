;; -*- lexical-binding: t; -*-

(defun my-center-man-mode--adjust ()
  (setq-local Man-width-max (min (window-max-chars-per-line)
                                 (default-value 'Man-width-max))))

;;;###autoload
(define-minor-mode my-center-man-mode nil
  :global nil
  (if my-center-man-mode
      (progn
        (add-hook 'my-focus-center-mode-after-adjust-hook 'my-center-man-mode--adjust nil t)
        (setq-local my-focus-center-mode-body-width (default-value 'Man-width-max))
        (my-focus-center-mode))
    (progn
      (my-focus-center-mode 0)
      (kill-local-variable 'my-focus-center-mode-body-width)
      (remove-hook 'my-focus-center-mode-after-adjust-hook 'my-center-man-mode--adjust t))))

(provide 'my-functions-man)

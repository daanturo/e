;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-eldoc-quit-buffer ()
  (interactive)
  (delete-window
   (get-buffer-window (eldoc-doc-buffer))))

;;;###autoload
(defun my-eldoc-display-all-composed ()
  (interactive)
  (dlet ((eldoc-documentation-strategy #'eldoc-documentation-compose))
    (eldoc 'interactive)))

;;;###autoload
(define-derived-mode my-eldoc-mode special-mode "Eldoc")
(with-eval-after-load 'ace-window
  (add-to-list 'aw-ignored-buffers 'my-eldoc-mode))

;;;###autoload
(defun my-trigger-parameter-hints ()
  (interactive)
  (progn (and (bound-and-true-p lsp-mode)
              (lsp-signature-activate))
         (save-selected-window
           (dolist (buf (my-buffers-with-major-mode/s 'my-eldoc-mode))
             (my-quit|delete-window-buffer buf)
             (kill-buffer buf))
           (my-eldoc-display-all-composed)
           ;; (eldoc-print-current-symbol-info)

           ;; don't automatically update this
           (with-current-buffer (eldoc-doc-buffer)
             (setq eldoc--doc-buffer nil)
             (my-eldoc-mode)
             (rename-buffer (format "%s<%s>" (buffer-name) (random))
                            'unique)))))

(provide 'my-functions-eldoc)

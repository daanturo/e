;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(my-accumulate-config-collect
 (add-to-list 'interpreter-mode-alist '("scala" . scala-mode)))

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'my-hook-scala-mode-hook)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-reselect-region/--list '())

;;;###autoload
(defun my-reselect-region/save (marker-mark marker-point)
  (interactive (progn
                 (cl-assert (use-region-p))
                 (list (mark-marker) (point-marker))))
  (-let* ((elem
           (list
            :mark marker-mark
            :point marker-point
            :buffer (buffer-name)
            :filename buffer-file-truename
            :point (point))))
    (setq my-reselect-region/--list
          (--> `(,elem ,@my-reselect-region/--list) (delete-dups it)))
    (message "`my-reselect-region/save': %s %d->%d"
             (buffer-name)
             (marker-position marker-mark)
             (marker-position marker-point))
    elem))

(defun my-reselect-region/--get-buffer (elem)
  (-let* (((&plist :buffer buf-name :filename filename) elem)
          (buf
           (cond
            ((buffer-live-p (get-buffer buf-name))
             buf-name)
            ((get-file-buffer filename))
            (filename
             (find-file-noselect filename)))))
    buf))

(defun my-reselect-region/--string (elem)
  (-let* (((&plist :mark pmark :point ppoint) elem)
          (buf (my-reselect-region/--get-buffer elem))
          (full-str
           (-->(with-current-buffer buf
                 (buffer-substring pmark ppoint))
               (string-replace "\n" "⏎" it))))
    full-str))


;;;###autoload
(defun my-reselect-region/select ()
  (interactive)
  (-let* ((sorted-list
           (cl-sort
            my-reselect-region/--list
            (-lambda ((&plist :buffer buf-l :filename file-l)
                      (&plist :buffer buf-r :filename file-r))
              (and (or (equal buf-l (buffer-name))
                       (equal file-l buffer-file-truename))
                   (not
                    (or (equal buf-r (buffer-name))
                        (equal file-r buffer-file-truename)))))))
          (table
           (-map
            (lambda (elem)
              (-let* (((&plist
                        :mark pmark
                        :point ppoint
                        :buffer buf
                        :filename filename)
                       elem)
                      (str (my-reselect-region/--string elem))
                      (cand
                       (format "%s %s %S %s"
                               (marker-position pmark)
                               (marker-position ppoint)
                               (or buf (file-name-nondirectory filename))
                               str)))
                (cons cand `(,@elem :annot ,str))))
            sorted-list))
          (annot-fn (lambda (cand) (map-nested-elt table (list cand :annot))))
          (selected
           (-->
            (completing-read
             "`my-reselect-region/select': "
             (lambda (string pred action)
               (cond
                ((equal action 'metadata)
                 `(metadata
                   (display-sort-function . nil)
                   (annotation-function . ,annot-fn)))
                (t
                 (complete-with-action
                  action (-map #'car table) string pred)))))
            (map-nested-elt table (list it))))
          ((&plist :mark pmark :point ppoint :buffer buf) selected)
          (buf (my-reselect-region/--get-buffer selected)))
    (switch-to-buffer buf)
    (my-set-mark-from-to pmark ppoint)))

;;; my-reselect-region.el ends here

(provide 'my-reselect-region)

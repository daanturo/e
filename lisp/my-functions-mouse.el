;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-window-at-mouse-cursor-position ()
  (-let* (((frame x . y) (mouse-position)))
    (window-at x y frame)))

;;;###autoload
(defmacro my-scroll--with (event &rest body)
  (declare (indent defun) (debug t))
  `(my-with-selected-window-maybe (mwheel-event-window ,event)
     ,@body))

;;;###autoload
(defun my-scroll-view-right-1-column (&optional event)
  (interactive "e")
  (my-scroll--with event
    (scroll-left 1)))
;;;###autoload
(defun my-scroll-view-left-1-column (&optional event)
  (interactive "e")
  (my-scroll--with event
    (scroll-right 1)))

;;;###autoload
(defun my-scroll-mode--pixel-scroll-menu-item-kmi (cmd)
  (and
   ;; (bound-and-true-p pixel-scroll-precision-mode)
   (not (derived-mode-p 'minibuffer-mode))
   cmd))

;;;###autoload
(define-minor-mode my-scroll-mode
  nil
  :global t
  :keymap
  `(
    ;;
    ([wheel-left] . my-scroll-view-right-1-column)
    ([wheel-right] . my-scroll-view-left-1-column)
    ([S-prior] . my-scroll-view-portion-backward)
    ([S-next] . my-scroll-view-portion-forward)
    ([prior] .
     (menu-item
      ""
      pixel-scroll-interpolate-up
      :filter my-scroll-mode--pixel-scroll-menu-item-kmi))
    ([next] .
     (menu-item
      ""
      pixel-scroll-interpolate-down
      :filter my-scroll-mode--pixel-scroll-menu-item-kmi))
    ;;
    )
  (my-autoload-functions
    'pixel-scroll
    '(pixel-scroll-interpolate-down
      pixel-scroll-interpolate-up)
    nil 'interactive))

;;;###autoload
(defun my-setup-smooth-scroll (&rest _)
  ;; Conflicts with `vertico-scroll-up/down', we handle it in
  ;; `my-scroll-mode'
  (my-bind :map 'pixel-scroll-precision-mode-map '("<prior>" "<next>") nil)
  (setq-default pixel-scroll-precision-interpolate-page t)
  (setq-default pixel-scroll-precision-use-momentum t)
  (cond
   ;; `ultra-scroll-mode' is significantly smoother than
   ;; `pixel-scroll-precision-mode' when scrolling view up
   ((fboundp #'ultra-scroll-mode)
    (setq scroll-conservatively 101)
    (setq scroll-margin 0)
    (my-add-advice-once
      #'mwheel-scroll
      :before (lambda (&rest _) (ultra-scroll-mode))))
   ((fboundp #'pixel-scroll-precision-mode)
    (my-add-advice-once
      #'mwheel-scroll
      :before (lambda (&rest _) (pixel-scroll-precision-mode))))))

(provide 'my-functions-mouse)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(setq!
 eat-semi-char-non-bound-keys
 (-uniq
  (append
   eat-semi-char-non-bound-keys
   `( ;
     [?\e ?o] ; "M-o" `my-other-window'
     [?\e ? ] ; "M-SPC" , `doom-leader-alt-key'
     ;;
     ))))

;; easier to navigate
(add-hook 'my-toggle-evil-maybe-leader-hook #'my-eat-switch-mode-by-evil-h)

(my-bind :map 'eat-semi-char-mode-map "C-S-l" #'recenter-top-bottom)
(my-bind :map 'eat-semi-char-mode-map "C-S-v" #'eat-yank) ; like other graphical ones
(my-bind :map 'eat-semi-char-mode-map "C-u" #'eat-self-input) ; shell native
(my-bind :map 'eat-semi-char-mode-map "C-y" #'eat-self-input) ; shell native

(provide 'my-after-eat)

;; Local Variables:
;; no-byte-compile: t
;; End:

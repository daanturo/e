;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-without-bash-completion-enabled-a (func &rest args)
  (dlet ((bash-completion-enabled nil))
    (apply func args)))

;; (advice-add #'compilation-read-command :around #'my-without-bash-completion-enabled-a)

(provide 'my-functions-bash-completion)

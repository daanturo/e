;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(my-accumulate-config-collect
 (add-to-list 'auto-mode-alist '("\\.sct\\'" . c-mode)))

(add-hook 'my-execu-compiler-alist '(c-mode . "gcc"))
(add-hook 'my-execu-compiler-alist '(c++-mode . "g++"))

(add-hook 'my-execu-cmd-alist '(c-mode . my-execu-cmd-lang-cc-c-c++))
(add-hook 'my-execu-cmd-alist '(c++-mode . my-execu-cmd-lang-cc-c-c++))

(defun my-lang-c-set-comment-syntax-h ()
  "."
  ;; "// ..." is much easier to type and edit than "/* ... */"
  (setq-local comment-start "// ")
  (setq-local comment-end ""))

(my-add-mode-hook-and-now '(c-mode c-ts-mode) #'my-lang-c-set-comment-syntax-h)

;; (defvar my-lang-c/c++-style "Mozilla")
;; (with-eval-after-load 'apheleia
;;   (add-to-list
;;    'apheleia-formatters
;;    `(my-clang-format
;;      "clang-format" ,(format "--fallback-style=%s" my-lang-c/c++-style))))

(dolist (mode '(c-mode c++-mode))
  (my-evalu-set-handler mode '("cling")))

;; bound in `c-mode-map', `c++-mode-map' but not treesit mode
(my-bind :map '(c-ts-base-mode-map) '("M-l m" "C-c C-e") #'c-macro-expand)

(provide 'my-hook-c-mode-common-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

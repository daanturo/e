;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'xref)

;;;###autoload
(defun my-xref-try-fallback-backends (body-fn)
  (-let* ((all-backends
           (my-hook-local-global-value 'xref-backend-functions)))
    (named-let
        recur ((backends all-backends))
      (dlet ((xref-backend-functions backends))
        (cond
         ;; don't catch any errors if the last one
         ((>= 1 (length backends))
          (funcall body-fn))
         (:else
          (condition-case _
              (progn
                (funcall body-fn))
            ((user-error) (recur (cdr backends))))))))))

(cl-defmacro my-xref--with-dumb-jump (dumb-jump-flag &rest body)
  (declare (debug t) (indent defun))
  `(cond
    (,dumb-jump-flag
     (dlet ((xref-backend-functions '(dumb-jump-xref-activate)))
       ,@body))
    (:else
     ,@body)))

;;;###autoload
(defun my-xref-goto-definition (identifier &optional dumb-jump)
  "(`xref-find-definitions' IDENTIFIER) that tries backends one by one.
Ref:
https://yhetil.org/emacs/6288a8d5-1891-7382-0404-422821f6a37f@yandex.ru/T/#m7a6a2e9cfaeba5cf8ed980e3e88c0516c857ff8f
https://emacs.stackexchange.com/questions/64247/i-want-xref-find-definitions-that-uses-the-second-backend-when-the-first-backend

DUMB-JUMP: use `dumb-jump'."
  ;; `current-prefix-arg' is used for `dump-jump', let-bind to it nil
  ;; to prevent xref from prompting
  (interactive (list
                (my-xref--with-dumb-jump
                  current-prefix-arg
                  (dlet ((current-prefix-arg nil))
                    (xref--read-identifier
                     "`my-xref-goto-definition': ")))
                current-prefix-arg))
  (my-xref-try-fallback-backends
   (lambda ()
     (my-xref--with-dumb-jump
       dumb-jump (xref-find-definitions identifier)))))

;; (keymap-global-set "M-." #'my-xref-goto-definition)

;;; my-functions-xref.el ends here

(provide 'my-functions-xref)

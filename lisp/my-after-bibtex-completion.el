;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(setq bibtex-completion-bibliography org-cite-global-bibliography)

;;; my-after-bibtex-completion.el ends here

(provide 'my-after-bibtex-completion)
;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

;; NOTE TIP: `magit-log-merged' can be used to view commits around/surrounding a revision

(require 'magit)

;;; Magit

;;;###autoload
(defun my-magit-mode-hook-h ()
  "")

;; ;; `line-move-visual'/`posn-at-point' causes extreme lags sometimes
;; (setq-local line-move-visual nil)

;;


;;;###autoload
(defun my-magit-delta-mode-maybe ()
  (magit-delta-mode)
  ;; Extend +/- highlights after the end of lines
  (setq-local face-remapping-alist
              (-difference
               face-remapping-alist
               '((magit-diff-removed . default)
                 (magit-diff-removed-highlight . default)
                 (magit-diff-added . default)
                 (magit-diff-added-highlight . default)))))

;;;###autoload
(defun my-magit-bind-digits ()
  (my-bind :map 'magit-section-mode-map "M-l 1" #'magit-section-show-level-1)
  (my-bind :map 'magit-section-mode-map "M-l 2" #'magit-section-show-level-2)
  (my-bind :map 'magit-section-mode-map "M-l 3" #'magit-section-show-level-3)
  (my-bind :map 'magit-section-mode-map "M-l 4" #'magit-section-show-level-4)
  (my-bind :map 'magit-section-mode-map "M-l <f1>" #'magit-section-show-level-1-all)
  (my-bind :map 'magit-section-mode-map "M-l <f2>" #'magit-section-show-level-2-all)
  (my-bind :map 'magit-section-mode-map "M-l <f3>" #'magit-section-show-level-3-all)
  (my-bind :map 'magit-section-mode-map "M-l <f4>" #'magit-section-show-level-4-all)
  (my-bind :map 'magit-section-mode-map '("1" "2" "3" "4" "M-1" "M-2" "M-3" "M-4") #'digit-argument))


(my-magit-bind-digits)

;;;###autoload
(progn

  (defun my-magit-use-default-bindings ()
    "."

    ;; NOTE default Magit bindings is more ubiquitous, they work OOTB and are
    ;; expected for simple installations

    (my-add-hook/s '(+evil-collection-disabled-list) '(magit forge))

    ;; (add-hook 'magit-mode-hook #'my-leader-mode)

    ;; inspired by evil-collection-magit
    (my-bind :map 'magit-mode-map "S-SPC" #'magit-diff-show-or-scroll-up) ; was SPC
    (my-bind
      :map 'magit-mode-map "S-<backspace>"
      #'magit-diff-show-or-scroll-down) ; consistent with the above
    (my-bind :map 'magit-mode-map "`" #'my-magit-cycle-process-buffer-window) ; from "$"

    ;; Doom!
    (setq forge-add-default-bindings t))

  ;; NOTE while evil bindings have the following advantages:

  ;; - Doom configures [1-4] keys for us

  ;; - Native easy visual selection ("o" as `exchange-point-and-mark') for
  ;; fine-grained operations on regions (not important anymore because of
  ;; `my-mark-or-copy-line')


  (defun my-setup-magit ()

    (my-magit-use-default-bindings)

    (add-hook 'magit-mode-hook #'my-magit-mode-hook-h)
    
    ;; (add-hook 'magit-mode-hook #'my-leader-spc-key-mode)

    ;; see long commands
    (add-hook 'magit-process-mode-hook #'my-enable-line-wrapping)

    ;; (my-safe-call #'magit-maybe-define-global-key-bindings 'force)
    (setq magit-define-global-key-bindings nil)
    (my-bind "C-x g" #'magit-status)
    (my-bind "C-x M-g" #'magit-dispatch)
    (my-bind "C-c M-g" #'magit-file-dispatch)

    (when (and (executable-find "delta") (fboundp 'magit-delta-mode))
      (my-add-hook/s '(magit-mode-hook) #'(my-magit-delta-mode-maybe)))))


;; (with-eval-after-load 'evil
;;   (setf (alist-get "^magit.*:" evil-buffer-regexps) 'emacs))

;; ;; Problematic with non-interactive calls
;; (advice-add 'magit-process-insert-section :before #'my-magit-auto-display-process-buffer)

(advice-add
 #'magit-read-branch-or-commit :around
 #'my-magit-read-branch-or-commit-initial-input-as-commit-at-point--around-a)

(set-popup-rule! "^magit-process:" :ttl nil :size 0.2)

(with-eval-after-load 'magit-section
  ;; Free up C-<tab>
  (when-let ((cmd (lookup-key magit-section-mode-map [C-tab])))
    (my-bind :map 'magit-section-mode-map :vi '(global normal)
      [C-tab] nil
      [C-M-tab] cmd)))

(setq magit-delta-hide-plus-minus-markers nil)

(my-add-list!
  'magit-section-initial-visibility-alist
  '((unpushed . show)))
;; (untracked . hide)



(setq! magit-log-margin '(t "%F %R %Z" magit-log-margin-width t 16)) ; Show absolute date and/or time instead of relative in `magit-log'
(setq-default magit-diff-refine-hunk t)
(setq-default magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
(setq-default magit-log-merged-commit-count 32)
(setq-default magit-save-repository-buffers nil)
;; (setq magit-revision-show-gravatars t)

(my-windowpopup-set '(derived-mode magit-process-mode))

;; `magit-refresh' is slow
(advice-add #'magit-refresh :around #'my-with-deferred-gc-around-a)

;; (my-bind
;;   :map '(magit-mode-map magit-diff-mode-map) (list my-leader-key) #'my-leader-spc-prefix-cmd)

(my-bind :map 'magit-mode-map "M-<up>" #'my-magit-status-in-parent-directory)
(my-bind :map 'magit-mode-map "M-l d" #'my-magit-diff-commit-or-branch-at-point-to-HEAD)
(my-bind :map 'magit-mode-map "M-l l" #'my-magit-log-surrounding-commits)
(my-bind :map 'magit-mode-map "M-l p" #'my-magit/switch-project-and-status)
(my-bind :map 'magit-mode-map "S-<delete>" #'my-magit-discard-no-trash)
(my-bind :map 'magit-mode-map '("M-RET" "M-<return>") #'my-with-saved-selected-window-press-enter|return)
(my-bind :map 'magit-todos-list-mode-map '("M-RET" "M-<return>") #'my-with-pop-to-buffer-force-other-window-save-selected-press-enter|return)

;;; git-commit

;; ;; doesn't work
;; (add-hook 'git-commit-mode-hook #'my-git-commit-display-window-on-right-h)


;;; my-hook-magit-mode-hook.el ends here

(provide 'my-hook-magit-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

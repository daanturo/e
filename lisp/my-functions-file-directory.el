;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)
(require '01-my-core-functions)


;;;###autoload
(defun my-filename-true-sans-slash (file-name)
  "(`file-truename' FILE-NAME) with final slash removed."
  ;; stripping before findind the true name is faster
  (file-truename (directory-file-name file-name)))

;;;###autoload
(defun my-directory-children (dir &optional full match nosort count)
  "`directory-files' on DIR.
Somehow, manually process by elisp is still faster than
regexp (`directory-files-no-dot-files-regexp')."
  (named-let recur ((accu '())
                    (files (directory-files dir full match nosort count))
                    (exclude '("." "..")))
    (let ((f (car files)))
      (cond ((not (and files exclude))
             `(,@(nreverse accu) . ,files))
            ((member f exclude)
             (recur accu (cdr files) (delete f exclude)))
            (t
             (recur (cons f accu) (cdr files) exclude))))))

;;;###autoload
(defun my-find-file-as-hidden-popup (file prefix &optional focus)
  (let* ((buf (or (get-file-buffer file)
                  (find-file-noselect file)))
         (buf-name (buffer-name buf))
         (prefix (format " *%s" prefix)))
    (unless (and (string-prefix-p prefix buf-name)
                 (string-suffix-p "*" buf-name))
      (with-current-buffer buf
        (rename-buffer (concat prefix (buffer-name) "*"))
        (my-no-mode-line-mode)))
    (my-with-advice #'doom-modeline-update-buffer-file-name :override #'ignore
      (if focus
          (pop-to-buffer buf)
        (save-selected-window (pop-to-buffer buf))))
    buf))

;;;###autoload
(defun my-find-file-read-only-no-select-in-hidden-buffer (file-path)
  (let ((buf (find-file-noselect file-path ':nowarn)))
    (with-current-buffer buf
      (read-only-mode t)
      (rename-buffer (concat " " (buffer-name)))
      buf)))

;;;###autoload
(defun my-completing-read-multiple-filenames
    (prompt &optional dir default-filename mustmatch initial predicate)
  (-let* ((dir (or dir default-directory)))
    (dlet ((default-directory dir))
      (-let* ((file-names
               (completing-read-multiple
                prompt
                'completion-file-name-table
                predicate
                mustmatch
                initial
                'file-name-history
                default-filename)))
        (-map
         (lambda (filename) (file-name-concat dir filename)) file-names)))))

;;;###autoload
(defun my-read-new-filename (prompt initial)
  "Read a new file name with INITIAL, prompting with PROMPT.
Avoid selecting existing files."
  (read-directory-name prompt nil nil nil (file-name-nondirectory initial)))

;;;###autoload
(defun my-double-quote-filename-maybe (filename)
  (if (string-search " " filename)
      (format "\"%s\"" filename)
    filename))

;;;###autoload
(defun my-uniqify-directory-list (directory-list)
  (--> (-map (lambda (f) (file-name-as-directory f))
             directory-list)
       -uniq))

;;;###autoload
(defun my-files->directory-list (file-list &optional abbr)
  (dlet ((non-essential t) (file-name-handler-alist '()))
    (-->
     (-map
      (lambda (f)
        (if (or (string-suffix-p "/" f) (file-directory-p f))
            f
          (file-name-parent-directory f)))
      file-list)
     (if abbr
         (-map #'abbreviate-file-name it)
       it)
     my-uniqify-directory-list)))

;;;###autoload
(defun my-recent-directory-list ()
  (recentf-mode)
  ;; to speedup, assume that a file would never be deleted and replaced with a
  ;; directory (at least in this session)
  (my-memoize-last-args #'my-files->directory-list recentf-list))

(defvar-local my-buffer-filename-function nil)
;;;###autoload
(defun my-buffer-filename (&optional fallback-current-directory)
  (-some-->
      (cond
       ((functionp my-buffer-filename-function)
        (funcall my-buffer-filename-function))
       (t
        (or buffer-file-name
            (bound-and-true-p dired-directory)
            (and fallback-current-directory default-directory))))
    (abbreviate-file-name it)))

(defvar-local my-buffer-filename-can-open-with-find-file t)

;;;###autoload
(defun my-directory-has-matching-files-p (directory regexp)
  (--> (directory-files directory nil regexp nil 3)
       (cl-delete '("." "..") it :test #'-contains?)
       (< 0 (length it))))

;;;###autoload
(defun my-find-file-may-not-true-name (file-name &rest args)
  (interactive (list (read-file-name "my-find-file-may-not-true-name :"
                                     nil (thing-at-point 'filename))))
  (my-with-find-file-follow-true-name nil
    (apply #'find-file file-name args)))

;;;###autoload
(defun my-parent-of-child-link-p (parent child)
  "Does PARENT has a child that points to the same file as CHILD?"
  (-let* ((children (directory-files parent 'full
                                     directory-files-no-dot-files-regexp)))
    (-find (lambda (file)
             (f-equal-p child file))
           children)))

;;;###autoload
(defun my-set-visited-filename (file-name &optional no-query along-with-file)
  "`set-visited-file-name' while avoiding marking as modified.
(set-visited-file-name FILE-NAME NO-QUERY ALONG-WITH-FILE)"
  (-let* ((buf0 (find-buffer-visiting file-name))
          (modified (buffer-modified-p buf0)))
    (prog1 (set-visited-file-name file-name no-query along-with-file)
      (when (and buf0 (not modified))
        (set-buffer-modified-p nil)))))

;;;###autoload
(defun my-delete-files (file-names &optional trash)
  (-map (lambda (it) (delete-file it trash))
        file-names))

;;;###autoload
(defun my-file-symlink-target-n (file n)
  "Non-natural-number N: (`file-truename' FILE)."
  (cond ((my-natural-num? n)
         (named-let recur ((lv n)
                           (target file))
           (cond ((zerop lv)
                  target)
                 (t
                  (recur (- n 1)
                         (my-file-symlink-target-1 target))))))
        (t
         (file-truename file))))

;;;###autoload
(defun my-delete-directory (dir &optional trash)
  (delete-directory dir t trash))

;;;###autoload
(defun my-normalize-directory-name (dir)
  (file-name-as-directory dir))

;;;###autoload
(defun my-find-file-not-found-fn ()
  "Offer to create parent directory.
Like `doom-create-missing-directories-h' but when the child file
is a directory (\"/\" as last), use `dired' instead."
  (unless (file-remote-p buffer-file-name)
    (-let* ((dir-flag (string-suffix-p "/" buffer-file-name))
            (dir-to-make
             (if dir-flag
                 buffer-file-name
               (file-name-parent-directory buffer-file-name)))
            (file-name (purecopy buffer-file-name)))
      (when (not (file-directory-p dir-to-make))
        (-let* ((form `(make-directory ,dir-to-make 'parents))
                (make-flag (y-or-n-p (format "%S?" form))))
          (if make-flag
              (progn
                (eval form t)
                (letrec ((fn
                          (lambda (&rest _)
                            (remove-hook 'window-buffer-change-functions fn)
                            (when (equal file-name buffer-file-name)
                              ;; prevent "dir<2>"
                              (kill-buffer (current-buffer))
                              (dired file-name)))))
                  ;; after executing `find-file-not-found-functions', the buffer
                  ;; is expected to be a normal text-file-visiting buffer anyway
                  ;; so killing it and replacing with a dired buffer will cause
                  ;; errors, we need this as a workaround
                  (when dir-flag
                    (add-hook 'window-buffer-change-functions fn)))
                t)
            nil))))))

;;;###autoload
(defun my-filename-like-hidden? (path)
  (let ((path (string-remove-suffix "/" path)))
    (if (length< path 1)
        nil
      (= (aref (file-name-nondirectory path) 0)
         (string-to-char ".")))))
(defalias 'my-filename-like-hidden-p 'my-filename-like-hidden?)

;;;###autoload
(cl-defun my-delete-files-async (files &optional ask callback buffer)
  (-let* ((form
           `(let ((del-fn
                   (lambda (f)
                     (if (and (file-directory-p f) (not (file-symlink-p f)))
                         (delete-directory f 'recursive)
                       (delete-file f)))))
              (mapc del-fn '(,@files)))))
    (when (if ask
              (-let* ((prompt
                       (format "Delete %d files PERMANENTLY: %S"
                               (length files)
                               (seq-into files 'vector))))
                (y-or-n-p (replace-regexp-in-string "\n[^z-a]*" "…" prompt)))
            t)
      (my-process-batch-eval-in-async-emacs form callback :buffer buffer))))

;;;###autoload
(cl-defun my-rename-file-by-modification-time (filename &optional time-fmt)
  (declare (interactive-only t))
  (interactive (list (dired-file-name-at-point)))
  (-let* ((filename (file-name-nondirectory filename))
          (time-fmt (or time-fmt "%F_%T"))
          (ext (file-name-extension filename))
          (annot
           (-->
            filename
            (file-attributes it)
            (file-attribute-modification-time it)
            (format-time-string time-fmt it)))
          (new
           (format "%s--%s.%s" (file-name-sans-extension filename) annot ext))
          (cmd (format "mv %S %S" filename new)))
    (async-shell-command (read-shell-command "Command: " cmd))))

;;;###autoload
(defun my-copy-file-or-directory (source dest &rest args)
  "Copy from SOURCE to DEST.
Aim to be faster than the Emacs Lisp version by calling external
program(s), therefore may not be portable. ARGS are passed to it."
  (cl-assert (executable-find "cp"))
  (apply #'call-process
         "cp"
         nil
         nil
         nil
         "-r"
         (expand-file-name source)
         (expand-file-name dest)
         args))

;;;###autoload
(defun my-add-dir-local-variable-and-save (mode variable value &optional file-2)
  (require 'files)
  (save-window-excursion
    (dlet ((dir-locals-file
            (if file-2
                ".dir-locals-2.el"
              dir-locals-file)))
      (-let* ((orig-file buffer-file-name))
        (unwind-protect
            (progn
              (add-dir-local-variable mode variable value))
          (-let* ((buf (get-file-buffer dir-locals-file)))
            (when buf
              (with-current-buffer buf
                (save-buffer 0)))))))))

;;;###autoload
(defun my-find-file-no-truename (filename &rest args)
  (interactive (find-file-read-args
                "`my-find-file-no-truename': "
                (confirm-nonexistent-file-or-buffer)))
  (dlet ((find-file-visit-truename nil))
    (apply #'find-file filename args)))

;;;###autoload
(defun my-find-file-truename (filename &rest args)
  (interactive (find-file-read-args
                "`my-find-file-truename': "
                (confirm-nonexistent-file-or-buffer)))
  (dlet ((find-file-visit-truename t))
    (apply #'find-file filename args)))

;;;###autoload
(defun my-find-file-alternative-truename (filename &optional level)
  "Find FILENAME at point with `my-file-symlink-target-n' LEVEL.
nil LEVEL: invert `find-file-visit-truename'."
  (interactive (list
                (or (dired-file-name-at-point)
                    (read-file-name
                     ;; find-file-read-args
                     "`my-find-file-alternative-truename':"))
                current-prefix-arg))
  (cond
   ((null level)
    (dlet ((find-file-visit-truename (not find-file-visit-truename)))
      (find-file filename)))
   (:else
    (my-find-file-no-truename
     (my-file-symlink-target-n filename level)))))

;;;###autoload
(cl-defun my-completing-read-multiple-filenames-in-project-relative-to-current (&optional prompt)
  (-let* ((prompt (or prompt "Files: "))
          (paths
           (-->
            (my-project-file-and-directory-list)
            (completing-read-multiple
             prompt
             (lambda (string pred action)
               (if (eq action 'metadata)
                   `(metadata (category . project-file))
                 (complete-with-action action it string pred))))
            (-map
             (lambda (rel-path)
               (-->
                rel-path
                (file-name-concat (my-project-root) it)
                (file-relative-name it default-directory)
                (if (string-prefix-p "../" it)
                    it
                  (my-ensure-string-prefix "./" it))))
             it))))
    paths))

;;;###autoload
(defun my-insert-and-copy-filename-in-project-relative-to-current
    (&optional no-insert)
  (interactive (list "P"))
  (-let*
      ((paths
        (my-completing-read-multiple-filenames-in-project-relative-to-current))
       (str (string-join paths "\n")))
    (cond
     (no-insert
      (kill-new str))
     (:else
      (my-insert-and-copy str)))
    str))

;;;###autoload
(defun my-find-file-goto-line-column
    (filename line &optional column find-file-args)
  (-let* ((buf (apply #'find-file filename find-file-args)))
    (when line
      (with-current-buffer buf
        (goto-char (point-min))
        (forward-line (1- line))
        (when column
          (move-to-column column))))
    buf))

;;;###autoload
(cl-defun my-find-file:line:column-parse (str &key (+line t))
  "Extract file, line, (optional) column from STR.
+LINE: consider Emacs's FILE+LINE:COLUMN variant."
  (save-match-data
    (-let* ((match-flag
             (string-match
              ;; "\\`\\(.*?\\)[:+]\\([0-9]+\\)\\(?::\\([0-9]+\\)\\)?\\'"
              (rx-to-string
               `(seq
                 bos
                 (group (*? anychar))
                 (any ":" ,@(and +line '("+")))
                 (group (+ digit))
                 (?  ":" (group (+ digit)))
                 eos))
              str)))
      (and match-flag
           (list
            (match-string 1 str)
            (--> (match-string 2 str) (string-to-number it))
            (-some--> (match-string 3 str) (string-to-number it)))))))

;; TODO: feature request that upstream extracts from startup.el

;;;###autoload
(defun my-open|find-file-goto:line:column (filename:? &rest args)
  "Like (`find-file' FILENAME:? @ARGS), but allow going to :LINE:COLUMN."
  (declare (interactive-only t))
  (interactive (find-file-read-args "Find file:(l:c)? " nil))
  (-let* ((position (my-find-file:line:column-parse filename:?))
          ((filename lin col) position)
          (filename (or filename filename:?)))
    (cond
     ((and position
           (file-exists-p filename:?)
           (y-or-n-p
            (format "%s exists, open it instead of %s?" filename:? filename)))
      (apply #'find-file filename:? args))
     (:else
      (my-find-file-goto-line-column filename lin col args)))))

;;;###autoload
(defalias #'my-open-file #'my-open|find-file-goto:line:column)

;;; my-functions-file-directory.el ends here

(provide 'my-functions-file-directory)

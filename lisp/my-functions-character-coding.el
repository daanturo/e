;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-decode-ascii-series (series-str)
  (-->
   series-str
   (seq-split it 2)
   (-map (lambda (str) (--> str (string-to-number it 16))) it)
   (apply #'string it)))

;;; my-functions-character-coding.el ends here

(provide 'my-functions-character-coding)

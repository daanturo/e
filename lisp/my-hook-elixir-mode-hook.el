;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(my-accumulate-config-collect
 (add-to-list 'interpreter-mode-alist '("elixir" . elixir-mode)))

(with-eval-after-load 'lsp-elixir
  (-when-let* ((cmd (executable-find "elixir-ls")))
    (setq lsp-elixir-server-command '("elixir-ls"))))

(my-evalu-set-handler 'elixir-mode '("iex"))

(provide 'my-hook-elixir-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

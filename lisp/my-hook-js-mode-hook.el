;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(autoload #'jtsx-electric-open-newline-between-jsx-element-tags-psif "jtsx")
;; (autoload #'jtsx-jsx-electric-closing-element "jtsx")
;; (autoload #'rjsx-electric-lt "rjsx-mode")

(defvar my-js-treesit-defun-type-regexp-orig
  (regexp-opt
   '("class_declaration"
     "method_definition"
     "function_declaration"
     "lexical_declaration")))

;;;###autoload
(defun my-javascript-mode-hook-h ()
  (when (< 0 (length (my-treesit-parser-list)))
    (when (fboundp #'jtsx-tsx-mode)
      (add-hook 'post-self-insert-hook
                #'jtsx-electric-open-newline-between-jsx-element-tags-psif
                nil
                'local))

    (cond
     ((equal treesit-defun-type-regexp my-js-treesit-defun-type-regexp-orig)
      (setq-local treesit-defun-type-regexp
                  (regexp-opt
                   '("class_declaration"
                     "method_definition"
                     "function_declaration"
                     ;; "lexical_declaration"
                     ))))
     (:else
      (message
       "`my-javascript-mode-hook-h': `treesit-defun-type-regexp' has changed!"))))
  ;;
  )


(my-add-mode-hook-and-now '(js-mode tsx-ts-mode) #'my-javascript-mode-hook-h)

(-let* ((keymaps '(js-mode-map tsx-ts-mode-map)))
  (cond
   ((fboundp #'jtsx-tsx-mode)
    (my-bind :map keymaps :from 'jtsx ">" #'jtsx-jsx-electric-closing-element))
   ((fboundp #'rjsx-minor-mode)
    (my-bind :map keymaps :from 'rjsx-mode "<" #'rjsx-electric-lt))))

(my-bind :map 'tsx-ts-mode-map "/"
  `(menu-item "" my-lang-javascript-jsx-dwim-/-insert
    :filter my-lang-javascript-jsx-dwim-/-menu-item-filter-treesit))

(my-evalu-set-handler 'tsx-ts-mode #'+javascript/open-repl)

(with-eval-after-load 'emmet-mode
  (add-to-list 'emmet-jsx-major-modes 'tsx-ts-mode))

;;; my-hook-js-mode-hook.el ends here

(provide 'my-hook-js-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

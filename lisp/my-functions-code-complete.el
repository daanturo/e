;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-omni-completion-context-capf-table nil)

;; (defvar my-omni-company-map (make-sparse-keymap))
(defvar my-omni-completion-map
  (-let* ((context-menu-item
           `(menu-item
             "Context" nil
             :filter
             ,(lambda (_)
                (my-get-alist-by-value-or-mode
                 my-omni-completion-context-capf-table)))))
    (define-keymap

      "<tab>" context-menu-item "TAB" context-menu-item
      "C-\\" #'cape-tex ; shouldn't be used automatically because of conflicts in LaTeX mode
      "C-a" #'cape-abbrev
      "C-f" #'cape-file
      "C-l" #'my-balanced?-cape-line
      "C-s" #'cape-ispell
      "C-t" #'tempel-complete
      "C-w" #'cape-dict

      )))

;;;###autoload
(defun my-C-SPC-dwim (arg)
  (interactive "P")
  ;; WORKAROUND
  (when (and (bound-and-true-p lsp-mode) (not lsp-managed-mode))
    (lsp-managed-mode))
  (cond
   (arg
    ;; C-u C-SPC
    (set-mark-command arg))
   ((bound-and-true-p company-mode)
    (company-complete))
   (t
    (when (bound-and-true-p copilot-mode)
      (copilot-complete))
    (while-no-input
      (completion-at-point))
    (my-keep-messaging-until-next-command
     (my-describe-keymap my-omni-completion-map))
    (set-transient-map my-omni-completion-map))))

;;;###autoload
(progn
  (defun my-cape-lazy-make-capf (name form &optional properties docstring)
    "Apply FORM and return a function with NAME.
Actually, do not call MAKER (first of FORM) eagerly, just return a
function with NAME that will replace itself with the result when first
called. PROPERTIES is a plist that will be spliced and passed to
`cape-capf-properties'. DOCSTRING: for the generated CAPF functionn.

Example:

(my-cape-lazy-make-capf #'my-cape-company-yasnippet
 '(cape-company-to-capf 'company-yasnippet)
 '(:sort t :category yasnippet))
"
    (declare (indent defun))
    (let* ((props
            (or properties
                ;; `cape-wrap-super' add its own sorting metadata
                ;; https://github.com/minad/cape/issues/142, override that by
                ;; default so that sorters like `corfu-sort-function' can work
                '(:display-sort-function nil :cycle-sort-function nil)))
           (docstring
            (or docstring
                (format
                 "%S
Properties: %S"
                 form props))))
      (defalias name
        (lambda (&rest first-call-args)
          (interactive (list t))
          (defalias name
            (-->
             (eval form t)
             (apply #'cape-capf-properties it props)
             (cape-capf-interactive it))
            docstring)
          (funcall (if (called-interactively-p t)
                       #'my-apply-interactively
                     #'apply)
                   name first-call-args))
        (format "`my-cape-lazy-make-capf' %s" docstring)))
    name))

;; https://github.com/minad/corfu#transfer-completion-to-the-minibuffer
;;;###autoload
(defun my-corfu-move-to-minibuffer ()
  (interactive)
  (pcase completion-in-region--data
    (`(,beg ,end ,table ,pred ,extras)
     (dlet ((completion-extra-properties extras)
            completion-cycle-threshold
            completion-cycling)
       (funcall (default-value 'completion-in-region-function)
                beg
                end
                table
                pred)))))

(defvar-local my-complete-at-point-alt-styles
    (lambda () (default-value 'completion-styles))
  "See `my-complete-at-point-with-alt-styles'.")

;;;###autoload
(defun my-complete-at-point-with-alt-styles ()
  "See `my-complete-at-point-alt-styles'."
  (interactive)
  (dlet ((completion-styles (cond ((functionp my-complete-at-point-alt-styles)
                                   (funcall my-complete-at-point-alt-styles))
                                  (t my-complete-at-point-alt-styles))))
    (completion-at-point)))

;;;###autoload
(define-minor-mode my-tab-completion-mode nil
  :global nil
  :keymap `((,(kbd "TAB") . my-complete-at-point-with-alt-styles)
            (,(kbd "<tab>") . my-complete-at-point-with-alt-styles)))

;;;###autoload
(defun my-complete-with-capfs (&rest capfs)
  (dlet ((completion-at-point-functions capfs))
    (completion-at-point)))

;;;###autoload
(defun my-complete-in-minibuffer ()
  "With the completions in the minibuffer UI, call `completion-at-point'.
Also `my-extra-capfs-table'."
  (interactive)
  (dlet ((completion-in-region-function
          (default-value 'completion-in-region-function)))
    (my-with-extra-capfs (completion-at-point))))

(defvar my-menu-item-filter-complete-in-minibuffer--tab-keys
  (list (kbd "TAB") (kbd "<tab>")))

;;;###autoload
(defun my-complete-menu-item-filter-in-minibuffer (cmd)
  (and
   ;; (< 1 (length (this-command-keys-vector)))
   (not
    (member
     (this-command-keys) my-menu-item-filter-complete-in-minibuffer--tab-keys))
   cmd))

;; Doesn't let add annotations without internal symbol(s)
;;;###autoload
(defun my-cape-complete-include|import|require (&optional interact)
  (interactive (list t))
  (-let* ((lang (my-prog-lang-of-mode major-mode t))
          (source (file-name-concat my-emacs-conf-dir/ "lib/include" lang)))
    (dlet ((cape-line-buffer-function
            (lambda () (list (find-file-noselect source)))))
      (cape-line interact))))


(defun my-complete-line-from-files--lines-from-sources (source/s)
  (-let* ((sources (ensure-list source/s))
          (lines
           (-->
            (-mapcat
             (lambda (source)
               (-->
                (cond
                 ((listp source)
                  source)
                 ((bufferp source)
                  (with-current-buffer source
                    (buffer-string)))
                 (:else
                  (f-read-text source)))
                (split-string it "\n" t)))
             sources)
            (-uniq it))))
    lines))

;;;###autoload
(cl-defun my-complete-line-from-files (&optional interact &key src initial include-comment)
  "Complete non-comment lines from SRC.
SRC can be a single element or a list of any of the type: buffer,
string (filename), list of strings (directly supplied lines), it
defaults to a template of common includes/imports. INTERACT: perform
interactively using the default `completion-in-region-function' (usually
from the `minibuffer'), else perform as a CAPF. INITIAL: when used
interactively, insert it as `minibuffer''s initial prompt.
INCLUDE-COMMENT: by default, skip comment lines and use comment at the
end of lines as annotations; when non-nil, just treat all as completion
candidates."
  (interactive (append
                (list t)
                (if current-prefix-arg
                    (list
                     :src
                     (read-file-name "`my-complete-line-from-files' source: "))
                  '())))
  (save-match-data
    (-let* ((lang (my-prog-lang-of-mode major-mode t))
            (lang-major-mode (intern (format "%s-mode" lang)))
            (cmt-beg
             (or comment-start (my-eval-in-mode lang-major-mode 'comment-start)))
            (cmt-regexp
             (format "\s*%s.*" (regexp-quote (string-trim-right cmt-beg))))
            (cmt-regexp-beg (concat "^" cmt-regexp))
            ((beg . end)
             (or (bounds-of-thing-at-point 'symbol) (cons (point) (point))))
            (thing-at-pt (buffer-substring beg end))
            (lines
             (-->
              (or src (file-name-concat my-emacs-conf-dir/ "lib/include" lang))
              (my-complete-line-from-files--lines-from-sources it)
              (if include-comment
                  it
                (-remove
                 (lambda (line) (string-match cmt-regexp-beg line)) it))))
            (split-regexp (format "^\\(.*?\\)\\(%s\\)" cmt-regexp))
            (table
             (-map
              (lambda (line)
                (cond
                 ((and (not include-comment) (string-match split-regexp line))
                  (cons
                   (match-string 1 line)
                   (my-propertize-annotation (match-string 2 line))))
                 (:else
                  (cons line nil))))
              lines))
            (cands (-map #'car table))
            (annot-fn (lambda (cand) (cdr (assoc cand table)))))
      (cond
       ((not interact)
        (list beg end cands :annotation-function annot-fn))
       (:else
        ;; TODO: be a proper dynamic completion function, insert all matching or consistent multiple filter
        (condition-case _
            (-let*
                ((lines
                  (-->
                   (dlet
                       (
                        ;; (completion-styles `(substring ,@completion-styles)) ; no flex by default
                        (crm-separator "[ \t]*;[ \t]*") ; "," comma is quite common
                        )
                     (minibuffer-with-setup-hook
                         (lambda ()
                           (my-crm-setup-alternate-tab-to-insert-key))
                       (completing-read-multiple
                        "Complete: "
                        (lambda (string pred action)
                          (cond
                           ((eq action 'metadata)
                            `(metadata (annotation-function . ,annot-fn)))
                           (t
                            (complete-with-action action cands string pred))))
                        nil nil initial)))
                   (if src
                       it
                     (delete-dups it))))
                 (inserting (string-join lines "\n")))
              (delete-region beg end)
              (insert inserting))
          (quit
           ;; return string for `tempel' expansion as-is in case of quitting
           (or (and initial (string-trim-right initial)) thing-at-pt))))))))

;; (my-cape-lazy-make-capf
;;   'my-lsp+snippet-capf
;;   '(apply #'cape-capf-super
;;     (-map
;;      (lambda (capf)
;;        (lambda ()
;;          (ignore-errors
;;            (funcall capf))))
;;      '(lsp-completion-at-point
;;        eglot-completion-at-point tempel-complete))))

(provide 'my-functions-code-complete)

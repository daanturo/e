;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(when (not (fboundp #'yaml-mode))
  (add-to-list 'auto-mode-alist '("\\.ya?ml\\'" . yaml-ts-mode)))

;;; my-hook-yaml-mode-hook.el ends here

(provide 'my-hook-yaml-mode-hook)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(defun my-dictionary-with-auto-server-before-a (&rest _)
  (when (null dictionary-server)
    (condition-case nil
        (dictionary-open-server dictionary-server)
      (error (setq dictionary-server "dict.org")))))
(my-add-advice-once
  #'dictionary-check-connection
  :before #'my-dictionary-with-auto-server-before-a)


;;; after-dictionary.el ends here

(provide 'my-after-dictionary)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'straight)

;;;###autoload
(defun my-straight-delete-build ()
  (interactive)
  (let ((dir (read-directory-name "Delete: "
                                  (straight--build-dir))))
    (async-shell-command
     (read-shell-command "Execute: "
                         (format "rm -rf %s" dir)))))

;;;###autoload
(defun my-straight-add-all-builds-to-load-path ()
  (interactive)
  (my-abbreviate-load-paths)
  (-let* ((build-dir (straight--build-dir))
          (builds
           (-map
            #'abbreviate-file-name
            (directory-files build-dir
                             'full
                             directory-files-no-dot-files-regexp))))
    (setq load-path (-uniq (append load-path builds)))))

;;;###autoload
(defun my-straight-momentarily-checkout-default-branch-of-all-packages
    (&optional dry-run)
  (interactive (list t))
  (-let* ((dirs
           (-filter
            #'file-directory-p
            (directory-files (straight--repos-dir)
                             'full directory-files-no-dot-files-regexp))))
    (dolist (dir dirs)
      (my-vc-git-momentarily-checkout-default-branch dir 1 dry-run))))

(provide 'my-functions-straight)

;; Local Variables:
;; no-byte-compile: t
;; End:

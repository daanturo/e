;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'request)
(require 'eieio)

(defvar my-dict-dictionaryapi.dev-default-language-code "en")

(defclass
  my-dict-word ()
  ((word :initarg :word)
   (language :initarg :lang)
   (phonetic :initarg :phonetic)
   (definition :initarg :definition)
   (synonyms :initarg :synonyms)
   (countable :initarg :countable)
   (type :initarg :type)
   (antonyms :initarg :antonyms)))

(defun my-dict-dictionaryapi.dev-insert-org-table-row--data-to-text (data)
  (-let* ((word (map-nested-elt data [0 word])))
    (seq-mapcat
     (lambda (top-lv-obj)
       (-let* ((phonetic
                (-some-->
                    (map-nested-elt top-lv-obj [phonetic])
                  (string-trim it "/" "/")))
               (meaning-lst (map-nested-elt top-lv-obj [meanings])))
         (seq-mapcat
          (lambda (meaning)
            (-let* ((word-type
                     (capitalize (map-nested-elt meaning [partOfSpeech])))
                    (definition-lst (map-nested-elt meaning [definitions])))
              (-map
               (lambda (definition-obj)
                 (-let* ((definition
                          (map-nested-elt definition-obj [definition]))
                         (synonyms (map-nested-elt definition-obj [synonyms]))
                         (antonyms (map-nested-elt definition-obj [antonyms]))
                         (countable ""))
                   (my-dict-word
                    :word word
                    :phonetic phonetic
                    :definition definition
                    :synonyms synonyms
                    :antonyms antonyms
                    :countable countable
                    :type word-type)))
               definition-lst)))
          meaning-lst)))
     data)))

(defvar my-dict-dictionaryapi.dev-insert-org-table-row--last-data nil)

;;;###autoload
(cl-defun my-dict-dictionaryapi.dev-insert-org-table-row (word language &key audio)
  (interactive (list
                (read-string "dictionaryapi.dev : ")
                (cond
                 (current-prefix-arg
                  (read-string "Language code :"))
                 (:else
                  my-dict-dictionaryapi.dev-default-language-code))
                :audio t))
  (cl-assert (< 0 (length word)))
  (-let* ((pt0 (point-marker))
          (buf0 (current-buffer))
          (url
           (format "https://api.dictionaryapi.dev/api/v2/entries/%s/%s"
                   language
                   word)))
    (request
      url
      :parser #'my-json-parse-buffer
      :complete
      (cl-function
       (lambda (&key data &allow-other-keys)
         (setq my-dict-dictionaryapi.dev-insert-org-table-row--last-data data)))
      :error
      (cl-function
       (lambda (&rest args &key error-thrown &allow-other-keys)
         (message "`my-dict-dictionaryapi.dev-insert-org-table-row' error: %S"
                  error-thrown)))
      :success
      (cl-function
       (lambda (&key data &allow-other-keys)
         (-let* ((audio-urls
                  (-->
                   data
                   (seq-mapcat
                    (lambda (it)
                      (-->
                       it
                       (map-nested-elt it [phonetics])
                       (-map (lambda (it1) (map-nested-elt it1 [audio])) it)))
                    it)
                   (delete "" it)))
                 (rows
                  (-->
                   (my-dict-dictionaryapi.dev-insert-org-table-row--data-to-text
                    data)
                   (-keep
                    (lambda (obj)
                      (-->
                       (string-join (list
                                     (oref obj word)
                                     (oref obj phonetic)
                                     (oref obj definition)
                                     (string-join (oref obj synonyms) ", ")
                                     (oref obj type)
                                     (oref obj countable)
                                     (string-join (oref obj antonyms) ", "))
                                    " | ")
                       (format "| %s |" it)))
                    it))))
           (with-current-buffer buf0
             (goto-char pt0)
             (save-excursion (insert (string-join rows "\n")))
             (ignore-errors
               (org-table-align))
             (when audio
               (-let* ((shell-cmd
                        (-->
                         audio-urls
                         (-map
                          (lambda (url) (format "mpv %s" url)) it)
                         (string-join it " ; ")))
                       (name
                        (format
                         "*%s audio %s*"
                         'my-dict-dictionaryapi.dev-insert-org-table-row word))
                       (buf (get-buffer-create name)))
                 (message "`my-dict-dictionaryapi.dev-insert-org-table-row': play with command: %s" shell-cmd)
                 (start-process "" buf "sh" "-c" shell-cmd))))))))))

(defvar my-dict-add-word-to-file-file-path nil)

;;;###autoload
(defun my-dict-add-word-to-file ()
  (declare (interactive-only t))
  (interactive)
  (cl-assert my-dict-add-word-to-file-file-path)
  (find-file my-dict-add-word-to-file-file-path)
  (goto-char (point-max))
  (call-interactively #'my-dict-dictionaryapi.dev-insert-org-table-row))

;;;###autoload
(define-minor-mode my-dictionary-record-file-mode
  nil
  :global nil
  :keymap
  `((,(kbd "C-+") .
     (menu-item
      "" nil
      :filter
      (lambda (_)
        (cond
         ((derived-mode-p '(org-mode))
          #'my-dict-dictionaryapi.dev-insert-org-table-row)
         (:else
          nil)))))))

;;; my-functions-dictionary.el ends here

(provide 'my-functions-dictionary)

;; -*- lexical-binding: t; -*-

(require '00-my-core-macros)

;; (defun my-racket-adjust-overlay-a (func &rest args)
;;   (-let* ((ovs (apply func args))
;;           ((ov0 . ov&) ovs))
;;     (print (overlay-properties ov0))
;;     (print ov0)
;;     (overlay-put ov0 'after-string
;;                  (--> (overlay-get ov0 'after-string)
;;                       (string-trim it)))
;;     ovs))

;;;###autoload
(defun my-racket-eval-string (string &optional _beg show-pos)
  "Evaluate STRING.
Display the result at SHOW-POS (default to current point)."
  (racket--cmd/async
   (racket--repl-session-id)
   `(eval ,string)
   (lambda (result)

     ;; Try not to place the evaluation result at the next line

     ;; (my-with-advice #'racket-show-pseudo-tooltip :around #'my-racket-adjust-overlay-a
     ;;   ;; see `racket--make-pseudo-tooltip-overlays'
     ;;   (dlet ((text-scale-mode t) (text-scale-mode-amount 1))))

     (racket-show (format "%s" result)
                  (or show-pos (point))
                  t))))

;;;###autoload
(defun my-racket-eval-region (beg end)
  (interactive "r")
  (my-racket-eval-string (buffer-substring-no-properties beg end)
                         beg end))

;;;###autoload
(defun my-racket-mode-hook-h (&rest _)
  "."
  ;; `racket-send-definition': "Wrong type argument: processp, nil"?
  (when (local-variable-p 'completion-at-point-functions)
    (add-to-list 'completion-at-point-functions t 'append))
  (setq-local eldoc-documentation-function
              (default-value 'eldoc-documentation-function)))

(my-add-mode-hook-and-now 'racket-mode #'my-racket-mode-hook-h)

(my-evalu-set-handler
 'racket-mode
 #'racket-repl
 :region #'my-racket-eval-region)

(provide 'my-hook-racket-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

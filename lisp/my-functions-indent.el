;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (require 'indent nil 'noerror)

;;;###autoload
(defun my-find-next-position-with-same-indent-as-current-line (&optional init-point back)
  (declare (side-effect-free t))
  (save-excursion
    (when init-point (goto-char init-point))
    (let* ((ind (my-line-non-whitespace-beg-position))
           (colu0 (current-column))
           (line0 (line-number-at-pos))
           (ind-col (my-column-at-position ind)))
      (cl-block nil
        (while (not (if back (bobp) (eobp)))
          (forward-line (if back -1 1))
          (back-to-indentation)
          (let ((line (line-number-at-pos))
                ;; columns are 0-based
                (found-point (+ (line-beginning-position) colu0)))
            (when (and (= ind-col (current-column))
                       (/= line0 (line-number-at-pos))
                       (= line (line-number-at-pos found-point)))
              (cl-return found-point))))))))

;;;###autoload
(defun my-find-next-position-with-same-indent-as-current-line-in-defun (&optional init-point back)
  (-let* (((beg . end) (bounds-of-thing-at-point 'defun))
          (found (my-find-next-position-with-same-indent-as-current-line
                  init-point back)))
    (if (and beg end found
             (<= beg found end))
        found
      nil)))

;;;###autoload
(defun my-decrease-indentation (beg end Δindent)
  (let ((beg (or beg (point-min)))
        (end (or end (point-max))))
    (save-excursion
      (save-match-data
        (let ((Δindent (or Δindent
                           (my-find-minimum-indentation-of-buffer))))
          (goto-char beg)
          (while (and (< (point) end)
                      (not (eobp)))
            (when (looking-at (format "[ \t]\\{1,%d\\}" Δindent))
              (delete-region (match-beginning 0)
                             (match-end 0)))
            (forward-line 1)))))))

;;;###autoload
(defun my-increase-indentation (beg end Δindent)
  (let ((beg (or beg (point-min)))
        (end (or end (point-max))))
    (save-excursion
      (goto-char beg)
      (while (and (< (point) end)
                  (not (eobp)))
        (insert-char (string-to-char " ") Δindent)
        (forward-line 1)))))

(defun my-indent-shift--get-region (&optional beg end)
  (cond
   ((or (use-region-p) beg end)
    ;; `indent-rigidly' performs weird when region doesn't cover line beg & end
    (list
     (save-excursion
       (goto-char (or beg (region-beginning)))
       (line-beginning-position))
     (save-excursion
       (goto-char (or end (region-end)))
       (line-end-position))))
   (:else
    (list (line-beginning-position) (line-end-position)))))

;;;###autoload
(defun my-indent-shift-right (beg end &optional left)
  "+Indent region between BEG and END.
LEFT: dedent (outdent) instead."
  (interactive (append (my-indent-shift--get-region)))
  (let*
      ((indent-unit (max 2 (/ tab-width 2)))
       (column
        (my-column-at-position
         (my-line-non-whitespace-beg-position (point))))
       (remainder (% column indent-unit))
       ;; that to reach a column that is divisible by the indent unit
       (amount
        (cond
         ((and (= 0 remainder) left)
          (- indent-unit))
         ((and (= 0 remainder) (not left))
          indent-unit)
         (left
          (- remainder))
         ((not left)
          (- indent-unit remainder)))))
    (indent-rigidly beg end amount))
  (setq deactivate-mark nil))

;;;###autoload
(defun my-indent-shift-left|outdent (beg end &optional right)
  "(`my-indent-shift-right' BEG END (not RIGHT))."
  (interactive (append
                (my-indent-shift--get-region)
                (list current-prefix-arg)))
  (my-indent-shift-right beg end
                         (if right
                             nil
                           'left)))

;;;###autoload
(defun my-indent-same-as-previous-non-empty-line ()
  (-let* ((ws
           (save-excursion
             (previous-line 1)
             (beginning-of-line)
             (save-match-data
               (while (and (not (bobp)) (looking-at "[ \t]*$"))
                 (previous-line 1)
                 (beginning-of-line))
               (-->
                (s-match
                 "[ \t]*" (buffer-substring (point) (pos-eol)))
                (nth 0 it))))))
    (when ws
      (save-excursion
        (beginning-of-line)
        (delete-horizontal-space)
        (insert ws)))))



;;; my-functions-indent.el ends here

(provide 'my-functions-indent)

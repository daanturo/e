;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-shannon-max-stop-logger ()
  (interactive)
  (remove-hook 'post-command-hook 'shannon-max-logger-log)
  (cancel-timer shannon-logger-timer))

;;;###autoload
(defun my-shannon-max-start-logger ()
  (interactive)
  (shannon-max-start-logger))


;;; my-functions-shannon-max.el ends here

(provide 'my-functions-shannon-max)

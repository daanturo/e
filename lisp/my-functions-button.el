;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-push-button-with-saved-column ()
  (interactive)
  (my-save-column
    (call-interactively #'push-button)))

;;;###autoload
(defun my-button-at-point ()
  (declare (obsolete nil nil))
  (button-at (point)))

;;;###autoload
(defun my-get-button-actions-&-data-at-point ()
  (-let* ((btn (button-at (point))))
    (vector
     (button-get btn 'mouse-action)
     (button-get btn 'action)
     (button-get btn 'button-data))))

;;; my-functions-button.el ends here

(provide 'my-functions-button)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-prog-lang-of-mode (mode &optional keep-dashes)
  "Get the corresponding programming language of MODE.
Support `inferior-.*(-ts)?-mode' modes. KEEP-DASHES: keep\"-\"s,
else replace with spaces."
  (-->
   (format "%s" mode)
   (string-remove-prefix "inferior-" it)
   (replace-regexp-in-string "\\(-ts\\)?-mode$" "" it)
   (if keep-dashes
       it
     (string-replace "-" " " it))))

;;;###autoload
(defun my-guess-prog-lang ()
  (-some-->
      (or (my-safe-call #'language-id-buffer) (my-prog-lang-of-mode major-mode))
    (cond
     ((member it '("JSX" "TSX"))
      "JavaScript")
     (:else
      it))))


;;; my-functions-programming-languages.el ends here

(provide 'my-functions-programming-languages)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(cl-defun my-markdown-quote-string-with-> (str &optional (level 1))
  (-let* ((pre (concat (my-multiply-string level ">") " ")))
    (--> (split-string str "\n")
         (my-for [line it]
           (concat pre line))
         (string-join it "\n"))))

;;;###autoload
(defun my-markddown-paste-quoted-string-with-> (level)
  (interactive "p")
  (insert (my-markdown-quote-string-with-> (current-kill 0)
                                           level)))

(defvar-local my-markdown-bounds-of-inner-code-block-at-point--last-language nil)
;;;###autoload
(defun my-markdown-bounds-of-inner-code-block-at-point ()
  (-let* ((bounds (my-bounds-by-regexps "^```\\(.*?\\)\n" "\n```$"))
          (lang (match-string 1)))
    (setq my-markdown-bounds-of-inner-code-block-at-point--last-language
          (pcase lang
            ("elisp" "emacs-lisp")
            (_ lang)))
    bounds))

;;;###autoload
(defun my-markdown-mark-inner-code-block-at-point ()
  (interactive)
  (my-mark-bounds-as-cons-maybe
   (my-markdown-bounds-of-inner-code-block-at-point)))

;;;###autoload
(defun my-markdown-edit-indirection-code-block ()
  (interactive)
  (-when-let* (((beg . end) (my-markdown-bounds-of-inner-code-block-at-point)))
    (dlet
        ((edit-indirect-guess-mode-function
          (-if-let*
              ((lang
                my-markdown-bounds-of-inner-code-block-at-point--last-language)
               (mode (intern (format "%s-mode" lang))))
              (lambda (&rest _) (funcall mode))
            edit-indirect-guess-mode-function)))
      (edit-indirect-region beg end 'display-buffer))))

;;;###autoload
(defun my-markdown-insert-link (&optional tooltip-prompt)
  (interactive "P")
  (dlet ((markdown-disable-tooltip-prompt (not tooltip-prompt)))
    (markdown-insert-link)))

;;;###autoload
(defun my-markdown-make-region-line-spoiler (beg end)
  (interactive (and (use-region-p) (list (region-beginning) (region-end))))
  (save-excursion
    (goto-char end)
    (insert "!<")
    (goto-char beg)
    (insert ">!")))

;;;###autoload
(defun my-markdown-live-preview ()
  (interactive)
  (markdown-live-preview-mode t))


(provide 'my-functions-markdown)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(setq keycast-log-newest-first nil)

(provide 'my-after-keycast)

;; Local Variables:
;; no-byte-compile: t
;; End:

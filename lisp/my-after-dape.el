;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(set-popup-rule! "^\\*dape-" :ignore t)
(set-popup-rule! "^\\*dape-repl\\*$" :ignore nil)

;; (dape-breakpoint-load)
;; (add-hook 'kill-emacs-hook #'dape-breakpoint-save)
;; (advice-add
;;  #'dape-breakpoint-save
;;  :around (my-make-with-dlet-variables-advice '((save-silently t))))
;; ;; Don't remove breakpoints when killing buffers, TODO: remove when
;; ;; https://github.com/svaante/dape/issues/133
;; (advice-add #'dape--breakpoint-buffer-kill-hook :override #'ignore)
;; ;; TODO: remove when fixed
;; (advice-add
;;  #'dape-breakpoint-save
;;  :around #'my-dape-breakpoint-save--kill-open-buffer-a)
;; (add-hook 'my-refresh-find|open-current-file-after-hook #'my-dape-breakpoint-load-non-sync-h)

;;; my-after-dape.el ends here

(provide 'my-after-dape)

;; Local Variables:
;; no-byte-compile: t
;; End:

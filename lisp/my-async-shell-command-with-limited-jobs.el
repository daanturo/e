;; -*- lexical-binding: t; -*-

;;;###autoload
(defun my-sensible-number-of-async-jobs ()
  (max 1 (1- (ceiling (/ (num-processors) 2)))))

(defvar my-async-shell-command-with-limited-jobs--queue '())
(defvar my-async-shell-command-with-limited-jobs--running-count 0)
;; (defvar my-async-shell-command-with-limited-jobs--queue-mutex
;;   (make-mutex (symbol-name 'my-async-shell-command-with-limited-jobs--queue-mutex)))
;; (defvar my-async-shell-command-with-limited-jobs--running-count-mutex
;;   (make-mutex (symbol-name 'my-async-shell-command-with-limited-jobs--running-count-mutex)))

(defun my-async-shell-command-with-limited-jobs--now (start-func finish-func buf dir)
  (progn ;with-mutex my-async-shell-command-with-limited-jobs--running-count-mutex
    (cl-incf my-async-shell-command-with-limited-jobs--running-count))
  (dlet ((default-directory dir))
    (async-shell-command (funcall start-func) buf)
    (set-process-sentinel
     (get-buffer-process buf)
     (lambda (proc event)
       (dlet ((debug-on-error nil))
         (with-demoted-errors "%S"
           (funcall finish-func proc event)))
       (my-async-shell-command-with-limited-jobs--execute-next)))))

(defun my-async-shell-command-with-limited-jobs--execute-next ()
  (progn ;with-mutex my-async-shell-command-with-limited-jobs--running-count-mutex
    (cl-decf my-async-shell-command-with-limited-jobs--running-count))
  (when (length> my-async-shell-command-with-limited-jobs--queue 0)
    (cl-destructuring-bind (start-func finish-func buf dir)
        (progn ;with-mutex my-async-shell-command-with-limited-jobs--queue-mutex
          (pop my-async-shell-command-with-limited-jobs--queue))
      (my-async-shell-command-with-limited-jobs--now start-func finish-func buf dir))))

;;;###autoload
(defun my-async-shell-command-with-limited-jobs (start-func finish-func buf dir)
  "Asynchronously run shell command with a limited number of ran jobs.
Run START-FUNC's shell command with output to BUF, then evaluate
FINISH-FUNC when it finishes (in directory DIR).

START-FUNC must be able accept 0 arguments and return a shell
command as string to be executed.

FINISH-FUNC must adhere to `set-process-sentinel's requirements."
  (if noninteractive
      (error "Emacs batch mode doesn't support `set-process-sentinel'!.")
    (if (< my-async-shell-command-with-limited-jobs--running-count
           (my-sensible-number-of-async-jobs))
        ;; nil
        ;; If the threshold isn't reached, just run now
        (my-async-shell-command-with-limited-jobs--now start-func finish-func buf dir)
      ;; Else add to the queue
      (progn   ;with-mutex my-async-shell-command-with-limited-jobs--queue-mutex
        (setq my-async-shell-command-with-limited-jobs--queue
              `(,@my-async-shell-command-with-limited-jobs--queue
                ,(list start-func finish-func buf dir)))))))

(provide 'my-async-shell-command-with-limited-jobs)

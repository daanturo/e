;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(defvar my-online-check-faces
  '(
    ;;

    ;; flymake
    flymake-error
    flymake-warning flymake-note

    ;; jinx
    jinx-misspelled

    ;;
    ))

(defun my-set-face-attribute (face frame &rest args)
  (when (facep face)
    (apply #'set-face-attribute face frame args)))

(defvar my-online-check-non-intrusive-mode-faces-orig nil)
;;;###autoload
(define-minor-mode my-online-check-non-intrusive-mode
  nil
  :global t
  (if my-online-check-non-intrusive-mode
      (progn
        (unless my-online-check-non-intrusive-mode-faces-orig
          (setq my-online-check-non-intrusive-mode-faces-orig
                (my-for
                  [face my-online-check-faces]
                  (list face (face-attribute face :underline)))))
        (dolist (face my-online-check-faces)
          (my-set-face-attribute face nil :underline nil)))
    (progn
      (cl-loop
       for
       (face udl)
       in
       my-online-check-non-intrusive-mode-faces-orig
       do
       (my-set-face-attribute face nil :underline udl)))))


;;; my-functions-online-check.el ends here

(provide 'my-functions-online-check)

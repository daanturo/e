;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-lang-tex-apply-latex-template ()
  "Copy LaTeX template's images/ and insert template at point ."
  (interactive)
  (let ((template-dir "~/Documents/courses/latex-template/"))
    (unless (file-directory-p (file-name-concat default-directory "images"))
      (copy-directory (file-name-concat template-dir "images") "./"))
    (insert-file-contents
     (completing-read
      "" (directory-files template-dir t (regexp-opt '(".tex" ".org")))))))

;;;###autoload
(defun my-lang-tex-insert-and-copy-LaTeX ()
  (interactive)
  (insert "LaTeX")
  (kill-new "LaTeX"))

;;;###autoload
(defun my-lang-tex-latex-directory ()
  (cond
   ((-let* ((pr (my-project-root)))
      (and (my-directory-has-matching-files-p pr "\\.tex\\'")
           pr)))
   (t default-directory)))

;;;###autoload
(defun my-lang-tex-make-latex-generate-command ()
  (concat my-latexmk-base-command " "
          (and (or
                (my-directory-has-matching-files-p default-directory "\\.pdf\\'")
                (equal ".pdf" tex-print-file-extension))
               "-pdf ")
          (and (and
                (my-directory-has-matching-files-p default-directory "\\.dvi\\'")
                (equal ".dvi" tex-print-file-extension))
               "-dvi ")
          (my-lang-tex-get-latexmk-engine-flag)))

(defvar-local my-lang-tex-make-latex--local-command nil)
;;;###autoload
(defun my-lang-tex-make-latex (&optional ask-interact)
  (interactive "P")
  (dlet ((default-directory (my-lang-tex-latex-directory)))
    (-let* ((cmd
             (my-confirm-shell-command
               ask-interact
               (or my-lang-tex-make-latex--local-command
                   (my-lang-tex-make-latex-generate-command)))))
      (when ask-interact
        (setq my-lang-tex-make-latex--local-command cmd))
      (message "%s" cmd)
      (my-process-async
       cmd
       (lambda (stdout &rest _)
         "Print the last line of the output as notification."
         (message "%S\n%s"
                  (vector #'my-lang-tex-make-latex cmd)
                  (replace-regexp-in-string
                   "[^z-a]*\n" "" (string-trim-right stdout))))))))

(defun my-lang-tex-get-latexmk-engine-flag (&optional engine)
  ;; (--> (or engine TeX-engine)
  ;;      (assoc it (TeX-engine-alist))
  ;;      (nth 3 it)
  ;;      (if (symbolp it) (symbol-value it) it)
  ;;      (car (split-string it))
  ;;      (format "-%s" it))
  (pcase TeX-engine
    ('xetex "-xelatex")
    ('luatex "-lualatex")
    (_ "")))

(defvar my-lang-tex-image-to-generate-latex-fragment-hugggingface-model nil)

;;;###autoload
(defun my-lang-tex-clipboard-image-to-generate-latex-fragment ()
  (interactive)
  ;; (cl-assert my-lang-tex-image-to-generate-latex-fragment-hugggingface-model)
  (-let* ((bin (file-name-concat my-emacs-bin-directory/ "llm-api-google"))
          (time0 (current-time)))
    (my-process-async
     (list
      bin
      my-llm-google-default-model
      "Latex code of this. Only raw code without formatting."
      "--clipboard-image")
     (lambda (stdout obj &rest _)
       (-let* (((&plist :exit exit :err err) obj))
         (cond
          ((= 0 exit)
           (message
            "`my-lang-tex-clipboard-image-to-generate-latex-fragment': %s"
            stdout)
           (when (< 5.0 (time-to-seconds (time-since time0)))
             (notifications-notify
              :body "my-lang-tex-clipboard-image-to-generate-latex-fragment finished!"))
           (kill-new stdout))
          (:else
           (message
            "`my-lang-tex-clipboard-image-to-generate-latex-fragment': error %s"
            err)))))
     :trim t)))

;;;###autoload
(defun my-org-latex-post-process ()
  (interactive)
  (my-regexp-replace-in-buffer "^\\\\label{sec:org[a-z0-9]+}$" ""))

;;;###autoload
(defun my-lang-tex-keep-body-only ()
  (interactive)
  (my-org-latex-post-process)
  (my-regexp-replace-in-buffer
   (rx-to-string
    `(seq
      bos
      (* anychar)
      "\n"
      "\\begin{document}"
      "\n"
      (?  (* anychar) "\n" "\\tableofcontents" "\n")
      (group-n 1 (* anychar))
      "\n"
      "\\end{document}"
      "\n"
      (* anychar)
      eos))
   "\\1"))

;;; my-functions-lang-tex.el ends here

(provide 'my-functions-lang-tex)


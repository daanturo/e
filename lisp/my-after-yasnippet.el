;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; if `yasnippet' has been loaded prematurely
(when (and (not noninteractive) (not after-init-time))
  (dlet ((backtrace-line-length nil))
    (backtrace)))

;; (my-backtrace)

;; (advice-add
;;  #'yas--modes-to-activate
;;  :around #'my-yas--modes-to-activate--treesit-modes-a)

(defvar my-yasnippet-dir
  (file-name-concat my-emacs-conf-dir/ "snippets-yasnippet"))

(add-to-list 'yas-snippet-dirs 'my-yasnippet-dir)

(defvar my-yasnippet-treesit-placeholder-snippet-dir
  (file-name-concat my-user-emacs-local-dir/ "treesit-yassnippet"))

(add-to-list 'yas-snippet-dirs 'my-yasnippet-treesit-placeholder-snippet-dir
             'append)

(unless (file-exists-p my-yasnippet-treesit-placeholder-snippet-dir)
  (cl-loop
   for
   mamode
   being
   the
   symbols
   for
   name
   =
   (symbol-name mamode)
   when
   (and name (commandp mamode) (string-match-p "-ts-mode$" name))
   do
   (-let* ((yas-parent-file
            (file-name-concat my-yasnippet-treesit-placeholder-snippet-dir
                              name ".yas-parents")))
     (message "%s" yas-parent-file)
     (make-directory (file-name-parent-directory yas-parent-file) 'parents)
     (f-write
      (replace-regexp-in-string "-ts-mode$" "-mode" name)
      'utf-8
      yas-parent-file))))

(require 'doom-snippets nil t)

(advice-add
 #'yas--templates-for-key-at-point
 :around #'my-yas--templates-for-key-at-point--force-distinct-a)

;; make similar to `tempel-map'
(my-bind :map 'yas-keymap "M-<down>" #'yas-next-field)
(my-bind :map 'yas-keymap "M-<up>" #'yas-prev-field)
(my-bind :map 'yas-keymap '("<escape>") #'my-yas-exit-all-snippets) ; `yas-abort-snippet' sometimes is no-op?
;; (my-bind :map 'yas-keymap '("TAB" "<tab>") nil)



;;; after-yasnippet.el ends here

(provide 'my-after-yasnippet)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar-local my-exchange-transpose-swap-regions--overlay nil)

(defcustom my-exchange-transpose-swap-regions-face 'highlight
  nil
  :group 'my-exchange-transpose-swap-regions
  :type 'face)

;;;###autoload
(defun my-exchange-transpose-swap-regions-do (&optional cur-beg cur-end)
  (interactive)
  (-when-let* ((_ (use-region-p))
               (cur-beg (or cur-beg (region-beginning)))
               (cur-end (or cur-end (region-end))))
    (-let* ((old-ov (overlayp my-exchange-transpose-swap-regions--overlay))
            (old-beg
             (and old-ov
                  (overlay-start my-exchange-transpose-swap-regions--overlay)))
            (old-end
             (and old-ov
                  (overlay-end my-exchange-transpose-swap-regions--overlay))))
      (cond
       ((and (null my-exchange-transpose-swap-regions--overlay) cur-beg cur-end)
        (setq-local my-exchange-transpose-swap-regions--overlay
                    (make-overlay cur-beg cur-end))
        (overlay-put
         my-exchange-transpose-swap-regions--overlay
         'face
         `((t (:inherit ,my-exchange-transpose-swap-regions-face))))
        (deactivate-mark))
       ((and my-exchange-transpose-swap-regions--overlay
             (or (not cur-beg)
                 (not cur-end)
                 (and (= cur-beg old-beg) (= cur-end old-end))))
        (my-exchange-transpose-swap-regions-cancel))
       ((and my-exchange-transpose-swap-regions--overlay cur-beg cur-end)
        (deactivate-mark)
        (transpose-regions old-beg old-end cur-beg cur-end)
        (my-exchange-transpose-swap-regions-cancel))))))

;;;###autoload
(defun my-exchange-transpose-swap-regions-cancel ()
  (interactive)
  (when (overlayp my-exchange-transpose-swap-regions--overlay)
    (delete-overlay my-exchange-transpose-swap-regions--overlay)
    (setq-local my-exchange-transpose-swap-regions--overlay nil)))

;;;###autoload
(defun my-exchange-transpose-swap-regions (&optional cancel)
  (interactive "P")
  (if cancel
      (my-exchange-transpose-swap-regions-cancel)
    (my-exchange-transpose-swap-regions-do)))

;;;###autoload
(defun my-exchange-transpose-swap-regions-menu-item-filter-for-M-t-key (_)
  (cond
   ((not (use-region-p))
    #'transpose-words)
   ((<= (prefix-numeric-value prefix-arg) 0)
    #'my-exchange-transpose-swap-regions-cancel)
   (t
    #'my-exchange-transpose-swap-regions)))

(provide 'my-exchange-transpose-swap-regions)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(my-autoload-functions 'lispy '(lispy--normalize-1))

;;;###autoload
(defun my-lispy-normalize (&optional whole-defun)
  "WARNING: `lispy--normalize-1' seems buggy.
Use `lispy' to prettify the upper sexp.
With WHOLE-DEFUN, apply at the beginning there."
  (interactive "P")
  (unless (my-point-in-comment-p)
    (my-save-line-and-column
      (my-backward-up-string)
      (with-demoted-errors "%S"
        (dlet ((current-prefix-arg nil))
          (call-interactively
           (if whole-defun
               #'beginning-of-defun
             #'backward-up-list))))
      (lispy--normalize-1))))

;;;###autoload
(defun my-lispy-parens-parinfer-auto-wrap (&optional arg)
  (interactive "P")
  (lispy-parens-auto-wrap (or arg 0)))

;;;###autoload
(defun my-lispy-wrap-try-not-edit-before-point-a (fn &rest args)
  (-let* ((pt0 (point))
          ;; (stppss-obj (syntax-ppss))
          ;; (paren-depth-0 (ppss-depth stppss-obj))
          (pt-1 (max (point-min) (- pt0 1)))
          (pt+1 (min (point-max) (+ pt0 1)))
          (spaces
           (or (-some--> (char-before) matching-paren)
               (string-match-p
                "[[:space:]\n]" (buffer-substring-no-properties pt-1 pt+1)))))
    ;; break the current symbol that lispy's wrapping won't including the quote
    ;; and/or the part of symbol name before the point
    (insert " ")
    (prog1 (dlet ((lispy-insert-space-after-wrap
                   (and lispy-insert-space-after-wrap spaces)))
             (cond
              ((get-text-property (pos-bol) 'read-only)
               ;; (apply #'derived-mode-p my-lisp-repl-modes)
               ;; (wrong-type-argument wholenump N)
               (my-with-advice
                 #'indent-sexp
                 :override #'ignore (apply fn args)))
              (:else
               (apply fn args))))
      (when (equal " " (buffer-substring-no-properties pt0 pt+1))
        (delete-region pt0 pt+1)))))

(provide 'my-functions-lispy)

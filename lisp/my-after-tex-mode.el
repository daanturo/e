;; -*- lexical-binding: t; -*-

(require '00-my-core-macros)

;;; Built-in tex-mode

(my-add-mode-hook-and-now 'latex-mode #'reftex-mode)

;;; Auctex

;;;###autoload
(my-accumulate-config-collect
 (add-hook 'LaTeX-mode-hook #'LaTeX-math-mode))

;; (setq! TeX-clean-confirm nil)
(setq! TeX-auto-save t)
(setq! TeX-parse-self t)
(setq! TeX-master nil)
;; ;; Enable synctex correlation. From Okular just press Shift + Left click to go to the good line.
;; (setq! TeX-source-correlate-mode t)
;; (setq! TeX-source-correlate-start-server t)

;; ;; Update PDF buffers after successful LaTeX runs.
;; (with-eval-after-load 'auctex
;;   (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer))

(with-eval-after-load 'tex
  ;; One of '(LaTeX-command-style TeX-command-extra-options) may be customized
  (unless (string-search "-shell-escape" TeX-command-extra-options)
    (setq! TeX-command-extra-options (concat TeX-command-extra-options " -shell-escape")))
  ;; (let ((command (cadr (assoc "" LaTeX-command-style))))
  ;;   (unless (string-match-p "-shell-escape" command)
  ;;     (setcdr (assoc "" LaTeX-command-style)
  ;;             (list (replace-regexp-in-string
  ;;                    (rx (group string-start (* (not space))))
  ;;                    (concat "\\1" " -shell-escape")
  ;;                    command)))))
  (my-add-list!
    'TeX-expand-list
    `(("%(my-default-latex)" ,(lambda ()
                                (dlet ((TeX-engine 'default))
                                  (TeX-command-expand "%l"))))
      ("%(my-luatex)" ,(lambda ()
                         (dlet ((TeX-engine 'luatex))
                           (TeX-command-expand "%l"))))))
  (my-add-list!
    'TeX-command-list
    `(("Default LaTeX -shell-escape"
       "%`%(my-default-latex)%(mode)%' -shell-escape %T"
       TeX-run-TeX
       nil
       (latex-mode doctex-mode))
      ("LuaLaTeX -shell-escape"
       "%`%(my-luatex)%(mode)%' -shell-escape %T"
       TeX-run-TeX
       nil
       (latex-mode doctex-mode))))
  (setq-default TeX-command-default "LuaLaTeX -shell-escape")
  (setq-default TeX-command-Show "LuaLaTeX -shell-escape")
  (setq-default TeX-engine 'luatex)
  (setq TeX-clean-confirm nil))

;; NOTE when using LuaLaTex/Xetex, don't include packages that support Languages
;; other than English

;; Because of the sidebar, the area for displaying isn't comfy enough
(my-after-each '(emacs preview)
  (setq-default
   preview-scale-function (lambda ()
                            (* (my-current-text-scale)
                               0.75))
   preview-scale nil))

;; (my-bind :vi '(normal visual) ", , m" #'my-preview-latex-math)

;; (my-lookup-key-in-all-maps "<normal-state> , ,")

(my-bind :map '(latex-mode-map LaTeX-mode-map bibtex-mode-map) "<f5>" #'my-lang-tex-make-latex)
(my-accumulate-config-collect
 (my-bind :map 'org-mode-map "<f5>" #'my-org-export-to-pdf-async))

;; Don't signal errors when called by `expreg--paragraph-defun'
(my-add-advice/s
  '(LaTeX-find-matching-begin LaTeX-find-matching-end)
  :around '(my-with-demoted-errors-a))

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'my-after-tex-mode)

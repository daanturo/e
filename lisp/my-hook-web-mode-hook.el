;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'web-mode)

;; ;; `web-mode' didn't correctly recognize some comments with tags inside them
;; (my-delete-in-list! 'auto-mode-alist
;;   (seq-find
;;    (my-fn1 (r . m)
;;      (and (string-match-p "html" r)
;;           (equal 'web-mode m)))
;;    auto-mode-alist))

;; Auto-close tags after typing ">"
(setq web-mode-auto-close-style 2)

;; `web-mode-mark-and-expand' doesn't highlight region after called
;; programatically
(advice-add #'er/add-web-mode-expansions :override #'ignore)

;; (advice-add #'web-mode-mark-and-expand :after #'my-no-deactivate-mark-keep-region-after-a)

;; ;; expand, don't comment (configured by Doom)
;; (my-bind :map 'web-mode-map "M-/" nil)

(my-bind :map 'web-mode-map "M-;" nil)

;; (setf (alist-get "javascript" (default-value 'web-mode-comment-formats) nil nil #'equal)
;;       "//")

(my-set-alist! 'web-mode-comment-formats "javascript" "//" :global t)
(my-set-alist! 'web-mode-comment-formats "jsx" "//" :global t)

(add-hook
 'my-expreg-before-first-expand-in-buffer-hook
 (lambda ()
   (when (and (derived-mode-p 'web-mode)
              (string-suffix-p ".html" buffer-file-name t))
     (my-treesit-ensure-parser-in-buffer 'html))))

;;; my-hook-web-mode-hook.el ends here

(provide 'my-hook-web-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

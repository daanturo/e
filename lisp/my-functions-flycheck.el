;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-quiet-flycheck-report-buffer-checker-status ()
  (interactive)
  (advice-add
   #'flycheck-report-buffer-checker-status
   :around (my-make-with-dlet-variables-advice '((inhibit-message t)))))


;;; my-functions-flycheck.el ends here

(provide 'my-functions-flycheck)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (cl-assert (executable-find "wl-copy"))
;; (cl-assert (executable-find "wl-paste"))

;;;###autoload
(defun my-clipboard-kill-ring-copy (arg)
  (interactive (list
                (if (use-region-p)
                    (buffer-substring (region-beginning) (region-end))
                  (read-string "Copy: "))))
  (kill-new
   (cond
    ((stringp arg)
     arg)
    (t
     (format "%s" arg)))))

(defvar my-functions-clipboard--file
  (abbreviate-file-name (or load-file-name buffer-file-name)))

(defun my-clipboard--error-display-server ()
  (user-error "%s: Unsupported display server." my-functions-clipboard--file))

;;;###autoload
(defun my-clipboard-paste-types ()
  (cond
   ((my-display-wayland-p)
    (process-lines "wl-paste" "--list-types"))
   (t
    (my-clipboard--error-display-server))))

;;;###autoload
(cl-defun my-clipboard-get (&key options mime-type file-out)
  "Compatible with `interprogram-paste-function'.
Return the clipboard content, unless specified by FILE-OUT.
OPTIONS (a list of strings) is passed to the underlying command.
MIME-TYPE: see \"wl-copy\"'s usage."
  (cond
   ((my-display-wayland-p)
    (with-temp-buffer
      (apply #'call-process
             `("wl-paste"
               nil
               ,(cond
                 (file-out
                  (list :file file-out))
                 (:else
                  t))
               nil
               "--no-newline"
               ,@options
               ,@(and mime-type (list "--type" (format "%s" mime-type)))))
      (buffer-string)))
   (t
    (my-clipboard--error-display-server))))

;;;###autoload
(defun my-clipboard-paste (&optional mime-type)
  (interactive (list
                (and current-prefix-arg
                     (completing-read
                      "MIME mime-type to paste: " (my-clipboard-paste-types)))))
  (insert (my-clipboard-get :mime-type mime-type)))

;;;###autoload
(cl-defun my-clipboard-copy (text &key mime-type)
  "Compatible with (funcall `interprogram-cut-function' TEXT).
MIME-TYPE: see `my-clipboard-get'."
  (interactive (list
                (and (use-region-p)
                     (buffer-substring (region-beginning) (region-end)))))
  (cond
   ((my-display-wayland-p)
    (and text
         (apply #'call-process
                `("wl-copy"
                  nil
                  nil
                  nil
                  ,@(and mime-type (list "--type" (format "%s" mime-type)))
                  "--"
                  ,text))))
   (t
    (my-clipboard--error-display-server))))

;;;###autoload
(cl-defun my-clipboard-kill-new-or-copy (text &key quiet timeout delay)
  "Try to copy TEXT to clipboard using Elisp implementation or alternative one.
Within TIMEOUT, try to use `kill-new', when the system clipboard
doesn't have TEXT on top or it takes too long to process, use an
alternative method. QUIET suppresses the message notified by the
latter method.

Note that after calling `kill-new' maybe there's a little delay
until the system clipboard get updated, therefore the alternative
will always be called. Non-nil DELAY: defer checking until DELAY."
  (-let*
      ((timeout (or timeout 1.0))
       (fn
        (lambda ()
          (-let* ((clipboard-top (my-clipboard-get)))
            (when (not (equal text clipboard-top))
              (my-clipboard-copy text)
              (unless quiet
                (message
                 "`my-clipboard-kill-new-or-copy': used `my-clipboard-copy' instead of `kill-new'!"))
              nil)))))
    (with-timeout (timeout (funcall fn))
      (kill-new text))
    (if delay
        (run-at-time delay nil fn)
      (funcall fn))))

;;;###autoload
(defun my-clipboard-copy-region-or-empty-string (&optional beg end)
  (interactive "r")
  (my-clipboard-copy
   (if (use-region-p)
       (buffer-substring beg end)
     "")))

;;;###autoload
(defun my-clipboard-use-wayland-commands (&optional reset)
  "Use Wayland shell commands for interprogram clipboard functions.
Use when Emacs hangs while copying/pasting.
\"Waiting for selection owner...\". See
https://yhetil.org/emacs-bugs/83h7057cyy.fsf@gnu.org/#r.

Non-nil RESET: reset default value."
  (interactive "P")
  (if reset
      (setq interprogram-paste-function #'gui-selection-value
            interprogram-cut-function #'gui-select-text)
    (setq interprogram-paste-function #'my-clipboard-get
          interprogram-cut-function #'my-clipboard-copy))
  (print (vector interprogram-paste-function
                 interprogram-cut-function)))

;;;###autoload
(defun my-clipboard-copy-file-contents (file &optional interactive-flag)
  (interactive (list (buffer-file-name) t))
  (prog1 (start-process-shell-command
          "" nil
          (cond
           ((my-display-wayland-p)
            (format "wl-copy < %s" file))
           (t
            (my-clipboard--error-display-server))))
    (when interactive-flag
      (message "Copied %s" file))))

(defvar my-yank-media-pre-hook '())
(defvar my-yank-media-post-hook '())

;;;###autoload
(defun my-clipboard-yank-maybe-paste-media ()
  (interactive)
  (-let* ((buf-state (vector (point) (buffer-string))))
    (condition-case _
        (progn
          (run-hooks 'my-yank-media-pre-hook)
          (prog1 (yank-media)
            (run-hooks 'my-yank-media-post-hook)))
      ((user-error)
       ;; `yank-media' didn't modify anything
       (when (equal buf-state (vector (point) (buffer-string)))
         (yank))))))
;; `delsel.el'
(put 'my-clipboard-yank-maybe-paste-media 'delete-selection 'yank)

;;; my-functions-clipboard.el ends here

(provide 'my-functions-clipboard)

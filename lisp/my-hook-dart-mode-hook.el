;; -*- lexical-binding: t; -*-

(require 'dash)

;; not really a REPL, but rather a control board
(set-repl-handler! 'dart-mode #'my-lang-flutter-show-cli)
;; `evil-collection' doesn't handle those yet, also `+eval-open-repl' forcefully
;; switches to insert state when `evil-mode' is active, so just disable evil
;; there
(with-eval-after-load 'evil
  (add-to-list 'evil-buffer-regexps
               `(,(format "\\`\\*%s\\*" (my-regexp-or ["Flutter" "Hover"])))))

(add-hook 'my-execu-direct-alist '(dart-mode . my-lang-flutter-run-or-hot-reload-web))

;; (my-add-hook/s '(flutter-mode-hook hover-mode-hook) '(my-leader-mode))

(my-bind :map 'dart-mode-map

  ;; "C-M-x" #'my-lang-flutter-run-or-hot-reload-web
  ;; "C-M-z" #'hover-run-or-hot-reload

  ;;
  )

(my-bind :map 'flutter-mode-map "C-o" #'my-lang-flutter-open-web)
(my-bind :map 'dart-mode-map "M-l o" #'my-lang-flutter-open-web)

;; it's implemented as a global mode, therefore affect all save commands
(setq hover-hot-reload-on-save nil)

(with-eval-after-load 'flutter
  (my-add-mode-hook-and-now 'dart-mode #'my-lang-flutter-hot-reload-on-save-mode))

(provide 'my-hook-dart-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

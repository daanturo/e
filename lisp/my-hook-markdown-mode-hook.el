;; -*- lexical-binding: t; -*-

;; `lsp-mode' requires `markdown-mode'

;; (my-backtrace)

(my-add-local-electric-pair 'markdown-mode ?` ?`)

(my-pair-add-multichar-pair 'markdown-mode "```" "```")

;; so that `my-pair-open-newline-before-h' functions
(my-bind :map 'markdown-mode-map "RET" nil)

(my-bind :map 'markdown-mode-map [remap markdown-insert-link] #'my-markdown-insert-link)

(setq! markdown-header-scaling t)

;;;###autoload
(my-accumulate-config-collect
 (setq markdown-fontify-code-blocks-natively t)
 (setq evil-markdown-key-theme '(navigation textobjects)))

(my-bind :map 'markdown-mode-map "M-l ." markdown-mode-command-map)
(my-bind :map 'markdown-mode-map "M-l l" #'my-markdown-live-preview)
(my-bind :map 'markdown-mode-map "M-l s" markdown-mode-style-map)

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'my-hook-markdown-mode-hook)

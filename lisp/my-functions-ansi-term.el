;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-ansi-term-read-program (&optional prompt)
  (read-shell-command (or prompt "Run program: ")
                      (or explicit-shell-file-name
                          (getenv "ESHELL")
                          shell-file-name)))

;;;###program
(defun my-ansi-term-renew (&optional name-arg)
  "With non-nil NAME-ARG, switch to or create a terminal with that name.
Else when major-mode is 'term-mode, restart it.
Else create a new terminal.
Run \"bash\"."
  (interactive)
  (cond
   (name-arg
    (if (get-buffer name-arg)
        (switch-to-buffer name-arg)
      (progn
        (term "bash")
        (rename-buffer name-arg))))
   ((equal major-mode 'term-mode)
    (if (term-check-proc (current-buffer))
        (ansi-term "bash")
      (let ((old-name (buffer-name)))
        (when (get-buffer "*terminal*")
          (with-current-buffer "*terminal*"
            (rename-uniquely)))
        ;; Ensure that this term is always available in case it was
        ;; terminated.  Change the name to `*terminal*' because
        ;; `*terminal*' can be restarted without loss of previous
        ;; output.
        (rename-buffer "*terminal*")
        (term "bash")
        (rename-buffer old-name))))))

;;;###autoload
(defun my-ansi-term-other-window (program &optional buf-name args)
  (interactive (list (my-ansi-term-read-program)))
  (-let* ((buf
           (save-window-excursion (apply #'ansi-term program buf-name args))))
    (save-selected-window
      (pop-to-buffer buf))
    buf))


(provide 'my-functions-ansi-term)

;; -*- lexical-binding: t; -*-

(require 'thingatpt)

(require 'dash)
(require '00-my-core-macros)
(require 'texmathp)

(require '00-my-core-macros)

;; (require 'ht)
(my-autoload-functions 'ht '(ht-find))


(defconst my-latex-math-even-backslashes-regexp
  (rx-to-string
   `(seq (?  (*? anychar)
             (not "\\"))
     (* "\\\\"))
   'no-group)
  "A non-greedy regexp used for expression bodies or the before of
beginnings:

optionally (anything, then a character that is not backslash),
then (even number of consecutive backslashes).")

(defun my-latex-math--get-type-commands-alist ()
  (--> (append texmathp-tex-commands texmathp-tex-commands-default)
       (-group-by 'cadr it)
       (-map (-lambda ((key . lst)) (cons key (-map 'car lst))) it)))
(defvar my-latex-math-type-commands-alist
  nil)

(defun my-latex-math-get-math-commands (&optional type)
  "Type is the second entry in each of `texmathp-tex-commands'."
  (unless my-latex-math-type-commands-alist
    (setq my-latex-math-type-commands-alist
          (my-latex-math--get-type-commands-alist)))
  (alist-get type my-latex-math-type-commands-alist))

(defcustom my-latex-math-default-line-diff-limit 40
  "The limit of line difference for searching operations that may
be too expensive.")

(defun my-latex-math--expression-bounds (beg-re end-re &optional line-diff-lim update-expr-fn)
  "UPDATE-EXPR-FN is used for nested environments, we limit the
search after finding out the macro name."
  (let* ((expr-re (concat beg-re my-latex-math-even-backslashes-regexp end-re))
         (p (point))
         (used-update-expr-fn (or update-expr-fn #'identity)))
    (save-match-data
      (or
       (and
        (save-excursion
          (my-latex-math--search-regexp-forward-backward beg-re end-re expr-re
                                                         line-diff-lim
                                                         used-update-expr-fn))
        (my-latex-math--match beg-re end-re line-diff-lim p 0))
       ;; for shorthands, `thing-at-point-looking-at' may not perform
       ;; correctly
       (and (thing-at-point-looking-at expr-re)
            (my-latex-math--match beg-re end-re line-diff-lim p 0))
       (and
        (or line-diff-lim update-expr-fn)
        (save-excursion
          (my-latex-math--search-regexp-backward-forward beg-re end-re expr-re
                                                         line-diff-lim
                                                         used-update-expr-fn))
        (my-latex-math--match beg-re end-re line-diff-lim p 0))))))
(defun my-latex-math--search-regexp-forward-backward (beg-re end-re expr-re line-diff-lim update-expr-fn)
  (and
   (re-search-forward (concat end-re "\\(?:[^z-a]\\|\\'\\)")
                      (and line-diff-lim (line-end-position line-diff-lim))
                      'noerror)
   (re-search-backward (funcall update-expr-fn expr-re)
                       (and line-diff-lim (line-beginning-position (- 1 line-diff-lim)))
                       'noerror)))
(defun my-latex-math--search-regexp-backward-forward (beg-re end-re expr-re line-diff-lim update-expr-fn)
  (and
   ;; the most expensive search, we limit it
   (let ((line-diff-lim (or line-diff-lim my-latex-math-default-line-diff-limit)))
     (re-search-backward (concat my-latex-math-even-backslashes-regexp beg-re)
                         (and line-diff-lim (line-beginning-position (- 1 line-diff-lim)))
                         'noerror))
   (re-search-forward (funcall update-expr-fn expr-re)
                      (and line-diff-lim (line-end-position line-diff-lim))
                      'noerror)))
(defun my-latex-math--match (beg-re end-re line-diff-lim p num)
  (let ((beg-pos (match-beginning num))
        (end-pos (match-end num)))
    (and (<= beg-pos p end-pos)
         ;; ;; Since `my-latex-math--search-regexp' has already limited, we
         ;; ;; don't need this check anymore
         ;; (or (not line-diff-lim) (not (equal beg-re end-re))
         ;;     (<= (- (line-number-at-pos end-pos) (line-number-at-pos beg-pos))
         ;;         line-diff-lim))
         (cons beg-pos end-pos))))

(defcustom my-latex-math-sw-line-limit 2
  "Max line difference between \"$\" \"$\" switches.")
(defun my-latex-math-sw-expression-bounds ()
  (or
   ;; Prevent wrong recognitions
   (and (my-latex-math--fontifying-math-if-possible-at-p (point))
        (or
         (my-latex-math--expression-bounds (regexp-quote "$") (regexp-quote "$")
                                           my-latex-math-sw-line-limit)
         (my-latex-math--expression-bounds (regexp-quote "$$") (regexp-quote "$$")
                                           (* 2 my-latex-math-sw-line-limit))))
   (my-latex-math--expression-bounds (regexp-quote "\\(") (regexp-quote "\\)"))
   (my-latex-math--expression-bounds (regexp-quote "\\[") (regexp-quote "\\]"))))

(defun my-latex-math--fontifying-math-if-possible-at-p (&optional p)
  "t when impossible."
  (let ((p (or p (point))))
    (or (not (member major-mode '(latex-mode)))
        (--> (text-properties-at p)
             (plist-get it 'face)
             ensure-list
             (member 'font-latex-math-face it)))))

(defun my-latex-math-env-expression-bounds ()
  (let ((envs (regexp-opt (my-latex-math-get-math-commands 'env-on))))
    (my-latex-math--expression-bounds
     (format "\\\\begin{\\(%s\\)}" envs)
     (format "\\\\end{\\(%s\\)}" envs)
     nil
     (lambda (expr-re)
       (string-replace (format "\\(%s\\)" envs)
                       (match-string-no-properties 1)
                       expr-re)))))

(defun my-latex-math-arg-expression-bounds ()
  (my-latex-math--expression-bounds
   (format "%s{" (regexp-opt (my-latex-math-get-math-commands 'arg-on)))
   "}"))

;;;###autoload
(defun my-latex-math-bounds-of-expression-at-point ()
  "See `texmathp-tex-commands' for supported types."
  (save-match-data
    (or (my-latex-math-sw-expression-bounds)
        (my-latex-math-env-expression-bounds)
        (my-latex-math-arg-expression-bounds))))

(defvar-local my-latex-math--no-expr-positions-cache nil
  "Key:position, Val: non-nil.")
(defvar-local my-latex-math--bounds->expr-cache nil
  "Key: (beg . end).
Val: expression between beg, end.")
(defvar-local my-latex-math--buffer-modified-tick nil)
;;;###autoload
(defun my-latex-math-cached-expression-at-point ()
  "Like `my-latex-math-bounds-of-expression-at-point', but with caching enabled by range of positions.
Cache is invalidated when the buffer is modified."
  (my-ensure-hash-table 'my-latex-math--bounds->expr-cache)
  (my-ensure-hash-table 'my-latex-math--no-expr-positions-cache)
  (cond ((not (equal my-latex-math--buffer-modified-tick (buffer-modified-tick)))
         (setq my-latex-math--buffer-modified-tick (buffer-modified-tick))
         (clrhash my-latex-math--bounds->expr-cache)
         (clrhash my-latex-math--no-expr-positions-cache)
         (my-latex-math--cache-expression))
        ((gethash (point) my-latex-math--no-expr-positions-cache)
         nil)
        (t
         (-let (((bound/s expr) (ht-find (-lambda ((beg . end) val)
                                           (<= beg (point) end))
                                         my-latex-math--bounds->expr-cache)))
           (if bound/s expr
             (my-latex-math--cache-expression))))))
(defun my-latex-math--cache-expression ()
  (if-let ((bounds (my-latex-math-bounds-of-expression-at-point)))
      (let ((expr (buffer-substring-no-properties (car bounds) (cdr bounds))))
        (puthash bounds expr my-latex-math--bounds->expr-cache)
        expr)
    (progn
      (puthash (point) t my-latex-math--no-expr-positions-cache)
      nil)))

;;;###autoload
(defun my-latex-math-expression-in-region-or-cached-at-point ()
  (if (use-region-p)
      (-let* ((text (buffer-substring-no-properties (region-beginning) (region-end))))
        (with-temp-buffer
          (insert text)
          (goto-char (floor (my-mean* (point-min) (point-max))))
          (my-latex-math-cached-expression-at-point)))
    (my-latex-math-cached-expression-at-point)))

;;;###autoload
(put 'my-latex 'bounds-of-thing-at-point 'my-latex-math-bounds-of-expression-at-point)

(provide 'my-latex-math)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;; (setq grip-preview-use-webkit nil)

;;;###autoload
(defun my-grip-mode-authenticate ()
  (interactive)
  (require 'auth-source)
  (-let* ((credential (auth-source-user-and-password "api.github.com")))
    (setq grip-github-user (elt credential 0))
    (setq grip-github-password (elt credential 1))))


;;; my-functions-grip-mode.el ends here

(provide 'my-functions-grip-mode)

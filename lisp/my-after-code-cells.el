;; -*- lexical-binding: t; -*-

(my-bind :map 'code-cells-mode-map
  "C-<end>" #'my-code-cells-goto-end-of-cell
  "C-<f9>" #'my-code-cells-run||eval-all
  "C-<home>" #'my-code-cells-goto-beg-of-cell
  "C-c C-c" #'code-cells-eval
  "M-H" #'my-code-cells-mark-cell-without-header
  '("p" "k") (code-cells-speed-key 'code-cells-backward-cell)
  '("n" "j") (code-cells-speed-key 'code-cells-forward-cell)
  "e" (code-cells-speed-key 'code-cells-eval)
  (kbd "TAB") (code-cells-speed-key (lambda ()
                                      "Show/hide current cell"
                                      (interactive)
                                      (outline-minor-mode)
                                      (if (outline-invisible-p (line-end-position))
                                          (outline-show-subtree)
                                        (outline-hide-subtree))))
  "C-c <C-i>" `(menu-item
                "" nil :filter
                ,(lambda (_)
                   (cond ((equal major-mode 'markdown-mode)
                          #'my-code-cells-markdown-insert-cell)))))

(my-bind :map 'code-cells-mode-map
  :vi 'normal
  "M-<down>" #'code-cells-move-cell-down
  "M-<up>" #'code-cells-move-cell-up
  "C-<return>" #'code-cells-eval
  "S-<return>" #'my-code-cells-eval-and-next-cell)

(add-hook
 'code-cells-mode-hook
 (lambda ()
   (when (member major-mode '(python-mode))
     (local-set-key
      [remap code-cells-eval] #'my-code-cells-eval-by-jupyter))))

;; Local Variables:
;; no-byte-compile: t
;; End:

(provide 'my-after-code-cells)

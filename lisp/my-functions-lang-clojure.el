;; -*- lexical-binding: t; -*-

(require 'dash)

;;;###autoload
(defun my-lang-clojure-repl ()
  "Ensure a jacked-in `cider' REPL."
  (interactive)
  (when (and (require 'cider nil ':noerror) (not (cider-repls)))
    (save-window-excursion
      (my-lang-clojure-set-cider-jack-in-tool)
      (cider-jack-in nil)))
  (cider-switch-to-repl-buffer)
  (cider-current-repl))

;;;###autoload
(defun my-lang-clojure-cider-search-and-insert-previous-prompt-input-history ()
  (interactive)
  (-let* ((hist cider-repl-input-history))
    (-->
     (completing-read
      "Input: "
      (lambda (string pred action)
        (if (eq action 'metadata)
            `(metadata (display-sort-function . identity))
          (complete-with-action action hist string pred))))
     (insert it))))


(provide 'my-functions-lang-clojure)

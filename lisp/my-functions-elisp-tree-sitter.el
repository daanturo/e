;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'tree-sitter nil t)

;; this file maybe loaded early by any functions that calls `beginning-of-defun'

;; NOTE: a node's position range is [`tsc-node-start-position', `tsc-node-end-position')

;;;###autoload
(defun my-elisp-tree-sitter-ancestor-node-p (node0 node1)
  "Is NODE0 ancestor of NODE1?"
  (and node0
       node1
       (my-loop
         [node2 (tsc-get-parent node1)]
         (cond
          ((null node2)
           nil)
          ((equal node0 node2)
           t)
          (t
           (recur node2))))))

;;;###autoload
(defun my-elisp-tree-sitter-map-children (func node)
  (let (retval)
    (tsc-mapc-children (lambda (child) (push (funcall func child) retval)) node)
    (nreverse retval)))

;;;###autoload
(defun my-elisp-tree-sitter-children (node)
  (my-elisp-tree-sitter-map-children #'identity node))

;;;###autoload
(defun my-elisp-tree-sitter-get-first-child (node &optional named)
  (if named
      (tsc-get-nth-named-child node 0)
    (tsc-get-nth-child node 0)))

;;;###autoload
(defun my-elisp-tree-sitter-get-last-child (node &optional named)
  (if named
      (tsc-get-nth-named-child node (1- (tsc-count-named-children node)))
    (tsc-get-nth-child node (1- (tsc-count-children node)))))

;;;###autoload
(cl-defun my-elisp-tree-sitter-bigger-ancestor
    (node &optional (compare-beg t) (compare-end t))
  "Return an ancestor node that has NODE inside its boundaries."
  (my-loop
    [node1 node]
    (let ((parent (tsc-get-parent node1)))
      (cond
       ((null parent)
        node1)
       ((and (or (not compare-beg)
                 (< (tsc-node-start-position parent)
                    (tsc-node-start-position node)))
             (or (not compare-end)
                 (< (tsc-node-end-position node) (tsc-node-end-position parent))))
        parent)
       (t
        (recur parent))))))

;;;###autoload
(defun tree-sitter-mark-bigger-node ()
  (interactive)
  (let* ((p (point))
         (m (or (mark) p))
         (beg (min p m))
         (end (max p m))
         (root (tsc-root-node tree-sitter-tree))
         (node (tsc-get-descendant-for-position-range root beg end))
         (node-beg (tsc-node-start-position node))
         (node-end (tsc-node-end-position node)))
    ;; Node fits the region exactly. Try its parent node instead.
    (when (and (= beg node-beg) (= end node-end))
      (when-let ((node (tsc-get-parent node)))
        (setq
         node-beg (tsc-node-start-position node)
         node-end (tsc-node-end-position node))))
    (set-mark node-end)
    (goto-char node-beg)))

(defun my-elisp-tree-sitter--bigger-region? (beg0 end0 beg1 end1)
  (and (<= beg1 beg0 end0 end1) (or (< beg1 beg0) (< end0 end1))))

;;;###autoload
(defun my-er/elisp-tree-sitter-expand-region ()
  (interactive)
  (-let* ((root (tsc-root-node tree-sitter-tree))
          (reg-beg
           (if (use-region-p)
               (region-beginning)
             (point)))
          (reg-end
           (if (use-region-p)
               (region-end)
             (point)))
          (node (tsc-get-descendant-for-position-range root reg-beg reg-end))
          (node-beg (tsc-node-start-position node))
          (node-end (tsc-node-end-position node)))
    (if (my-elisp-tree-sitter--bigger-region? reg-beg reg-end node-beg node-end)
        (my-set-mark-from-to node-end node-beg)
      (-if-let* ((parent (tsc-get-parent node))
                 (first-inner (my-elisp-tree-sitter-first-inner-child parent))
                 (inner-beg (tsc-node-start-position first-inner))
                 (last-inner (my-elisp-tree-sitter-last-inner-child parent))
                 (inner-end (tsc-node-end-position last-inner))
                 (big
                  (my-elisp-tree-sitter--bigger-region?
                   reg-beg reg-end inner-beg inner-end)))
          (my-set-mark-from-to inner-end inner-beg)
        (my-set-mark-from-to
         (tsc-node-end-position parent) (tsc-node-start-position parent))))))

;;;###autoload
(defun my-elisp-tree-sitter-backward-up-node (arg)
  (interactive "p")
  (dotimes (_ arg)
    (-let* ((node (tree-sitter-node-at-pos))
            (node-beg (tsc-node-start-position node)))
      (if (< node-beg (point))
          (goto-char node-beg)
        (goto-char
         (tsc-node-start-position
          (my-elisp-tree-sitter-bigger-ancestor node :compare-end nil)))))))

;;;###autoload
(defun my-elisp-tree-sitter-first-inner-child (parent-node)
  "Return the first child node of PARENT-NODE that begins after
PARENT-NODE's beginning."
  (let ((parent-beg (tsc-node-start-position parent-node)))
    (-->
     (tsc-count-children parent-node) (number-sequence 0 (- it 1) 1)
     (-some
      (lambda (idx)
        (let ((child (tsc-get-nth-child parent-node idx)))
          (and (/= parent-beg (tsc-node-start-position child)) child)))
      it))))
;;;###autoload
(defun my-elisp-tree-sitter-last-inner-child (parent-node)
  "Return the last child node of PARENT-NODE that ends after
PARENT-NODE's ending."
  (let ((parent-end (tsc-node-end-position parent-node)))
    (-->
     (tsc-count-children parent-node) (number-sequence (- it 1) 0 -1)
     (-some
      (lambda (idx)
        (let ((child (tsc-get-nth-child parent-node idx)))
          (and (/= parent-end (tsc-node-end-position child)) child)))
      it))))

;;;###autoload
(defun my-elisp-tree-sitter-print-tree ()
  (interactive)
  (save-selected-window
    (dlet ((inhibit-message t))
      (let ((sexp (-> tree-sitter-tree tsc-tree-to-sexp read pp))
            (buf
             (generate-new-buffer
              (format "*%s %s*" 'tree-sitter (buffer-name)))))
        (with-current-buffer buf
          (lisp-data-mode)
          (save-excursion (insert sexp)))
        (split-window-sensibly)
        (switch-to-buffer-other-window buf)))))

;;;###autoload
(defun my-elisp-tree-sitter-pulse-node (node &optional arg)
  (interactive (list
                (if current-prefix-arg
                    (my-elisp-tree-sitter-node-with-type/s-at
                     (my-elisp-tree-sitter-defun-types))
                  (tree-sitter-node-at-pos))
                current-prefix-arg))
  (pulse-momentary-highlight-region
   (tsc-node-start-position node) (tsc-node-end-position node))
  (message "%s" (tsc-node-type node)))

;;;###autoload
(cl-defun my-elisp-tree-sitter-beg-of-defun (&optional (arg 1))
  (interactive "p")
  ;; (my-skip-chars-position " ")
  (-let* ((p (point))
          ((sp-node bp-node cur-node bn-node sn-node)
           (my-elisp-tree-sitter-neighbor-nodes
            #'my-elisp-tree-sitter-defun-node-p p))
          (target-node
           (my-elisp-tree-sitter-navigate-defun--compute-target-node
            (-non-nil (list sp-node bp-node cur-node))
            p
            #'tsc-node-start-position
            arg
            'reverse-distance)))
    (when target-node
      (goto-char (tsc-node-start-position target-node)))))

;;;###autoload
(cl-defun my-elisp-tree-sitter-end-of-defun (&optional (arg 1))
  (interactive "p")
  (-let* ((p (point))
          ((sp-node bp-node cur-node bn-node sn-node)
           (my-elisp-tree-sitter-neighbor-nodes
            #'my-elisp-tree-sitter-defun-node-p p))
          (target-node
           (my-elisp-tree-sitter-navigate-defun--compute-target-node
            (-non-nil (list sn-node bn-node cur-node))
            p
            #'tsc-node-end-position
            arg
            nil)))
    (when target-node
      (goto-char (tsc-node-end-position target-node)))))

(defun my-elisp-tree-sitter-navigate-defun--compute-target-node
    (initial-nodes current-position position-fn arg reverse-distance)
  "Find the target node, take INITIAL-NODES one by one until
POSITION-FN's result on the corresponding target node points to a
position different from CURRENT-POSITION."
  (-some
   (my-elisp-tree-sitter-navigate-defun--compute-target-node-function
    current-position position-fn arg reverse-distance)
   initial-nodes))

(defun my-elisp-tree-sitter-navigate-defun--compute-target-node-function
    (current-position position-fn arg reverse-distance)
  (lambda (initial-node)
    (when-let*
        ;; Positive ARG: reduce it by 1 to get the relative distance we
        ;; meant, unless at the position where POSITION-FN points to:
        ;; keep "increased" to jump out
        ((fixed-arg
          (if (and (< 0 arg)
                   (/= current-position (funcall position-fn initial-node)))
              (1- arg)
            arg))
         (distance
          (if reverse-distance
              (- fixed-arg)
            fixed-arg))
         (target-node
          (my-elisp-tree-sitter-get-node-from initial-node distance 'climb))
         (target-position (funcall position-fn target-node)))
      (and (funcall (cond
                     ((< 0 distance)
                      #'<)
                     ((> 0 distance)
                      #'>)
                     (t
                      #'always))
                    current-position target-position)
           target-node))))

;;;###autoload
(defun my-elisp-tree-sitter-get-node-from
    (intial-node distance &optional climb named-only)
  "From INITIAL-NODE, find a node that is DISTANCE away from it.
When DISTANCE is exceeded, return the walked node. With non-nil
CLIMB, allow ascending to a bigger ancestor and treat the
ascended distance as 1."
  (named-let
      recur ((node intial-node) (delta distance))
    (let ((new-delta (my-step-to-0 delta))
          (prev-sibling
           (if named-only
               (tsc-get-prev-named-sibling node)
             (tsc-get-prev-sibling node)))
          (next-sibling
           (if named-only
               (tsc-get-next-named-sibling node)
             (tsc-get-next-sibling node)))
          (parent
           (and climb
                (my-elisp-tree-sitter-bigger-ancestor
                 node (< delta 0) (< 0 delta)))))
      (cond
       ;; non-nil prev-sibling, still must move left
       ((and (> 0 delta) prev-sibling)
        (recur prev-sibling new-delta node))
       ;; non-nil next-sibling, still must move right
       ((and (< 0 delta) next-sibling)
        (recur next-sibling new-delta node))
       ;; go to parent
       ((and parent (not (zerop delta)))
        (recur parent new-delta node))
       ;; reached the exact node (zerop delta) or can't go anymore
       (t
        node)))))

;;;###autoload
(defun my-elisp-tree-sitter-node-with-type/s-at
    (type/s &optional pos l-excl-lim r-excl-lim)
  (let ((types (ensure-list type/s)))
    (my-elisp-tree-sitter-node-at
     (lambda (node)
       (member (tsc-node-type node) types))
     pos l-excl-lim r-excl-lim)))

;;;###autoload
(cl-defun my-elisp-tree-sitter-node-at
    (&optional pred pos l-excl-lim r-excl-lim)
  "Return the smallest node that satisfies PRED at POS."
  ;; `1-' edge case
  (let* ((pos (max 1 (or pos (point)))))
    (my-loop
      [node
       (tsc-get-descendant-for-position-range
        (tsc-root-node tree-sitter-tree) pos pos)]
      (cond
       ((null node)
        nil)
       ((or (not pred) (funcall pred node))
        node)
       (t
        (if-let ((parent (tsc-get-parent node)))
            (and (or (not l-excl-lim)
                     (< l-excl-lim (tsc-node-start-position parent)))
                 (or (not r-excl-lim)
                     (<= (tsc-node-end-position parent) r-excl-lim))
                 (recur parent))
          nil))))))

(defconst my-elisp-tree-sitter-function-types
  '(arrow_function
    function
    function_declaration
    function_definition
    function_item
    lambda
    lambda_expression
    method
    method_declaration
    method_definition

    ;; function_declarator
    ))
(defconst my-elisp-tree-sitter-class-types
  '(class_declaration
    class_definition class_specifier impl_item struct_specifier

    ;; module program
    ))

(defun my-elisp-tree-sitter-defun-types ()
  (append my-elisp-tree-sitter-function-types my-elisp-tree-sitter-class-types))

(defun my-elisp-tree-sitter-defun-node-p (node)
  (or (member (tsc-node-type node) (my-elisp-tree-sitter-defun-types))
      ;; top-level
      (let ((parent (tsc-get-parent node)))
        (and parent (null (tsc-get-parent parent))))))

;;;###autoload
(defun my-elisp-tree-sitter-defun-node-at (&optional pos)
  (my-elisp-tree-sitter-node-at
   #'my-elisp-tree-sitter-defun-node-p (or pos (point))))

;;;###autoload
(cl-defun my-elisp-tree-sitter-neighbor-nodes (&optional pred pos)
  "Return a list of 5 nodes that satisfy PRED: smallest node before
POS, node before POS, node at POS, node after POS, smallest node
after POS. Each of them can be `nil'."
  (-let* ((pos (or pos (point)))
          (node0 (my-elisp-tree-sitter-node-at pred pos))
          (super-node (or node0 (tree-sitter-node-at-pos nil pos)))
          ((lnode rnode)
           (and super-node
                (my-elisp-tree-sitter-lax-children-nodes-around
                 super-node pos))))
    (->>
     (list
      (and lnode
           (my-elisp-tree-sitter-node-at
            pred
            (1- (tsc-node-end-position lnode)) nil pos))
      lnode node0 rnode
      (and rnode
           (my-elisp-tree-sitter-node-at
            pred (tsc-node-start-position rnode) pos nil)))
     (-map (lambda (node) (and node (funcall pred node) node))))))

;;;###autoload
(cl-defun my-elisp-tree-sitter-lax-children-nodes-around (parent-node &optional pos)
  "Like `my-elisp-tree-sitter-children-nodes-around', but when fails, try
getting nodes at the boundaries of spaces around POS."
  (let ((pos (or pos (point))))
    (or (my-elisp-tree-sitter-children-nodes-around parent-node pos)
        (list
         (my-elisp-tree-sitter-node-at
          nil (1- (my-skip-chars-position nil pos nil 'backward)))
         (my-elisp-tree-sitter-node-at nil (my-skip-chars-position nil pos))))))

;;;###autoload
(cl-defun my-elisp-tree-sitter-children-nodes-around
    (parent-node &optional (pos (point)))
  "Get list of 2 child nodes of PARENT-NODE before and after POS.
Assume that POS is within PARENT-NODE's range. Try super nodes
before sub nodes."
  (named-let
      recur0 ((node parent-node))
    (cond
     ((zerop (tsc-count-children node))
      nil)
     ;; first child is after POS
     ((let ((child0 (my-elisp-tree-sitter-get-first-child node)))
        (and (< pos (tsc-node-start-position child0)) `(nil ,child0))))
     ;; last child is before POS
     ((let ((childn (my-elisp-tree-sitter-get-last-child node)))
        (and (<= (tsc-node-end-position childn) pos) `(,childn nil))))
     ;; (must at least one child)
     (t
      (let ((children (my-elisp-tree-sitter-children node)))
        (named-let
            recur1
            ((child0 (car children))
             (child1 (cadr children))
             (siblings (cddr children)))
          (cond
           ;; within a child
           ((and (<= (tsc-node-start-position child0) pos)
                 (< pos (tsc-node-end-position child0)))
            (recur0 child0))
           ;; terminate the loop
           ((null child1)
            nil)
           ;; between two children
           ((and (<= (tsc-node-end-position child0) pos)
                 (< pos (tsc-node-start-position child1)))
            `(,child0 ,child1))
           (t
            (recur1 child1 (car siblings) (cdr siblings))))))))))

;;;###autoload
(defun my-elisp-tree-sitter-mark-defun (arg)
  "`mark-defun' using `tree-sitter'.
Ideally, just setting `beginning-of-defun-function' and
`end-of-defun-function' ought to suffice, but vanilla
`mark-defun' has some complex movement logic that we can't
completely control and predict."
  (interactive "p")
  (cond
   ((and (not (use-region-p)) (= 1 arg))
    (my-elisp-tree-sitter-mark-defun-1 'extend))
   (t
    (mark-defun arg t))))

(defun my-elisp-tree-sitter-mark-defun-1 (&optional extend)
  "Handle the fundamentally most used case of `mark-defun' (without
prefix arg and marked region) by `tree-sitter'. With non-nil
EXTEND, try to mimic `mark-defun''s behavior: extend `mark' after
a new line, extend `point' up to the previous line (when the
extended region is empty)."
  (interactive "P")
  (-let* ((node (my-elisp-tree-sitter-defun-node-at (my-skip-chars-position)))
          (end-pos (tsc-node-end-position node))
          (beg-pos (tsc-node-start-position node)))
    (set-mark
     (if extend
         (my-skip-chars-position "\n" end-pos (1+ end-pos))
       end-pos))
    (goto-char beg-pos)
    (when extend
      ;; When the region between the line beginning and `beg-pos' is empty, go
      ;; to the former
      (when (save-match-data
              (looking-back "^[[:space:]]+" (line-beginning-position)))
        (forward-line 0)
        ;; Additionally, when the line above is empty, extend that line, too
        (let ((p (point)))
          (forward-line -1)
          (unless (= ?\n (char-after))
            (goto-char p)))))))

;; NOTE some keywords are treated as anonymous by tree-sitter

;;;###autoload
(defun my-elisp-tree-sitter-forward-node (arg)
  (interactive "p")
  (my-elisp-tree-sitter-skip-anonymous-nodes)
  (-let* ((cur-node
           (tree-sitter-node-at-pos
            :named (my-skip-chars-position nil nil nil nil)))
          (arg*
           (if (= (point) (tsc-node-end-position cur-node))
               arg
             (1- arg)))
          (to-go-node
           (my-elisp-tree-sitter-get-node-from cur-node arg* nil 'named)))
    (goto-char (tsc-node-end-position to-go-node))))

;;;###autoload
(defun my-elisp-tree-sitter-backward-node (arg)
  (interactive "p")
  (my-elisp-tree-sitter-skip-anonymous-nodes 'backward)
  (-let* ((cur-node
           (tree-sitter-node-at-pos
            :named (1- (my-skip-chars-position nil nil nil t))))
          (arg*
           (if (= (point) (tsc-node-start-position cur-node))
               arg
             (1- arg)))
          (to-go-node
           (my-elisp-tree-sitter-get-node-from cur-node (- arg*) nil 'named)))
    (goto-char (tsc-node-start-position to-go-node))))

(defun my-elisp-tree-sitter-skip-anonymous-nodes (&optional backward)
  (interactive "P")
  (while (progn
           (if backward
               (skip-chars-backward "[:space:]\n")
             (skip-chars-forward "[:space:]\n"))
           (-if-let*
               ((node
                 (tree-sitter-node-at-pos :named (and backward (1- (point)))))
                ;; met the beginning (if BACKWARD, end) of a named node: stop
                ;; finding
                (_
                 (= (point)
                    (if backward
                        (tsc-node-end-position node)
                      (tsc-node-start-position node)))))
               nil
             (-let* ((node
                      (tree-sitter-node-at-pos
                       :anonymous (and backward (1- (point)))))
                     (parent (tsc-get-parent node)))
               (cond
                ((or (null node)
                     (my-elisp-tree-sitter-node=
                      node
                      (if backward
                          (my-elisp-tree-sitter-get-first-child parent)
                        (my-elisp-tree-sitter-get-last-child parent))))
                 nil)
                (t
                 (progn
                   (goto-char
                    (if backward
                        (tsc-node-start-position node)
                      (tsc-node-end-position node))))
                 t)))))))

;;;###autoload
(defun my-elisp-tree-sitter-up-node (arg)
  (interactive "p")
  (dotimes (_ arg)
    (let* ((node (tree-sitter-node-at-pos))
           (beg (tsc-node-start-position node)))
      (if (and (< beg (point)) (< 1 (tsc-count-children node)))
          (goto-char beg)
        (goto-char
         (tsc-node-start-position
          (my-elisp-tree-sitter-bigger-ancestor node :compare-end nil)))))))

(defun my-elisp-tree-sitter-down-node--next-list (init-pos)
  (my-loop
    [node
     (or (tree-sitter-node-at-pos :named init-pos)
         (tree-sitter-node-at-pos nil init-pos))]
    (let ((beg (tsc-node-start-position node)))
      (cond
       ((null node)
        nil)
       ((and (< 1 (tsc-count-children node)) (<= (point) beg))
        node)
       (t
        (let ((s-node (tree-sitter-node-at-pos nil (max beg init-pos))))
          (recur
           (or (tsc-get-next-named-sibling s-node)
               (tsc-get-next-sibling s-node)))))))))
;;;###autoload
(defun my-elisp-tree-sitter-down-node (arg)
  (interactive "p")
  (dotimes (_ arg)
    (-some-->
        (my-skip-chars-position)
      (my-elisp-tree-sitter-down-node--next-list it)
      (my-elisp-tree-sitter-get-first-child it)
      (tsc-node-end-position it)
      (goto-char it))))

;;;###autoload
(defun my-elisp-tree-sitter-node= (&rest nodes)
  (apply #'equal (-map #'tsc-node-position-range nodes)))

;;;###autoload
(defun my-elisp-tree-sitter-register-modes-with-available-grammars ()
  (dolist (file (directory-files (file-name-concat tree-sitter-langs-grammar-dir "bin")
                                 nil "\\.so$"))
    (let* ((base (file-name-base file))
           (mode (intern (concat base "-mode"))))
      (unless (assoc mode tree-sitter-major-mode-language-alist)
        (push (cons mode (intern base)) tree-sitter-major-mode-language-alist)))))

;;;###autoload
(defun my-elisp-tree-sitter-symlink-grammars-from (&optional glob)
  (interactive)
  (let ((glob (or glob "/usr/lib/libtree-sitter-*.so")))
    (dolist (file (file-expand-wildcards glob))
      (let ((base
             (string-remove-prefix "libtree-sitter-" (file-name-base file))))
        (make-symbolic-link file
                            (file-name-concat tree-sitter-langs-grammar-dir
                                              "bin"
                                              (concat base ".so"))
                            'ok))))
  (my-elisp-tree-sitter-register-modes-with-available-grammars))

;; (defun my-elisp-tree-sitter-faces ()
;;   (-filter (lambda (face)
;;              (string-prefix-p "tree-sitter-hl-face:" (symbol-name face)))
;;            (face-list)))
;; (defvar-local my-no-tree-sitter-hl-mode--face-remap-cookies nil)
;; ;;;###autoload
;; (define-minor-mode my-no-tree-sitter-hl-mode nil
;;   :global nil
;;   (if my-no-tree-sitter-hl-mode
;;       (unless my-no-tree-sitter-hl-mode--face-remap-cookies
;;         (setq my-no-tree-sitter-hl-mode--face-remap-cookies
;;               (-map (lambda (face)
;;                       (face-remap-add-relative
;;                        face
;;                        :foreground nil :inherit nil))
;;                     (my-elisp-tree-sitter-faces))))
;;     (when my-no-tree-sitter-hl-mode--face-remap-cookies
;;       (mapc 'face-remap-remove-relative my-no-tree-sitter-hl-mode--face-remap-cookies)
;;       (setq my-no-tree-sitter-hl-mode--face-remap-cookies nil)))
;;   (force-window-update (current-buffer)))

;;; my-functions-elisp-tree-sitter.el ends here

(provide 'my-functions-elisp-tree-sitter)

;; -*- lexical-binding: t; -*-


;;;###autoload
(define-minor-mode my-display-point-mode
  "Display the current `point' in the mode line.
For debugging purpose only, as this mode has some performance
overhead."
  :global t
  :lighter (:eval (concat " |: " (number-to-string (point)))))

(add-hook 'minions-prominent-modes 'my-display-point-mode)

(provide 'my-display-point-mode)

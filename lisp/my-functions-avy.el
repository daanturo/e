;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-avy-show-dispatch-help-a (&rest _)
  (avy-show-dispatch-help))

;;;###autoload
(defun my-avy-action-exchange-sexp (pt)
  "Exchange/Tranpose the symbolic expressions at PT and `point', goto PT.
(\"exchange\" is used instead of \"swap\" because in
`avy-dispatch-alist', \"t\" as \"tranpose\" is already taken,
between \"exchange\" and \"swap\", only the former is mentioned
in the Emacs manual's section \"Tranposing Text\")"
  (-let* ((target-window (selected-window))
          (origin-window (cdr (ring-ref avy-ring 0)))
          ((target-beg . target-end) (with-selected-window target-window
                                       (my-eval-at-position pt
                                         (bounds-of-thing-at-point 'sexp))))
          ((origin-beg . origin-end) (with-selected-window origin-window
                                       ;; (goto-char pt)
                                       (bounds-of-thing-at-point 'sexp))))
    (my-tranpose-regions-between (window-buffer origin-window) origin-beg origin-end
                                 (window-buffer target-window) target-beg target-end)
    (select-window target-window)
    (goto-char pt)
    t))

;;;###autoload
(defun my-avy-action-embark-act (pt)
  (my-eval-at-position pt
    (embark-act))
  (select-window (cdr (ring-ref avy-ring 0)))
  t)

;;;###autoload
(defun my-avy-show-dispatch-help ()
  "Like `avy-show-dispatch-help', but supports more keystrokes
other than single characters."
  (message
   "%s"
   (mapconcat
    (-lambda ((key . def))
      (format "%s %s"
              (--> key
                   (vector it)
                   (key-description it)
                   (propertize it 'face 'help-key-binding))
              (--> (format "%s" def)
                   (replace-regexp-in-string "\\(my-\\)?avy-action-" "" it)
                   (propertize it 'face 'font-lock-function-name-face))))
    avy-dispatch-alist
    "  ")))

;;;###autoload
(defun my-avy-goto-char-timer-for-action (&optional arg)
  "(`avy-goto-char-timer' ARG), optimized for dispatch actions."
  (interactive "P")
  ;; make actions always available
  (dlet ((avy-single-candidate-jump nil))
    ;; always show dispatchs
    (my-with-advice
      #'avy-read
      :before #'my-avy-show-dispatch-help-a (avy-goto-char-timer arg))))

(provide 'my-functions-avy)

;; -*- lexical-binding: t; -*-

(require 'dash)

;; See also: https://github.com/rnkn/olivetti, https://git.sr.ht/~ashton314/auto-olivetti

(defvar my-focus-center-mode-after-adjust-hook '())

(defun my-focus-center-mode--adjust (&optional window-or-frame)
  (-let* ((windows (my-window-or-frames->windows window-or-frame)))
    (dolist (window windows)
      (with-selected-window window
        (when my-focus-center-mode
          (-let* ((margin-w (or my-focus-center-mode-margin-width
                                (-->
                                 (/ (- (window-total-width window)
                                       my-focus-center-mode-body-width)
                                    2.0)
                                 ;; take `text-scale-mode' into account: since expanding
                                 ;; the body width will eventually take all the
                                 ;; horizontal space, we shrink the margins by a
                                 ;; fraction instead
                                 (if (bound-and-true-p text-scale-mode)
                                     (/ (float it)
                                        (expt text-scale-mode-step text-scale-mode-amount))
                                   it)
                                 (max it 0)
                                 floor))))
            (set-window-margins window
                                margin-w
                                (and my-focus-center-mode-right-margin margin-w)))
          ;; (unless my-focus-center-mode (set-window-margins window nil nil))
          (run-hooks 'my-focus-center-mode-after-adjust-hook))))))

;;;###autoload
(define-minor-mode my-focus-center-mode
  "Center text, set `my-focus-center-mode-body-width'."
  :global nil
  (if my-focus-center-mode
      (progn
        (my-add-hook/s '(
                         ;; window-configuration-change-hook
                         window-size-change-functions
                         ;; window-buffer-change-functions
                         ;; window-setup-hook
                         ;; window-state-change-functions
                         ;; window-selection-change-functions
                         text-scale-mode-hook)
          #'(my-focus-center-mode--adjust) nil t)
        (my-focus-center-mode--adjust))
    (progn
      (my-remove-hook/s '(
                          ;; window-configuration-change-hook
                          window-size-change-functions
                          ;; window-buffer-change-functions
                          ;; window-setup-hook
                          ;; window-state-change-functions
                          ;; window-selection-change-functions
                          text-scale-mode-hook)
        #'(my-focus-center-mode--adjust) t)
      (set-window-margins (selected-window) nil nil))))

;;;###autoload
(define-minor-mode my-left-margin-mode
  "`my-focus-center-mode' without `my-focus-center-mode-right-margin'."
  :global nil
  (if my-left-margin-mode
      (progn
        (setq-local my-focus-center-mode-margin-width my-sidebar-width
                    my-focus-center-mode-right-margin nil)
        (my-focus-center-mode))
    (progn
      (kill-local-variable 'my-focus-center-mode-margin-width)
      (kill-local-variable 'my-focus-center-mode-right-margin)
      (my-focus-center-mode 0))))

(defvar my-focus-center-mode-body-width 160 "See also `my-focus-center-mode-margin-width'.")
(defvar my-focus-center-mode-margin-width nil "Override `my-focus-center-mode-body-width'.")
(defvar my-focus-center-mode-right-margin t "Enlarge the window's right margin?.")

;;;###autoload
(progn
  (defun my-focus-center-mode-enabler-with-width (width)
    (my-maydefun-set
      (my-concat-symbols '@ 'my-focus-center-mode width)
      ((&rest _)
       (setq-local my-focus-center-mode-body-width width)
       (my-focus-center-mode)))))

;;;###autoload
(define-minor-mode my-scaled-center-mode nil
  :global nil
  (if my-scaled-center-mode
      (progn
        (text-scale-adjust 1)
        (setq-local my-focus-center-mode-body-width 96)
        (my-focus-center-mode))
    (progn
      (text-scale-adjust 0)
      (kill-local-variable 'my-focus-center-mode-body-width)
      (my-focus-center-mode 0))))

(provide 'my-focus-center-mode)

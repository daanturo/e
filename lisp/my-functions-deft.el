;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'org)

;;;###autoload
(defun my-deft-preview-open-file-below ()
  (interactive)
  (-when-let* ((file (deft-filename-at-point)))
    (dlet ((display-buffer-alist
            '((""
               ;; display-buffer-below-selected
               display-buffer-at-bottom
               ;;
               ))))
      (deft-open-file-other-window))
    (-let* ((buf
             (or (get-file-buffer file)
                 (get-file-buffer (file-truename file)))))
      (and buf (get-buffer-window buf)))))

;;;###autoload
(defun my-deft-org-roam-parse-title (file contents &rest _)
  (or
   ;; prefer org's parsed #+TITLE
   (with-temp-buffer
     (insert contents)
     (org-get-title (current-buffer)))
   ;; get from file name
   (-->
    file
    (file-name-base it)
    (replace-regexp-in-string "^[0-9]+-" "" it)
    (replace-regexp-in-string "[_]+" " " it))))

;;;###autoload
(defun my-deft-org-roam-parse-summary (contents _title &rest _)
  (progn
    ;; with-temp-buffer (insert contents)
    (-let* ((filetags
             (nth
              1
              (s-match
               "^#\\+\\(?:filetags\\|FILETAGS\\):\s*\\(:.*:\\)" contents))))
      (-->
       contents
       ;; :PROPERTIES:, :ID:, ...
       (replace-regexp-in-string "^:[A-Z]+:.*" "" it)
       ;; #+ headers
       (replace-regexp-in-string "^[# ]+\\+.*" "" it)
       ;; "trim" all lines
       (replace-regexp-in-string "^[ \t]+\\(.+\\)[ \t]*$" "\\1" it)
       ;; replace all newlines
       (replace-regexp-in-string "\n+" "  " it)
       (string-trim it)
       (concat filetags "\t" it)))))

;;; my-functions-deft.el ends here

(provide 'my-functions-deft)

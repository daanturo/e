;; -*- lexical-binding: t; -*-

(require 'cl-lib)
(require 'cl-macs)
;; (require 'subr-x nil 'noerror)
(require 'bytecomp)                     ; `byte-compile-dest-file'
(autoload #'package-installed-p "package")

;; (require 'my-early-init)

(defvar my-site-lisp-dir/)

(defconst my-emacs~version
  (format "%s.%s" emacs-major-version (max 1 emacs-minor-version))
  "Return Emacs's current (or to be released) version.")

(defvar my-native-comp-flag
  (if (fboundp 'native-comp-available-p)
      (native-comp-available-p)
    nil)
  "Non-nil means `native-compile' is supported.")
;; Disabled for batch sessions to avoid lengthy compilations.

;;; Helper forms

;; compat
(defun my-string-suffix-p (suffix string  &optional ignore-case)
  (declare (pure t) (side-effect-free t))
  (let* ((start-pos (- (length string) (length suffix))))
    (and (>= start-pos 0)
         (eq t (compare-strings suffix nil nil
                                string start-pos nil ignore-case)))))

;;;###autoload
(defun my-remove-str-fixes (pre suf str)
  (declare (pure t) (side-effect-free t))
  (substring
   str
   (if (string-prefix-p pre str)
       (length pre)
     0)
   (if (my-string-suffix-p suf str)
       (- (length str) (length suf))
     (length str))))

;;;###autoload
(defun my-ensure-string-prefix (prefix string)
  (declare (pure t) (side-effect-free t))
  (if (string-prefix-p prefix string)
      string
    (concat prefix string)))

;;;###autoload
(defun my-ensure-string-suffix (suffix string)
  (declare (pure t) (side-effect-free t))
  (if (my-string-suffix-p suffix string)
      string
    (concat string suffix)))

;;;###autoload
(defun my-regexp-or (regexps &optional group)
  (declare (pure t) (side-effect-free t))
  (format (if group
              "\\(%s\\)"
            "\\(?:%s\\)")
          (mapconcat (lambda (regexp) (format "\\(?:%s\\)" regexp)) regexps
                     "\\|")))

;;;###autoload
(defun my-filename-join (&rest components)
  (replace-regexp-in-string
   "/+" "/"
   (mapconcat #'identity components "/")))

;;;###autoload
(defun my-filename-all-extensions-as-string (filename)
  "Return FILENAME's part from the first \".\".
Ignore FILENAME's ancestors. The return value always include a
period."
  (substring filename (string-match-p "\\.[^/]+$" filename)))

;;;###autoload
(defun my-mapcat (func seq)
  (apply #'append (mapcar func seq)))

;;;###autoload
(defmacro my-profiler-cpu-progn (&rest body)
  (declare (debug t))
  ;; profiling is more important than benchmarking here, so try not to include
  ;; the benchmarker's overhead in the profiler
  `(benchmark-progn
     (profiler-start 'cpu)
     ;; instead of `prog1', as BODY may raise errors
     (unwind-protect
         (progn ,@body)
       (profiler-stop)
       (save-window-excursion
         (profiler-report)))))

;;;###autoload
(defun my-process-sync->output-string (cmdargs &optional no-trim)
  "Return process CMDARGS's output (STDOUT and STDERR).
NO-TRIM: by default, trim the last newline, unless this is non-nil."
  ;; just empty instead command not found
  (ignore-error (file-missing file-error)
    (with-temp-buffer
      (apply #'call-process (car cmdargs) nil t nil (cdr cmdargs))
      (let* ((str (buffer-string))
             (leng (length str)))
        (cond
         ((and (not no-trim) (< 0 leng) (equal ?\n (aref str (- leng 1))))
          (substring str 0 (- leng 1)))
         (t
          str))))))

;;;###autoload
(defun my-vc-git-get-current-revision-commit-hash (&optional dir)
  (let* ((default-directory (or dir default-directory)))
    (my-process-sync->output-string '("git" "rev-parse" "HEAD"))))

;;;###autoload
(defun my-elisp-remove-provide-in-buffer ()
  (goto-char (point-max))
  (while (re-search-backward "\\(\\'\\|\n\\)(provide '[^\s]+)$" nil t)
    (replace-match "")
    (delete-blank-lines)))

;;;###autoload
(defun my-file-symlink-target-1 (file)
  "Return one level of symlink target of FILE."
  (let* ((path (expand-file-name (directory-file-name file)))
         (target (file-symlink-p path)))
    (or (and target
             ;; `file-symlink-p' returns relative path
             (expand-file-name target (file-name-directory path)))
        file)))

;;;###autoload
(defun my-file-symlink-target (file)
  "A stripped down version of `file-truename' on FILE.
Doesn't resolve files which have an ancestor which is a link,
therefore should be a bit faster."
  (let* ((cur file)
         new)
    (while cur
      (setq new (my-file-symlink-target-1 cur))
      (if (equal new cur)
          (setq cur nil)
        (setq cur new)))
    new))

;;;###autoload
(defun my-delete-file-local-variables (&rest variables)
  "Delete local VARIABLES from the currently opened file.
Doesn't save, though."
  (dolist (var variables)
    (delete-file-local-variable var)
    (delete-file-local-variable-prop-line var)))

(defvar my-download|copy-file-from-url-by-emacs--yes nil)
(defun my-download|copy-file-from-url-by-emacs (url local-path)
  (condition-case err
      (url-copy-file url local-path)
    (error
     (when (or (y-or-n-p (format "%sn, disable %s \n and continue downloading %s ?"
                                 err 'gnutls-verify-error url))
               my-download|copy-file-from-url-by-emacs--yes)
       (setq my-download|copy-file-from-url-by-emacs--yes t)
       (let* ((gnutls-verify-error nil))
         (url-copy-file url local-path))))))

;;;###autoload
(defun my-synchronouly-download-file (url local-path)
  (cond
   ((executable-find "curl")
    (my-process-sync->output-string (list "curl" "-o" local-path url)))
   (t
    (my-download|copy-file-from-url-by-emacs url local-path))))

;;;###autoload
(defalias #'my->Str #'prin1-to-string)

;;;###autoload
(defun my-print (&rest more)
  "Print and return MORE.
Try to be compatible with Doom's CLI. See also `my-println🌈'."
  (mapc #'princ more)
  more)

;;;###autoload
(defun my-println (&rest more)
  "`my-print' on MORE with spaces interposed and new line at last."
  (apply #'my-print
         (cl-loop for (head . tail) on more
                  collect head collect
                  (if tail " " "\n")))
  more)

;;;###autoload
(defun my-println🌈 (&rest more)
  "`my-println' on MORE with a personalized indicator."
  (apply #'my-println "🌈" more))

;;;###autoload
(defun my-prn (&rest more)
  (apply #'my-println (mapcar #'my->Str more))
  more)

;;;###autoload
(defun my-prn🌈 (&rest more)
  (apply #'my-println "🌈" (mapcar #'my->Str more))
  more)

;;;###autoload
(defun my-read-text-file (path)
  (declare (side-effect-free t))
  (with-temp-buffer
    (insert-file-contents path)
    (buffer-string)))

;;;###autoload
(defun my-write-text-file (file text &optional append)
  "Write TEXT into FILE. Note that final newline isn't automatically ensured.
See `write-region' for APPEND."
  (unless (file-exists-p file)
    (make-empty-file file 'parents))
  (with-temp-buffer
    (insert text)
    (write-region (point-min) (point-max) file append :silent)))

;;;###autoload
(defun my-append-text-file (file text)
  "(`my-write-text-file' FILE TEXT true)."
  (my-write-text-file file text :append))

;;;###autoload
(cl-defun my-some-files-newer-than-p (file-test-old files-test-new &optional
                                                    (true-file-fn #'identity))
  "Return if one of FILES-TEST-NEW is newer than FILE-TEST-OLD.
TRUE-FILE-FN is called on the test new ones before comparing with
FILE-TEST-OLD."
  (cl-find-if
   (lambda (file-test-new)
     (file-newer-than-file-p (funcall true-file-fn file-test-new)
                             file-test-old))
   files-test-new))

;;; Compatibilities

(defconst my-semantic-version-regexp "[0-9]+\\(:?\\.[0-9]+\\)*"
  "Beginning and end aren't bound.")

(defconst my-compats-lisp-dir (concat my-site-lisp-dir/ "my-compats"))

;;;###autoload
(defun my-compats ()
  "Load my settings for compatibilities on older Emacs versions.
In `my-compats-lisp-dir'."
  (my-useelisp-install my-compats-lisp-dir)
  (let* ((dir my-compats-lisp-dir)
         (regexp
          (format "^my-compats-\\(%s\\)\\.el$" my-semantic-version-regexp))
         (loaded '())
         (files (directory-files dir nil regexp)))
    (dolist (file files)
      (let* ((ver
              (progn
                (string-match regexp file)
                (match-string 1 file))))
        (when (version< emacs-version ver)
          (let* ((base (my-remove-str-fixes "" ".el" file)))
            ;; (path!ext (concat dir "/" base))

            (require (intern base))
            (push base loaded)))))
    loaded))

;;; Emacs Lisp compilation

;;;###autoload
(defun my-compiled-function-p (func)
  (declare (side-effect-free t))
  (and (functionp func)
       (let* ((sym-fn (if (symbolp func)
                          (symbol-function func)
                        func)))
         (or (and (fboundp 'subr-native-elisp-p)
                  (subr-native-elisp-p sym-fn)
                  'native)
             (and (byte-code-function-p sym-fn)
                  'byte)
             ;; an alias
             (and (symbolp sym-fn)
                  (my-compiled-function-p sym-fn))))))

(defconst my-elisp-compiled-period-extension-regexp "\\.el[cn]$")

(defvar my-elisp-no-compile nil)

;;;###autoload
(defun my-elisp-clean-compiled-files (&optional dir)
  "Clean compiled elisp files in DIR or the configuration directory.
For after moving the config directory, byte-compiled files were
produced by another Emacs version, therefore are incompatible.
Put this function's definition before loading any compiled files
to allow calling it via M-x, even when errors happen."
  (interactive)
  (let* ((dir* (expand-file-name (or dir my-emacs-conf-dir/)))
         (re
          (my-regexp-or
           (list my-elisp-compiled-period-extension-regexp "\\.el~$")))
         (files
          (mapcar
           #'abbreviate-file-name
           (cond
            ((fboundp #'directory-files-recursively)
             (directory-files-recursively dir* re))
            (t
             (process-lines "find" dir* "-type" "f" "-regex" re))))))
    (my-println🌈 "Deleting" #'my-elisp-clean-compiled-files files)
    (mapc (lambda (file) (delete-file file)) files)))
;;;###autoload
(defalias #'my--elc #'my-elisp-clean-compiled-files)

;;;###autoload
(defun my-elisp-outdated-byte-compiled-p (file)
  (let* ((dest-file (byte-compile-dest-file file)))
    (and (file-newer-than-file-p file dest-file)
         `(:byte ,dest-file))))

;;;###autoload
(defun my-elisp-outdated-native-compiled-p (file)
  (and my-native-comp-flag
       (let* ((dest-file (comp-el-to-eln-filename file)))
         (and (file-newer-than-file-p file dest-file)
              `(:native ,dest-file)))))

;;;###autoload
(defun my-elisp-outdated-compiled-p (file)
  (or (my-elisp-outdated-byte-compiled-p file)
      (my-elisp-outdated-native-compiled-p file)))

;;;###autoload
(defun my-elisp-native-compile-queued-p (file)
  (and
   (member 'comp features)
   (or (gethash file comp-async-compilations)
       (assoc file comp-files-queue))))

;;;###autoload
(defun my-elisp-compile-file (file &optional load-now)
  "Compile FILE and when non-nil LOAD-NOW, load it.
Use the byte-compiled version first and asynchronously replace
with the native version, since native compilation is slow to
perform."
  (save-window-excursion
    (let* ((no-compile my-elisp-no-compile)
           (warning-minimum-level :emergency)
           ;; suppress warnings in CLI
           (byte-compile-warnings
            (if noninteractive
                '()
              (bound-and-true-p byte-compile-warnings)))
           (byte-compiled
            (and (not no-compile)
                 ;; `my-elisp-outdated-compiled-p' may return
                 ;; true because of outdated native compilation
                 ;; (`byte-recompile-file' can't be used here
                 ;; because it returns `no-byte-compile' for
                 ;; non-outdated files)
                 (if (my-elisp-outdated-byte-compiled-p file)
                     (byte-compile-file file)
                   t)))
           (load-prefer-newer t))
      (when load-now
        (load (file-name-sans-extension file)))
      byte-compiled)))

;;;###autoload
(defun my-elisp-try-loading-compiled (file)
  "Try to load FILE's compiled version.
When an error occurs (likely compilation was performed by a
different Emacs version), load FILE instead."
  (let* ((load-prefer-newer t))
    (condition-case err
        (load (file-name-sans-extension file))
      (error
       (my-println #'my-elisp-try-loading-compiled
                   (abbreviate-file-name file) err)
       (load file)))))

;;;###autoload
(defun my-elisp-source-file-p (filename)
  ;; (string-match-p (format "\\.el%s$" (regexp-opt load-file-rep-suffixes)) filename)
  (string-match-p "\\.el" (my-filename-all-extensions-as-string filename)))

(defvar my-elisp-compile-exclude-regexps
  '(
    "test[^/]*\\.el$"                   ; tests
    "\\.[^/]+\\.el$"))                    ; hidden


;;;###autoload
(defun my-elisp-recompile-file (file &optional verbose)
  (let* ((outdated (my-elisp-outdated-compiled-p file)))
    (when outdated
      (cl-destructuring-bind (type dest-file) outdated
        ;; batch sessions: when the native (but not byte) compiled file is
        ;; outdated, because recompiling takes a long time, we delete to prevent
        ;; loading it; native compilation will happen in an interactive session
        (if (and noninteractive
                 (equal type :native))
            (delete-file dest-file)
          (let* ((compile-result (my-elisp-compile-file file))
                 (not-compiled (member compile-result '(no-byte-compile))))
            (when (and (not not-compiled)
                       verbose)
              (my-println🌈 "Finished (my-elisp-compile-file \"" file "\")" compile-result))
            (and (not not-compiled)
                 compile-result)))))))

;;;###autoload
(defun my-elisp-recompile-directory (&optional dir exclude-regexps verbose)
  (interactive "D my-elisp-recompile-directory : ")
  (let* ((dir (or dir default-directory))
         (regexp-to-exclude (my-regexp-or
                             (or exclude-regexps
                                 my-elisp-compile-exclude-regexps))))
    (dolist (file (cl-remove-if (lambda (f) (string-match-p regexp-to-exclude f))
                                (directory-files dir 'full "\\.el\\'")))
      (my-elisp-recompile-file file verbose))))

;;; Autoloads handling

(defun my-elisp-generate-autoload--make (dir output-file)
  (cond
   ((fboundp #'loaddefs-generate)
    (loaddefs-generate dir output-file))
   ((fboundp #'make-directory-autoloads)
    (make-directory-autoloads dir output-file))
   (t
    (setq generated-autoload-file output-file)
    (update-directory-autoloads dir))))

(defmacro my-elisp-generate-autoload--edit (output &rest body)
  (declare (debug t) (indent defun))
  `(with-current-buffer (find-file-noselect ,output)
     (setq lexical-binding t)
     (add-file-local-variable-prop-line 'lexical-binding t)
     ;; allow compilation
     (my-delete-file-local-variables 'no-byte-compile 'no-native-compile)
     ,@body
     (save-buffer 0)
     (kill-buffer (current-buffer))))

;;;###autoload
(cl-defun my-elisp-generate-autoload (pkg-dir &key output no-provide load-now force)
  "Generate autoload file for elisp files in PKG-DIR.
Because keeping the autoload file in PKG-DIR is sometimes not
desirable. OUTPUT is used to specify path explicitly. NO-PROVIDE: remote
the `provide' footer in the generated file. LOAD-NOW: load
immediately. FORCE: always generate new."
  (let*
      ((make-backup-files nil)
       (inhibit-message t)
       ;; prevent asking about unsafe variable, which are potentially high up
       ;; in the directory hierarchy
       (enable-dir-local-variables nil)
       (feature
        (if output
            (file-name-base output)
          (format "%s-autoloads" (file-name-base pkg-dir))))
       (feature.el (format "%s.el" feature))
       (intermediate (my-filename-join pkg-dir feature.el))
       (output (or output intermediate)))
    (when (or force (not (file-exists-p output)))
      ;; (and update
      ;;      (my-some-files-newer-than-p
      ;;       output
      ;;       (directory-files pkg-dir 'full "\\.el\\'")
      ;;       #'my-file-symlink-target))
      (my-elisp-generate-autoload--make pkg-dir intermediate)

      ;; TODO: "file changed, reread from disk?" sometimes appear
      (my-elisp-generate-autoload--edit intermediate
        ;; delete the `(provide 'feature)' line, it may cause conflicts
        (when no-provide
          (my-elisp-remove-provide-in-buffer)))

      (unless (equal intermediate output)
        (rename-file intermediate output 'ok))
      (my-elisp-compile-file output))
    (when load-now
      (load (file-name-sans-extension output) nil 'quiet))
    output))

;;; my-useelisp

(defvar my-useelisp-repositories-dir/
  (my-filename-join (or (bound-and-true-p my-emacs-conf-dir/)
                        (locate-user-emacs-file "")) "repositories/"))


;; `el-get' was tried, but loading it was taking too much time

(defvar my-useelisp-default-globs '("*.el" "lisp/*.el" "extensions/*.el"))
(defvar my-useelisp-ignore-regexps
  '( ;
    "\\(^\\|[-_/]\\)test\\(s\\)?\\." ;
    "\\(^\\|[/]\\).\dir-locals\\.el$")) ;
;;


(defvar my-useelisp--registered-symlinks '()
  "List of abbreviated installed symlink paths.
Normally empty unless (`my-useelisp-should-install-p').
In that case call (`my-useelisp-install-all-registered') to
populate.")

(defvar my-useelisp-build-dir nil)

(defvar my-useelisp--build-dirs '())

(defvar my-useelisp-should-install
  (not (file-exists-p my-useelisp-build-dir)))

;;;; Install

;;;###autoload
(defun my-get-non-empty-env (var)
  (let* ((val (getenv var)))
    (and (< 0 (length val)) val)))

(defun my-useelisp-should-install-p ()
  "`my-useelisp-should-install' and some other factors."
  (or
   ;; init-file-debug noninteractive
   my-useelisp-should-install
   (my-get-non-empty-env "MY_EMACS_SYNC")))

(defun my-useelisp--expand-globs (dir globs regexp)
  (and globs
       (let* ((default-directory dir)
              (expanded-lst (my-mapcat #'file-expand-wildcards globs))
              (relative-files
               (cl-remove-if-not
                (lambda (file)
                  (and (string-match-p regexp file)
                       (not
                        (cl-some
                         (lambda (re) (string-match-p re file))
                         my-useelisp-ignore-regexps))))
                expanded-lst)))
         relative-files)))

(defun my-useelisp-install--handle-build-dir (build-dir)
  (unless (member build-dir my-useelisp--build-dirs)
    (make-directory build-dir 'parents)
    (add-to-list 'load-path build-dir)
    (add-to-list 'my-useelisp--build-dirs build-dir)))

(defun my-useelisp-install--globs-from-arg (globs)
  (cond ((null globs)
         my-useelisp-default-globs)
        ((member :defaults globs)
         (append (remove :defaults globs)
                 my-useelisp-default-globs))
        (t
         globs)))

(defvar my-useelisp--registered-source-directories '())

;;;###autoload
(cl-defun my-useelisp-install (dir
                               &rest
                               plist
                               &key
                               globs
                               globs!comp
                               regexp
                               (install (my-useelisp-should-install-p))
                               (to-dir my-useelisp-build-dir)
                               &allow-other-keys)
  "Install package from DIR in `my-useelisp-build-dir' (ensured in `load-path').
Make symbolic links from file-lists who match GLOBS (default:
`my-useelisp-default-globs') and REGEXP (default: non-hidden
file-lists) and compile them, do the same for GLOBS!COMP without
compilation.

With non-nil LOAD-NOW, load them in that order (when a list), or
the load the file-lists produced by globbing (when `t').

Normally autoloads aren't known, we must load
`my-useelisp-ensure-autoload' later (ideally when all packages
are declared with this).

PLIST."
  (my-useelisp-install--handle-build-dir to-dir)
  (push `(,dir ,@plist) my-useelisp--registered-source-directories)
  (when install
    (let* ((regexp (or regexp "\\`[^.]"))
           (files-no-compile (my-useelisp--expand-globs dir globs!comp regexp))
           (globs (my-useelisp-install--globs-from-arg globs))
           (files-compile
            (cl-set-difference
             (my-useelisp--expand-globs dir globs regexp) files-no-compile
             :test #'equal))
           (rel-files (delete-dups (append files-compile files-no-compile)))
           (file-lists
            (cl-loop
             for rel-file in rel-files collect
             (list
              rel-file
              (expand-file-name rel-file dir)
              (my-filename-join to-dir (file-name-nondirectory rel-file))))))
      ;; Make all symlinks first to ensure a proper environment for compilation
      (let* ((default-directory to-dir))
        (cl-loop
         for (rel abs link) in file-lists do
         (progn
           (when install
             (make-symbolic-link abs link 'ok))
           (push
            (abbreviate-file-name link) my-useelisp--registered-symlinks))))
      (cl-loop
       for (rel abs link) in file-lists do
       (when (and install (not (member rel files-no-compile)))
         (my-elisp-recompile-file link))
       collect (my-remove-str-fixes "" ".el" link)))))

;;;###autoload
(defun my-useelisp-ensure-autoload
    (&optional load-now force directory)
  "Like `package-quickstart-refresh'.
See `my-elisp-generate-autoload' for LOAD-NOW & FORCE."
  (interactive)
  (let* ((fn
          (lambda (force)
            (my-elisp-generate-autoload
             (or directory my-useelisp-build-dir)
             :load-now load-now
             :force force))))
    (condition-case _err
        (funcall fn (or force (my-useelisp-should-install-p)))
      ;; a source file are deleted but its symlink isn't
      ((file-missing file-error)
       (require '01-my-useelisp-x)
       (my-useelisp-refresh)
       (funcall fn t)))))


(defvar my-useelisp-files--registered-list '())

;;;###autoload
(cl-defun my-useelisp-file (pkg-name url file-name &rest plist &key
                                     (active t)
                                     (install (my-useelisp-should-install-p))
                                     &allow-other-keys)
  (let* ((pkg-name (format "%s" pkg-name))
         (pkg-path (concat my-useelisp-repositories-dir/ pkg-name "/"))
         (file-path (my-filename-join pkg-path file-name)))
    (when install
      (make-directory pkg-path 'parents)
      ;; Perform downloading when not already exist or forced ensure
      (let* ((old-local-path (concat file-path "." (format-time-string "%F"))))
        (when (file-exists-p file-path)
          (rename-file file-path old-local-path 'override))
        (benchmark 1 `(print (my-synchronouly-download-file ,url ,file-path)))
        (when (file-exists-p old-local-path)
          (cond
           ;; unsuccessful download: move back the old one
           ((not (file-exists-p file-path))
            (rename-file old-local-path file-path))
           ;; nothing new: permanently delete the old one
           ((string= (my-read-text-file old-local-path)
                     (my-read-text-file file-path))
            (delete-file old-local-path))
           ;; a new version: only trash to give a chance to revert
           (t
            (move-file-to-trash old-local-path))))))
    (push `(,pkg-name ,url ,file-name @plist)
          my-useelisp-files--registered-list)
    (cons pkg-path
          (when active
            (apply #'my-useelisp-install pkg-path plist)))))

(defvar my-useelisp-repos--registered-list '())

;;;###autoload
(cl-defun my-useelisp-repo (pkg-name url &rest plist &key branch rev (depth 1)
                                     (active t) (install (my-useelisp-should-install-p)) _globs
                                     &allow-other-keys)
  "Install PKG-NAME from URL/BRANCH.
Ensure a fully functional package inside the configuration
directory, so libraries that are not external packages that
depend on it can be carried around and used without
re-downloading PACKAGE-NAME.
See `my-useelisp-install' for keyword arguments (PLIST)."
  (declare (indent defun))
  (let* ((pkg-name (format "%s" pkg-name))
         (pkg-path (concat my-useelisp-repositories-dir/ pkg-name "/")))
    ;; goal: for performance, normally this shouldn't do anything after the
    ;; first initialization
    (when install
      (unless (file-exists-p pkg-path)
        (let* ((cmd (concat "git clone" " "
                            ;; When rev, fetch all
                            (and depth (not rev) (format "--depth=%s" depth)) " "
                            (and branch (format "-b %s" branch)) " "
                            url " " pkg-path)))
          (my-println🌈 cmd)
          (shell-command cmd)))
      (when rev
        (let* ((default-directory pkg-path)
               (prev-commit (my-vc-git-get-current-revision-commit-hash)))
          (unless (string-prefix-p rev prev-commit) ; `rev' may not be full
            (my-println🌈 default-directory ":")
            (call-process "git" nil nil nil "checkout" rev)
            (unless (string= prev-commit (my-vc-git-get-current-revision-commit-hash))
              (my-elisp-clean-compiled-files pkg-path))))))
    (push `(,pkg-name ,url ,@plist) my-useelisp-repos--registered-list)
    (cons
     pkg-path
     (when active
       (apply #'my-useelisp-install pkg-path plist)))))

;;;###autoload
(defun my-useelisp-repo-instead (pkg-name &rest args)
  "(`my-useelisp-repo' PKG-NAME @ARGS).
Use this when the default package manager can't install PKG-NAME
properly without meddling with other packages."
  (add-hook 'my-ignored-packages pkg-name)
  (apply #'my-useelisp-repo pkg-name args))

;;; 00-my-useelisp.el ends here

(provide '00-my-useelisp)

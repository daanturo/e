;; -*- lexical-binding: t; -*-

;; (my-backtrace)

(require 'dash)
(require '00-my-core-macros)

(require 'org)

;;;###autoload
(progn
  (defun my-setup-org ()

    ;; when org-mode is installed via vc
    (when (bound-and-true-p my-package-user-dir/)
      (add-to-list
       'load-path
       (file-name-concat my-package-user-dir/ "org" "lisp")))

    (setq org-directory "~/Documents/org")

    (setq evil-org-use-additional-insert nil)
    (setq evil-org-key-theme '(navigation textobjects calendar))

    (cond
     ((fboundp #'org-latex-preview-auto-mode)
      (setq-default org-startup-with-latex-preview t)
      (add-hook 'org-mode-hook #'org-latex-preview-auto-mode))
     ((fboundp #'org-fragtog-mode)
      (add-hook 'org-mode-hook #'org-fragtog-mode)
      (my-add-hook-once
        'org-mode-hook #'my-org-auto-latex-preview-fragment-mode)
      ;; TODO: until https://yhetil.org/orgmode/D2E9D483-0AFD-4AA3-AE65-B1D34DCE0CA6@getmailspring.com/T/#u
      (advice-add
       #'org-latex-preview
       :around #'my-with-ignore-message-a)))


    ;; `org-startup-with-inline-images' being set a (dir-)local variable may not
    ;; be effective since the `org-display-inline-images' call is execute inside
    ;; `org-mode' body, TODO: report?
    (add-hook
     'org-mode-local-vars-hook #'my-org-startup-with-inline-images-h)

    (add-hook 'my-idle-load-list 'org)))



;;; Appearance

(my-custom-set-faces
  `(org-level-1 :height ,(/ 4 3.0) :inherit outline-1)
  `(org-level-2 :height ,(/ 4 3.0) :inherit outline-2)
  `(org-level-3 :height ,(/ 4 3.0) :inherit outline-3)
  `(org-level-4 :height ,(/ 4 3.0) :inherit outline-4)
  `(org-level-5 :height ,(/ 4 3.0) :inherit outline-5)
  `(org-level-6 :height ,(/ 4 3.0) :inherit outline-6)
  `(org-level-7 :height ,(/ 4 3.0) :inherit outline-7)
  `(org-level-8 :height ,(/ 4 3.0) :inherit outline-8))

(my-when-graphical (my-set-org-image-actual-width))
(advice-add #'org-toggle-inline-images :before #'my-set-org-image-actual-width)

(add-hook 'my-yank-media-post-hook #'my-org-display-inline-images-maybe)

(setq org-columns-default-format
      (replace-regexp-in-string
       "%[0-9]*ITEM"
       "%ITEM"
       (my-string-insert-new-before-1st-space-or-end org-columns-default-format " %TIMESTAMP %SCHEDULE %DEADLINE")))
;; (setq org-agenda-view-columns-initially t)
;; (with-eval-after-load 'org-agenda
;;   (setf (alist-get 'todo org-agenda-prefix-format)
;;         " %(let ((scheduled (org-get-scheduled-time (point)))) (if scheduled (format-time-string \"%Y-%m-%d\" scheduled) \"\")) %i %-12:c"))

;; Stay away from Doom's Org config
;; (advice-add #'+org-init-appearance-h :override #'ignore)
;; (setq org-startup-indented nil)         ; more performant, also reduce confusion about virtual indents
;; (setq org-hide-leading-stars nil)       ; don't obfuscate the syntax

(with-eval-after-load 'org-faces
  (set-face-background 'org-column nil))

;; Be consistent with `outline'
(setq org-cycle-level-faces nil)

;; Don't hide the syntax
(setq org-link-descriptive nil)

;;; Agenda

(setq org-agenda-show-all-dates nil)
(setq org-agenda-span 60)
(setq org-agenda-todo-ignore-deadlines -1)
(setq org-deadline-past-days 1)
(setq org-scheduled-past-days 7)

;;; Other Org extensions

;; Only show minutes
(setq org-pomodoro-time-format "%.2m'")

;; Store all attached images in a flattened subdirectory
(setq-default org-download-image-dir "./images"
              org-download-heading-lvl nil)

;; NOTE: when XDG_SESSION_TYPE isn't set in a Wayland session,
;; `org-download''s inserter doesn't work

;; (my-bind :map 'org-mode-map "C-S-v"
;;   (cond
;;    ((fboundp #'yank-media)
;;     #'yank-media)
;;    ((fboundp #'org-download-enable)
;;     #'org-download-clipboard)))

;; Doom initializes using in `early-init-file'
(with-eval-after-load 'ox
  (when (and (member my-emacs-kit '(doom)) (null org-export-async-init-file))
    (setq org-export-async-init-file early-init-file)))

;;;; org-latex-preview

;; ;; this is slow since it's synchronous
;; (setq org-startup-with-latex-preview t)

;; Unicode support for given! I can't make this work for only previewing now, as the line
;; "% Intended LaTeX compiler: ..." is always inserted
(setq org-latex-compiler "lualatex")
(setq org-latex-preview-process-precompiled nil)

;; TODO: report that when using lualatex previews are too small

(with-eval-after-load 'org-latex-preview
  (-let* ((str "
\\usepackage{unicode-math}
"))
    (when (not (string-search str org-latex-preview-preamble))
      (setq org-latex-preview-preamble
            (concat org-latex-preview-preamble str)))))


;; (defun my-org--latex-dvisvgm-image-converter ()
;;   (-->
;;    org-preview-latex-process-alist
;;    (alist-get 'dvisvgm it)
;;    (plist-get it :image-converter)))
;; (setq org-preview-latex-default-process 'my-lualatex-dvisvgm)
;; (setf
;;  (alist-get 'my-lualatex-dvisvgm org-preview-latex-process-alist)
;;  `(:programs
;;    ("lualatex" "dvisvgm")
;;    :description "dvi > svg"
;;    :message "you need to install the programs: lualatex and dvisvgm."
;;    :image-input-type "dvi"
;;    :image-output-type "svg"
;;    :latex-compiler ("lualatex -interaction nonstopmode -output-directory %o %f")
;;    ;; ;; "xelatex" and "lualatex" " does not support precompilation."
;;    ;; :latex-precompiler ("%l -output-directory %o -ini -jobname=%b \"&%l\" mylatexformat.ltx %f")
;;    :image-converter ,(my-org--latex-dvisvgm-image-converter)))
;; ;; "'determined-at-runtime"
;; (with-eval-after-load 'org-latex-preview
;;   (setf (plist-get
;;          (alist-get 'my-lualatex-dvisvgm org-preview-latex-process-alist)
;;          :image-converter)
;;         (my-org--latex-dvisvgm-image-converter)))

(with-eval-after-load 'org-latex-preview
  (my-put-plist! 'org-latex-preview-appearance-options :zoom 1.25))

(when (not (file-name-absolute-p org-preview-latex-image-directory))
  (setq org-preview-latex-image-directory
        (file-name-concat my-local-share-emacs-dir/
                          "org-preview-latex-image/")))

;;; Key bindings

(add-hook
 'my-er/expand-region-before-first-expand-in-buffer-hook
 (lambda ()
   (when (derived-mode-p 'org-mode)
     (add-hook 'er/try-expand-list 'my-org-mark-inner-code-block nil 'local))))


(my-bind :map 'org-mode-map "C-*" #'my-org-insert-stars-to-make-heading)

(defvar my-org-emphasize-prefix-map (make-sparse-keymap))

(define-prefix-command #'my-org-emphasize-prefix 'my-org-emphasize-prefix-map)

(my-bind :map 'my-org-emphasize-prefix-map
  '("b" "*" "8") #'my-org-emphasize-bold
  '("i" "/")     #'my-org-emphasize-italic
  '("u" "_" "-") #'my-org-emphasize-underline
  '("v" "=")     #'my-org-emphasize-verbatim
  '("c" "~")     #'my-org-emphasize-code
  '("s" "+")     #'my-org-emphasize-strike-through
  "SPC"          #'my-org-emphasize-remove-emphasis)


(my-bind :map 'org-mode-map
  '("S-RET" "S-<return>") `(menu-item "" org-table-copy-down :filter
                                      ,(lambda (cmd)
                                         (and (org-table-check-inside-data-field 'noerror)
                                              cmd))))

;; (my-bind :map '(org-mode-map) "C-v" #'my-clipboard-yank-maybe-paste-media)

(my-bind :map 'org-mode-map '([remap org-transpose-words] [remap transpose-words]) #'my-org-tranpose-words-with-fallback)

(my-bind :map 'org-mode-map "M-l f" #'org-emphasize)
(my-bind :map 'org-mode-map "M-l p l" #'my-org-link-preview-toggle-all)

(setq org-support-shift-select t)

;;; after-org.el ends here

(provide 'my-after-org)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;; Predicates

(defun my-safe-completion-string-not-match-regexp? (regexp str)
  ;; case-sensitive removals
  (dlet ((case-fold-search nil))
    (not (string-match-p regexp str))))

(defun my-candidate-not-match-regexp?-fn (regexp)
  (lambda (cand &rest _)
    (let ((cand-str (cond ((stringp cand) cand)
                          ((consp cand) (car cand))
                          ((symbolp cand) (symbol-name cand))
                          (t cand))))
      (my-safe-completion-string-not-match-regexp? regexp cand-str))))

(defvar my-completion-style-negater " !"
  "Must be a literal for performance.
Must not contain trailing spaces so that it can be put at the
end.")
(defun my-completion-style-negater-regexp ()
  (concat my-completion-style-negater "\\( \\|\\'\\)"))

(defun my-completion-style-filter (input)
  "Return '(new-input-string filter-predicate) by processing INPUT."
  ;; "`without-regexp'": Valid patterns after " ! ": don't match any of those
  (-let* (((+str . -patterns)
           (my-completion-style--partition-patterns-from-special
            input
            ;; allow a "!" at the beginning as a "positive"
            ;; match
            my-completion-style-negater)))
    (and
     -patterns
     (list
      ;; New input to compare is stripped from " ! " onwards
      +str
      ;; For each candidate, we can reject if it matches one of those patterns
      ;; by the `some' loop, but grouping all valid patterns to check at once
      ;; is slightly faster
      (my-candidate-not-match-regexp?-fn (my-regexp-or -patterns))))))

(defun my-completion-style--partition-patterns-from-special (str special)
  "Return a list whose:
- First element is the string in STR before SPECIAL
- The rest is split patterns after SPECIAL which are valid."
  (-if-let* ((special-pos (string-search special str)))
      (cons (substring str 0 special-pos)
            (--> (substring str (+ (length special) special-pos))
                 (my-completion-style--separate it)
                 (-take-while #'my-valid-regexp? it)))
    (list str)))

(defun my-completion-style--separate (pattern)
  (cond ((not (bound-and-true-p orderless-component-separator))
         (split-string pattern " " 'omit-nulls))
        ((functionp orderless-component-separator)
         (funcall orderless-component-separator pattern))
        (t
         (split-string pattern orderless-component-separator t))))

;;; General

;; (defvar my-completion-style-filter-function #'my-completion-style-filter
;;   "The function receives an input string, return nil or a new
;; input string to compare and a predicate function.")

(defun my-completion-style--process-input-&-pred (input pred)
  "Call `my-completion-style-filter-function' on INPUT.
If it returns non-nil new input S and new pred P, return S and a
predicate that satisfies both PRED and P; Else the arguments."
  (-if-let* (((new-input new-pred) (my-completion-style-filter input)))
      (list new-input
            (cond ((and pred new-pred) (-andfn pred new-pred))
                  (t (or pred new-pred))))
    `(,input ,pred)))

(defun my-completion-style-process
    (complete-func string table pred point &optional may-change-point)
  (-let* (((new-str new-pred)
           (my-completion-style--process-input-&-pred string pred)))
    (funcall complete-func
             new-str table new-pred
             (if may-change-point
                 (min point (length new-str))
               point))))

;; Depends on `orderless'

;;;###autoload
(defun my-completion-style-try-completion (string table pred point)
  (my-completion-style-process #'orderless-try-completion string table pred point 'may-change-point))

;;;###autoload
(defun my-completion-style-all-completions (string table pred point)
  (my-completion-style-process #'orderless-all-completions string table pred point))

;;;###autoload
(defun my-completion-style-try-completion-no-flex (string table pred point)
  (dlet ((my-completion-style-dispatch-first-flex-maybe-flag nil))
    (my-completion-style-try-completion string table pred point)))

;;;###autoload
(defun my-completion-style-all-completions-no-flex (string table pred point)
  (dlet ((my-completion-style-dispatch-first-flex-maybe-flag nil))
    (my-completion-style-all-completions string table pred point)))

;;;###autoload
(add-to-list 'completion-styles-alist
             '(my-completion-style
               my-completion-style-try-completion
               my-completion-style-all-completions
               ""))

;;;###autoload
(add-to-list 'completion-styles-alist
             '(my-completion-style-no-flex
               my-completion-style-try-completion-no-flex
               my-completion-style-all-completions-no-flex
               ""))

;;;###autoload
(defun my-ivy-regex-plus (str)
  "Like `ivy--regex-plus', but with stricter splitting."
  ;; (equal ivy--regex-function #'regexp-quote)
  (-let* (((+part . -patterns)
           (my-completion-style--partition-patterns-from-special str my-completion-style-negater)))
    (let ((+part (--> (split-string +part " " 'omit-nulls)
                      (-map #'my-ensure-valid-regexp-or-literal it)
                      (-map #'my-char-fold-string-maybe it)
                      (string-join it ".*?"))))
      (if -patterns
          (cons (cons +part t)
                (-map #'list -patterns))
        +part))))

;;; Sorting

;;;###autoload
(defun my-completion-style-prefer-literal (candidates literal)
  (let* (
         ;; (components (string-split literal nil 'omit-nulls))
         ;; (first-comp (nth 0 components))
         )
    (named-let
        recur ((queue candidates) (lst0 '()) (lst1 '()))
      (-let* ((cur-cand (car queue)))
        (cond
         ((null queue)
          (append (nreverse lst0) (nreverse lst1)))
         ((or (string-search literal cur-cand)
              ;; (string-prefix-p first-comp cur-cand)
              )
          (recur (cdr queue) (cons cur-cand lst0) lst1))
         (t
          (recur (cdr queue) lst0 (cons cur-cand lst1))))))))

;;;###autoload
(defun my-vertico-sort-prefer-literal (candidates &optional component)
  "Prioritize some of CANDIDATES who includes COMPONENT literally.
PREFIX defaults to the first minibuffer input component.
CANDIDATES are sorted first. Useful for problematic candidates
such as \"`c++-mode'\".
CLOBBER `match-data'!"
  (-let* ((sorted (vertico-sort-history-length-alpha candidates))
          (component
           (or component
               (-some-->
                   (minibuffer-contents-no-properties)
                 (my-regexp-match "\\([^ \t\n\r]+\\)" it 1)))))
    (if (or (null component) (equal component (regexp-quote component)))
        sorted
      (my-completion-style-prefer-literal sorted component))))

(defvar-local my-sort-minibuffer-candidates-by-string-distance--table nil)
;;;###autoload
(defun my-sort-minibuffer-candidates-by-string-distance
    (candidates &optional input)
  (unless my-sort-minibuffer-candidates-by-string-distance--table
    (setq-local my-sort-minibuffer-candidates-by-string-distance--table
                (make-hash-table :test #'equal :size 8192)))
  (-let* ((input (or input (minibuffer-contents-no-properties))))
    (cl-sort
     candidates #'<
     :key
     (lambda (cand)
       (with-memoization
           (gethash
            (vector input cand)
            my-sort-minibuffer-candidates-by-string-distance--table)
         (string-distance cand input))))))

;;; Configurations

;;;###autoload
(progn

  (defun my-orderless-available-p ()
    (assoc 'orderless completion-styles-alist))

  (defun my-setup-orderless ()
    (with-eval-after-load 'orderless
      (my-add-list!
        'orderless-style-dispatchers
        `(my-orderless-dispatcher my-orderless-dispatch-first-flex))))

  (defun my-setup-completion-style ()

    (-let* ((orderless-flag (my-orderless-available-p)))
      ;; beware of other completion styles, they may unexpectedly show
      ;; candidates when we have filtered out all, and we may not know for sure
      ;; that the appeared candidates are the result of typed filtering; dynamic
      ;; completion tables need `basic'
      (my-after-each
        '(orderless emacs)
        (setq-default completion-styles
                      `(,@(and orderless-flag '(my-completion-style))
                        ,(and (assoc 'flex completion-styles-alist) 'flex)
                        partial-completion
                        basic

                       ;;; may not work because of flex-matching
                        ;; ,@(and (assoc 'typo completion-styles-alist) '(typo))
                        ))

        (-let* ((complete-cats
                 `((file
                    ;; flex is annoying when typing new file names
                    (styles
                     ,@(and orderless-flag '(my-completion-style-no-flex))
                     partial-completion
                     basic)))))
          (setq completion-category-defaults complete-cats)
          (dolist (elem complete-cats)
            (add-hook 'completion-category-overrides elem)))))

    (setq ido-enable-flex-matching t)

    (my-bind :map '(minibuffer-mode-map) "M-s" nil)
    (my-bind
      :map
      'minibuffer-mode-map
      "M-s r"
      #'my-orderless-toggle-literal|regexp-styles-quote)
    (my-bind :map 'minibuffer-mode-map "M-s c" #'my-completion-cycle-case-fold)
    (my-bind
      :map
      '(minibuffer-mode-map ivy-minibuffer-map)
      "M-s '"
      #'my-completion-style-toggle-char-fold-search)
    (my-bind
      :map 'ivy-minibuffer-map "M-s r" #'my-ivy-toggle-regexp-quote-and-notify)
    (my-bind
      :map 'ivy-minibuffer-map "M-s c" #'my-ivy-toggle-case-fold-and-notify)

    (my-setup-orderless)))

;; See `orderless-affix-dispatch-alist'

;;;###autoload
(defun my-orderless-dispatcher (component index total)
  (cond

   ;; Ensure that $ works with Consult commands, which add disambiguation suffixes
   ((string-suffix-p "$" component)
    (cons
     #'orderless-regexp
     (format "\\(?:%s\\|%s\\)"
             (concat (substring component 0 -1) "[\x200000-\x300000]*$")
             component)))

   ;; prefer regular expressions
   ((not (string= component (regexp-quote component)))
    #'orderless-regexp)

   ;; "!" is handled by `my-completion-style'
   ((my-orderless-dispatch-special-character
     "%" #'char-fold-to-regexp component index total))
   ((my-orderless-dispatch-special-character
     "=" #'orderless-literal component index total))

   ;;
   ))

;;;###autoload
(defun my-orderless-dispatch-special-character
    (character style component index total)
  "Assume that COMPONENT isn't a special regular expression."
  (let ((pre (string-prefix-p character component))
        (suf (string-suffix-p character component)))
    (cond
     ((and pre suf)
      #'orderless-regexp)
     (pre
      (my-orderless-regexp-whole-or-style-on-part
       style component (substring component 1)))
     (suf
      (my-orderless-regexp-whole-or-style-on-part
       style component (substring component 0 (1- (length component))))))))

;;;###autoload
(defun my-orderless-regexp-whole-or-style-on-part (style component part)
  "Matching STYLE on PART and the regexp version of COMPONENT."
  (cons
   #'orderless-regexp (format "\\(?:%s\\|%s\\)" (funcall style part) component)))

(defvar-local my-completion-style-dispatch-first-flex-maybe-flag t
  "Non-nil means disable `my-orderless-dispatch-first-flex'.
It's annoying when all patterns are used for flex-matching.")

;;;###autoload
(defun my-orderless-dispatch-first-flex (_pattern index _total)
  "Let the first component, when \"easy to type\" (likely not a regexp) `flex'.
Only when INDEX = 0. This function may be just a branch of another
grander dispatcher, but we split it case it can be controlled in another
way."
  (and my-completion-style-dispatch-first-flex-maybe-flag
       ;; (not (member (my-minibuffer-completion-category) '(file)))
       (zerop index)
       #'orderless-flex))

(defvar-local my-orderless-only-literal nil)
;;;###autoload
(defun my-orderless-toggle-literal|regexp-styles-quote ()
  (interactive)
  (setq-local my-orderless-only-literal (not my-orderless-only-literal))
  (if my-orderless-only-literal
      (setq-local orderless-matching-styles '(orderless-literal)
                  orderless-style-dispatchers nil)
    (setq-local orderless-matching-styles (default-value 'orderless-matching-styles)
                orderless-style-dispatchers (default-value 'orderless-style-dispatchers))))

;;;###autoload
(defun my-completion-cycle-case-fold ()
  (interactive)
  (cond
   ((bound-and-true-p orderless-smart-case)
    (my-completion-to-case-sensitive)
    (message "Case-sensitive"))
   ((not completion-ignore-case)
    (my-completion-to-case-insensitive)
    (message "Case-insensitive"))
   (completion-ignore-case
    (my-completion-to-smart-case)
    (message "Smart case"))))

;;;###autoload
(defun my-completion-to-case-sensitive (&optional global)
  (interactive)
  (my-lexical-eval
   `(,(if global 'setq-default 'setq-local)
     ,@(my-flatten-1 my-completion-case-sensitive-variables))))
;;;###autoload
(defun my-completion-to-case-insensitive (&optional global)
  (interactive)
  (my-lexical-eval
   `(,(if global 'setq-default 'setq-local)
     ,@(my-flatten-1 my-completion-case-insensitive-variables))))
;;;###autoload
(defun my-completion-to-smart-case (&optional global)
  (interactive)
  (my-lexical-eval
   `(,(if global 'setq-default 'setq-local)
     ,@(my-flatten-1 my-completion-smart-case-variables))))

;;;###autoload
(defconst my-completion-case-sensitive-variables
  `((orderless-smart-case nil)
    (completion-ignore-case nil)))
;;;###autoload
(defconst my-completion-case-insensitive-variables
  `((orderless-smart-case nil)
    (completion-ignore-case t)))
;;;###autoload
(defconst my-completion-smart-case-variables
  `((orderless-smart-case t)
    (completion-ignore-case (default-value 'completion-ignore-case))))

(defvar-local my-completion-style-char-fold-search t
  "See also `search-default-mode'.")
;;;###autoload
(defun my-completion-style-toggle-char-fold-search ()
  (interactive)
  (setq-local my-completion-style-char-fold-search
              (not my-completion-style-char-fold-search))
  (message "`%s': %s" 'my-completion-style-char-fold-search
           my-completion-style-char-fold-search)
  my-completion-style-char-fold-search)

;;;###autoload
(defun my-char-fold-string-maybe (str)
  (if (and my-completion-style-char-fold-search
           (not (my-string-special-regexp|not-literal-p str)))
      (char-fold-to-regexp str)
    str))

;;;###autoload
(defun my-completion-style-disable-flex-local ()
  (setq-local my-completion-style-dispatch-first-flex-maybe-flag nil)
  (setq-local completion-styles
              (-map
               (lambda (style)
                 (cond
                  ((equal 'flex style)
                   'partial-completion)
                  (:else
                   style)))
               completion-styles)))

(provide 'my-completion-style)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-leader2-key-menu-item-kmi (_)
  (cond
   ((and (my-equal* ?\; (char-before) last-command-event)
         (equal #'self-insert-command last-command))
    #'self-insert-command)
   (:else
    #'my-leader2)))

;;;###autoload
(defun my-leader-or-vi-refresh-prefer-insert (&rest _)
  (interactive)
  (my-safe-call #'my-evil-or-leader-mode-refresh)
  (when (and (bound-and-true-p evil-local-mode)
             (not (member evil-state my-evil-typing-state-list)))
    (evil-insert-state)))

;;;###autoload
(define-minor-mode my-leader-spc-key-mode
  nil
  :global nil
  :keymap `((,(kbd "SPC") . my-leader-spc-prefix-cmd)))

(defvar my-leader-spc-key-exlude-mode-list '())

;;;###autoload
(defun my-leader-spc-key-mode-maybe ()
  (when (not (my-derived-mode-p my-leader-spc-key-exlude-mode-list))
    (my-leader-spc-key-mode)))

;;; my-leader.el ends here

(provide 'my-leader)

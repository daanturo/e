;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defun my-extra-completion-styles-always-match-try-completion
    (_string _table _pred point)
  (cons "" point))

;; NOT WORK 😭

(defun my-extra-completion-styles-always-match-all-completions
    (string table _pred _point)
  (all-completions
   ;; ""
   string
   table))

(add-to-list
 'completion-styles-alist
 `(my-extra-completion-styles-always-match
   my-extra-completion-styles-always-match-try-completion
   my-extra-completion-styles-always-match-all-completions
   "Always match, let the dynamic completion table handle filtering."))

;;; my-extra-completion-styles.el ends here

(provide 'my-extra-completion-styles)

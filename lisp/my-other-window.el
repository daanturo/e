;; -*- lexical-binding: t; -*-

;;;###autoload
(defun my-ace-window-dispatch (char)
  "Read more than single characters"
  (let ((alist `((left . windmove-left)
                 (down . windmove-down)
                 (up . windmove-up)
                 (right . windmove-right)
                 ;; "M-o M-o" for `other-window'
                 (,(aref (kbd "M-o") 0) . other-window))))
    (if-let ((cmd (alist-get char alist)))
        (progn (call-interactively cmd)
               (throw 'done 'exit))
      (aw-dispatch-default char))))

;;;###autoload
(progn
  (defun my-setup-other-window ()
    (my-bind
      :map
      '(global-map ;
        diff-mode-map html-mode-map ivy-minibuffer-map
        ;;
        )
      '("M-o") #'my-other-window)
    (with-eval-after-load 'ace-window
      (setq aw-scope 'frame)
      (setq aw-dispatch-function #'my-ace-window-dispatch)
      (my-add-list!
        'aw-dispatch-alist
        `(
          ;; `(,(aref (kbd "M-o") 0) other-window)
          (?a windmove-left)
          (?s windmove-down)
          (?w windmove-up)
          (?d windmove-right)
          ;;
          (?b windmove-left)
          (?n windmove-down)
          (?p windmove-up)
          (?n windmove-right)
          ;;
          (?h windmove-left)
          (?j windmove-down)
          (?k windmove-up)
          (?l windmove-right)
          ;;
          ))
      ;; `my-custom-set-faces' & foreground color?
      (set-face-attribute 'aw-leading-char-face nil :height 3.0 :box t))))

;;;###autoload
(defun my-other-window (arg)
  (interactive "p")
  (cond
   ((and (window-minibuffer-p (selected-window))
         (window-live-p (minibuffer-selected-window)))
    (select-window (minibuffer-selected-window)))
   ;; ((active-minibuffer-window)
   ;;  (select-window (active-minibuffer-window)))
   ;; Force selecting the sidebar
   ((eq 2 (length (window-list)))
    (select-window (next-window)))
   ((fboundp 'ace-window)
    (ace-window arg))
   (t
    (other-window arg))))

(provide 'my-other-window)

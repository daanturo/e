;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


(my-add-list!
  'er/try-expand-list
  `(
    ;;

    ;; my-mark-filename-balanced-component
    ;; my-mark-balanced-non-space

    ;;   Having this in the global list makes it easier to visually compare,
    ;; also eliminates the need for hook
    ,@(and (bound-and-true-p global-subword-mode) '(er/mark-subword))

    ;; my-er/treesit-mark-bigger-list-or-node
    ))

(setq-default expand-region-subword-enabled t)
(with-eval-after-load 'subword-mode-expansions
  (remove-hook 'subword-mode-hook 'er/add-subword-mode-expansions))

;;; after-expand-region-core.el ends here

(provide 'my-after-expand-region-core)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(progn

  (defun my-citar-capf-setup-h ()
    (add-hook 'completion-at-point-functions #'citar-capf nil 'local))

  (defun my-setup-citar ()

    (cond
     ((bound-and-true-p org-cite-global-bibliography)
      (setq-default citar-bibliography org-cite-global-bibliography))
     (:else
      (with-eval-after-load 'oc
        (setq-default citar-bibliography org-cite-global-bibliography))))

    (add-hook 'LaTeX-mode-hook 'my-citar-capf-setup-h)
    (add-hook 'org-mode-hook 'my-citar-capf-setup-h)

    ;;
    ))

;; https://github.com/emacs-citar/citar/issues/802

(setq citar-select-multiple nil)        ; "<C-i>" error

;; ;; (when (display-graphic-p)
;; ;;   (setq citar--multiple-setup (cons "<tab>" "<return>")))

;; (defun my-citar-multiple-workaround-C-i-a (func &rest args)
;;   (dlet ((citar--multiple-setup
;;           (cond
;;            ((display-graphic-p)
;;             (cons "<tab>" "<return>"))
;;            (:else
;;             citar--multiple-setup))))
;;     (apply func args)))
;; (dolist (func '(citar--multiple-exit citar--setup-multiple-keymap))
;;   (advice-add func :around #'my-citar-multiple-workaround-C-i-a))

(with-eval-after-load 'org-roam
  (my-safe-call #'citar-org-roam-mode))
(with-eval-after-load 'embark
  (my-safe-call #'citar-embark-mode))

;;; my-after-citar.el ends here

(provide 'my-after-citar)

;; Local Variables:
;; no-byte-compile: t
;; End:

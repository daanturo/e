;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-set-system-theme-by-emacs-theme--a (func &rest args)
  (if this-command
      (let ((old-bg-mode (frame-parameter nil 'background-mode)))
        (prog1
            (apply func args)
          (let ((new-bg-mode (frame-parameter nil 'background-mode)))
            (unless (equal old-bg-mode new-bg-mode)
              (my-set-system-theme-by-emacs-theme)))))
    (apply func args)))

(defvar my-old-system-theme nil "May be a color scheme instead.")

;;;###autoload
(defun my-set-system-theme (&optional bg-mode)
  (start-process-shell-command
   "" nil
   (pcase (getenv "XDG_CURRENT_DESKTOP")
     ("KDE"
      (concat "QT_QPA_PLATFORM='wayland;xcb' plasma-apply-colorscheme "
              (cond
               ((equal 'light bg-mode) "BreezeLight")
               ((equal 'dark bg-mode) "BreezeDark")
               (t my-old-system-theme))))
     (_ ""))))

;; busctl --user call org.freedesktop.portal.Desktop
;; /org/freedesktop/portal/desktop org.freedesktop.portal.Settings Read
;; ss "org.freedesktop.appearance" "color-scheme"


;; (pcase (getenv "XDG_CURRENT_DESKTOP")
;;   ("KDE" (-->
;;           "QT_QPA_PLATFORM='wayland;xcb' plasma-apply-colorscheme --list-schemes"
;;           (shell-command-to-string it)
;;           (string-lines it)
;;           (-find (lambda (line) (string-search "(current color scheme)" line)) it)
;;           (my-regexp-match "[a-z]+" it 0)))
;;   (_ ""))

(provide 'my-functions-system-theme)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;; Legacy functions

;;;###autoload
(defun my-counsel-rg||ripgrep-all ()
  (interactive)
  (dlet ((counsel-rg-base-command
          (my-inserted-at counsel-rg-base-command 1
                          "--hidden"
                          "--no-ignore")))
    (call-interactively #'counsel-rg)))

;;;###autoload
(defun my-counsel-rg||ripgrep-multiline-dotall (initial-directory)
  (interactive "P")
  (let ((initial-directory (if current-prefix-arg
                               (read-directory-name "In directory: ")
                             nil)))
    (counsel-rg "-U --multiline-dotall --" initial-directory)))

;;; Non-interactive functions

;;;###autoload
(cl-defun my-rgrep-grep->string (regex &key timeout)
  "Synchronously return grepped string for extended REGEX in current directory.
With non-nil TIMEOUT, return '`timeout' when it is exceeded. Ignore dot files."
  (-let* ((cmdargs
           (cond
            ((executable-find "rg")
             '("rg"
               "--no-filename"
               "--color=never"
               "--no-line-number"
               "--regexp"))
            ((executable-find "grep")
             '("grep"
               "--no-filename"
               "--color=never"
               "--recursive"
               "--exclude=.*"
               "--extended-regexp")))))
    (with-timeout ((or timeout most-positive-fixnum)
                   (if timeout
                       'timeout
                     ""))
      (my-process-sync->output-string `(,@cmdargs ,regex)))))

;;; Current functions

;;;###autoload
(defun my-rgrep-intial-directory (arg)
  "ARG: `stringp': use it, `nil': project root, a list: ask.
Else `default-directory'."
  (cond
   ((stringp arg)
    arg)
   ((null arg)
    (my-project-root))
   ((listp arg)
    (read-directory-name "Search from directory:"))
   (t
    default-directory)))

(defvar my-rgrep-include-hidden nil)
;;;###autoload
(put 'my-rgrep-include-hidden 'safe-local-variable #'booleanp)

;;;###autoload
(cl-defun my-rgrep-ripgrep|rg-initial-input (&key (--opts nil) tests)
  "Generate options for ripgrep/rg.
--OPTS: puts options on the right of \"--\", instead of the left.
  TESTS: do not exclude test/ in globs."
  (-->
   (concat
    ;; terms -- opts
    (and --opts " -- ")
    ;; Only search in files with the same current extension by default
    (-some--> buffer-file-name (file-name-extension it) (format " -g *.%s" it))
    ;; Exclude tests
    (and (not tests) " -g !test/** ")
    " -g !.git/ "
    ;; indicate that those flags are enabled by default
    (if my-rgrep-include-hidden
        " --hidden "
      " --no-hidden ")
    " --no-follow "
    ;; Enable multi-line matching
    " -U "
    ;; deterministic order
    " --sort path "
    ;; opts -- terms
    (if --opts
        ""
      " -- "))
   ;; for `counsel-rg', the options part must not have spaces before
   (if --opts
       it
     (string-trim-left it))
   (replace-regexp-in-string " +" " " it)))

(defvar my-rgrep-insert-initial t)

;;;###autoload
(defun my-rgrep-ripgrep (&optional init-dir)
  (interactive)
  (-let* ((init-input
           (and my-rgrep-insert-initial
                (my-rgrep-ripgrep|rg-initial-input :--opts t))))
    (minibuffer-with-setup-hook (lambda ()
                                  (when my-rgrep-insert-initial
                                    (let ((inp (minibuffer-contents)))
                                      (my-replace-buffer-string
                                       (concat
                                        inp
                                        (substring
                                         inp
                                         0
                                         (string-match-p " " inp)))))
                                    (search-backward " -- ")))
      (cond
       ((fboundp #'consult-ripgrep)
        (consult-ripgrep init-dir init-input))
       ((fboundp #'counsel-rg)
        (counsel-rg init-input init-dir))))))

;;;###autoload
(defun my-rgrep-recursive-grep-project-or-cwd (&optional dir)
  (interactive "P")
  (-let* ((init-dir (my-rgrep-intial-directory dir)))
    (cond ((executable-find "rg")
           (my-rgrep-ripgrep init-dir))
          (t
           (consult-grep init-dir)))))

;;;###autoload
(defun my-rgrep-current-directory ()
  (interactive)
  (my-rgrep-recursive-grep-project-or-cwd default-directory))

;;;###autoload
(defun my-rgrep-simple (&optional dir)
  "Interactively search in DIR without splitting and initial input."
  (interactive (list (if current-prefix-arg
                         (read-directory-name "rgrep in: ")
                       (my-project-root))))
  (dlet ((consult-async-split-style nil)
         (my-rgrep-insert-initial nil))
    (my-rgrep-ripgrep dir)))

;;;###autoload
(defun my-rgrep-ripgrep-all-rga-current-directory-cwd ()
  (interactive)
  (cl-assert (executable-find "rga"))
  (dlet ((consult-ripgrep-args
          (-->
           (my-get-default-value-of-symbol 'consult-ripgrep-args)
           (replace-regexp-in-string "^rg " "rga " it))))
    (my-rgrep-ripgrep default-directory)))

(provide 'my-functions-rgrep)

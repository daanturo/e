;; -*- lexical-binding: t; -*-

(require 'dash)
(require 'dash-docs)

(defvar-local dash-docs-docsets nil)


;;;###autoload
(progn
  (defvar my-dash-docs--local-docsets-alist '())
  (defvar my-dash-docs-add-local--adviced nil)
  ;; `set-docsets!' only applies for future buffers
  (defun my-dash-docs-add-local (mode/s &rest docsets)
    (declare (indent defun))
    (unless my-dash-docs-add-local--adviced
      (my-add-advice/s '(dash-docs-browse-url consult-dash +lookup/in-docsets)
        :before #'my-dash-docs-apply-local)
      (setq my-dash-docs-add-local--adviced t))
    (dolist (mode (ensure-list mode/s))
      (push (cons mode docsets)
            my-dash-docs--local-docsets-alist))))

;;;###autoload
(defun my-dash-doc-installed-p (docset)
  (or (dash-docs-docset-installed-p docset)
      (dash-docs-docset-path docset)))

;;;###autoload
(defun my-dash-docs-apply-local (&rest _)
  "Can also act as a :before advice."
  (-let* ((docsets (my-get-alist-all-by-mode
                    my-dash-docs--local-docsets-alist)))
    (setq-local dash-docs-docsets
                (--> (append dash-docs-docsets docsets)
                     -uniq
                     (-filter #'my-dash-doc-installed-p it)))))

;;;###autoload
(defun my-dash-docs-at-point (&optional all)
  "Dash docs at point with filters."
  (interactive "P")
  (dlet ((major-docset (my-get-major-docset major-mode))
         (dash-docs-docsets (my-local-docsets-to-query major-mode all)))
    (when (and major-docset
               (not (dash-docs-docset-installed-p major-docset))
               (y-or-n-p (concat "Download Dash doc " major-docset "? "
                                 "Invoke this again when the installation finishes.")))
      (my-install-dash-docset major-docset))
    (consult-dash (my-thing-at-point-dwim))))

;;;###autoload
(cl-defun my-local-docsets-to-query (&optional (mode major-mode) all)
  (if all
      (dash-docs-installed-docsets)
    (--> (cons (my-get-major-docset mode)
               (-filter #'dash-docs-docset-installed-p dash-docs-docsets))
         (-remove #'null it)
         -uniq)))

;;;###autoload
(cl-defun my-get-major-docset (&optional (mode major-mode))
  (--> (or dash-docs-docsets (dash-docs-installed-docsets))
       (-filter (lambda (ds)
                  (string-search (downcase (my-prog-lang-of-mode mode))
                                 (downcase ds)))
                it)
       (sort it #'my-by-length<)
       car))

;;;###autoload
(defun my-update-dash-docsets (&optional no-async)
  (interactive)
  (-let* ((cmd "dasht-docsets-update"))
    (if no-async
        (shell-command cmd)
      (async-shell-command cmd))))

(provide 'my-functions-dash-docs)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'starhugger)

(add-hook 'minions-prominent-modes 'starhugger-inlining-mode)
(add-hook 'minions-prominent-modes 'starhugger-auto-mode)

;; (my-bind :map 'starhugger-inlining-mode-map '("M-RET" "M-<return>") #'starhugger-show-fetched-suggestions)

(my-bind :map 'starhugger-inlining-mode-map '("TAB" "<tab>" "<backtab>") (starhugger-inline-menu-item #'starhugger-accept-suggestion))
(my-bind :map 'starhugger-inlining-mode-map "M-[" (starhugger-inline-menu-item #'starhugger-show-prev-suggestion))
(my-bind :map 'starhugger-inlining-mode-map "M-]" (starhugger-inline-menu-item #'starhugger-show-next-suggestion))

(my-bind :map 'starhugger-inlining-mode-map '("C-f") (starhugger-inline-menu-item #'starhugger-accept-suggestion-by-character))
(my-bind :map 'starhugger-inlining-mode-map '("M-f") (starhugger-inline-menu-item #'starhugger-accept-suggestion-by-word))
(my-bind :map 'starhugger-inlining-mode-map '("C-e") (starhugger-inline-menu-item #'starhugger-accept-suggestion-by-line))
(my-bind :map 'starhugger-inlining-mode-map '("M-}") (starhugger-inline-menu-item #'starhugger-accept-suggestion-by-paragraph))

(add-hook 'starhugger-inlining-mode-hook #'my-starhugger-inlining-mode-h)

;; when left by default, the returned snippet is too short to understand the idea
(setq starhugger-max-new-tokens '(64 96))

;; (setq starhugger-max-prompt-length (* 1024 6))
(setq starhugger-max-prompt-length (* 1024 1))
;; (setq starhugger-enable-dumb-grep-context t) ; depend on the model

;; https://github.com/huggingface/text-generation-inference/issues/1124
;; (setq starhugger-fill-in-the-middle nil)

;; (advice-add
;;  #'starhugger--prompt-build-components
;;  :around #'my-starhugger--prompt-build-components-filename-a)

(set-popup-rule! "\\`\\*starhugger-suggestions.*\\*" :ignore t)
(set-popup-rule! "\\`\\*starhugger[^*]*\\*" :height 0.5 :modeline nil)

;; ;;;###autoload
;; (defun my-starhugger-try-to-set-alternative-model ()
;;   (interactive)
;;   (my-starhugger-test-alternative-model-availbility-then-set
;;    "https://api-inference.huggingface.co/models/bigcode/starcoderplus"
;;    (lambda ()
;;      (my-starhugger-test-alternative-model-availbility-then-set
;;       "https://api-inference.huggingface.co/models/bigcode/starcoderplus-megatron"))))

;; (my-add-hook-once
;;   'starhugger-before-request-hook 'my-starhugger-try-to-set-alternative-model)

(defcustom my-starhugger-ollama-model nil
  nil
  :type 'string)

(defcustom my-starhugger-HF-model nil
  nil
  :type 'string)

;;;###autoload
(defun my-starhugger-set-token (&optional token persist)
  "Set `starhugger-api-token' to TOKEN, prefix arg to PERSIST to `custom-file'."
  (interactive (list
                (read-string "Leave empty for the default: ")
                current-prefix-arg))
  (setq starhugger-api-token
        (if (and token (< 0 (length token)))
            token
          (my-get-cached-auth-or-pass "huggingface" "token")))
  (when persist
    (require 'starhugger)
    (custom-set-variables (list 'starhugger-api-token starhugger-api-token))
    (custom-save-all)))

(defun my-starhugger-use-hugging-face ()
  (interactive)
  (when my-starhugger-HF-model
    (setq! starhugger-model-id my-starhugger-HF-model))
  (setq starhugger-completion-backend-function
        #'starhugger-hugging-face-inference-api)
  (my-starhugger-set-token))

(defun my-starhugger-use-ollama ()
  (interactive)
  (when my-starhugger-ollama-model
    (setq! starhugger-model-id my-starhugger-ollama-model))
  (setq starhugger-completion-backend-function
        #'starhugger-ollama-completion-api))

(defun my-starhugger-set-completion-backend (&optional force-HF)
  (cond
   (force-HF
    (my-starhugger-use-hugging-face))
   ((my-net-check-port-open-p 11434)
    (my-starhugger-use-ollama))
   (:else
    (my-starhugger-use-hugging-face))))

;; (defun my-starhugger-completion-backend (&rest args)
;;   (my-starhugger-set-completion-backend)
;;   (apply starhugger-completion-backend-function args))
;; (setq starhugger-completion-backend-function #'my-starhugger-completion-backend)

;; ;; put this at the end of file, otherwise any errors in typing will stop loading
;; (unless starhugger-api-token
;;   (my-add-hook-once 'starhugger-before-request-hook #'my-starhugger-set-token))

;;;###autoload
(defun my-starhugger-lazy-setup (&rest _)
  (my-starhugger-set-completion-backend))


(my-add-advice-once
  #'starhugger-trigger-suggestion
  :before #'my-starhugger-lazy-setup)

(add-hook 'my-setup-expand-snippet-after-tab-completion-last-commands #'starhugger-accept-suggestion)

;;; after-starhugger.el ends here

(provide 'my-after-starhugger)

;; Local Variables:
;; no-byte-compile: t
;; End:

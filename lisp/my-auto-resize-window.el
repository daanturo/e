;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(defvar my-auto-resize-window-width-fraction (/ 1.0 5))
(defvar my-auto-resize-window-height-fraction nil)

(defun my-auto-resize-window--h (&optional wd)
  (vector
   (when my-auto-resize-window-width-fraction
     (-let* ((target
              (ceiling (* my-auto-resize-window-width-fraction (frame-width))))
             (delta (- target (window-width wd))))
       (window-resize wd delta t))))
  (when my-auto-resize-window-height-fraction
    (-let* ((target
             (ceiling (* my-auto-resize-window-height-fraction (frame-height))))
            (delta (- target (window-height wd))))
      (window-resize wd delta nil))))

;;;###autoload
(define-minor-mode my-auto-resize-window-mode
  nil
  :global nil
  (if my-auto-resize-window-mode
      (progn
        (add-hook 'window-size-change-functions #'my-auto-resize-window--h
                  nil
                  'local))
    (progn
      (remove-hook 'window-size-change-functions #'my-auto-resize-window--h
                   'local))))

;;; my-auto-resize-window.el ends here

(provide 'my-auto-resize-window)

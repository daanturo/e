;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'xdg)
(require 'mailcap)

;;;###autoload
(defun my-xdg-open-file-with-default-application (filename)
  "`browse-url-of-file' FILENAME, but ignore `browse-url-filename-alist'."
  (interactive (list (my-buffer-filename t)))
  (dlet ((browse-url-default-handlers nil))
    ;; `browse-url-of-file' doesn't accept "~.*" files
    (browse-url-of-file (expand-file-name filename))))

(defun my-xdg-open-with--file-path-at-point ()
  (-some-->
      (or (dired-file-name-at-point) (thing-at-point 'filename))
    (expand-file-name it)
    (file-truename it)))

;;;###autoload
(cl-defun my-xdg-get-app-exec-command-args (exec-format target &optional target-to-list-fn &key app-path)
  "Return the command line arguments to open TARGET from EXEC-FORMAT.
TARGET, if a file, must not contain the home \"~\" prefix; TARGET can be
a list of files or URLs. TARGET-TO-LIST-FN: function that transforms
TARGET into a list of command line arguments, since some apps such as
Firefox requires \"--new-window\" before the URL. APP-PATH is the
.desktop files to read instead of using EXEC-FORMAT directly."
  (-let*
      ((target-to-list-fn (or target-to-list-fn #'ensure-list))
       (target-args (funcall target-to-list-fn target))
       ;; https://specifications.freedesktop.org/desktop-entry-spec/latest/exec-variables.html
       (spec-alist
        `(("%f" . ,target-args)
          ("%F" . ,target-args)
          ("%u" . ,target-args)
          ("%U" . ,target-args)
          ("%i" . ())
          ("%c" . ())
          ("%k" . ())))
       (lst
        (split-string-shell-command
         (if app-path
             (gethash "Exec" (xdg-desktop-read-file app-path))
           exec-format))))
    (-mapcat
     (lambda (elem)
       (-let* ((replacing-args (cdr (assoc elem spec-alist))))
         (cond
          (replacing-args
           replacing-args)
          (:else
           (list elem)))))
     lst)))

(defun my-xdg-open-with--applications-for-file (filename)
  (-when-let* (
               ;; (_extn (file-name-extension filename))
               (file-path (expand-file-name filename))
               (mime-typ (mailcap-file-name-to-mime-type file-path))
               (apps (xdg-mime-apps mime-typ)))
    (-some-->
        (-map #'xdg-desktop-read-file apps)
      (-map
       (lambda (ht)
         (puthash
          :command-args
          (my-xdg-get-app-exec-command-args
           (gethash "Exec" ht) file-path)
          ht)
         ht)
       it))))

(defun my-xdg--exec (cmdargs &optional name buf)
  (apply #'start-process (or name (format "%S" cmdargs)) buf cmdargs))

;;;###autoload
(defun my-xdg-open-file-with-completing-read (&optional file-path desktop-contents)
  (interactive)
  (-let* ((file-path (or file-path (my-xdg-open-with--file-path-at-point)))
          (desktop-contents
           (or desktop-contents
               (my-xdg-open-with--applications-for-file file-path)))
          (names (-map (lambda (ht) (gethash "Name" ht)) desktop-contents))
          (table
           (-map (lambda (ht) (list (gethash "Name" ht) ht)) desktop-contents))
          (annot-fn
           (lambda (name)
             (-let* (((_ ht) (assoc name table))
                     ((&hash "Comment" app-comment "Exec" app-exec) ht))
               (-->
                (format "\t%s\t%S" (or app-comment "") app-exec)
                (propertize it 'face 'completions-annotations)))))
          (app
           (completing-read
            (format "Open %S with: " (file-name-nondirectory file-path))
            (lambda (string pred action)
              (if (eq action 'metadata)
                  `(metadata
                    (display-sort-function . identity)
                    (annotation-function . ,annot-fn))
                (complete-with-action action names string pred))))))
    (-let* (((_ ht) (assoc app table))
            ((&hash :command-args cmdargs) ht))
      (my-xdg--exec cmdargs))))

;;;###autoload
(defun my-xdg-open-with-context-menu-pre-expand (menu &optional _click)
  (-when-let* ((file-path (my-xdg-open-with--file-path-at-point)))
    (-let* ((sub-menu (make-sparse-keymap)))
      (define-key-after
        sub-menu
        [completing-read]
        `(menu-item "(completing-read)" my-xdg-open-file-with-completing-read))
      (dolist (desktop-ht (my-xdg-open-with--applications-for-file file-path))
        (-let* (((&hash
                  "Name"
                  app-name
                  "Comment"
                  app-comment
                  "Exec"
                  _app-exec
                  :command-args cmdargs)
                 desktop-ht))
          (define-key-after sub-menu (vector (format "%S" desktop-ht))
            `(menu-item
              ,app-name
              ,(lambda ()
                 (interactive)
                 (my-xdg--exec cmdargs))))))
      (define-key-after
        menu [my-xdg-open-with-context-menu-pre-expand] `(menu-item "Open with" ,sub-menu))))
  menu)

;;;###autoload
(defun my-xdg-open-with-context-menu-completing-read (menu &optional _click)
  (-when-let* ((file-path (my-xdg-open-with--file-path-at-point)))
    (define-key
     menu
     [my-open-with-context-menu-completing-read--separator]
     menu-bar-separator)
    (define-key
     menu
     [my-xdg-open-with-context-menu-completing-read]
     `(menu-item "Open with" my-xdg-open-file-with-completing-read)))
  menu)

;; ;;;###autoload
;; (defun my-xdg-mime-apps (mimetype)
;;   (-let* ((conf-files (--> (xdg-mime-apps-files) (-filter #'file-exists-p it)))
;;           (regexp
;;            (rx-to-string
;;             `(seq
;;               bol
;;               "[Added Associations]"
;;               (*? anychar)
;;               bol
;;               ,(concat mimetype "=")
;;               (group (regexp "\\(?:[^;]+?;\\)+?$")))))
;;           (apps
;;            (-->
;;             (save-match-data
;;               (-map
;;                (lambda (conf-f)
;;                  (-let* ((file-str (f-read-text conf-f)))
;;                    (and (string-match regexp file-str)
;;                         (match-string 1 file-str))))
;;                conf-files))
;;             (delete nil it)
;;             (-mapcat (lambda (line) (split-string line ";" t)) it)
;;             (delete-dups it))))
;;     apps))

;;;###autoload
(defun my-xdg-build-app-name-regexp (app-name-or-re)
  (format "\\<%s\\>[^/]*\\.desktop$" app-name-or-re))

;;;###autoload
(defun my-xdg-open-file-and-parent-directory (url)
  (interactive (browse-url-interactive-arg "URL: "))
  (-let* ((parent (file-name-parent-directory url)))
    (browse-url-xdg-open url)
    (browse-url-xdg-open parent)))

(defvar my-xdg-browser-new-window-option-table '(("firefox" . "--new-window")))

(defun my-xdg-browser-get-new-window-args-function (app)
  "Get from `my-xdg-browser-new-window-option-table' and APP.
MODIFY `match-data'!"
  (-some
   (-lambda ((app1 . nw-arg))
     (-let* ((regexp (my-xdg-build-app-name-regexp app1)))
       (and (string-match regexp app)
            (lambda (args1) (append (list nw-arg) (ensure-list args1))))))
   my-xdg-browser-new-window-option-table))

(defvar my-xdg-browse-url-known-interceptors '())
(add-hook 'my-xdg-browse-url-known-interceptors "Junction") ; https://apps.gnome.org/Junction/ re.sonny.Junction

;;;###autoload
(defun my-xdg-guess-web-url-p (str)
  (save-match-data
    (or (string-match "\\`https?://" str)
        (not
         (or
          ;; a file path
          (string-match "\\`/" str)
          ;; looks like not supported
          ;; (string-match "\\`~" str)
          ;; already a proper URI?
          (string-match "\\`[^.]+:." str))))))

;;;###autoload
(defun my-xdg-browse-url* (url &optional new-window &rest args)
  "(`browse-url' URL NEW-WINDOW @ARGS) but try to by pass some interceptors."
  (interactive (browse-url-interactive-arg "URL: "))
  (cond
   ((not (my-xdg-guess-web-url-p url))
    (apply #'browse-url-default-browser url new-window args))
   (:else
    (-let* ((regexp
             (my-xdg-build-app-name-regexp
              (regexp-opt my-xdg-browse-url-known-interceptors)))
            (apps (xdg-mime-apps "x-scheme-handler/https"))
            (interceptor-flag (string-match-p regexp (nth 0 apps))))
      (cond
       ;; won't (likely) be intercepted: proceed normally
       ((not interceptor-flag)
        (apply #'browse-url-default-browser url new-window args))
       (:else
        (-let* ((direct-app
                 (save-match-data
                   (dlet ((case-fold-search nil))
                     (-find
                      (lambda (app) (not (string-match regexp app)))
                      (-slice apps 1))))))
          (cond
           ((null direct-app)
            (apply #'my-browse-url-default-browser-no-xdg-open
                   url
                   new-window
                   args))
           (:else
            (condition-case _err
                ;; an alternative: let's try
                (-let* ((ht (xdg-desktop-read-file direct-app))
                        (cmdargs
                         (my-xdg-get-app-exec-command-args
                          (gethash "Exec" ht) url
                          (and new-window
                               (my-xdg-browser-get-new-window-args-function
                                direct-app)))))
                  (my-xdg--exec cmdargs))
              (error
               ;; fallback: just accept the interceptor
               (apply #'browse-url-default-browser
                      url
                      new-window
                      args))))))))))))

;;; my-functions-xdg.el ends here

(provide 'my-functions-xdg)

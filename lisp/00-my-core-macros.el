;; -*- lexical-binding: t; -*-

;; NOTE For functions that are used in macros in the same file, and those macros
;; are also called in the above file, use `eval-and-compile' around its
;; definition. While loading the autoload file before is viable,
;; `native-compile-async' will complain about that.

(require 'dash)
(require 'subr-x)
(require 's)
;; (require 'cl-macs)
;; (require 'cl-seq)

;; (eval-when-compile
;;   (when my-emacs-not-latest
;;     (require 'compat)))

;;; Helpers

;; rejected upstream
;;;###autoload
(defmacro dlet* (binders &rest body)
  (declare (indent 1) (debug let))
  `(let (_)
     ,@(mapcar (lambda (binder)
                 `(defvar ,(if (consp binder) (car binder) binder)))
               binders)
     (let* ,binders ,@body)))

;;;###autoload
(defmacro my-defn (name params &rest body)
  "Define function NAME and return NAME."
  (declare (indent defun) (doc-string 3))
  `(progn
     (cl-defun ,name ,params ,@body)
     ',name))

(define-minor-mode my-maydefun-force-mode
  nil
  :global t)

;;;###autoload
(defmacro my-maydefun (name params &rest body)
  "Define function NAME unless already defined.
A use case: let F be function defined in L, L is interpreted but
it's autoload file A is compiled, we can dump F into A using an
autoload cookie, so F is compiled when A is loaded but it will be
back to being interpreted when L is loaded, this prevents F's
interpreted version from replacing it's compiled version.

No need for this when L is (self-)?compiled."
  (declare (indent defun) (doc-string 3) (debug t))
  (cond
   ((and (fboundp name) (not my-maydefun-force-mode))
    ;; (when (not noninteractive) (message "In %s: `%s' is already defined" load-file-name ',name))
    `',name)
   (t
    `(progn
       (cl-defun ,name ,params ,@body)
       ',name))))

;;;###autoload
(defmacro my-maydefun-set (name lambda-args &optional docstring)
  "Ensure a function whose name is dynamic NAME is defined with
LAMBDA-ARGS and DOCSTRING. NAME can be a string which will be
interned, LAMBDA-ARGS is list that will be spliced and pass to
`lambda'. Just return the interned NAME when it is already
defined.

Just like

(progn (defalias NAME (lambda ,@LAMBDA-ARGS) DOCSTRING)
        NAME)

But avoid redundant `lambda' creation."
  (declare (indent defun) (doc-string 3))
  `(let ((func (cond ((stringp ,name) (intern ,name))
                     (t ,name))))
     (unless (fboundp func)
       (defalias func
         (lambda ,@lambda-args)
         ,docstring))
     func))

;;;###autoload
(defun my-unquote (exp &optional one)
  "Return unquoted EXP.
ONE: remove only the first quote, else (normally) all."
  (declare (pure t) (side-effect-free t))
  (named-let
      recur ((expr exp))
    (if (member (car-safe expr) '(quote function))
        (-let* ((stripped (cadr expr)))
          (if one
              stripped
            (recur stripped)))
      expr)))

;;;###autoload
(defmacro my-ignore-macro (&rest _)
  nil)

;;;###autoload
(defmacro my-thread-as* (initial-form variable &rest forms)
  "Thread FORMS elements as VARIABLE, starting from INITIAL-FORM."
  (declare (debug (form symbolp body)))
  (if (null forms)
      initial-form
    (-let* ((let-varlist `((,variable ,initial-form)))
            final-return form)
      (while forms
        (setq form (pop forms))
        (setq form (if (symbolp form) `(,form ,variable) form))
        (if forms
            (push `(,variable ,form) let-varlist)
          (setq final-return form)))
      `(let* ,(nreverse let-varlist)
         ,final-return))))

;;;###autoload
(defmacro my-thread-as (initial-form variable &rest forms)
  "Thread FORMS elements as VARIABLE, starting from INITIAL-FORM."
  (declare (debug (form symbolp body)))
  `(let* ((,variable ,initial-form)
          ,@(mapcar (lambda (form)
                      `(,variable ,(if (symbolp form)
                                       (list form variable)
                                     form)))
                    forms))
     ,variable))

;;;###autoload
(defun my-normalize-bindings (bindings)
  (declare (pure t) (side-effect-free t))
  (cond
   ((vectorp bindings)
    (-partition 2 (append bindings '())))
   (t
    bindings)))

;;;###autoload
(defmacro my-for (seq-exprs body-expr)
  "List comprehension.
Takes a vector of one or more SEQ-EXPRS pairs, each followed by
zero or more modifiers, and yields a sequence of evaluations of
BODY-EXPR. Collections are iterated in a nested fashion,
rightmost fastest, and nested coll-exprs can refer to bindings
created in prior binding-forms. Supported modifiers are: :let
[binding-form expr ...], :while test, :when test."
  (declare (indent defun) (debug t))
  (let ((all-binds (my-normalize-bindings seq-exprs)))
    `(my--for ,all-binds ,body-expr)))
(defmacro my--for (bindings body-expr)
  (let* ((first-expr (car bindings))
         (next-modifiers-exprs (-take-while (lambda (%) (keywordp (car %)))
                                            (cdr bindings)))
         (rest-exprs (-drop (length next-modifiers-exprs) (cdr bindings))))
    (cl-destructuring-bind (my--for-element my--for-sequence) first-expr
      (let ((processed-modifiers (-mapcat #'my-for--process-modifier
                                          next-modifiers-exprs)))
        (append `(cl-loop for ,my--for-element being the elements of ,my--for-sequence
                          ,@processed-modifiers)
                ;; "flatten" non-bottom iterated levels
                (if (seq-empty-p rest-exprs)
                    `(collect ,body-expr)
                  `(append (my--for ,rest-exprs ,body-expr))))))))
(defun my-for--process-modifier (pair)
  (cl-destructuring-bind (kw expr/s) pair
    (pcase kw
      (:let (cl-loop for (var val) in (my-normalize-bindings expr/s)
                     append `(for ,var = ,val)))
      (:when `(when ,expr/s))
      (:while `(while ,expr/s)))))

;;;###autoload
(defmacro my-loop (bindings &rest body)
  "Evaluates BODY in a lexical context in which the symbols in the
BINDING-forms are bound to their respective initial bindings or
parts therein. Acts as a `recur' target.
BINDINGS is either:
[var0 val0
 var1 val1 ...]
or
((var0 val0)
 (var1 val1) ...).
VAR can be destructured like `-let*'."
  (declare (indent defun) (debug t))
  (let* ((bindings* (my-normalize-bindings bindings))
         (match-forms (-map #'car bindings*))
         (sources (-map #'cadr bindings*))
         (intermediate-vars (cl-loop for i below (length bindings*)
                                     collect (intern (format "my-loop--source-%d" i)))))
    `(named-let recur ,(-zip-lists intermediate-vars sources)
       (-let* ,(-zip-lists match-forms intermediate-vars)
         ,@body))))

;;;###autoload
(defun my-symbol-to-number (sym &optional beg-pos)
  (string-to-number (substring (symbol-name sym)
                               beg-pos)))

;;;###autoload
(defmacro my-fn% (&optional docstring-or-first-form &rest body)
  "Return an interactive variadic lambda with BODY.
With %[1..] %& as arguments.  DOCSTRING as-is."
  ;; Find the maximum num of `%num'
  (let* ((max-arg-index
          (cl-loop
           for sym in
           (cons '%0 (flatten-tree (cons docstring-or-first-form body))) when
           (and (symbolp sym)
                (string-match-p "^%[0-9]+$" (symbol-name sym)))
           maximize (my-symbol-to-number sym 1))))
    `(lambda (,@(my-for
                  [i (number-sequence 1 max-arg-index)]
                  (intern (format "%%%s" i)))
              &rest %&)
       ,@(and (stringp docstring-or-first-form) (list docstring-or-first-form))
       (interactive)
       ,docstring-or-first-form
       ,@body)))

;;;###autoload
(defmacro my-fn1 (destr-form &rest body)
  "Return an anonymous function whose arity is one (1).
DESTR-FORM is bounded to the result of it's argument and execute BODY."
  (declare (indent defun) (debug t))
  `(lambda (my-fn1--arg)
     (cl-destructuring-bind ,destr-form my-fn1--arg
       ,@body)))

;;;###autoload
(defmacro my-safe (&rest forms)
  "Execute FORMS like `progn''s body when each of their first element is a function or a macro."
  (let ((body (cl-loop for form in forms
                       for first = (car-safe form)
                       when (or (functionp first)
                                (macrop first))
                       collect form)))
    `(progn ,@body)))

;;;###autoload
(defmacro my-set (place placeholder value)
  "Set PLACE to VALUE with PLACEHOLDER as PLACE's old value.
(let ((xs (list '(1) 2 3))) (my-set (car xs) it (append '(0) it)) xs)
=> '((0 1) 2 3)
"
  `(let ((,placeholder ,place))
     (setf ,place ,value)))

;;;###autoload
(defmacro my-inplace-delete! (place &rest elements)
  "In PLACE, inplace delete each element which is `equal' to one of ELEMENTS."
  `(let ((res (-difference ,place (list ,@elements))))
     (setf ,place res)
     res))

;;;###autoload
(defmacro my-symbols->plist (&rest symbols)
  "Return (list :sym sym ...), for each sym in SYMBOLS."
  `(list ,@(-mapcat (lambda (sym)
                      (list (intern (format ":%s" sym))
                            sym))
                    symbols)))

;;;###autoload
(defmacro my-fn--> (&rest forms)
  (declare (debug t))
  `(lambda (my-fn-->arg)
     (--> my-fn-->arg
          ,@forms)))

;;;###autoload
(defmacro my-eval-at-position (pos &rest body)
  "Evaluate BODY at POS, saving current `point'.
For each form of BODY: when it's a function, `funcall' it
instead."
  (declare (indent defun) (debug t))
  `(save-excursion
     (goto-char ,pos)
     ,@(cl-loop for form in body
                collect (if (functionp (my-unquote form))
                            (list 'funcall form)
                          form))))

;;;###autoload
(defmacro my-save-in-local-variable (var form)
  (declare (indent defun) (debug t))
  `(progn
     (unless (local-variable-p ',var)
       (setq-local ,var ,form))
     ,var))

;;;###autoload
(defmacro my-cond-let (&rest clauses)
  "Try each of CLAUSES until one succeeds.

Each clause looks like (SPECS BODY...).

Each SPECS looks like ((VAR VAL)...) or [VAR VAL...], or just a
normal (`if' condition) expression without bindings. When all VALs
evaluate to true, bind them to their corresponding VARs and then the
expressions in BODY are evaluated and the last one's value is the value
of the cond-form; otherwise evaluate the next clause.

When no SPECS is satisfied, return nil.

Note: binding is done according to `-let*'. VALs are evaluated
sequentially, and evaluation stops after the first `nil' VAL is
encountered."
  (declare (debug t))
  (let ((reversed-clauses (nreverse clauses))
        (spec-fn
         (lambda (spec)
           (cond
            ((sequencep spec)
             (my-normalize-bindings spec))
            (:else
             `((_ ,spec)))))))
    (-reduce-from
     (-lambda (accu (spec . body))
       `(-if-let* ,(funcall
                    spec-fn
                    spec)
            (progn
              ,@body)
          ,accu))
     (-let (((spec . body) (car reversed-clauses)))
       `(-when-let* ,(funcall
                      spec-fn
                      spec)
          ,@body))
     (cdr reversed-clauses))))

;;;###autoload
(defmacro my-record-return-values-of (func &rest body)
  "While evaluating BODY, return return values of FUNC as a list."
  (declare (indent defun) (debug t))
  `(let* ((retvals '())
          (adv (lambda (fn &rest args)
                 (let ((retval (apply fn args)))
                   (push retval retvals)
                   retval))))
     (advice-add ,func :around adv)
     (progn (progn ,@body)
            (advice-remove ,func adv)
            (nreverse retvals))))

;;;###autoload
(defmacro my-benchmark-progn* (print-format &rest body)
  "Print, evaluate BODY and print the time taken.
The return value is the value of the final form in BODY.
PRINT-FORMAT is used to format the message with the float elapsed
time as single argument (like \"%fs\n\")."
  (declare (debug t) (indent defun))
  `(-let* ((time0 (current-time)))
     (prog1 (progn ,@body)
       (princ (format ,print-format (float-time (time-since time0)))))))

;;;###autoload
(defmacro my-s$ (format-str)
  "(`s-lex-format' FORMAT-STR) with `s-lex-value-as-lisp' disabled.
To prevent unexpected modifications to the said variable."
  (declare (debug t))
  `(dlet ((s-lex-value-as-lisp nil))
     (s-lex-format ,format-str)))

;;;###autoload
(defmacro my-run-at-0-current-buffer (&rest body)
  "Execute BODY immediately when free, preserving current buffer."
  (declare (debug t) (indent defun))
  `(-let* ((buf (current-buffer)))
     (run-at-time
      nil nil
      (lambda ()
        (when (buffer-live-p buf)
          (with-current-buffer buf
            ,@body))))))

;;;###autoload
(cl-defmacro my-with-buffer-scrolling (buffer-or-name &rest body)
  "Execute the forms in BODY with BUFFER-OR-NAME temporarily current.
Like `with-current-buffer', but allow scrolling the visible
window of BUFFER-OR-NAME when at the buffer end, if any."
  (declare (debug t) (indent defun))
  `(-let* ((wd (get-buffer-window ,buffer-or-name t)))
     (cond
      (wd
       (with-selected-window wd
         ;; disallow scrolling when visibly not at end of buffer
         (-let* ((eob-flag (eobp))
                 (pt0 (point)))
           (unwind-protect
               (progn
                 ,@body)
             (unless eob-flag
               (goto-char pt0))))))
      (t
       (with-current-buffer ,buffer-or-name
         ,@body)))))

;;;###autoload
(cl-defmacro my-without-filename-handler (&rest body)
  "Run BODY without `file-name-handler-alist'.
The said alist may unexpectedly load libraries."
  (declare (debug t) (indent defun))
  `(dlet ((file-name-handler-alist '()))
     ,@body))

;;; Configurers

;;;###autoload
(defmacro my-when-graphical (&rest body)
  "Evaluate BODY once when running in a graphical environment."
  `(letrec ((func (lambda ()
                    (when (display-graphic-p)
                      ,@body
                      (remove-hook 'server-after-make-frame-hook func)))))
     (add-hook 'server-after-make-frame-hook func)
     (funcall func)))

;;;###autoload
(defmacro my-with-advice (symbol where function &rest body)
  "Run BODY with temporary (advice-add SYMBOL WHERE FUNCTION).
When FUNCTION is already added to SYMBOL prior to execution, this
won't unexpectedly remove FUNCTION from SYMBOL after that, it
will only remove what would have been added by itself.

\(my-with-advice #'print :override #'ignore
  (print 0)
  (my-with-advice #'print :override #'ignore)
  (print 1))

The above expression won't print \"1\"."
  (declare (indent defun) (debug t))
  `(let* ((advice-cookie-name (gensym "my-with-advice")))
     (advice-add ,symbol ,where ,function `((name . ,advice-cookie-name)))
     (unwind-protect
         (progn ,@body)
       (advice-remove ,symbol advice-cookie-name))))

;;;###autoload
(defmacro my-with-advices (symbols where function &rest body)
  (declare (indent defun) (debug t))
  (named-let
      recur
      ((form
        `(progn
           ,@body))
       (syms (nreverse (my-unquote symbols))))
    (cond
     ((seq-empty-p syms)
      form)
     (t
      (recur
       `(my-with-advice #',(car syms) ,where ,function
          ,form)
       (cdr syms))))))

(defun my-with-demoted-signals--signal-a (error-symbol data)
  (message "`my-with-demoted-signals': %S %S" error-symbol data))
;;;###autoload
(defmacro my-with-demoted-signals (&rest body)
  (declare (debug t))
  `(my-with-advice #'signal :override #'my-with-demoted-signals--signal-a
     ,@body))

;;;###autoload
(defmacro my-with-demoted-errors (&rest body)
  (declare (debug t))
  `(condition-case err
       (progn
         ,@body)
     (error
      (message "`my-with-demoted-errors': %S %s"
               err
               (if debug-on-error
                   (format "; from %s"
                           (-->
                            ',body
                            (format "%s" it)
                            (my-remove-str-fixes "(" ")" it)
                            (replace-regexp-in-string "\n" " " it)))
                 ""))
      nil)))

;;;###autoload
(defmacro my-with-demoted-errors-no-echo (&rest body)
  (declare (debug t))
  `(dlet ((inhibit-message t))
     (my-with-demoted-errors ,@body)))

;;;###autoload
(defmacro my-eval-until-no-error (&rest forms)
  "Evaluate each of FORMS until no error is signaled and return
that last evaluated value."
  (declare (debug t))
  (cl-reduce (lambda (next-forms prev-form)
               `(condition-case _
                    ,prev-form
                  ('error ,next-forms)))
             (nreverse forms)))

;;;###autoload
(defmacro my-condition-case* (var handler &rest bodyform)
  (declare (indent defun) (debug t))
  `(condition-case ,var
       (progn ,@bodyform)
     ,handler))

;;;###autoload
(defmacro my-condition-case** (handler &rest bodyform)
  (declare (indent defun) (debug t))
  `(condition-case _
       (progn ,@bodyform)
     (error ,handler)))

;;;###autoload
(defmacro my-with-deferred-gc (&rest body)
  "Defer garbage collection while executing BODY."
  (declare (debug t))
  `(dlet ((gc-cons-threshold most-positive-fixnum)
          (gc-cons-percentage 0.75))
     ,@body))

;;;###autoload
(defmacro my-get-buffer-differences-after (&rest body)
  "After executing BODY, return visible appeared & disappeared buffers."
  `(let ((old-buffers (my-get-all-buffers-in-frames)))
     ,@body
     (let ((new-buffers (my-get-all-buffers-in-frames)))
       (list
        (-difference new-buffers old-buffers)
        (-difference old-buffers new-buffers)))))

;;;###autoload
(defmacro my-after-each (libs &rest body)
  "Evaluate BODY like `with-eval-after-load' multiple times after each of LIBS has been loaded."
  (declare (indent defun))
  `(dolist (my-after-each--lib ,libs)
     (with-eval-after-load my-after-each--lib
       ,@body)))

;;;###autoload
(defmacro my-after-once (libs &rest body)
  "Evaluate BODY like `with-eval-after-load' once after one of LIBS has been loaded."
  (declare (indent defun))
  `(letrec ((h (lambda (&rest _)
                 (when (-some (-cut member <> features) ,libs)
                   (remove-hook 'after-load-functions h)
                   ,@body))))
     (add-hook 'after-load-functions h)
     (funcall h)))

;;;###autoload
(defmacro my-after-all (libs &rest body)
  "Evaluate BODY like `with-eval-after-load' once after all of LIBS have been loaded."
  (declare (indent defun))
  `(letrec ((h (lambda (&rest _)
                 (when (-every (-cut member <> features) ,libs)
                   (remove-hook 'after-load-functions h)
                   ,@body))))
     (add-hook 'after-load-functions h)
     (funcall h)))

;;;###autoload
(defmacro my-with-mode/s (mode/s arg-bool &rest body)
  "Execute BODY with MODE/S's statuses as ARG-BOOL (`t' or `nil').
Return BODY's evaluated value."
  (declare (indent defun) (debug t))
  `(progn
     (cl-assert (member ,arg-bool '(nil t)))
     (let* ((modes (ensure-list ,mode/s))
            (bound-val (lambda (sym) (and (boundp sym) (symbol-value sym))))
            (set-fn
             (lambda (mode new-status)
               (when (fboundp mode)
                 (when (not (equal new-status (funcall bound-val mode)))
                   (funcall mode (or new-status 0))))))
            (old-mode-value-alist
             (-map (lambda (mode) (cons mode (funcall bound-val mode))) modes)))
       (dolist (mode modes)
         (funcall set-fn mode ,arg-bool))
       ;; not `prog1', restore modes even when return abnormally
       (unwind-protect
           (progn
             ,@body)
         (dolist (mode modes)
           (funcall set-fn mode (alist-get mode old-mode-value-alist)))))))

;;;###autoload
(defmacro my-in-other-window (ensure-window &rest body)
  "Execute BODY in another window. With non-nil ENSURE-WINDOW, always splits, else depends on `split-window-sensibly'."
  `(progn
     (if ,ensure-window
         (if (> (window-pixel-height) (window-pixel-width))
             (split-window-below)
           (split-window-right))
       (split-window-sensibly))
     (other-window 1)
     ,@body))

;;;###autoload
(defmacro my-without-spaces-inserted (&rest body)
  "After executing BODY, remove spaces between the previous point
and current point."
  `(let ((p (point)))
     ,@body
     (let ((str (buffer-substring-no-properties p (point))))
       (delete-region p (point))
       (insert (replace-regexp-in-string " " "" str)))))

;;;###autoload
(defmacro my-with-minibuffer (&rest body)
  "Evaluate BODY in the active minibuffer and return it's value.
When there isn't an active minibuffer, no-op."
  `(save-selected-window
     (if (active-minibuffer-window)
         (progn
           (select-window (active-minibuffer-window))
           ,@body)
       nil)))

;;;###autoload
(defmacro my-quit-minibuffer-and-run (&rest body)
  (declare (debug t))
  `(progn
     (run-at-time
      nil nil
      (lambda ()
        (with-demoted-errors "Error: %S"
          ,@body)))
     (my-with-minibuffer (abort-recursive-edit))))

;;;###autoload
(defmacro my-add-first-editable-hook (&rest body)
  (declare (indent defun) (debug t))
  `(letrec ((editable-hooks
             '(window-buffer-change-functions minibuffer-setup-hook))
            (func
             (lambda (&rest _)
               (when (and after-init-time ; initialization is finished
                          (not buffer-read-only))
                 ,@body
                 (my-remove-hook/s editable-hooks (list func))))))
     (my-add-hook/s editable-hooks (list func))
     (funcall func)))

;;;###autoload
(defmacro my-hook-while (hook condition &rest body)
  "Hook BODY into HOOK while CONDITION is unsatisfied."
  (declare (indent defun) (debug t))
  `(letrec
       ((func
         (lambda (&rest _)
           (add-hook ,hook func)
           (if ,condition
               (progn ,@body)
             (remove-hook ,hook func)))))
     (funcall func)))

;;;###autoload
(defmacro my-add-lazy-hook-when (hook condition &rest body)
  "Add a function into HOOK that runs BODY once when CONDITION is
satisfied, that function is returned."
  (declare (indent defun) (debug t))
  `(letrec
       ((func
         (lambda (&rest _)
           (when ,condition
             ,@body
             (remove-hook ,hook func)))))
     (add-hook ,hook func)
     func))

;;;###autoload
(defmacro my-hook-once-when-then-forced-next (hook condition function)
  (declare (indent defun) (debug t))
  `(my-add-hook-once ,hook
     (lambda ()
       (if ,condition
           (funcall ,function)
         (my-add-hook-once ,hook ,function)))))

;;;###autoload
(defvar my-accumulate-config--list '()
  "Aggregated forms for evaluation at a later time.
Use when it's not correct to evaluate those in the autoload file.")
;;;###autoload
(defmacro my-accumulate-config-collect (&rest body)
  `(add-to-list
    'my-accumulate-config--list
    ;; (lambda () ,@body)                       ;
    '(progn ,@body))) ;

;;;###autoload
(defun my-accumulate-config-execute ()
  (mapc (if (functionp (car my-accumulate-config--list))
            #'funcall
          (lambda (form)
            (eval form t)))
        my-accumulate-config--list))

;;;###autoload
(defmacro my-add-startup-hook (&rest body)
  "Add BODY to `emacs-startup-hook'.
(info \"Summary: Sequence of Actions at Startup\")"
  (declare (indent defun) (debug t))
  `(add-hook 'emacs-startup-hook
             (lambda ()
               ,@body)))

;;;###autoload
(defmacro my-with-file (file &rest body)
  (declare (indent defun) (debug t))
  `(with-current-buffer (find-file-noselect ,file)
     ,@body))

;;;###autoload
(defmacro my-disable-mode-when (mode advice-name condition)
  (declare (indent defun) (debug t))
  `(progn
     (defun ,advice-name (func &optional arg)
       (if ,condition
           (when (bound-and-true-p ,mode)
             (funcall func 0))
         (funcall func arg)))
     (advice-add #',mode :around #',advice-name)))

;; (when (not (fboundp #'modulep!)) (defmacro modulep! (&rest _) nil))
;; (when (not (fboundp #'package!)) (defmacro package! (&rest _) nil))

(cl-defmacro modulep! (&rest _)
  "Treat all modules as used."
  (declare (debug t) (indent defun))
  t)

(cl-defmacro package! (pkg &rest _)
  (declare (debug t) (indent defun))
  `(when
       ;; allow ignoring when disable before
       (not (assoc ',pkg my-pkg!--list))
     (my-pkg! ',pkg)))

;;; Utilities

;;;###autoload
(defmacro my-with-selected-window-maybe (wd &rest body)
  (declare (indent defun) (debug t))
  `(if (equal ,wd (selected-window))
       (progn ,@body)
     (with-selected-window ,wd
       ,@body)))

;;;###autoload
(defmacro my-save-line (&rest body)
  (declare (indent defun) (debug t))
  `(let* ((line0 (line-number-at-pos)))
     (unwind-protect
         (progn
           ,@body)
       (goto-char (point-min))
       (forward-line (1- line0)))))

;;;###autoload
(defmacro my-save-column (&rest body)
  (declare (indent defun) (debug t))
  `(let* ((column0 (current-column)))
     (unwind-protect
         (progn
           ,@body)
       (move-to-column column0))))

;;;###autoload
(defmacro my-save-line-and-column (&rest body)
  (declare (indent defun) (debug t))
  ;; restore column after restoring line, since `forward-line' doesn't preserve
  ;; column
  `(my-save-column
     (my-save-line
       ,@body)))

;;;###autoload
(defmacro my-save-position-in-window (&rest body)
  (declare (debug t))
  `(-let* ((delta (- (line-number-at-pos)
                     (line-number-at-pos (window-start)))))
     (unwind-protect (progn ,@body)
       (recenter delta))))

(defvar my-evil-save-state-notify-flag nil)
(defvar-local evil-state nil)
;;;###autoload
(defmacro my-evil-save-state (&rest body)
  "Run BODY while preserving the evil state after that.
Can be safe to use even when `evil' isn't installed. Use this
when `evil-save-state' doesn't work."
  (declare (debug t))
  `(-let* ((orig-state evil-state))
     (unwind-protect
         (progn
           ,@body)
       (when (not (equal orig-state evil-state))
         (when my-evil-save-state-notify-flag
           (message "`my-evil-save-state': `%s'->`%s'" evil-state orig-state))
         (evil-change-state orig-state)))))

;;;###autoload
(defmacro my-profiler-cpu-when-debug-progn (&rest body)
  (declare (debug t))
  `(if init-file-debug
       (let ((my-profiler-cpu-when-debug-progn--already
              (profiler-cpu-running-p)))
         (if my-profiler-cpu-when-debug-progn--already
             (message
              "`my-profiler-cpu-when-debug-progn' : `profiler-cpu-running-p'")
           (profiler-start 'cpu))
         (unwind-protect
             (progn
               ,@body)
           (unless my-profiler-cpu-when-debug-progn--already
             (profiler-cpu-stop)
             (when (called-interactively-p t)
               (save-window-excursion (profiler-report))))))
     (progn
       ,@body)))

;;;###autoload
(defmacro my-with-display-buffer-same-window (&rest body)
  (declare (debug t))
  `(dlet ((display-buffer-alist '(("" (display-buffer-same-window)))))
     ,@body))

;;;###autoload
(defmacro my-with-find-file-follow-true-name (true? &rest body)
  (declare (debug t) (indent defun))
  `(dlet ((find-file-visit-truename ,true?)
          (vc-follow-symlinks ,true?))
     ,@body))

;;;###autoload
(defmacro my-with-opended-file-and-save (file-name &rest body)
  (declare (debug t) (indent defun))
  `(with-current-buffer (find-file-noselect ,file-name)
     (-let* ((already-modified (buffer-modified-p)))
       (if already-modified
           (error "`my-with-opended-file-and-save': %s is modified!"
                  ,file-name)
         (prog1 (progn ,@body)
           (save-buffer 0))))))

(defvar my-progn-once--list '())

;;;###autoload
(defmacro my-progn-once (cookie-prefix &rest body)
  "If BODY hasn't been evaluated (distinguish with COOKIE-PREFIX), run it."
  (declare (debug t) (indent defun))
  (-let* ((flag (format "%s %S" cookie-prefix body)))
    `(if (member ,flag my-progn-once--list)
         nil
       (unwind-protect
           (progn ,@body)
         (push ,flag my-progn-once--list)))))

;;;###autoload
(defmacro my-with-ensured-current-directory (&rest body)
  (declare (debug t))
  `(condition-case err
       (progn ,@body)
     ('file-error
      (if (file-directory-p default-directory)
          (error "%s" (error-message-string err))
        (progn (my-println🌈 'my-with-ensured-current-directory
                             "Error: " err
                             "Creating " default-directory)
               (make-directory default-directory t)
               ,@body)))))

;;;###autoload
(defmacro my-with-current-buffer (buf &rest body)
  (declare (debug t) (indent defun))
  `(if ,buf
       (with-current-buffer ,buf
         ,@body)
     (progn ,@body)))

(defvar my-extra-capfs-table '()
  "Table of extra (maybe because of performance) `completion-at-point-functions'.
Each element is a list of (SYMBOL CAPF), when SYMBOL is non-nil
or is a major mode that current `major-mode' derives from, add
CAPF to the beginning of `completion-at-point-functions' (order
matters).")

;;;###autoload
(defmacro my-with-extra-capfs (&rest body)
  (declare (debug t) (indent defun))
  `(dlet ((completion-at-point-functions
           (append
            (-keep
             (-lambda
               ((sym/s capf))
               (and (-some
                     (lambda (sym)
                       (or (and (boundp sym) (symbol-value sym))
                           (derived-mode-p sym)))
                     (ensure-list sym/s))
                    capf))
             my-extra-capfs-table)
            completion-at-point-functions)))
     ,@body))

;;;###autoload
(defmacro my-with-save-window-and-fit-popup (buffer-to-fit &rest body)
  (declare (debug t) (indent defun))
  `(save-selected-window
     (prog1 (progn
              ,@body)
       (-some-->
           (get-buffer-window ,buffer-to-fit)
         (with-selected-window it
           (my-fit-current-popup-window-height nil 1))))))

;;;###autoload
(cl-defmacro my-with-minibuffer-no-preselect (&rest body)
  (declare (debug t) (indent defun))
  `(dlet ((vertico-preselect 'prompt))
     ;; TODO: for `ido', `fido', `icomplete'

     ,@body))

;;;###autoload
(cl-defmacro my-at-project-root (&rest body)
  (declare (debug t) (indent defun))
  `(dlet ((default-directory
           (or (-some--> (project-current) (project-root it))
               default-directory)))
     ,@body))

;;;###autoload
(cl-defmacro my-with-saved-windows-displaying-current-buffer-file (&rest body)
  (declare (debug t) (indent defun))
  `(-let* ((displaying
            (-->
             (frame-list) (-mapcat #'window-list it)
             (-filter
              (lambda (wd)
                (and (equal
                      buffer-file-name (buffer-file-name (window-buffer wd)))
                     (not (equal wd (selected-window)))))
              it)
             (-map
              (lambda (wd)
                (with-selected-window wd
                  (vector wd (point))))
              it))))
     (prog1 (progn
              ,@body)
       (-let* ((buf (current-buffer)))
         (mapc
          (-lambda ([wd pt])
            (with-selected-window wd
              (switch-to-buffer buf)
              (goto-char pt)))
          displaying)))))

;;;###autoload
(cl-defmacro my-with-save-text-scale (&rest body)
  (declare (debug t) (indent defun))
  `(-let* ((amount (or (bound-and-true-p text-scale-mode-amount) 0))
           (buf (current-buffer)))
     (unwind-protect
         (progn
           ,@body)
       (when (/= 0 amount)
         (with-current-buffer buf
           (text-scale-set amount))))))

;;;###autoload
(cl-defmacro my-with-default-forward-sexp-function-if-non-code (&rest body)
  "Run BODY while binding `forward-sexp-function' appropriately.
`treesit-forward-sexp' may not work well in strings and comments."
  (declare (debug t) (indent defun))
  `(dlet ((forward-sexp-function
           (cond
            ((my-point-in-string-or-comment-p nil nil :face t)
             nil)
            (:else
             forward-sexp-function))))
     ,@body))

(defvar my-brace-characters (string-to-list "()[]{}"))

;;;###autoload
(defun my-after-brace-p (&optional braces)
  (let* ((braces (or braces my-brace-characters))
         (skip " \t\n\r"))
    (member
     (save-excursion
       (skip-chars-backward skip)
       (char-before))
     braces)))

;;;###autoload
(defun my-before-brace-p (&optional braces)
  (let* ((braces (or braces my-brace-characters))
         (skip " \t\n\r"))
    (member
     (save-excursion
       (skip-chars-forward skip)
       (char-after))
     braces)))

;;;###autoload
(defun my-brace-around-p (&optional braces)
  (let* ((braces (or braces my-brace-characters)))
    (or (my-after-brace-p braces) (my-before-brace-p braces))))

;;;###autoload
(cl-defmacro my-with-default-forward-sexp-function-if-braces (&rest body)
  (declare (debug t) (indent defun))
  `(dlet ((forward-sexp-function
           (cond
            ((my-brace-around-p)
             nil)
            (:else
             forward-sexp-function))))
     ,@body))

;;;###autoload
(cl-defmacro my-with-ensured-undo-boundary (&rest body)
  (declare (debug t) (indent defun))
  ;; https://github.com/minad/corfu/issues/495 undo after `corfu-complete'
  ;; deletes too much that the original candidate is lost
  `(let* ((boundary-marker (make-symbol "ignore (corfu undo)"))
          (marker-elem (list #'apply boundary-marker)))
     (defalias boundary-marker #'ignore)
     (undo-boundary) ;
     (push marker-elem buffer-undo-list)
     (push (point) buffer-undo-list)
     (unwind-protect
         (progn
           ,@body)
       (let* ((idx (seq-position buffer-undo-list marker-elem)))
         (setf (nth idx buffer-undo-list) nil)))))

;;;###autoload
(defun my-with-ensured-undo-boundary-a (func &rest args)
  (my-with-ensured-undo-boundary (apply func args)))

;;;###autoload
(cl-defmacro my-with-ignore-message (&rest body)
  (declare (debug t) (indent defun))
  `(my-with-advice #'message :override #'ignore
     (dlet ((inhibit-message t))
       ,@body)))

;;;###autoload
(cl-defmacro my-vc-with-clone-depth (depth _other-args &rest body)
  (declare (debug t) (indent defun))
  `(cond
    ((null ,depth)
     ,@body)
    (,depth
     (my-with-advice #'vc-git--out-ok :around
       (lambda (fn cmd &rest git-args)
         (cond
          ((equal cmd "clone")
           (apply fn cmd (format "--depth=%d" ,depth) git-args))
          (:else
           (apply fn cmd git-args))))
       ,@body))))


;;;###autoload
(cl-defmacro my-with-set-mark-select-region-unless-already (&rest body)
  (declare (debug t) (indent defun))
  `(progn
     ;; (handle-shift-selection)
     (when (not mark-active)
       (push-mark nil t t))
     (prog1 (progn
              ,@body))))
;; (setq-local deactivate-mark t)

;;;###autoload
(cl-defmacro my-with-display-buffer-no-window (&rest body)
  (declare (debug t) (indent defun))
  `(dlet ((display-buffer-alist '("" display-buffer-no-window (nil))))
     ,@body))

;;; 00-my-core-macros.el ends here

(provide '00-my-core-macros)

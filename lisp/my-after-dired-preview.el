;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(setq dired-preview-delay 0)

;;; after-dired-preview.el ends here

(provide 'my-after-dired-preview)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(with-eval-after-load 'xref
  ;; demote the etags backend
  (when (member 'etags--xref-backend (default-value 'xref-backend-functions))
    (remove-hook 'etags--xref-backend #'dumb-jump-xref-activate)
    (add-hook 'etags--xref-backend #'dumb-jump-xref-activate 99)))

;;; my-after-xref.el ends here

(provide 'my-after-xref)

;; Local Variables:
;; no-byte-compile: t
;; End:

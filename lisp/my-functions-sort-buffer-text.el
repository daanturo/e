;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(cl-defun my-sort-in-region (reverse beg
                                     end
                                     &optional
                                     nextrecfun
                                     endrecfun
                                     startkeyfun
                                     endkeyfun
                                     predicate
                                     &rest
                                     sort-subr-other-args)
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (let ((inhibit-field-text-motion t))
        (apply #'sort-subr
               reverse
               nextrecfun
               endrecfun
               startkeyfun
               endkeyfun
               predicate
               sort-subr-other-args)))))

;;;###autoload
(defun my-sort-lines-in-paragraph ()
  (interactive)
  (my-save-line-and-column
    (-let* (((beg . end) (my-bounds-of-visual-text-paragraph))
            (lines (string-lines (buffer-substring-no-properties beg end))))
      (if (my-binary->seq-test #'string< lines)
          (message "This paragraph's lines are already sorted.")
        (sort-lines nil beg end)))))

;;;###autoload
(defun my-sort-characters-in-region ()
  (interactive)
  (-let* ((sorted (--> (buffer-substring-no-properties (region-beginning) (region-end))
                       (seq-map #'identity it)
                       (sort it #'<)
                       (apply #'string it))))
    (replace-region-contents (region-beginning)
                             (region-end)
                             (-const sorted))))

;;;###autoload
(defun my-sort-lines-pretend-uncommented (reverse beg end)
  (interactive "P\nr")
  (my-sort-in-region
   reverse beg end 'forward-line 'end-of-line #'my-skip-comment-start-forward))

;;;###autoload
(defun my-sort-lines-by-length (reverse beg end)
  (interactive "P\nr")
  (my-sort-in-region reverse beg end 'forward-line 'end-of-line
                     nil nil
                     (lambda (l0 l1)
                       (< (- (cdr l0) (car l0)) (- (cdr l1) (car l1))))))

;;;###autoload
(defun my-sort-string-sequence-in-region (delimiter reg-beg reg-end)
  (interactive "sDelimiter: \nr")
  (-let* ((str (buffer-substring-no-properties reg-beg reg-end))
          (components (split-string str (regexp-quote delimiter)))
          (sorted (--> (-sort #'string< components)
                       (cl-loop for i below (length it)
                                for comp = (elt it i)
                                collect (if (equal comp (elt components i))
                                            comp
                                          (propertize comp 'face 'diff-changed)))
                       (string-join it delimiter))))
    (message "From '%s' to '%s'" str sorted)
    (replace-region-contents reg-beg reg-end
                             (-const sorted))))


;;;###autoload
(defun my-sort-paragraphs-to-end-of-buffer (reversed)
  "Like (`sort-paragraphs' REVERSED) but also consider sexps."
  (interactive "P")
  (save-excursion
    (skip-chars-forward "\n")
    (my-sort-in-region
     reversed (point) (point-max)
     (lambda ()
       (while (and (not (eobp)) (looking-at paragraph-separate))
         (forward-line 1)))
     (lambda ()
       (my-forward-sexp-or-paragraph)
       (when (and (eobp) (not (bolp)))
         (insert "\n"))))))

;;; my-functions-sort-buffer-text.el ends here

(provide 'my-functions-sort-buffer-text)

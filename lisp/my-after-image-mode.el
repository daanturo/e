;; -*- lexical-binding: t; -*-

(my-bind
  :map 'image-mode-map '([remap kill-ring-save] [remap cua-copy-region])
  `(menu-item
    ""
    my-clipboard-copy-file-contents
    ,(lambda (cmd) (and (not (use-region-p)) cmd))))

;; Unbind `image-mode-wallpaper-set'
(my-bind :map 'image-mode-map "W" nil)

(my-context-menu-add-local 'image-mode 'my-image-context-menu)

(provide 'my-after-image-mode)

;; Local Variables:
;; no-byte-compile: t
;; End:

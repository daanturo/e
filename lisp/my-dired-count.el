;; -*- lexical-binding: t; -*-

;;;###autoload
(defun my-dired-count-lighter ()
  (let ((marked-num (length (my-dired-get-explitly-marked-files dired-marker-char)))
        (delete-num (length (my-dired-get-explitly-marked-files dired-del-marker)))
        (total (length (my-directory-children default-directory))))
    (concat (format " %d/%d%c" marked-num total dired-marker-char)
            (and (< 0 delete-num)
                 (format " %d%c" delete-num dired-del-marker)))))

(defvar my-dired-count-exclude-update-keys (list (kbd "<up>") (kbd "<down>")))

(defvar-local my-dired-count-lighter--cache "")
(defun my-dired-count-lighter--cached ()
  (when (not (member (this-command-keys) my-dired-count-exclude-update-keys))
    (let* ((val
            (while-no-input
              (my-dired-count-lighter))))
      (when (not (member val '(nil t)))
        (setq-local my-dired-count-lighter--cache val))))
  my-dired-count-lighter--cache)

(defun my-dired-count--update-h (&rest _)
  (when (not (member (this-command-keys) my-dired-count-exclude-update-keys))
    (while-no-input
      (force-mode-line-update))))


;;;###autoload
(define-minor-mode my-dired-count-show-selection-mode
  "Show counts of selected/total files."
  :lighter
  (:eval (my-dired-count-lighter--cached))
  (if my-dired-count-show-selection-mode
      (progn
        (add-hook 'post-command-hook 'my-dired-count--update-h nil 'local))
    (progn
      (remove-hook 'post-command-hook 'my-dired-count--update-h 'local))))

(provide 'my-dired-count)

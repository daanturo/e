;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(unless (proper-list-p tempel-path)
  (setq tempel-path (list tempel-path)))

(add-to-list
 'tempel-path (concat my-emacs-conf-dir/ "snippets-tempel/*/*.eld"))

(add-to-list 'tempel-path (concat my-emacs-conf-dir/ "snippets-tempel/*.eld"))

(add-hook 'doom-escape-hook #'my-tempel-done-h)

(require 'tempel-collection nil 'noerror)

(my-bind :map 'tempel-map "TAB" #'tempel-next "<backtab>" #'tempel-previous)

;; (put 'tempel-abort 'completion-predicate nil)

(my-bind :map 'tempel-map '("<escape>") #'tempel-abort)

(add-hook 'vertico-multiform-commands '(tempel-insert grid (vertico-grid-annotate . 32)))

;;; after-tempel.el ends here

(provide 'my-after-tempel)

;; Local Variables:
;; no-byte-compile: t
;; End:

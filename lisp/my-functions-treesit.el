;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'treesit)

;;; Library functions

;;;###autoload
(defun my-treesit-language->major-mode (lang)
  (intern (format "%s-ts-mode" lang)))

;;;###autoload
(defun my-treesit-node-text* (node)
  "(`treesit-node-text' NODE) without text properties.
Only for testing quickly, use the mentioned function instead."
  (treesit-node-text node 'no-property))

;;;###autoload
(defun my-treesit-ancestor (node level)
  (funcall (apply #'-compose (-repeat level #'treesit-node-parent)) node))

;;;###autoload
(defun my-treesit-children-texts (node &optional named txt-properties pred)
  (-let* ((children
           (if pred
               (treesit-filter-child node pred named)
             (treesit-node-children node named))))
    (my-for [child children] (treesit-node-text child (not txt-properties)))))

;;;###autoload
(defun my-treesit-children-types (node &optional named pred)
  (-let* ((children
           (if pred
               (treesit-filter-child node pred named)
             (treesit-node-children node named))))
    (my-for [child children] (treesit-node-type child))))

;;;###autoload
(defun my-treesit-backward-up-node (&optional arg)
  (interactive "p")
  (dotimes (_ arg)
    (-let* ((cur-node (treesit-node-at (point)))
            (cur-beg (treesit-node-start cur-node))
            (target-parent
             (treesit-parent-until
              cur-node (lambda (node) (/= (treesit-node-start node) cur-beg)))))
      (goto-char (treesit-node-start target-parent)))))

;;;###autoload
(defun my-treesit-some-child (pred node &optional named)
  (named-let
      recur ((child (treesit-node-child node 0 named)))
    (cond
     ((null child)
      nil)
     ((funcall pred child))
     (t
      (recur (treesit-node-next-sibling child named))))))

;;;###autoload
(defun my-treesit-find-child (pred node &optional named)
  (named-let
      recur ((child (treesit-node-child node 0 named)))
    (cond
     ((null child)
      nil)
     ((funcall pred child)
      child)
     (t
      (recur (treesit-node-next-sibling child named))))))

;;;###autoload
(defun my-treesit-node-at-point (&optional named)
  (treesit-node-at (point) nil named))

;;;###autoload
(defun my-treesit-node-on-region (&optional named)
  (and (use-region-p)
       (treesit-node-on (region-beginning) (region-end) nil named)))

;;;###autoload
(defun my-treesit-ancestor-with-type (child-node typ/s &optional include-self)
  (-let* ((typs (ensure-list typ/s)))
    (treesit-parent-until
     child-node
     (lambda (node) (member (treesit-node-type node) typs))
     include-self)))

;;;###autoload
(defun my-treesit-pulse-node (node)
  (interactive (list (treesit-node-at (point))))
  (pulse-momentary-highlight-region
   (treesit-node-start node) (treesit-node-end node))
  node)

;;;###autoload
(defun my-treesit-node-bounds (node &optional as-cons)
  (-let* ((beg (treesit-node-start node))
          (end (treesit-node-end node)))
    (cond
     (as-cons
      (cons beg end))
     (:else
      (list beg end)))))

;;;###autoload
(defun my-treesit-named-node? (node)
  (declare (side-effect-free t))
  (treesit-node-check node 'named))
;;;###autoload
(defalias #'my-treesit-named-node-p #'my-treesit-named-node?)

(defun my-treesit-unnamed-child-node-between-named-p (parent-node)
  (named-let
      recur ((cur-node (treesit-node-child parent-node 0)))
    (let* ((next1-node (treesit-node-next-sibling cur-node))
           (next2-node (treesit-node-next-sibling next1-node)))
      (cond
       ((null next2-node)
        nil)
       ((and next1-node
             next2-node
             (treesit-node-check cur-node 'named)
             (not (treesit-node-check next1-node 'named))
             (treesit-node-check next2-node 'named))
        t)
       (t
        (recur next1-node))))))

;;;###autoload
(defun my-treesit-top-level-node (&optional pos)
  (-let* ((pos (or pos (point)))
          (node0 (treesit-node-at pos))
          (root (treesit-buffer-root-node)))
    (treesit-parent-while node0 (lambda (node) (not (equal node root))))))

(when (not (fboundp #'treesit--things-around))
  ;; TODO: migrate from this when emacs 30.0.60 is out
  (defun treesit--things-around (pos thing)
    (list
     (treesit--thing-prev pos thing)
     (treesit--thing-next pos thing)
     (treesit--thing-at pos thing))))


;;; Commands

(defmacro my-treesit-go-defun--wrap (&rest body)
  (declare (indent defun) (debug t))
  `(-let* ((pt0 (point)))
     (prog1 (progn
              ,@body)
       (when (and (not (and transient-mark-mode mark-active)) (/= pt0 (point)))
         (push-mark pt0))
       (when treesit-defun-skipper
         (funcall treesit-defun-skipper)))))

;;;###autoload
(defun my-treesit-go-beg-of-defun (&optional arg)
  (interactive "^P")
  (-let* ((pnv (prefix-numeric-value arg)))
    (my-treesit-go-defun--wrap
      (cond
       ((equal arg 0)
        (dlet ((treesit-defun-tactic 'top-level))
          (treesit-beginning-of-defun 1)))
       ((equal arg 1)
        (dlet ((treesit-defun-tactic 'nested))
          (goto-char (treesit-node-start (treesit-defun-at-point)))))
       (:else
        (treesit-beginning-of-defun pnv))))))

;;;###autoload
(defun my-treesit-go-end-of-defun (&optional arg)
  (interactive "^P")
  (-let* ((pnv (prefix-numeric-value arg)))
    (my-treesit-go-defun--wrap
      (cond
       ((equal arg 0)
        (dlet ((treesit-defun-tactic 'top-level))
          (treesit-end-of-defun 1)))
       ((equal arg 1)
        (dlet ((treesit-defun-tactic 'nested))
          (goto-char (treesit-node-end (treesit-defun-at-point)))))
       (:else
        (treesit-end-of-defun pnv))))))

;;;###autoload
(defun my-treesit-mark-current-defun (&optional beg-to-end)
  (interactive)
  (-when-let* ((node (treesit-defun-at-point))
               (beg (treesit-node-start node))
               (end (treesit-node-end node)))
    (push-mark)
    (cond
     (beg-to-end
      (push-mark beg nil t)
      (goto-char end))
     (:else
      (push-mark end nil t)
      (goto-char beg)))
    (list beg end)))

;;;###autoload
(defun my-mark-treesit-defun-maybe (&optional arg interact)
  "(`mark-defun' ARG INTERACT).
Fix case when `mark-defun' marks a function that isn't relevant
to current point."
  (interactive "p\nd")
  (-let* ((pt0 (point))
          (rap0 (use-region-p))
          (mark-defun-retval (mark-defun arg interact)))
    (cond
     ((or rap0 (/= 1 arg))
      mark-defun-retval)
     ((not (use-region-p))
      (my-treesit-mark-current-defun))
     ((not (<= (region-beginning) pt0 (region-end)))
      (deactivate-mark)
      (goto-char pt0)
      (my-treesit-mark-current-defun)))))


;;; Config functions

;;;###autoload
(progn

  (defun my-treesit-available-p ()
    (if (fboundp #'treesit-available-p)
        (progn
          (defalias #'my-treesit-available-p #'treesit-available-p)
          (treesit-available-p))
      (progn
        (defalias #'my-treesit-available-p #'ignore)
        nil)))

  (defun my-treesit-language-available-p (language &rest args)
    (if (fboundp #'treesit-language-available-p)
        (progn
          (defalias
            #'my-treesit-language-available-p #'treesit-language-available-p)
          (apply #'treesit-language-available-p language args))
      (progn
        (defalias #'my-treesit-language-available-p #'ignore)
        nil)))

  (defun my-treesit-parser-list (&optional buffer)
    "(`treesit-parser-list' BUFFER). Can be safely used detect treesit."
    (cond
     ((fboundp #'treesit-parser-list)
      (defalias #'my-treesit-parser-list #'treesit-parser-list)
      (treesit-parser-list buffer))
     (:else
      (defalias #'my-treesit-parser-list #'ignore)
      nil)))

  ;;
  )

(defvar-local my-treesit-language-available-cached-p--table '())
;;;###autoload
(defun my-treesit-language-available-cached-p (language &optional fresh)
  (when fresh
    (setq-local my-treesit-language-available-cached-p--table '()))
  (-let* ((cached
           (alist-get language my-treesit-language-available-cached-p--table)))
    (cond
     ((equal :false cached)
      nil)
     (cached
      cached)
     (t
      (-let* ((got (treesit-language-available-p language)))
        (setf (alist-get language my-treesit-language-available-cached-p--table)
              (or got :false))
        got)))))

;;;###autoload
(defun my-treesit-parser-path-list (&optional no-shorten)
  (-let* ((glob "libtree-sitter-*"))
    (-->
     (append
      (-mapcat
       (lambda (dir)
         (file-expand-wildcards (file-name-concat dir glob)))
       treesit-extra-load-path)
      (file-expand-wildcards
       (file-name-concat user-emacs-directory "tree-sitter" glob))
      (file-expand-wildcards (concat "/lib/" glob)))
     (if no-shorten
         it
       (-map #'abbreviate-file-name it)))))

;;;###autoload
(defun my-treesit-installed-language-grammar-list ()
  (-->
   (my-treesit-parser-path-list t) (-map #'file-name-base it)
   (-map
    (lambda (file-name)
      (-->
       (string-remove-prefix "libtree-sitter-" file-name)
       (pcase it
         ("cpp" "c++")
         (_ it))))
    it)
   (-uniq it)))

;;;###autoload
(progn
  (defun my-treesit-mode->ts-mode (mode)
    (-let* ((str (format "%s" mode)))
      (if (string-suffix-p "-ts-mode" str)
          mode
        (intern (replace-regexp-in-string "-mode$" "-ts-mode" str)))))
  (defun my-treesit-ts-mode->mode (mode)
    (intern (replace-regexp-in-string "-ts-mode$" "-mode" (format "%s" mode)))))

;;;###autoload
(defun my-treesit-language->legacy-modes (lang-or-mode)
  "Return the list of major modes to be remapped for LANG-OR-MODE."
  (-let* ((lang
           (cond
            ((symbolp lang-or-mode)
             (my-prog-lang-of-mode lang-or-mode t))
            (t
             lang-or-mode))))
    (pcase lang

      ("bash" '(sh-mode))
      ("html" '(html-mode mhtml-mode))
      ("javascript" '(javascript-mode js-mode))
      ("json" '(json-mode js-json-mode))
      ("rust" '(rust-mode rustic-mode))
      ;; ("tsx" '(js-mode ))

      (_ (list (intern (format "%s-mode" lang)))))))

;;;###autoload
(defun my-treesit-make-major-mode-remap-alist ()
  (-let* ((langs (my-treesit-installed-language-grammar-list)))
    (cl-loop
     for lang in langs append
     (-let* ((legacy-modes (my-treesit-language->legacy-modes lang))
             (ts-mode
              (-->
               (intern (format "%s-ts-mode" lang))
               (pcase it
                 ('javascript-ts-mode 'js-ts-mode)
                 (_ it)))))
       (-map (lambda (legacy-mode) (cons legacy-mode ts-mode)) legacy-modes)))))

;;;###autoload
(defun my-treesit-use-ts-modes (&optional reset)
  (declare (obsolete nil nil))
  (if reset
      (progn
        (custom-reevaluate-setting 'major-mode-remap-alist)
        (custom-save-all))
    (progn
      (customize-save-variable
       'major-mode-remap-alist (my-treesit-make-major-mode-remap-alist)))))


;;;###autoload
(cl-defun my-treesit-ensure-parser-in-buffer (language &optional buffer &key fresh)
  (-let* ((lst
           (-map
            #'treesit-parser-language
            (my-safe-call #'treesit-parser-list buffer))))
    (when (and (not (member language lst))
               (my-treesit-language-available-cached-p language fresh))
      (treesit-parser-create language buffer))))

;; ;;;###autoload
;; (defun my-treesit-move-defun-change-tactic-a (func &rest args)
;;   "FUNC ARGS.
;; prefix arg = 0: `treesit-defun-tactic' <- 'top-level
;; prefix arg = 1:
;; "
;;   (cond
;;    ;; when called interactively and prefix arg is 1, use top-level tactic
;;    ((and (called-interactively-p 'any) (member current-prefix-arg '(1)))
;;     (dlet ((treesit-defun-tactic 'top-level))
;;       (apply func args)))
;;    (:else
;;     (apply func args))))

;; (my-add-advice/s '(treesit-beginning-of-defun treesit-end-of-defun) :around '(my-treesit-move-defun-change-tactic-a))


;;; my-functions-treesit.el ends here

(provide 'my-functions-treesit)

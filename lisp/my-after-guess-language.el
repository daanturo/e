;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-backtrace)


;; TODO: feature request those autoloads
;;;###autoload
(progn

  (autoload #'guess-language "guess-language")
  (autoload #'guess-language-buffer "guess-language")
  (autoload #'guess-language-paragraph "guess-language")
  (autoload #'guess-language-region "guess-language"))



(setq!
 guess-language-languages
 (-map
  #'intern
  (-intersection
   ;; system locales
   (my-locale-language-code-list)
   ;; supported by the package
   (directory-files guess-language-trigrams-directory
                    nil
                    directory-files-no-dot-files-regexp))))

(add-hook
 'guess-language-after-detection-functions
 #'my-guess-language-switch-jinx-function)

;;; after-guess-language.el ends here

(provide 'my-after-guess-language)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;;###autoload
(defun my-findutil-in-project-or-prompt-project ()
  "Visit a file or directory in the current project."
  (interactive)
  (if-let ((pr (my-project-root)))
      (let ((root-dir (abbreviate-file-name pr)))
        (my-findutil-find-file-recursively
         root-dir
         ;; :files (-map (lambda (file) (string-remove-prefix root-dir file)) (my-project-file-and-directory-list))
         :category 'project-file))
    (progn
      (call-interactively #'project-dired))))

(defvar my-findutil-fd-base-command-args
  '("fd"
    "--full-path"
    "--color=never"
    "--hidden"
    "--follow"
    "--exclude"
    "**/.git/"))

(defvar my-findutil-find-exclude-patterns '())
(add-hook 'my-findutil-find-exclude-patterns "*/.git/*")

(defvar my-findutil-backends
  `(
    ;;
    ("fd" ,(lambda () (executable-find "fd")))
    ("find" ,(lambda () (executable-find "find")))
    ;;
    ))

(defun my-findutil-get-backend (&optional excludes includes)
  (-some
   (-lambda ((name condition . _))
     (and (or includes (not (member name excludes))) (funcall condition) name))
   (or includes my-findutil-backends)))

(defun my-findutil-process-output-lines (cmdargs/s &optional return-error)
  (-let* ((cmdargs-list
           (cond
            ((listp (nth 0 cmdargs/s))
             cmdargs/s)
            (:else
             (list cmdargs/s)))))
    (-mapcat
     (lambda (cmdargs)
       (and (not (equal cmdargs '()))
            (condition-case err
                (apply #'process-lines cmdargs)
              (error (and  err)))))
     cmdargs-list)))

(cl-defun my-findutil-find-compute-command-args (&key pre suf type +exclude)
  `("find" ,@pre ,@(cond
                    ((equal "d" type)
                     (list "-mindepth" "1" "-type" "d"))
                    (type
                     (list "-type" type)))
    "-not" "(" ,@(-->
                  (-mapcat
                   (lambda (patt) (list "-o" "-path" patt))
                   (append my-findutil-find-exclude-patterns +exclude))
                  (drop 1 it))
    "-prune" ")" ,@suf))

;;;###autoload
(defun my-findutil-file-command-args (&optional backend)
  (-let* ((alist
           `(("fd" . (,@my-findutil-fd-base-command-args "--type=file"))
             ;; ("git" . ,my-vc-git-list-non-ignored-files-command-args-list)
             ("find" . ,(my-findutil-find-compute-command-args :type "f")))))
    (cdr (assoc (or backend (my-findutil-get-backend)) alist))))

;;;###autoload
(defun my-findutil-dir-command-args (&optional backend)
  (-let* ((alist
           `(("fd" . (,@my-findutil-fd-base-command-args "--type=directory"))
             ("find" . ,(my-findutil-find-compute-command-args :type "d")))))
    (cdr
     (assoc
      (or backend (my-findutil-get-backend)) alist))))

;;;###autoload
(defun my-findutil-symlink-command-args (&optional backend)
  (-let* ((alist
           `(("fd" .
              (,@my-findutil-fd-base-command-args
               ;; "--no-ignore-vcs" "--no-follow" "--no-hidden"
               "--type=symlink"))
             ("find" . ,(my-findutil-find-compute-command-args :type "l")))))
    (cdr (assoc (or backend (my-findutil-get-backend)) alist))))

;;;###autoload
(cl-defun my-findutil-file|dir-command-args (&optional include-hidden &key backend)
  (-let* ((alist
           `(("fd" .
              (,@my-findutil-fd-base-command-args
               ,(if include-hidden
                    "--hidden"
                  "--no-hidden")))
             ("find" .
              ,(my-findutil-find-compute-command-args
                :+exclude
                (if include-hidden
                    nil
                  '("*/.*")))))))
    (cdr (assoc (or backend (my-findutil-get-backend)) alist))))


;;;###autoload
(defun my-findutil-rotate-backend-executables (&optional local)
  (interactive "P")
  (-let* ((val
           (-rotate
            -1
            (if local
                my-findutil-backends
              (default-value 'my-findutil-backends)))))
    (if local
        (setq-local my-findutil-backends val)
      (setq-default my-findutil-backends val))
    (message "`my-findutil-rotate-backend-executables': %S" (-map #'car val))
    val))

;; `affe-find' still handle hidden files better

;;;###autoload
(defun my-findutil-async-from-home (&optional include-hidden)
  (interactive "P")
  (-let* ((dir "~")
          (cmdargs (my-findutil-file|dir-command-args include-hidden)))
    (cond
     (include-hidden
      (my-affe-find dir :find-cmdargs cmdargs))
     ((executable-find "fd")
      (dlet ((consult-fd-args cmdargs))
        (consult-fd dir)))
     ((executable-find "find")
      (dlet ((consult-find-args cmdargs))
        (consult-find dir))))))

;;;###autoload
(defun my-fzf-from-home ()
  (interactive)
  (dlet ((fzf/args (concat fzf/args " " fzf/args-for-preview " --color=16"))
         (fzf/window-height most-positive-fixnum)
         ;; (default-directory "~")
         )
    (fzf-find-file "~")))

;;;###autoload
(defun my-fzf-cands-from-term (&optional dir)
  (interactive)
  (dlet ((default-directory (or dir default-directory)))
    (-let* ((fzf-prompt "---")
            (term-buf
             (save-window-excursion
               (ansi-term
                (combine-and-quote-strings
                 (list
                  "fzf"
                  "--border=none"
                  "--height=7749"
                  "--info=hidden"
                  "--marker=''"
                  "--no-scrollbar"
                  "--no-separator"
                  "--pointer=''"
                  (format "--prompt='%s'" fzf-prompt)))
                (generate-new-buffer-name " my-fzf-cands-from-term"))))
            (get-cands-fn
             (lambda ()
               (with-current-buffer term-buf
                 (-let* ((str-out
                          (buffer-substring-no-properties
                           (point-min) (point-max)))
                         (lines
                          (-->
                           str-out (split-string it nil t)
                           (-remove
                            (lambda (line)
                              (string-prefix-p fzf-prompt line))
                            it))))
                   (-map #'string-trim lines)))))
            old-input
            (choice
             (unwind-protect
                 (dlet ((completion-styles '(basic)))
                   (completing-read
                    "fzf: "
                    (lambda (string pred action)
                      (-let* ((input (string-trim string)))
                        (when (not (equal old-input input))
                          (with-current-buffer term-buf
                            (term-send-raw-string (kbd "C-u"))
                            (term-send-raw-string input))
                          (setq old-input input)))
                      (cond
                       ((equal action 'metadata)
                        `(metadata (category . file)))
                       (t
                        (complete-with-action
                         action (funcall get-cands-fn) string pred))))))
               (my-kill-buffer-no-ask term-buf))))
      choice)))

;;;###autoload
(cl-defun my-findutil-async (&optional dir &key initial-input)
  (interactive)
  (my-affe-find dir :initial-input initial-input))

;;;###autoload
(cl-defun my-affe-find (&optional dir &key initial-input find-cmdargs)
  (interactive)
  (dlet ((affe-find-command
          (concat
           (mapconcat
            (lambda (str) (replace-regexp-in-string "\\(\\*\\)" "\\\\\\1" str))
            (or find-cmdargs (my-findutil-file|dir-command-args t))
            " ")
           " --follow")))
    (-let* ((file (--> (affe-find dir initial-input) (expand-file-name it dir))))
      ;; handle whether `affe-find' visits the find or not
      (unless (file-equal-p file (my-buffer-filename))
        (find-file file))
      file)))

;;;###autoload
(defun my-findutil-get-file-list-recursive-by-command (&optional dir backend)
  (dlet ((default-directory (or dir default-directory)))
    (-map
     (my-fn% (string-remove-prefix "./" %1))
     ;; TODO: consider non-ignored submodules
     (or
      ;; (my-vc-list-non-ignored-files)
      (my-findutil-process-output-lines
       (my-findutil-file-command-args backend))))))

;;;###autoload
(cl-defun my-findutil-get-dir-list-recursive-by-command (&optional dir backend &key files cmd)
  (dlet ((default-directory (or dir default-directory))
         (file-name-handler-alist '()))
    (-map
     (my-fn% (string-remove-prefix "./" (file-name-as-directory %1)))
     (cond
      (files
       (-->
        files (-map #'file-name-directory it) (delete-dups it) (delete nil it)))
      (:else
       (my-findutil-process-output-lines
        (or cmd (my-findutil-dir-command-args backend))))))))

;;;###autoload
(defun my-findutil-get-symlink-list-recursively-by-command
    (&optional dir backend)
  (dlet ((default-directory (or dir default-directory)))
    (-map
     (my-fn% (string-remove-prefix "./" %1))
     (my-findutil-process-output-lines
      (my-findutil-symlink-command-args backend)))))

;;;###autoload
(cl-defun my-findutil-get-file-and-dir-list-recursive (&optional (dir default-directory))
  "Recursively get list of files from DIR."
  (my-with-deferred-gc
   (-let* ((be (my-findutil-get-backend))
           (file-list (my-findutil-get-file-list-recursive-by-command dir be)))
     (-->
      (append
       file-list
       (my-findutil-get-dir-list-recursive-by-command
        dir be
        ;; find isn't .gitignore-aware, maybe very slow
        :files
        (if (member be '("find"))
            file-list
          nil))
       (my-findutil-get-symlink-list-recursively-by-command dir be))
      ;; (my-vc-git-filter-remove-ignored it dir)
      ))))

;;;###autoload
(cl-defun my-findutil-completing-read-file-recursively (prompt &optional dir &key files category initial-input pred)
  (let ((dir (or dir default-directory))
        (category         
         (or category
             (if (my-project-directory-is-root-p dir)
                 'project-file
               'file))))
    ;; `read-file-name-default' binds `default-directory'
    (dlet ((default-directory dir))
      (-->
       (completing-read prompt
                        (lambda (string predicate action)
                          (cond
                           ((equal 'metadata action)
                            `(metadata (category . ,category)))
                           (:else
                            (let* ((filenames
                                    (or
                                     files
                                     (my-findutil-get-file-and-dir-list-recursive
                                      dir))))
                              (complete-with-action
                               action filenames string predicate)))))
                        pred nil initial-input)
       (file-name-concat dir it)))))

;;;###autoload
(cl-defun my-findutil-find-file-recursively (&optional dir &rest args &key prompt &allow-other-keys)
  "Find/open file from DIR, prompt to read from the recursive list."
  (interactive (list default-directory))
  (-let* ((dir (or dir default-directory))
          (prompt (or prompt (format "Find file in %s: " dir))))
    (find-file (apply #'my-findutil-completing-read-file-recursively prompt dir args))))

;;;###autoload
(defun my-findutil-fallback-to-async (&optional dir &rest args)
  (interactive)
  (-let* ((dir (or dir (my-project-root) default-directory))
          (files
           ;; switching (GUI) window also aborts with `this-command' being
           ;; `nil', but I haven't found a way to detect that and continue
           ;; evaluation
           (while-no-input
             (my-findutil-get-file-and-dir-list-recursive dir))))
    (cond
     ((equal files t)
      (message
       "`my-findutil-fallback-to-async': can't wait, switched to `my-findutil-async' instead.")
      (apply #'my-findutil-async dir args))
     (t
      (apply #'my-findutil-find-file-recursively dir :files files args)))))

;;;###autoload
(defun my-findutil-dir-list (&optional dir)
  (when-let ((default-directory (or dir default-directory)))
    (cond
     ((-let* ((cmdargs (my-findutil-dir-command-args)))
        (and cmdargs
             (my-findutil-get-dir-list-recursive-by-command
              default-directory
              nil
              :cmd cmdargs))))
     (t
      (-map
       #'file-name-as-directory
       (directory-files-recursively dir ""
                                    'include-directories
                                    #'file-directory-p))))))

;;; my-functions-findutil.el ends here

(provide 'my-functions-findutil)

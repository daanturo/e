;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'gptel)

;;;###autoload
(defun my-gptel-set-model-and-backend ()
  (interactive)
  (-let* ((model-backend-table
           (-mapcat
            (-lambda ((_ . backend))
              (-let* ((models (gptel-backend-models backend)))
                (--map (list it backend) models)))
            gptel--known-backends))
          (cptr-assoc-list
           (-map
            (-lambda ((model backend))
              (list
               (concat
                model "\t\t"
                (-->
                 (format
                  "(%s\t%s)"
                  (gptel-backend-name backend) (gptel-backend-host backend))
                 (propertize it 'face 'completions-annotations)))
               model backend))
            model-backend-table))
          (annot-fn
           (lambda (cand)
             ;; (-let* (((_ _ backend) (assoc cand cptr-assoc-list)))
             ;;   (-->
             ;;    (format "\t\t%s" backend)
             ;;    (propertize it 'face 'completions-annotations)))
             ))
          (cptr-coll (-map #'car cptr-assoc-list))
          (str-choice
           (completing-read
            "Model: "
            (lambda (string pred action)
              (if (eq action 'metadata)
                  `(metadata (annotation-function . ,annot-fn))
                (complete-with-action action cptr-coll string pred))))))
    (-let* (((_ model backend) (assoc str-choice cptr-assoc-list)))
      (setq gptel-model model)
      (setq gptel-backend backend))))


;;; my-functions-gptel.el ends here

(provide 'my-functions-gptel)

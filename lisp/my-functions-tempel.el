;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)



(defvar-local my-tempel-expandable-p--cache nil)
;;;###autoload
(defun my-tempel-expandable-p (&optional bounds-fn)
  (when (require 'tempel nil 'noerror)
    (-let*
        ()
      ;; (cache-key (vector (point) bounds-fn (my-buffer-string-no-properties)))
      
      (
       ;; with-memoization (alist-get cache-key my-tempel-expandable-p--cache nil nil #'equal)
       progn
       ;; only store the latest cache
       (setq-local my-tempel-expandable-p--cache nil)
       (-when-let* ((bounds
                     (cond
                      (bounds-fn
                       (funcall bounds-fn))
                      (:else
                       (bounds-of-thing-at-point 'symbol))))
                    ((beg . end) bounds)
                    ;; only expand when at the end
                    (_ (= end (point)))
                    (template-name (buffer-substring-no-properties beg end))
                    (template-table (tempel--templates))
                    (template
                     (-some-->
                         template-name (intern it) (assoc it template-table))))
         (list beg end template))))))

;;;###autoload
(defun my-tempel-done-h (&rest _)
  (and tempel--active
       (progn (tempel-done)
              t)))

;;;###autoload
(defun my-tempel-expand-word ()
  "Expand the word before point only (not the whole symbol)."
  (interactive)
  (backward-word 1)
  (-let* ((pt--1 (point-marker)))
    (insert " ")
    (forward-word 1)
    (unwind-protect
        (tempel-expand t)
      (save-excursion
        (goto-char pt--1)
        (delete-char 1)))))

;;;###autoload
(cl-defun
    my-tempel-expand-prev-sexp-to-point (&optional interactive)
  "Including templates with special characters that aren't otherwise matched.
INTERACTIVE: doesn't act as a \"capf\"."
  (interactive (list t))
  (if interactive
      (tempel--interactive #'my-tempel-expand-prev-sexp-to-point)
    (-when-let* (((beg end template)
                  (my-tempel-expandable-p #'my-bounds-of-prev-sexp-to-point))
                 (matching-templates (list template)))
      (list
       beg
       end
       matching-templates
       :category 'tempel
       :exclusive 'no
       :exit-function (apply-partially #'tempel--exit matching-templates nil)))))

;; too low-level and prone to changes, don't use this
(defun my-tempel--get-templates-from-completion-table-closure (func)
  (cond
   ((interpreted-function-p func)
    (-some--> func (aref it 2) (or (cdr (assoc 'templates it)) (cdar it))))
   ((or (byte-code-function-p func) (native-comp-function-p func))
    (-some--> func (aref it 2) (aref it 0)))))

;;;###autoload
(defun my-tempel--completion-table--mark-candidates-a
    (func templates &rest args)
  (dolist (str templates)
    (put-text-property 0 0 'my-snippet t str))
  (apply func templates args))

;;;###autoload
(defun my-tempel-complete-capf ()
  (declare)
  ;; (my-with-advice
  ;;   #'tempel--completion-table
  ;;   :around
  ;;   #'my-tempel--completion-table--mark-candidates-a
  ;;   (tempel-complete))
  (cond
   ((fboundp #'cape-wrap-silent)
    (cape-wrap-silent #'tempel-complete))
   (:else
    (tempel-complete))))
;; (-when-let* ((orig (tempel-complete)))
;;   (-let* (((beg end table-fn . rest) orig))
;;     (apply #'list
;;            beg end
;;            (lambda (str pred action)
;;              (cond
;;               ((equal t action))
;;               (:else
;;                (funcall table-fn str pred action))))
;;            rest)))


;;;###autoload
(defun my-tempel-at-line-beg (str &optional no-newline)
  (when
      ;; also consider read-only prompts
      (= (line-beginning-position) (point))
    (insert str)
    (when (not no-newline)
      (newline-and-indent))))

;;; my-functions-tempel.el ends here

(provide 'my-functions-tempel)

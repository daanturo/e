;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'telega)

;; ;;;###autoload
;; (defun my-telega-completing-read-function
;;     (prompt coll &optional pred _require-match &rest args)
;;   (apply completing-read-function
;;          prompt coll pred
;;          ;; ignore require-match being
;;          nil args))

;;;###autoload
(defun my-telega-log (&rest args)
  (apply #'my-log-to* (format "*%s*" 'my-telega-log) args))

;;;###autoload
(defun my-telega-in-background ()
  (interactive)
  (save-window-excursion (telega t)))

;;;###autoload
(defun my-telega-group-or-channel-id->chat-id (supergroup-id)
  (-let* ((str (format "%s" supergroup-id)))
    (cond
     ((string-prefix-p "-100" str)
      supergroup-id)
     (:else
      (string-to-number
       (my-ensure-string-prefix "-100" (format "%s" (abs supergroup-id))))))))

;;;###autoload
(defun my-telega-current-chat ()
  (or telega-chatbuf--chat telega--chat (telega-chat-at (point))))

;;;###autoload
(defun my-telega-get-chat (&optional obj-or-ID basic)
  (-let* ((basic-ID (- (abs obj-or-ID))))
    (cond
     ((or (numberp obj-or-ID))
      (or (ignore-errors
            (telega-chat-get
             (if basic
                 basic-ID
               (my-telega-group-or-channel-id->chat-id obj-or-ID))))
          (telega-chat-get basic-ID)))
     (obj-or-ID
      obj-or-ID)
     (t
      (my-telega-current-chat)))))

;;;###autoload
(defun my-telega-get-user (obj-ID-or-username)
  (cond
   ((and (stringp obj-ID-or-username) (string-prefix-p "@" obj-ID-or-username))
    (telega-user--by-username obj-ID-or-username))
   ((or (stringp obj-ID-or-username) (numberp obj-ID-or-username))
    (telega-user-get obj-ID-or-username))
   (t
    obj-ID-or-username)))

;;;###autoload
(defun my-telega-get-username (obj-or-ID &optional with-@)
  (-let* ((user (my-telega-get-user obj-or-ID)))
    (-some-->
        user
      (plist-get it :usernames)
      (plist-get it :active_usernames)
      (seq-first it)
      (if with-@
          (concat "@" it)
        it))))

;;;###autoload
(defun my-telega-get-name-of-user (obj-or-ID)
  (-let* ((user (my-telega-get-user obj-or-ID)))
    (-->
     (list (my-get-in* user :first_name) (my-get-in* user :last_name))
     (remove "" it)
     (-map #'my-telega-get-string-display it)
     (string-join it " "))))

;;;###autoload
(defun my-telega-get-user-info-str (obj-or-ID)
  (-let* ((user (my-telega-get-user obj-or-ID))
          (id (plist-get user :id))
          (username (or (my-telega-get-username user t) ""))
          (name
           (-->
            (list
             (plist-get user :first_name) (plist-get user :last_name))
            (string-join it " ") (my-telega-get-string-display it)))
          (phone_number
           (-some-->
               (plist-get user :phone_number)
             (if (seq-empty-p it)
                 ""
               (my-ensure-string-prefix "+" it)))))
    (my-s$ "ID: ${id}, ${username}, ${name}, ${phone_number}")))

;;;###autoload
(defun my-telega-message-sender-and-forwarded-original-id (msg)
  (-->
   (my-telega-get-message msg)
   (list
    (map-nested-elt it '(:sender_id :user_id))
    (map-nested-elt it '(:forward_info :origin :sender_user_id)))))

;;;###autoload
(defun my-telega-get-message-original-sender-id (msg)
  (-->
   (my-telega-get-message msg)
   (or (map-nested-elt it '(:forward_info :origin :sender_user_id))
       (map-nested-elt it '(:forward_info :origin :chat_id)))))

;;;###autoload
(defun my-telega-try-get-message-original-sender-id (msg)
  (-->
   (my-telega-get-message msg)
   (or (my-telega-get-message-original-sender-id it)
       (map-nested-elt it '(:sender_id :user_id)))))

;;;###autoload
(defun my-telega-message-sent-by-or-forwarded-from (msg user-id)
  (member
   user-id
   (my-telega-message-sender-and-forwarded-original-id
    (my-telega-get-message msg))))

;;;###autoload
(defun my-telega-get-type-of-message (msg)
  (--> (my-telega-get-message msg) (my-get-in* it :content :@type)))

;;;###autoload
(defun my-telega-message-type-in-p (msg types)
  (--> (my-telega-get-type-of-message msg) (member it types)))

;;;###autoload
(defun my-telega-get-chat-member-count (chat)
  (plist-get (telega-chat--info chat) :member_count))

;;;###autoload
(defun my-telega-get-chat-member-list (&optional chat not-bots)
  (-let* ((chat (my-telega-get-chat chat)))
    (-->
     (telega--searchChatMembers
         chat
         ""
         nil
       :limit (my-telega-get-chat-member-count chat))
     (-filter
      (cond
       (not-bots
        (-lambda
          ((&plist :type)) (not (equal "userTypeBot" (plist-get type :@type)))))
       (t
        #'always))
      it))))

;;;###autoload
(defun my-telega-get-chat-member-limited-message-list
    (chat sender &optional filter from-msg-id limit)
  (telega--searchChatMessages
      ;; https://core.telegram.org/tdlib/docs/classtd_1_1td__api_1_1search_chat_messages.html
      (my-telega-get-chat chat)
      `(:@type ,(or filter "searchMessagesFilterEmpty"))
      (or from-msg-id 0)
      0
    :limit (or limit 16384)             ; actually limited to 100
    :sender (my-telega-get-user sender)))

;;;###autoload
(defun my-telega-get-chat-all-message-list
    (chat &optional log-file msg-filter-fn max-wait-between-requests from-msg-id)
  (-let* ((chat-obj (my-telega-get-chat chat)))
    (named-let
        recur ((from (or from-msg-id 0)) (message-coll []))
      (-let* ((chathist (telega--getChatHistory chat-obj from 0 100))
              (messages (my-get-in* chathist :messages))
              (filtered
               (if msg-filter-fn
                   (seq-filter msg-filter-fn messages)
                 messages))
              (next-from (-some--> messages (my-at* it -1) (my-get-in* it :id))))
        (when log-file
          (with-temp-buffer
            (seq-doseq (msg filtered)
              (insert (format "%S\n" msg)))
            (append-to-file (point-min) (point-max) log-file)))
        ;; too many requests!
        (sleep-for (cl-random (or max-wait-between-requests 0.125)))
        (cond
         ((null chathist)
          (sleep-for (cl-random 1.0))
          (recur from message-coll))
         ((seq-empty-p messages)
          (nreverse message-coll))
         (t
          (recur next-from (seq-concatenate 'vector message-coll filtered))))))))

;;;###autoload
(defun my-telega-get-chat-member-messages-count (chat sender)
  (-let* ((returned (my-telega-get-chat-member-limited-message-list chat sender))
          (_total (plist-get returned :total_count))
          (messages
           (-->
            (plist-get returned :messages)
            (seq-remove #'telega-msg-special-p it))))
    (length messages)))

;;;###autoload
(defun my-telega-get-chat-members-with-messages-count (chat)
  (-->
   (my-for
     [mem (my-telega-get-chat-member-list chat t)]
     (list
      :user mem
      :total_count (my-telega-get-chat-member-messages-count chat mem)))
   (sort
    it
    (lambda (arg0 arg1)
      (< (plist-get arg0 :total_count) (plist-get arg1 :total_count))))))

;;;###autoload
(defun my-telega-get-chat-members-with-messages-count* (chat upper-bound)
  "Like `my-telega-get-chat-members-with-messages-count' CHAT but human-readable.
Only list members with fewer or equal than UPPER-BOUND messages."
  (-let* ((stats (my-telega-get-chat-members-with-messages-count chat))
          (max-digits (length (format "%s" upper-bound))))
    (-->
     (my-for
       [elem stats
             :let [num (plist-get elem :total_count)]
             :when (<= num upper-bound)]
       (-let* ((in4 (my-telega-get-user-info-str (plist-get elem :user)))
               (num* (format (my-s$ "%${max-digits}s") num)))
         (my-s$ "${num*} msgs ${in4}")))
     (string-join it "\n"))))

;;;###autoload
(defun my-telega-message->url (msg)
  (cl-assert msg)
  (--> (telega--getMessageLink msg)))

;;;###autoload
(defun my-telega-url->message (msg-link)
  (cl-assert msg-link)
  (-->
   (telega-server--call (list :@type "getMessageLinkInfo" :url msg-link))
   (map-nested-elt it '(:message))))

;;;###autoload
(defun my-telega-message-id->message (msg-id chat-id)
  (telega-server--call
   (list
    :@type "getMessage"
    :chat_id (my-telega-group-or-channel-id->chat-id chat-id)
    :message_id msg-id)))

;;;###autoload
(defun my-telega-get-message (obj &optional from-chat-id)
  (cond
   ((and (numberp obj) from-chat-id)
    (my-telega-message-id->message obj from-chat-id))
   ((stringp obj)
    (my-telega-url->message obj))
   (t
    obj)))

;;;###autoload
(defun my-telega-get-message-text (msg)
  (-->
   msg
   (my-telega-get-message it)
   (or (map-nested-elt it '(:content)))
   (or (map-nested-elt it '(:caption :text)) (map-nested-elt it '(:text :text)))
   (my-telega-get-string-display it)))

;;;###autoload
(defun my-telega-get-string-display (str-obj)
  (and str-obj
       (named-let
           recur
           ((str (substring-no-properties str-obj))
            ;; replace from right to left, as some property lengths are 2
            (intervals (nreverse (object-intervals str-obj))))
         (-let* (((beg end prop) (car intervals))
                 (display (plist-get prop 'telega-display)))
           (cond
            ((seq-empty-p intervals)
             str)
            (display
             (recur
              (concat (substring str 0 beg) display (substring str end))
              (cdr intervals)))
            (t
             (recur str (cdr intervals))))))))

;;;###autoload
(defun my-telega-completing-read-search-message-filter ()
  (-let* ((types
           (-->
            telega-chat--message-filters
            (-keep
             (-lambda
               ((k . v))
               (and (proper-list-p v)
                    (--> (plist-get v :@type) (and it (stringp it) it))))
             it))))
    (completing-read "Message filter: " (cons "searchMessagesFilterEmpty" types)
                     nil
                     nil
                     nil
                     nil
                     "searchMessagesFilterEmpty")))

;;;###autoload
(defun my-telega-chatbuf-filter-by-sender-id
    (&optional query sender-id search-message-filter)
  (interactive (list
                nil
                (read-number "Sender ID: ")
                (my-telega-completing-read-search-message-filter)))
  (-let* ((sender (my-telega-get-user sender-id)))
    (telega-chatbuf-filter
     (list
      :title (format "search \"%s\"" query)
      :tdlib-msg-filter `(:@type ,(or search-message-filter "searchMessagesFilterEmpty"))
      :query query
      :sender sender))
    (when (and query (not (string-empty-p query)))
      (telega-highlight-text (regexp-quote query)))))

;;;###autoload
(defun my-telega-get-group-list ()
  (-->
   telega--ordered-chats
   (-filter
    (lambda (chat)
      (-let* ((typ (my-get-in* chat :type))
              (@typ (my-get-in* typ :@type)))
        (and (member @typ '("chatTypeSupergroup" "chatTypeBasicGroup"))
             (not (my-get-in* typ :is_channel)))))
    it)))

;;;###autoload
(defun my-telega-completing-read-chat-and-member ()
  (-let* ((chat
           (telega-completing-read-chat "Chat: " (my-telega-get-group-list))))
    (list
     chat
     ;; only 50?
     (telega-completing-read-chat-member "Member: " chat))))

;;;###autoload
(defun my-telega-delete-chat-messages-by-sender (chat-id sender-id &optional no-ask)
  (-let* ((chat-id (my-telega-group-or-channel-id->chat-id chat-id))
          (chat (my-telega-get-chat chat-id))
          (sender (my-telega-get-user sender-id)))
    (when (or no-ask
              (y-or-n-p
               (format "Delete all messages of %S (%s) from %S (%s)?"
                       (my-telega-get-user-info-str sender)
                       sender-id
                       (my-get-in* chat :title)
                       chat-id)))
      (telega-server--send
       (list
        :@type "deleteChatMessagesBySender"
        :chat_id chat-id
        :sender_id (telega--MessageSender sender))))))

;;;###autoload
(cl-defun my-telega-completing-read-owned-chat (&optional owner-only &key init-input)
  (-let* ((owned-chats
           (-filter
            (lambda (chat)
              (telega-chat-match-p
                  chat
                `(or saved-messages
                     (me-is-owner
                      ,@(if owner-only
                            '()
                          '(or-admin))))))
            telega--ordered-chats)))
    (minibuffer-with-setup-hook (lambda ()
                                  (when init-input
                                    (insert init-input)))
      (telega-completing-read-chat
       (format "Owned%s chat: "
               (if owner-only
                   ""
                 " (or admin)"))
       owned-chats))))

;;;###autoload
(defun my-telega-browse-url|link ()
  (declare (interactive-only t))
  (interactive)
  (telega-browse-url (read-string "URL: ")))

;;;###autoload
(defun my-telega-clear-cache (&optional ask)
  (interactive (list (not current-prefix-arg)))
  (-let* ((profile-photos-dir
           (file-name-concat telega-database-dir "profile_photos"))
          (cache-dir telega-cache-dir))
    (async-start
     (lambda ()
       (mapcar
        (lambda (file-or-dir)
          (if (file-directory-p file-or-dir)
              (delete-directory file-or-dir t)
            (delete-file file-or-dir)))
        (list cache-dir profile-photos-dir)))
     (lambda (_result)
       (make-directory cache-dir t)
       (make-directory profile-photos-dir t)
       (message "`telega': cleared %s" (list cache-dir profile-photos-dir))))))

;;;###autoload
(defun my-telega-msg-add-reaction-annotate-a (func &rest args)
  (dlet ((completion-extra-properties
          (my-merge-plist
           (bound-and-true-p completion-extra-properties)
           ;; Each emoji in `telega-emoji-reaction-list' looks fine, but their
           ;; extra text properties make displaying super ugly, let's annotate
           ;; each for readability, TODO: ask upsteam?
           `(:annotation-function
             ,(lambda (cand) (concat " " (substring-no-properties cand)))))))
    (apply func args)))

;;;###autoload
(defun my-telega-goto-random-message-in-chat ()
  (interactive)
  (-let* ((last-url
           (-->
            (my-telega-get-chat) (my-get-in* it :last_message)
            (condition-case _err
                (my-telega-message->url it)
              (error (telega-tme-internal-link-to it)))))
          ((_ chat-url idx-str)
           (or (s-match "^\\(.*/\\)\\([0-9]+\\)$" last-url)
               (s-match "^\\(tg:telega:@.*#\\)\\([0-9]+\\)" last-url)))
          (random-idx (+ 1 (random (string-to-number idx-str))))
          (url (format "%s%d" chat-url random-idx)))
    (message "`my-telega-goto-random-message-in-chat': %s" url)
    (telega-browse-url url)))

(defcustom my-telega-backup-user-emacs-config-directory-and-upload-chat-ID nil
  nil
  :group 'my-telega)

;;;###autoload
(defun my-telega-backup-user-emacs-config-directory-and-upload--1
    (&optional chat-dest)
  (require 'telega)
  (my-telega-in-background)
  (-let*
      ((run-fn
        (lambda ()
          (-let*
              ((chat-dest
                (or
                 chat-dest
                 (my-telega-get-chat
                  my-telega-backup-user-emacs-config-directory-and-upload-chat-ID))))
            (cl-assert chat-dest)
            (my-backup-user-emacs-config-directory
             (lambda (backup-path)
               (-let* ((file-name (file-name-nondirectory backup-path))
                       (caption-str file-name)
                       (chat-title (telega-chat-title chat-dest)))
                 (telega--sendMessage
                  chat-dest
                  `(:@type
                    "inputMessageDocument"
                    :document
                    ,(telega-chatbuf--gen-input-file backup-path 'Document)
                    :caption ,(telega-string-fmt-text caption-str))
                  nil nil
                  :callback
                  (lambda (&optional created-msg &rest _)
                    (cond
                     (created-msg
                      (notifications-notify
                       :body (format "Sending %s to %s" file-name chat-title)))
                     (:else
                      (notifications-notify
                       :body
                       (format "Failed to send %s to %s"
                               file-name
                               chat-title))))))))
             t)))))
    ;; the server may not be live yet
    (letrec
        ((timer
          (run-with-timer
           0.0 1.0
           (lambda ()
             (when (telega-server-live-p)
               (condition-case err
                   (progn
                     (funcall run-fn)
                     (cancel-timer timer))
                 (user-error
                  (message
                   "`my-telega-backup-user-emacs-config-directory-and-upload--1' error: %S"
                   err))))))))
      timer)))


;;;###autoload
(defun my-telega-backup-user-emacs-config-directory-and-upload
    (&optional chat-dest)
  (interactive)
  (require 'telega)
  (unless chat-dest
    (cl-assert my-telega-backup-user-emacs-config-directory-and-upload-chat-ID))
  (cond
   ((not (telega-server-live-p))
    (dlet ((comint-terminfo-terminal "xterm-256color")
           ;; (process-environment
           ;;  `(,@process-environment
           ;;    ,@(-difference
           ;;       (-some--> (frame-parameter (selected-frame) 'environment))
           ;;       process-environment)))
           )
      (with-environment-variables (("TERM" comint-terminfo-terminal))
        (my-process-shell-async
          `("emacsclient-named" "telega" "-c"
            ;; "--eval" "(toggle-debug-on-error)"
            "--eval"
            ,(format
              "%S"
              '(my-telega-backup-user-emacs-config-directory-and-upload--1)))))))
   (:else
    (my-telega-backup-user-emacs-config-directory-and-upload--1 chat-dest))))

;;;###autoload
(defun my-telega-completing-read-chat-no-sort (&optional prompt)
  (my-with-deferred-gc
   (telega-completing-read-msg-sender
    (or prompt "Telega chat: ") telega--ordered-chats)))

;;;###autoload
(defun my-telega-describe-chat|view-profile ()
  (declare (interactive-only t))
  (interactive)
  (telega-describe-chat (telega-completing-read-chat "View profile: ")))

;;;###autoload
(defun my-telega-get-chat-admin-list (chat)
  (-let* ((chat* (my-telega-get-chat chat)))
    (telega--searchChatMembers
        chat* ""
        (list :@type "chatMembersFilterAdministrators"))))

;;;###autoload
(defun my-telega-completing-read-chat (prompt &optional chats)
  (let* ((chats (or chats telega--ordered-chats))
         (completion-ignore-case t)
         (choices
          (-map
           (lambda (chat)
             (-let* ((title
                      (-->
                       chat
                       (plist-get it :title)
                       (my-telega-get-string-display it)))
                     (typ (my-get-in* chat :type :@type)))
               (cons
                (format "%s\t\"%s\"\t%s\t%d"
                        typ
                        title
                        (or (my-telega-get-username chat) "")
                        (my-get-in* chat :id))
                chat)))
           chats))
         (compl-collection (-map #'car choices))
         (choice
          (funcall telega-completing-read-function
                   prompt
                   (lambda (string pred action)
                     (if (eq action 'metadata)
                         `(metadata (display-sort-function . identity))
                       (complete-with-action
                        action compl-collection string pred)))
                   nil
                   nil ; require-match = nil
                   )))
    (cond
     ((string-match-p
       (my-regexp-or
        '("^[0-9]+$" ; ID
          ))
       choice)
      (-->
       choice
       (string-to-number it)
       (or (my-telega-get-user it) (my-telega-get-chat it))))
     ((string-match-p
       (my-regexp-or
        '("^@[_a-zA-Z0-9]+$" ; https://core.telegram.org/method/account.checkUsername
          ))
       choice)
      (or (my-telega-get-user choice) (my-telega-get-chat choice)))
     (:else
      (cdr (assoc choice choices))))))

;;;###autoload
(defun my-telega-describe-chat-or-user (char-or-user)
  (interactive (list (my-telega-completing-read-chat "Open chat with: ")))
  (condition-case _
      (telega-describe-chat char-or-user)
    (error (telega-describe-user char-or-user))))

;;;###autoload
(defun my-telega-installed-tdlib-version ()
  (-let* ((file
           (file-name-concat telega-server-libs-prefix
                             "lib/cmake/Td/TdConfigVersion.cmake")))
    (-some-->
        (s-match
         "^set(PACKAGE_VERSION \"\\(.*?\\)\")$"
         (f-read-text file 'utf-8))
      (nth 1 it))))

;;;###autoload
(defun my-telega-find-revision-still-satisfy-installed ()
  (-let* ((installed-version (my-telega-installed-tdlib-version))
          (src-dir (file-name-concat my-package-user-dir/ "telega/"))
          (default-branch "master")
          (default-file "telega.el"))
    (dlet ((default-directory src-dir))
      (-let*
          ((revision-match-p
            (lambda (rev)
              (-let*
                  ((lines
                    (process-lines "git"
                                   "show"
                                   (format "%s:%s" rev default-file)))
                   (min-version
                    (-some
                     (lambda (line)
                       (-some-->
                           (s-match
                            "^(defconst telega-tdlib-min-version \"\\([0-9]+\\(\\.[0-9]+\\)*\\)\")$"
                            line)
                         (nth 1 it)))
                     lines)))
                (version<= min-version installed-version)))))
        (cond
         ((funcall revision-match-p default-branch)
          default-branch)
         (:else
          (-let*
              ((revisions
                ;; the parent of the commit that changes `telega-tdlib-min-version'
                (-->
                 (process-lines "git"
                                "log"
                                default-branch
                                "--pretty=format:%P"
                                "--"
                                "telega.el")
                 (-mapcat
                  (lambda (line) (split-string line nil t)) it)))
               (rev-target (-find revision-match-p revisions)))
            rev-target)))))))

;;;###autoload
(defun my-telega-auto-downgrade-to-match-installed-tdlib ()
  (interactive)
  (-when-let* ((rev
                (my-telega-find-revision-still-satisfy-installed)))
    (my-package-vc-checkout-revision-and-rebuild 'telega rev)))

;;;###autoload
(defun my-telega-force-tdlib-min-version
    (&optional when-set-callback when-not-set-callback quiet)
  (interactive)
  (-let* ((installed-version-nums (version-to-list my-telega-tdlib-version))
          (required-version-nums (version-to-list telega-tdlib-min-version)))
    (cond
     ((and
       ;; same major and minor version
       (equal
        (-slice installed-version-nums 0 -1)
        (-slice required-version-nums 0 -1))
       ;; only allow one micro version difference
       ;; (<= (- (-last-item required-version-nums)
       ;;        (-last-item installed-version-nums))
       ;;     1)
       )
      (unless quiet
        (notifications-notify
         :body
         (format "`telega': outdated TDLib (%s < %s)"
                 my-telega-tdlib-version
                 telega-tdlib-min-version)))
      (setq telega-tdlib-min-version my-telega-tdlib-version)
      (when when-set-callback
        (funcall when-set-callback)))
     (:else
      (when when-not-set-callback
        (funcall when-not-set-callback))))))

;;;###autoload
(defun my-open-telega-directory ()
  (interactive)
  (my-xdg-open-file-with-default-application telega-directory))

;;;###autoload
(defun my-telega--on-updateOption--force-tdlib-min-version-a (fn &rest args)
  (condition-case err
      (apply fn args)
    (error
     (cond
      ((and (null my-telega-tdlib-version)
            (null my-telega-tdlib-min-version-original))
       (setq my-telega-tdlib-min-version-original telega-tdlib-min-version)
       (setq my-telega-tdlib-version (plist-get telega--options :version))
       (my-telega-force-tdlib-min-version
        (lambda () (apply fn args)) ;
        (lambda () (signal (car err) (cdr err))))
       ;;
       )
      (:else
       (signal (car err) (cdr err)))))))

(provide 'my-functions-telega)

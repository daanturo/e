;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-kmi-filter-point-in-string-or-comment (arg)
  (and (my-point-in-string-or-comment-p nil nil) arg))

;;;###autoload
(defun my-kmi-filter-point-in-string-or-comment-strict (arg)
  (and (my-point-in-string-or-comment-p nil nil :face t) arg))

;;;###autoload
(defun my-kmi-filter-indent-shift (cmd)
  (and (use-region-p) (not buffer-read-only) (not (minibufferp)) cmd))

;;;###autoload
(defun my-kmi-filter-not-read-only (cmd)
  (and (not buffer-read-only) cmd))

;;;###autoload
(defun my-kmi-filter-minibuffer-file-category (arg)
  (and (member (my-minibuffer-completion-category) '(file project-file)) arg))

;;;###autoload
(cl-defun my-kmi-menu-item-by-pressed-keys (regexp-cmd-alist)
  (-let* ((dft (cdr (assoc "" regexp-cmd-alist))))
    `(menu-item
      "" ,(and (symbolp dft) dft)
      :filter
      ,(lambda (_)
         (-let* ((pressed-key-seq
                  (--> (this-command-keys-vector) (key-description it))))
           (save-match-data
             (-some-->
                 (-find
                  (-lambda ((regexp . _cmd))
                    (cond
                     ((functionp regexp)
                      (funcall regexp pressed-key-seq))
                     (:else
                      (string-match regexp pressed-key-seq))))
                  regexp-cmd-alist)
               (cdr it))))))))

;;; my-functions-key-menu-item.el ends here

(provide 'my-functions-key-menu-item)

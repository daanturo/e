;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)


;;; Public

;;;###autoload
(defun my-goto-outline-heading ()
  "Jump to a heading with completing read support.

Compared to `consult-outline'/`consult--read', `ivy-read' has
direct support for `:preselect' while maintaining order.

Compared to `counsel-outline', this presents the original
headings' colors.

The line number is directly used as prefix for completion
candidates to simplify the case of multiple headings with a same
name."
  (interactive)
  (let ((lst (my-get-outline-line-heading-list))
        (l0 (line-number-at-pos nil 'absolute)))
    (ivy-read "Go to heading: "
              (my-goto-outline--line-heading-list->candidates lst)
              ;; `ivy' has direct support for this
              :preselect (-find-last-index (-lambda ((line heading)) (<= line l0)) lst)
              :action #'my-goto-outline--heading-jump
              :caller #'my-goto-outline-heading)))

;;;###autoload
(defun my-get-outline-line-heading-list ()
  "Return a list of (headings' line numbers and themselves)."
  (let ((re (format "^\\(?:%s\\)" outline-regexp)))
    (save-match-data
      (save-excursion
        (goto-char (point-max))
        (my-with-deferred-gc
         (--> (my-loop [retval nil]
                (if (search-backward-regexp re nil ':noerror)
                    (progn
                      (font-lock-ensure (line-beginning-position) (line-end-position))
                      (-let* ((match (match-string 0))
                              (line (string-trim-right (thing-at-point 'line))))
                        (recur (cons (list (line-number-at-pos nil 'absolute)
                                           (if (my-by-length< line match)
                                               match
                                             line))
                                     retval))))
                  retval))
              (my-goto-outline--align-newlines it)))))))

;;; Internal

(defun my-goto-outline--align-newlines (lst)
  (if (seq-empty-p lst)
      '()
    (-let* ((pre-newlines (my-for [(idx heading) lst]
                            (car (split-string heading "\n"))))
            (max-len (-max (-map #'length pre-newlines))))
      (my-for [(idx heading) lst]
        (list idx
              (-if-let* ((pos (string-search "\n" heading))
                         (replacement (s-repeat (- max-len pos -4) " ")))
                  (progn (put-text-property pos (1+ pos)
                                            'display replacement
                                            heading)
                         heading)
                heading))))))

(defun my-goto-outline--heading-jump (cand)
  (let ((l (my-goto-outline--candidate->line-number cand)))
    (with-no-warnings (goto-line l))
    (my-recenter-top-to-fraction 0.25)))

(defun my-goto-outline--line-heading-list->candidates (lst)
  (let ((line-digit-num (-some--> lst -last-item car number-to-string length)))
    (my-for [(l h) lst]
      (my-goto-outline--line-heading->candidate l h line-digit-num))))

(defun my-goto-outline--line-heading->candidate (line heading line-digit-num)
  (concat (--> (number-to-string line)
               (s-pad-left line-digit-num " " it)
               (propertize it 'face 'line-number))
          " "
          (my-stylize-string heading :height (face-attribute 'default :height))))

(defun my-goto-outline--candidate->line-number (cand)
  (--> (split-string cand " " t) car string-to-number))

(provide 'my-goto-outline)

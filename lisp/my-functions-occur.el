;; -*- lexical-binding: t; -*-

;;;###autoload
(defun my-get-occur-buffer (regexp nlines region src-buf)
  "Apply `occur' on REGEXP NLINES REGION using SRC-BUF as the source.
Don't change displayed windows, return the *Occur* buffer name."
  (save-window-excursion
    (let* ((new-bufs
            (car (my-get-buffer-differences-after
                  (with-current-buffer src-buf
                    (occur regexp nlines region)))))
           (occur-bufs (seq-filter
                        (-partial #'string-match-p "occur")
                        (seq-map #'buffer-name new-bufs))))
      (if (cdr occur-bufs)
          (user-error "Multiple Occur buffers appeared")
        (car occur-bufs)))))

;;;###autoload
(cl-defun my-list|hide-not-matching|unmatched-lines|reversed-occur
    (regexp &optional (src-buf (current-buffer)) region-beg region-end)
  "List lines in SRC-BUF which doesn't match REGEXP."
  (interactive (list (read-regexp
                      (format "%s" 'my-list|hide-not-matching|unmatched-lines|reversed-occur))
                     (current-buffer)
                     (if (use-region-p) (region-beginning) (point-min))
                     (if (use-region-p) (region-end) (point-max))))
  (my-with-deferred-gc
   (let* ((buffer-name-to-insert
           (buffer-name src-buf))
          (output-buffer
           (generate-new-buffer
            (format "*%s %s %s*"
                    #'my-list|hide-not-matching|unmatched-lines|reversed-occur
                    regexp
                    (replace-regexp-in-string
                     (regexp-quote (symbol-name #'my-list|hide-not-matching|unmatched-lines|reversed-occur))
                     "" buffer-name-to-insert)))))
     (with-current-buffer output-buffer
       (apply #'insert-buffer-substring
              buffer-name-to-insert
              (list region-beg region-end))
       ;; (cl-letf (((symbol-function #'occur-after-change-function) #'ignore)) (flush-lines regexp (point-min) (point-max)))
       (dlet ((inhibit-read-only t))
         (remove-text-properties (point-min) (point-max) '(read-only t)))
       (flush-lines regexp (point-min) (point-max)))
     (when (called-interactively-p 'any)
       (pop-to-buffer output-buffer))
     output-buffer)))

;;;###autoload
(defun my-occur-execute-in-original-buffer ()
  (interactive)
  (let ((buf (current-buffer))
        (wind (selected-window)))
    (occur-mode-goto-occurrence)
    (call-interactively
     (key-binding (read-key-sequence "Key sequence: ")))
    (select-window wind)
    (unless (equal buf (current-buffer))
      (switch-to-buffer buf))))

(provide 'my-functions-occur)

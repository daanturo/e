;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'eieio)

(autoload #'shell-mode "shell")

;;;###autoload
(progn
  (defun my-set-process-callback (process callback)
    "(`set-process-sentinel' PROCESS CALLBACK).
CALLBACK is ensured to run exactly one, even when PROCESS
finishes before setting it."
    (-let* ((evaluated-flag
             (-let* ((sym (gensym "my-set-process-callback")))
               (set sym nil)
               sym))
            (sentinel-to-set
             (lambda (process event)
               (unless (symbol-value evaluated-flag)
                 (set evaluated-flag t)
                 (funcall callback process event)))))
      (prog1 (set-process-sentinel process sentinel-to-set)
        (unless (process-live-p process)
          (funcall sentinel-to-set process ""))))))


;;;###autoload
(defun my-copy-buffer-process-pid (&optional buf)
  (interactive)
  (-let* ((pid (process-id (get-buffer-process (or buf (current-buffer))))))
    (kill-new (format "%s" pid))
    (message "%s" pid)
    pid))

;;;###autoload
(defun my-send-EOF-to-process-unless-has-children (proc)
  (unless (condition-case _
              (process-running-child-p proc)
            (error))
    (process-send-eof proc)))

;;;###autoload
(defun my-process-sync-1 (separate-stderr cmdargs)
  (with-temp-buffer
    (-let* ((err-file
             (and separate-stderr (make-temp-file "my-process-sync-run err")))
            (exit-code
             (apply #'call-process
                    (car cmdargs) nil
                    (if separate-stderr
                        `(t ,err-file)
                      t)
                    nil (cdr cmdargs))))
      (list
       :exit exit-code
       :out (buffer-string)
       :err
       (and separate-stderr
            (with-temp-buffer
              (insert-file-contents err-file)
              (buffer-string)))))))

;;;###autoload
(defun my-process-sync-run (cmdargs &optional mix-stderr)
  "Synchronously run CMDARGS and return a plist.
Plist: (:exit exit-code :out stdout :err stderr). MIX-STDERR mix stderr
with stdout."
  (my-process-sync-1 (not mix-stderr) cmdargs))

;;;###autoload
(defun my-process-sync-success-stdout (cmdargs)
  "Return right-trimmed stdout iff the process exits successfully.
Success: exit status = 0. Like (`my-process-sync-run' CMDARGS)'s stdout
whose final newline is trimmed."
  ;; ((file-missing file-error) "Searching for program" "No such file or directory" "foo")
  (condition-case _err
      (-let* (((&plist :out :exit) (my-process-sync-run cmdargs)))
        (and (= 0 exit) (string-remove-suffix "\n" out)))
    ((file-missing file-error) nil)))

;; this is problematic, sometimes we get `cl-no-applicable-method' error when
;; constructing

;; (defclass
;;   my-process-output ()
;;   ((exit :initarg :exit :type integer)
;;    (out :initarg :out :type string)
;;    (err :initarg :err :type (or string null))))

(defvar my-process-shell-command->arg-list-fn (lambda (cmd) (list "bash" "-c" cmd)))

;;;###autoload
(cl-defun my-process-async (cmdarg/s
                            &optional callback &key trim (separate-stderr t) out-buf err-buf query-exit)
  "Asynchronous run CMDARG/S with CALLBACK.
CMDARG/S can be a of strings, or a single string to be ran by Bash.
CALLBACK arguments: (stdout (plist :exit :out :err ) sentinel-args &rest
_). SEPARATE-STDERR: separate stderr and stdout (default). OUT-BUF:
stdout buffer, ERR-BUF: stderr buffer. TRIM: trim a regexp or a trailing
newline from stdout for CALLBACK. QUERY-EXIT: enable
`process-query-on-exit-flag'."
  (-let* ((stdout-buffer
           (or out-buf (generate-new-buffer " my-process-async out-buf" t)))
          (stderr-buffer
           (or err-buf
               (and separate-stderr
                    (generate-new-buffer " my-process-async err-buf" t))))
          (buf-str-fn
           (lambda () (buffer-substring-no-properties (point-min) (point-max))))
          (sentinel
           (lambda (proc event)
             (-let* ((output-obj
                      (list
                       :out
                       (with-current-buffer stdout-buffer
                         (-->
                          (funcall buf-str-fn)
                          (cond
                           ((stringp trim)
                            (string-trim-right it trim))
                           (trim
                            (string-trim-right it "\n"))
                           (:else
                            it))))
                       :exit (process-exit-status proc)
                       :err
                       (and stderr-buffer
                            (with-current-buffer stderr-buffer
                              (funcall buf-str-fn))))))
               (when callback
                 (funcall callback
                          (plist-get output-obj :out)
                          output-obj
                          (list proc event)))
               ;; don't kill the passed buffers
               (when (not out-buf)
                 (kill-buffer stdout-buffer))
               (when (and (not err-buf) stderr-buffer)
                 (kill-buffer stderr-buffer)))))
          (cmdargs
           (cond
            ((stringp cmdarg/s)
             (funcall my-process-shell-command->arg-list-fn cmdarg/s))
            (:else
             cmdarg/s)))
          (process
           (make-process
            :name (format "my-process-async %S" cmdargs)
            :command cmdargs
            :buffer stdout-buffer
            :stderr stderr-buffer
            :sentinel sentinel
            :noquery (not query-exit))))
    process))

;;;###autoload
(cl-defun my-process-shell-async (cmdarg/s &optional callback &rest args &key buffer)
  "Asynchronously run shell CMDARG/S with output to BUFFER.
If CMDARG/S is a string, it's ran using shell features. CALLBACK
is ran after the process finishes, (see `set-process-sentinel')
if IN-OUTBUF. Output is displayed in BUFFER, or if t:
automatically create. ARGS is the rest plist.

Intended to be an alternative to `my-process-async' that looks more
\"interactive\"."
  (declare (indent defun))
  (-let* ((name (format " *%s %S*" #'my-process-shell-async cmdarg/s))
          (outbuf
           (or buffer
               (and (not (member :buffer args)) (generate-new-buffer name))))
          (callback*
           (-lambda (_stdout _out-obj (process-cb event-cb))
             (when callback
               (with-current-buffer outbuf
                 (funcall callback process-cb event-cb)))))
          (proc
           (dlet ((process-environment
                   `(,@(my-shell--environment)
                     ;; sometimes DISPLAY isn't present in
                     ;; `process-environment' but in frame's
                     ,(format "DISPLAY=%S" (getenv "DISPLAY")))))
             (my-process-async
              cmdarg/s
              callback*
              :separate-stderr nil
              :out-buf outbuf))))
    (when outbuf
      (with-current-buffer outbuf
        ;; what `shell-command' does for display
        (shell-command-save-pos-or-erase)
        (shell-mode)
        (set-process-filter proc #'comint-output-filter)))
    (save-selected-window
      (pop-to-buffer outbuf))
    proc))

;;;###autoload
(defun my-parent-process-id (pid)
  (-some-->
      (process-lines-ignore-status "ps" "-o" "ppid=" (format "%s" pid)) (car it)
      ;; (string-trim it)
      (string-to-number it)))

;;;###autoload
(defun my-new-session-process-external-terminal-emulator (&optional proc bury)
  (interactive (list nil t))

  (cl-assert (executable-find "reptyr"))

  (-let* ((proc (or proc (get-buffer-process (current-buffer))))
          (pid-str (my->str (process-id proc)))
          (bufname (buffer-name))
          (wd (selected-window)))

    ;; echo 0 | sudo tee -a /proc/sys/kernel/yama/ptrace_scope
    ;; (stop-process proc)

    (message "`my-new-session-process-external-terminal-emulator:': %S"
             (my-terminal-execute-in-external-emulator
              ;; "kill" "-CONT"
              "sudo" "reptyr" "-T" pid-str))

    ;; the elisp terminal emulator will hang while trying to send inputs to the
    ;; shell process, just bury the window to prevent that
    (when bury
      (letrec ((h-fn
                (lambda ()
                  (remove-hook 'pre-command-hook h-fn 'local)
                  (quit-window nil wd)
                  (when (y-or-n-p
                         (format
                          "Kill buffer %s? If leave running it may hangs Emacs"
                          bufname))
                    (my-kill-buffer-no-ask bufname)))))
        (add-hook 'pre-command-hook h-fn nil 'local)))

    ;; (kill-process proc)
    ))

;;;###autoload
(cl-defun my-process-batch-eval-in-async-emacs (elisp-form
                                                &optional
                                                callback
                                                &key
                                                pre-args
                                                (mid-args '("-Q" "--batch"))
                                                suf-args
                                                (mix-stderr t)
                                                buffer)
  "Evaluate ELISP-FORM asynchronously in another Emacs process.
See `my-process-async' for CALLBACK. PRE-ARGS, MID-ARGS, SUF-ARGS are
placed before program name, after it and before eval options, after eval
options, respectively. MIX-STDERR combines stdout and stderr, this is
enabled by default because `message' writes to stderr.
BUFFER: buffer for stdout."
  (-let* ((cmdargs
           `(,@pre-args
             ,invocation-name
             ,@mid-args
             "--eval"
             ,(format "%S" elisp-form)
             ,@suf-args)))
    (my-process-async
     cmdargs
     callback
     :trim t
     :separate-stderr (not mix-stderr)
     :out-buf buffer)))

;;;###autoload
(defun my-process-eval-in-new-session-emacs
    (elisp-form &optional suf-opts &key pre-opts mid-opts)
  "Evaluate ELISP-FORM in an detached Emacs instance.
SUF-OPTS, PRE-OPTS, MID-OPTS, see `my-process-batch-eval-in-async-emacs'."
  (my-process-batch-eval-in-async-emacs
   elisp-form
   nil
   :pre-args `(,@pre-opts "setsid")
   :mid-args mid-opts
   :suf-args suf-opts))

;;;###autoload
(defun my-process-terminate (process &optional remote)
  (interactive (list (get-buffer-process (current-buffer)) nil))
  (signal-process process 'SIGTERM remote))


;;; my-functions-process.el ends here

(provide 'my-functions-process)

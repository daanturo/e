;; -*- lexical-binding: t; -*-


;; alter between `multiple-cursors' & `evil-mc' when switching `evil' states? --
;; rejected, for now we only use 1 implementation, so adaptation to an official
;; one in the future should be more lenient

(require 'multiple-cursors-core)

(require '00-my-core-macros)

;; (my-backtrace)

(advice-add #'mc/keyboard-quit :before #'my-mc-save-cursor-when-not-region-and-not-prefix-args-before-a)

(add-hook 'minions-prominent-modes 'my-mc-indicate-paused-cursors-mode)

(add-hook 'multiple-cursors-mode-enabled-hook #'my-mc-resume-paused-cursors-when-indicated)

;; evil's non-insert states don't play well with `multiple-cursors', we
;; shouldn't set `evil-state' where `evil-normal-state' is easily switched to by
;; accident

(my-bind :map 'my-mc-indicate-paused-cursors-mode-map :vi '(normal motion)
  '("RET" "<return>") #'my-mc-toggle-cursor-at-point)

;; It's confusing to display previously paused cursors when active
;; (my-bind 'mc/keymap [remap my-mc-indicate-paused-cursors-mode] #'ignore)

(my-bind [remap +multiple-cursors/evil-mc-toggle-cursor-here] #'my-mc-toggle-cursor-at-point)
(my-bind [remap +multiple-cursors/evil-mc-toggle-cursors] #'my-mc-toggle-pausing-cursors)
(my-bind [remap evil-mc-undo-all-cursors] #'my-mc-indicate-paused-cursors-mode)
(my-bind :map 'mc/keymap '("<return>" "<emacs-state> <escape>" "<normal-state> <escape>") nil)

(my-bind :map 'mc/keymap (kbd "M-n") #'mc/cycle-forward)
(my-bind :map 'mc/keymap (kbd "M-p") #'mc/cycle-backward)
;; Cursors cycling is used by the above bindings, free "C-v" for pasting
(when (bound-and-true-p cua-mode)
  (my-bind :map 'mc/keymap (kbd "C-v") nil))

(my-add-hook-once 'multiple-cursors-mode-hook
  (lambda (&rest _)
    (when (fboundp 'iedit-mode)
      (my-add-advice/s
        #'(corfu-insert corfu-complete)
        :around #'my-corfu-completions-for-mc--a)
      (my-add-advice/s
        #'(consult-completion-in-region)
        :around #'my-completion-by-minibuffer-for-mc-a))))

(add-hook 'multiple-cursors-mode-hook 'my-toggle-corfu-tick-advice-mc-compat-h)

(add-hook 'multiple-cursors-mode-enabled-hook #'my-mc-evil-compat-switch-state-h)

(add-hook 'mc/unsupported-minor-modes 'devil-mode)
(add-hook 'mc/unsupported-minor-modes 'parinfer-rust-mode)

;; watch out for mutating `esc-map'

;; [remap keyboard-escape-quit]
;; (my-bind :map 'mc/keymap :vi 'insert "ESC ESC ESC" #'mc/keyboard-quit)

;; [remap evil-normal-state]
(my-bind :map 'mc/keymap :vi 'insert '("<escape>" "ESC") esc-map)

;;;###autoload
(progn
  (defun my-setup-multiple-cursors ()
    (with-eval-after-load 'multiple-cursors-core
      (require 'multiple-cursors))
    (my-after-each '(emacs multiple-cursors-core)
      ;; Reset from Doom's .local to allow version-controlling, once for non-Doom and once for Doom
      (setq mc/list-file (file-name-concat my-emacs-conf-dir/ "etc" "multiple-cursors-lists.el")))
    (my-bind "C-<" #'mc/mark-previous-like-this)
    (my-bind "C->" #'mc/mark-next-like-this)
    (my-bind "C-M-<" #'mc/unmark-next-like-this)
    (my-bind "C-M->" #'mc/unmark-previous-like-this)
    (my-bind "C-S-<down>" #'mc/mark-next-like-this)
    (my-bind "C-S-<up>" #'mc/mark-previous-like-this)
    (my-bind "C-c C-S-c" #'my-mc-edit-lines-dwim)
    (my-bind "M-<mouse-1>" #'mc/add-cursor-on-click)
    (my-bind "M-g r t" #'my-mc-toggle-pausing-cursors)
    (global-unset-key (kbd "M-<down-mouse-1>"))))

(provide 'my-after-multiple-cursors)

;; Local Variables:
;; no-byte-compile: t
;; End:

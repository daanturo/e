;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'face-remap)

(defvar my-face-font-preview-sentence "The quick brown fox jumps over the lazy dog Il|1234567890Oo.")

;;;###autoload
(defun my-read-font-family-name (&optional prompt)
  (-let* ((coll (-uniq (font-family-list)))
          ;; (alst (-map (lambda (font) (cons font (propertize
          ;; my-face-font-preview-sentence 'face `(:family ,font)))) coll))
          (annot-fn
           (lambda (cand)
             (concat
              "\t\t"
              "\""
              (propertize my-face-font-preview-sentence 'face `(:family ,cand))
              "\""))))
    (completing-read
     (or prompt "Font: ")
     (lambda (string pred action)
       (if (eq action 'metadata)
           `(metadata (annotation-function . ,annot-fn) ; (category . face)
             )
         (complete-with-action action coll string pred))))))

;;;###autoload
(defun my-set-default-face-font-family ()
  (declare (interactive-only t))
  (interactive)
  (-let* ((name
           (my-read-font-family-name
            (format "`%s': " #'my-set-default-face-font-family))))
    (set-face-attribute 'default nil :family name)))

;;;###autoload
(defun my-get-system-default-variable-width-font ()
  (-->
   (my-process-sync->output-string
    '("gsettings" "get" "org.gnome.desktop.interface" "font-name"))
   (s-match "^'?\\([^,]+\\).*'?$" it) (elt it 1)))

;;;###autoload
(defun my-natural-read-mode (&optional font-family)
  (-let* ((font-family (or font-family (my-get-system-default-variable-width-font))))
    (buffer-face-set (list :family font-family))
    (when (= 0 text-scale-mode-amount)
      (text-scale-set 4))
    (visual-line-mode)))

;;;###autoload
(defun my-toggle-natural-read-mode (&optional choose-font)
  (interactive "P")
  (if buffer-face-mode
      (progn
        (buffer-face-mode 0)
        (text-scale-set 0))
    (my-natural-read-mode (and choose-font (my-read-font-family-name)))))

;;; my-functions-face-font.el ends here

(provide 'my-functions-face-font)

;; -*- lexical-binding: t; -*-

;;;###autoload
(defun my-code-cells-python-mode ()
  (interactive)
  (python-mode)
  (code-cells-mode))

;;;###autoload
(defun my-code-cells-eval-by-jupyter ()
  (interactive)
  (save-selected-window (my-jupyter-ensure-repl-buffer))
  (call-interactively #'code-cells-eval))

;;;###autoload
(defun my-code-cells-eval-and-next-cell ()
  (interactive)
  (call-interactively #'code-cells-eval)
  (code-cells-forward-cell))

;;;###autoload
(defun my-code-cells-run||eval-all ()
  (interactive)
  (let ((p (point)))
    (code-cells-eval-above most-positive-fixnum)
    (goto-char p)))

;;;###autoload
(defun my-code-cells-goto-beg-of-cell ()
  (interactive)
  (unless (looking-at-p (code-cells-boundary-regexp))
    (code-cells-backward-cell)))

;;;###autoload
(defun my-code-cells-goto-end-of-cell ()
  (interactive)
  (goto-char (1- (cadr (code-cells--bounds)))))

;;;###autoload
(defun my-code-cells-mark-cell-without-header (&optional arg)
  (interactive "P")
  (goto-char (1- (cadr (code-cells--bounds))))
  (push-mark nil t t)
  (my-code-cells-goto-beg-of-cell)
  (forward-line))

;;;###autoload
(defun my-code-cells-markdown-insert-markdown-cell ()
  (interactive)
  (insert  "<!-- #region -->" "\n")
  (save-excursion
    (insert "\n" "<!-- #endregion -->")))

(defvar-local my-code-cells-language nil)
;;;###autoload
(defun my-code-cells-markdown-insert-code-cell ()
  (interactive)
  (unless my-code-cells-language
    (setq my-code-cells-language (my-jupyter-get-nodebook-language buffer-file-name)))
  (insert (format "```%s" my-code-cells-language) "\n")
  (save-excursion
    (insert "\n" "```")))

;;;###autoload
(defun my-code-cells-markdown-insert-cell (&optional not-code)
  (interactive "P")
  (if not-code
      (my-code-cells-markdown-insert-markdown-cell)
    (my-code-cells-markdown-insert-code-cell)))

;;;###autoload
(defun my-code-cells-use-markdown ()
  (interactive)
  (setq code-cells-convert-ipynb-style
        `(("jupytext" "--to" "ipynb" "--from" "markdown")
          ("jupytext" "--to" "markdown" "--from" "ipynb")
          markdown-mode
          code-cells-convert-ipynb-hook))
  (unless (buffer-modified-p)
    (revert-buffer)))

;;;###autoload
(defun my-code-cells-use-programming-script ()
  (interactive)
  (setq code-cells-convert-ipynb-style
        '(("jupytext" "--to" "ipynb")
          ("jupytext" "--to" "auto:percent")
          nil
          code-cells-convert-ipynb-hook))
  (unless (buffer-modified-p)
    (revert-buffer)))

(provide 'my-functions-code-cells)

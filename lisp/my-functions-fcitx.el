;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'fcitx)

;;;###autoload
(defun my-fcitx-dbus-command (method)
  "Not compatible with fcitx5."
  (dbus-call-method
   :session
   "org.fcitx.Fcitx5"
   "/controller"
   "org.fcitx.Fcitx.Controller1"
   method))

;;;###autoload
(defun my-fcitx-IM-active-p ()
  ;; fcitx state, 0 for close, 1 for inactive, 2 for active
  (if fcitx-use-dbus
      (= 2 (my-fcitx-dbus-command "State"))
    (string= "2\n" (shell-command-to-string fcitx-remote-command))))

;;;###autoload
(defun my-fcitx-deactivate-IM ()
  (if fcitx-use-dbus
      (my-fcitx-dbus-command "Deactivate")
    (call-process fcitx-remote-command nil nil nil "-c")))

;;;###autoload
(defun my-fcitx--activate-IM ()
  (if fcitx-use-dbus
      (my-fcitx-dbus-command "Activate")
    (call-process fcitx-remote-command nil nil nil "-o")))

;;;###autoload
(defun my-fcitx-activate-IM ()
  ;; (my-with-deferred-gc (while (not (my-fcitx-IM-active-p)) (my-fcitx--activate-IM) (sleep-for (/ 1.0 (expt 2 10)))))
  (my-fcitx--activate-IM)
  ;; Activating immediately sometimes doesn't do anything
  (letrec ((timer
            (run-with-timer
             0
             ;; repeat delay: too much -> noticeable late activation, too little
             ;; -> too much computation
             (/ 1.0 (expt 2 4))
             (lambda ()
               (if (my-fcitx-IM-active-p)
                   (cancel-timer timer)
                 (my-fcitx--activate-IM))))))))

;;;###autoload
(defun my-fcitx-activate-IM&auto-mode ()
  (interactive)
  (when (not (bound-and-true-p pgtk-use-im-context-on-new-connection))
    (pgtk-use-im-context t)
    (my-fcitx-activate-IM)
    (my-fcitx-auto-de?activate-mode)))

;;;###autoload
(defun my-fcitx-deactivate-IM&auto-mode ()
  (interactive)
  (my-fcitx-auto-de?activate-mode 0)
  (my-fcitx-deactivate-IM)
  (pgtk-use-im-context nil))

;;;###autoload
(defun my-fcitx-toggle ()
  (interactive)
  (if (my-fcitx-IM-active-p)
      (my-fcitx-deactivate-IM&auto-mode)
    (my-fcitx-activate-IM&auto-mode)))

;;;###autoload
(defun my-fcitx-suitable-evil-state-p ()
  "Whether current state is a state which is reasonable for
enabling input methods, as switching to another state shouldn't
be counted."
  (if (bound-and-true-p evil-mode)
      (member evil-state fcitx-active-evil-states)
    t))

;;;###autoload
(defun my-fcitx-should-deactivate-IM&auto-mode-p ()
  (when (and
         ;; Fcitx is not active
         (not (my-fcitx-IM-active-p))
         ;; Current command is not a sequence of multiple keys, since prefix keys are ignored by `fcitx.el'
         (length= (this-single-command-keys) 1)
         (my-fcitx-suitable-evil-state-p))
    t))

;;;###autoload
(defun my-fcitx--deactivate-IM&auto-mode-maybe (&rest _)
  "Deactivate fcitx when various conditions are met."
  (cond ((my-fcitx-should-deactivate-IM&auto-mode-p)
         ;; `handle-switch-frame'?
         (my-fcitx-deactivate-IM&auto-mode))))

;;;###autoload
(defun my-fcitx--activate-IM-when-insert-h ()
  "Deactivate fcitx when various conditions are met."
  (when (and (my-fcitx-suitable-evil-state-p)
             my-fcitx-auto-de?activate-mode)
    (my-fcitx-activate-IM)))

;; NOTE edge case: exactly 1 buffer is already active, then activate in another
;; buffer, how to activate `my-fcitx-auto-de?activate-mode' in the latter one?
;; Currently `my-fcitx-auto-de?activate-mode' is only enabled explicitly so it
;; can't use a hook to activate itself (?!?). IDEA: when a buffer is
;; fcitx-active, globally add pre/post-command-hook to record the new fcitx
;; status

;;;###autoload
(define-minor-mode my-fcitx-auto-de?activate-mode
  "May automatically deactivate fcitx when it's not active to use
other keys caught by input methods."
  :global nil
  (if my-fcitx-auto-de?activate-mode
      (progn
        ;; Prevent some bugs
        (my-fcitx-turn-off||disable-auto-de?activate-mode-in-other-buffers)
        ;; Re-activate fcitx
        (add-hook 'evil-insert-state-entry-hook #'my-fcitx--activate-IM-when-insert-h nil 'local)
        ;; Automatically deactivate my-fcitx mode when it's not active
        (add-hook 'pre-command-hook #'my-fcitx--deactivate-IM&auto-mode-maybe nil 'local)
        ;; Per-buffer activation
        (add-hook 'window-selection-change-functions #'my-fcitx--toggle-by-buffer nil 'local)
        ;; When closing a frame who has an active buffer, deactivate before
        ;; switching to prevent the IM from activating in the landing buffer
        (add-hook 'delete-frame-functions #'my-fcitx--may-deactivate-IM&auto-mode-before-delete-frame nil 'local))
    (progn
      (remove-hook 'evil-insert-state-entry-hook #'my-fcitx--activate-IM-when-insert-h 'local)
      (remove-hook 'pre-command-hook #'my-fcitx--deactivate-IM&auto-mode-maybe 'local)
      (remove-hook 'window-selection-change-functions #'my-fcitx--toggle-by-buffer 'local)
      (remove-hook 'delete-frame-functions #'my-fcitx--may-deactivate-IM&auto-mode-before-delete-frame 'local))))

;;;###autoload
(defun my-fcitx--may-deactivate-IM&auto-mode-before-delete-frame (&rest _)
  (when (and my-fcitx-auto-de?activate-mode
             (my-fcitx-IM-active-p))
    (my-fcitx-deactivate-IM)))

;;;###autoload
(defun my-fcitx--toggle-by-buffer (&rest _)
  "When fcitx was activated in current buffer and not explicitly
deactivated, re-activate it."
  (cond ((not my-fcitx-auto-de?activate-mode)
         (my-fcitx-deactivate-IM))
        ((my-fcitx-suitable-evil-state-p)
         (my-fcitx-activate-IM))
        ;; After switching from a fcitx-active window, when the current buffer
        ;; is in normal-state and `my-fcitx-auto-de?activate-mode', fcitx is
        ;; activated when it shouldn't, we must temporarily disable it
        (t
         (my-fcitx-deactivate-IM))))

;;;###autoload
(defun my-fcitx-activated-buffer-list ()
  (--filter (with-current-buffer it my-fcitx-auto-de?activate-mode)
            (buffer-list)))

;;;###autoload
(defun my-fcitx-turn-off||disable-auto-de?activate-mode-in-other-buffers ()
  (dolist (buf (remove (current-buffer) (buffer-list)))
    (with-current-buffer buf
      (when my-fcitx-auto-de?activate-mode
        (my-fcitx-auto-de?activate-mode 0)))))

(provide 'my-after-fcitx)

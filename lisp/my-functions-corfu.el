;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(defun my-corfu-set-quit ()
  (interactive)
  (progn
    (setq corfu-quit-no-match t)
    (setq corfu-quit-at-boundary t)))

;;;###autoload
(defun my-corfu-complete-with-selected-or-first ()
  "`corfu-complete' but defaults to the first candidate unless selected."
  (interactive)
  (my-with-ensured-undo-boundary
    (when (< corfu--index 0)
      (corfu-next))
    (corfu-complete)))


;;;###autoload
(defun my-corfu-menu-item-filter-if-selected (cmd)
  (and (<= 0 corfu--index) cmd))

(provide 'my-functions-corfu)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;;###autoload
(progn
  (defun my-setup-minuet ()

    (my-leader2-bind "a m y" #'minuet-complete-with-minibuffer)
    (my-leader2-bind "a m i" #'minuet-show-suggestion)
    (my-leader2-bind "a m a" #'minuet-auto-suggestion-mode)

    (with-eval-after-load 'minuet
      (my-bind :map 'minuet-active-mode-map "M-p" #'minuet-previous-suggestion)
      (my-bind :map 'minuet-active-mode-map "M-n" #'minuet-next-suggestion)
      (my-bind :map 'minuet-active-mode-map "M-A" #'minuet-accept-suggestion)
      (my-bind :map 'minuet-active-mode-map "M-a" #'minuet-accept-suggestion-line)
      (my-bind :map 'minuet-active-mode-map "M-e" #'minuet-dismiss-suggestion)

      ;; Ollama
      (setq minuet-provider 'openai-fim-compatible)
      ;; (setq minuet-n-completions 1) ; recommended for Local LLM for resource saving
      (setq minuet-context-window 1024)
      (plist-put
       minuet-openai-fim-compatible-options
       :end-point "http://localhost:11434/v1/completions")
      ;; an arbitrary non-null environment variable as placeholder
      (plist-put minuet-openai-fim-compatible-options :name "Ollama")
      (plist-put minuet-openai-fim-compatible-options :api-key "TERM")

      (minuet-set-optional-options
       minuet-openai-fim-compatible-options
       :max_tokens 256))))

;;; my-after-minuet.el ends here

(provide 'my-after-minuet)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;; (my-add-list! 'completion-at-point-functions '(codeium-completion-at-point))

;;;###autoload
(autoload #'my-complete-with-codeium "after-codeium" nil t)

(defalias
  #'my-complete-with-codeium
  (cape-capf-interactive #'codeium-completion-at-point))

(provide 'my-after-codeium)

;; Local Variables:
;; no-byte-compile: t
;; End:

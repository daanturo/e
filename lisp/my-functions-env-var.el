;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)



;;;###autoload
(progn
  (defvar my-env-file nil))

(defvar my-env--saved-var-table nil)

;;;###autoload
(defun my-env-var-list-from-systemd (&optional refresh async-cb)
  (when refresh
    (setq my-env--saved-var-table nil))
  (-let* ((cmdargs '("systemctl" "--user" "show-environment"))
          (post-process
           (lambda (out &rest _)
             (-let* ((envs (my-env-parse out :systemd t)))
               (when async-cb
                 (funcall async-cb envs))
               (setq my-env--saved-var-table envs)
               envs))))
    (condition-case _
        (cond
         (my-env--saved-var-table
          (when async-cb
            (funcall async-cb my-env--saved-var-table))
          my-env--saved-var-table)
         (async-cb
          (my-process-async cmdargs post-process))
         (:else
          (funcall post-process (my-process-sync->output-string cmdargs))))
      ((file-missing file-error)
       (when async-cb
         (funcall async-cb nil))))))

;;;###autoload
(defun my-env-get-saved-var (variable)
  (alist-get variable (my-env-var-list-from-systemd) nil nil #'equal))

(defvar my-get-env-process-environment--table nil)

;;;###autoload
(defun my-get-env-process-environment (var &optional refresh)
  (when refresh
    (setq my-get-env-process-environment--table nil))
  (unless my-get-env-process-environment--table
    (setq my-get-env-process-environment--table
          (make-hash-table :test #'equal))
    (dolist (str process-environment)
      (-when-let* ((pos (string-search "=" str)))
        (puthash
         (substring str 0 pos)
         (substring str (+ 1 pos))
         my-get-env-process-environment--table))))
  (gethash var my-get-env-process-environment--table))

;;;###autoload
(defun my-env-ensure-set (variable &optional quiet)
  (when (--> (getenv variable) (or (null it) (string-empty-p it)))
    (-if-let* ((env (my-env-get-saved-var variable)))
        (progn
          (unless quiet
            (message "`my-env-ensure-set': %S isn't set, setting it to %S."
                     variable
                     env))
          (setenv variable env))
      (progn
        (unless quiet
          (message "`my-env-ensure-set': %S isn't found." variable))))))

;;;###autoload
(defun my-env-set-missing-from-saved (&optional async quiet interact)
  ;; (interactive (list nil nil t))
  (-let* ((set-fn
           (lambda (env-alist)
             (dolist (elem env-alist)
               (-let* (((var . val) elem))
                 ;; exclude frame-local `getenv's
                 (when (not
                        (equal
                         val (my-get-env-process-environment var interact)))
                   (unless quiet
                     (message "`my-env-set-missing-from-saved': set %s to %S."
                              var
                              "systemctl"
                              "--user"
                              "show-environment"
                              val))
                   (setenv var val))))
             (when (functionp async)
               (funcall async env-alist)))))
    (if async
        (my-env-var-list-from-systemd nil set-fn)
      (funcall set-fn (my-env-var-list-from-systemd nil)))))

;;;###autoload
(progn

  (cl-defun my-env-parse (src &key file systemd)
    "Parse environment variables to an alist from SRC.
SRC is a string that is typically the \"env\" program's default
output. FILE: SRC is a file so read from it instead. SYSTEMD: also
recognized systemd's \"systemctl show-environment --user\"'s $'' output."
    (-let* ((str
             (cond
              (file
               (my-read-text-file src))
              (:else
               src))))
      (save-match-data
        (-some-->
            str (split-string it "\n" t)
            (-map
             (lambda (line)
               (string-match "\\(.*?\\)=\\(.*\\)" line)
               (cons
                (match-string 1 line)
                (-let* ((val (match-string 2 line)))
                  (cond
                   ;; VAR=$'value'
                   ((and systemd (string-match "^\\$'\\(.*\\)'$" val))
                    (match-string 1 val))
                   (:else
                    val)))))
             it)))))

  (defun my-env-path-inherited? (&optional value)
    (-let* ((value (or value (getenv "PATH"))))
      (string-match-p
       (format "\\(^\\|:\\)%s/"
               (string-remove-suffix "/" (getenv "HOME")))
       value)))
  (defalias #'my-env-path-inherited-p #'my-env-path-inherited?)

  (defun my-env-set-from-file (file)
    (interactive (list my-env-file))
    (-let* ((envs (my-env-parse file :file t)))
      (save-match-data
        (dolist (env envs)
          (-let* (((var . val) env))
            (when (or
                   ;; variable not set
                   (null (getenv var))
                   ;; like PATH
                   (string-match ":[^/]*/" val))
              (setenv var val)))))))

  (defun my-env-set-exec-path ()
    (setq-default exec-path
                  (delete-dups
                   (eval (car (get 'exec-path 'standard-value)) t)))))

;;;###autoload
(defun my-env-generate-from-shell (&optional destination)
  (let* ((cmdargs
          `("env" "--ignore-environment" ,(format "HOME=%s" (getenv "HOME"))
            ;; more from systemd
            ,@(condition-case _err
                  (process-lines "systemctl" "--user" "show-environment")
                (error '()))
            ,shell-file-name
            ;; use login shell to ensure that interactive (login?) variables
            ;; are property initialized
            "-l" "-c"
            "printenv" ;; "-0"
            )))
    (with-temp-buffer
      ;; ignore current Emacs env vars
      (dlet ((process-environment '()))
        (apply #'call-process
               (car cmdargs)
               nil
               (or destination (current-buffer))
               (cdr cmdargs)))
      (buffer-string))))

;;;###autoload
(defun my-env-generate-env-file (&optional _async set-now)
  (interactive (list nil t))
  (let* ((str-env (my-env-generate-from-shell (list :file my-env-file))))
    (when set-now
      (my-env-set-from-file my-env-file)
      (message "`my-env-generate-env-file' %s" my-env-file))
    str-env))

;;;###autoload
(defun my-get-env-in-string-list (var string-list)
  (-let* ((prefix (concat var "=")))
    ;; "VAR=VAL"
    (-some
     (lambda (str)
       (and (string-search prefix str) (substring str (+ (length var) 1))))
     string-list)))

;;;###autoload
(defun my-get-frame-env-var (var)
  (-let* ((lst (-some--> (frame-parameter (selected-frame) 'environment))))
    (my-get-env-in-string-list var lst)))

;;;###autoload
(defun my-get-separated-PATH ()
  (--> (getenv "PATH")
       (split-string it ":")))

;;; my-functions-env-var.el ends here

(provide 'my-functions-env-var)

;; -*- lexical-binding: t; -*-

(require 'dash)

(require '00-my-core-macros)
(require 'multiple-cursors-core)

;; (my-backtrace)

(defvar my-mc-always-keep-prefix-list '())

(defun my-mc-remove-obsolete-commands-from-lists (&optional all)
  "With non-nil ALL, remove commands from other packages, too."
  (-let* ((always-keep?
           (lambda (name)
             (-some
              (lambda (prefix) (string-prefix-p prefix name))
              my-mc-always-keep-prefix-list)))
          (obsolete-commands
           (-filter
            (lambda (cmd)
              (-let* ((name (symbol-name cmd)))
                (and (not (commandp cmd))
                     (not (funcall always-keep? name))
                     (or all (string-match-p (format "^%s" 'my-) name)))))
            (append mc/cmds-to-run-once mc/cmds-to-run-for-all))))
    (dolist (cmd obsolete-commands)
      (my-delete-in-list! 'mc/cmds-to-run-once cmd)
      (my-delete-in-list! 'mc/cmds-to-run-for-all cmd))
    obsolete-commands))

;;;###autoload
(defun my-mc-move-command-between-run-for-all-and-once (from command to)
  "FROM & TO is in `mc/cmds-to-run-{once,for-all}'.
Useful when COMMAND is mistakenly placed in FROM as manual
editing procedure is tedious."
  (interactive
   (progn
     (require 'multiple-cursors-core)
     (let* ((lists '(mc/cmds-to-run-once mc/cmds-to-run-for-all))
            (from (intern (completing-read "Move command from: " lists)))
            (command (intern (completing-read "Command: " (symbol-value from))))
            (to (intern (completing-read (format "Move `%s' to: " command)
                                         (remove from lists)))))
       (list from command to))))
  (my-mc-remove-obsolete-commands-from-lists)
  (mc/save-lists)
  (add-to-list to command)
  (set from (remove command (symbol-value from)))
  (mc/save-lists))

(defvar my-mc-buffer-saved-cursor-alist nil
  "List of fake cursors which were created by `multiple-cursors'.
Keys are buffer objects. We can make it a buffer-local variable,
but it's easier to watch a global variable.")

;;;###autoload
(cl-defun my-mc-get-paused-cursor-list (&optional (buffer (current-buffer)))
  (alist-get buffer my-mc-buffer-saved-cursor-alist nil nil #'equal))

;;;###autoload
(cl-defun my-mc-set-paused-cursor-list (positions &optional (buffer (current-buffer)))
  (setf (alist-get buffer my-mc-buffer-saved-cursor-alist nil nil #'equal) positions))

;;;###autoload
(defun my-mc-fake-cursor-pos-list ()
  (-map #'overlay-start (mc/all-fake-cursors)))

;;;###autoload
(defun my-mc-cursor-begs-ordered-list ()
  (-let* ((lst '()))
    (mc/for-each-cursor-ordered
     (push (overlay-start cursor)
           lst))
    (nreverse lst)))

;;;###autoload
(defun my-mc--save-fake-cursors ()
  (when (mc/all-fake-cursors)
    (my-mc-set-paused-cursor-list (my-mc-fake-cursor-pos-list))))

(defvar my-mc-indicate-paused-cursors-mode-map (make-sparse-keymap))

;;;###autoload
(define-minor-mode my-mc-indicate-paused-cursors-mode
  "Show paused `multiple-cursors''s cursors."
  :keymap my-mc-indicate-paused-cursors-mode-map
  :lighter " Paused cursors"

  (let ((positions (my-mc-get-paused-cursor-list)))

    (when (and (bound-and-true-p multiple-cursors-mode)
               my-mc-indicate-paused-cursors-mode)
      (message "Please turn %s off to avoid confusion" 'my-mc-indicate-paused-cursors-mode))

    ;; `overlay' is slower than `text-properties' but doesn't modify the buffer

    (remove-overlays (point-min) (point-max) 'my-mc t)
    (if my-mc-indicate-paused-cursors-mode
        (dolist (pos positions)
          (let ((ol (make-overlay pos (1+ pos))))
            (overlay-put ol 'face 'mc/cursor-face)
            (overlay-put ol 'my-mc t)))
      nil)

    ;;
    ))

;;;###autoload
(defun my-mc-toggle-cursor-at-point ()
  "Toggle the cursor at point.

If `multiple-cursors-mode' is active, remove the cursor at point
and jump back the previous one (because the point is always a
real cursor).

Else if paused cursors are showing, toggle the cursor at point.

Else discard paused cursors and begin toggling new ones."
  (interactive)
  (if multiple-cursors-mode
      (let ((p (point)))
        (mc/cycle-backward)
        (mc/remove-fake-cursor (mc/fake-cursor-at-point p)))
    (let ((position-list
           (if my-mc-indicate-paused-cursors-mode
               (my-mc-get-paused-cursor-list)
             nil)))
      (setq position-list
            (if (member (point) position-list)
                (remove (point) position-list)
              (append position-list (list (point)))))
      (my-mc-set-paused-cursor-list position-list)
      (my-mc-indicate-paused-cursors-mode))))

;;;###autoload
(defun my-mc-pause-cursors ()
  "Freeze cursors."
  (interactive)
  (when multiple-cursors-mode
    (my-mc--save-fake-cursors)
    (my-mc-indicate-paused-cursors-mode)
    (mc/disable-multiple-cursors-mode)))

;;;###autoload
(defun my-mc-create-fake-cursor-at (position)
  (save-excursion
    (goto-char position)
    (mc/create-fake-cursor-at-point)))

;;;###autoload
(defun my-mc-resume-cursors ()
  "Thaw cursors."
  (interactive)
  (let ((cursor-list (my-mc-get-paused-cursor-list)))
    (mapc #'my-mc-create-fake-cursor-at cursor-list)
    (my-mc-indicate-paused-cursors-mode -1)
    (mc/maybe-multiple-cursors-mode)))

;;;###autoload
(defun my-mc-toggle-pausing-cursors ()
  "If `multiple-cursors-mode' is active, freeze fake cursors, else thaw them."
  (interactive)
  (if multiple-cursors-mode
      (my-mc-pause-cursors)
    (my-mc-resume-cursors)))

;; `multiple-cursors-mode-disabled-hook' is ran after removing fake cursors, so we advise this instead
;;;###autoload
(defun my-mc-save-cursor-when-not-region-and-not-prefix-args-before-a (&rest _)
  (unless
      (or
       ;;  When regions are active, `mc/keyboard-quit' unmark them instead
       (use-region-p)
       current-prefix-arg)
    (my-mc--save-fake-cursors)))

;;;###autoload
(defun my-mc-resume-paused-cursors-when-indicated (&rest _)
  (when my-mc-indicate-paused-cursors-mode
    (my-mc-resume-cursors))
  (my-mc-indicate-paused-cursors-mode -1))

;;;###autoload
(defun my-mc-goto-last-cursor ()
  (interactive)
  (when (< (point) (-max (my-mc-fake-cursor-pos-list)))
    (mc/cycle (mc/last-fake-cursor-before (point-max))
              nil
              "")))

;;;###autoload
(defun my-mc-goto-first-cursor ()
  (interactive)
  (when (> (point) (-min (my-mc-fake-cursor-pos-list)))
    (mc/cycle (mc/first-fake-cursor-after (point-min))
              nil
              "")))

;;;###autoload
(defun my-mc-sort-regions-or-sexps-by-length ()
  (interactive)
  (require 'mc-separate-operations)
  (unless (use-region-p)
    (mc/execute-command-for-all-cursors 'mark-sexp))
  (setq mc--strings-to-replace (sort (mc--ordered-region-strings) 'my-by-length<))
  (mc--replace-region-strings))

;;;###autoload
(defun my-mc-edit-lines-dwim ()
  "Like `mc/edit-lines', but operate on current paragraph by default."
  (interactive)
  (if (use-region-p)
      (mc/edit-lines)
    (progn
      (my-mark-inner-paragraph)
      (mc/edit-ends-of-lines)
      (mc/for-each-cursor-ordered
       (deactivate-mark)))))

;;;###autoload
(defun my-mc-mark-next-with-same-indent (&optional back)
  (interactive "P")
  (save-excursion
    (let ((found (my-find-next-position-with-same-indent-as-current-line-in-defun (point) back)))
      (if found
          (progn (my-mc-create-fake-cursor-at found)
                 (mc/maybe-multiple-cursors-mode))
        (progn
          (my-message #'my-mc-mark-next-with-same-indent ":"
                      "can't find a position!"))))))

;;;###autoload
(defun my-mc--buffer-substring-at-fake-cursor (position bwd fwd)
  (buffer-substring-no-properties
   (+ bwd position)
   (+ fwd position)))

;;;###autoload
(defun my-mc-get-matched-fake-cursor-start-list
    (bwd fwd real-cursor-content &optional fake-cursor-positions)
  (-filter
   (lambda (p)
     (string= real-cursor-content
              (my-mc--buffer-substring-at-fake-cursor p bwd fwd)))
   (or fake-cursor-positions
       (my-mc-fake-cursor-pos-list))))

;;;###autoload
(defun my-mc-around-of-fake-cursors-match-real-p
    (beg-pos end-pos &optional fake-cursor-positions)
  "Return whether the relative contents around fake cursors match
the real cursor's from BEG-POS to END-POS.
When FAKE-CURSOR-POSITIONS is non-nil, compute there."
  (let ((fake-cursor-positions (or fake-cursor-positions (my-mc-fake-cursor-pos-list))))
    (= (length fake-cursor-positions)
       (length
        (my-mc-get-matched-fake-cursor-start-list
         (- beg-pos (point))
         (- end-pos (point))
         (buffer-substring-no-properties beg-pos end-pos)
         fake-cursor-positions)))))

;;;###autoload
(defun my-mc-switch-to-iedit-by-region (beg end &optional fake-cursor-pos-lst)
  "For the real cursor and FAKE-CURSOR-POS-LST, activate `iedit'.
FAKE-CURSOR-POSITIONS defaults to to`multiple-cursors''s fake
cursors. With the content between BEG and END as the mandatory
match. For each of fake cursor, the content to be compared will
be taken relatively just like [BEG `point' END]."
  (interactive "r")
  (if (bound-and-true-p iedit-mode)
      (user-error "%s is already enabled." 'iedit-mode)
    (let* ((fake-pos-lst (or fake-cursor-pos-lst (my-mc-fake-cursor-pos-list)))
           (bwd (- beg (point)))
           (fwd (- end (point))))
      (if (my-mc-around-of-fake-cursors-match-real-p beg end fake-pos-lst)
          (progn
            ;; Disabling `multiple-cursors' first may erase `iedit''s overlays (?)
            (when (use-region-p)
              (deactivate-mark))
            (mc/disable-multiple-cursors-mode)
            (my-iedit-make-occurences-at-points
             bwd fwd (cons (point) fake-pos-lst)))
        (message
         "`my-mc-switch-to-iedit-by-region': There are unmatched cursors.")))))

;;;###autoload
(defun my-mc-switch-to-iedit-by-symbol (&optional fake-cursor-positions-input)
  "For the real cursor and `multiple-cursors''s fake cursors, activate `iedit'.
Use symbol at point to match."
  (interactive)
  (-let* (((beg . end) (bounds-of-thing-at-point 'symbol)))
    (my-mc-switch-to-iedit-by-region beg end fake-cursor-positions-input)))

;;;###autoload
(defun my-mc-switch-to-iedit ()
  (interactive)
  (if (use-region-p)
      (my-mc-switch-to-iedit-by-region (region-beginning) (region-end))
    (my-mc-switch-to-iedit-by-symbol)))


;; Just running `iedit-switch-to-mc-mode' to switch back may not work
;;;###autoload
(defun my-completion-by-minibuffer-for-mc-a (func &rest args)
  "Switch from `multiple-cursors-mode' to `iedit-mode' to propagate completions.
Condition: all fake cursors are visible in current window (most
likely just a small number for performance reasons) and all's
completion regions are matched with the real cursor."
  (if-let* ((_ multiple-cursors-mode)
            (poss (my-mc-fake-cursor-pos-list))
            (_ (-every #'pos-visible-in-window-p poss)))
      (progn
        (my-mc-switch-to-iedit-by-symbol poss)
        (prog1
            (apply func args)
          (run-at-time 0 nil #'iedit-switch-to-mc-mode)))
    (apply func args)))

;;;###autoload
(defun my-corfu-completions-for-mc--a (func &rest args)
  "Switch from `multiple-cursors-mode' to `iedit-mode' to propagate completions.
Condition: all fake cursors are visible in current window (most
likely just a small number for performance reasons) and all's
completion regions are matched with the real cursor."
  (if-let* ((_ multiple-cursors-mode)
            (poss (my-mc-fake-cursor-pos-list))
            (_ (-every #'pos-visible-in-window-p poss)))
      (progn
        (-let* (((beg end _table _pred . _) completion-in-region--data))
          (my-mc-switch-to-iedit-by-region beg end poss))
        (prog1 (apply func args)
          (run-at-time 0 nil #'iedit-switch-to-mc-mode)))
    (apply func args)))

;; (defun my-corfu-tick-mc-compat--a (func &rest args) (if multiple-cursors-mode (list (current-buffer)) (apply func args)))
;;;###autoload
(defun my-corfu-tick-mc-compat--a (func &rest args)
  (list (current-buffer)))

;;;###autoload
(defun my-toggle-corfu-tick-advice-mc-compat-h (&rest _)
  (funcall (if multiple-cursors-mode #'my-add-advice/s #'my-remove-advice/s)
           #'corfu--auto-tick :around #'my-corfu-tick-mc-compat--a))

(defvar my-mc-padded-number-format "%%0%dd"
  "(`format' (`format' <this> digit-count) num-to-insert).")

(defvar my-mc-padded-number-length-format "%d"
  "Default to decimal length.")

;;;###autoload
(defun my-mc-insert-padded-numbers (&optional beg)
  (interactive "P")
  (-let* ((num (or (and beg (prefix-numeric-value beg))
                   mc/insert-numbers-default))
          (digit-count (length (format my-mc-padded-number-length-format
                                       (mc/num-cursors))))
          (fmt (format my-mc-padded-number-format digit-count)))
    (mc/for-each-cursor-ordered
     (mc/execute-command-for-fake-cursor
      (lambda ()
        (interactive)
        (insert (format fmt num)))
      cursor)
     (setq num (+ 1 num)))))

;; inspired by `+multiple-cursors-compat-switch-to-emacs-state-h', but switching
;; back (being complex) doesn't look like needed?

;; (defvar-local my-mc-evil--last-state nil)

;;;###autoload
(defun my-mc-evil-compat-switch-state-h ()
  (when (and (bound-and-true-p evil-state)
             (not (member evil-state my-evil-typing-state-list)))
    ;; (setq-local my-mc-evil--last-state evil-state)
    ;; (evil-emacs-state)

    (evil-insert-state)

    ;; propagate `cursor-type' to the first fake cursor, as
    ;; `multiple-cursors-mode-enabled-hook' is ran AFTER the first fake cursor
    ;; is created, therefore it may not look identical to other ones, see
    ;; `mc/make-cursor-overlay-inline'/(overlay-put overlay 'face
    ;; 'mc/cursor-face)

    (mc/execute-command-for-all-fake-cursors #'ignore)
    ;; (evil-refresh-cursor 'insert)

    ;;
    ))

;; ;;;###autoload
;; (defun my-mc-evil-compat-restore-state-h ()
;;   (when (bound-and-true-p my-mc-evil--last-state)
;;     (cond
;;      ;; https://github.com/doomemacs/doomemacs/pull/5279
;;      ((member my-mc-evil--last-state '(multiedit multiedit-insert))
;;       (evil-normal-state))
;;      (t
;;       (evil-change-state my-mc-evil--last-state)))
;;     (setq-local my-mc-evil--last-state nil)))


(provide 'my-functions-multiple-cursors)

;; -*- lexical-binding: t; -*-

(require 'dash)

(require 'recentf)

;;;###autoload
(defun my-recentf-exclude (file)
  (<= (f-depth file) 1))

;;;###autoload
(defun my-recentf-add-directory-h (&rest _)
  (recentf-add-file default-directory))

(provide 'my-functions-recentf)

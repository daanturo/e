;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'tramp-loaddefs)

;; /method:user@host:/path/to/file

(defconst my-tramp-filename-regexp
  (rx-to-string
   `(seq
     bos
     (? 
      (seq
       ;; method
       "/" (group (+? anychar)) ":"
       (? 
        (seq
         ;; user
         (group (+? anychar))
         ;; host
         "@" (group (+? anychar)))
        ;;
        )
       ":"))
     (group (* anychar)))
   t))

;;;###autoload
(defun my-tramp-parse-remote-filename (filename)
  (save-match-data
    (string-match my-tramp-filename-regexp filename)
    (cdr (my-regexp-match-strings filename))))

;; (my-tramp-bisect-filename "/etc")
;; (my-tramp-bisect-filename "/sudo::/path/to/file")
;; (my-tramp-bisect-filename "/sudo:root@localhost:/path/to/file")

;;;###autoload
(defun my-tramp-bisect-filename (filename)
  (if (tramp-tramp-file-p filename)
      (progn
        (save-match-data
          (string-match my-tramp-filename-regexp filename)
          (-let* ((pos (match-beginning 4)))
            (list (substring filename 0 pos)
                  (substring filename pos)))))
    (progn
      (list nil filename))))

(defvar-local my-tramp-read-filename-toggle--non-path-components nil)

;;;###autoload
(defun my-tramp-read-filename-toggle-remote ()
  "Default to /sudo method."
  (interactive)
  (-let* (((lp rp) (my-tramp-bisect-filename (minibuffer-contents-no-properties)))
          (beg (minibuffer-prompt-end)))
    (if lp
        (progn
          (replace-region-contents beg (point-max) (-const rp))
          (setq-local my-tramp-read-filename-toggle--non-path-components
                      lp))
      (progn
        (my-insert-at-buffer-position
         beg
         (or my-tramp-read-filename-toggle--non-path-components
             ;; only "/sudo::" is enough, but supplying also user and host helps
             ;; discriminating Connection Local Profiles
             "/sudo:root@localhost:"))))))

(defvar my-root-sudo-tramp-method "sudo")

;;;###autoload
(defun my-root-sudo-method-prefix ()
  (format "/%s:root@localhost:" my-root-sudo-tramp-method))

;;;###autoload
(defun my-root-sudo-filename-path (filename)
  (format "%s%s"
          (my-root-sudo-method-prefix)
          (file-truename (expand-file-name filename))))

;;;###autoload
(cl-defun my-root-sudo-open-file (filename)
  "Open FILENAME as ROOT.
For more complex operations install crux.el and use `crux-sudo-edit'."
  (interactive (list (read-file-name "`my-root-sudo-edit-file': " nil)))
  (-let* ((filename1 (file-truename (expand-file-name filename)))
          (root-path (my-root-sudo-filename-path filename1))
          (orig-buffer (get-file-buffer filename1))
          (pt0
           (and orig-buffer
                (with-current-buffer orig-buffer
                  (point))))
          (buf (find-file root-path)))
    (when pt0
      (with-current-buffer buf
        (goto-char pt0)))
    buf))

;;;###autoload
(defun my-root-sudo-current-file ()
  "`my-root-sudo-open-file' current file."
  (interactive)
  (my-root-sudo-open-file (my-buffer-filename)))

(provide 'my-functions-tramp)

;; -*- lexical-binding: t; -*-

(require 'dash)

(require 'undo-tree)

(my-bind :map 'undo-tree-map
  "C-z" #'undo-tree-undo
  "C-S-z" #'undo-tree-redo
  "C-/" nil
  ;;
  )

;; Allow binding "C-/"
(advice-add #'undo-tree-overridden-undo-bindings-p :override #'ignore)

(add-to-list 'undo-tree-incompatible-major-modes 'minibuffer-mode)

(setq undo-tree-visualizer-diff t)
(setq undo-tree-visualizer-timestamps t)

;; disable persistence, the default saving location is awful
(setq undo-tree-auto-save-history nil)

(provide 'my-after-undo-tree)

;; Local Variables:
;; no-byte-compile: t
;; End:

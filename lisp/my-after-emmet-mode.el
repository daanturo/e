;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(my-bind :map 'emmet-mode-keymap '("<C-return>") nil)

;;; my-after-emmet-mode.el ends here

(provide 'my-after-emmet-mode)

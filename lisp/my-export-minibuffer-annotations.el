;; -*- lexical-binding: t; -*-

(require 'dash)

(require '00-my-core-macros)


;;;###autoload
(defun my-get-minibuffer-completion-property (prop)
  (or (completion-metadata-get (my-completion-metadata) prop)
      (plist-get completion-extra-properties
                 (intern (format ":%s" prop)))))

;;;###autoload
(defun my-get-minibuffer-affixation-function (&optional metadata)
  (or (my-get-minibuffer-completion-property 'affixation-function)
      ;; Create an affixation function from the annotation function
      (-when-let* ((annotator
                    (my-get-minibuffer-completion-property
                     'annotation-function)))
        (lambda (candidates)
          (-map
           (lambda (cand)
             (-if-let* ((suffix (funcall annotator cand)))
                 (list cand "" suffix)
               cand))
           candidates)))))

;;;###autoload
(defun my-get-minibuffer-annotations (&optional candidates metadata)
  (-let* ((candidates (or candidates (my-minibuffer-candidates)))
          (affixation-function (my-get-minibuffer-affixation-function)))
    ;; don't truncate
    (dlet ((marginalia-field-width most-positive-fixnum))
      (my-with-advice #'marginalia--truncate :override (my-fn% %1)
        (--> (sort candidates #'my-length-alpha<)
             ;; ;; Double reversing may extend docstrings a bit, but make values less readable
             ;; nreverse
             ;; (marginalia--affixate metadata (marginalia--annotator (completion-metadata-get metadata 'category)) it)
             (if affixation-function
                 (funcall (my-get-minibuffer-affixation-function) it)
               it)
             ;; nreverse
             )))))

;;;###autoload
(defun my-pretty-symbol-value (sym)
  (and (boundp sym)
       (--> (symbol-value sym)
            (cond
             ;; dont quote those special values
             ((member it '(t nil))
              it)
             ;; sharp-quote functions
             ((and (symbolp it)
                   (fboundp it))
              (list 'function it))
             ;; ;; quote symbols or lists
             ;; ((or (symbolp it) (listp it)) (list 'quote it))
             ;; other values
             (t it))
            (format "%S" it)
            my-string-in-lisp-mode
            my-replace-newlines)))

;;;###autoload
(defun my-export-minibuffer-annotations ()
  (interactive)
  (-let* ((candidates (my-minibuffer-candidates)))
    (my-export--minibuffer
     (format "*%s %s*"
             #'my-export-minibuffer-annotations
             (minibuffer-contents-no-properties))
     (--> candidates
          my-get-minibuffer-annotations
          ;; each element contains a list of three(?) elements
          (my-for [annotation/s it]
            (list nil (seq-into (-map #'my-replace-newlines annotation/s)
                                'vector)))))))

;;;###autoload
(defun my-export-minibuffer-symbols (&optional no-docstring)
  (my-with-deferred-gc
   (-let* ((candidates (my-minibuffer-candidates))
           (entries (my-for [cand (sort candidates #'my-length-alpha<)
                                  :let [sym (intern cand)]]
                      (list
                       nil
                       (vector
                        (propertize cand 'face 'marginalia-symbol)
                        (or (my-pretty-symbol-value sym)
                            "")
                        (if no-docstring
                            ""
                          (propertize
                           (--> (my-get-symbol-all-documentations|docstrings sym)
                                (string-join it " \n ")
                                my-replace-newlines)
                           ;; 'face 'completions-annotations
                           )))))))
     (my-export--minibuffer
      (format "*%s %s %s*"
              (my-minibuffer-completion-category)
              #'my-export-minibuffer-symbols
              (minibuffer-contents-no-properties))
      entries))))

(defun my-export--minibuffer (buf-name entries)
  (-let* ((buf (generate-new-buffer buf-name)))
    ;; (pop-to-buffer buf)
    (with-current-buffer buf
      (setq-local tabulated-list-entries entries)
      (my-tabulated-list-auto-columns)
      (my-exported-annotations-mode)
      (tabulated-list-print t)
      buf)))

;;;###autoload
(define-derived-mode my-exported-annotations-mode tabulated-list-mode "Annotated Candidates"
  "`tabulated-list-format' must be set before."
  (tabulated-list-init-header))
(define-key my-exported-annotations-mode-map
            [remap tabulated-list-widen-current-column] #'my-tabulated-list-fit-width-all-columns)
(define-key my-exported-annotations-mode-map
            [remap tabulated-list-narrow-current-column] #'my-tabulated-list-restore-from-fit-width-all-columns)

;;;###autoload
(cl-defun my-export-minibuffer-annotations-and-quit ()
  (interactive)
  (-let* ((buf (my-export-minibuffer-annotations)))
    (my-quit-minibuffer-and-run
     (pop-to-buffer buf))))

;;;###autoload
(cl-defun my-export-minibuffer-symbols-and-quit (&optional no-docstring)
  (interactive "P")
  (-let* ((buf (my-export-minibuffer-symbols no-docstring)))
    (my-quit-minibuffer-and-run
     (pop-to-buffer buf))))

(provide 'my-export-minibuffer-annotations)

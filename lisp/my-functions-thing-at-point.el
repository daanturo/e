;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

;;; Code:

;;;###autoload
(defun my-bounds-of-filename-component-at-point ()
  (-let* ((path-sepa-re "[/\\]")
          (re (rx-to-string `(or (regexp ,path-sepa-re)
                                 "\n"
                                 bol eol)))
          (lbp (line-beginning-position))
          (lep (line-end-position))
          (beg (save-excursion
                 (search-backward-regexp re lbp 'noerror)
                 (if (string-match-p path-sepa-re (char-to-string (char-after)))
                     (+ (point) 1)
                   (point))))
          (end (save-excursion
                 (search-forward-regexp re lep 'noerror)
                 (if (string-match-p path-sepa-re (char-to-string (char-before)))
                     (- (point) 1)
                   (point)))))
    (and beg end
         (< beg end)
         ;; not whole line
         (not (and (= beg lbp)
                   (= end lep)))
         (cons beg end))))

;;;###autoload
(defun my-mark-filename-component ()
  (interactive)
  (-some--> (my-bounds-of-filename-component-at-point)
    (my-set-mark-from-to (cdr it)
                         (car it))))

;;;###autoload
(defun my-mark-filename-balanced-component ()
  (interactive)
  (-when-let* (((beg . end) (my-bounds-of-filename-component-at-point))
               (_ (puni-region-balance-p beg end)))
    (my-set-mark-from-to end beg)))

;;;###autoload
(put 'my-filename-component 'bounds-of-thing-at-point
     'my-bounds-of-filename-component-at-point)

;;;###autoload
(cl-defun my-bounds-by-regexps
    (beg-re
     end-re
     &key
     beg-bound
     end-bound
     (beg-form '(match-end 0))
     (end-form '(match-beginning 0))
     beg-then-end
     no-save-excursion
     no-empty
     allow-outside
     ignore-case)
  "Get the bounds (a cons cell) of region delimited by BEG-RE and END-RE.
BEG-FORM and END-FORM is used to extract the returned bounds from
the match data (see the source code for the default (inner
positions)). Ignore cases with IGNORE-CASE. NO-EMPTY: the
returned bounds must not be the same. BEG-BOUND and END-BOUND are
limits. BEG-THEN-END: search the beginning before the end.
Non-nil NO-SAVE-EXCURSION: do not protect current point after
searching for the first bound. ALLOW-OUTSIDE: return even when
the region doesn't contain current point."
  ;; find beg after, leaving `match-data' to find some info
  (dlet ((case-fold-search ignore-case))
    (save-excursion
      (-when-let* ((pt0 (point))
                   (beg-fn
                    (lambda ()
                      (-let* ((fn
                               (lambda ()
                                 (when (re-search-backward beg-re
                                                           beg-bound
                                                           'noerror)
                                   (goto-char (eval beg-form t))))))
                        (if no-save-excursion
                            (funcall fn)
                          (save-excursion (funcall fn))))))
                   (end-fn
                    (lambda ()
                      (-let* ((fn
                               (lambda ()
                                 (when (re-search-forward end-re
                                                          end-bound
                                                          'noerror)
                                   (goto-char (eval end-form t))))))
                        (if no-save-excursion
                            (save-excursion (funcall fn))
                          (funcall fn)))))
                   ([beg-pos end-pos]
                    (if beg-then-end
                        (vector (funcall beg-fn) (funcall end-fn))
                      (seq-reverse (vector (funcall end-fn) (funcall beg-fn))))))
        (and beg-pos
             end-pos
             (and (if allow-outside
                      (<= beg-pos end-pos)
                    (<= beg-pos pt0 end-pos))
                  (if no-empty
                      (< beg-pos end-pos)
                    t))
             (cons beg-pos end-pos))))))

;;;###autoload
(defun my-bounds-of-inner-vertical-bar-|-table-cell ()
  (my-bounds-by-regexps "|" "|"
                        :beg-bound (line-beginning-position)
                        :end-bound (line-end-position)))

;;;###autoload
(defun my-mark-inner-vertical-bar-|-table-cell ()
  (interactive)
  (my-mark-bounds-as-cons-maybe
   (my-bounds-of-inner-vertical-bar-|-table-cell)))

;;;###autoload
(defun my-bounds-of-inner-string ()
  (save-excursion
    (-let* ((ppss (syntax-ppss))
            (in-str? (nth 3 ppss))
            (outer-str-beg (nth 8 ppss)))
      (when in-str?
        (goto-char outer-str-beg)
        (-let* ((inner-str-beg (save-excursion
                                 (while (not (my-point-in-string-p))
                                   (forward-char))
                                 (point)))
                (inner-str-end (progn
                                 (forward-sexp)
                                 (while (not (my-point-in-string-p))
                                   (backward-char))
                                 (point))))
          (when (<= inner-str-beg inner-str-end)
            (cons inner-str-beg inner-str-end)))))))
;;;###autoload
(put 'my-inner-string 'bounds-of-thing-at-point
     'my-bounds-of-inner-string)

;;;###autoload
(defun my-bounds-of-non-space ()
  (my-bounds-by-regexps
   "^\\|[ \t\n\r]"
   "$\\|[ \t\n\r]"
   :beg-bound (pos-bol)
   :end-bound (pos-eol)))

;;;###autoload
(defun my-mark-balanced-non-space ()
  (interactive)
  (-when-let* (((beg . end) (my-bounds-of-non-space)))
    (when (puni-region-balance-p beg end)
      (my-set-mark-from-to end beg))))

;;;###autoload
(defun my-bounds-of-visual-text-paragraph ()
  (and (not (my-point-in-emptyish-line-p))
       (my-bounds-by-regexps
        "\\(?:\n\\|\\`\\)\\(\n\\|\\`\\)"
        ;; including newline
        "\\(\n\\|\\'\\)\\(?:\n\\|\\'\\)"
        :beg-form '(match-end 0)
        :end-form '(match-end 1)
        :beg-then-end t
        :no-save-excursion t)))
;;;###autoload
(put
 'my-visual-text-paragraph
 'bounds-of-thing-at-point
 'my-bounds-of-visual-text-paragraph)
;;;###autoload
(defun my-mark-visual-text-paragraph-by-bounds ()
  (interactive)
  (my-mark-bounds-as-cons-maybe (my-bounds-of-visual-text-paragraph)))


;;;###autoload
(defun my-looking-back-symbol-with-only-digits-p ()
  "Roughly equivalent to (my-looking-back-p \"\\_<[0-9]+\").
But more efficient."
  (-let* ((at-num (<= ?0 (char-before) ?9)))
    (and at-num
         (named-let
             recur ((pos (- (point) 1)))
           (-let* ((char (char-before pos)))
             (cond
              ;; [ \t\n\f\r_-]
              ((member char '(?\s ?\t ?\n ?\f ?\r ?_ ?-))
               t)
              ((<= ?0 char ?9)
               (recur (- pos 1)))
              (t
               nil)))))))

;;;###autoload
(defun my-bounds-of-prev-sexp-to-point ()
  (dlet (
         ;; be consistent about sexps
         (forward-sexp-function nil))
    (ignore-error (scan-error)
      (-let* ((end (point-marker))
              (beg
               (save-excursion
                 (backward-sexp)
                 (point))))
        (cons beg end)))))

;;;###autoload
(defun my-prev-sexp-to-point ()
  (-when-let* (((beg . end) (my-bounds-of-prev-sexp-to-point)))
    (buffer-substring-no-properties beg end)))

;;;###autoload
(defun my-bounds-of-identifier* ()
  (save-excursion
    (-let* ((skip-str "-0-9A-Z_a-z")
            (pt0 (point))
            (beg
             (progn
               (skip-chars-backward skip-str)
               (point)))
            (end
             (progn
               (skip-chars-forward skip-str)
               (point))))
      (and (< beg end) (<= beg pt0 end) (cons beg end)))))

;;;###autoload
(defun my-mark-identifier* ()
  (interactive)
  (-when-let* (((beg . end) (my-bounds-of-identifier*)))
    (my-set-mark-from-to end beg)))

;;; my-functions-thing-at-point.el ends here

(provide 'my-functions-thing-at-point)

;; -*- lexical-binding: t; -*-

(require 'eshell)

(require 'dash)
(require '00-my-core-macros)

(defun my-eshell-add-shell-aliases ()
  (interactive)
  (-->
   (my-get-shell-aliases)
   (-map '-cons-to-list it)
   (my-add-list! 'eshell-command-aliases-list it)))

;;;###autoload
(progn
  (defun my-eshell-in-background ()
    (let ((default-directory "~")
          (gc-cons-threshold most-positive-fixnum))
      (save-window-excursion
        (eshell)))))
;;;###autoload
(my-accumulate-config-collect
 (setq
  eshell-cmpl-ignore-case t             ; must be set before loading `eshell'
  ))

(my-eshell-add-shell-aliases)

(my-bind :map 'eshell-mode-map "M-h" #'eshell-completion-help)
;; (my-bind :map 'eshell-mode-map '("C-s" [remap +eshell/search-history]) nil) ; set by Doom

(provide 'my-hook-eshell-mode-hook)

;; Local Variables:
;; no-byte-compile: t
;; End:

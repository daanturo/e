;; -*- lexical-binding: t; -*-

(require '00-my-core-macros)
(require 'dash)

;; (my-backtrace)

;; no need for `bound-and-true-p'
;;;###autoload
(defvar er/try-expand-list nil)

;; NOTE: default expansions must be added before `er/try-expand-list' becomes
;; buffer-local to be usable for buffers (that set it locally), after
;; `expand-region.el' is too late

(defvar my-er/expand-region-before-first-expand-in-buffer-hook '())
(defun my-er/expand-region--before-a (&rest _)
  (run-hooks 'my-er/expand-region-before-first-expand-in-buffer-hook)
  (setq-local my-er/expand-region-before-first-expand-in-buffer-hook '()))
(advice-add #'er/expand-region :before #'my-er/expand-region--before-a)

(add-hook
 'my-er/expand-region-before-first-expand-in-buffer-hook
 (lambda ()
   (when (derived-mode-p 'org-mode 'markdown-mode)
     (add-hook 'er/try-expand-list 'my-mark-inner-vertical-bar-|-table-cell
               nil
               'local))))

(provide 'my-after-expand-region)

;; Local Variables:
;; no-byte-compile: t
;; End:

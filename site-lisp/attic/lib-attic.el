;; -*- lexical-binding: t; -*-

(let ((f (or load-file-name buffer-file-name)))
  (user-error "Don't load %s." f))

(require 'dash)

(defvar my-themes-rank-list
  '(
    ;; Dark +
    doom-horizon
    doom-city-lights
    doom-dracula
    doom-moonlight
    doom-old-hope
    doom-one
    doom-snazzy
    doom-vibrant


    ;; Dark ~
    doom-Iosvkem
    doom-acario-dark
    doom-dark+
    doom-henna
    doom-laserwave
    doom-material
    doom-palenight
    doom-rouge
    doom-solarized-dark
    doom-spacegrey
    doom-tomorrow-night

    ;; Dark -
    doom-ayu-mirage
    doom-challenger-deep
    doom-ephemeral
    doom-fairy-floss
    doom-gruvbox
    doom-homage-black
    doom-manegarm
    doom-miramare
    doom-molokai
    doom-monokai-classic
    doom-monokai-pro
    doom-monokai-spectrum
    doom-nord
    doom-nova
    doom-oceanic-next
    doom-opera
    doom-outrun-electric
    doom-peacock
    doom-plain-dark
    doom-sourcerer
    doom-wilmersdorf
    doom-zenburn

    ;; Light
    doom-acario-light
    doom-ayu-light
    doom-flatwhite
    doom-gruvbox-light
    doom-homage-white
    doom-nord-light
    doom-one-light
    doom-opera-light
    doom-plain
    doom-solarized-light
    doom-tomorrow-day

    ;;
    ))

;; doesn't work in Doom-Emacs OOTB (overridden by `+popup-mode')
(add-to-list 'display-buffer-alist '("\\*Async Shell Command\\*.*" . '(display-buffer-no-window . nil)))

'((which-function-mode)
  (setq which-func-cleanup-function
        (defun my-which-func-cleanup-function (str)
          (truncate-string-to-width str 24 nil nil 'ellipsis))))

'((defun my-evil-define-key*-avoid-emacs-keys-in-insert-state (func state/s keymap key def &rest bindings)
    "Advice for `evil-define-key*', which should be FUNC.
In insert state: remap \"C-x .*\" keys to \"C-s .*\", ignore other collided keys."
    (let* ((key-readable (if (ignore-errors (key-description key)) (key-description key) key))
           (states (ensure-list state/s))
           (insert-not-emacs-flag (and (member 'insert states)
                                       (not (member 'emacs states))))
           (global-looked-up-command (lookup-key global-map (kbd key-readable)))
           (global-free-flag (or (equal def global-looked-up-command)
                                 (numberp global-looked-up-command)
                                 (null global-looked-up-command)))
           (mode-of-map (if (listp keymap) (evil-mode-for-keymap keymap) keymap)))
      (if (and insert-not-emacs-flag
               (member keymap (list global-map nil 'global)))
          (if (or global-free-flag
                  (string-match-p "^C-x " key-readable))
              (let ((new-key (replace-regexp-in-string "^C-x " "C-c " key-readable)))
                (when (not (equal key-readable new-key))
                  (push (list state/s mode-of-map key-readable def new-key bindings "C-x to C-c" load-file-name) my-evil-insert-state-modified-keys))
                (apply func nil keymap (kbd new-key) def bindings))
            (push (list state/s mode-of-map key-readable def nil bindings "In global map, conflicts and not omni complete" load-file-name) my-evil-insert-state-modified-keys))
        ;; (if (and (not global-free-flag)
        ;;          insert-not-emacs-flag)
        ;;     (push (list state/s mode-of-map key-readable def nil bindings "In local map, conflicts" load-file-name) my-evil-insert-state-modified-keys))
        (apply func state/s keymap key def bindings))))
  (advice-add 'evil-define-key* :around 'my-evil-define-key*-avoid-emacs-keys-in-insert-state))

;; Activate all `dash-docs' sets
'(mapc 'dash-docs-activate-docset (dash-docs-installed-docsets))

'(progn
   (require 'multiple-cursors-core)
   (cl-loop for command being the symbols
            when (and (commandp command)
                      (string-match-p "^special-lispy" (symbol-name command)))
            do (progn
                 (my-delete-in-list! 'mc/cmds-to-run-once command)
                 (cl-pushnew command mc/cmds-to-run-for-all)))
   (mc/save-lists))

;; `rainbow-mode' screws up `+doom-dashboard', `list-faces-display' (a child of `help-mode')

'(let ((c-spc-insert (lookup-key evil-insert-state-map (kbd "C-SPC")))
       (c-x-insert (lookup-key evil-insert-state-map (kbd "C-x"))))
   (when (and (not (keymapp c-spc-insert)) c-x-insert)
     (define-key evil-insert-state-map (kbd "C-x") nil)
     (define-key evil-insert-state-map (kbd "C-SPC") c-x-insert)
     (define-key evil-insert-state-map (kbd "C-SPC SPC") c-spc-insert)
     (define-key evil-insert-state-map (kbd "C-SPC C-SPC") (lookup-key global-map (kbd "C-SPC")))))

'(let ((c-spc-insert (lookup-key
                      evil-insert-state-map
                      (kbd "C-SPC")))
       (c-x-insert (lookup-key
                    evil-insert-state-map
                    (kbd "C-x"))))
   (when (and (not (keymapp c-spc-insert))
              c-x-insert)
     (global-set-key
      (kbd "S-SPC SPC")
      c-spc-insert)
     (map-keymap
      (lambda (event def)
        (global-set-key
         (kbd (concat
               "S-SPC "
               (let ((key-desc (key-description
                                (vector event))))
                 (if (string-match-p "^C-" key-desc)
                     (upcase
                      (string-remove-prefix
                       "C-"
                       key-desc))
                   key-desc))))
         def))
      c-x-insert)))

'(put #'foo 'function-documentation
      (concat (format "Perform either `%s' or `%s'.%s\n--------\n%s"
                      #'evil-repeat-pop #'my-correct-dwim
                      (documentation #'evil-repeat-pop) (documentation #'my-correct-dwim))))

'(autoload #'tramp-register-crypt-file-name-handler "tramp-crypt")
'(with-eval-after-load 'tramp
   ;; Don't use an authentication file for TRAMP passwords, credit: https://debbugs.gnu.org/46674
   (connection-local-set-profile-variables 'remote-without-auth-sources '((auth-sources . nil)))
   (connection-local-set-profiles '(:application tramp) 'remote-without-auth-sources))

(defun my-iedit-evil-mc-a (func &rest args)
  "Use `evil-mc' instead of `multiple-cursors' when called with prefix argument."
  (if current-prefix-arg
      (cl-letf (((symbol-function 'mc/create-fake-cursor-at-point) #'evil-mc-make-cursor-here)
                ((symbol-function 'multiple-cursors-mode) #'ignore))
        (apply func args))
    (apply func args)))

'(advice-add #'iedit-switch-to-mc-mode :around #'my-iedit-evil-mc-a)

'(with-eval-after-load 'gcmh
   (custom-reevaluate-setting 'gcmh-high-cons-threshold))

;; Meta-digit key bindings are occupied by workspaces module
'(dotimes (i 10) (global-set-key (kbd (format "M-%s" i)) #'digit-argument))


'(progn
   (defconst my-rainbow-delimiter-colors '(
                                           "cyan"
                                           "pink"
                                           "turquoise"
                                           "plum"
                                           "aquamarine"
                                           "violet"
                                           "coral"
                                           "purple"
                                           "salmon"
                                           ))
   (with-eval-after-load 'rainbow-delimiters
     (dolist (i (number-sequence 1 rainbow-delimiters-max-face-count))
       (set-face-foreground
        (intern (format "rainbow-delimiters-depth-%s-face" i))
        (nth (1- i) my-rainbow-delimiter-colors)))))


'(with-eval-after-load 'persp-mode
   (custom-reevaluate-setting 'uniquify-buffer-name-style)
   (custom-reevaluate-setting 'persp-emacsclient-init-frame-behaviour-override) ;C-x 5 2
   )

;; Enable comments highlighting, disable due to reduced contrast against marked region
'(set (intern (format "%s-brighter-comments" doom-theme)) t)

(defun my-ivy-toggle-help||tab (&rest _args)
  "Either describe current symbol or perform partial complete.
If last command is `self' and \"*Help*\" window is shown, delete it; else describe current symbol.
Ivy's native TABs are performed from the second invocation onward."
  (interactive)
  (eval-and-compile (require 'ivy))
  (let ((help-window (get-buffer-window (help-buffer)))
        (symbol (intern (ivy-state-current ivy-last))))
    (if (equal last-command this-command)
        (if help-window
            (quit-window nil help-window)
          (ivy-partial-or-done))
      (describe-symbol symbol))))

'(my-custom-set-faces '(ivy-minibuffer-match-face-1 :foreground nil :weight bold :inherit ivy-minibuffer-match-highlight))

(defun my-lisp-prettify||format-neighbor-defuns ()
  "Use `lispy' to prettify/format 2 defuns around the current one.
Normalizing current defun is either too destructive or impossible to preserve point position."
  (interactive)
  (let ((top-point-of-current-defun (save-excursion
                                      (beginning-of-defun)
                                      (point))))
    (save-excursion
      (beginning-of-defun 2)
      (unless (= (point) top-point-of-current-defun)
        (lispy--normalize-1)))
    (save-excursion
      (end-of-defun 2)
      (beginning-of-defun)
      (unless (= (point) top-point-of-current-defun)
        (lispy--normalize-1))))
  (my-safe (crux-cleanup-buffer-or-region)))

(defun my-xref-find-references-dwim (&optional arg)
  (interactive "P")
  (let ((xref-prompt-for-identifier (or arg
                                        (not (symbol-at-point))))
        (init-dir (or (my-project-root)
                      default-directory)))
    (if (my-xref-use-tag-p)
        (progn
          (cond ((and (executable-find "rg")
                      (fboundp 'rg))
                 (rg
                  (symbol-name (symbol-at-point))
                  "everything"
                  (or (my-project-root)
                      default-directory)))
                (t
                 (rgrep
                  (symbol-name (symbol-at-point))
                  "*"
                  (or (my-project-root)
                      default-directory))))
          (other-window 1))
      (call-interactively
       'xref-find-references))))

(defun my-xref-use-tag-p ()
  (string-match-p "tag" (symbol-name (car xref-backend-functions))))
(defalias 'my-xref-use-tag? 'my-xref-use-tag-p)

(defun my-xref-find-definitions-or-imenu ()
  (interactive)
  (if (my-xref-use-tag-p)
      (or (my-safe (imenu-anywhere))
          (call-interactively 'imenu))
    (call-interactively 'xref-find-definitions)))

(defun my-evil-colletion-define-key--no-insert-a (func states &rest args)
  (let* ((states (if (listp states) states (list states)))
         (states-to-bind (remove 'insert states)))
    ;; When state is `nil', `evil-define-key*' applies bindings to all states,
    ;; in that case we must not ignore it
    (when (or (null states)
              states-to-bind)
      (apply func states-to-bind args))))

(defconst my-major-mode-list (-uniq (seq-filter #'commandp (seq-map #'cdr auto-mode-alist))))
'(dolist (mode my-major-mode-list)
   (with-eval-after-load
       (intern
        (string-remove-suffix "-mode" (format "evil-%s" mode)))
     (define-key (symbol-value (intern (format "evil-%s-map" mode))) [insert-state] nil)))

;;;###autoload
(defun my-quelpa-async-load+install+upgrade (quelpa-args)
  (require 'async nil 'noerror)
  (let* ((pkg (car quelpa-args))
         (pkg-installed-flag (require pkg nil 'noerror))
         (operation (intern (format "%s--%s" #'my-quelpa-async-load+install+upgrade (symbol-name pkg)))))
    (defalias operation
      (lambda ()
        (async-start (lambda ()
                       (package-initialize)
                       (quelpa quelpa-args))
                     (lambda (_result)
                       (unless pkg-installed-flag
                         (package-initialize)
                         (require pkg nil 'noerror)))))
      (format "Asynchronously apply `quelpa' on \'%s" quelpa-args))
    (add-hook 'package--post-download-archives-hook operation)))

(defmacro my-bind--macro (key/s def &optional keymap after-lib autoload-from)
  "Define KEY/S (string or list of strings) as DEF in KEYMAP (default: `global-map').
With non-nil AFTER-LIB, execute after loading it. Autoload DEF from AUTOLOAD-FROM."
  `(with-eval-after-load
       (or ,after-lib 'emacs)
     (when (and (or (fboundp ,def)
                    (null ,def)
                    (listp ,def)
                    (when (and
                           ,autoload-from
                           (locate-library (format "%s" ,autoload-from)))
                      (autoload ,def
                        (format "%s" ,autoload-from)
                        nil 'interactive)
                      t))
                (when (boundp ',keymap)
                  (or (null ,keymap)
                      (keymapp ,keymap))))
       (dolist (key
                (if (listp ,key/s)
                    ,key/s
                  (list ,key/s)))
         (define-key (or ,keymap global-map)
	                 (if (vectorp key) key (kbd key))
	                 ,def)))))

(defvar-local my-completion-low-priority-candidate-hash-table (make-hash-table))
(defun my-completion-low-priority-candidate-p (cand &optional from-hash-table)
  (if from-hash-table
      (gethash cand my-completion-low-priority-candidate-hash-table)
    (member (get-text-property 0 'company-backend cand)
            my-completion-low-priority-backend-list)))
'(for cand in cands
      do (puthash cand
                  (my-completion-low-priority-candidate-p cand)
                  my-completion-low-priority-candidate-hash-table))

;;;###autoload
(cl-defun my-sorted-unique (seq &optional <fn (=fn #'equal))
  "Return unique elements of SEQ (compared by =FN) after sorting by <FN."
  (let ((sorted-seq
         (sort seq
               (cond (<fn <fn)
                     ((numberp (car seq)) #'<)
                     ((stringp (car seq)) #'string<)))))
    ;; (cl-loop for cell on sorted-seq
    ;;          when (or (= 1 (length cell))
    ;;                   (not (funcall =-fn
    ;;                                 (nth 0 cell)
    ;;                                 (nth 1 cell))))
    ;;          collect (nth 0 cell))
    (seq-remove
     #'null
     (seq-map-indexed
      (lambda (elt idx)
        (cond ((= idx 0) elt)
              ((funcall =fn elt (nth (1- idx) sorted-seq)) nil)
              (t elt)))
      sorted-seq))))

;;;###autoload
(defun my-dap-save-template (&optional arg)
  (interactive "P")
  (let ((case-fold-search t))
    (if-let ((dap-template-file-nondir "dap-templates.el")
             (project-root (when (and (not (buffer-file-name))
                                      (string-match-p "dap template"
                                                      (buffer-name)))
                             ;; Mark current defun to show which template to be saved
                             (completing-read
                              (format "Append DAP template at point to %s at project root: "
                                      dap-template-file-nondir)
                              (projectile-open-projects))))
             (dap-template-file (file-name-concat project-root dap-template-file-nondir)))
        (save-excursion
          (mark-defun)
          (append-to-file (region-beginning) (region-end) dap-template-file))
      (funcall-interactively 'save-buffer arg))))

(defun my-test-tail-call ()
  (cl-loop
   for speed in (number-sequence -1 3)
   collect
   (progn
     (defun test-tail (num)
       (if (= num 0)
           0
         (test-tail (1- num))))
     (let ((native-comp-speed speed))
       (native-compile 'test-tail)
       (ignore-errors
         (test-tail 65536))))))

;;;###autoload
(defmacro my-add-init (sth fn-id arg-list &rest body)
  "Initialize STH lazily.
When STH is a hook, add to STH a function whose name contains
FN-ID, with ARG-LIST and BODY, and remove itself from STH before
running.
When STH is a function, behave like `advice-add', with
WHERE as the first element of BODY.
Else evaluate BODY after loading STH."
  (declare (indent defun))
  ;; Reason for not generating form inside `cond': STH & FN-ID may be unable to
  ;; be evaluated
  `(let ((func (my-concat-symbols '@ ,sth ,fn-id)))
     (cond ((string-match-p "\\`hook-" (format "%s" ,sth))
            (let ((hook (intern (string-remove-prefix "hook-" (format "%s" ,sth)))))
              (fset func
                    (lambda ,arg-list
                      (remove-hook hook func)
                      ,@body))
              (add-hook hook func)))
           ((string-match-p "\\`after-" (format "%s" ,sth))
            (with-eval-after-load
                (intern
                 (string-remove-prefix (symbol-name 'after-) (format "%s" ,sth)))
              ,@body)))))

;; `coterm' sometimes messes up the display of indentations
'(my-add-init 'comint-mode-hook 'coterm (&rest _) (my-safe (coterm-mode))) ; `coterm-mode' requires `term'

(defmacro clj-loop-no-named-let (bindings &rest body)
  `(funcall
    (cl-labels
        ((recur
           ,(seq-remove
             #'null
             (seq-map-indexed
              (lambda (elt idx)
	            (when (cl-evenp idx) elt))
              bindings))
           ,@body))
      #'recur)
    ,@(seq-remove
       #'null
       (seq-map-indexed
        (lambda (elt idx)
          (when (cl-oddp idx) elt))
        bindings))))

`(my-custom-set-faces
   `(font-lock-builtin-face :slant italic) ; italic `:keyword'
   `(font-lock-preprocessor-face :slant italic) ; with keyword-style arguments passing, distinguish keywords with passed values
   )

(defun my-spell-complete-set-up (&rest _)
  "Setup completion for words using `ispell'.
 NOTE: Use `ispell-alternate-dictionary' instead of this."
  (interactive)
  (cond
   ((executable-find "hunspell")
    (let ((dict-files
           (seq-map
            (lambda (dic) (concat "/usr/share/hunspell/" dic ".dic"))
            my-locale-spell-list)))
      (setq ispell-complete-word-dict (or ispell-complete-word-dict "~/.local/hunspell-word-dict"))
      (when (and (not (file-exists-p ispell-complete-word-dict))
                 (file-writable-p ispell-complete-word-dict))
        (shell-command (format "cat %s > %s"
                               (string-join dict-files " ")
                               ispell-complete-word-dict)))))
   ((executable-find "aspell")
    (setq ispell-complete-word-dict (or ispell-complete-word-dict "~/.local/aspell-word-dict"))
    "aspell -d en dump master | sort - > %s")))

;;;###autoload
(defun my-treemacs-remove-all ()
  "Remove all `treemacs' projects."
  (interactive)
  (ignore-errors
    (mapc #'treemacs-do-remove-project-from-workspace
          (seq-mapcat #'treemacs-workspace->projects (treemacs-workspaces)))))

(defmacro setq! (&rest args)
  "Set VAR to VAL and trigger setter functions on each pair of ARGS with `setq'-like syntax."
  `(dolist (pair (seq-partition ',args 2))
     (-let* (((var . val) pair))
       (funcall (or (get var 'custom-set) #'set-default) var val))))

(defmacro cmd! (&rest body)
  "(lambda (&rest _) (interactive) ,@BODY)."
  `(lambda (&rest _) (interactive) ,@body))

(defmacro pushnew! (place &rest members)
  `(dolist (member (list ,@members))
     (cl-pushnew member ,place :test #'equal)))

(defmacro delq! (element list &optional fetcher)
  `(setq ,list (delq (if ,fetcher
                         (funcall ,fetcher ,element ,list)
                       ,element)
                     ,list)))

;;;###autoload
(defvar my-asdf-command
  (cond
   ((file-exists-p "/opt/asdf-vm/asdf.sh")
    "source /opt/asdf-vm/asdf.sh && asdf")))

(defvar my-old-nodejs-version-list
  (cond
   (my-asdf-command
    (split-string (shell-command-to-string
                   (format "%s list nodejs" my-asdf-command))))))

(setq lsp-clients-angular-node-get-prefix-command
      (cond
       (my-asdf-command
        (format
         "%s local nodejs %s && unset npm_config_prefix && %s"
         my-asdf-command
         (my-at my-old-nodejs-version-list -1)
         "npm config get --global prefix"))))

;;;###autoload
(defun my-async-shell-command-buffer-content ()
  (with-current-buffer shell-command-buffer-name-async
    (string-trim (buffer-string))))

;;;###autoload
(defmacro my-cut (&rest params)
  (let* ((missing-args-indexes (my-keep-indexed
                                (lambda (elt idx) (if (equal elt '<>) idx nil))
                                params))
         (form (seq-map-indexed
                (lambda (elt idx)
                  (if (equal elt '<>)
                      (intern (format "%s-%s-%s" 'my-cut 'arg idx))
                    elt))
                params)))
    `(lambda ,(seq-map (lambda (idx) (intern (format "%s-%s-%s" 'my-cut 'arg idx))) missing-args-indexes)
       ,(if (equal '<> (car params))
            `(funcall ,(car form) ,@(cdr form))
          `(,@form)))))

;; Otherwise there will be lags when using the flex completion style
;; corfu-quit-no-match t
;; Not quitting is annoying when: after typing `def' to define a new symbol, words completions, etc.
;; corfu-quit-at-boundary t

;;;###autoload
(defun my-balanced--kill-line (&optional back)
  ;; in comments?
  (let* ((move-func (if back #'backward-sexp #'forward-sexp))
         (orig-line (line-number-at-pos))
         (to-del-pos (save-excursion
                       (my-loop [old-point (point)]
                         (ignore-error 'scan-error (funcall move-func))
                         (let ((p (point)))
                           (if (or
                                ;; end of line
                                (eolp)
                                ;; end of list
                                (equal old-point p)
                                ;; moved to another line
                                (/= orig-line (line-number-at-pos p)))
                               p
                             (recur p)))))))
    (kill-region (point) to-del-pos)))

;;;###autoload
(cl-defun my-variadic-plist-to-alist (plist &optional (keyword-test #'keywordp))
  "Convert a variadic PLIST into an association list.
From the form of '(:k0 e0k0 e1k0 ... :kn e0kn e1kn ...) into
'((:k0 e0k0 e1k0 ... ) ... (:kn e0kn e1kn ...)) where each :ki is
recognized by KEYWORD-TEST."
  (let*
      ((virtual-plist
        ;; Construct virtual input with the last element as a keyword to get the
        ;; sub lists easily
        `(,@plist ,(seq-find keyword-test plist)))
       (keyword-index-list
        (my-keep-indexed
         (lambda (element index)
           (if (funcall keyword-test element)
               index
             nil))
         virtual-plist)))
    (my-for [i (my-range
                ;; As the last element of `virtual-plist' is artificial, don't
                ;; get its trailing elements
                (1- (length keyword-index-list)))]
      (seq-subseq virtual-plist
                  (my-at keyword-index-list i)
                  (my-at keyword-index-list (1+ i))))))

(defun my-get-paragraph-sp-parstart ()
  "Stripped down from what is used in `forward-paragraph'."
  (format "^[ \t]*\\(?:%s\\|%s\\)" paragraph-start paragraph-separate))

`(add-hook 'minibuffer-setup-hook
           (my-defn my-setup-smartparens-single-quote-in-minibuffer (&rest _)
             "In M-:, don't auto-close ', but still enable for other minibuffer operations.
The behaviors may not be correct when there are recursive
minibuffers."
             (sp-local-pair '(minibuffer-mode minibuffer-inactive-mode) "'" "'" :actions
                            (if minibuffer-completing-symbol
                                '(wrap navigate)
                              '(insert wrap navigate)))))

(my-defn my-minibuffer-completing-symbol? (&rest _)
  minibuffer-completing-symbol)


'(my-bind :vi 'insert '("C-a" "C-e") nil) ; when Doom's key bindings are left at the default


;; XXX `downlist' doesn't work for commments
;;;###autoload
(defun my-end-of-list||inner-sexp-position ()
  ;; ;; is interrupted by comments
  ;; (my-loop [old-point (point)] (ignore-error 'scan-error (forward-sexp))
  ;;   (if (equal old-point (point)) (point) (recur (point))))
  (save-excursion
    (if (condition-case _
            (progn (up-list 1 'escape-string 'no-syntax-crossing)
                   t)
          (scan-error nil))
        (progn (down-list -1)
               (point))
      (point-max))))
;;;###autoload
(defun my-beg-of-list||inner-sexp-position ()
  (save-excursion
    (if (condition-case _
            (progn (up-list -1 'escape-string 'no-syntax-crossing)
                   t)
          (scan-error nil))
        (progn (down-list 1)
               (point))
      (point-min))))

(defvar my-dired-preview-mode-map (make-sparse-keymap))
;;;###autoload
(define-minor-mode my-dired-preview-mode
  "Preview files with up/down."
  :global nil
  :lighter " 🔎"
  :keymap my-dired-preview-mode-map)
(my-bind :map 'my-dired-preview-mode-map "<up>" #'my-dired-preview-prev)
(my-bind :map 'my-dired-preview-mode-map "<down>" #'my-dired-preview-next)
(defun my-dired-preview-file-and-toggle-preview-mode ()
  (interactive)
  (my-dired-preview-mode 'toggle)
  (when my-dired-preview-mode
    (my-dired-preview)))

(defvar my-dired-last-previewed nil)

;;;###autoload
(defun my-dired-preview (&optional file)
  "Preview FILE, kill the previously previewed buffer by this command.
FILE defaults to the file at point."
  (interactive)
  (when (and (bufferp my-dired-last-previewed)
             (not (buffer-modified-p my-dired-last-previewed)))
    (kill-buffer my-dired-last-previewed))
  (let* ((file-to-visit (or file (dired-file-name-at-point)))
         (visited-buffer (get-file-buffer file-to-visit)))
    (dired-display-file)
    (setq my-dired-last-previewed
          (if visited-buffer nil (get-file-buffer file-to-visit)))))

;;;###autoload
(defun my-dired-preview-prev (&optional arg)
  (interactive "p")
  (dired-previous-line arg)
  (my-dired-preview))

;;;###autoload
(defun my-dired-preview-next (&optional arg)
  (interactive "p")
  (dired-next-line arg)
  (my-dired-preview))

;; Workaround Emacs 28's bug
;;;###autoload
(defun my-dired-browse-url-of-dired ()
  (interactive)
  (cl-letf (((symbol-function #'browse-url-select-handler)
             (lambda (&rest _)
               browse-url-browser-function)))
    (browse-url-of-dired-file)))

;;;###autoload
(defun my-filename-all-extensions-as-list (filename &optional period)
  (letrec ((helper-fn (lambda (f extensions)
                        (let ((ext (file-name-extension f period)))
                          (if ext
                              (funcall helper-fn (file-name-sans-extension f)
                                       (cons ext extensions))
                            (nreverse extensions)))))
           (f (file-name-nondirectory filename)))
    (funcall helper-fn f '())))

;;;###autoload
(defun my-multiple-cursors||iedit-add (direction)
  "Use either `iedit' or an mc implementation to mark in DIRECTION (\"next\", \"prev\").
If the region or `iedit' is active, use `iedit', else mc.
The first invocation of `iedit' will mark current occurrence first."
  (interactive)
  (require 'iedit)
  (call-interactively
   (let ((iedit-command (intern (format "evil-multiedit-match-and-%s" direction)))
         (evil-mc-command (intern (format "evil-mc-make-cursor-move-%s-line" direction))))
     (when (and (use-region-p)
                (not iedit-mode))
       (funcall iedit-command))
     (if (or (use-region-p) iedit-mode)
         iedit-command
       evil-mc-command))))

;;;###autoload
(defun my-rpartial (func &rest args)
  "Use `cut' or other alternatives instead.
Why not right-apply-partially: new optional arguments in future versions."
  (declare (pure t) (side-effect-free t))
  (lambda (&rest args1)
    (apply func (append args1 args))))

;;;###autoload
(defun my-cons-pair-p (arg)
  "Return whether ARG is a cons pair but it's `cdr' is not a list."
  (declare (pure t) (side-effect-free t))
  (and (consp arg)
       (not (proper-list-p (cdr-safe arg)))))
(defalias 'my-cons-pair? 'my-cons-pair-p)

;;;###autoload
(defmacro my-as-> (initial-expression name &rest forms)
  "Thread NAME through the following FORMS starting with INITIAL-EXPRESSION."
  (declare (indent defun) (debug t))
  (if (null forms)
      `,initial-expression
    `(let ((,name ,initial-expression))
       (my-as-> ,(if (listp (car forms)) (car forms) (list (car forms) name)) ,name ,@(cdr forms)))))
;;;###autoload
(defmacro my--> (initial-expression &rest forms)
  "From the value of INITIAL-EXPRESSION, thread each expression through FORMS as `it'."
  (declare (indent defun) (debug t))
  `(my-as-> ,initial-expression it ,@forms))

;;;###autoload
(defmacro my-define-advice (symbol post-fix where lambda-list &rest body)
  "(define-advice SYMBOL (WHERE LAMBDA-LIST POST-FIX) ,@BODY).
SYMBOL and POST-FIX must be quoted as input."
  `(define-advice ,(my-lexical-eval symbol) (,where ,lambda-list ,(my-lexical-eval post-fix)) ,@body))

(defun my-dap-launch-save-template-to-launch-json (template)
  (interactive (list (dap--select-template t)))
  (let* ((json-launch-path (dap-launch-find-launch-json))
         (new-json-launch-file (expand-file-name "launch.json" (lsp-workspace-root)))
         (config
          ;; Convert `nil's to empty vectors.
          (mapcar (lambda (elt) (if elt elt [])) template))
         (json-config (json-serialize config))
         (json-compat-list-to-convert `((configurations . [,config])))
         (json-object (json-serialize json-compat-list-to-convert)))
    (if json-launch-path
        (progn
          (kill-new json-config)
          (message json-config)
          (find-file new-json-launch-file))
      (progn
        (f-write json-object 'utf-8 new-json-launch-file)
        (find-file new-json-launch-file)
        (json-pretty-print-buffer)
        (save-buffer)))))

;;;###autoload
(defun my--p->?-predicate (pred)
  (let ((name (format "%s" pred)))
    (if (string-match-p "-p$" name)
        (intern (concat (string-remove-suffix "-p" name) "?"))
      pred)))
;;;###autoload
(defun my-?->-p-predidate (pred)
  (let ((name (format "%s" pred)))
    (if (string-match-p "\\?$" name)
        (intern (concat (string-remove-suffix "?" name) "-p"))
      pred)))
;;;###autoload
(defun my-define-other-style-of-predicate (pred)
  ;; customize `imenu-generic-expression'
  (let ((name-str (symbol-name pred)))
    (when (string-match-p "-p$" name-str)
      (defalias (my--p->?-predicate name-str) pred))
    (when (string-match-p "\\?$" name-str)
      (defalias (my-?->-p-predidate name-str) pred))))

;;;###autoload
(defmacro my-with-alt-completion-styles (&rest body)
  `(dlet ((completion-styles (my-completion-styles+))
          (orderless-matching-styles (-uniq (my-orderless-styles+))))
     ,@body))

;;;###autoload
(defun my-orderless-styles+ ()
  (-uniq (cons 'orderless-flex (bound-and-true-p orderless-matching-styles))))

;;;###autoload
(defmacro my-with-default-completion-in-region-function (&rest body)
  `(dlet ((completion-in-region-function (default-value 'completion-in-region-function)))
     ,@body))

;;;###autoload
(defun my-key0|key1&absent-key0-in-plist (key0 key1 plist)
  (or (plist-get key0)
      (and (plist-get key1)
           (not (member key0 plist)))))

;; (with-eval-after-load 'minibuffer
;;   (set-face-attribute 'completions-common-part nil :weight 'bold))

;; (my-add-advice-once #'completion-try-completion :before (my-fn% (set-face-bold 'completions-common-part t)))
;; (my-add-advice-once #'completion-all-completions :before (my-fn% (set-face-bold 'completions-common-part t)))

;; (custom-set-faces
;;  `(completions-common-part ((t (:weight bold)))) ; brigher, more readable
;;  )

;;;###autoload
(defun my-define-minibuffer-key (key/s def)
  (let ((keys (cl-loop for k in (ensure-list key/s)
                       collect (if (vectorp k) k (kbd k)))))
    (dolist (k keys)
      (define-key minibuffer-mode-map k def))
    (with-eval-after-load 'ivy
      (dolist (k keys)
        (define-key ivy-minibuffer-map k def)))))

;;;###autoload
(defun my-insert-space-regexp (arg)
  "Insert ARG spaces, without moving the cursor.
Insert different things with prefix arguments."
  (interactive "P")
  (if (or (null arg) (numberp arg))
      (save-excursion
        (self-insert-command
         (prefix-numeric-value arg)
         (string-to-char " ")))
    (insert "[:space:]")))

;;;###autoload
(defun my-kill-empty-repl-buffers ()
  (--> (process-list)
       (seq-filter #'process-query-on-exit-flag it)
       (seq-map #'process-buffer it)
       (seq-map #'buffer-name it)
       (--filter (string-match-p "^\s?\\*.*repl.*\\*$" it) it)))

(defvar-local my-beg-defun-line-number-cached-lines-cache '())
(defvar-local my-beg-defun-line-number-cached-tick 0)
(defun my-beg-defun-line-number-cached-clear (&rest _)
  (setq-local my-beg-defun-line-number-cached-tick (buffer-modified-tick))
  (setq-local my-beg-defun-line-number-cached-lines-cache '()))
;;;###autoload
(defun my-beg-defun-line-number-cached (&optional invalidate-cache)
  ;; modified buffers: clear the cache
  (when (or invalidate-cache
            (/= my-beg-defun-line-number-cached-tick (buffer-modified-tick)))
    (my-beg-defun-line-number-cached-clear))
  (let* ((l (line-number-at-pos))
         (found-pair (seq-find (lambda (pair)
                                 (cl-destructuring-bind (begl . endl) pair
                                   (<= begl l endl)))
                               my-beg-defun-line-number-cached-lines-cache)))
    (if found-pair
        (car found-pair)
      (cl-destructuring-bind (beg . end) (bounds-of-thing-at-point 'defun)
        (let ((begl (line-number-at-pos beg)))
          (push (cons begl (line-number-at-pos end))
                my-beg-defun-line-number-cached-lines-cache)
          begl)))))

;;;###autoload
(defun my-current-language-support-kebab-case-p ()
  (declare (side-effect-free t))
  (char-equal ?_ (char-syntax ?-)))
(defalias 'my-current-language-support-kebab-case? 'my-current-language-support-kebab-case-p)

;;;###autoload
(defun my-current-minibuffer-region ()
  (declare (side-effect-free t))
  (my-with-minibuffer
   (list
    (minibuffer-prompt-end)
    (point-max))))

;;;###autoload
(defun my-add-initial-input-to-next-completing-read (input &optional provided-handler)
  "For the next `completing-read', add INPUT as \"initial-input\" to it.
When \"initial-input\" is a non-empty string: if PROVIDED-HANDLER
is non-nil, call it on the provided initial input and INPUT, else
just use the value passed to `completing-read'.

DEPRECATED, use `minibuffer-with-setup-hook' to insert initial input instead."
  (my-add-advice-once #'completing-read :around
    (lambda (func prompt collection &optional predicate require-match initial-input &rest other-args)
      (apply func prompt collection
             predicate require-match
             (cond ((or (not initial-input)
                        (string-empty-p initial-input))
                    input)
                   (provided-handler
                    (funcall provided-handler initial-input input))
                   (t
                    initial-input))
             other-args))))

;;;###autoload
(defun my-directory-children-trim-firsts (dir &optional full match)
  (let ((files (directory-files dir full match)))
    (if (equal '("." "..") `(,(car files) ,(cadr files)))
        (cddr files)
      files)))

;;;###autoload
(defun my-count-directory-children (dir &optional match)
  (my-loop [accu 0
                 files (directory-files dir match)
                 exclude '("." "..")]
    (cond ((not (and files exclude))
           (+ accu (length files)))
          ((when-let* ((f (car files))
                       (_ (member f exclude)))
             (recur accu (cdr files) (delete f exclude))))
          (t
           (recur (1+ accu) (cdr files) exclude))))
  ;; (let ((exclude '("." "..")))
  ;;   ;; (apply '+ (-map (lambda (f) (if (member f exclude) 0 1))
  ;;   ;;                 (directory-files dir nil match)))
  ;;   ;; (seq-reduce (lambda (total file)
  ;;   ;;               (if (member file exclude)
  ;;   ;;                   total
  ;;   ;;                 (1+ total)))
  ;;   ;;             (directory-files dir nil match)
  ;;   ;;             0)
  ;;   )
  )

;;;###autoload
(defun my-completing-filename-insert-/-after~-h ()
  (when (and (eobp)
             (string-suffix-p "/~" (minibuffer-contents-no-properties)))
    (insert "/")))

;;;###autoload
(defun my-completing-filename-post-insert-~-h ()
  (when minibuffer-completing-file-name
    (add-hook 'post-self-insert-hook #'my-completing-filename-insert-/-after~-h nil 'local)))

(defconst my-directory-files-non-full-almost-all-regexp
  (rx string-start (or
                    (not ".")
                    (seq "." (not "."))
                    (seq ".." anything)))
  ;; "^\\(?:[^.]\\|\\.[^.]\\|\\.\\..\\)"
  "Just use `directory-files-no-dot-files-regexp'.
Regular expression for files that are neither \".\" nor \"..\".
One of those case:
- Doesn't start with \".\" (non-hidden)
- Starts with \".\", but followed by a character that isn't \".\" (hidden, not \"..\")
- Starts with \"..\", but there must be more (hidden, \"..\" as initial, not \"..\")")

'(my-custom-theme-set-faces
   'doom-horizon
   `(font-lock-builtin-face :foreground "deep pink")
   `(font-lock-constant-face :foreground "hot pink")
   `(font-lock-doc-face :foreground "salmon")
   `(font-lock-function-name-face :foreground "cyan")
   `(font-lock-type-face :foreground "violet"))

;;;###autoload
(defun my-setup-minibuffer-file-map-maybe (&rest _)
  ;; minibuffer-completing-file-name
  (when (member (my-minibuffer-completion-category) '(file project-file))
    (use-local-map
     (make-composed-keymap minibuffer-local-filename-completion-map
                           (current-local-map)))))

;;;###autoload
(defun my-elisp-tree-sitter-innermost-parent-node-with-type/s (type/s &optional beg end)
  (let* ((types (ensure-list type/s))
         (beg (or beg (point)))
         (end (or end beg)))
    (my-loop [node (tsc-get-descendant-for-position-range
                    (tsc-root-node tree-sitter-tree)
                    beg end)]
      (cond ((null node)
             nil)
            ((member (tsc-node-type node) types)
             node)
            (t
             (recur (tsc-get-parent node)))))))

;;;###autoload
(defun my-elisp-tree-sitter-node-with-type/s-at (types &optional pos)
  (let ((pos (or pos (point))))
    (seq-some
     (lambda (type)
       (tree-sitter-node-at-pos type pos 'ignore-invalid-type))
     (ensure-list types))))

;;;###autoload
(cl-defun my-elisp-tree-sitter-lax-children-nodes-around (parent-node &optional (pos (point)))
  (or
   (my-elisp-tree-sitter-children-nodes-around parent-node pos)
   (my-elisp-tree-sitter-children-nodes-around
    parent-node
    (my-skip-chars-position nil pos))
   (my-elisp-tree-sitter-children-nodes-around
    parent-node
    (my-skip-chars-position nil pos nil 'backward))))

;;;###autoload
(defun my-prefix-argument? (arg)
  (cond ((null arg) arg)
        ((numberp arg) arg)
        ((and (listp arg)
              (numberp (car arg)))
         arg)
        ((equal '- arg) arg)))
(defalias #'my-prefix-argument-p #'my-prefix-argument?)

(defvar-local my-current-prefix-bindings nil
  "Last typed prefix bindings.")
(defvar-local my-current-prefix-bindings-buffer nil
  "The buffer to locate the correct `my-current-prefix-bindings'.")
;;;###autoload
(defun my-embark-prefix-help-bindings ()
  (interactive)
  (let* ((keys (this-command-keys-vector))
         (prefix (seq-take keys (1- (length keys))))
         (buf (current-buffer)))
    (setq-local my-current-prefix-bindings prefix
                my-current-prefix-bindings-buffer buf)
    (minibuffer-with-setup-hook
        (lambda ()
          (setq-local my-current-prefix-bindings-buffer buf
                      embark-exporters-alist '((t . my-export-prefix-bindings))))
      (embark-prefix-help-command))))
(defun my-export-prefix-bindings (&optional _)
  (interactive)
  (with-current-buffer my-current-prefix-bindings-buffer
    (describe-bindings my-current-prefix-bindings))
  (pop-to-buffer (help-buffer)))

'(with-eval-after-load 'outline-minor-faces
   ;; Save us from installing `backline'
   (set-face-attribute 'outline-minor-0 nil :background 'unspecified)
   ;; don't enlarge
   (set-face-attribute 'outline-minor-file-local-prop-line nil :inherit 'font-lock-comment-face))

'(my-add-advice/s '(consult-theme) :around #'my-set-system-theme-by-emacs-theme--a) ;; load-theme enable-theme

(defun my-with-popup-completions-a (func &rest args)
  (dlet ((completion-in-region-function
          (my-1st-fn #'corfu--in-region completion-in-region-function)))
    (apply func args)))
(defun my-with-minibuffer-completions-a (func &rest args)
  (dlet ((completion-in-region-function (default-value 'completion-in-region-function)))
    (apply func args)))
(defun my-corfu-hooks--h (&rest _)
  (kill-local-variable 'completion-in-region-function))

;; automatically find consult commands and use them as remaps, but
;; `mapatoms' is too expensive
(defvar my-consult-not-remapper-by-name-commands nil)
'(let ((consult-lib-prefix (format "^%s-" 'consult)))
   (cl-loop for consult-cmd being the symbols do
            (when (and
                   (string-match-p
                    consult-lib-prefix
                    (symbol-name consult-cmd))
                   (commandp consult-cmd)
                   (or
                    ;; maybe only autoloaded commands are available at this time and their `symbol-plist' isn't set
                    (not (symbol-plist consult-cmd))
                    ;; or it set and all are available, but must be autoloaded
                    (get consult-cmd 'autoload))
                   (not (get consult-cmd 'byte-obsolete-info))
                   (not (member consult-cmd '(consult-org-agenda))))
              (let ((cmd0
                     (intern (string-remove-prefix "consult-" (symbol-name consult-cmd)))))
                (if (commandp cmd0)
                    (progn (my-bind (vector 'remap cmd0) consult-cmd))
                  (push consult-cmd my-consult-not-remapper-by-name-commands))))))

;;;###autoload
(defun my-split-mixed-plist (lst)
  "Partition LST into 2 two groups as a cons cell: one with keys
and values and one with others."
  (declare (pure t) (side-effect-free t))
  (my-loop [tail lst
                 reversed-plist '()
                 reversed-non-plist '()]
    (cond ((seq-empty-p tail)
           (cons (nreverse reversed-plist)
                 (nreverse reversed-non-plist)))
          ((keywordp (car tail))
           (recur (cddr tail)
                  `(,(cadr tail) ,(car tail) . ,reversed-plist)
                  reversed-non-plist))
          (t
           (recur (cdr tail)
                  reversed-plist
                  `(,(car tail) . ,reversed-non-plist))))))

;;;###autoload
(defun my-enable-minor-mode-idle-timer (mode-symbol idle-function)
  (let ((func (my-concat-symbols '@ 'my-minor-mode-idle-timer mode-symbol 'function))
        (timer-sym (my-concat-symbols '@ 'my-minor-mode-idle-timer mode-symbol 'timer)))
    (my-maydefun-set func
      (()
       (dolist (buff (seq-filter #'get-buffer-window (buffer-list)))
         (with-current-buffer buff
           (when (symbol-value mode-symbol)
             (funcall idle-function))))))
    (funcall func)
    (unless (my-bounded-value timer-sym)
      (set timer-sym
           (run-with-idle-timer 0.5 'repeat func)))))
;;;###autoload
(defun my-disable-minor-mode-idle-timer (mode-symbol)
  (let ((func (my-concat-symbols '@ 'my-minor-mode-idle-timer mode-symbol 'function))
        (timer-sym (my-concat-symbols '@ 'my-minor-mode-idle-timer mode-symbol 'timer)))
    (when (seq-empty-p
           (my-buffers-with-minor-mode mode-symbol))
      (when timer-sym
        (cancel-timer timer-sym)
        (set timer-sym nil)))))

;;;###autoload
(defun my-add-hook-limited-when-startup (hook func)
  (add-hook 'emacs-startup-hook
            (lambda ()
              (add-hook hook func)))
  (my-add-hook-once hook
    (lambda ()
      (while-no-input
        (funcall func)))))

(defun my-partition-patterns-at-special (str special &optional take-before)
  "Return a list of 2 lists:
- Patterns in STR before SPECIAL (when non-nil TAKE-BEFORE)
- Patterns in STR after SPECIAL."
  (let ((patterns (split-string str " " 'omit-nulls)))
    (my-loop [reversed-before '() after '() pats patterns]
      (cond ((null pats)
             (list (nreverse reversed-before)
                   after))
            ((string= special (car pats))
             (list (nreverse reversed-before)
                   (-take-while #'my-valid-regexp? (cdr pats))))
            (t
             (recur (cons (car pats) reversed-before)
                    after
                    (cdr pats)))))))

;;;###autoload
(defun my-keep-height-fraction-of-line-in-window--a (func &rest args)
  (let* ((beg-line (line-number-at-pos (window-start)))
         (end-line (line-number-at-pos (window-end)))
         (cur-line (line-number-at-pos))
         (fraction (/ (float (- cur-line beg-line))
                      (- end-line beg-line)))
         (w (selected-window)))
    (unwind-protect (apply func args)
      (with-selected-window w
        (my-recenter-top-to-fraction fraction)))))
;;;###autoload
(defun my-Man-fit-to-window--a (func &rest args)
  (my-with-advice #'Man-update-manpage :around
    #'my-keep-height-fraction-of-line-in-window--a
    (apply func args)))
(advice-add #'Man-fit-to-window :around #'my-Man-fit-to-window--a)

;;;###autoload
(defun my-make-native-command-when-fail (evil-command)
  (my-defalias* (my-concat-symbols '@ evil-command #'my-make-native-command-when-fail)
    (lambda ()
      (interactive)
      (condition-case _
          (call-interactively evil-command)
        ('user-error
         (let ((native-command (my-evil-non-vi-command (this-command-keys-vector))))
           (message "Current key: %s, %s failed, calling `%s' instead."
                    (key-description (this-command-keys-vector))
                    evil-command
                    native-command)
           (call-interactively native-command)))))))

(when (fboundp 'doom-modeline-mode)
  (remove-hook 'after-init-hook 'doom-modeline-mode)
  (add-hook 'window-setup-hook
            (lambda () (my-add-hook-once 'window-buffer-change-functions 'doom-modeline-mode))))

;;;###autoload
(defun my-gen-around-advice-wrapping-a-function (wrappee wrapper)
  "Generate an advising func where WRAPPEE is replaced by WRAPPER."
  (let ((adv (intern (format "%s-%s-%s" #'my-gen-around-advice-wrapping-a-function wrappee wrapper))))
    (defalias adv
      (lambda (func &rest args)
        (cl-letf (((symbol-function wrappee) wrapper))
          (apply func args)))
      (format "With %s replaced by %s." wrappee wrapper))
    adv))

;;;###autoload
(defmacro my-update-hash (key old-doc-placeholder form table &optional default)
  (declare (indent defun))
  `(let ((,old-doc-placeholder (gethash ,key ,table ,default)))
     (puthash ,key ,form ,table)))

;; Configure smartparens in special modes (`wdired-mode', etc.) isn't easy
'(with-eval-after-load 'smartparens
   (setq-default sp-autoskip-closing-pair nil) ; allow inserting closing delimiters when accidentally unbalanced
   (my-bind :map 'smartparens-mode-map "C-M-SPC" #'sp-mark-sexp) ; for comments
   (my-bind :map 'smartparens-mode-map "M-K" #'sp-kill-sexp) ; to kill the sexp that contains the point
   (sp-local-pair sp-lisp-modes "'" "'" :when '(sp-in-string-p sp-in-comment-p)) ; Single quote "'" for 'Lisp(s)
   (sp-local-pair '(minibuffer-mode minibuffer-inactive-mode) "'" "'" :actions '(wrap navigate)) ; allow wrapping instead of complete removal
   (sp-pair "(" ")" :trigger "[" :trigger-wrap "[")
   (sp-pair "[" "]" :trigger "]" :trigger-wrap "]"))
;; ;; <div class="> 🙃
;; (with-eval-after-load 'smartparens
;;   (sp-local-pair my-html-tag-major-mode-list "\"" "\"" :unless '(sp-point-before-same-p))
;;   (sp-local-pair my-html-tag-major-mode-list "'" "'" :unless '(sp-point-before-same-p)))

(interactive-form 'self-insert-command)

;;;###autoload
(progn
  (defun my-wayland-p ()
    "Return if using a Wayland compositor.
XDG_SESSION_TYPE may not be known by the emacs systemd service."
    (or (equal "wayland" (getenv "XDG_SESSION_TYPE"))
        (string-search "wayland" (getenv "WAYLAND_DISPLAY"))
        (string-search "wayland" (getenv "DESKTOP_SESSION")))))

(dolist (file (directory-files default-directory nil "my-.*\\.el$"))
  (my-elisp-add-missing-provide-in-file file t))

(let* ((latex-command-in-list (assoc-string "latex" TeX-command-list 'case-fold))
       (options (nth 1 latex-command-in-list))
       (new-command-name "latex -shell-escape (for minted,...)")
       (new-command (append (list new-command-name
                                  (replace-regexp-in-string (rx (group string-start (* (not space))))
                                                            (concat "\\1" " -shell-escape")
                                                            options))
                            (nthcdr 2 latex-command-in-list))))
  (add-to-list 'TeX-command-list new-command)
  (setq-default TeX-command-default new-command-name))

;;;###autoload
(cl-defun my-recursive-map (func tree &optional (leaf-predicates '(stringp)))
  "Map FUNC recursively on TREE and try to preserve the its type structure.
For each node whose type matches one of LEAF-PREDICATES, apply
FUNC directly on it instead of descending. FUNC must be operable
on each leaf node.
Use `-tree-map-nodes'."
  (cond
   ;; tree matches one of  leaf-predicates
   ((-some (-cut funcall <> tree) leaf-predicates)
    (funcall func tree))
   ;; including `nil'
   ((proper-list-p tree)
    (-map (-cut my-recursive-map func <> leaf-predicates)
          tree))
   ((consp tree)
    (cons (my-recursive-map func (car tree) leaf-predicates)
          (my-recursive-map func (cdr tree) leaf-predicates)))
   ((seqp tree)
    (let ((type (type-of tree)))
      (--> (seq-map (-cut my-recursive-map func <> leaf-predicates)
                    tree)
           (if type
               (seq-into it type)
             it))))
   (t
    (funcall func tree))))

;;;###autoload
(defun my-ensured-added-option-latex-command (command option)
  "OPTION's beginning should be a space."
  (if (and (string-match-p (rx-to-string `(seq string-start
                                               (? "%")
                                               (or "latex" "pdflatex")
                                               " ")
                                         t)
                           command)
           (not (string-search option command)))
      (replace-regexp-in-string "^\\([^ ]+\\) "
                                (format "\\1%s " option)
                                command)
    command))

(defun my-preview-latex--clean-intermediate-files (in-dir)
  (async-start
   (lambda ()
     (dlet ((default-directory in-dir))
       (shell-command "latexmk -C")
       (dolist (file (directory-files in-dir nil directory-files-no-dot-files-regexp))
         (unless (file-exists-p (concat (file-name-base file) ".tex"))
           (delete-file file)))))))

(autoload 'org-latex-regexps "org")
;;;###autoload
(defun my-preview-latex-get-expression-at-point ()
  ;; (my-latex-math-expression-at-point)
  (save-match-data
    (-some (-lambda ((key regexp match-num . _))
             (let ((p (point)))
               (save-excursion
                 (and (re-search-backward regexp nil 'noerror)
                      (<= (match-beginning match-num) p (match-end match-num))
                      (match-string-no-properties match-num)))))
           org-latex-regexps)))

;; Lower "begin"'s priority
(let ((begin (assoc "begin" org-latex-regexps)))
  (when begin
    (setq org-latex-regexps
          `(,@(delete begin org-latex-regexps) ,begin))))

(--> (-map 'car lsp-language-id-configuration)
     (-filter 'functionp it)
     (-map 'my-get-parent-modes it)
     (-flatten it)
     (-uniq it))

(defun my-pair-bracket--count-back (char)
  (my-loop [p (point) count 0]
    (if (equal char (char-before p))
        (recur (1- p) (1+ count))
      count)))

(defun my-pair-bracket--insert-or-wrap (open-char close-char n)
  "Insert N OPEN-CHAR.
When:
- No active region
- N = 1
- A positive number (NB) of backslashes precede the point
, then 1 (odd NB) or 2 (even NB) will be inserted after the point ant before
CLOSE-CHAR."
  (if-let* ( (_ (not (use-region-p)))
             (_ (= 1 n))
             (backslash-count (my-pair-bracket--count-back ?\\))
             (_ (< 0 backslash-count))
             (backslash-string (concat "\\" (and (cl-evenp backslash-count) "\\"))))
      (progn
        (insert-char open-char)
        (save-excursion
          (insert (concat backslash-string (char-to-string close-char)))))
    (progn
      (setq last-command-event open-char)
      (self-insert-command n open-char)
      (setq this-command #'self-insert-command))))

;;;###autoload
(defun my-pair-bracket-round-parenthesis (n)
  "Insert N \"(\", with backslashes when needed.
See `my-pair-bracket--insert-or-wrap' for conditions to insert backslashes."
  (interactive "p")
  (my-pair-bracket--insert-or-wrap ?\( ?\) n))

;;;###autoload
(defun my-pair-bracket-square-bracket (n)
  "`my-pair-bracket-round-parenthesis' for \"[\"."
  (interactive "p")
  (my-pair-bracket--insert-or-wrap ?\[ ?\] n))

;;;###autoload
(defun my-pair-bracket-curly-brace (n)
  "`my-pair-bracket-round-parenthesis' for \"{\"."
  (interactive "p")
  (my-pair-bracket--insert-or-wrap ?\{ ?\} n))

;;;###autoload
(defmacro my-dlet-vars (vars &rest body)
  (declare (indent defun) (debug t))
  `(dlet ,(symbol-value vars)
     ,@body))

;; lispy workarounds
'(

  ;; Don't use keys with prefix
  (setq lispy-mode-map-base (make-sparse-keymap))
  (define-key lispy-mode-map-base (kbd ";") #'lispy-comment)

  (cl-loop for s being the symbols
           when (let ((name (symbol-name s)))
                  (and (string-match-p (format "^%s." 'lispy-mode-map) name)
                       (not (string-match-p
                             (format "\\(--\\|%s\\)" 'lispy-mode-map-base)
                             name))))
           collect s)

  (defconst my-lispy-map-list
    (append '(;; lispy-mode-map-base
              lispy-mode-map)
            (seq-map (lambda (theme) (intern (format "%s-%s" 'lispy-mode-map theme)))
                     lispy-key-theme)))
  (my-bind :map my-lispy-map-list
    '(
      "`"                               ; Auto-close for `highlight'
      "DEL" ; `lispy-delete-backward-or-splice-or-slurp' is incorrect when active region
      )
    nil) ;; Fix conflict with `outline-cycle' (when (member 'parinfer lispy-key-theme) (let ((filter-fn (lambda (cmd) (unless (my-point-in-comment-p) cmd)))) (my-bind :map my-lispy-map-list "TAB" `(menu-item "" lispy-indent-adjust-parens :filter ,filter-fn) "<backtab>" `(menu-item "" lispy-dedent-adjust-parens :filter ,filter-fn)))) ;;; (ly-raw newline)#autoload
  (defun my-lispy-space-ignore-when-edebug (arg)
    "Workaround `lispy-space' being no-op in `edebug-eval-expression'.
ARG is passed."
    (interactive "p")
    (dlet ((edebug-active nil))
      (lispy-space arg)))
  (let ((helper (lambda (cmd0 replacement)
                  ;; ;; Copy the original command's `interactive-form'
                  ;; (put replacement 'interactive-form (interactive-form cmd0))
                  (my-bind :map 'read-expression-map (vector 'remap cmd0) replacement))))
    ;; Fix no-op SPC in `eval-expression' while `edebug' is active
    (funcall helper #'lispy-space #'my-lispy-space-ignore-when-edebug)))

;; ;; handled `tree-sitter-mark-bigger-node'
;; (dolist (mode my-html-tag-major-mode-list)
;;   (add-hook (my-mode->hook mode) #'my-sgml-minor-mode)
;;   (with-eval-after-load 'expand-region
;;     (er/enable-mode-expansions mode #'er/add-html-mode-expansions) ; uses `sgml-get-context'
;;     (er/enable-mode-expansions mode #'my-er/add-html-tag-expansions)))

(defvar my-expanded-yasnippet--cache (make-hash-table :test 'equal))
;;;###autoload
(defun my-expanded-yasnippet (template &optional major-mode-env)
  (let* ((mode (or major-mode-env major-mode))
         (env-buffer-name (format " *%s*" 'my-expanded-yasnippet))
         (key (list template mode)))
    (if-let* ((expanded (gethash key my-expanded-yasnippet--cache)))
        expanded
      (progn
        ;; Re-use the buffer where expansions occur to prevent the overhead
        ;; applying modes
        (unless (get-buffer env-buffer-name)
          (get-buffer-create env-buffer-name)
          (with-current-buffer env-buffer-name
            (funcall mode)
            (yas-minor-mode)))
        (with-current-buffer env-buffer-name
          (delete-region (point-min) (point-max))
          (insert template)
          (yas-expand)
          ;; (yas-expand-snippet template (point) (point))
          (puthash key
                   (buffer-string)
                   my-expanded-yasnippet--cache))))))

;;;###autoload
(defun my-doom-disable-auto-revert (&optional global)
  (remove-hook 'focus-in-hook 'doom-auto-revert-buffers-h (not global))
  (remove-hook 'after-save-hook 'doom-auto-revert-buffers-h (not global))
  (remove-hook 'doom-switch-buffer-hook 'doom-auto-revert-buffer-h (not global))
  (remove-hook 'doom-switch-window-hook 'doom-auto-revert-buffer-h (not global)))

;;;###autoload
(defun my-puni-backward-delete-char (&optional num-arg raw-arg)
  "Like `puni-backward-delete-char', but splice emptyish sexps."
  (interactive "p\nP")
  (my-with-deferred-gc
   (-if-let* (((sexp-beg . sexp-end) (ignore-errors (puni-bounds-of-sexp-around-point)))
              ((list-beg . list-end) (ignore-errors (puni-bounds-of-list-around-point)))
              (_ (and (= (point) list-beg)
                      (= (1+ sexp-beg) list-beg)
                      (= (1- sexp-end) list-end)
                      (string-match-p "\\`[ \t\n\r]*\\'"
                                      (buffer-substring-no-properties list-beg list-end)))))
       (progn
         (puni-delete-region sexp-beg sexp-end)
         (when (< 1 num-arg)
           (puni-backward-delete-char (1- num-arg))))
     (puni-backward-delete-char num-arg))))

;; `my-pair' instead
(defun my-puni-mode-maybe ()
  ;; Using `puni-global-mode' then exclude many modes is not fine.
  ;; Using `prog-mode-hook' & friends, how about individual REPL modes?.
  (unless (or buffer-read-only
              ;; `find-file''s "M-backspace"
              minibuffer-completing-file-name)
    (puni-mode)))
(define-globalized-minor-mode my-puni-global-mode puni-mode my-puni-mode-maybe)
;; C-M-[ae], M-[()]: leave at the default; C-d: sometimes we need forced
;; deletion while <delete> doesn't do anything; C-k: another version globally
;; that maybe kill to end/beginning of current list; C-w: not much useful, it
;; can used for other special things; DEL: vanilla version can splice just
;; fine, also puni's parsing may be slow
(my-bind :map 'puni-mode-map '("C-M-a" "C-M-e" "C-d" "C-k" "C-w" "DEL" "M-(" "M-)") nil)

;; ;; find a suitable dictionary file
;; (my-filter-map-symbols (my-fn%
;;                         (and (boundp %1)
;;                              (string-match-p "^[^\\+].*dict"
;;                                              ;; "dict\\|spell"
;;                                              (symbol-name %1))
;;                              (or (null (symbol-value %1))
;;                                  (and (stringp (symbol-value %1))
;;                                       (string-match-p "/" (symbol-value %1))
;;                                       (not (string-match-p " " (symbol-value %1)))
;;                                       (f-file-p (symbol-value %1))))))
;;                        (my-fn% (cons %1 (symbol-value %1))))

;;;###autoload
(defun my-make-evil-mark-command (evil-func &optional no-shrink)
  "Produce an command that uses EVIL-FUNC to mark a region.
With non-nil no-shrink, when the returned region by EVIL-FUNC is
completely inside the original region, expand the region a bit
and try again."
  (let ((cmd (my-concat-symbols '@ 'my-mark evil-func
                                (if no-shrink 'no-shrink 'may-shrink))))
    (when (or (not (commandp cmd))
              debug-on-error)
      (defalias cmd
        (lambda (&optional arg beg0 end0)
          (interactive "p")
          (let ((beg0 (or beg0 (if (use-region-p) (region-beginning) (1- (point)))))
                (end0 (or end0 (if (use-region-p) (region-end) (point)))))
            ;; (funcall evil-func arg beg0 end0 'inclusive)
            (-let (((new-beg new-end _) (funcall evil-func arg beg0 end0)))
              (if (or (<= new-beg beg0)
                      (<= end0 new-end)
                      (not no-shrink))
                  (my-set-mark-from-to new-end new-beg)
                (funcall cmd arg (1- beg0) (1+ end0))))))))
    cmd))

`(web-mode-html-attr-name-face :foreground "yellow") ; unspecified :inherit 'font-lock-function-name-face
`(web-mode-html-attr-value-face :foreground unspecified :inherit 'font-lock-string-face) ;

(defun my-ein-auto-rename-notebook-buffer ()
  "Remove the unnecessary number in buffer name."
  (interactive)
  (-when-let* ((matches
                (s-match
                 "\\`\\(.*\\*\\)<[0-9]+>\\(\\[.*\\]\\)?[^*]*\\'"
                 (buffer-name))))
    (-let* (((_ main lang) matches)
            (new-name (concat main lang)))
      (unless (get-buffer new-name)
        (rename-buffer new-name)))))

;;;###autoload
(progn

  (defvar my-ein-notify-auto-notebook-mode-modeline-map
    (let ((map (make-sparse-keymap)))
      (define-key map [mode-line down-mouse-1] 'my-ein-auto-open-notebook)
      map))

  (add-to-list 'mode-line-misc-info
               '(my-ein-notify-auto-notebook-mode
                 ("" my-ein-notify-auto-notebook-var)))

  (define-minor-mode my-ein-notify-auto-notebook-mode nil
    :global nil
    ;; :lighter (:eval (my-ein-notify-auto-notebook-func))
    ;; :lighter my-ein-notify-auto-notebook-var
    (if my-ein-notify-auto-notebook-mode
        (progn
          (make-local-variable 'global-mode-string)
          (add-to-list 'global-mode-string
                       '(t (:eval (my-ein-notify-auto-notebook-func)))))
      (progn
        (kill-local-variable 'global-mode-string))))

  (defun my-ein-notify-auto-notebook-func ()
    (--> (propertize "Notebook 📔"
                     'help-echo "Open as a Jupyter Notebook"
                     'mouse-face 'mode-line-highlight
                     'local-map my-ein-notify-auto-notebook-mode-modeline-map)
         (format " %s " it)))

  (defvar my-ein-notify-auto-notebook-var
    `(" "
      (:propertize "Notebook 📔"
                   help-echo "Open as a Jupyter Notebook"
                   mouse-face mode-line-highlight
                   local-map ,my-ein-notify-auto-notebook-mode-modeline-map)
      " "))

  ;;
  )

(setq highlight-indent-guides-auto-odd-face-perc 25 highlight-indent-guides-auto-even-face-perc 12.5)

;;;###autoload
(defun my-remove-unimportant-projects ()
  (project-forget-projects-under user-emacs-directory 'recursive))

;;;###autoload
(defmacro my-let (bindings &rest expressions)
  "(`let*' BINDINGS EXPRESSIONS) with `cl-destructuring-bind' for each of BINDINGS."
  (declare (indent defun) (debug t))
  (seq-reduce
   (lambda (prev args-expr)
     `(cl-destructuring-bind
          ,@args-expr
          ,prev))
   (nreverse bindings)
   `(progn ,@expressions)))

;;;###autoload
(defun my-compose (&rest functions)
  "Compose FUNCTIONS \\'(f0 f1 ... func) into (f0 ( f1 ( ... (func)))).
All but the last of FUNCTIONS must be unary or variadic (able to
take only one argument). When FUNCTIONS is empty, return a
variadic `identity'."
  (declare (pure t) (side-effect-free t))
  (lambda (&rest args)
    (seq-reduce (lambda (l-arg func)
                  (funcall func l-arg))
                (nreverse (butlast functions))
                (if functions
                    (apply (car (last functions)) args)
                  args))))
(defalias #'my-comp #'my-compose)

;;;###autoload
(defun my-juxt (&rest fs)
  "Return a function that the is the juxtaposition of FS.
It accepts a variable number of args and returnss a sequence
containing the result of applying each of FS to the args."
  (declare (pure t) (side-effect-free t))
  (lambda (&rest args)
    (seq-map
     (lambda (f)
       (apply f args))
     fs)))

;;;###autoload
(defun my-replace-all-with~/-maybe ()
  (interactive)
  (if (and (equal (syntax-table) minibuffer-local-filename-syntax)
           (eobp)
           (equal ?/ (char-before)))
      (my-replace-buffer-string "~/")
    (call-interactively #'self-insert-command)))

;; Allow passing "-S" to `env' for passing arguments into the interpreter
(setq auto-mode-interpreter-regexp
      "#![ \t]?\\([^ \t\n\r]*/bin/env[ \t]\\(?:-S \\)?\\)?\\([^ \t\n\r]+\\)")

(advice-add #'pixel-scroll-precision
            :around
            (lambda (fn &rest args)
              (my-with-advice #'doom-modeline-format--main :override #'ignore
                (apply fn args))))

;;;###autoload
(defun my-outline-minor-mode-may-ignore-a (fn &optional arg)
  "Workaround https://debbugs.gnu.org/cgi/bugreport.cgi?bug=57176
Headings are scaled multiple times."
  (cond ((and
          ;; active
          outline-minor-mode
          ;; customized
          outline-minor-mode-highlight
          ;; not disable
          (not (or (equal arg 'toggle)
                   (and (numberp arg)
                        (<= arg 0)))))
         ;; skip, don't re-apply
         outline-minor-mode)
        (t
         (funcall fn arg))))

(define-key minibuffer-mode-map (kbd "C-S-SPC") #'my-vertico-preview-mode)
(define-minor-mode my-vertico-preview-mode nil
  :local t
  :keymap
  `((,(kbd "<up>") . (lambda (n) (interactive "p") (vertico-previous n) (my-minibuffer-preview-call-no-quit)))
    (,(kbd "<down>") . (lambda (n) (interactive "p") (vertico-next n) (my-minibuffer-preview-call-no-quit)))))

;;;###autoload
(defun my-safe-functions (&rest functions)
  (lambda ()
    (dolist (func functions)
      (when (functionp func)
        (funcall func)))))

;;;###autoload
(defun my-evil-next-insert-once (&rest _)
  (when (fboundp #'evil-mode)
    (-let* ((evil-default-state0 evil-default-state))
      (setq-default evil-default-state 'insert)
      (my-add-hook-once 'evil-local-mode-hook
        (lambda ()
          (setq-default evil-default-state evil-default-state0))))))

;; Don't split to show the splash screen
(when (-find #'file-regular-p (cdr command-line-args))
  (setq inhibit-splash-screen t))

;;;###autoload
(defun my-list-doom-dark-themes-with-brightness (&optional except-regex except-themes)
  (let ((current-theme (my-at custom-enabled-themes -1))
        (custom-safe-themes t)
        (except-regex
         (or except-regex (rx
                           (or
                            "dark+"
                            "gruvbox"
                            "molokai"
                            "monokai"
                            "solarized"
                            "zenburn"
                            (regexp "doom-plain$")
                            (regexp "tomorrow-day$")
                            ;;
                            )))))
    (prog1
        (cl-sort
         (seq-remove
          #'null
          (cl-loop for theme in (custom-available-themes)
                   when (and (string-match-p "doom" (symbol-name theme))
                             (not (string-match-p "\\(light$\\|white\\)" (symbol-name theme)))
                             (not (string-match-p except-regex (symbol-name theme))))
                   collect (progn (load-theme theme)
                                  (let ((background (face-attribute 'default :background nil nil)))
                                    (list theme (/ (apply '+ (color-name-to-rgb background)) 3) background)))))
         #'<
         :key #'cadr)
      (load-theme current-theme))))

;;;###autoload
(defun my-sort-sexps (reverse beg end)
  "Don't use this! Doesn't work now!"
  (declare (pre-alpha t))
  (interactive "P\nr")
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (let ((inhibit-field-text-motion t))
        (sort-subr
         reverse
         (lambda ()
           (-let* (((beg . end) (bounds-of-thing-at-point 'sexp)))
             (if (and beg end
                      (<= beg (point) end))
                 (forward-sexp 2)
               (forward-sexp 1))
             (backward-sexp 1)))
         (lambda ()
           (goto-char (cdr (bounds-of-thing-at-point 'sexp)))))))))

(my-custom-set-faces '(writegood-passive-voice-face :underline unspecified))
;; disable passive voice detection
(advice-add #'writegood-passive-voice-font-lock-keywords-regexp :override (-const "\\`\\'"))

;; doesn't show for the initial call
'(when (fboundp #'anzu-mode)
   (my-add-hook-once 'isearch-mode-hook #'global-anzu-mode))

;;;###autoload
(defun my-backward-delete-char-or-splice (&optional num-arg raw-arg)
  (interactive "p\nP")
  (cond
   ((and (not (use-region-p))
         (let* ((cb (char-before)))
           (and cb
                (null raw-arg)
                (-let* (((syntax pair _ str|cmt-beg) (electric-pair-syntax-info cb)))
                  (and pair (not str|cmt-beg)
                       (equal (car (puni-bounds-of-sexp-around-point))
                              (1- (point)))
                       (progn
                         (my-with-advice #'pulse-momentary-highlight-region :override #'ignore
                           (puni-splice))
                         t)))))))
   (t
    (backward-delete-char-untabify num-arg))))

;;;###autoload
(defun my-pair-open-newline-between-multicha-pairs-h ()
  (when (and (eq last-command-event ?\n)
             (< (1+ (point-min)) (point) (point-max))
             (my-pair-find-multi-char-paren (- (point) 1)))
    (save-excursion (newline 1))))
;;;###autoload
(defun my-pair-find-multi-char-paren (&optional open-end-pt clos-beg-pt)
  "Use inside a hook of `post-self-insert-hook'.
Unfortunately not performant enough."
  (-let* ((pt (point))
          (open-end-pt (or open-end-pt pt))
          (clos-beg-pt (or clos-beg-pt pt)))
    (-some
     (-lambda ((mode . pairs))
       (-some
        (-lambda ((open . clos))
          (-when-let* ((clos-end-pt (+ clos-beg-pt (length clos)))
                       (open-beg-pt (- open-end-pt (length open)))
                       (found-clos (equal (ignore-error args-out-of-range
                                            (buffer-substring clos-beg-pt clos-end-pt))
                                          clos)))
            (and found-clos
                 (or
                  ;; for markdown's ```lang|```, just having the close is enough
                  (derived-mode-p mode)
                  (equal (ignore-error args-out-of-range
                           (buffer-substring open-beg-pt open-end-pt))
                         open)))))
        pairs))
     my-pair-mode-pair-alist)))

;; ;; workaround: `(ns)' causes `clojure-find-ns' -> ... ->
;; ;; `clojure-forward-logical-sexp' -> `forward-sexp' -> `scan-sexp' to throw
;; ;; `scan-error'
;; (my-demote-errors-of-while-executing #'forward-sexp #'clojure-find-ns)

;; (add-hook 'cider-mode-hook
;;           (lambda ()
;;             (when (boundp 'lispy-compat)
;;               (make-local-variable 'lispy-compat)
;;               (cl-pushnew 'cider lispy-compat))))

;; (with-eval-after-load 'inf-clojure
;;   ;; `inf-clojure-repl-features' isn't configured for `lein'
;;   ;; with "clojure" as the command, the prompt is buggy
;;   (setq inf-clojure-custom-startup "clojure"))

;;;###autoload
(defun my-regexp-matched-substrings (regexp str)
  "Use `s-match' REGEXP STR."
  (declare (pure t) (side-effect-free t))
  (save-match-data
    (if (string-match regexp str)
        (cl-loop for (beg end) on (match-data) by #'cddr
                 when (and beg end)
                 collect (substring str beg end))
      nil)))
(defun my-shell-command-switch-added (switch &optional from)
  "SWITCH must begin with \"-\"."
  (let ((lone-dash (rx (seq (group-n 1 (or string-start (not "-")) "-")
                            (not "-"))))
        (from (or from shell-command-switch)))
    (string-trim
     (if (string-match-p lone-dash from)
         (replace-regexp-in-string lone-dash switch from
                                   nil nil 1)
       (concat from " " switch)))))

;;;###autoload
(defun my-after-evil-emrbace-a (func &rest args)
  (with-eval-after-load 'evil-embrace
    (apply func args)))
(my-add-advice/s (cl-loop for sym being the symbols
                          when (string-match-p "^\\+evil-embrace-.*-h$" (symbol-name sym))
                          collect sym)
  :around #'my-after-evil-emrbace-a)

(defun my-consult--outline-candidates-a (fn &rest args)
  (-let* ((retval (apply fn args))
          (height (face-attribute 'default :height)))
    (my-for [heading retval]
      (my-stylize-string heading :height height))))
(advice-add #'consult--outline-candidates :around #'my-consult--outline-candidates-a)

(add-hook 'lsp-managed-mode-hook
          (my-defn my-lsp-managed-mode-h ()
            (remove-function (local 'eldoc-documentation-function) #'lsp-eldoc-function)
            (add-hook 'eldoc-documentation-functions (lambda (_) (lsp-eldoc-function))
                      nil 'local)))

;;;###autoload
(defun my-require-compat-maybe ()
  "Just use `require,' other packages would load it anyway."
  (let* ((compat-dir (concat my-useelisp-repositories-dir/ "compat"))
         (version-files (directory-files compat-dir nil "compat-[0-9]+\\.el$"))
         (compat-emacs-versions (mapcar (lambda (version-file)
                                          (my-remove-str-fixes
                                           "compat-" ""
                                           (file-name-base version-file)))
                                        version-files)))
    (when (cl-some (lambda (ver)
                     (version< emacs-version ver))
                   compat-emacs-versions)
      (require 'compat))))

;;;###autoload
(defun my-lsp-execute-code-action (&optional line-diff)
  "`lsp-execute-code-action' at LINE-DIFF lines from current point."
  (interactive "p")
  (when (and (require 'lsp)
             (bound-and-true-p lsp-mode)
             (lsp-feature? "textDocument/codeAction"))
    (save-excursion
      (unless (zerop line-diff) (forward-line line-diff))
      (let ((action (when (bound-and-true-p lsp-mode)
                      (lsp-code-actions-at-point))))
        (when action (call-interactively 'lsp-execute-code-action))
        action))))

;;;###autoload
(defun my-lsp-execute-code-action-around ()
  "Execute `lsp''s code action on one of current, previous, or next line.
Where the first suggestion is found."
  (interactive)
  (or (my-lsp-execute-code-action 0)
      (my-lsp-execute-code-action -1)
      (my-lsp-execute-code-action 1)))

(setq sp-ignore-modes-list nil)         ; Enable in mini-buffers

(with-eval-after-load 'xref
  (when (and (boundp 'xref-search-program-alist)
             (executable-find "rg"))
    (setq xref-search-program 'ripgrep)))

(add-hook 'warning-suppress-types '(comp)) ; don't popup warnings, especially for packages

(defun my-turn-on-outline-minor-highlight-mode ()
  (cond ((and (my-should-outline-comment-mode-p)
              ;; Not exist a match that is not a comment?
              (not (and (bound-and-true-p outline-regexp)
                        (string-match-p outline-regexp " "))))
         (my-outline-minor-highlight-mode))))

(defun my-define-minor-mode--optional-arguments/28-a (func &rest args)
  (if (keywordp (car args))
      (apply func
             (plist-get :init-value args)
             (plist-get :lighter args)
             (plist-get :keymap args)
             args)
    (apply func args)))
(advice-add #'define-minor-mode :around #'my-define-minor-mode--optional-arguments/28-a)

;;;###autoload
(defun my-doom-package!-ignore-error-a (func &rest args)
  (condition-case err
      (apply func args)
    ('doom-module-error
     (dlet ((inhibit-message t))
       (message "%S" (error-message-string err))))))

;;;###autoload
(defun my-lsp-maybe (&rest _)
  (interactive)
  (when (-some (-lambda ((mode-or-pattern . language))
                 (or (and buffer-file-name
                          (stringp mode-or-pattern)
                          (string-match-p mode-or-pattern buffer-file-name))
                     (derived-mode-p mode-or-pattern)))
               lsp-language-id-configuration)
    (lsp-deferred)))

;; reduce contrast -> make comments harder to read
(set (intern (format "%s-brighter-comments" doom-theme)) t)

(when (fboundp #'anzu-mode) (my-add-advice-once #'isearch-mode :before (lambda (&rest _) (global-anzu-mode))))

(defun my-lsp-mode-setup-completion ()
  (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
        ;; no sorting when use `orderless'?? (maybe because the built-in flex
        ;; style performs sorting but orderless doesn't)
        `(flex ,@completion-styles)))

(defmacro my-undoom-flag! (category module del-flags)
  `(when (boundp 'doom-modules)
     (-let* ((key '(,category . ,module))
             (old-plist (gethash key doom-modules))
             (old-flags (plist-get old-plist :flags))
             (new-flags (-difference old-flags ',del-flags)))
       (puthash key (plist-put old-plist :flags new-flags) doom-modules))))

;; for `compile', completions include aliases but not when executed, workaround
;; until disabling aliases completions for them is possible
(my-add-advice/s
  '(compile recompile compilation-revert-buffer)
  :around #'my-with-interactive-sh-a)

;; major modes supported by `eglot'
(-->
 eglot-server-programs
 (-map #'car it)
 (-flatten it)
 -uniq
 (-filter (-andfn #'symbolp (-not #'keywordp)) it)
 (sort it (lambda (s0 s1)
            (string< (symbol-name s0)
                     (symbol-name s1)))))

;;;###autoload
(defun my-set-symbol-documentation|docstring (symbol variable? new-doc-fn)
  "Set SYMBOL's doc to (NEW-DOC-FN <SYMBOL's current doc>).
Non-nil VARIABLE? means SYMBOL is a variable."
  (declare (indent defun))
  (-let* ((prop
           (if variable?
               'variable-documentation
             'function-documentation))
          (old-doc
           (if variable?
               (documentation-property symbol 'variable-documentation)
             (my-get-function-documentation symbol))))
    (put symbol prop (funcall new-doc-fn old-doc))))

;; profiler between pre and post the first command
(letrec ((fn0 (lambda () (profiler-start 'cpu))))
  (add-hook 'pre-command-hook fn0)
  (letrec ((fn1
            (lambda ()
              (remove-hook 'pre-command-hook fn0)
              (remove-hook 'post-command-hook fn1)
              (profiler-stop)
              (save-window-excursion (profiler-report)))))
    (add-hook 'post-command-hook fn1)))

(my-add-hook-once
  'pre-command-hook
  (lambda ()
    (profiler-start 'cpu)
    (my-add-hook-once
      'post-command-hook
      (lambda ()
        (profiler-stop)
        (save-window-excursion (profiler-report))))))

(my-bind
  :map
  '(global-map
    emacs-lisp-mode-map
    lisp-interaction-mode-map
    ielm-map
    read-expression-map
    bibtex-mode-map)
  "ESC TAB" #'my-complete-in-minibuffer)

(add-hook
 'my-override-keymap-alist
 `(t
   .
   ,(define-keymap
      "ESC TAB"
      `(menu-item
        ""
        my-complete-in-minibuffer
        :filter my-kmi-filter-not-read-only))))

(-->
 (my-lookup-key-in-all-maps "SPC")
 (-map #'cdr it)
 (-filter 'symbolp it)
 (-frequencies it)
 (cl-sort it '< :key 'cdr))

;;;###autoload
(defun my-gen-project-buffer-name (what &optional path no-asterisk)
  "Return \"*WHAT project-name PATH*\".
NO-ASTERISK."
  (let* ((project-path (abbreviate-file-name
                        (or path (my-project-root) default-directory)))
         (project-name (my-project?-name project-path)))
    (s-wrap
     (format "%s %s %s" what project-name project-path)
     (if no-asterisk "" "*"))))

(unless no-retry
  (my-add-hook-once
    'window-buffer-change-functions
    (lambda (&optional _) (my-evil-or-leader-maybe t))
    nil
    'local))

;;;###autoload
(defun my-leader|SPC-SPC-to-scroll (keymap-symbol/s &optional scroll-command)
  (my-bind
    :map
    keymap-symbol/s
    (vector 'remap (my-lookup-key "SPC" my-leader-spc-map))
    (or scroll-command #'scroll-up-command)))

;;;###autoload
(defun my-+emacs-lisp-extend-imenu-h-after-a (&rest _)
  (my-delete-alist! 'imenu-generic-expression "Section"))
(advice-add #'+emacs-lisp-extend-imenu-h :after #'my-+emacs-lisp-extend-imenu-h-after-a)

(defun my-doom-elisp-imenu-keep-others-a (func &rest args)
  (let ((v0 imenu-generic-expression))
    (prog1 (apply func args)
      (setq-local imenu-generic-expression
                  (seq-union
                   imenu-generic-expression
                   ;; avoid duplications
                   (seq-remove (lambda (l) (null (car l))) v0))))))
(advice-add #'+emacs-lisp-extend-imenu-h :around #'my-doom-elisp-imenu-keep-others-a)

(defun starhugger--overlay-modification-h
    (ov after? change-beg change-end &optional old-len)
  (when (and starhugger-dismiss-suggestion-after-change
             after?
             (not (starhugger--suggestion-accepted-partially)))
    (starhugger-dismiss-suggestion)))

(defun my-starhugger--prompt-build-components ()
  "Prompt components builder.
A function that takes no arguments and returns a vector of 2
strings: before point (required) and after point (this can be
nil, then fill-in-the-middle mode won't be use).

`starhugger-fill-tokens' will be inserted so that the final
prompt becomes: <token><before point><token><after point><token>.")

(defun my-leader-mode-menu-item-filter-SPC (cmd)
  (and
   (-let* ((native-cmd
            (my-leader-mode-menu-item-filter-get-native-SPC-command)))
     ;; (string-match-p "\\<\\(scroll\\|line\\)\\>" (format "%s" native-cmd))
     (or buffer-read-only
         (not (member native-cmd '(self-insert-command)))
         (member (get-text-property (point) 'read-only) '(t))))
   cmd))

(defvar-local smerge-mode nil)
;;;###autoload
(defun my-rainbow-delimiters-mode-maybe ()
  (when (and (not
              (derived-mode-p
               'autoconf-mode ;
               'makefile-mode ;
               )))
    (-let* ((inner-h
             (lambda ()
               (when (not smerge-mode)
                 (rainbow-delimiters-mode)))))
      (cond
       (buffer-file-name
        (add-hook 'find-file-hook inner-h 96 t))
       (t
        (funcall inner-h))))))


;;;###autoload
(defun my-inspector-pprint-inspected-object--protect-q-key-a (func &rest args)
  (-let* ((orig-cmd (keymap-lookup emacs-lisp-mode-map "q")))
    (unwind-protect
        (apply func args)
      (-let* ((new-cmd (keymap-lookup emacs-lisp-mode-map "q")))
        (when (not (equal orig-cmd new-cmd))
          (message
           "`inspector-pprint-inspected-object' changed the key binding of `q' in `emacs-lisp-mode-map' to `%s', reverting to `%s'."
           new-cmd orig-cmd)
          (keymap-set emacs-lisp-mode-map "q" orig-cmd))))))
;; https://github.com/mmontone/emacs-inspector/issues/28 is solved
(advice-add
 #'inspector-pprint-inspected-object
 :around #'my-inspector-pprint-inspected-object--protect-q-key-a)

;;; Lazy expansions for er/ expand-region


(defvar my-expand-region-lazy-table--time-flag nil)

;;;###autoload
(progn

  (defvar my-expand-region-lazy-table '()
    "Duplicate keys are also aggregated.
See `my-expand-region-lazy-before-a'.")

  (defun my-expand-region-lazy-register (modes functions)
    "In MODES, add FUNCTIONS to `er/try-expand-list'.
Doesn't use a hook but lazily apply before calling the
command (typically `er/expand-region') so the real functions may
not be observable before calling
`my-expand-region-lazy-before-a'. See
`my-expand-region-lazy-table' for the expand strategies to be
added."
    (declare (indent defun))
    (dolist (mode modes)
      (add-to-list 'my-expand-region-lazy-table (cons mode functions)))
    (setq-default my-expand-region-lazy-table--time-flag (current-time)))

  (define-minor-mode my-expand-region-lazy-add-mode
    "See `my-expand-region-lazy-table'."
    :global
    t
    (if my-expand-region-lazy-add-mode
        (progn
          (advice-add
           #'er/expand-region
           :before #'my-expand-region-lazy-before-a))
      (progn
        (advice-remove #'er/expand-region #'my-expand-region-lazy-before-a))))

  ;;
  )

;;;###autoload
(defun my-expand-region-lazy-add-now (&optional notify)
  (interactive (list t))
  (-let* ((functions (my-get-alist-all-by-mode my-expand-region-lazy-table))
          (new-expanders (-difference functions er/try-expand-list)))
    (setq-local er/try-expand-list (append er/try-expand-list new-expanders))
    (dlet ((inhibit-message (or inhibit-message (not notify))))
      (message "`my-expand-region-lazy-add-now': in %s added %s"
               (buffer-name)
               new-expanders)))
  er/try-expand-list)

;;;###autoload
(defun my-expand-region-lazy-before-a (&rest _)
  "Make `my-expand-region-lazy-register' calls effective."
  ;; only perform when the current buffer's locally saved time stamp is older
  ;; than the globally modified table, or no saved local time stamp
  (when (or (not (local-variable-p 'my-expand-region-lazy-table--time-flag))
            (time-less-p
             (buffer-local-value
              'my-expand-region-lazy-table--time-flag (current-buffer))
             (default-value 'my-expand-region-lazy-table--time-flag)))
    (prog1 (my-expand-region-lazy-add-now)
      (setq-local my-expand-region-lazy-table--time-flag (current-time)))))

;; (my-elisp-add-doc-to-variable-maybe
;;  'er/try-expand-list
;;  "\n\nWhen `my-expand-region-lazy-add-mode' is active, before calling,
;; `my-expand-region-lazy-table''s appropriate functions are also
;; added to this.

;; Call `er/expand-region' once (in the desired buffer) then
;; re-inspect this.")

;; (my-elisp-add-doc-to-function-maybe
;;  #'er/expand-region "\n\nSee `er/try-expand-list'.")

;; (my-expand-region-lazy-add-mode)

;;; ?

;;;###autoload
(defun my-mc-keyboard-quit-from-evil-esc-h ()
  (when (and (bound-and-true-p evil-local-mode)
             (equal
              last-command (keymap-lookup evil-normal-state-map "<escape>"))
             multiple-cursors-mode)
    (mc/keyboard-quit)))

;; https://github.com/minad/jinx/issues/93
(defvar-local my-jinx-catch-not-unicode-error-a--flag nil)
;;;###autoload
(defun my-jinx-catch-not-unicode-error-a (func &rest args)
  (condition-case err
      (apply func args)
    (wrong-type-argument
     (-let* ((pred (cadr err)))
       (cond
        (my-jinx-catch-not-unicode-error-a--flag
         nil)
        ((equal pred 'unicode-string-p)
         (setq my-jinx-catch-not-unicode-error-a--flag t)
         (message "`jinx' error: %S" err))
        (t
         (signal (car err) (cdr err))))))))

(defvar-local my-rainbow-mode-toggle-no-hl-line--state nil)
(defvar-local rainbow-mode nil)
;;;###autoload
(defun my-rainbow-mode-toggle-no-hl-line ()
  (interactive)
  (if rainbow-mode
      (progn
        (when my-rainbow-mode-toggle-no-hl-line--state
          (hl-line-mode 1))
        (rainbow-mode 0))
    (progn
      (setq my-rainbow-mode-toggle-no-hl-line--state hl-line-mode)
      (when my-rainbow-mode-toggle-no-hl-line--state
        (hl-line-mode 0))
      (rainbow-mode 1))))

(defvar my-user-profile-local-file-path--made nil)
;;;###autoload
(defun my-user-profile-local-file-path (&rest components)
  (-let* ((profile-local-dir (file-name-concat
                              my-emacs-conf-dir/ ".local"
                              (replace-regexp-in-string
                               "/+" "_"
                               (directory-file-name user-emacs-directory))))
          (parent-dir (apply #'file-name-concat
                             profile-local-dir
                             (butlast components))))
    (unless my-user-profile-local-file-path--made
      (make-directory profile-local-dir 'parents)
      (setq my-user-profile-local-file-path--made t))
    (file-name-concat parent-dir (-last-item components))))

;; configure `projectile'

(with-eval-after-load 'project
  (cond
   ((fboundp #'projectile-mode)
    (autoload #'project-projectile "projectile")
    (add-to-ordered-list 'project-find-functions #'project-projectile 0))
   (t
    (add-to-ordered-list 'project-find-functions #'my-project-bottom-up-root-fn
                         0))))

(with-eval-after-load 'projectile
  (setq-default projectile-require-project-root nil) ;; Avoid superfluous prompts
  (setq-default projectile-enable-caching nil) ;; May quickly become outdated
  (setq-default projectile-dirconfig-file ".project") ;; Don't be so special
  (add-to-list 'projectile-project-root-files-bottom-up ".project"))


;;;###autoload
(defun my-lang-python--pyimport-insert-missing-open-template-around-a (func &rest args)
  (if (called-interactively-p 'any)
      (my-eval-until-no-error
       (my-apply-interactively func args)
       (progn
         (my-find-file-read-only-no-select-in-hidden-buffer
          (file-name-concat my-lisp-dir/ "lang" "python-imports.py"))
         (my-apply-interactively func args))
       (when (fboundp #'importmagic-mode)
         (importmagic-mode)
         (importmagic-fix-symbol-at-point)))
    (my-apply-interactively func args)))
(advice-add
 #'pyimport-insert-missing
 :around #'my-lang-python--pyimport-insert-missing-open-template-around-a)


;; there are various functions that calls `project-current' ALOT!, since its
;; running time isn't instantaneous and there shouldn't be a frequent reason to
;; switch project root for the current file and while without refreshing, expect
;; it to be applied instantly, let's cache it
(defvar-local my-project-current-cache-a--cache '())
;;;###autoload
(defun my-project-current-cache-a (func &rest args)
  (with-memoization (alist-get args my-project-current-cache-a--cache
                               nil nil #'equal)
    (apply func args)))

;;;###autoload
(defun my-kill-local-variable-with-matching-name (sym regexp)
  (-let* ((val (symbol-value sym))
          (sym-local-name
           (and (local-variable-p sym) val (symbolp val) (symbol-name val))))
    (when (and sym-local-name (string-match-p regexp sym-local-name))
      (kill-local-variable sym))))

;;;###autoload
(defun my-project-el-current-object-backend (&optional project-obj)
  (nth 1 (or project-obj (project-current))))

;;;###autoload
(defun my-eglot--keep-trying-code-action (&optional timeout)
  (-let* ((timeout (or timeout (/ 1.0 32)))
          (buf (current-buffer)))
    (letrec ((fn
              (lambda ()
                (with-current-buffer buf
                  (if (with-timeout (timeout nil)
                        (flymake-diagnostics)
                        t)
                      (progn
                        (call-interactively #'eglot-code-actions))
                    (run-at-time timeout nil fn))))))
      (funcall fn))))

(when (boundp '+doom-dashboard-functions)
  (setq +doom-dashboard-functions
        (-splice-list
         (lambda (fn) (equal #'doom-dashboard-widget-banner fn))
         (list
          #'doom-dashboard-widget-banner #'my-doom-edit-dashboard-widget-banner)
         +doom-dashboard-functions)))

(with-eval-after-load 'dash-docs
  (unless (dash-docs-docset-installed-p "Java")
    (let* ((se-lastest-version
            (cl-loop
             for docset in (dash-docs-official-docsets)
             when (string-match-p "java_se[0-9]+" docset)
             maximize (string-to-number
                       (substring docset (string-match "[0-9]+" docset)))))
           (java-se (concat "Java_SE" (number-to-string se-lastest-version))))
      ;; The installable choice has version prepended but the offline downloaded doesn't
      (when (y-or-n-p "Install Java's latest docset?")
        (dash-docs-ensure-docset-installed java-se))))
  (my-dash-docs-add-local 'java-mode "Java"))

;; ;;;###autoload
;; (define-overloadable-function my-buffer-filename-function () ""
;;   (funcall (mode-local-symbol 'my-buffer-filename-function)))
;;;###autoload
(defun my-copy-buffer-filename (&optional arg)
  "Copy (`my-buffer-filename') or `default-directory'.
- ARG = -1: apply `file-name-parent-directory'
- ARG = '-: apply `file-name-base'
- ARG = 0: apply `file-name-nondirectory'
- ARG = 1: apply `my-file-symlink-target-1'
- ARG = '(4): copy buffer name instead
- Non-nil ARG: apply `file-truename'"
  (interactive "P")
  (-when-let* ((file
                (-some-->
                    (my-buffer-filename t)
                  (s-chop-prefixes '("/sudo:root@localhost:") it)
                  directory-file-name
                  abbreviate-file-name
                  (cond
                   ((equal arg -1)
                    (file-name-parent-directory it))
                   ((equal arg '-)
                    (file-name-base it))
                   ((equal arg 0)
                    (file-name-nondirectory it))
                   ((equal arg 1)
                    (my-file-symlink-target-1 it))
                   ((equal arg '(4))
                    (buffer-name))
                   (arg
                    (file-truename it))
                   (t
                    it)))))
    (kill-new file)
    (message "Copied: %s" file)
    file))

(advice-add
 #'highlight-indent-guides-mode
 :around #'my-highlight-indent-guide-filter-a)
(setq
 highlight-indent-guides-method 'character
 highlight-indent-guides-auto-character-face-perc 25) ; balance between light and dark themes
(with-eval-after-load 'highlight-indent-guides
  (if (daemonp)
      (highlight-indent-guides-auto-set-faces)
    (my-add-hook-once
      'my-refresh-find|open-current-file-after-hook
      #'highlight-indent-guides-auto-set-faces)))

;; (when (fboundp #'smart-jump-go)
;;   (my-bind "M-." #'smart-jump-go)
;;   (my-bind "M-," #'smart-jump-back)
;;   (my-bind "M-?" #'smart-jump-references))
;; (with-eval-after-load 'smart-jump
;;   (dlet ((smart-jump-registered-p nil))
;;     (smart-jump-setup-default-registers)))


;; the crux version is better when when region spans multiple lines
;; but ends before EOL, crux puts the new text on their own lines,
;; while `duplicate-dwim''s new text is just next to the region end on
;; the same line

;;;###autoload
(defun my-duplicate-line-or-region-down (arg)
  (interactive "p")
  (-let* ((rbeg (region-beginning))
          (rend (region-end))
          (region-cover-lines-flag
           (and (use-region-p)
                (save-excursion
                  (goto-char rbeg)
                  (or (bolp) (eolp)))
                (save-excursion
                  (goto-char rend)
                  (or (bolp) (eolp)))))
          (worker
           (cond
            ((and (use-region-p) (not region-cover-lines-flag))
             (my-1st-fn
              #'duplicate-dwim
              #'crux-duplicate-current-line-or-region))
            (:else
             (my-1st-fn
              #'crux-duplicate-current-line-or-region
              #'duplicate-dwim)))))
    (prog1 (funcall worker arg))))

;;;###autoload
(defun my-duplicate-line-or-region-up (arg)
  (interactive "p")
  (save-excursion (my-duplicate-line-or-region-down arg)))

;; defer like Doom does
(when (fboundp #'envrc-global-mode)
  (my-add-hook-once
    '(find-file-hook dired-initial-position-hook) #'envrc-global-mode))

(set-popup-rule! "\\`\\*\\(v?term\\|terminal\\)\\( [^*]+\\)?\\*" :height 0.4 :ttl nil :quit nil)
(set-popup-rule! "\\`\\*eldoc\\( for [^*]+\\)?\\*" :modeline nil :height 1)

(with-eval-after-load 'doom-modeline
  (doom-modeline-def-modeline 'my-doom-modeline-no nil)
  (add-hook 'doom-modeline-mode-alist '(treemacs-mode .  my-doom-modeline-no)))

(my-add-advice/s
  #'(delete-other-windows split-window-below split-window-right delete-window)
  :around #'my-window-ask-to-try-again-with-ignore-window-parameters-interactive-a)

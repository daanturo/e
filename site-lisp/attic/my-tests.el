;; -*- lexical-binding: t; no-byte-compile: t; -*-

(error "Don't evalute this whole file!")

(global-set-key
 (kbd "<pause>")
 (lambda (beg end)
   (interactive "r")
   (message "%S" (list beg end))))

(my-for [x [0 1 2 3 4 5]
           :let [y (* x 3)]
           :when (cl-evenp y)]
  y)

(my-for--process-modifier '(:let [y (* x 3)]))

(cl-loop for x in-ref [0 1 2 3 4 5]
         for y = (* x 3)
         when (cl-evenp y)
         collect y)

(let (( digits [1 2 3]))
  (my-for [x1 digits x2 digits]
    (* x1 x2)))

(my-for [x ['a 'b 'c]
           y [1 2 3]]
  (vector x y))

(my-for [x (-iota 6 1)
           :let [y (* x x)
                   z (* x x x)]]
  (vector x y z))

(my-for [x (-iota 6)]
  (* x x)
  )
(my-for [[x y] '([:a 1] [:b 2] [:c 0]) :when (= y 0)] x)

(defalias 'for 'my-for)
(defalias 'range 'my-range)

(benchmark-progn (length (for [x (range 1000) y (range 10000)
                                 :while (> x y)]
                              (vector x y))))

(for [x (range 3) y (range 3) :while (/= x 1)] (vector x y))

(for [x (range 3) :while (/= x 1) y (range 3)] (vector x y))

(defun prime? ( n)
  (-every
   (-not 'zerop)
   (-map (my-fn% (% n %1)) (range 2 n))))

(for [x (range 3 33 2) :when (prime? x)]
     x)

(for [x (range 3 33 2) :while (prime? x)]
     x)

(for [x (range 3 17 2) :when (prime? x)
        y (range 3 17 2) :when (prime? y)]
     (vector x y))

(for [x (range 3 17 2) :while (prime? x)
        y (range 3 17 2) :while (prime? y)]
     (vector x y))


(for [x [1 2 3]
        y [1 2 3]
        :while (<= x y)
        z [1 2 3]]
     (vector x y z))

(setq my-faces (--keep (when (string-match-p "orderless" (symbol-name it))
                         (symbol-name it))
                       (face-list)))

(my-completion-style-all-completions "orderless ! [0-9]"  my-faces nil 0)

(orderless-all-completions "orderless ! [0-9]"  my-faces nil 0)

(completing-read ": " (-map 'symbol-name (face-list))
                 nil nil "orderless ! [0-9]")

(orderless-all-completions ""
                           '("doom-themes-base-faces"
                             "doom-leader-quit/session-map" "doom-init-time"
                             "doom-large-file-excluded-modes"
                             "doom-themes-base-vars" "doom/describe-module"
                             "doom-savehist-remove-unprintable-registers-h")
                           (lambda (cand)
                             (not (string-match-p "doom" cand)))
                           0)

(my-completion-style-all-completions "=doom ! doom"
                                     '("doom-themes-base-faces"
                                       "doom-leader-quit/session-map" "doom-init-time"
                                       "doom-large-file-excluded-modes"
                                       "doom-themes-base-vars" "doom/describe-module"
                                       "doom-savehist-remove-unprintable-registers-h")
                                     nil
                                     0)

(-take 7 (my-shuffle (my-filter-map-symbols
                      (my-fn% (and (string-match-p "doom" (symbol-name %1))
                                   (not (string-match-p "--" (symbol-name %1)))))
                      'symbol-name)))

(benchmark-progn (replace-regexp-in-string "\\(\\`\\| \\)!.*" "" "orderless-all-completions ! [0-9]"))

(my-with-deferred-gc
 (benchmark 65536 '(string-replace "  " "[[:space:]]" "orderless-all-completions !  [0-9]"))
 (benchmark 65536 '(replace-regexp-in-string "  " "[[:space:]]" "orderless-all-completions !  [0-9]"))
 t)

(let ((str "orderless-all-completions !  [0-9]"))
  (my-with-deferred-gc
   (list
    (benchmark-call (lambda () (string-match-p "  " str)) 65536)
    (benchmark-call (lambda () (cl-search "  " str)) 65536)
    (benchmark-call (lambda () (string= str (string-replace " " " " str))) 65536))))


(my-with-deferred-gc
 (my-profiler-cpu-progn
  (benchmark
   65536
   `(my-completion-style-compute-filtered-input-and-predicate
     "orderless-all-completions ! [0-9]"
     nil))))

(my-for [c (append (my-alphabet)
                   (my-alphabet "A"))
           m '("C-" "M-")
           :let [k (concat m c)
                   found (my-lookup-key k)
                   cmd (if (commandp found) found nil)]]
  (cons k cmd))

(my-with-deferred-gc
 (let ((default-directory "~"))
   (benchmark-progn (call-process-shell-command "fd --hidden"))
   (benchmark-progn (call-process-shell-command "find"))
   nil))

(my-with-deferred-gc
 (benchmark 512 '(my-sticky-header-beg-defun-line))
 (benchmark 512 '(thing-at-point 'defun))
 nil)

(list
 (benchmark 1024 '(my-beg-defun-position))
 (benchmark 1024 '(my-beg-defun-line-number-cached))
 (benchmark 1024 '(car (bounds-of-thing-at-point 'defun))))

(my-profiler-cpu-progn
 (benchmark 1024 '(my-beg-defun-line-number-cached)))

(funcall (my-1st-fn #'native-compile #'byte-compile)
         (defun my-ls-A1 (dir &optional full match nosort count)
           (let ((files (directory-files dir full match nosort count))
                 (exclude '("." ".."))
                 res)
             (while (and files exclude)
               (let ((f (pop files)))
                 (if (member f exclude)
                     (setq exclude (delete f exclude))
                   (push f res))))
             `(,@(nreverse res) . ,files))))

(my-with-deferred-gc
 (benchmark 8192 '(my-count-directory-children "~"))
 (benchmark 8192 '(length (my-directory-children-trim-firsts "~")))
 (benchmark 8192 '(length (directory-files "~" nil my-directory-files-non-full-almost-all-regexp)))
 (benchmark 8192 '(length (delete "." (delete ".." (directory-files "~")))))
 (benchmark 8192 '(length (my-directory-children "~")))
 (benchmark 8192 '(length (my-ls-A1 "~")))
 (benchmark 8192 '(length (cl-delete '("." "..") (directory-files "~") :test #'-contains? :count 2)))
 (benchmark 8192 '(length (cl-delete ".." (cl-delete "." (directory-files "~") :test #'equal :count 1) :test #'equal :count 1)))
 (benchmark 8192 '(length (cl-delete '("." "..") (directory-files "~") :test #'-contains?)))
 (benchmark 8192 '(length (-remove (-cut member <> '("." "..")) (directory-files "~"))))
 (benchmark 1024 '(length (seq-difference (directory-files "~") '("." ".."))))
 (benchmark 1024 '(length (f-entries "~")))
 t)

(my-with-deferred-gc
 ;; (benchmark 8192 '(length (directory-files "~" nil my-directory-files-non-full-almost-all-regexp)))
 (list
  (benchmark 8192 '(my-directory-children "~"))
  (benchmark 8192 '(directory-files "~" nil directory-files-no-dot-files-regexp))))

(my-with-deferred-gc
 (length (directory-files "~" t my-directory-files-non-full-almost-all-regexp))
 (length (directory-files "~" t directory-files-no-dot-files-regexp))
 nil)

(my-with-deferred-gc
 (let ((l0 (my-shuffle (-iota 1024)))
       (l1 (my-shuffle (-iota 1024))))
   (list (benchmark-run (expt 2 50) (list l0 l1))
         (benchmark-run (expt 2 50) `(,l0 ,l1)))))

(--> (seq-mapcat 'my-keymap-bindings (current-active-maps))
     (-map 'car it)
     (sort it #'my-by-length>)
     (-remove
      (my-fn%
       (or  (string-match-p
             (my-regexp-or
              '("remap" "menu-bar" "-state>" "wheel" "mouse" "key-chord"
                "drag-n-drop" "^M-SPC"))
             %1)
            (string-match-p "^<[_-a-zA-Z0-9]*>$" %1)))
      it)
     (-take 37 it))

(my-for
  [b (buffer-list)
     :when (member (with-current-buffer b major-mode)
                   '(minibuffer-mode minibuffer-inactive-mode))]
  (cons b
        (with-current-buffer b
          (key-binding (kbd "`")))))

(progn
  (benchmark 8192 '(string-search "  " "orderless-all-completions !  [0-9]"))
  (benchmark 8192 '(string-match-p "  " "orderless-all-completions !  [0-9]"))
  t)

(let ((l `(:a 0 :b 1)))
  (cl-destructuring-bind (&key a b) l
    (my-symbols->plist a b)))

(list
 (ignore-errors (tsc-get-descendant-for-point-range (tsc-root-node tree-sitter-tree) (point) (point)))
 (ignore-errors (tsc-get-descendant-for-position-range (tsc-root-node tree-sitter-tree) (point) (point)))
 (ignore-errors (tsc-get-named-descendant-for-point-range (tsc-root-node tree-sitter-tree) (point) (point)))
 (ignore-errors (tsc-get-named-descendant-for-position-range (tsc-root-node tree-sitter-tree) (point) (point))))

(my-cond-let
 ((([a b] [1 2])
   ((c . d) nil))
  b)
 ([[a b] [1 2]
   (c . d) '(0 1 3)]
  (list a b c d)))

(my-for ((case '(

                 ";; * e"               ; 1
                 ";; *** e"             ; 3
                 ";;* e"                ; 1
                 ";;*** e"              ; 3
                 ";;*e"                 ; ?
                 ";;; e"                ; 1
                 ";;;;; e"              ; 3
                 ";;;;;e"               ; 3
                 ";;;e"                 ; ?

                 ;;
                 ))
         (re-sym '(+emacs-lisp-outline-regexp lispy-outline my-lisp-outline-regexp outline-regexp))
         (lv-fn '(lisp-outline-level my-outline-comment-level lispy-outline-level outline-level))
         (:let [re (symbol-value re-sym)]))
  (with-temp-buffer
    (insert case)
    (goto-char (point-min))
    (list re-sym lv-fn case
          (string-match re (thing-at-point 'line))
          (match-data)
          (ignore-errors (funcall lv-fn)))))

(list
 (benchmark 8192 '(my-common-character? (aref ":key" 0)))
 (benchmark 8192 '(string-match-p "^[a-zA-Z0-9-]" "defn"))
 (benchmark 8192 '(my-common-character? (aref "defn" 0)))
 (benchmark 8192 '(string-match-p "^[a-zA-Z0-9-]" ":key")))

(my-with-deferred-gc
 (list
  (benchmark 8192 '(my-bind "C-v" #'yank))
  (benchmark 8192 '(keymap-global-set "C-v" #'yank))
  (benchmark 8192 '(global-set-key (kbd "C-v") #'yank))))

(my-with-deferred-gc
 (my-profiler-cpu-progn
  (benchmark
   8192
   '(my-bind :map 'org-mode-map :vi 'insert :from 'drag-stuff
      "M-<up>" #'drag-stuff-up "M-<down>" #'drag-stuff-down))))

(my-with-deferred-gc
 (my-profiler-cpu-progn
  (benchmark
   8192
   '(my-bind "C-v" #'yank))))

(my-with-deferred-gc
 (my-profiler-cpu-progn
  (benchmark
   8192
   '(global-set-key (kbd "C-v") #'yank))))

(benchmark 8192 '(-find 'file-regular-p (cdr command-line-args)))
(benchmark 8192 '(-find 'buffer-file-name (buffer-list)))

(benchmark 128 '(shell-command-to-string (format "git ls-files %s 2> /dev/null" "~/.config/my-emacs/init.el")))
(benchmark 128 '(= 0 (call-process-shell-command (format "git ls-files --error-unmatch %s" "~/.config/my-emacs/init.el"))))
(benchmark 128 '(my-vc-git-cached-tracked-p "~/.config/my-emacs/init.el"))
(benchmark 128 '(vc-git-root "~/.config/my-emacs/init.el"))
(benchmark 128 '(vc-backend "~/.config/my-emacs/init.el"))

(benchmark-run 8192
  (+vertico-orderless-dispatch "delfile" 0 1))

(benchmark-run-compiled 8192
  (+vertico-orderless-dispatch "delfile" 0 1))

(my-multiply-string 3 (car (split-string "foo me \n foo now" )))

(-filter (lambda (ft)
           (string-prefix-p "my-" (format "%s " ft)))
         features)

(benchmark 8192 '(my-completion-style--partition-patterns-from-special "foo foo ! me now" "!"))
"Elapsed time: 0.010039s"
(my-completion-style--partition-patterns-from-special "foo foo ! me now" " !")
(my-completion-style--partition-patterns-from-special " !" "!")

(my-loop [[a b] [[1 2] [3 4 ]]]
  (list a b))

(benchmark 8192 '(let ((bindings [1 2 3 4 5 6 7 8 9]))
                   (cl-loop for i below (floor (/ (1- (length bindings))
                                                  2))
                            collect (list (aref bindings i)
                                          (aref bindings (1+ i))))))
(benchmark 8192 '(let ((bindings [1 2 3 4 5 6 7 8 9]))
                   (seq-partition (append bindings nil) 2)))

;; autoload\n(progn\n (orderless-without-literal "  (defun")

(list
 (benchmark 8192 '(equal "KDE is an international" (regexp-quote "KDE is an international")))
 (benchmark 8192 '(string= "KDE is an international" (regexp-quote "KDE is an international"))))

(my-export-minibuffer-symbols (cl-loop for s being the symbols
                                       for sn = (symbol-name s)
                                       when (string-prefix-p "lsp-" sn)
                                       collect sn))

(list (progn (looking-at outline-regexp)
             (funcall outline-level))
      (progn (looking-at outline-regexp)
             (outline-level)))

(my-profiler-cpu-progn
 (benchmark 1024 '(find-file-noselect buffer-file-name)))
(my-profiler-cpu-progn (benchmark 1024 '(my-ensure-buffer-file buffer-file-name)))

(list
 (string-match-p (format "\\$%s\\$" my-preview-latex-math-body-regexp) "$$") ; T
 (string-match-p (format "\\$%s\\$" my-preview-latex-math-body-regexp) "$\\0\\\\$") ; T
 (string-match-p (format "\\$%s\\$" my-preview-latex-math-body-regexp) "$\\0\\$") ; F
 (string-match-p (format "\\$%s\\$" my-preview-latex-math-body-regexp) "$\\\\$") ; T
 (string-match-p (format "\\$%s\\$" my-preview-latex-math-body-regexp) "$\\\\\\$") ; F
 (string-match-p (format "\\$%s\\$" my-preview-latex-math-body-regexp) "$\\$") ; F
 (string-match-p (format "\\$%s\\$" my-preview-latex-math-body-regexp) "$0$") ; T
 )

(list
 (benchmark 1024  '(equal "``" (my-relative-buffer-substring 2 0)))
 (benchmark 1024  '(let ((open "``")
                         (back-length 2))
                     (save-match-data
                       (looking-back (regexp-quote (substring open 0 back-length))
                                     (- (point) back-length))))))

(cl-loop for s being the symbols
         when (and (string-match-p "ein" (symbol-name s))
                   (stringp (my-bounded-value s))
                   (f-directory-p (my-bounded-value s)))
         collect (cons s (symbol-value s)))


(list
 (benchmark 1024  '(equal "``" (my-relative-buffer-substring 2 0)))
 (benchmark 1024  '(let ((open "``")
                         (back-length 2))
                     (save-match-data
                       (looking-back (regexp-quote (substring open 0 back-length))
                                     (- (point) back-length))))))

(let* ((test-bound 8)
       (test-count (* (my-factorial test-bound)
                      1024)))
  (-->
   (let ((lst (-iota test-bound)))
     (-frequencies
      (my-for [i (-iota test-count)]
        (my-shuffle lst))))
   (-map 'cdr it)
   (list :expected-mean 1024
         :actual-mean (my-mean it)
         :variance (my-variance it)
         :sd (sqrt (my-variance it))
         :max (-max it)
         :min (-min it))))

(list
 (benchmark 2048 '(f-parent "~"))
 (benchmark 2048 '(file-name-parent-directory "~")))

(my-with-deferred-gc
 (list
  (benchmark 8192 '(cl-reduce '+ (-iota 1024)))
  (benchmark 8192 '(seq-reduce '+ (-iota 1024) 0))
  (benchmark 8192 '(-reduce '+ (-iota 1024)))
  ;;
  ))
;; ("Elapsed time: 0.426577s" "Elapsed time: 0.516646s" "Elapsed time: 0.339041s")


;; implementations of `my-delete-in-list!'
(my-with-deferred-gc
 (list
  (benchmark 8192 '(dlet ((l (-iota 512))
                          (e (-iota 128 0 2)))
                     (apply (lambda (list-var &rest elements)
                              (set list-var (cl-delete elements (symbol-value list-var)
                                                       :test #'-contains?)))
                            'l e)))
  (benchmark 8192 '(dlet ((l (-iota 512))
                          (e (-iota 128 0 2)))
                     (apply (lambda (list-var &rest elements)
                              (set list-var (-difference (symbol-value list-var) elements)))
                            'l e)))
  (benchmark 8192 '(dlet ((l (-iota 512))
                          (e (-iota 128 0 2)))
                     (apply (lambda (list-var &rest elements)
                              (set list-var (-reduce-from (lambda (seq elt) (delete elt seq))
                                                          (symbol-value list-var)
                                                          elements)))
                            'l e)))
  ;;
  ))

(run-at-time
 (format-time-string "%H:%M")
 1
 (lambda ()
   (dlet ((custom-safe-themes t))
     (customize-set-variable 'custom-enabled-themes
                             (list (seq-random-elt (custom-available-themes)))))))

(progn
  (defun my-test-interpreted-fun ()
    (while-no-input (force-mode-line-update)))
  (defsubst my-test-inline-fun ()
    (while-no-input (force-mode-line-update)))
  (defun my-test-byte-fun ()
    (while-no-input (force-mode-line-update)))
  (defun my-test-native-fun ()
    (while-no-input (force-mode-line-update)))
  (byte-compile #'my-test-byte-fun)
  (native-compile #'my-test-inline-fun)
  (native-compile #'my-test-native-fun)
  (let ((forms (list
                '(my-test-interpreted-fun)
                '(my-test-byte-fun)
                '(my-test-inline-fun)
                '(my-test-native-fun))))
    (cl-loop for form in (my-shuffle forms)
             collect (cons form
                           (benchmark '8192 form)))))

(my-for [x (-iota 3)]
  (my-for [y (-iota 3)]
    (my-for [z (-iota 3)]
      0)))
(let ((tensor '(((0 0 0) (0 0 0) (0 0 0))
                ((0 0 0) (0 0 0) (0 0 0))
                ((0 0 0) (0 0 0) (0 0 0)))))
  (setf (my-in tensor 0 0 0)
        1)
  (setf (my-in tensor 1 -1 1)
        2)
  (setf (my-in tensor 2 -3)
        3)
  tensor)
;; => (((1 0 0) (0 0 0) (0 0 0))
;;  ((0 0 0) (0 0 0) (0 2 0))
;;  (3 (0 0 0) (0 0 0)))

(let ((mat0 (make-vector 3 (make-vector 3 0)))
      (mat1 (my-make-tensor [3 3] 0)))
  (setf (my-in mat0 0 0) t)
  (setf (my-in mat1 0 0) t)
  (list mat0
        mat1))


(list (= 3 (my-levenshtein-distance "kitten" "sitting"))
      (= 3 (my-levenshtein-distance "Saturday" "sunDay" 'no-case)))

(equal
 (my-for [x (-iota 7 1)]
   (my-for [y (-iota 6 1)]
     (--> (sqrt (+ (expt (- 7 x) 2)
                   (expt (- 6 y) 2)))
          (/ (round (* it 10.0))
             10.0))))
 '((7.8 7.2 6.7 6.3 6.1 6.0)
   (7.1 6.4 5.8 5.4 5.1 5.0)
   (6.4 5.7 5.0 4.5 4.1 4.0)
   (5.8 5.0 4.2 3.6 3.2 3.0)
   (5.4 4.5 3.6 2.8 2.2 2.0)
   (5.1 4.1 3.2 2.2 1.4 1.0)
   (5.0 4.0 3.0 2.0 1.0 0.0)))

(let ((proc (--> (async-shell-command "echo 0")
                 window-buffer get-buffer-process)))
  (sleep-for 1)
  (set-process-sentinel proc
                        (lambda (&rest _)
                          (print "will this be printed?")))
  nil)

(let ((proc (--> (async-shell-command "echo 0")
                 window-buffer get-buffer-process)))
  (sleep-for 1)
  (my-set-process-callback proc
                           (lambda (&rest _)
                             (print "will this be printed?")))
  nil)

(my-async-elisp-auto-output-buffer
 "worker"
 `(message "Hello '%s'!" ,(buffer-name))
 :callback
 (lambda (_ _ buf)
   (print (string-trim-right (my-buffer-string buf) "\n\\{1\\}"))))

(my-async-elisp-kill-emacs-daemon "worker")

(my-async-elisp "worker"
                `(progn (find-file ,(expand-file-name "README.org" my-emacs-conf-dir/))
                        (org-latex-export-to-pdf))
                :callback
                (lambda (&rest _)
                  (message "%s" (my-ISO-time))))

(my-async-elisp "oe"
                `(print ))

(--> (dlet ((default-directory (my-project-root)))
       (shell-command-to-string "tree"))
     (replace-regexp-in-string "[├─│└ ]" " " it))

(dlet ((last-command-event ?\ ))
  (benchmark 8192 '(my-pair-insert-space-between-pairs-h)))

(my-with-deferred-gc
 (my-profiler-cpu-progn
  (dlet ((last-command-event ?\n))
    (benchmark 8192 '(my-pair-open-newline-before-h)))))

(my-with-deferred-gc
 (my-profiler-cpu-progn
  (dlet ((last-command-event ?\n))
    (benchmark 8192 '(my-pair-open-newline-between-multicha-pairs-h)))))

(my-profiler-cpu-progn
 (benchmark 8192 '(my-pair-find-multi-char-paren)))

(my-profiler-cpu-progn
 (my-pair-find-multi-char-paren))

(my-profiler-cpu-progn
 (dlet ((last-command-event ?\n))
   ;; (benchmark 8192 '(my-pair-open-newline-before-h))
   (benchmark 8192 '(my-pair-looking-at-local-multichar-closing))))

;; cause stack overflow
(letrec ((fn (lambda (n)
               (funcall fn (+ n 1)))))
  (funcall fn 0))

(benchmark 8192 '(zerop (buffer-size)))

(benchmark 8192 '(and (bobp) (eobp)))

;; can `let*' bindings cause stackoverflow?
(benchmark-progn
  (let* ((varlist (cl-loop for i below (* 16 max-lisp-eval-depth)
                           collect `(v (+ v 1))))
         (form `(let* ((v 0)
                       ,@varlist)
                  v)))
    (eval form 'lexical)))

(let ((form '(progn (consult-customize
                     consult-ripgrep consult-git-grep consult-grep
                     consult-bookmark consult-recent-file
                     +default/search-project +default/search-other-project
                     +default/search-project-for-symbol-at-point
                     +default/search-cwd +default/search-other-cwd
                     +default/search-notes-for-symbol-at-point
                     +default/search-emacsd
                     consult--source-recent-file consult--source-project-recent-file consult--source-bookmark
                     :preview-key (kbd "C-SPC"))
                    (consult-customize
                     consult-theme
                     :preview-key (list (kbd "C-SPC") :debounce 0.5 'any)))))
  (list (benchmark 1024 `(string-search "\"C-SPC\"" (format "%S" ',form)))
        (benchmark 1024 `(my-tree-contain-node? ',form "C-SPC"))
        (benchmark 1024 `(member "C-SPC" (flatten-tree ',form)))))


(dlet ((default-directory "~/.emacsen/emacs/"))
  (list
   (benchmark
    16
    `(with-temp-buffer
       (call-process "git" nil t nil "blame" "-L1,1" "--" "README")
       (goto-char (point-min))
       (if (looking-at "\\`[[:alnum:]]+")
           (match-string 0)
         'some-other-value)))
   (benchmark
    16
    `(-->
      (my-process-sync->output-string '("git" "blame" "-L" "1,1" "--" "README"))
      (let ((n 0))
        (when (string-match "^[^ ]+" it)
          (match-string n it)))))))

(list
 "\n" (save-excursion (syntax-ppss (+ 0 (point))))
 "\n" (save-excursion (syntax-ppss (+ 1 (point))))
 "\n" (save-excursion (syntax-ppss (+ 2 (point))))
 "\n" (save-excursion (syntax-ppss (+ 3 (point)))))

(my-profiler-cpu-progn
 (benchmark 1 '(my-accumulate-config-execute)))


(my-with-deferred-gc
 (list
  (benchmark 8192 '(file-truename (directory-file-name my-emacs-conf-dir/)))
  (benchmark 8192 '(directory-file-name (file-truename my-emacs-conf-dir/)))
  (benchmark 8192 '(my-filename-true-sans-slash my-emacs-conf-dir/))))

(--> (my-get-separated-PATH)
     (-annotate (lambda (dir)
                  (and (file-directory-p dir)
                       (length (my-directory-children dir))))
                it))

(my-profiler-cpu-progn (my-useelisp-refresh))

(require 'straight)
(-filter (lambda (lp)
           (not (string-prefix-p (file-truename (straight--build-dir))
                                 lp)))
         load-path)

(my-with-deferred-gc
 (list
  (benchmark 8192 '(expand-file-name "c" "~/p"))
  (benchmark 8192 '(my-filename-join "~/p" "c"))
  (benchmark 8192 '(file-name-concat "~/p" "c"))))

(my-custom-theme-set-faces 'doom-dracula
  '(font-lock-builtin-face :foreground "deep pink")
  '(font-lock-function-name-face :foreground "dodger blue")
  '(font-lock-preprocessor-face :foreground "aquamarine"))

(list
 (benchmark 1024 '(format "\\(:?%s\\|%s\\)"
                   (orderless-literal "c++")
                   (orderless-regexp "c++")))
 (benchmark 1024 '(concat "\\(:?"
                   (orderless-literal "c++") "\\|"
                   (orderless-regexp "c++")
                   "\\)")))

(list
 (benchmark 8192 '(regexp-quote "c+++++++"))
 (benchmark 8192 '(funcall (-const "c\\+\\+\\+\\+\\+\\+\\+") nil)))

(list
 (benchmark 8192 '(funcall (-const "c++")))
 (benchmark 8192 '(orderless-regexp "c++")))

(list
 (benchmark 8192 '(funcall (-const "c++")))
 (benchmark 8192 '(orderless-regexp "c++")))

(-every (lambda (pat)
          (string-match-p pat "c++-mode"))
        (orderless-pattern-compiler "c++ mode"))

(my-orderless-dispatcher "c++" 0 1)

(string-match-p "\\(:?c\\+\\+\\|c++\\)" "c++-mode")


(my-completion-style-all-completions
 "c++"
 '("c++-mode" "cpp-mode" "c-mode")
 nil
 3)

(my-with-deferred-gc
 (list
  (benchmark 1024 '(--> (directory-files "~")
                        (-separate (lambda (f) (not (string-prefix-p "." f)))
                                   it)
                        (append (nth 0 it)
                                (nth 1 it))))
  (benchmark 1024 '(--> (directory-files "~")
                        (my-append-separate (lambda (f) (not (string-prefix-p "." f)))
                                            it)))))

(my-with-deferred-gc
 (-let* ((buf (current-buffer)))
   (list
    (benchmark
     8192
     `(with-current-buffer ,buf
        (file-truename "~")))
    (benchmark
     8192
     `(if (equal ,buf (current-buffer))
          (file-truename "~")
        (with-current-buffer ,buf
          (file-truename "~")))))))

(list
 (benchmark 1024 '(key-binding (kbd "SPC")))
 (benchmark 1024 '(keymap-local-lookup "SPC")))

(my-profiler-cpu-progn
 (call-interactively
  #'execute-extended-command-for-buffer)
 (run-at-time 0 nil (lambda () (minibuffer-keyboard-quit))))

(-let* ((var nil)
        (sym (make-symbol "")))
  (set sym nil)
  (list
   (with-current-buffer (generate-new-buffer " ")
     (setq-local var 0)
     (make-local-variable sym)
     (set sym 0)
     (vector var (symbol-value sym)))
   (with-current-buffer (generate-new-buffer " ")
     (setq-local var 1)
     (make-local-variable sym)
     (set sym 1)
     (vector var (symbol-value sym)))
   (vector var (symbol-value sym))))

(-let* ((my-frp0
         (frame-parameters (window-frame (get-buffer-window " *corfu*" t))))
        (my-frp1 (frame-parameters (selected-frame))))
  (vector (-difference my-frp0 my-frp1) (-difference my-frp1 my-frp0)))

(my-with-deferred-gc
 (list
  (benchmark 8192 '(nth 1 (s-match "\\([^ \t\n\r]+\\)" "c++ mode")))
  (benchmark 8192 '(replace-regexp-in-string "[ \t\n\r].*" "" "c++ mode"))
  (benchmark
   8192 '(save-match-data (my-regexp-match "\\([^ \t\n\r]+\\)" "c++ mode" 1)))
  (benchmark 8192 '(my-regexp-match "\\([^ \t\n\r]+\\)" "c++ mode" 1))
  ;;
  ))

(my-with-deferred-gc
 (list
  (benchmark
   8
   '(my-vertico-prefixed-sort
     (my-filter-map-symbols 'always 'symbol-name) "c++"))
  (benchmark
   8
   '(my-vertico-prefixed-sort
     (my-filter-map-symbols 'always 'symbol-name) "cpp"))
  (benchmark
   8
   '(my-vertico-sort-prefer-literal
     (my-filter-map-symbols 'always 'symbol-name) "c++"))
  (benchmark
   8
   '(my-vertico-sort-prefer-literal
     (my-filter-map-symbols 'always 'symbol-name) "cpp"))
  (benchmark
   8
   '(vertico-sort-history-length-alpha
     (my-filter-map-symbols 'always 'symbol-name)))
  (benchmark
   8
   '(my-sort-minibuffer-candidates-by-string-distance
     (my-filter-map-symbols 'always 'symbol-name) "c++"))))

(my-profiler-cpu-progn
 (length
  (my-vertico-sort-prefer-literal
   (my-filter-map-symbols 'always 'symbol-name) "c++")))

;; find special characters in symbol names
(-->
 (my-filter-map-symbols
  (lambda (sym)
    (-let* ((name (symbol-name sym)))
      (not (equal name (regexp-quote name)))))
  'symbol-name)
 (-mapcat 'my-string->string-list it)
 (delete-dups it)
 (-difference it (append (my-alphabet) (my-alphabet "A"))))

(-let* ((str "1"))
  (while (not (string-prefix-p "0" str))
    (setq str (format-time-string "%N")))
  (print str))

;; the order of those hooks? Conclusion: `minibuffer-mode-hook', then `minibuffer-setup-hook'
(add-hook
 'minibuffer-setup-hook
 (lambda () (my-log-to nil 'minibuffer-setup-hook)))
(add-hook
 'minibuffer-mode-hook
 (lambda () (my-log-to nil 'minibuffer-mode-hook)))

(file-size-human-readable
 (memory-report-object-size (make-hash-table :size (expt 2 17) :test #'equal)))

;; `kbd' vs `key-description' speed
(list
 "\n"
 (benchmark 8192 '(key-description "  "))
 "\n"
 (benchmark 8192 '(kbd "SPC SPC")))

(list
 (benchmark 1024 '(executable-find "bash"))
 (benchmark 1024 '(file-executable-p "/bin/bash")))

(package-vc--clone
 (package-desc-create :name 'corfu :kind 'vc)
 nil
 (format "~/.local/state/emacs-%s/elpa/corfu/" emacs-version)
 :last-release)



(defun my-test-string-quoting (path &optional str)
  (-let* ((str0 (or str (f-read path 'utf-8)))
          (str1 (prin1-to-string str0)))
    (equal str0 (--> (read-from-string str1) (car it)))))

;; only fail on non-text files
(-filter
 ;; (-lambda (%1) (and (my-test-string-quoting %1) %1))
 (-not #'my-test-string-quoting)
 (-->
  (directory-files-recursively "/etc" "." nil t)
  (-filter #'file-readable-p it)))

(list
 (benchmark 1024 '(string-match-p "^\\*org " (buffer-name)))
 (benchmark 1024 '(string-prefix-p "*org " (buffer-name) t)))

;; blamer conflict with starhugger
(progn
  (switch-to-buffer-other-window (get-buffer-create "*test*"))
  (erase-buffer)
  (mapc #'delete-overlay (overlays-in (point-min) (point-max)))
  (insert "before")
  (save-excursion (insert "
after"))
  (-let* ((ov
           (make-overlay
            ;; (point-min)
            ;; (1+ (point-min))
            (point) (point))))
    (overlay-put
     ov
     'after-string
     (propertize " righ!" 'face `(:foreground "cyan" :underline t)))
    ;; (overlay-put
    ;;  ov 'before-string
    ;;  (propertize "!left " 'face `(:foreground "cyan" :underline t)))
    )
  ;; (starhugger--init-overlay
  ;;    "ov0
  ;; ov1"
  ;;    (point))
  )

(progn
  (switch-to-buffer-other-window (get-buffer-create "*test*"))
  (erase-buffer)
  (mapc #'delete-overlay (overlays-in (point-min) (point-max)))
  (insert "0")
  (save-excursion (insert "
1"))
  (starhugger--init-overlay
   "abcd
efgh"
   (point)))


;; test if `after-change-functions' can check if interactive
(-let* ((buf (generate-new-buffer-name "*test*")))
  (save-selected-window
    (switch-to-buffer-other-window buf)
    (add-hook 'after-change-functions
              (lambda (&optional beg end old-len)
                (my-prn🌈
                 "site-lisp/attic/my-tests.el:981:49"
                 (called-interactively-p t)
                 (called-interactively-p 'any)
                 (called-interactively-p 'interactive))
                (my-prn🌈 (my-symbols->plist this-command beg end old-len)))
              nil t)
    (run-at-time
     1.0 nil
     (lambda ()
       (with-current-buffer buf
         (insert "test!"))))
    nil))


(save-selected-window
  (pop-to-buffer (generate-new-buffer "*test*"))
  (insert
   (propertize my-face-font-preview-sentence
               'face
               `(:family ,(my-get-system-default-variable-width-font)))))

(list
 (benchmark
  1024
  '(with-current-buffer (current-buffer)
     nil))
 (benchmark
  1024
  '(with-current-buffer (current-buffer)
     (exp float-pi)))
 (benchmark
  1024 '(starhugger--with-current-buffer (current-buffer) (exp float-pi)))
 (benchmark 1024 '(exp float-pi)))

(list (benchmark 8192 '(current-buffer)))

(dlet ((url-request-method "POST")
       (url-request-extra-headers
        `(("Content-Type" . "application/json")
          ("Authorization" . ,(my-s$ "Bearer ${starhugger-api-token}"))))
       (url-request-data
        (-->
         "🤗"
         ;; (url-hexify-string it) ;
         (json-serialize `((inputs . ,it))) ;
         (encode-coding-string it 'utf-8) ;
         ;;
         )
        ;; (starhugger--json-serialize `((inputs . "🤗")))
        ))
  (url-retrieve
   ;; "https://api-inference.huggingface.co/models/gpt2"
   "https://api-inference.huggingface.co/models/bigcode/starcoder"
   (lambda (status)
     (save-selected-window
       (setq my-test-status status)
       (-let* ((str
                (buffer-substring
                 (or url-http-end-of-headers (point-min)) (point-max))))
         (pop-to-buffer "*my-test*")
         ;; (insert (format "%S\n" status))
         (insert
          str "\n"
          (-->
           str (url-unhex-string it) (decode-coding-string it 'utf-8))
          "\n\n"))))))

(starhugger--request
 "🤗"
 (lambda (status)
   (my-prn🌈 "site-lisp/attic/my-tests.el:1034:34" status)
   (-let* ((str (buffer-string)))
     (save-window-excursion
       (pop-to-buffer "*my-test*")
       (insert str)))))

(my-with-deferred-gc
 (list
  (benchmark 1 '(my-auth-source-get-no-ask "huggingface" "token"))
  (benchmark 1 '(my-pass-get-pass-name-fresh "huggingface/token"))
  (benchmark 1 '(auth-source-pass-get 'secret "huggingface/token"))))

(progn
  (-let* ((time0 (time-to-seconds))
          (cb
           (lambda (&rest args)
             (my-log🌈
              (- (time-to-seconds) time0)
              'request
              (or (plist-get args :data) args)))))
    (request
      starhugger-model-api-endpoint-url
      :type "POST"
      :data (json-serialize `((inputs . ,(my-random-string 8))))
      :headers '(("Content-Type" . "application/json"))
      :success cb
      :error cb))
  (-let* ((time0 (time-to-seconds)))
    (dlet ((url-request-method "POST")
           (url-request-data
            (json-serialize `((inputs . ,(my-random-string 8)))))
           (url-request-extra-headers `(("Content-Type" . "application/json"))))
      (url-retrieve
       starhugger-model-api-endpoint-url
       (lambda (status)
         (-let* ((content
                  (and url-http-end-of-headers
                       (buffer-substring url-http-end-of-headers (point-max)))))
           (my-log🌈
            (- (time-to-seconds) time0)
            'url-retrieve
            (my-json-string->alist content))))
       nil t))))

(starhugger--request
 "🌈"
 (lambda (&rest args) (my-prn🌈 "site-lisp/attic/my-tests.el:1062:5" args)))


;; can another sub sub entry in `emulation-mode-map-alists' (or sub entry in
;; `minor-mode-map-alist') handle the key if the preceding entry has already
;; checked and returned nil?
(-let* ((mt0 `(menu-item "" pwd :filter 'ignore))
        (mt1 `(menu-item "" pwd :filter ,(lambda (cmd) cmd)))
        (kmap0 (define-keymap "<pause>" mt0))
        (kmap1 (define-keymap "<pause>" mt1))
        (elem0 (cons t kmap0))
        (elem1 (cons t kmap1)))
  (setq my-test-emulation-mode-map-alists-elem (list elem0 elem1))
  (add-to-list
   'emulation-mode-map-alists 'my-test-emulation-mode-map-alists-elem))
;; YES!


(-let* ((cand " ")
        (l (length cand)))
  (vector
   (benchmark
    8192
    `(cond
      ((zerop ,l)
       0)
      ((my-common-character? (aref ,cand 0))
       ,l)
      (t
       (* 2 ,l))))
   (benchmark
    8192
    `(cond
      ((my-common-character? (aref ,cand 0))
       ,l)
      (t
       (* 2 ,l))))
   (benchmark
    8192
    `(cond
      ((ignore-error (args-out-of-range) (my-common-character? (aref ,cand 0)))
       ,l)
      (t
       (* 2 ,l))))))

(my-with-deferred-gc
 (defvar my-test-cache (make-hash-table :test #'equal :size (* 1024 64)))
 (defun my-test-score-completion-candidate+memoize (cand)
   (with-memoization (gethash cand my-test-cache)
     (my-score-code-completion-candidate cand)))
 (defun my-test-sort-completion-candidates+memoize (cands)
   (cl-sort cands #'< :key #'my-test-score-completion-candidate+memoize))
 (-let* ((cands (-map #'symbol-name (my-emacs-symbol-list))))
   (ignore-errors
     (native-compile 'my-test-score-completion-candidate+memoize))
   (ignore-errors
     (native-compile 'my-test-sort-completion-candidates+memoize))
   (ignore-errors
     (native-compile 'my-sort-code-completion-candidates))
   (vector
    (benchmark 8 `(my-sort-code-completion-candidates ',cands))
    (benchmark 8 `(cl-sort ',cands #'< :key #'my-score-code-completion-candidate))
    (benchmark 8 `(my-test-sort-completion-candidates+memoize ',cands)))))

(progn
  (require 'lsp-mode)
  (benchmark-progn
    (-->
     (-map
      (-lambda ((k . _))
        (when (symbolp k)
          (with-temp-buffer
            (delay-mode-hooks
              (ignore-errors
                (my-safe-call k))))
          (and (not (provided-mode-derived-p k 'prog-mode)) k)))
      lsp-language-id-configuration)
     (delete-dups it))))

(start-process "" nil "emacs"
               "-q" "--eval"
               "(progn
  (find-file \"/tmp/test.txt\")

  (dlet ((gc-cons-threshold most-positive-fixnum) (gc-cons-percentage 0.75))

    (benchmark-progn (find-file \"/tmp/test.md\"))

    (benchmark-progn (find-file \"/tmp/test.org\"))

    ;;
    )

  (switch-to-buffer-other-window (messages-buffer))

  ;;
  )")

;; "--load" as command line argument: `find-file''s buffer won't be displayed
;; but the splash screen (`initial-buffer-choice') instead
(-let* ((tmp-file (make-temp-file "")))
  (with-temp-file tmp-file
    (insert
     ";; -*- lexical-binding: t; -*-

(find-file (make-temp-file \"test.txt\"))
"))
  (start-process "" nil "emacs" "-q" "-l" tmp-file))

(progn
  (setq my-buf "*test*")
  (setq my-img-data (--> (my-image-at-point) (my-mixed-plist-get it :data)))
  (setq my-proc (start-process my-buf my-buf "tesseract" "-" "-"))
  (process-send-string my-proc my-img-data))

(-let* ((proc (apheleia-format-buffer (my-apheleia-get-formatters-safe)))
        (cb0 (process-sentinel proc)))
  (set-process-sentinel
   proc
   (lambda (proc event)
     (funcall cb0 proc event)
     (message "%S"
              (vector
               (process-exit-status proc) (my-symbols->plist proc event))))))

(kill-new
 (-->
  (starhugger-project-context--command-args)
  (mapconcat (lambda (el)
               (-->
                el
                (string-replace "\"" "\\\"" it)
                (string-replace "$" "\\$" it)
                (format "\"%s\"" it)))
             it
             " ")))

(-let* ((var (my-ISO-time))
        (fn (lambda () (-iota 7))))
  (async-start
   (lambda () (vector (funcall fn) var (format-time-string "%F %T")))
   (lambda (result)
     (let* ((var nil))
       (message "test-async %S"
                (with-memoization var
                  result))))))

(start-process "" nil "emacs"
               "--eval"
               "(benchmark-progn (require 'eglot))"
               "--eval"
               "(benchmark-progn (require 'lsp-mode))"
               "-e"
               "my-2msg"
               ;;
               )

(async-start
 (lambda () (require 'dash))
 (lambda (res) (my-prn🌈 "site-lisp/attic/my-tests.el:1201:14" res)))

;; list all Arch Linux packages, sorted by number of recursive dependants
(-let* ((count-recursive-dependants
         (lambda (pkg-name)
           (-->
            pkg-name
            (format "pactree -r -u %s | wc -l" pkg-name)
            (shell-command-to-string it)
            (string-to-number it))))
        (packages (process-lines "pacman" "-Qsq")))
  (-->
   packages
   (-map
    (lambda (pkg) (list pkg (funcall count-recursive-dependants pkg))) packages)
   (cl-sort it #'> :key #'cadr)))


(my-navistack-minibuffer-backspace-to-back-act
 (list
  (lambda (_ &rest _)
    (completing-read "0: " '("0" "1" "2" "3" "4" "5" "6" "7" "8" "9")))
  (lambda (num &rest _)
    (completing-read
     "1: " (--map (concat num it) '("0" "1" "2" "3" "4" "5" "6" "7" "8" "9"))))
  (lambda (num &rest _)
    (completing-read
     "2: "
     (--map (concat num it) '("0" "1" "2" "3" "4" "5" "6" "7" "8" "9"))))))


(my-with-deferred-gc

 (defun starhugger--trim-from-stop-tokens (str &optional stop-token-lst)
   (cond
    ((not starhugger-chop-stop-token)
     str)
    (:else
     (named-let
         recur
         ((stop-token-lst (or stop-token-lst starhugger-stop-tokens)) (retval str))
       (-let*
           ((stop-token (car stop-token-lst))
            ;; I think literal `string-search' is faster than `replace-regexp-in-string'?
            (found (and stop-token (string-search stop-token retval))))
         (cond
          ((seq-empty-p stop-token-lst)
           retval)
          (found
           (recur (cdr stop-token-lst) (substring retval 0 found)))
          (:else
           (recur (cdr stop-token-lst) retval))))))))

 (-let*
     ((str
       "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
        occaecat cupidatat non proident, sunt in culpa qui officia deserunt
        mollit anim id est laborum.")
      (toks '("esse" "fugiat")))
   (vector
    (equal
     (starhugger--trim-from-stop-tokens str toks)
     (replace-regexp-in-string (format "%s[^z-a]*" (regexp-opt toks)) "" str))
    (benchmark
     1024
     `(replace-regexp-in-string
       ,(format "%s[^z-a]*" (regexp-opt toks)) "" ,str))
    (benchmark 1024 `(starhugger--trim-from-stop-tokens ,str ',toks))))

 ;;
 )


(let ((temp-buffer (generate-new-buffer " *temp*" t)))
  (save-current-buffer
    (set-buffer temp-buffer)
    (unwind-protect
        (progn
          (insert "wth?")
          (write-file "~/temp/test/file"))
      (my-prn🌈 "site-lisp/attic/my-tests.el:1283:19" (buffer-name temp-buffer))
      (and (buffer-name temp-buffer) (kill-buffer temp-buffer)))))

(add-hook 'buffer-list-update-hook #'_my-h)
(defun _my-h ()
  (run-with-timer
   1 nil (lambda () (remove-hook 'buffer-list-update-hook #'_my-h)))
  (when (get-file-buffer "~/temp/test/file")
    (my-backtrace)))

;; (add-hook 'find-file-hook #'_my-h)
(remove-hook 'buffer-list-update-hook #'_my-h)

(get-file-buffer "~/temp/test/file")

(dlet ((default-directory (or (my-project-root) default-directory)))
  (-let* (coll
          (coll-fn
           (lambda ()
             (unless coll
               (my-process-async
                '("fd" "--color=never")
                (lambda (stdout &rest _)
                  (setq coll (split-string stdout nil t)))))
             coll)))
    (completing-read
     "File: "
     (lambda (string pred action)
       (cond
        ((equal action 'metadata)
         `(metadata (category . file)))
        (t
         (complete-with-action action (funcall coll-fn) string pred)))))))

﻿;; -*- lexical-binding: t; -*-

;; Ignore thousands of native compilations jobs that prolongs installation time
;; and wastes disk space (for libraries that won't be used)
(advice-add #'native-compile-async :override #'ignore)

(require 'my-functions-command-line)

;; (princ (list 'load-prefer-newer ":" load-prefer-newer)) (terpri)

(when (fboundp #'my-cli-tasks-before)
  (add-hook 'doom-before-sync-hook #'my-cli-tasks-before))

(when (fboundp #'my-cli-tasks-after)
  (add-hook 'doom-after-sync-hook #'my-cli-tasks-after))

(add-hook 'my-doom-before-upgrade-hook #'my-useelisp-update-all)

(advice-add 'doom-cli-upgrade :before #'my-doom-run-before-upgrade-hook-a)

;; some XDG_* environment variables can't be inherited by a systemd service
;; (Emacs daemon)

(with-eval-after-load 'doom-cli-env
  (pushnew! doom-env-deny

            "^XDG_SESSION_ID$"
            ;; don't set this to a non-Bash compatible one!
            "^SHELL$")


  (pushnew! doom-env-allow

            ;; it's not like that switching desktop environment is frequent
            "^XDG_CURRENT_DESKTOP$"))



;; Local Variables:
;; no-byte-compile: t
;; End:

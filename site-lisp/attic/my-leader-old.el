;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(when (fboundp #'evil-mode)
  (autoload 'evil-local-mode "evil-core")

  (autoload 'evil-motion-state "evil-states")
  (autoload 'evil-normal-state "evil-states")
  (autoload 'evil-insert-state "evil-states")
  (autoload 'evil-emacs-state "evil-states")

  ;;
  )

;;; my-leader-mode

(defvar-local my-leader-mode--prev-state nil)
;;;###autoload
(define-minor-mode my-leader-mode
  nil
  :lighter " <leader>"
  :keymap
  `(
    ;; ;; inspired by `view-mode-map'
    ;; (,(kbd "x") . ,(my-dispatch-cmds '((use-region-p) 'exchange-point-and-mark)))

    ;; `doom-leader-key'
    ,(cons (kbd "SPC") #'my-leader-spc-prefix-cmd)

    ;; `doom-leader-alt-key'
    ,(cons
      (kbd "M-SPC M-SPC")
      `(menu-item
        ""
        nil
        :filter my-leader-mode-menu-item-filter-get-native-SPC-command))

    ;;
    )
  (if my-leader-mode
      (progn
        (setq-local my-leader-mode--prev-state
                    (buffer-local-set-state cursor-type 'box))
        (when (bound-and-true-p evil-local-mode)
          (evil-local-mode 0)))
    (progn
      (buffer-local-restore-state my-leader-mode--prev-state))))
(add-hook 'minions-prominent-modes 'my-leader-mode)

(defun my-leader-mode-menu-item-filter-get-native-SPC-command (&optional _)
  (my-with-mode/s '(my-leader-mode) nil (key-binding (kbd "SPC"))))

;;;###autoload
(defun my-leader-mode-menu-item-filter-dispatch-SPC-SPC (_)
  (cond
   ((derived-mode-p 'dired-mode)
    #'my-findutil-fallback-to-async)
   ;; allow scrolling (or another command) in special buffers that doesn't
   ;; belong to a project
   ((and (not (equal (buffer-name) (my-get-initial-buffer-choice-name)))
         (equal (kbd "SPC SPC") (this-command-keys))
         (not (project-current))
         buffer-read-only)
    (keymap-local-lookup "SPC"))
   (t
    #'my-findutil-in-project-or-prompt-project)))

;;; my-evil-or-leader

(defvar my-evil-or-leader-mode-alist '())
(my-add-list!
  'my-evil-or-leader-mode-alist
  '( ;

    ;; ("\\`\\*[Oo]rg " nil)

    (image-mode my-leader-mode)
    (tar-mode my-leader-mode)

    (comint-mode nil)
    (minibuffer-mode nil)
    (minibuffer-inactive-mode nil)

    (prog-mode evil-local-mode)
    (text-mode evil-local-mode)
    (conf-mode evil-local-mode)
    (messages-buffer-mode evil-local-mode) ; no useful keys OOTB compared to evil

    ;;
    )
  t)

(defun my-evil-or-leader-assoc-from-alist ()
  (save-match-data
    (-find
     (-lambda ((key . _))
       (cond
        ((symbolp key)
         (or (and (boundp key) (symbol-value key))
             (provided-mode-derived-p major-mode key)))
        ((stringp key)
         (string-match key (buffer-name)))))
     my-evil-or-leader-mode-alist)))

;;;###autoload
(defun my-evil-or-leader-state-when-invoke-as-editor ()
  (and (bound-and-true-p evil-local-mode)
       (equal (buffer-name) (my-invoked-as-editor-original-buffer))
       #'evil-insert-state))

;;;###autoload
(defun my-evil-or-leader-get-decision--1 ()
  (-let* ((from-alist (my-evil-or-leader-assoc-from-alist))
          ((_ got . got-args) from-alist)
          (funda (member major-mode '(fundamental-mode)))
          (proc (get-buffer-process (current-buffer))))
    (cond
     (from-alist
      (and (functionp got)
           (if got-args
               (lambda () (apply got got-args))
             got)))
     ((or buffer-file-name
          funda
          (and (not proc)
               (not buffer-read-only) cursor-type
               (not
                (and (local-variable-p 'require-final-newline)
                     (not require-final-newline)))
               (not (string-suffix-p "-repl-mode" (symbol-name major-mode)))))
      (and (fboundp #'evil-mode) #'evil-local-mode))
     ((and buffer-read-only
           (not
            (or proc
                ;; other heuristics to detect process in buffer
                (and (local-variable-p 'filter-buffer-substring-function)
                     mode-line-process))))
      #'my-leader-mode))))

;;;###autoload
(defun my-evil-or-leader-get-decision ()
  "Decide to use evil, leader, or none.
About REPL buffers, using insert state for them is potentially
annoying, as we may press ESC multiple times to quit the window,
then coming back to them later would drop us into normal state
and require a extra key press to start typing, also usually modal
navigation is not so useful for those."
  (-let* ((decision (my-evil-or-leader-get-decision--1)))
    (cond
     ((and (member decision '(evil-local-mode))
           (or (my-evil-or-leader-state-when-invoke-as-editor) decision)))
     (t
      decision))))

;;;###autoload
(defun my-evil-or-leader-maybe ()
  (interactive)
  (-some--> (my-evil-or-leader-get-decision) (funcall it)))

(defvar-local evil-local-mode nil)
(defvar evil-state-properties '())

(defun my-evil--mode-p (symbl)
  (and (or (member symbl '(evil-mode evil-local-mode))
           (-some
            (-lambda
              ((k . plst))
              (or (equal symbl k) (equal symbl (plist-get plst :toggle))))
            evil-state-properties))
       'evil-local-mode))

;;;###autoload
(defun my-evil-or-leader-mode-refresh (&rest _)
  (interactive)
  (-let* ((target (my-evil-or-leader-get-decision))
          (mode-of-target (or (my-evil--mode-p target) target)))
    (dolist (mode '(evil-local-mode my-leader-mode))
      (when (and (not (equal mode mode-of-target)) (symbol-value mode))
        (funcall mode 0)))
    (when target
      (funcall target))))

(define-minor-mode my-evil-or-leader-mode
  nil)

(defun my-evil-or-leader-maybe--set-window-buffer-a
    (func window buffer-or-name &rest args)
  (prog1 (apply func window buffer-or-name args)
    (when buffer-or-name
      (with-current-buffer buffer-or-name
        (unless (or (bound-and-true-p evil-local-mode)
                    (bound-and-true-p my-leader-mode))
          (my-evil-or-leader-maybe))))))

;;;###autoload
(define-globalized-minor-mode my-evil-or-leader-global-mode
  my-evil-or-leader-mode
  my-evil-or-leader-mode-refresh
  (if my-evil-or-leader-global-mode
      (progn
        ;; `hack-local-variables-hook'?
        (add-hook 'read-only-mode-hook 'my-evil-or-leader-mode-refresh)
        ;; mimic how evil initializes in buffers that are untouchable even by
        ;; global minor modes
        (advice-add
         #'set-window-buffer
         :around #'my-evil-or-leader-maybe--set-window-buffer-a))
    (progn
      (remove-hook 'read-only-mode-hook 'my-evil-or-leader-mode-refresh)
      (advice-remove
       #'set-window-buffer #'my-evil-or-leader-maybe--set-window-buffer-a))))

(defvar my-toggle-evil-maybe-leader-hook '()
  "Hooks run after `my-toggle-evil-maybe-leader'.")

(defvar-local my-toggle-evil-maybe-leader--last-state [nil nil])

;;;###autoload
(defun my-toggle-evil-maybe-leader ()
  "Toggle `evil' in current buffer.
When the state to switch to Emacs state, switch to insert
instead. When evil is going to be turned on, disable
`my-leader-mode'. When evil is going to be turned off and
previously `my-leader-mode' is on, re-turn-on the latter."
  (interactive)
  (dlet ((inhibit-message t))
    (-let* (([last-evil last-leader] my-toggle-evil-maybe-leader--last-state)
            (cur-evil evil-state)
            (cur-leader my-leader-mode))
      (cond
       (evil-local-mode
        (evil-local-mode 0)
        ;; after turning off evil locally, cursor doesn't revert back
        (kill-local-variable 'cursor-type)
        (when last-leader
          (my-leader-mode)))
       (t
        (-let* ((state-to-switch (evil-initial-state-for-buffer)))
          (cond
           (last-evil
            (evil-change-state last-evil))
           ((member state-to-switch '(emacs))
            (evil-change-state 'insert))
           (t
            (evil-local-mode))))
        (my-leader-mode 0)))
      (setq-local my-toggle-evil-maybe-leader--last-state
                  (vector cur-evil cur-leader))))
  (run-hooks 'my-toggle-evil-maybe-leader-hook)
  evil-local-mode)

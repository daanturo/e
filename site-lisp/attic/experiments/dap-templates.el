;; -*- lexical-binding: t; -*-

(require 'dap-mode)

(defvar dap-project-specific-templates-file
  (or (and (featurep 'no-littering)
	   (no-littering-expand-var-file-name "dap/dap-project-specific-templates.el"))
      (expand-file-name (locate-user-emacs-file ".dap-project-specific-templates")))
  "Where to persist project-specific DAP templates.")

(defvar dap-mode-templates-file
  (or (and (featurep 'no-littering)
	   (no-littering-expand-var-file-name "dap/dap-mode-templates.el"))
      (expand-file-name (locate-user-emacs-file ".dap-mode-templates")))
  "Where to persist project-specific DAP templates.")

(defvar dap-project-specific-templates nil
  "List of project-specific DAP templates.
Value is an alist where each element is a cons cell of the form
    (PROJECT . CONFIGURATION).")

(defvar dap-mode-specific-templates nil
  "List of mode-specific DAP templates.
Value is an alist where each element is a cons cell of the form
    (MODE . CONFIGURATION).")

(defun dap-save-custom-template ()
  )

(defun dap--read-custom-templates ())

(provide 'dap-templates)

;;; dap-templates.el ends here

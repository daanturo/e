;; -*- lexical-binding: t; -*-

(require 'compat-30)

;;; Defs

(defun treesit-node-get (node instructions)
  (declare (indent 1))
  (while (and node instructions)
    (pcase (pop instructions)
      ('(field-name) (setq node (treesit-node-field-name node)))
      ('(type) (setq node (treesit-node-type node)))
      (`(child ,idx ,named) (setq node (treesit-node-child node idx named)))
      (`(parent ,n)
       (dotimes (_ n)
         (setq node (treesit-node-parent node))))
      (`(text ,no-property) (setq node (treesit-node-text node no-property)))
      (`(children ,named) (setq node (treesit-node-children node named)))
      (`(sibling ,step ,named)
       (dotimes (_ (abs step))
         (setq node
               (if (> step 0)
                   (treesit-node-next-sibling node named)
                 (treesit-node-prev-sibling node named)))))))
  node)

;;; Adaptations

;; Make `provided-mode-derived-p', `derived-mode-p''s non-variadic calls that
;; are used on newer versions runnable on older versions

;; (derive-mode-p '(parent-mode-1 parent-mode-2 ...)) -> (derive-mode-p parent-mode-1 parent-mode-2 ...)

(defun my-emacs30/derived-mode-p-a (func modes &rest deprecated-args)
  (cond
   ((listp modes)
    (apply func modes))
   (:else
    (apply func modes deprecated-args))))
(advice-add #'derived-mode-p :around #'my-emacs30/derived-mode-p-a)

;; (provided-mode-derive-p child-mode '(parent-mode-1 parent-mode-2 ...)) -> (provided-mode-derive-p child-mode parent-mode-1 parent-mode-2 ...)

(defun my-emacs30/provided-mode-derived-p-a
    (func mode modes &rest deprecated-args)
  (cond
   ((listp modes)
    (apply func mode modes))
   (:else
    (apply func mode modes deprecated-args))))
(advice-add #'provided-mode-derived-p :around #'my-emacs30/provided-mode-derived-p-a)

;; `transient-define-prefix''s autoload form produces in the autoload file:
;; (put 'my-transient-name 'transient--prefix (transient-prefix :command 'my-transient-name :transient-suffix t :transient-non-suffix t))

;; workaround by providing a dummy definition

(when (not (fboundp #'transient-prefix))
  (defalias #'transient--suffix-only #'ignore)
  (defalias #'transient-prefix #'ignore))

;;; Settings

;;; my-compats-30.el ends here

(provide 'my-compats-30)

;; Local Variables:
;; no-byte-compile: t
;; End:

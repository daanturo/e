;; -*- lexical-binding: t; -*-

(require 'compat-25)

;;; Defs

;; ;; should this be right? or should I avoid for older version?
;; (defalias #'char-fold-to-regexp #'identity)

;; Credit: copied from upstream
(defun comment-line (n)
  (interactive "p")
  (if (use-region-p)
      (comment-or-uncomment-region
       (save-excursion
         (goto-char (region-beginning))
         (line-beginning-position))
       (save-excursion
         (goto-char (region-end))
         (line-end-position)))
    (when (and (eq last-command 'comment-line-backward)
               (natnump n))
      (setq n (- n)))
    (let ((range
           (list (line-beginning-position)
                 (goto-char (line-end-position n)))))
      (comment-or-uncomment-region
       (apply #'min range)
       (apply #'max range)))
    (forward-line 1)
    (back-to-indentation)
    (unless (natnump n) (setq this-command 'comment-line-backward))))

;; Credit: copied from upstream
(defun vc-root-dir ()
  (require 'vc)
  (let ((backend (vc-deduce-backend)))
    (if backend
        (condition-case err
            (vc-call-backend backend 'root default-directory)
          (vc-not-supported
           (unless (eq (cadr err) 'root)
             (signal (car err) (cdr err)))
           nil)))))

;;; Adaptations

(defun my-emacs25-project-root-a (&optional dir)
  (let ((default-directory (or dir default-directory)))
    (vc-root-dir)))

(advice-add #'my-project-root :override #'my-emacs25-project-root-a)

;; To bind keys
(with-eval-after-load 'ido
  (ido-init-completion-maps))

;;; Settings

;; try after installing packages
(with-eval-after-load 'my-packages
  (if (require 'undo-tree nil 'noerror)
      (global-undo-tree-mode)
    (my-println🌈 "No redo!")))

;; ;; just disable is simpler
;; (defvar my-emacs25-undo-tree-history-directory
;;   (locate-user-emacs-file "undo-tree-history"))
;; (with-eval-after-load 'undo-tree
;;   (add-to-list 'undo-tree-history-directory-alist
;;                `("" . ,my-emacs25-undo-tree-history-directory)))

(define-globalized-minor-mode my-emacs25-eldoc-global-mode eldoc-mode eldoc-mode)
(my-emacs25-eldoc-global-mode)

(define-key global-map (kbd "C-x C-;") #'comment-line)

;;; my-compats-25.el ends here

(provide 'my-compats-25)

;; Local Variables:
;; no-byte-compile: t
;; End:

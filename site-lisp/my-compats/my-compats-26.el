;; -*- lexical-binding: t; -*-

(require 'compat-26)

;;; Defs

;; Extend optional arguments

;; Credit: copied from upstream
(defun string-trim-left (string &optional regexp)
  (if (string-match (concat "\\`\\(?:" (or regexp "[ \t\n\r]+") "\\)") string)
      (substring string (match-end 0))
    string))

;; Credit: copied from upstream
(defun string-trim-right (string &optional regexp)
  (let ((i (string-match-p (concat "\\(?:" (or regexp "[ \t\n\r]+") "\\)\\'")
                           string)))
    (if i (substring string 0 i) string)))

;; Credit: copied from upstream
(defun string-trim (string &optional trim-left trim-right)
  (string-trim-left (string-trim-right string trim-right) trim-left))

(defun assoc (key alist &optional testfn)
  (let* ((testfn (or testfn #'equal))
         (found nil)
         (processing alist))
    (while (and (not found) (< 0 (length processing)))
      (let* ((pair (car-safe processing)))
        (when (consp pair)
          (let* ((k (car pair)))
            (when (funcall testfn k key)
              (setq found pair)))))
      (setq processing (cdr processing)))
    found))

;;; Adaptations

;;; Settings

(defun my-emacs26-shell--environment-a ()
  process-environment)
(advice-add #'my-shell--environment :override #'my-emacs26-shell--environment-a)

;;; my-compats-26.el ends here

(provide 'my-compats-26)

;; Local Variables:
;; no-byte-compile: t
;; End:

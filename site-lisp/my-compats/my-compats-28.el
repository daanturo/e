;; -*- lexical-binding: t; -*-

(require 'compat-28)

;;; Defs

(defun my-emacs28-revert-buffer-quick ()
  (interactive)
  (revert-buffer t (not (buffer-modified-p))))

(with-eval-after-load 'project
  (unless (fboundp #'project-root)
    (defun project-root (project)
      (car (project-roots project)))))

;;; Adaptations

(defalias #'minibuffer-mode #'minibuffer-inactive-mode)
(defvaralias 'minibuffer-mode 'minibuffer-inactive-mode)
(defvaralias 'minibuffer-mode-hook 'minibuffer-inactive-mode-hook)
(defvaralias 'minibuffer-mode-map 'minibuffer-local-map)

(defun my-emacs28-mapconcat-optional-separator--a (func function sequence &optional separator)
  (funcall func function sequence (or separator "")))
(advice-add #'mapconcat :around #'my-emacs28-mapconcat-optional-separator--a)

(defun my-emacs28-directory-files-optional-count--a (func directory &optional full match nosort
                                                          count)
  (let* ((files (funcall func directory
                         full match nosort)))
    (if count
        (-take count files)
      files)))
(advice-add #'directory-files :around #'my-emacs28-directory-files-optional-count--a)

;; Credit: copied from upstream
(put 'remove-hook 'interactive-form
     '(interactive
       (let* ((default (and (symbolp (variable-at-point))
                            (symbol-name (variable-at-point))))
              (hook (intern (completing-read
                             (format-prompt "Hook variable" default)
                             obarray #'boundp t nil nil default)))
              (local
               (and
                (local-variable-p hook)
                (symbol-value hook)
                (or (not (default-value hook))
                    (y-or-n-p (format "%s has a buffer-local binding, use that? "
                                      hook)))))
              (fn-alist (mapcar
                         (lambda (x) (cons (with-output-to-string (prin1 x)) x))
                         (if local (symbol-value hook) (default-value hook))))
              #'(alist-get (completing-read
                            (format "%s hook to remove: "
                                    (if local "Buffer-local" "Global"))
                            fn-alist
                            nil t nil 'set-variable-value-history)
                           fn-alist nil nil #'string=))
         (list hook function local))))

;;; Settings

;; `use-short-answers'
(advice-add #'yes-or-no-p :override #'y-or-n-p)

(define-key global-map (kbd "C-x x g") #'my-emacs28-revert-buffer-quick)
(define-key global-map (kbd "C-x x n") #'clone-buffer)
(define-key global-map (kbd "C-x x t") #'toggle-truncate-lines)
(define-key global-map (kbd "C-x x u") #'rename-uniquely)

;;; my-compats-28.el ends here

(provide 'my-compats-28)

;; Local Variables:
;; no-byte-compile: t
;; End:

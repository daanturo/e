;; -*- lexical-binding: t; -*-

(require 'compat 29)

;;; Defs

;; for interactive
(defun my-emacs29-elisp-eval-region-or-buffer ()
  (interactive)
  (if (use-region-p)
      (eval-region (region-beginning) (region-end))
    (eval-buffer))
  (message "`my-emacs29-elisp-eval-region-or-buffer' in %s" (buffer-name)))

;; ;; Let us call `multisession-value' more freely, but `my-multisession' defined
;; (when (not (fboundp #'define-multisession-variable))
;;   (defalias #'multisession-value #'ignore)
;;   (gv-define-simple-setter multisession-value ignore))

;;; Adaptations

;;; Settings

(define-key emacs-lisp-mode-map (kbd "C-c C-e") #'my-emacs29-elisp-eval-region-or-buffer)
(define-key global-map (kbd "M-g i") #'imenu)
(define-key global-map [remap recentf-open] #'recentf-open-files)(autoload #'recentf-open-files "recentf"  nil 'interactive)

;;; my-compats-29.el ends here

(provide 'my-compats-29)

;; Local Variables:
;; no-byte-compile: t
;; End:

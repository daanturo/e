;; -*- lexical-binding: t; -*-

(require 'compatold-24)

(require 'advice)

;;; Defs

(defalias #'add-face-text-property #'ignore)
(defalias #'set-default-toplevel-value #'set-default) ; used by `setq!'
(defvar completion-in-region-function nil)
(defvar lisp-mode-symbol-regexp "\\(?:\\sw\\|\\s_\\|\\\\.\\)+")

(defun my-emacs24.4-isearch-insert|yank-symbol-or-char ()
  (interactive)
  (isearch-yank-string (or (thing-at-point 'symbol)
                           (thing-at-point 'char))))

(defun my-emacs24.4-isearch-forward-symbol-at-point ()
  (interactive)
  (isearch-forward-symbol nil 1)
  (my-emacs24.4-isearch-insert|yank-symbol-or-char))

;;; Adaptations

;; Let the forward-compatibility package handle non-symbol advice functions
;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=59820
(defun my-emacs24.4-advice--ensure-symbol (func)
  (if (symbolp func)
      func
    (let* ((sym (intern (format "nadvice--compat--%S" func))))
      (unless (fboundp sym)
        (defalias sym func))
      sym)))
(defadvice advice-add (around -my-emacs24.4-paches-a 0
                              (symbol how function &optional props &rest _) activate)
  ""
  (if (symbolp function)
      ad-do-it
    ;; support for `lambda' as FUNCTION
    (let* ((advice-fn (my-emacs24.4-advice--ensure-symbol function)))
      (ad-set-arg 2 advice-fn)
      ad-do-it)))
(defadvice advice-remove (around -my-emacs24.4-paches-a 0
                                 (symbol function) activate)
  ""
  (if (symbolp function)
      ad-do-it
    ;; support for `lambda' as FUNCTION
    (let* ((advice-fn (my-emacs24.4-advice--ensure-symbol function)))
      (ad-set-arg 1 advice-fn)
      ad-do-it)))

;; (defadvice advice-add (around -my-emacs24.4-paches-a disable))
;; (ad-disable-advice #'advice-add 'around '-my-emacs24.4-paches-a)

;; (ad-deactivate #'advice-add)


;; (advice-add #'pwd :after (lambda (&rest _) ))
;; (advice-add #'pwd :after #'ignore)


;; (error "Stack overflow in equal")
(defun my-emacs24.4-add-hook-once--override-a (hook/s func &optional depth local)
  (-let* ((hooks (ensure-list hook/s)))
    (-let* ((sym (gensym "my-add-hook-once")))
      (defalias sym (lambda (&rest _)
                      (my-remove-hook/s hooks (list func) local)
                      (my-remove-hook/s hooks (list sym) local)))
      (my-add-hook/s hooks (list sym) nil local))
    (my-add-hook/s hooks (list func) depth local)))
(advice-add #'my-add-hook-once :override #'my-emacs24.4-add-hook-once--override-a)

(gv-define-setter alist-get (value key alist &rest _)
  `(push '(,key . ,value)
         ,alist))

(advice-add #'my-run-at-future-hhmm-time :override #'ignore) ; `encode-time' arguments error

(defun my-emacs24.4-symbol-function--a (func sym &rest args)
  (cond
   ;; instead of raising an error
   ((null sym)
    nil)
   (t
    (apply func sym args))))
(advice-add #'symbol-function :around #'my-emacs24.4-symbol-function--a)

(with-current-buffer "*Messages*"
  (special-mode))

(electric-indent-mode)

;;;; ** Numeric comparison functions =, <, >, <=, >= can now take many arguments.

(defun my-emacs24.4--compare (func num nums)
  (let* ((lhs num)
         (right nums)
         (res t))
    (while (and res right)
      (let* ((rhs (car-safe right)))
        (cond
         ((funcall func lhs rhs)
          (setq lhs rhs)
          (setq right (cdr-safe right)))
         (t
          (setq res nil)))))
    res))

(defun my-emacs24.4--extend-compare (func)
  (let* ((orig-func (intern (format "%s-original-%s" #'my-compat-24.4 func))))
    (unless (fboundp orig-func)
      (defalias orig-func (symbol-function func)))
    (defalias
      func
      (lambda (num &rest nums) (my-emacs24.4--compare orig-func num nums))
      (format "Variadic version of `%s'." orig-func))
    func))

(my-emacs24.4--extend-compare #'=)
(my-emacs24.4--extend-compare #'<)
(my-emacs24.4--extend-compare #'>)
(my-emacs24.4--extend-compare #'<=)
(my-emacs24.4--extend-compare #'>=)


;;; Settings

;; ease debugging
(require 'subr-x)
;; (advice-remove #'require #'compat--require)

(define-key global-map (kbd "M-s .") #'my-emacs24.4-isearch-forward-symbol-at-point)
(define-key isearch-mode-map (kbd "<return>") #'isearch-exit)
(define-key isearch-mode-map (kbd "C-M-w") #'my-emacs24.4-isearch-insert|yank-symbol-or-char)

;;; my-compats-24.4.el ends here

(provide 'my-compats-24.4)

;; Local Variables:
;; no-byte-compile: t
;; End:

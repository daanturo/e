;; -*- lexical-binding: t; -*-

;;; Defs

;; https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=69ec333eab0b801949d33ef5ae505addc9061793

(defcustom completion-pcm-leading-wildcard nil ".")

;;; Adaptations

(defun my-emacs31-completion-pcm--string->pattern--substring-partial-a
    (func string &optional point &rest args)
  (let* ((pattern (apply func string point args)))
    (when completion-pcm-leading-wildcard
      (when (stringp (car pattern))
        (push 'prefix pattern)))
    pattern))

(advice-add
 #'completion-pcm--string->pattern
 :around #'my-emacs31-completion-pcm--string->pattern--substring-partial-a)

;;; Settings

;;; my-compats-31.el ends here

(provide 'my-compats-31)

;; Local Variables:
;; no-byte-compile: t
;; End:

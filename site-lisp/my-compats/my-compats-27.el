;; -*- lexical-binding: t; -*-

(require 'compat-27)

;;; Defs

(unless (functionp #'proper-list-p)
  (defun proper-list-p (object)
    (and (listp object)
         (equal (cdr (last object)) nil)
         (length object))))

;;; Adaptations

(defun my-emacs27-encode-time--a (func time-list-or-obsolete-second &rest obsolescent-arguments)
  (if obsolescent-arguments
      ;; already backward compatible
      (progn
        (apply func time-list-or-obsolete-second obsolescent-arguments))
    ;; only take '(SECOND MINUTE HOUR DAY MONTH YEAR &optional ZONE)
    (progn
      (apply func time-list-or-obsolete-second))))
(advice-add #'encode-time :around #'my-emacs27-encode-time--a)

;; variadic
(defun my-emacs27-setq-local-a (func &rest args)
  (let* ((returneds (cl-loop for (var val) on args by #'cddr
                             collect (set (make-local-variable var)
                                          val))))
    (car (last returneds))))
(advice-add #'setq-local :around #'my-emacs27-setq-local-a)

(put 'make-empty-file 'interactive-form
     '(interactive (list (read-file-name "`make-empty-file': ") t)))

;; Pre-27, `self-insert-command' doesn't support "&optional C"
(defun my-emacs27-self-insert-command-optional-char-override-a (n &optional char)
  (dlet ((last-command-event char))
    (self-insert-command n)))
(advice-add #'my-self-insert-command :override #'my-emacs27-self-insert-command-optional-char-override-a)

;;; Settings

;;; my-compats-27.el ends here

(provide 'my-compats-27)

;; Local Variables:
;; no-byte-compile: t
;; End:

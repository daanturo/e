;; -*- lexical-binding: t; -*-

(require 'dash)
(require '00-my-core-macros)

(require 'ert)

(ert-deftest my-environment-tests nil

  ;; Wayland?
  (should (equal "wayland" (getenv "XDG_SESSION_TYPE")))
  (should (executable-find "wl-paste"))

  ;;
  )

(ert-deftest my-pure-functions-test nil

  (should (my-plist? '(:a 1 :b 2)))
  (should (my-plist? '()))
  (should-not (my-plist? '(:a 1 :b)))
  (should-not (my-plist? '(:a 1 b 2)))

  ;;
  )

;;; my-general-tests.el ends here

(provide 'my-general-tests)

;; -*- lexical-binding: t; -*-

(require 'dash)
(require 'ert)

(ert-deftest my-test-my-lang-javascript-jsx-dwim-/-menu-item-filter ()
  (cl-flet*
      ((fn
         (text0 text1)
         (with-temp-buffer
           (tsx-ts-mode)
           (insert text0)
           (save-excursion (insert text1))
           (my-lang-javascript-jsx-dwim-/-menu-item-filter-treesit t))))

    (should-not (fn "2 " " 3"))

    (should-not
     (fn
      "function Fn() {
  return <div class='hi' "
      "/>
}"))
    (should
     (fn
      "function Fn() {
  return <div class='hi' "
      "
}"))

    (should-not
     (fn
      "function Fn() {
  return (
    <div class='hi' "
      "/>
  )
}"))
    (should
     (fn
      "function Fn() {
  return (
    <div class='hi' "
      "
  )
}"))

    (should-not
     (fn
      "function Fn() {
  return <div/> "
      "
}"))
    (should
     (fn
      "function Fn() {
  return <div "
      "
}"))

    (should-not
     (fn
      "export default function Board() {
  return (
    <>
      <div className='board-row'>
        <Square />
        <Square "
      "/>
        <Square />
      </div>
      <div className='board-row'>
        <Square />
        <Square />
        <Square />
      </div>
      <div className='board-row'>
        <Square />
        <Square />
        <Square />
      </div>
    </>
  );
}"))
    (should
     (fn
      "export default function Board() {
  return (
    <>
      <div className='board-row'>
        <Square />
        <Square "
      "
        <Square />
      </div>
      <div className='board-row'>
        <Square />
        <Square />
        <Square />
      </div>
      <div className='board-row'>
        <Square />
        <Square />
        <Square />
      </div>
    </>
  );
}"))

    (should-not
     (fn
      "export default function Board() {
  return (
    <>
      <div className='board-row'>
        <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
        <Square value={squares[1]} onSquareClick={() => handleClick(1)} />"
      "
        <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
      </div>
    </>
  );
};"))
    (should
     (fn
      "export default function Board() {
  return (
    <>
      <div className='board-row'>
        <Square value={squares[0]} onSquareClick={() => handleClick(0)} />
        <Square value={squares[1]} onSquareClick={() => handleClick(1)} "
      "
        <Square value={squares[2]} onSquareClick={() => handleClick(2)} />
      </div>
    </>
  );
};"))

    (should-not
     (fn
      "export default function Fn() {
  return <p> {2 /"
      " 3}</p>;
}"))
    (should-not
     (fn
      "export default function Fn() {
  return <p> {2 "
      " 3}</p>;
}"))

    (should-not
     (fn
      "
export default function Game() {
  return (
    <div className='game'>
      <div className='game-board'>
        <Board "
      "/>
      </div>
      <div className='game-info'>
        <ol>{/*TODO*/}</ol>
      </div>
    </div>
  );
}"))
    (should (fn "
export default function Game() {
  return (
    <div className='game'>
      <div className='game-board'>
        <Board "
                "
      </div>
      <div className='game-info'>
        <ol>{/*TODO*/}</ol>
      </div>
    </div>
  );
}"))


    (should-not
     (fn
      "
export default function Game() {
  return (
    <div className='game'>
      <div className='game-board'>
        <Board />
      </div>
      <div className='game-info'>
        <ol>{"
      "}</ol>
      </div>
    </div>
  );
}"))

    (should
     (fn
      "export default function Game() {
  const [history, setHistory] = useState([Array(9).fill(null)]);
  const currentSquares = history.at(-1);

  return (
    <div className='game'>
      <div className='game-board'>
        <Board />
      </div>
      <div"
      "
      <div className='game-info'>
        <ol></ol>
      </div>
    </div>
  );
}"))

    ;;
    )
  ;;
  )

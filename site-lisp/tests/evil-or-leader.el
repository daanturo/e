;; -*- lexical-binding: t; -*-

(require 'dash)

'(
  ;;

  (git-timemachine-mode my-leader-mode) ; workaround "n" "p"

  ;;
  )

(defun my-ert-check-evil-or-leader (&rest target/s)
  (message "`my-ert-check-evil-or-leader': %S %S" (buffer-name) major-mode)
  (-let* ((modes '(evil-local-mode my-leader-mode)))
    (-every
     (lambda (mode)
       (if (member mode target/s)
           (symbol-value mode)
         (not (symbol-value mode))))
     modes)))

(defmacro my-test-with-evil-or-leader (&rest body)
  (declare (debug t))
  `(save-window-excursion
     (save-excursion
       (dlet ((inhibit-message t))
         ,@body))))

(ert-deftest my-test-evil-or-leader-synchrounous-tests ()

  ;; `Custom-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (progn
                          (customize-group 'emacs)
                          ;; the first call doesn't return buffer?
                          (customize-group 'emacs))
     (should (my-ert-check-evil-or-leader 'evil-local-mode))))

  ;; `Info-mode'
  (my-test-with-evil-or-leader
   (info "emacs")
   (with-current-buffer "*info*"
     (should (my-ert-check-evil-or-leader 'my-leader-mode))))

  ;; `Man-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (man "emacs")
     (should (my-ert-check-evil-or-leader 'my-leader-mode))))

  ;; `dired-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (dired "/tmp")
     (should (my-ert-check-evil-or-leader 'my-leader-mode))))

  ;; `eat-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (eat)
     (should (my-ert-check-evil-or-leader))))

  ;; `eshell-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (eshell)
     (should (my-ert-check-evil-or-leader))))

  ;; `grep-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (grep "grep -r def")
     (should (my-ert-check-evil-or-leader 'my-leader-mode))))

  ;; `help-mode'
  (my-test-with-evil-or-leader
   (describe-symbol '=)
   (with-current-buffer (help-buffer)
     (should (my-ert-check-evil-or-leader 'my-leader-mode))))

  ;; `helpful-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (helpful-symbol '=)
     (should (my-ert-check-evil-or-leader 'my-leader-mode))))

  ;; `inferior-emacs-lisp-mode'
  (my-test-with-evil-or-leader
   (ielm "*ielm*")
   (with-current-buffer "*ielm*"
     (should (my-ert-check-evil-or-leader))))

  ;; `inferior-python-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (process-buffer (run-python))
     (should (my-ert-check-evil-or-leader))))

  ;; `inspector-mode'
  (my-test-with-evil-or-leader
   (inspector-inspect-expression nil)
   (with-current-buffer "*inspector*"
     (should (my-ert-check-evil-or-leader 'my-leader-mode))))

  ;; `ivy-occur-mode'
  (my-test-with-evil-or-leader
   (let* ((cmd0 (keymap-global-lookup "A-C-H-M-S-s-s")))
     (with-temp-buffer
       (keymap-global-set "A-C-H-M-S-s-s" #'swiper)
       (insert "def")
       (execute-kbd-macro (kbd "A-C-H-M-S-s-s def C-c C-o"))
       (keymap-global-set "A-C-H-M-S-s-s" cmd0)
       (with-current-buffer (current-buffer)
         (should (my-ert-check-evil-or-leader 'my-leader-mode))))))

  ;; `lisp-interaction-mode'
  (my-test-with-evil-or-leader
   (with-temp-buffer
     (lisp-interaction-mode)
     (should (my-ert-check-evil-or-leader 'evil-local-mode))))

  ;; `magit-status-mode'
  (my-test-with-evil-or-leader
   (magit-status)
   (should (my-ert-check-evil-or-leader 'my-leader-mode)))

  ;; `messages-buffer-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer "*Messages*"
     (should (my-ert-check-evil-or-leader 'evil-local-mode))))

  ;; `occur-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (find-file early-init-file)
     (dlet ((occur-hook '()))
       (occur "^("))
     (with-current-buffer "*Occur*"
       (should (my-ert-check-evil-or-leader 'my-leader-mode)))))

  ;; `package-menu-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (list-packages t)
     (should (my-ert-check-evil-or-leader 'my-leader-mode))))

  ;; `scheme-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (find-file (make-temp-file "my-test" nil "test.scm"))
     (should (my-ert-check-evil-or-leader 'evil-local-mode))))

  ;; `sh-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (find-file (make-temp-file "my-test" nil "test.sh"))
     (should (my-ert-check-evil-or-leader 'evil-local-mode))))

  ;; `shell-mode'
  (my-test-with-evil-or-leader
   (with-current-buffer (shell)
     (should (my-ert-check-evil-or-leader))))

  ;;
  )

(ert-deftest my-test-evil-or-leader-asynchrounous-tests ()

  ;; `ein'
  (my-test-with-evil-or-leader
   (require 'ein-jupyter)
   (-let* ((dir "~/temp/jupyter")
           (py (file-name-concat dir "test.py"))
           (ipynb (file-name-concat dir "test.ipynb")))
     (make-directory dir t)
     (dlet ((default-directory dir))
       (make-empty-file py t)
       (shell-command (my-s$ "p2j ${py}"))
       (unless (ein:jupyter-server-process)
         (ein:jupyter-server-start ein:jupyter-server-command dir))
       (switch-to-buffer *ein:jupyter-server-buffer-name*)
       (with-current-buffer (current-buffer)
         (should (my-ert-check-evil-or-leader 'evil-local-mode)))
       (-some-->
           (my-buffers-with-major-mode/s 'ein:notebooklist-mode) car
           (with-current-buffer it
             (should (my-ert-check-evil-or-leader 'my-leader-mode))))
       (my-ein-auto-open-notebook
        ipynb nil
        (lambda (buf)
          (with-current-buffer buf
            (my-log*🌈 "ein.el" (my-ISO-time) (buffer-name) evil-local-mode)
            (should (my-ert-check-evil-or-leader 'evil-local-mode))
            (-some--> (get-buffer-window buf) (quit-window nil it))))))))

  ;; `cider-repl-mode'
  (my-test-with-evil-or-leader
   (require 'cider)
   (-let* ((val cider-repl-pop-to-buffer-on-connect))
     (setq cider-repl-pop-to-buffer-on-connect nil)
     (setq cider-connection-message-fn nil)
     (my-add-hook-once
       'cider-connected-hook
       (lambda ()
         (setq cider-repl-pop-to-buffer-on-connect val)
         (-some-->
             (-find
              (lambda (buf)
                (with-current-buffer buf
                  (equal 'cider-repl-mode major-mode)))
              (buffer-list))
           (with-current-buffer it
             (my-prn🌈
              (buffer-name)
              (list evil-local-mode my-leader-mode)
              (should (my-ert-check-evil-or-leader)))))))
     (process-buffer (cider-jack-in-clj nil))
     nil))

  ;;
  )

;; Local Variables:
;; no-byte-compile: t
;; End:

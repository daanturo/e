#!/usr/bin/env bash

if [[ ! -f "${XDG_RUNTIME_DIR}/emacs/test-daemon" ]]; then

    emacs --bg-daemon=test-daemon --eval "
(my-add-hook-once
 'before-make-frame-hook
 (lambda (&rest _)
   (ignore-errors
     (profiler-start 'cpu))
   (my-add-hook-once
    'post-command-hook
    (lambda (&rest _)
      (profiler-stop)
      (profiler-report)))))
"

fi

emacsclient -s "test-daemon" -c

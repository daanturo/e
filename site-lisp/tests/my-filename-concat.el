;; -*- lexical-binding: t; -*-

(require 'ert)

(progn
  (ert-deftest test-my-filename-concat ()

    (should (equal (my-filename-concat "") ""))
    (should (equal (my-filename-concat "f") "f"))
    (should (equal (my-filename-concat "" "f") "f"))
    (should (equal (my-filename-concat "d" "f") "d/f"))
    (should (equal (my-filename-concat "d/" "f") "d/f"))
    (should (equal (my-filename-concat "d" "/f") "d//f"))

    (should (equal (my-filename-concat "d" "f") "d/f"))
    (should (equal (my-filename-concat "d" "f") "d/f"))
    (should (equal (my-filename-concat "d" "f" "c") "d/f/c"))
    (should (equal (my-filename-concat "d/" "f") "d/f"))
    (should (equal (my-filename-concat "d//" "f") "d//f"))
    (should (equal (my-filename-concat "d/" "f/" "c") "d/f/c"))
    (should (equal (my-filename-concat "d") "d"))
    (should (equal (my-filename-concat "d/") "d/"))
    (should (equal (my-filename-concat "d" "") "d"))
    (should (equal (my-filename-concat "d" "" "" "" nil) "d"))
    (should (equal (my-filename-concat "" "f") "f"))
    (should (equal (my-filename-concat "" "") ""))

    ;;
    )
  (ert 'test-my-filename-concat))

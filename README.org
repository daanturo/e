* # Installation

#+begin_src bash
  # Install Emacs

  sudo apt install --no-install-recommends emacs git
  sudo dnf -y install emacs emacs-el git
  sudo guix package --install emacs git
  sudo nix-env -iA nixpkgs.emacs nixpkgs.git
  sudo pacman -Sy --needed emacs git
  sudo yum -y install emacs emacs-el git
  sudo zypper -y install --no-recommends emacs emacs-el git
#+end_src

#+begin_src bash
  mkdir -p ~/.config/

  # Assume that ~/.config/emacs doesn't exist
  git clone https://gitlab.com/daanturo/e.git ~/.config/emacs
#+end_src

#+begin_src bash
  env MY_EMACS_SYNC=true emacs -nw --kill
#+end_src

#+begin_src bash
  # Optional full packages installation
  env MY_EMACS_SYNC=true emacs -nw --eval "(my-unkit-sync)" --kill
#+end_src

* # Update/Pull from this repository

Run from within Emacs

*Warning*: This will discard any local changes in source-controlled files.

#+BEGIN_SRC emacs-lisp
M-x my-pull-config
#+END_SRC

Or from CLI:

#+begin_src bash
# Find the first config directory
pushd ~/.config/doom || pushd ~/.emacs.d || pushd ~/.config/emacs ; pwd ; git fetch ; git reset --hard origin/main ; popd
#+end_src

;;-*-coding: utf-8;-*-
(define-abbrev-table 'emacs-lisp-mode-abbrev-table
  '(
    (";;;" ";;;###autoload" nil :count 0)
   ))

(define-abbrev-table 'global-abbrev-table
  '(
    ("impor" "import" nil :count 1)
    ("inser" "insert" nil :count 1)
   ))


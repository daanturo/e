﻿;; -*- lexical-binding: t; -*-
(set-default-toplevel-value 'lexical-binding t)

;;; Preparations

(defun my-profiler-profile-pre-command-start (&optional stop-hook)
  (defvar my-profiler-profile-pre-command--load-time-form-list '())
  (let* ((time-form-lst '())
         (stop-hook (or stop-hook 'pre-command-hook))
         (benchmark-load-a
          (lambda (fn &rest args)
            (let* ((time0 (current-time))
                   (retval (apply fn args))
                   (delta-time (float-time (time-since time0))))
              (push (list delta-time `(,fn ,@args)) time-form-lst)
              retval)))
         (benchmark-require-a
          (lambda (fn feat &rest args)
            (if (member feat features)
                (apply fn feat args)
              (apply benchmark-load-a fn feat args)))))
    (profiler-start 'cpu)
    (advice-add #'load :around benchmark-load-a)
    (advice-add #'require :around benchmark-require-a)
    (defun my-profiler-profile-pre-command-stop ()
      (profiler-stop)
      (save-window-excursion (profiler-report))
      (advice-remove #'require benchmark-require-a)
      (advice-remove #'load benchmark-load-a)
      (push (cl-sort time-form-lst #'> :key #'car)
            my-profiler-profile-pre-command--load-time-form-list)
      (remove-hook stop-hook 'my-profiler-profile-pre-command-stop))
    
    (add-hook stop-hook 'my-profiler-profile-pre-command-stop nil)))
;; Analyze startup
(when (and (not (member (getenv "MY_EMACS_PROFILE") '(nil "")))
           (not (profiler-cpu-running-p)))
  (my-profiler-profile-pre-command-start))
;; (my-profiler-profile-pre-command-start 'emacs-startup-hook)

;; prevent the white flash before theme is applied, revert later when no theme
;; can be found, do this early because some processing can be executed before
;; config.el
(defvar my-emacs-default-face-colors nil)
(when (and (display-graphic-p) (null after-init-time))
  (setq my-emacs-default-face-colors (list
                                      :background (face-attribute 'default :background)
                                      :foreground (face-attribute 'default :foreground)))
  (set-face-attribute 'default (selected-frame) :background "#2D2D2D" :foreground "#FFFFFF"))

(require 'my-early-init (concat (file-name-directory (directory-file-name (or load-file-name buffer-file-truename))) "early-init"))
;; Set like Doom since too many libraries try to save stuffs directly under `user-emacs-directory'
(when (null my-emacs-kit) (setq user-emacs-directory my-user-emacs-local-dir/))

(defvar inhibit-message nil)         ; Introduced in Emacs 25.

(setq my-elisp-no-compile (< emacs-major-version 25)) ; problems with `subr-x'

;; Set early to deal to questions's origins (`toggle-debug-on-quit')
(setq enable-recursive-minibuffers t)

;;NOTE load order: `my-useelisp' (to pull essential libraries) -> `nadvice'?
;;(for `compat' and patches) -> `compat'? -> `dash' -> `my-compats'? ->
;;`00-my-core-macros' -> others

;;; Ensure essential packages

(defvar my-useelisp-build-dir
  (concat my-user-emacs-local-dir/ (format "my-useelisp-%s" emacs-major-version)))
(add-to-list 'load-path my-lisp-dir/)
(add-to-list 'load-path my-useelisp-build-dir) ; prefer compiled to `my-lisp-dir/'

(add-to-list 'load-path my-site-lisp-dir/)

(let ((load-prefer-newer t))
  (require '00-my-useelisp))

;; `package-quickstart-file''s information about "installed" packages after
;; copying the config dir may raise errors, Emacs may not recognize package
;; installed in `package-user-dir' as "installed"
(with-eval-after-load 'package (when (my-useelisp-should-install-p)
                                 (delete-file package-quickstart-file)
                                 (delete-file (concat (file-name-sans-extension package-quickstart-file) ".elc"))))

;; The elpa repo is faster with https?
(my-useelisp-repo 'nadvice "https://git.savannah.gnu.org/git/emacs/elpa.git" :branch "externals/nadvice" :active (not (fboundp #'advice-add)))
(when (not (fboundp #'advice-add)) (require 'nadvice))
(my-useelisp-repo 'compatold "https://github.com/daanturo/compatold" :active (version< emacs-version "24.4"))
(my-useelisp-repo 'compat "https://github.com/emacs-compat/compat" :active my-emacs-not-latest)
(when (version< emacs-version "24.4") (provide 'subr-x) (require 'compatold))
(my-useelisp-repo 'seq "https://git.savannah.gnu.org/git/emacs/elpa.git" :branch "externals/seq" :active my-emacs-not-latest)
(when (not (fboundp #'seqp)) (require 'seq))
(when my-emacs-not-latest (require 'compat))
(my-useelisp-repo 'dash "https://github.com/magnars/dash.el" :globs!comp '("dash-functional*")) ; for performance, use `dash' over `seq' when possible
(my-useelisp-repo 's "https://github.com/magnars/s.el")
(my-useelisp-repo 'f "https://github.com/rejeep/f.el" :globs!comp '("*shortdoc*"))

(require 'dash)
(require 'map nil 'noerror)
(defconst my-doom-emacs-dir (nth 0 (my-useelisp-repo 'doom-emacs "https://github.com/doomemacs/doomemacs" :globs '("lisp/doom-lib.el"))))
(when (not (my-compiled-function-p 'doom-unquote))
  (load (my-filename-join my-useelisp-build-dir "doom-lib") nil 'quiet))

(my-useelisp-install my-lisp-dir/) ; put symlinks in `my-useelisp-build-dir'
(my-compats)

(my-useelisp-repo 'ht "https://github.com/Wilfred/ht.el")
(my-useelisp-repo 'queue "https://git.savannah.gnu.org/git/emacs/elpa.git" :branch "externals/queue" :active (< emacs-major-version 25)) ; for `undo-tree'
(my-useelisp-repo 'undo-tree "https://gitlab.com/tsc25/undo-tree.git" :active (< emacs-major-version 25))
(my-useelisp-repo 'undo-fu "https://codeberg.org/ideasman42/emacs-undo-fu.git" :active (and (<= 25 emacs-major-version) (< emacs-major-version 28)))
(my-useelisp-repo 'ivy "https://git.savannah.gnu.org/git/emacs/elpa.git" :branch "externals/ivy" :active (not (fboundp #'fido-vertical-mode)))

(defconst my-useelisp-autoload--file
  (my-useelisp-ensure-autoload 'load-now))

(require '00-my-core-macros)
(require '01-my-core-functions)

;;; Core initializations

(defvar my-process-environment-orig (purecopy process-environment))
(setq my-env-file (file-name-concat my-user-emacs-local-dir/ "my-env"))

(when (not (file-exists-p my-env-file))
  (my-env-generate-env-file))

(when (and (daemonp) (file-exists-p my-env-file))
  (my-env-set-from-file my-env-file)
  (my-env-set-exec-path))

;; Set early to prevent incompatible calls with non-POSIX shells
(when-let* ((_ (not (string-suffix-p "/bash" shell-file-name)))
            (path (executable-find "bash")))
  (setenv "SHELL" path)
  (setq-default shell-file-name path))

;; Put here to answer package questions
(setq use-short-answers t)              ; Emacs >=28

;; Put this here in case config.el is screwed, while trying to ensure that it's evaluated after Doom's
(setq recentf-max-saved-items 8192)

(add-hook 'treesit-extra-load-path "/var/guix/profiles/per-user/root/guix-profile/lib/tree-sitter/")
(add-hook 'treesit-extra-load-path "~/.guix-profile/lib/tree-sitter/")
(add-hook 'treesit-extra-load-path "~/.local/share/tree-sitter-lib/")

(my-pre-config-evil)

;;; Others

(provide 'my-init)

(require 'emacs-my-q (file-name-concat my-emacs-conf-dir/ "emacs-my-q"))

;; Remember to set before installing packages, so customized the package list
;; will be saved in another file
(setq custom-file (file-name-concat my-emacs-conf-dir/ "custom.el"))

;; `package-activate-all' enables packages without loading package.el
(cond
 (my-emacs-kit
  (ignore))
 ((fboundp #'package-activate-all)
  (package-activate-all))
 (:else
  (package-initialize)))

(cond
 ((or (my-get-non-empty-env "MY_EMACS_SYNC")
      (and (bound-and-true-p package-user-dir)
           (my-directory-not-exist-or-empty-p package-user-dir)))
  (load (file-name-concat my-emacs-conf-dir/ "packages") nil t)))

(when (and (not my-emacs-kit) (not noninteractive))
  (load (file-name-concat my-emacs-conf-dir/ "config") nil t))

;;; init.el ends here

;; Local Variables:
;; no-byte-compile: t
;; End:

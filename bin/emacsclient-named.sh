#!/usr/bin/env bash

socket_name="$1"

# set -x

# test if daemon with that name is running, else start new
if emacsclient -s "$socket_name" -e "(ignore)" &>/dev/null; then

    emacsclient -s "$socket_name" "${@:2}"

else

    emacs --bg-daemon="$socket_name"
    emacsclient -s "$socket_name" "${@:2}"

#     # the daemon doesn't survive last frame being killed by this way
#     emacs --eval "(progn
#   (set-variable 'server-name \"$socket_name\")
#   (server-start)
#   ;;
#   )
# " "${@:2}"

fi

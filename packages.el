;; -*- lexical-binding: t; -*-

(require 'my-pkg)

;; (my-backtrace)

;; (unless my-emacs-kit
;;   (message "Run `%s' to install declared but not installed packages."
;;            'my-pkg-install-added))

(when (not my-emacs-kit)
  (package-initialize))

;;; Settings

(setq package-install-upgrade-built-in t)

;;; Important packages

;; auto-complete
(when (<= 27 emacs-major-version)
  (my-pkg! 'corfu :now t)) ; :lisp-dir "extensions"

;; interactive project-wide search
(when (<= 27 emacs-major-version)
  (my-pkg! 'consult :now t))

;; vertical mini-buffer completions, Emacs 28+ has
;; `fido-vertical-mode', but `vertico' is much better
(when (<= 27 emacs-major-version)
  (my-pkg! 'vertico :now t))

;;; Other packages

(my-pkgs!
 '(
   ;; format: off
   
   (dumb-jump :unstable t)
   (fcitx :unstable t)
   (helpful :unstable t)
   (lsp-mode :unstable t) dap-mode dape
   (treemacs :unstable t) lsp-treemacs treemacs-nerd-icons
   (which-key :prefer-built-in t)
   affe fzf
   all-the-icons-completion kind-icon treemacs-all-the-icons insert-kaomoji nerd-icons-completion
   apheleia format-all
   async
   auctex cdlatex
   bash-completion
   citar citar-org-roam (org-ref :unstable t)
   coercion grugru
   colorful-mode stripes rainbow-delimiters hl-todo
   consult-dash consult-dir consult-lsp consult-yasnippet embark-consult counsel
   corfu cape
   crux mwim drag-stuff
   devil key-chord hydra ivy-hydra
   doom-modeline minions breadcrumb nyan-mode
   dtrt-indent indent-bars
   edit-indirect edit-server crdt
   elisp-autofmt elisp-demos highlight-quoted highlight-function-calls
   ellama gptel
   envrc ; direnv
   epkg
   evil goto-chg evil-surround evil-multiedit ; (evil-collection :unstable t) ; don't need evil-collection if non-motion/normal state is used in other special modes
   expreg expand-region
   iedit (multiple-cursors :unstable t) phi-search
   inspector tree-inspector macrostep
   jinx academic-phrases flymake-proselint langtool (guess-language :unstable t)
   keycast
   language-id
   lispy lispyville parinfer-rust-mode
   magit forge orgit-forge magit-delta git-link diff-hl git-auto-commit-mode git-timemachine
   org-contrib org-modern engrave-faces grip-mode
   org-roam org-roam-ui deft consult-org-roam
   puni smartparens
   sideline-flymake
   swiper
   tempel tempel-collection
   treesit-fold
   vertico consult marginalia embark orderless wgrep
   vundo undo-fu-session

   ;; format: on
   ))




;; (my-pkg! 'typo "https://git.sr.ht/~pkal/typo")


(my-pkgs! '(biblio))


(my-pkgs!
 '(jupyter
   ein code-cells
   ;; for code completions in EIN, currently
   elpy))

(my-pkg! 'sage-shell-mode)

(my-pkgs!
 '(dired-du
   disk-usage dired-preview diredfl trashed nerd-icons-dired))
;; peep-dired

;; `dired-hacks' is a collection of packages in a single git repo, use
;; VC to simplify installing all
(my-pkgs! '((dired-hacks :url "https://github.com/Fuco1/dired-hacks")))

;; the latest TRAMP may break Doom's autoloading mechanism

;; (my-pkg! 'lispy "https://github.com/daanturo/lispy")

(my-pkg!
 'consult-xdg-recent-files
 "https://github.com/daanturo/consult-xdg-recent-files.git")

;; install instruction for non-package.el is complex
(my-pkg! 'eat)

(my-pkg! 'starhugger :vc t :unstable t)

(my-pkgs!
 `((doom-themes :unstable t)
   ef-themes
   ,@(and (not my-emacs-not-latest) '(modus-themes))))

(my-pkgs! '(yasnippet yasnippet-snippets))
(my-pkg!
 'doom-snippets
 "https://github.com/doomemacs/snippets"
 :files '("*.el" "*"))

;;


(my-pkg! 'recentf :disable t) ; Don't let Doom configure it early


(my-pkg! 'better-jumper :disable t) ; xref.el looks good enough
(my-pkg! 'corfu-history :disable t) ; don't change the completion candidates order by usage
(my-pkg! 'evil-embrace :disable t) ; Doom's configuration forces it to load `expand-region' eagerly
(my-pkg! 'evil-escape :disable t) ; in insert-state, when region is active, "C-g" goes to normal-state instead of unmarking the region
(my-pkg! 'explain-pause-mode :disable t) ; too often disruptions as it frequently asks for bug reports
(my-pkg! 'writegood-mode :disable t) ; let me use passive forms

(defvar my-ignored-packages '())

(my-pkgs! my-ignored-packages :ignore t)

(when my-emacs-kit
  (my-pkg! 'eldoc :prefer-built-in (not my-emacs-not-latest))
  (my-pkg! 'use-package :prefer-built-in (not my-emacs-not-latest)))

;; ;; The pulled repo is too heavy
;; (my-pkg! 'org :prefer-built-in (not my-emacs-not-latest))


(my-pkg! 'zoutline :ignore t)           ; beg `lispy' not to load `org'

(provide 'my-packages)

(defvar my-local-packages-el (file-name-concat my-emacs-conf-dir/ "my-local-packages.el"))

(with-eval-after-load 'package
  (when (and (null my-emacs-kit)
             (bound-and-true-p package-quickstart)
             (bound-and-true-p package-quickstart-file)
             (not (file-exists-p package-quickstart-file))
             (fboundp #'package-quickstart-refresh))
    (package-quickstart-refresh)))

(my-require-provided 'my-local-packages my-local-packages-el 'noerror t)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; -*- lexical-binding: t; -*-

;;; Base configurations

(when (not my-emacs-kit)
  (my-require-provided 'my-custom custom-file 'noerror nil)) ;; (format "\n%S\n" '(message "Loading %S (`custom-file')" load-file-name))

(setq disabled-command-function nil)

(defconst my-edit-modes '(prog-mode text-mode conf-mode diff-mode bibtex-mode))
(defconst my-edit-mode-hooks (-map #'my-mode->hook my-edit-modes))
(defconst my-edit-mode-keymaps (-map #'my-mode->map-symbol my-edit-modes))

;; When starts with a file (some args are not prefixed with "-"?), don't show the splash screen alongside it
(defconst my-initial-file-in-CLI-args
  (-some (lambda (arg) (not (string-prefix-p "-" arg))) (cdr command-line-args))
  "Guess if Emacs is opened with a file.")

;; When "--load", the initial buffer choice will override the window layout made
;; by `find-file', TODO: report this issue
(when (< 0 (length (cl-rest command-line-args)))
  (my-add-advice-once #'+doom-dashboard-init-h :override #'ignore)
  (setq initial-buffer-choice nil))

;;;; Early keys configs 

(my-when-graphical ;; Key sequences: "<C-[im\[]>"
 (define-key input-decode-map [?\C-i] [C-i]) (define-key input-decode-map [?\C-\S-i] [C-S-i])
 (define-key input-decode-map [?\C-m] [C-m]) (define-key input-decode-map [?\C-\S-m] [C-S-m])
 (define-key input-decode-map [?\C-\[] [C-\[]) (define-key input-decode-map [?\C-\S-\]] [C-S-\]]))

(defconst my-leader-key "SPC")
(defconst my-leader-alt-key "M-SPC") (setq-default doom-leader-alt-key my-leader-alt-key)
(defvar my-leader-spc-map (make-sparse-keymap))
(define-prefix-command 'my-leader-spc-prefix-cmd 'my-leader-spc-map)
(my-bind doom-leader-alt-key #'my-leader-spc-prefix-cmd)
(my-bind :vi '(motion) (list my-leader-key) #'my-leader-spc-prefix-cmd)
(add-hook 'special-mode-hook #'my-leader-spc-key-mode-maybe) ; using a minor mode 

(my-safe-call #'key-chord-mode)

(defvar my-override-keymap-alist '() "Meant to be part of `emulation-mode-map-alists'.")
(add-to-ordered-list 'emulation-mode-map-alists 'my-override-keymap-alist 0)
(defvar my-override-keymap (make-sparse-keymap))
(add-hook 'my-override-keymap-alist (cons t my-override-keymap))

;;;; Leader

(defvar my-leader2-keymap (make-sparse-keymap))
(define-prefix-command #'my-leader2 'my-leader2-keymap)

;; Use "M-r" as a prefix, as the default command isn't very useful; convention:
;; "M-r <single-char> <single-char>+" and "M-r M-<char>" for balance-ness
;; (when (not (keymapp (keymap-global-lookup "M-r"))) (my-bind "M-r" nil))
(my-bind "M-R" `(menu-item "" nil :filter ,(lambda (_) (or (keymap-local-lookup "M-r") (keymap-global-lookup "M-r"))))) ; originally `move-to-window-line-top-bottom'
(my-bind :map 'my-override-keymap ";" `(menu-item "" nil :filter my-leader2-key-menu-item-kmi))
(my-bind :map 'my-leader2-keymap ";" #'self-insert-command)
(my-bind :map 'my-leader2-keymap "SPC" (my-fn% "Insert \"; \"." (insert "; ")))
(my-bind :map 'my-leader2-keymap "RET" (my-fn% "Insert \";\n\"." (insert ";") (newline)))
(defun my-leader2-bind (&rest args)
  ;; (-let* ((k (car args)))
  ;;   (when (and (stringp k)
  ;;              (string-prefix-p "a " k))
  ;;     (my-backtrace)))
  (apply #'my-bind :map 'my-leader2-keymap args))
;; Use "M-l" as local leader prefix
(my-bind "M-l M-l" #'downcase-dwim)

;;;; CUA

(setq cua-remap-control-v nil cua-remap-control-z nil) ; Make overriding easier
(cua-mode)                                             ; also `delete-selection-mode'
(setcdr cua-global-keymap nil)
;; In `global-map' to allow special bindings in major modes
(my-bind "C-v" #'my-clipboard-yank-maybe-paste-media)
(my-bind "M-Y" (my-make-command-with-mode-off 'delete-selection-mode #'yank-pop))
(my-bind :map 'isearch-mode-map "C-v" #'isearch-yank-kill)

(defvar my-interactive-search-command (my-1st-fn #'swiper-isearch #'isearch-forward-regexp))

;; some modes override those keys
(my-bind "C-M-S-q" #'save-buffers-kill-emacs) ; quit daemon
(my-bind "C-b" #'my-sidebar-toggle)
(my-bind "C-f" my-interactive-search-command) ; "C-f" means next in swiper
(my-bind '("C-s" "M-s M-s") `(menu-item "" nil :filter ,(lambda (_) (or (key-binding (kbd "C-x C-s")) #'save-buffer)))) ; easier to press
(my-bind :map '(my-override-keymap) "C-o" #'my-open|find-file-goto:line:column)

(my-bind "C-S-f" #'my-set-mark-and-forward-char "C-S-b" #'my-set-mark-and-backward-char)
(my-bind :map '(mc/keymap) "C-s" nil "C-f" #'phi-search)
(my-bind :map '(isearch-mode-map) "C-f" #'isearch-repeat-forward)
(my-bind :map '(swiper-map) "C-f" #'swiper-C-s)
(my-bind :map 'minibuffer-mode-map [remap swiper-isearch] #'isearch-forward-regexp) ; `swiper' can't search in minibuffers

;;;; Multi-session persistency

;; noticeably faster
(when (ignore-errors (sqlite-available-p)) (setq multisession-storage 'sqlite))

;;;; Tree sitter

(setq-default treesit-font-lock-level 4)
(my-persistuse 'my-major-mode-remap-alist (lambda () (my-treesit-make-major-mode-remap-alist))
  (lambda (lst) (setq major-mode-remap-alist (delete-dups (append my-major-mode-remap-alist lst))))
  :elem-pred 'consp)
(my-bind :map 'my-override-keymap "C-M-a" (my-dispatch-cmds '((my-treesit-parser-list) #'my-treesit-go-beg-of-defun)))
(my-bind :map 'my-override-keymap "C-M-e" (my-dispatch-cmds '((my-treesit-parser-list) #'my-treesit-go-end-of-defun)))
(my-bind :map 'my-override-keymap [remap mark-defun] (my-dispatch-cmds '((my-treesit-parser-list) #'my-mark-treesit-defun-maybe))) ; TODO: remove when no longer needed

(advice-add #'treesit-major-mode-setup :after #'my-treesit-major-mode-setup-run-hook--after-a)
(add-hook 'my-treesit-major-mode-setup-hook #'my-treesit-opt-out-h)

(my-setup-elisp-tree-sitter)

;;; Interface Enhancement

(when (fboundp #'display-line-numbers-mode)
  (dolist (hook my-edit-mode-hooks)
    (add-hook hook #'display-line-numbers-mode)))

(when (fboundp #'helpful-at-point) (my-setup-helpful))
(my-bind :map 'help-map '("g" "C-o") nil) ; `describe-gnu-project': when accidentally pressed will open the browser, `describe-distribution': not very useful

(mapc #'my-safe-call #'(context-menu-mode)) ; the first command maybe one of those
(with-eval-after-load 'mouse (my-add-list! 'context-menu-functions '(my-context-menu) t))

(when my-initial-file-in-CLI-args
  (setq inhibit-splash-screen t))
(setq inhibit-startup-echo-area-message user-login-name)

(my-setup-sideline)

;;;; Window & Frame Management

(setq server-client-instructions nil)   ; Silence "When done with this frame, type C-x 5 0"

;;;;; Font

;; For zooming, sacrifice "C-0", "C--" as prefix arguments, "M-" keys are much better anyway
(my-bind "C--" #'text-scale-decrease)
(my-bind "C-0" #'text-scale-adjust)
(my-bind "C-=" #'text-scale-increase)
(my-bind "C-M--" #'global-text-scale-adjust)
(my-bind "C-M-0" #'global-text-scale-adjust)
(my-bind "C-M-=" #'global-text-scale-adjust)

;;;;; Layout

(my-windowpopup-set '(derived-mode shell-command-mode shell-mode))

(winner-mode)

;;;;; Switching
(my-setup-other-window)

;;;; Tabbar
(my-bind :vi 'normal :map '(global-map magit-status-mode-map) "g t" #'my-tab-bar-switch-to-next-tab-or-new-tab)
(setq tab-bar-tab-name-function #'my-tab-bar-tab-name)
(with-eval-after-load 'tab-bar (tab-bar-history-mode))
(my-bind "C-S-w" #'tab-bar-close-tab)

;;;; Navigation

(my-bind "C-S-o" #'open-line)

;; Keep the vanilla "C-x b" buffers-only
(my-bind '("C-TAB" "C-<tab>") (my-1st-fn #'consult-buffer #'switch-to-buffer) [remap switch-to-buffer] nil)

(my-bind "C-M-s-b" #'bookmark-jump "C-M-s-d" #'bookmark-set)

(my-bind "C-M-s-SPC" #'my-mark-jump-back-in-current-buffer)

(add-hook 'occur-hook #'occur-rename-buffer) ; multiple occur buffers 

(setq view-read-only t)
(my-setup-smooth-scroll)
(my-scroll-mode)

(my-safe (key-chord-define-global "jk" #'my-avy-goto-char-timer-for-action))
(my-bind "C-'" #'my-avy-goto-char-timer-for-action)

(when (fboundp 'mwim) (my-bind "C-a" 'mwim-beginning "C-e" 'mwim-end))

(setq isearch-allow-motion t)
(setq isearch-allow-scroll 'unlimited)
(setq isearch-lazy-count t)
(setq isearch-repeat-on-direction-change t)
(setq search-exit-option nil)           ; 'edit ?

(my-bind :map 'Buffer-menu-mode-map "S-<delete>" #'my-Buffer-menu-kill-buffer-at-point-now)

;;;; Key-bindings

(my-bind "M-<f10>" #'menu-bar-open)
;; support for `my-buffer-filename-function'
(my-bind "C-M-s-n" #'my-new-buffer)
(my-bind :map '(splash-screen-keymap +doom-dashboard-mode-map)
  '("i" [remap evil-insert]) #'my-new-buffer-maybe
  '("e" [remap evil-forward-word-end]) #'my-edit-clipboard)
(my-bind :map 'y-or-n-p-map (kbd "SPC") nil) ; Dangerous

(defvar evil-state nil)
(my-setup-evil)                         ; Tip: access `keyboard-escape-quit' with "M-ESC ESC", to close windows

(my-setup-devil)

;; Use undo as normal for transient
(my-bind :map 'transient-base-map "C-z" nil "C-M-z" #'transient-suspend)
(with-eval-after-load 'transient (transient-remove-suffix 'transient-common-commands "C-z"))
(with-eval-after-load 'transient (transient-bind-q-to-quit)) ; some transients don't have an easy to press quit key

(my-bind :map 'my-leader-spc-map ";" (cond ((fboundp #'inspector-inspect) #'my-inspector-expression) (t #'pp-eval-expression)))
(my-bind :map 'my-leader-spc-map "RET" #'bookmark-jump)
(my-bind :map 'my-leader-spc-map "SPC" #'my-findutil-in-project-or-prompt-project) ; also directories, compared to `project-find-file'
(my-bind :map 'my-leader-spc-map "b r" #'revert-buffer-quick)
(my-bind :map 'my-leader-spc-map "f p" (lambda () (interactive) (my-findutil-find-file-recursively my-emacs-conf-dir/)))
(my-bind :map 'my-leader-spc-map "f r" #'recentf-open-files)
(my-bind :map 'my-leader-spc-map "h '" #'describe-char)
(my-bind :map 'my-leader-spc-map "h F" #'describe-face)
(my-bind :map 'my-leader-spc-map "h P" #'find-library)
(my-bind :map 'my-leader-spc-map "h t" #'consult-theme) ; `load-theme' needs disabling old ones manually
(my-bind :map 'my-leader-spc-map "q r" #'restart-emacs)

(my-leader2-bind "%" #'my-query-replace-regexp-in-whole-buffer)
(my-leader2-bind "." #'embark-act)
(my-leader2-bind "/" #'my-rgrep-recursive-grep-project-or-cwd)
(my-leader2-bind "M-'" #'my-edit-region-or-string-at-point-dwim)
(my-leader2-bind "M-a" #'mark-whole-buffer)
(my-leader2-bind "M-d" #'my-docs-documentation-at-point)
(my-leader2-bind "M-h" #'my-highlight-just-dwim)
(my-leader2-bind "M-i" #'my-project-imenu)
(my-leader2-bind "M-o" #'my-goto-outline-heading)
(my-leader2-bind "M-t" #'my-exchange-transpose-swap-regions)
(my-leader2-bind "M-w" #'my-search-online-by-web-browser)
(my-leader2-bind "a a" #'aidermacs-transient-menu)
(my-leader2-bind "a e" #'ellama-transient-main-menu)
(my-leader2-bind "a g" #'gptel-menu)
(my-leader2-bind "b =" #'my-switch-to-buffer-with-same-major-mode)
(my-leader2-bind "b b" #'consult-bookmark)
(my-leader2-bind "b k" #'my-kill-other-undisplayed-buffers)
(my-leader2-bind "b r" #'my-rename-buffer)
(my-leader2-bind "c SPC" #'cape-prefix-map)
(my-leader2-bind "c a" #'my-copy-to-clipboard-whole-buffer)
(my-leader2-bind "c c" #'my-starhugger-global-auto-mode)
(my-leader2-bind "c p" #'flymake-show-project-diagnostics)
(my-leader2-bind "c v" #'duplicate-dwim) ; "C-c C-v"?
(my-leader2-bind "c x" #'flymake-show-buffer-diagnostics)
(my-leader2-bind "d ." #'my-spell-correct) ; `ispell': "M-$" `flyspell': "C-."
(my-leader2-bind "d d" #'powerthesaurus-transient)
(my-leader2-bind "e R" #'crux-eval-and-replace)
(my-leader2-bind "e n" #'my-elisp-eval-next-sexp)
(my-leader2-bind "e u" #'my-elisp-eval-upper-sexp)
(my-leader2-bind "f c" #'my-copy-filename)
(my-leader2-bind "f d" #'consult-dir)
(my-leader2-bind "f l" #'my-findutil-async-from-home) ; "locate"
(my-leader2-bind "f r" #'recentf-open)
(my-leader2-bind "f u" #'crux-sudo-edit)
(my-leader2-bind "h d d" #'toggle-debug-on-error)
(my-leader2-bind "h d q" #'toggle-debug-on-quit)
(my-leader2-bind "i [" #'my-pair-auto-wrap-square-bracket)
(my-leader2-bind "i ]" #'my-pair-auto-wrap-round-bracket|parenthesis)
(my-leader2-bind "i r SPC" #'my-insert-space-regexp)
(my-leader2-bind "i s" #'tempel-insert)
(my-leader2-bind "i u" #'insert-char)
(my-leader2-bind "i w s" (my-fn% (my-wrap-or-insert "\\_<" "\\_>"))) ; symbol
(my-leader2-bind "i w w" (my-fn% (my-wrap-or-insert "\\<" "\\>")))  ; word
(my-leader2-bind "i {" #'my-pair-auto-wrap-curly-bracket)
(my-leader2-bind "n c" #'org-roam-capture)
(my-leader2-bind "n d" #'deft)
(my-leader2-bind "n f" #'org-roam-node-find)
(my-leader2-bind "n g" #'org-roam-graph)
(my-leader2-bind "n j" #'org-roam-dailies-capture-today)
(my-leader2-bind "o f" #'make-frame-command)
(my-leader2-bind "o r" #'my-evalu-switch-to-repl)
(my-leader2-bind "o w" #'my-xdg-open-file-with-default-application)
(my-leader2-bind "p f" #'my-findutil-fallback-to-async)
(my-leader2-bind "p i" #'my-insert-and-copy-filename-in-project-relative-to-current)
(my-leader2-bind "p p" #'my-project-switch-project-and-find-file)
(my-leader2-bind "p t" #'my-terminal-emulator-at-project-root)
(my-leader2-bind "p x" #'my-scratchs)
(my-leader2-bind "r r" ctl-x-r-map) ; easier to access than "C-x r"
(my-leader2-bind "s <f4>" #'my-save-buffer-and-close) ; "C-s M-<f4>"
(my-leader2-bind "s d" #'my-rgrep-current-directory)
(my-leader2-bind "s p" #'my-rgrep-simple)
(my-leader2-bind "s s" #'swiper)
(my-leader2-bind "t c" #'colorful-mode)
(my-leader2-bind "t f" #'my-online-check-non-intrusive-mode)
(my-leader2-bind "t w" #'visual-line-mode)
(my-leader2-bind "v 4 f" #'vc-revision-other-window)
(my-leader2-bind "v g" #'magit-status)
(my-leader2-bind "v m" #'my-vc-smerge-transient)
(my-leader2-bind "v t" #'git-timemachine-toggle)
(my-leader2-bind "w 0" #'delete-window)
(my-leader2-bind "w 1" #'my-force-delete-other-windows)
(my-leader2-bind "w 2" #'my-force-split-window-below)
(my-leader2-bind "w 3" #'my-force-split-window-right)
(my-leader2-bind "w 4" #'my-cycle-window-direction)
(my-leader2-bind "w ]" #'my-try-to-enlarge|widen|fit-window-width-maybe)
(my-leader2-bind "w s" #'window-toggle-side-windows) ; mainly to close bottom popups
(my-leader2-bind "w t" #'window-swap-states) ; like the transpose keys - tranpose windows
(my-leader2-bind "z d" #'narrow-to-defun)
(my-leader2-bind "z i" #'my-fold-hide-lines-with-indent-greater-than)
(my-leader2-bind "z n" #'narrow-to-region)
(my-leader2-bind "z p" #'narrow-to-page)
(my-leader2-bind "z w" #'widen)

;; Like VSCode's "C-k" prefix
(my-bind "M-k M-k" #'kill-sentence)

(my-bind "M-k M-0" #'treesit-fold-close-all)
(my-bind "M-k M-f" #'my-code-format-region|selection-or-line)
(my-bind "M-k M-j" #'treesit-fold-open-all)
(my-bind "M-k M-l" #'treesit-fold-toggle)

(my-bind "M-i" (keymap-lookup my-leader2-keymap "i"))
(my-bind "M-i M-i" #'tab-to-tab-stop)

;;;; Minibuffer

(my-bind "M-R" #'vertico-repeat)
(my-bind :map 'minibuffer-mode-map "M-S" (my-dispatch-cmds '((equal 'file (my-minibuffer-completion-category)) #'my-tramp-read-filename-toggle-remote)))
(my-bind :map 'minibuffer-mode-map '("<prior>" "<next>") nil) ; Prefer scrolling candidates instead of history

(mapc #'my-safe-call '(minibuffer-depth-indicate-mode savehist-mode undelete-frame-mode))

(my-setup-completion-style)
(my-setup-embark)
(my-setup-counsel)
(my-setup-minibuffer)

(add-hook 'minibuffer-setup-hook #'my-minibuffer-completion-style-disable-flex-for-file-h)

(setq use-dialog-box nil)               ; use minibuffer

;;;; Mode-line

(my-add-hook/s '(minions-prominent-modes) '(glasses-mode smart-dash-mode))
(setq doom-modeline-buffer-file-name-style 'truncate-with-project) ; when too much, may hide other information in the modeline
(setq doom-modeline-major-mode-icon t doom-modeline-modal-icon nil)
(setq doom-modeline-minor-modes (fboundp 'minions-mode))
(setq nyan-wavy-trail t nyan-minimum-window-width 96 nyan-bar-length 12)

(when (not (fboundp #'doom-modeline-mode)) (setq-default mode-line-format my-mode-line-format-default))
(defun my-setup-modeline-modes ()
  (dolist (fn '(doom-modeline-mode minions-mode nyan-mode)) (when (boundp fn) (make-thread fn))))


;; in daemon mode, defer this to make it responsible quicker
(cond ((daemonp)
       (my-add-hook-once 'server-after-make-frame-hook #'my-setup-modeline-modes)
       (add-hook 'my-idle-run-once-hook #'my-setup-modeline-modes))
      (:else (my-setup-modeline-modes)))

;;; Editing Enhancements

(global-subword-mode)

(my-bind "C-/" #'my-comment-line [remap comment-line] nil) ; inspired by other editors, don't let evil remap it
(my-bind "C-<backspace>" #'backward-kill-sexp)
(my-bind "C-M-S-h" #'my-mark-bigger-defuns-or-paragraphs)
(my-bind "C-M-s-l" #'my-mark-or-copy-line)
(my-bind "C-S-r" #'my-refresh-find|open-current-file)
(my-bind "C-c ;" #'my-transpose-line-and-swap-comment-status)
(my-bind "C-c M-d" #'crux-duplicate-and-comment-current-line-or-region)
(my-bind "C-c n" #'crux-cleanup-buffer-or-region)
(my-bind "C-x S" #'my-save-unsaved-buffers-with-files)
(my-bind "C-|" #'my-jump-bracket)
(my-bind "M-<insert>" #'my-insert-multiple-times)
(my-bind "M-H" #'my-mark-visual-text-paragraph-by-simple-search)
(my-bind "M-J" #'my-join-next-line)    ; like vi's ESC S-j
(my-bind "M-Q" #'my-join-region-or-paragraph)
(my-bind "M-S-SPC" #'cycle-spacing)
(my-bind "M-Z" #'zap-up-to-char)
(my-bind "S-<backspace>" #'my-kill-line-backward-balanced)

(my-bind '("M-c M-c" "M-c c") #'capitalize-dwim)
(my-bind '("M-c M-u" "M-c u") #'upcase-dwim)
(my-bind '("M-c M-l" "M-c l") #'downcase-dwim)

(my-bind '("C-M-S-s-<return>") #'my-open-then-new-indented-line)
(my-bind '("C-M-s-<return>") #'my-enter|return-dwim)
(my-bind '("S-RET" "S-<return>") #'my-open-new-indented-line)

(my-bind :map '(global-map org-mode-map) '("C-RET" "C-<return>") #'my-open-line-below)
(my-bind '("C-S-RET" "C-S-<return>") #'crux-smart-open-line-above)

(my-bind "M-T c" #'transpose-chars)
(my-bind "M-T e" #'transpose-sexps)
(my-bind "M-T l" #'transpose-lines)
(my-bind "M-T p" #'transpose-paragraphs)
(my-bind "M-T r" #'transpose-regions)
(my-bind "M-T s" #'transpose-sentences)

(my-bind "C-M-S-<down>" #'my-duplicate-line-or-region-down)
(my-bind "C-M-S-<up>" #'my-duplicate-line-or-region-up)

;; TODO: feature request https://yhetil.org/emacs/497AEED3-677D-453A-A567-F3CFF7D13688@getmailspring.com/
(my-add-advice-once '(comment-line comment-dwim) :before (lambda (&rest _) (advice-add #'comment-normalize-vars :around #'my-comment-normalize-vars--default-a)))

(my-bind "M-%" #'my-query-replace-literal-in-whole-buffer)
(advice-add #'perform-replace :around #'my-high-scroll-margin-a)

(my-bind "M-u" (my-dispatch-cmds '((and (my-evil-motion-derived-state-p) (outline-on-heading-p)) #'outline-up-heading)
                                 '((my-treesit-parser-list) #'my-treesit-backward-up-node)
                                 '((bound-and-true-p tree-sitter-mode) #'my-elisp-tree-sitter-backward-up-node)
                                 '(:else #'backward-up-list)))

(my-bind "M-/" #'my-expand-or-select-expansions "C-M-/" #'hippie-expand)
(my-bind "C-SPC" #'my-C-SPC-dwim) ; use "C-@"

(my-bind "C-c D" #'my-delete-current-file)
(my-bind "C-c r" #'my-rename|move-file&buffer)

;; (setq display-line-numbers-type 'relative)
(setq text-scale-mode-step 1.125)       ; default: 1.2 is too much
(setq-default cursor-type 'bar)

;; move cursor to the latest created line/region
(setq duplicate-line-final-position -1) 
(setq duplicate-region-final-position -1)

(when (fboundp #'breadcrumb-mode)
  (when my-initial-file-in-CLI-args (my-add-hook-once 'find-file-hook (lambda () (setq-local header-line-format '(""))))) ; minimize visual disruption
  (add-hook 'my-prog-after-try-lsp-hook #'my-breadcrumb-local-mode-maybe)) ; prevent duplications
;; (advice-add #'breadcrumb--header-line :around #'my-breadcrumb--header-line--a)

(when (fboundp #'editorconfig-mode) (my-add-hook-once 'find-file-hook #'editorconfig-mode))

(setq backup-directory-alist `(("." . ,(file-name-concat my-user-emacs-local-dir/ "backup/"))))

;; prevent weird displayed relative paths on the breadcrumb and mode
;; line due to project root isn't a prefix of current file path
(setq find-file-visit-truename t)
(add-hook 'dired-mode-hook #'my-find-file-visit-no-truename-h) ; but in dired, navigating like this is easier
(setq vc-follow-symlinks t)

(add-hook 'text-mode-hook #'visual-line-mode)
(setq-default fill-column 80) ; 70 is too small
(setq-default truncate-lines t) ; this type of wrapping affects performance

(my-hl-line-global-mode)
(global-auto-revert-mode)
(my-without-filename-handler (save-place-mode))

;;;; Indentation Enhancement

(setq-default indent-tabs-mode nil)     ; use space instead of tab
(setq-default tab-width 4)              ; `tab-to-tab-stop' less surprising

;; (when (fboundp #'dtrt-indent-global-mode) (my-add-hook-once 'find-file-hook #'dtrt-indent-global-mode))
(my-add-advice/s '(newline-and-indent default-indent-new-line +default/newline-above +default/newline-below my-enter|return-dwim)
  :before #'my-re-enable-dtrt-mode-before-a)

(my-setup-indent-bars)

(my-bind '("S-TAB" "<backtab>") #'my-indent-shift-left|outdent)
(my-bind :map 'my-override-keymap "<C-[>" `(menu-item "" my-indent-shift-left|outdent :filter my-kmi-filter-indent-shift))
(my-bind :map 'my-override-keymap "C-]" `(menu-item "" my-indent-shift-right :filter my-kmi-filter-indent-shift))

;;;; Symbols/Tokens

(when (fboundp #'hl-todo-mode) (my-add-startup-hook (my-add-hook-once 'find-file-hook #'global-hl-todo-mode)))
(with-eval-after-load 'hl-todo (my-add-list! 'hl-todo-include-modes '(conf-mode) t))

(my-setup-colorful-mode)

(setq glasses-separator "﹍")           ; Distinguish with the real "_"
(setq glasses-original-separator "") ; Don't convert anything else

;;;; Whitespaces Enhancement

(with-eval-after-load 'ws-butler (setq ws-butler-keep-whitespace-before-point t))

;; Single space after period is now dominant. Set `sentence-end-double-space' to
;; nil so that most sentence commands work correctly; but let it be t for
;; `fill-paragraph' to preserve existing double spaces.
(setq-default sentence-end-double-space nil)
(advice-add #'fill-paragraph :around #'my-with-sentence-end-double-space-a)
(my-auto-sentence-end-double-space-after-period-mode)

;;;; Massive Edit

(my-bind "<C-m>" (my-dispatch-cmds '((string-match-p "^[ \t]+$" (my-relative-buffer-substring 1 1)) #'my-mc-mark-next-with-same-indent)
                                   '((my-treesit-parser-list) #'my-iedit-mode-toggle-on-current-treesit-function)
                                   '(:else #'iedit-mode-toggle-on-function)))
(my-setup-iedit)
(my-setup-multiple-cursors)

;;;; Quotes & Parenthesis & Delimiters Handling

;;;;; Insert & Edit

(my-add-first-editable-hook (electric-pair-mode))
(advice-add 'electric-pair-post-self-insert-function :around #'my-electric-pair-post-self-insert-function--inhibit-a)
(my-pair-navigate-mode)
(my-add-hook-once 'window-configuration-change-hook 'my-global-pair-edit-mode)
(my-setup-closing-square-bracket-to-opening-round-parenthesis)
(my-setup-sexp-keys)
(setq lispy-key-theme '(special parinfer))
(my-setup-parinfer-rust)
(my-setup-lispy)
(my-pair-add-multichar-pair 'global "\\\"" "\\\"")
(my-pair-add-multichar-pair 'global "\\\\\"" "\\\\\"")

;; for multiple-cursors
(setq puni-confirm-when-delete-unbalanced-active-region nil)

;;;;; Select

(my-bind :map 'cua-global-keymap "M-v" #'my-expand-region)

;;;;; Highlight

(when (fboundp #'rainbow-delimiters-mode) (add-hook 'prog-mode-hook #'my-rainbow-delimiters-mode-maybe))
(setq rainbow-delimiters-max-face-count 7) ; Don't set to 6 as for Doom themes, there are only 5 recurring colors
(add-hook 'smerge-mode-hook #'my-rainbow-delimiters-turn-off-maybe) ; while merge conflicts happen, parentheses are messy anyway

(my-bind :map 'org-mode-map "C-M-s-f" #'my-org-emphasize-prefix)
(my-bind :map 'TeX-mode-map "C-M-s-f" #'TeX-font)

(show-paren-mode)
(setq show-paren-when-point-in-periphery nil)

;;;; Kill-ring / Clipboard

(setq save-interprogram-paste-before-kill t) ; Otherwise the top of system clipboard may easily be overridden by Emacs
(my-bind "C-M-S-c" #'my-clipboard-copy-region-or-empty-string)
(my-add-advice/s '(backward-kill-word puni-backward-kill-word) :around #'my-delete-instead-of-kill-when-interactive-a)

;;;; Drag Lines

(-let* ((bindings (list "M-<left>" #'drag-stuff-left "M-<right>" #'drag-stuff-right
                        "M-<up>" #'my-move-lines-up "M-<down>" #'my-move-lines-down)))
  (apply #'my-bind bindings)
  ;; Only use org-meta-* in non-insert states
  (apply #'my-bind :map 'org-mode-map :vi 'insert bindings))

(my-safe-call #'drag-stuff-global-mode)

;;;; Undo & Redo

(defvar my-undo-command (my-1st-fn 'undo-fu-only-undo 'undo-only 'undo))
(defvar my-redo-command (my-1st-fn 'undo-fu-only-redo 'undo-redo))
(my-bind
  '("C-z" "C-M-s-z") my-undo-command
  '("C-S-z" "C-M-s-y") my-redo-command
  "C-x u" (my-1st-fn #'vundo #'undo-tree-visualize))
;; (setq undo-in-region t)                 ; maybe buggy
(when (fboundp #'undo-fu-session-global-mode) (undo-fu-session-global-mode))

;;;; Multiple Major-Mode

;;;; Snippet

(my-bind "C-c s" #'my-yas-insert-snippet)

(defun my-tab-key-menu-item-filter (&optional _ call)
  (interactive (list nil t))
  (-let* ((cmd
           (condition-case err
               (cond
                ;; ((get-buffer-process (current-buffer)) nil) ; REPL's tab key
                (completion-in-region-mode
                 my-popup-complete-command)
                ((ignore-errors
                   (starhugger-at-suggestion-beg-p t))
                 nil)
                ((minibufferp)
                 nil)
                (buffer-read-only
                 nil)
                ((member evil-state '(normal visual motion))
                 ;; `my-insert-&-ESC-TAB'
                 nil)
                ((my-tempel-expandable-p #'my-bounds-of-prev-sexp-to-point)
                 #'my-tempel-expand-prev-sexp-to-point)
                ((my-yasnippet-expandable-p)
                 #'yas-expand-from-trigger-key))
             (error (message "`my-tab-key-menu-item-filter' %S" err) nil))))
    (when call
      (call-interactively cmd))
    cmd))
(-let* ((defnt `(menu-item "" nil :filter my-tab-key-menu-item-filter)))
  (my-bind :map 'my-override-keymap "TAB" defnt "<tab>" defnt))
;; provide a fallback for accessing the TAB key
(my-bind '("C-M-S-<iso-lefttab>") `(menu-item "" nil :filter (lambda (_) (keymap-local-lookup "TAB"))))

(my-bind "M-*" #'tempel-insert)

(my-bind "M-\\" #'my-starhugger-dwim)

(my-add-hook/s 'my-idle-load-list '(tempel yasnippet))

;;;; Text conversion

(when (boundp 'coercion-command-map) (my-bind "M-c SPC" coercion-command-map))

(my-bind :vi '(normal) "]r" #'grugru-forward "[r" #'grugru-backward) ; like Doom's `rotate-text'

;;; Programming

(my-bind '("C-S-i" "<C-S-i>") #'my-code-format-dwim)
(my-bind "C-c f" #'my-code-format-try-region-in-cloned-buffer) ; "C-S-c f" actually
(when (fboundp #'apheleia-global-mode) (my-setup-apheleia))

;;;; Completion

(my-bind '([remap complete-symbol] [remap completion-at-point]) `(menu-item "" my-complete-in-minibuffer :filter my-complete-menu-item-filter-in-minibuffer))
(setq completion-ignore-case t read-file-name-completion-ignore-case t read-buffer-completion-ignore-case t)
(my-add-hook/s `(,@my-edit-mode-hooks comint-mode-hook minibuffer-mode-hook) #'(abbrev-mode))

(my-setup-corfu)
(my-setup-cape)
(my-company-settings)
(with-eval-after-load 'minibuffer
  (my-add-hook/s
    '(completion-at-point-functions)
    (--> `(,@(and (fboundp #'cape-capf-super) (list (my-cape-lazy-make-capf #'my-cape-super-misc-capf
                                                      ;; #'cape-file
                                                      '(cape-capf-super #'cape-dabbrev #'cape-dict #'cape-keyword #'my-tempel-complete-capf))))
           cape-file ,@(and (fboundp #'tempel-complete) (list #'my-tempel-complete-capf)) cape-dict)
         (-filter #'functionp it))))
(setq abbrev-file-name (file-name-concat my-emacs-conf-dir/ "etc" "abbrev_defs.el"))

(my-setup-hook-run-before-complete-at-point)

(defconst my-complete-super-capf-merging '(:with tempel-complete)) ; :with : prefer other CAPFs when the primary doesn't complete
(add-hook 'my-before-complete-at-point-hook #'my-completion-wrap-first-capf->-super-capf-h)

;;;; Jump to Definition / Tagging

(setq xref-prompt-for-identifier nil)

;; don't want to prioritize this, but it must be before etags
(when (fboundp #'dumb-jump-xref-activate) (add-hook 'xref-backend-functions #'dumb-jump-xref-activate 75))
(setq dumb-jump-disable-obsolete-warnings t) ; allow using `dumb-jump-go' independently

(my-bind "M-." #'my-xref-goto-definition)

;;;; LSP Client

(my-setup-lsp)(defvar lsp-mode nil)
(my-bind "<f2> <f2>" (my-dispatch-cmds '(lsp-mode #'lsp-rename) '(:else #'eglot-rename)))
(my-bind "C-." #'my-lsp-execute-code-action)

;;;; Debugging
(defvar my-leader-help-key-regexp (rx-to-string `(seq bol (or ,my-leader-key ,my-leader-alt-key) " h ") t))

(my-bind "C-M-`" #'my-project-show-compilation-buffer)
(add-hook 'compilation-start-hook 'my-compilation-comint-mode)
(with-eval-after-load 'compile (setq compilation-always-kill nil)) ; Doom sets this to t, don't kill running processes
(my-bind :map 'compilation-button-map '("M-RET" "M-<return>") #'my-with-saved-selected-window-press-enter|return)

(my-bind :map '(prog-mode-map) '("M-RET" "M-<return>") #'my-evalu-dwim) ; text-mode-map conf-mode-map

(setq my-execu-confirm-key "<f5>")
(my-setup-dap-debug my-execu-confirm-key)

;;;; Document

(my-bind :map '(global-map lsp-mode-map) "C-S-SPC" #'my-trigger-parameter-hints) ; sometimes `lsp-signature-activate' doesn't show anything useful
(setq eldoc-echo-area-prefer-doc-buffer 'maybe)
(setq-default eldoc-documentation-strategy #'eldoc-documentation-enthusiast)

;;;; Code Folding

;; C-x >< CUA

(my-setup-outline)
(my-setup-treesit-fold)

(my-advice-function-to-run-hook #'set-selective-display 'my-set-selective-display-after-hook :after)

;;;; Error Checking
(my-setup-flymake)
(my-bind :map 'flymake-mode-map "<f8>" #'flymake-goto-next-error "S-<f8>" #'flymake-goto-prev-error)
(my-bind :map '(flymake-diagnostics-buffer-mode-map flymake-project-diagnostics-mode-map)
  "p" #'my-flymake-diagnostics-buffer-peek-prev "n" #'my-flymake-diagnostics-buffer-peek-next)

(add-hook 'next-error-hook #'my-recenter-top-to-fraction)

;;; Project management

(with-eval-after-load 'project
  (add-to-list 'project-vc-extra-root-markers ".project")
  (setq project-compilation-buffer-name-function #'project-prefixed-buffer-name))
;; (advice-add #'project-current :around #'my-project-current-cache-a) ; `project-try-vc' uses `vc-file-getprop' to cache

;;; File Manager

(setq delete-by-moving-to-trash t)
(setq large-file-warning-threshold nil)
(my-setup-dired)

(my-setup-recentf)

(my-setup-treemacs)

;;; Programming Language

(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode)) ; don't fall back to `objc-mode'
(add-to-list 'magic-fallback-mode-alist '("\\(\\(\n\\)*#+[ \t]\\|\\[.*\\]\\)" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.opt\\'" . conf-mode-maybe) t)

(my-setup-lang-emacs-elisp)

(my-bind :map '(lisp-mode-shared-map) "M-l m" #'macrostep-expand)

(defvar my-bash-cli-editor-file-regexp "bash-fc\\.[[:alnum:]]\\{6\\}")
(add-to-list 'auto-mode-alist `(,(format "/%s\\'" my-bash-cli-editor-file-regexp) . sh-mode) t) ; edit bash commands from CLI

(my-add-list! 'lisp-imenu-generic-expression
  ;; top-level aliases (note the (sharp)? quote)
  `(("aliases" ,(concat "^(" (regexp-opt '("defalias" "defvaralias")) "\\s-+#?'\\(" lisp-mode-symbol-regexp "\\)") 1)))

(add-hook 'after-save-hook #'my-set-auto-mode-from-fundamental-maybe-h)

(my-bind :map (-map #'my-mode->map-symbol my-lisp-major-modes) [remap evil-indent]
  '(menu-item "" my-elisp-code-format-dwim :filter my-apheleia-menu-item-filter-not-external-formatters))

;;; AI

(my-setup-llm-interaction)

;;; Keys Cheat Sheet

(setq which-key-idle-delay 0.5)
(when (and (fboundp #'which-key-mode)) (which-key-mode))

;;; Note

(my-setup-org)
(my-setup-org-roam)

;; Monday
(setq! calendar-week-start-day 1)

(setq-default org-cite-global-bibliography '("~/Documents/knowledge/bib/references.bib"))

(my-setup-citar)

;;; Version control

(with-eval-after-load 'savehist (add-to-list 'savehist-additional-variables 'log-edit-comment-ring)) ; save unfinished commit messages

(my-setup-vc-git)
(my-setup-magit)

(my-setup-sideline-blame)

(when (fboundp #'global-diff-hl-mode) (my-add-hook-once '(find-file-hook dired-initial-position-hook) (lambda () (make-thread #'global-diff-hl-mode))))

;;; Integration

(setq password-cache-expiry 3600)

;;;; Console

(advice-add #'shell-completion-vars :after #'my-read-shell-command-run-hook-after-a)

(with-eval-after-load 'shell
  (my-safe-add-hook/s '(shell-dynamic-complete-functions) #'(bash-completion-dynamic-complete)))

(add-hook 'my-omni-completion-context-capf-table '((sh-mode sh-base-mode) . my-shell-fish-complete-command))
(add-hook 'my-extra-capfs-table '((my-read-shell-command-minibuffer-mode) my-shell-fish-completion-at-point))

(setq async-shell-command-buffer 'new-buffer)
(my-add-advice/s '(async-shell-command projectile-run-async-shell-command-in-root) :around #'my-with-interactive-sh-maybe-a)

(setq shell-command-prompt-show-cwd t)
(my-bind "S-<f4>" #'my-terminal-emulator-dwim) ; Inspired by KDE Dolphin
(my-bind "<f7>" #'my-async-shell-command-maybe-region)   ; Quicker alternative to S-M-7
(my-bind :map 'minibuffer-local-shell-command-map "M-s M-s" #'my-read-shell-command-toggle-sudo)

(defvar my-shell/terminal-search-keys '("C-S-f" "<normal-state> C-S-f"
                                        "C-s" "<normal-state> C-s" "<insert-state> C-s"))
(my-bind :map '(term eshell-mode-map) my-shell/terminal-search-keys my-interactive-search-command)
(my-bind :map 'eshell-mode-map "C-S-t" (my-new-shell||terminal-command 'eshell))
(my-bind :map 'term-raw-map "C-S-t" (my-new-shell||terminal-command 'ansi-term 'shell-file-name))
(my-bind :map 'eat-mode-map "C-S-t" (my-new-shell||terminal-command 'eat nil t))

(my-add-hook/s '(my-evalu-repl-hook comint-mode-hook) #'(my-repl-input-history-init-comint-h))
(advice-add #'comint-write-input-ring :around #'my-comint-ring-fix-separator-a)
(setq comint-pager "cat") ; integrated REPLs are not fully functional terminal emulators

;;;; Operating System
(setq Man-notify-method 'pushy
      Man-width-max 128
      woman-fill-frame t)
(add-hook 'Man-mode-hook 'my-center-man-mode)

(my-setup-envrc)

(add-hook 'my-idle-run-repeat-hook #'my-env-generate-env-file)

;;;; Search

;; See `+vertico/embark-export-write'
(my-bind :map 'grep-mode-map "i" #'wgrep-change-to-wgrep-mode)
(my-bind :map 'my-dired-key-mode-map "i" #'wdired-change-to-wdired-mode)
(my-bind :map 'occur-mode-map "i" #'occur-edit-mode)

;;;; Tools?

(when (and (member (daemonp) '("server")) (fboundp #'edit-server-start))
  (add-hook 'my-idle-run-once-hook #'edit-server-start))

;;; Internet

(my-bind "M-s M-w" #'my-eww-new) ; from `eww-search-words'

(setq browse-url-browser-function #'my-xdg-browse-url*)

;;;; Mail

(add-to-list 'auto-mode-alist '("/\\*message\\*-[-0-9]+\\'" . message-mode))

(defalias #'my-compose-new-email-message #'compose-mail)

;;; Package Management

(setq epkg-repository (file-name-concat my-user-emacs-local-dir/ "epkgs"))

(add-hook 'my-idle-run-repeat-hook #'my-useelisp-refresh)

;;; Config Management

(setq initial-major-mode #'fundamental-mode)
(setq initial-scratch-message nil)
(when (equal my-emacs-kit 'doom) (my-doom-emacs-config))
(setq warning-minimum-level :error)     ; hide warnings that should be mailed to the package maintainers instead of disrupting users' workflow

(setq byte-compile-warnings '(not docstrings)) ; https://yhetil.org/emacs-devel/871qw0yi8j.fsf@posteo.net/

(my-bind :map 'profiler-report-mode-map "<backtab>" #'my-profiler-expand-report)

(add-hook 'my-idle-run-repeat-hook #'my-check-obsolete-or-deprecated)

;; go to build dir first to view artifacts, looks like not effective?
(advice-add #'find-library :around (my-make-with-dlet-variables-advice '((find-file-visit-truename nil))))

(when (not (member my-emacs-kit '(doom)))
  (add-hook 'hack-local-variables-hook #'my-run-local-var-hooks-h))

;; Hide some dangerous commands from M-x
(dolist (sym '(emacs-news-mode)) (put sym 'completion-predicate #'ignore))

;;; Library

(my-after-each '(emacs svg-lib) (setq-default svg-lib-icons-dir (file-name-concat my-local-share-emacs-dir/ "svg-lib/"))) ; share (too slow to download)

;;; Appearance

;; Don't save `custom-set-faces' to `custom.el', especially programmatically customized faces
(advice-add #'custom-save-faces :override #'ignore)

(my-custom-set-faces
  `(font-lock-doc-face :slant normal) ; don't italic that is harder too read for long ones
  `(font-lock-keyword-face :weight normal) ; sometimes they are not "true" keywords
  `(font-lock-variable-use-face :inherit unspecified) ; don't colorize too much that they become the same
  `(hi-yellow :distant-foreground "black") ; make inner text readable when too bright
  `(line-number-current-line :weight bold) ; standout for a bit
  `(link :weight semibold) ; `tree-sitter' uses `link' for function calls & `bold' is too distracting
  `(wgrep-face :background unspecified) ; just being bold and bright is enough, the background color often makes it harder to read
  `(fixed-pitch-serif :inherit fixed-pitch :family unspecified)) ; it's painful to read if left at the default

;; try to make comments readable in all themes, there are places they are used as the primary documentation
(face-spec-set
 'font-lock-comment-face
 `((((class color) (background dark)) :foreground "#B0B0B0" :slant normal)
   (((class color) (background light)) :foreground "#606060" :slant normal)))

;; When used as server, maximize new frames by default; because non-daemon Emacs
;; frame usually takes longer to appear, when we snapped the frame before Emacs
;; is fully initialized, maximizing later will discard the layout
(when (daemonp) (add-to-list 'default-frame-alist '(fullscreen . maximized)))
(column-number-mode)
(add-to-list 'default-frame-alist '(alpha-background . 96)) ; a little transparency

;;; Theme

(with-eval-after-load 'doom-themes-base
  (setq doom-themes-base-faces
        (cl-delete-if
         (-lambda ((face . _))
           (member face '(yas-field-highlight-face))) ; `region' > `match'

         doom-themes-base-faces)))

(defvar my-picked-theme-rotate-delta (cond ((equal (daemonp) t) 0)
                                           ((not (daemonp)) -1)
                                           (t -2)))
(defvar my-picked-theme-alist
  ;; Distinguish standalone instances with the daemon
  `((dark ,@(--> '(doom-dracula doom-xcode doom-palenight ef-elea-dark modus-vivendi-tinted modus-vivendi dracula wombat)
                 (-rotate my-picked-theme-rotate-delta it)))
    ;; TODO: fix """ Undefined color: "unspecified-bg" """ when theme is nil
    (light ,@(--> '(ef-light doom-one-light modus-operandi doom-tomorrow-day)
                  (-rotate my-picked-theme-rotate-delta it)))))

(defun my-personalize-themes (&optional force)
  (when (or force (daemonp))
    (setq ef-light-palette-overrides
          '((variable "DeepPink") (bg-mode-line "#F0F0F0")))
    (dolist (light-theme
             (alist-get 'light my-picked-theme-alist nil nil #'equal))
      (when (and light-theme
                 (string-prefix-p "^ef-" (format "%s" light-theme)))
        (add-hook
         (intern (format "%s-palette-overrides" light-theme))
         '(bg-main "white"))))
    (my-custom-theme-set-faces
      'ef-light
      `(highlight-quoted-symbol :foreground "OrangeRed")) ; default: inherits but frequently placed near `font-lock-constant-face'

    (my-custom-theme-set-faces
      'doom-dracula
      '(font-lock-builtin-face :slant italic :foreground "cyan") ; like official
      '(font-lock-constant-face :foreground "medium purple 1") ; like official
      '(font-lock-preprocessor-face :foreground "sandy brown") ; like official
      '(font-lock-regexp-grouping-backslash
        :foreground "cyan"
        :inherit bold) ; like official
      '(font-lock-string-face :foreground "olive drab 1") ; strings being yellow is too bright
      '(font-lock-type-face
        :foreground "cadet blue 2"
        :inherit font-lock-builtin-face)))) ; like official

(my-personalize-themes)
(dolist (theme (-mapcat 'cdr my-picked-theme-alist))
  (when theme
    ;; (set (intern (format "%s-brighter-comments" theme)) t)
    (set (intern (format "%s-comment-bg" theme)) nil)
    (set (intern (format "%s-colorful-headers" theme)) t)))
;; (setq custom-safe-themes t) ; Mark all themes as safe

(when (display-graphic-p)
  (setq doom-theme (cadr (assoc 'dark my-picked-theme-alist)))) ; overwrite later

;; query the system's color scheme asynchronously, so delay `doom-init-theme-h' until then
(my-theme-auto-set (not my-emacs-kit)
  (lambda (theme &rest _) (when (and my-emacs-default-face-colors (not theme)) (apply #'set-face-attribute 'default nil my-emacs-default-face-colors))))

;;; Multimedia

(add-hook 'my-yank-media-pre-hook #'my-image-convert-clipboard-to-preferred-format)

;;; Writing aid

(my-add-startup-hook
  (cond
   ((fboundp #'jinx-mode)
    (my-add-hook-once my-edit-mode-hooks #'global-jinx-mode))
   ((not my-emacs-kit)
    (my-flyspell-global-mode))))

(my-setup-jinx)

(with-eval-after-load 'ispell (my-config-ispell))
(with-eval-after-load 'flyspell (setcdr flyspell-mode-map nil)) ; Leave useful keys binding for other commands

(my-bind "<f2> u" #'my-copy-special-character)

(my-add-startup-hook (my-safe-add-hook/s 'text-mode-hook '(flymake-proselint-setup) 64)) ; after `flymake-mode'

;; Diacritics
(with-eval-after-load 'char-fold
  (setq! char-fold-include (pushnew! char-fold-include '(?d "đ"))))

;; On PGTK, "S-SPC" (shift+space) is caught by the input method, along many other keys
(if (boundp 'pgtk-initialized)
    (progn (setq pgtk-use-im-context-on-new-connection nil)
           (my-bind "s-SPC" #'my-fcitx-activate-IM&auto-mode))
  (my-safe (fcitx-default-setup)))

;;; Files?

(my-bind :map '(embark-file-map my-dired-key-mode-map) '("S-RET" "S-<return>") #'my-find-file-alternative-truename)

(add-to-ordered-list 'find-file-not-found-functions #'my-find-file-not-found-fn 0)

(defvar my-compress-suffix "zst" "No dot (.).")

;;; Other configurations

(my-accumulate-config-execute)

;; my-after-lib.el my-hook-mode-hook.el
(-let* ((regexp
         (my-rx->str
          '(seq
            string-start
            (or "my-" (seq (?  "my-") "site-lisp--"))
            (group (or "after" "hook"))
            "-"
            (group (* nonl))
            ".el"
            string-end))))
  (dolist (el-filepath (directory-files my-lisp-dir/ 'full regexp))
    (-let* ((filename (file-name-nondirectory el-filepath))
            ((_ type dest) (s-match regexp filename))
            (fn
             (lambda (&rest _)
               (dlet ((inhibit-message t))
                 (load filename)))))
      (cond
       ((equal type "hook")
        (-let* ((hooks
                 `(,(intern dest)
                   ,(intern
                     (replace-regexp-in-string
                      "-mode-hook$" "-ts-mode-hook" dest)))))
          (my-add-hook-once
            hooks (my-defalias* (intern el-filepath) fn))))
       ((equal type "after")
        (with-eval-after-load dest
          (funcall fn)))))))

(add-hook 'my-idle-run-once-hook #'my-idle-do-load-h)

(defvar my-idle-run-once-hook nil)
(defvar my-idle-run-repeat-hook nil)

;; non-essential things to run when idle without delaying Emacs
(run-with-idle-timer 60 nil #'run-hooks 'my-idle-run-once-hook)
(run-with-idle-timer (* 60 60) 'repeat #'run-hooks 'my-idle-run-repeat-hook)

;;; config.el ends here

(provide 'my-config)

;; Local Variables:
;; no-byte-compile: t
;; End:

;; M-x (emacs-init-time)

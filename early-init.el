;; -*- lexical-binding: t; -*-

;;; Preparations

(defconst my-emacs-micro-version
  (save-match-data
    (and (string-match "^[0-9]+\\.[0-9]+\\.\\([0-9]+\\)" emacs-version)
         (string-to-number (match-string 1 emacs-version)))))

(defvar my-emacs-not-latest (< 0 emacs-minor-version))

(defvar my-emacs-kit
  (cond
   ((boundp 'doom-version)
    'doom)
   (t
    nil))
  "Non-nil when using an Emacs distribution.")

(defvar my-emacs-kit-str (format "%s" (or my-emacs-kit "")))

(defvar my-emacs-profile-name (bound-and-true-p chemacs-profile-name))

(defvar my-emacs-conf-dir/
  (abbreviate-file-name
   (file-truename
    (file-name-directory (or load-file-name buffer-file-truename)))))

(defvar my-lisp-dir/ (concat my-emacs-conf-dir/ "lisp/"))

(defvar my-site-lisp-dir/ (concat my-emacs-conf-dir/ "site-lisp/"))

;; TODO: handle XDG_*_* variables https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html

(defvar my-local-share-emacs-dir/ (concat "~/.local/share/" "emacs/")
  "Sharable among all Emacs profiles.")

(defvar my-user-emacs-local-dir/ (concat my-emacs-conf-dir/ ".local/")
  "Local, VC-ignored things.")

;; (defvar my-local-state-emacs-dir/
;;   (concat my-user-emacs-local-dir/ (format "emacs-%s/" emacs-major-version))
;;   "Not sharable between Emacs profiles and versions.")

;; NOTE why not `no-littering'?: There are some problematic files such as
;; `package-quickstart.el' (different paths before and after loading
;; `no-littering', which is a package, too), `savehist.el' (`no-littering''s
;; `savehist-file' value is added to `load-path', which prevents loading `savehist'.)

(defun my-make-emacs-save-directories (&rest _)
  (make-directory my-local-share-emacs-dir/ t))
;; (make-directory my-local-state-emacs-dir/ t)


(defvar my-modules-dir/ (concat my-emacs-conf-dir/ "modules/"))

;; `package-user-dir' must be set in early-init, when `package-enable-at-startup' is non-nil
(defvar my-package-user-dir/
  (concat my-user-emacs-local-dir/ "elpa/"))

(defun my-set-package-user-dir ()
  (setq-default package-user-dir my-package-user-dir/)
  (setq-default package-quickstart-file
                (concat
                 my-user-emacs-local-dir/
                 (format "package-quickstart-%s%s.el"
                         emacs-major-version
                         (if my-emacs-kit
                             (format "-%s" my-emacs-kit)
                           "")))))

(when (not (member my-emacs-kit '(doom)))
  ;; even when this is executed, some times `package-user-dir' is still set somewhere else?
  (my-set-package-user-dir)
  (eval-after-load 'core-packages '(my-set-package-user-dir))
  (eval-after-load 'package '(my-set-package-user-dir)))

(defvar my-emacs-bin-directory/ (concat my-emacs-conf-dir/ "bin/"))

;; (setq-default
;;  ;; native-comp-speed 3                    ; Enable Tail Call Optimization
;;  ;; native-comp-deferred-compilation t     ; Doom used to disable this
;;  )

(setq-default package-quickstart t)
;; more controlled behaviors
(setq-default package-enable-at-startup nil)

(defvar gc-cons-threshold-orig gc-cons-threshold)
(defvar gc-cons-percentage-orig gc-cons-percentage)

(unless my-emacs-kit
  
  ;; Avoid garbage collection from slowing down startup, remember to re-set them later
  (setq-default gc-cons-threshold most-positive-fixnum)
  (setq-default gc-cons-percentage 0.75)

  (setq-default read-process-output-max (* 1024 1024)) ;; 1mb, for LSP

  (setq-default inhibit-startup-screen t)

  ;; `after-init-hook' is ran before `emacs-startup-hook'
  (add-hook
   'emacs-startup-hook
   (lambda ()
     ;; (print (list gc-cons-threshold gc-cons-percentage))
     (setq-default gc-cons-threshold (* 64 (expt 2 20)))
     (setq-default gc-cons-percentage gc-cons-percentage-orig))))

;; When changing the font to be larger, tell Emacs not to (futilely) resize the
;; frame to preserve the number of displaying columns
;; https://tony-zorman.com/posts/2022-10-22-emacs-potpourri.html
(setq-default frame-inhibit-implied-resize t)

;; (defvar my-early-init-loaded-times 0)
;; (setq-default my-early-init-loaded-times (1+ my-early-init-loaded-times))

;; ;; reduce the blinding white light time frame at startup
;; (set-background-color "black")

;; from Doom
(push '(tool-bar-lines . 0)   default-frame-alist)
(setq-default tool-bar-mode nil)
;; I want scroll bar displayed, but they if only they are thinner
(push '(vertical-scroll-bars) default-frame-alist)
(setq-default scroll-bar-mode nil)

;; Hide mode line until a prettier implement appears, only effective
;; when set in early init?
(defvar my-mode-line-format-default mode-line-format)
(setq-default mode-line-format nil)

(provide 'my-early-init)

;; Local Variables:
;; no-byte-compile: t
;; End:

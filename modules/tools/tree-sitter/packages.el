;; -*- lexical-binding: t; no-byte-compile: t; -*-

(package! tree-sitter)
(package! tree-sitter-langs)

;; -*- lexical-binding: t; no-byte-compile: t; -*-

(package! evil)
(package! evil-collection)

(package! evil-nerd-commenter)
(package! evil-snipe)
(package! evil-surround)

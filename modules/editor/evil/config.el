;; -*- lexical-binding: t; -*-

(require 'dash)

;; (use-package! evil :demand t)

(defun my-doom-use-package!-evil--filter-args--a (args)
  (-let* (((name . body) args)
          (commands (my-plist-get* body :commands)))
    `(,name :defer t ,@(and commands `(:commands ,@commands)))))

(advice-add
 #'use-package!
 :filter-args #'my-doom-use-package!-evil--filter-args--a)

(load (file-name-concat doom-emacs-dir "modules/editor/evil/config")
      'noerror
      'nomessage)

(advice-remove #'use-package! #'my-doom-use-package!-evil--filter-args--a)

;; NOTE: For unknown reasons, advising `+evil-escape-a' directly will prevent
;; functions in `my-evil-force-normal-state-hook' from running, so let it be the
;; last element in that hook
(add-hook 'my-evil-force-normal-state-hook #'doom/escape 96)
;; (advice-add #'evil-force-normal-state :after #'+evil-escape-a)

(advice-add #'evil-join :around #'+evil-join-a)

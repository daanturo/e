;; -*- lexical-binding: t; -*-

;; ;; Unless symlinking autoload(s)
;; (defalias #'set-evil-initial-state! #'ignore)

;; many Doom settings require (featurep 'evil)
(unless noninteractive

  (require 'evil)

  ;; (push 'evil features)
  ;; ;; `evil' was pushed to `features' without loading (for laziness) to
  ;; ;; force Doom to configure
  ;; (when (and (not (featurep 'evil-core)) (featurep 'evil))
  ;;   (setq features (delete 'evil features)))


  ;;
  )

;; force `evil-collection' initialization even when not using +everywhere
(defun my-evil-fake-modulep!-+everywhere-a (mcr catgr &optional modl flag)
  (if (equal catgr '+everywhere)
      t
    (eval `(,mcr ,catgr ',modl ',flag) t)))

(with-eval-after-load 'evil
  (my-with-advice
    #'modulep!
    :around #'my-evil-fake-modulep!-+everywhere-a
    (load (file-name-concat doom-emacs-dir "modules/editor/evil/init")
          'noerror 'nomessage)))

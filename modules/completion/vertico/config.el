;; -*- lexical-binding: t; -*-

(with-eval-after-load 'marginalia
  (pushnew!
   marginalia-command-categories
   '(+default/find-file-under-here . file)
   '(doom/find-file-in-emacsd . project-file)
   '(doom/find-file-in-other-project . project-file)
   '(doom/find-file-in-private-config . file)
   '(doom/describe-active-minor-mode . minor-mode)))

;; Doom's `embark' config somehow disable `which-key''s `which-key-undo-key'
;; capability
